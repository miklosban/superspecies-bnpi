# SuperSpecies

Superspecies

What is this? 
Scientific species names and additional information in a "modular" format for easy data management.

It is automatically checking input data sources and create a new database and an export after each update of source files

Source files (_atomic tables_) always have two columns:
Scientific_name additional_information

These simple tables merged into a big table in the database creating process and in this process and before this process all tables and connections checked automatically to ensure the final output is valid and correct.

## Get SuperSpecies output
https://openbiomaps.gitlab.io/superspecies/


## Build the development environment for major updates, e.g. adding new rules
At first time run these commands to get database up and running:

```
mkdir ~/.dbt && cat ./profiles.yml >> ~/.dbt/profiles.yml
docker-compose up -d
dbt deps
dbt seed
dbt compile
dbt run
```

Without setting dbt profiles
```
docker-compose up -d
dbt deps --profiles-dir ./
dbt seed --profiles-dir ./
dbt compile --profiles-dir ./
dbt run --profiles-dir ./
```

Than open http://localhost:36433 in browser (look up login credentials in profiles.yml)

### Try running the following commands:
- dbt seed
- dbt run
- dbt test


## Updating the database - adding new rows into some atomic tables

Example:

Adding a new species names

Check the new name in the database 
```
grep "Spinus spinus" ssp_*.csv
```

and check the synonyms
```
grep "Carduelis spinus" ssp_*.csv
```

Get last index of speciesnames:
```
tail -n -1 ssp_speciesnames.csv
```
Add the new name to the database
```
echo "50633","Spinus spinus" >> ssp_speciesnames.csv
```
Add to other tables:
```
echo '"Spinus spinus","25000"' >> ssp_eszmeiertek.csv
echo '"Spinus spinus","FV"' >> ssp_iagreements_bern.csv
echo '"Spinus spinus","csíz"' >> ssp_nationalnames_hun.csv
echo '"Spinus spinus","BD_2"' >> ssp_natura2000.csv
echo '"Spinus spinus","CARSPI"' >> ssp_shortnames.csv
echo '"Spinus spinus","Védett"' >> ssp_vedettseg.csv
echo '"Spinus spinus","madarak"' >> ssp_taxon_hun.csv
```

Update a table
```
echo '"Spinus spinus","accepted name"' >> ssp_namestatus.csv
```
update Carduelis spinus to synonym.... ?? in libreoffice or using csvsql


USING csvkit in-memory SQL database
```
csvsql -i postgresql ssp_namestatus.csv
csvsql --query "UPDATE ssp_namestatus SET namestatus='synonym for Spinus spinus' WHERE species_name='Carduelis spinus'; SELECT * FROM ssp_namestatus" ssp_namestatus.csv > ssp_namestatus.csv
```


