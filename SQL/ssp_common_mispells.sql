\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_common_mispells DROP CONSTRAINT ssp_common_mispells_species_name_fkey;
--ALTER TABLE ONLY public.ssp_common_mispells DROP CONSTRAINT ssp_common_mispells_pkey;
--DROP TABLE public.ssp_common_mispells;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_common_mispells; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_common_mispells (
    species_name character varying(64) NOT NULL,
    mispell character varying(64)[] NOT NULL
);


ALTER TABLE public.ssp_common_mispells OWNER TO ssp_admin;

--
-- Data for Name: ssp_common_mispells; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_common_mispells (species_name, mispell) FROM stdin;
Platanthera bifolia	{"Plathantera bifolia"}
Hemaris tityus	{"Hemaris tytius"}
Cerambyx cerdo	{"Cerambix cerdo"}
Melitaea athalia	{"Melithea athalia"}
\.


--
-- Name: ssp_common_mispells ssp_common_mispells_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_common_mispells
    ADD CONSTRAINT ssp_common_mispells_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_common_mispells ssp_common_mispells_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_common_mispells
    ADD CONSTRAINT ssp_common_mispells_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

