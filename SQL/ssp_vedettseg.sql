\c superspecies;
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Debian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Debian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--ALTER TABLE ONLY public.ssp_vedettseg DROP CONSTRAINT ssp_vedettseg_species_name_fkey;
--ALTER TABLE ONLY public.ssp_vedettseg DROP CONSTRAINT ssp_vedettseg_pkey;
--DROP TABLE public.ssp_vedettseg;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ssp_vedettseg; Type: TABLE; Schema: public; Owner: ssp_admin
--

CREATE TABLE public.ssp_vedettseg (
    species_name character varying(64) NOT NULL,
    vedettseg character varying(24) NOT NULL
);


ALTER TABLE public.ssp_vedettseg OWNER TO ssp_admin;

--
-- Data for Name: ssp_vedettseg; Type: TABLE DATA; Schema: public; Owner: ssp_admin
--

COPY public.ssp_vedettseg (species_name, vedettseg) FROM stdin;
Acanthopsyche siederi	Nem védett / indifferens
Acanthosoma haemorrhoidale	Nem védett / indifferens
Acartauchenius scurrilis	Nem védett / indifferens
Acentrotypus brunnipes	Nem védett / indifferens
Acetropis carinata	Nem védett / indifferens
Acetropis longirostris	Nem védett / indifferens
Parasteatoda lunata	Nem védett / indifferens
Cryptachaea riparia	Nem védett / indifferens
Parasteatoda simulans	Nem védett / indifferens
Parasteatoda tepidariorum	Nem védett / indifferens
Achenium ephippium	Nem védett / indifferens
Achenium humile	Nem védett / indifferens
Acherontia atropos	Védett
Acheta domesticus	Nem védett / indifferens
Achroia grisella	Nem védett / indifferens
Achtheres percarum	Nem védett / indifferens
Achlya flavicornis	Nem védett / indifferens
Achyra nudalis	Nem védett / indifferens
Acidota crenata	Nem védett / indifferens
Acidota cruentata	Nem védett / indifferens
Acilius canaliculatus	Nem védett / indifferens
Acasis appensata	Védett
Acasis viretata	Nem védett / indifferens
Accipiter brevipes	Fokozottan védett
Accipiter gentilis	Védett
Accipiter gentilis buteoides	Védett
Accipiter nisus	Védett
Acentria ephemerella	Nem védett / indifferens
Adalia decempunctata	Nem védett / indifferens
Adarrus multinotatus	Nem védett / indifferens
Adela croesella	Nem védett / indifferens
Adela cuprella	Nem védett / indifferens
Adela mazzolella	Nem védett / indifferens
Adela paludicolella	Nem védett / indifferens
Adela reaumurella	Nem védett / indifferens
Adela violella	Nem védett / indifferens
Lacon punctatus	Nem védett / indifferens
Lacon querceus	Védett
Adelphocoris lineolatus	Nem védett / indifferens
Adelphocoris quadripunctatus	Nem védett / indifferens
Adelphocoris reichelii	Nem védett / indifferens
Adelphocoris seticornis	Nem védett / indifferens
Adelphocoris ticinensis	Nem védett / indifferens
Adelphocoris vandalicus	Nem védett / indifferens
Aderus populneus	Nem védett / indifferens
Adexius scrobipennis	Nem védett / indifferens
Adicella balcanica	Nem védett / indifferens
Adicella filicornis	Nem védett / indifferens
Adicella reducta	Nem védett / indifferens
Adicella syriaca	Nem védett / indifferens
Adomerus biguttatus	Nem védett / indifferens
Adosomus roridus	Nem védett / indifferens
Adoxophyes orana	Nem védett / indifferens
Adrastus axillaris	Nem védett / indifferens
Adrastus kryshtali	Nem védett / indifferens
Adrastus lacertosus	Nem védett / indifferens
Adrastus limbatus	Nem védett / indifferens
Adrastus montanus	Nem védett / indifferens
Adrastus pallens	Nem védett / indifferens
Adscita geryon	Védett
Adscita statices	Nem védett / indifferens
Aedes cinereus	Nem védett / indifferens
Aedes rossicus	Nem védett / indifferens
Aedes vexans	Nem védett / indifferens
Aedia funesta	Nem védett / indifferens
Aedia leucomelas	Nem védett / indifferens
Aegithalos caudatus	Védett
Aegle kaekeritziana	Nem védett / indifferens
Aegolius funereus	Védett
Aegomorphus clavipes	Nem védett / indifferens
Aegopinella minor	Nem védett / indifferens
Aegopinella nitens	Nem védett / indifferens
Aegopinella pura	Nem védett / indifferens
Aegopinella ressmanni	Nem védett / indifferens
Aegopis verticillus	Nem védett / indifferens
Aegosoma scabricorne	Védett
Aegypius monachus	Fokozottan védett
Aeletes atomarius	Nem védett / indifferens
Aeletes hopffgarteni	Nem védett / indifferens
Aelia acuminata	Nem védett / indifferens
Aelia klugii	Nem védett / indifferens
Aelia rostrata	Nem védett / indifferens
Aellopus atratus	Nem védett / indifferens
Aelurillus m-nigrum	Nem védett / indifferens
Aelurillus v-insignitus	Nem védett / indifferens
Aesalus scarabaeoides	Védett
Aeshna affinis	Nem védett / indifferens
Aeshna cyanea	Nem védett / indifferens
Aeshna grandis	Nem védett / indifferens
Aeshna juncea	Nem védett / indifferens
Acalles ptinoides	Nem védett / indifferens
Kyklioacalles roboris	Nem védett / indifferens
Acallocrates denticollis	Nem védett / indifferens
Acalypta carinata	Nem védett / indifferens
Acalypta gracilis	Nem védett / indifferens
Acalypta marginata	Nem védett / indifferens
Acalypta musci	Nem védett / indifferens
Acalypta nigrina	Nem védett / indifferens
Acalypta parvula	Nem védett / indifferens
Acalypta platycheila	Nem védett / indifferens
Acalyptris loranthella	Nem védett / indifferens
Acalyptus carpini	Nem védett / indifferens
Acalyptus sericeus	Nem védett / indifferens
Acanthaclisis occitanica	Védett
Acanthinula aculeata	Nem védett / indifferens
Acanthobodilus immundus	Nem védett / indifferens
Acanthocinus aedilis	Védett
Acanthocinus griseus	Nem védett / indifferens
Abacoproeces saltuum	Nem védett / indifferens
Abax carinatus	Nem védett / indifferens
Abax ovalis	Nem védett / indifferens
Abax parallelepipedus	Nem védett / indifferens
Abax parallelus	Nem védett / indifferens
Abax schueppeli	Nem védett / indifferens
Abax schueppeli rendschmidtii	Nem védett / indifferens
Abdera affinis	Nem védett / indifferens
Abdera quadrifasciata	Nem védett / indifferens
Abdera triguttata	Nem védett / indifferens
Abemus chloropterus	Nem védett / indifferens
Ablattaria laevigata	Nem védett / indifferens
Ablepharus kitaibelii fitzingeri	Fokozottan védett
Abraeus granulum	Nem védett / indifferens
Abraeus perpusillus	Nem védett / indifferens
Abraeus roubali	Nem védett / indifferens
Abramis ballerus	Nem védett / indifferens
Blicca bjoerkna	Nem védett / indifferens
Abramis brama	Nem védett / indifferens
Abramis sapa	Nem védett / indifferens
Abraxas grossulariata	Nem védett / indifferens
Abrostola agnorista	Védett
Abrostola asclepiadis	Nem védett / indifferens
Abrostola tripartita	Nem védett / indifferens
Abrostola triplasia	Nem védett / indifferens
Absidia pilosa	Nem védett / indifferens
Absidia rufotestacea	Nem védett / indifferens
Acalles camelus	Nem védett / indifferens
Acalles commutatus	Nem védett / indifferens
Acalles echinatus	Nem védett / indifferens
Acalles papei	Nem védett / indifferens
Acalles papei balcanicus	Nem védett / indifferens
Acalles parvulus	Nem védett / indifferens
Acanthococcus aceris	Nem védett / indifferens
Acanthococcus desertus	Nem védett / indifferens
Acanthococcus devoniensis	Nem védett / indifferens
Acanthococcus erinaceus	Nem védett / indifferens
Acanthococcus greeni	Nem védett / indifferens
Acanthococcus munroi	Nem védett / indifferens
Acanthococcus roboris	Nem védett / indifferens
Acanthococcus uvaeursi	Nem védett / indifferens
Acanthocyclops robustus	Nem védett / indifferens
Acanthocyclops vernalis	Nem védett / indifferens
Acanthodelphax denticauda	Nem védett / indifferens
Acanthodelphax spinosus	Nem védett / indifferens
Acanthodiaptomus denticornis	Nem védett / indifferens
Acantholeberis curvirostris	Nem védett / indifferens
Dichomeris alacella	Nem védett / indifferens
Acanthopsyche atra	Nem védett / indifferens
Acanthopsyche ecksteini	Nem védett / indifferens
Aeshna mixta	Nem védett / indifferens
Aeshna viridis	Fokozottan védett
Acilius sulcatus	Nem védett / indifferens
Acinopus ammophilus	Védett
Acinopus picipes	Védett
Acipenser baeri	Nem védett / indifferens
Acipenser gueldenstaedtii	Védett
Acipenser nudiventris	Védett
Acipenser ruthenus	Nem védett / indifferens
Acipenser stellatus	Védett
Aclerda subterranea	Nem védett / indifferens
Acleris bergmanniana	Nem védett / indifferens
Acleris cristana	Nem védett / indifferens
Acleris emargana	Nem védett / indifferens
Acleris ferrugana	Nem védett / indifferens
Acleris fimbriana	Nem védett / indifferens
Acleris forsskaleana	Nem védett / indifferens
Acleris hastiana	Nem védett / indifferens
Acleris hippophaeana	Nem védett / indifferens
Acleris holmiana	Nem védett / indifferens
Acleris kochiella	Nem védett / indifferens
Acleris lipsiana	Nem védett / indifferens
Acleris literana	Nem védett / indifferens
Acleris logiana	Nem védett / indifferens
Acleris lorquiniana	Nem védett / indifferens
Acleris notana	Nem védett / indifferens
Acleris permutana	Nem védett / indifferens
Acleris quercinana	Nem védett / indifferens
Acleris rhombana	Nem védett / indifferens
Acleris roscidana	Nem védett / indifferens
Acleris rufana	Nem védett / indifferens
Acleris scabrana	Nem védett / indifferens
Acleris schalleriana	Nem védett / indifferens
Acleris shepherdana	Nem védett / indifferens
Acleris sparsana	Nem védett / indifferens
Acleris umbrana	Nem védett / indifferens
Acleris variegana	Nem védett / indifferens
Aclypea undata	Nem védett / indifferens
Acmaeodera degener	Védett
Acmaeoderella flavofasciata	Nem védett / indifferens
Acmaeoderella mimonti	Védett
Acmaeops pratensis	Nem védett / indifferens
Acompsia cinerella	Nem védett / indifferens
Acompsia tripunctella	Nem védett / indifferens
Acompus pallipes	Nem védett / indifferens
Acompus rufipes	Nem védett / indifferens
Acontia lucida	Nem védett / indifferens
Aconurella prolixa	Nem védett / indifferens
Aconurella quadrum	Nem védett / indifferens
Acosmetia caliginosa	Nem védett / indifferens
Acrida ungarica	Védett
Acritus homoeopathicus	Nem védett / indifferens
Acritus minutus	Nem védett / indifferens
Acritus nigricornis	Nem védett / indifferens
Acrobasis consociella	Nem védett / indifferens
Acrobasis glaucella	Nem védett / indifferens
Acrobasis obtusella	Nem védett / indifferens
Acrobasis sodalella	Nem védett / indifferens
Acrocephalus agricola	Védett
Acrocephalus agricola septima	Védett
Acrocephalus arundinaceus	Védett
Acrocephalus melanopogon	Védett
Acrocephalus paludicola	Fokozottan védett
Acrocephalus palustris	Védett
Acrocephalus schoenobaenus	Védett
Acrocephalus scirpaceus	Védett
Acrocercops brongniardella	Nem védett / indifferens
Acrochordum evae	Nem védett / indifferens
Acrolepia autumnitella	Nem védett / indifferens
Acrolepiopsis tauricella	Nem védett / indifferens
Acrolepiopsis assectella	Nem védett / indifferens
Acrolocha minuta	Nem védett / indifferens
Acrolocha pliginskii	Nem védett / indifferens
Acroloxus lacustris	Nem védett / indifferens
Acronicta aceris	Nem védett / indifferens
Acronicta alni	Nem védett / indifferens
Acronicta auricoma	Nem védett / indifferens
Acronicta cuspis	Nem védett / indifferens
Acronicta euphorbiae	Nem védett / indifferens
Acronicta leporina	Nem védett / indifferens
Acronicta megacephala	Nem védett / indifferens
Acronicta psi	Nem védett / indifferens
Acronicta rumicis	Nem védett / indifferens
Acronicta strigosa	Nem védett / indifferens
Acronicta tridens	Nem védett / indifferens
Acroperus elongatus	Nem védett / indifferens
Acroperus harpae	Nem védett / indifferens
Acrossus depressus	Nem védett / indifferens
Acrossus luridus	Nem védett / indifferens
Acrossus rufipes	Nem védett / indifferens
Acrotona aterrima	Nem védett / indifferens
Acrotona benicki	Nem védett / indifferens
Acrotona convergens	Nem védett / indifferens
Acrotona exigua	Nem védett / indifferens
Acrotona muscorum	Nem védett / indifferens
Acrotona nigerrima	Nem védett / indifferens
Acrotona obfuscata	Nem védett / indifferens
Acrotona parens	Nem védett / indifferens
Acrotona parvula	Nem védett / indifferens
Acrotona piceorufa	Nem védett / indifferens
Acrotona pygmaea	Nem védett / indifferens
Acrotona troglodytes	Nem védett / indifferens
Acrotrichis arnoldi	Nem védett / indifferens
Acrotrichis atomaria	Nem védett / indifferens
Acrotrichis brevipennis	Nem védett / indifferens
Acrotrichis dispar	Nem védett / indifferens
Acrotrichis fascicularis	Nem védett / indifferens
Acrotrichis grandicollis	Nem védett / indifferens
Acrotrichis intermedia	Nem védett / indifferens
Acrotrichis montandoni	Nem védett / indifferens
Acrotrichis norvegica	Nem védett / indifferens
Acrotrichis sericans	Nem védett / indifferens
Acrotrichis sitkaensis	Nem védett / indifferens
Acrotrichis thoracica	Nem védett / indifferens
Acrotylus insubricus	Nem védett / indifferens
Acrotylus longipes	Védett
Acrulia inflata	Nem védett / indifferens
Actebia praecox	Védett
Actenia brunnealis	Nem védett / indifferens
Actenia honestalis	Nem védett / indifferens
Actenicerus siaelandicus	Nem védett / indifferens
Actidium boudieri	Nem védett / indifferens
Actinotia polyodon	Nem védett / indifferens
Actinotia radiosa	Nem védett / indifferens
Actites hypoleucos	Védett
Aculepeira armida	Nem védett / indifferens
Aculepeira ceropegia	Nem védett / indifferens
Acupalpus brunnipes	Nem védett / indifferens
Acupalpus dubius	Nem védett / indifferens
Acupalpus elegans	Nem védett / indifferens
Acupalpus exiguus	Nem védett / indifferens
Acupalpus flavicollis	Nem védett / indifferens
Acupalpus interstitialis	Nem védett / indifferens
Acupalpus luteatus	Nem védett / indifferens
Acupalpus maculatus	Nem védett / indifferens
Acupalpus meridianus	Nem védett / indifferens
Acupalpus notatus	Nem védett / indifferens
Acupalpus parvulus	Nem védett / indifferens
Acupalpus suturalis	Nem védett / indifferens
Acylophorus glaberrimus	Nem védett / indifferens
Adaina microdactyla	Nem védett / indifferens
Adalia bipunctata	Nem védett / indifferens
Adalia conglomerata	Nem védett / indifferens
Aethalura punctulata	Nem védett / indifferens
Aethes beatricella	Nem védett / indifferens
Aethes bilbaensis	Nem védett / indifferens
Aethes cnicana	Nem védett / indifferens
Aethes dilucidana	Nem védett / indifferens
Aethes flagellana	Nem védett / indifferens
Aethes francillana	Nem védett / indifferens
Aethes hartmanniana	Nem védett / indifferens
Aethes kindermanniana	Nem védett / indifferens
Aethes margaritana	Nem védett / indifferens
Aethes margarotana	Nem védett / indifferens
Aethes moribundana	Nem védett / indifferens
Aethes nefandana	Nem védett / indifferens
Aethes piercei	Nem védett / indifferens
Aethes rubigana	Nem védett / indifferens
Aethes rutilana	Nem védett / indifferens
Aethes sanguinana	Nem védett / indifferens
Aethes smeathmanniana	Nem védett / indifferens
Aethes tesserana	Nem védett / indifferens
Aethes tornella	Nem védett / indifferens
Aethes triangulana	Nem védett / indifferens
Aethes vicinana	Nem védett / indifferens
Aethes williana	Nem védett / indifferens
Byrsinus flavicornis	Nem védett / indifferens
Agabus affinis	Nem védett / indifferens
Agabus biguttatus	Nem védett / indifferens
Agabus bipustulatus	Nem védett / indifferens
Agabus congener	Nem védett / indifferens
Agabus conspersus	Nem védett / indifferens
Agabus fuscipennis	Nem védett / indifferens
Agabus guttatus	Nem védett / indifferens
Agabus labiatus	Nem védett / indifferens
Agabus melanarius	Nem védett / indifferens
Agabus nebulosus	Nem védett / indifferens
Agabus paludosus	Nem védett / indifferens
Agabus striolatus	Nem védett / indifferens
Agabus uliginosus	Nem védett / indifferens
Agabus undulatus	Nem védett / indifferens
Agalenatea redii	Nem védett / indifferens
Agallia brachyptera	Nem védett / indifferens
Agallia consobrina	Nem védett / indifferens
Agalmatium flavescens	Nem védett / indifferens
Agapanthia cardui	Nem védett / indifferens
Agapanthia cynarae	Nem védett / indifferens
Agapanthia dahli	Nem védett / indifferens
Agapanthia intermedia	Nem védett / indifferens
Agapanthia kirbyi	Nem védett / indifferens
Agapanthia maculicornis	Védett
Agapanthia osmanlis	Nem védett / indifferens
Agapanthia villosoviridescens	Nem védett / indifferens
Agapanthia violacea	Nem védett / indifferens
Agapanthiola leucaspis	Védett
Agapeta hamana	Nem védett / indifferens
Agapeta largana	Nem védett / indifferens
Agapeta zoegana	Nem védett / indifferens
Agapetus delicatulus	Nem védett / indifferens
Agapetus fuscipes	Nem védett / indifferens
Agapetus laniger	Nem védett / indifferens
Agapetus ochripes	Nem védett / indifferens
Agardhiella lamellata	Nem védett / indifferens
Agardhiella parreyssii	Nem védett / indifferens
Agaricochara latissima	Nem védett / indifferens
Agaricophagus cephalotes	Nem védett / indifferens
Agaricophagus reitteri	Nem védett / indifferens
Agathidium arcticum	Nem védett / indifferens
Agathidium atrum	Nem védett / indifferens
Agathidium badium	Nem védett / indifferens
Agathidium banaticum	Nem védett / indifferens
Agathidium bohemicum	Nem védett / indifferens
Agathidium confusum	Nem védett / indifferens
Agathidium discoideum	Nem védett / indifferens
Agathidium haemorrhoum	Nem védett / indifferens
Agathidium laevigatum	Nem védett / indifferens
Agathidium leonhardianum	Nem védett / indifferens
Agathidium mandibulare	Nem védett / indifferens
Agathidium marginatum	Nem védett / indifferens
Agathidium nigrinum	Nem védett / indifferens
Agathidium nigripenne	Nem védett / indifferens
Agathidium nudum	Nem védett / indifferens
Agathidium pisanum	Nem védett / indifferens
Agathidium plagiatum	Nem védett / indifferens
Agathidium pseudopallidum	Nem védett / indifferens
Agathidium seminulum	Nem védett / indifferens
Agathidium varians	Nem védett / indifferens
Agdistis adactyla	Nem védett / indifferens
Agdistis heydeni	Nem védett / indifferens
Agdistis intermedia	Védett
Agdistis tamaricis	Nem védett / indifferens
Allagelena gracilens	Nem védett / indifferens
Agelena labyrinthica	Nem védett / indifferens
Agenioideus apicalis	Nem védett / indifferens
Agenioideus cinctellus	Nem védett / indifferens
Agenioideus nubecula	Nem védett / indifferens
Agenioideus sericeus	Nem védett / indifferens
Agenioideus usurarius	Nem védett / indifferens
Aglais urticae	Védett
Aglenus brunneus	Nem védett / indifferens
Aglia tau	Védett
Aglossa caprealis	Nem védett / indifferens
Aglossa pinguinalis	Nem védett / indifferens
Aglossa signicostalis	Nem védett / indifferens
Agnathus decoratus	Védett
Agnetina elegantula	Védett
Agnocoris reclairei	Nem védett / indifferens
Agnocoris rubicundus	Nem védett / indifferens
Agoliinus nemoralis	Nem védett / indifferens
Agonopterix adspersella	Nem védett / indifferens
Agonopterix alstromeriana	Nem védett / indifferens
Agonopterix angelicella	Nem védett / indifferens
Agonopterix arenella	Nem védett / indifferens
Agonopterix assimilella	Nem védett / indifferens
Agonopterix astrantiae	Nem védett / indifferens
Agonopterix atomella	Nem védett / indifferens
Agonopterix capreolella	Nem védett / indifferens
Agonopterix carduella	Nem védett / indifferens
Agonopterix ciliella	Nem védett / indifferens
Agonopterix cnicella	Nem védett / indifferens
Agonopterix curvipunctosa	Nem védett / indifferens
Agonopterix doronicella	Nem védett / indifferens
Agonopterix furvella	Nem védett / indifferens
Agonopterix heracliana	Nem védett / indifferens
Agonopterix hippomarathri	Nem védett / indifferens
Agonopterix kaekeritziana	Nem védett / indifferens
Agonopterix laterella	Nem védett / indifferens
Agonopterix liturosa	Nem védett / indifferens
Agonopterix nanatella	Nem védett / indifferens
Agonopterix nervosa	Nem védett / indifferens
Agonopterix ocellana	Nem védett / indifferens
Agonopterix oinochroa	Nem védett / indifferens
Brenthis daphne	Védett
Agonopterix pallorella	Nem védett / indifferens
Agonopterix parilella	Nem védett / indifferens
Agonopterix petasitis	Nem védett / indifferens
Agonopterix propinquella	Nem védett / indifferens
Agonopterix purpurea	Nem védett / indifferens
Agonopterix putridella	Nem védett / indifferens
Agonopterix rotundella	Nem védett / indifferens
Agonopterix selini	Nem védett / indifferens
Agonopterix senecionis	Nem védett / indifferens
Agonopterix subpropinquella	Nem védett / indifferens
Agonopterix thapsiella	Nem védett / indifferens
Agonopterix yeatiana	Nem védett / indifferens
Agonum afrum	Nem védett / indifferens
Agonum angustatum	Nem védett / indifferens
Agonum atratum	Nem védett / indifferens
Agonum duftschmidi	Nem védett / indifferens
Agonum gracilipes	Nem védett / indifferens
Agonum hypocrita	Nem védett / indifferens
Agonum impressum	Nem védett / indifferens
Agonum longicorne	Nem védett / indifferens
Agonum lugens	Nem védett / indifferens
Agonum marginatum	Nem védett / indifferens
Agonum muelleri	Nem védett / indifferens
Agonum nigrum	Nem védett / indifferens
Agonum permoestum	Nem védett / indifferens
Agonum sexpunctatum	Nem védett / indifferens
Agonum versutum	Nem védett / indifferens
Agonum viduum	Nem védett / indifferens
Agonum viridicupreum	Nem védett / indifferens
Agramma atricapillum	Nem védett / indifferens
Agramma confusum	Nem védett / indifferens
Agramma fallax	Nem védett / indifferens
Agramma laetum	Nem védett / indifferens
Agramma minutum	Nem védett / indifferens
Agramma ruficorne	Nem védett / indifferens
Agraylea multipunctata	Nem védett / indifferens
Agraylea sexmaculata	Nem védett / indifferens
Agrilinus ater	Nem védett / indifferens
Agrilinus convexus	Nem védett / indifferens
Agrilinus rufus	Nem védett / indifferens
Agrilinus sordidus	Nem védett / indifferens
Agrilus albogularis	Nem védett / indifferens
Agrilus angustulus	Nem védett / indifferens
Agrilus ater	Nem védett / indifferens
Agrilus auricollis	Nem védett / indifferens
Agrilus betuleti	Nem védett / indifferens
Agrilus biguttatus	Nem védett / indifferens
Agrilus convexicollis	Nem védett / indifferens
Agrilus convexifrons	Nem védett / indifferens
Agrilus croaticus	Nem védett / indifferens
Agrilus cuprescens	Nem védett / indifferens
Agrilus cyanescens	Nem védett / indifferens
Agrilus delphinensis	Nem védett / indifferens
Agrilus derasofasciatus	Nem védett / indifferens
Agrilus graminis	Nem védett / indifferens
Agrilus guerini	Védett
Agrilus hastulifer	Nem védett / indifferens
Agrilus hyperici	Nem védett / indifferens
Agrilus integerrimus	Nem védett / indifferens
Agrilus kubani	Nem védett / indifferens
Agrilus laticornis	Nem védett / indifferens
Agrilus lineola	Nem védett / indifferens
Agrilus litura	Nem védett / indifferens
Agrilus macroderus	Nem védett / indifferens
Agrilus obscuricollis	Nem védett / indifferens
Agrilus olivicolor	Nem védett / indifferens
Agrilus populneus	Nem védett / indifferens
Agrilus pratensis	Nem védett / indifferens
Agrilus roscidus	Nem védett / indifferens
Agrilus salicis	Nem védett / indifferens
Agrilus sericans	Nem védett / indifferens
Agrilus sinuatus	Nem védett / indifferens
Agrilus subauratus	Nem védett / indifferens
Agrilus sulcicollis	Nem védett / indifferens
Agrilus viridis	Nem védett / indifferens
Agrilus viscivorus	Nem védett / indifferens
Agriopis aurantiaria	Nem védett / indifferens
Agriopis bajaria	Nem védett / indifferens
Agriopis leucophaearia	Nem védett / indifferens
Agriopis marginaria	Nem védett / indifferens
Agriotes acuminatus	Nem védett / indifferens
Agriotes brevis	Nem védett / indifferens
Agriotes lineatus	Nem védett / indifferens
Agriotes medvedevi	Nem védett / indifferens
Agriotes modestus	Nem védett / indifferens
Agriotes obscurus	Nem védett / indifferens
Agriotes pallidulus	Nem védett / indifferens
Agriotes pilosellus	Nem védett / indifferens
Agriotes proximus	Nem védett / indifferens
Agriotes rufipalpis	Nem védett / indifferens
Agriotes sputator	Nem védett / indifferens
Agriotes ustulatus	Nem védett / indifferens
Agriphila brioniellus	Nem védett / indifferens
Agriphila deliella	Nem védett / indifferens
Agriphila geniculea	Nem védett / indifferens
Agriphila hungaricus	Nem védett / indifferens
Agriphila inquinatella	Nem védett / indifferens
Agriphila latistria	Nem védett / indifferens
Agriphila poliellus	Nem védett / indifferens
Agriphila selasella	Nem védett / indifferens
Agriphila straminella	Nem védett / indifferens
Agriphila tolli	Nem védett / indifferens
Agriphila tristella	Nem védett / indifferens
Agrius convolvuli	Nem védett / indifferens
Agrochola circellaris	Nem védett / indifferens
Agrochola helvola	Nem védett / indifferens
Agrochola humilis	Nem védett / indifferens
Agrochola laevis	Nem védett / indifferens
Agrochola litura	Nem védett / indifferens
Agrochola lota	Nem védett / indifferens
Agrochola lychnidis	Nem védett / indifferens
Agrochola macilenta	Nem védett / indifferens
Agrochola nitida	Nem védett / indifferens
Agroeca brunnea	Nem védett / indifferens
Agroeca cuprea	Nem védett / indifferens
Agroeca lusatica	Nem védett / indifferens
Agroeca proxima	Nem védett / indifferens
Agrotera nemoralis	Nem védett / indifferens
Agrotis bigramma	Nem védett / indifferens
Agrotis cinerea	Nem védett / indifferens
Agrotis clavis	Nem védett / indifferens
Agrotis exclamationis	Nem védett / indifferens
Agrotis ipsilon	Nem védett / indifferens
Agrotis puta	Nem védett / indifferens
Agrotis segetum	Nem védett / indifferens
Agrotis vestigialis	Nem védett / indifferens
Agrypnia pagetana	Nem védett / indifferens
Agrypnia varia	Nem védett / indifferens
Agrypnus murinus	Nem védett / indifferens
Aguriahana stellulata	Nem védett / indifferens
Agyrtes bicolor	Nem védett / indifferens
Agyrtes castaneus	Nem védett / indifferens
Ahasverus advena	Nem védett / indifferens
Aiolopus strepens	Védett
Aiolopus thalassinus	Nem védett / indifferens
Airaphilus elongatus	Nem védett / indifferens
Aizobius sedi	Nem védett / indifferens
Akimerus schaefferi	Védett
Alabonia staintoniella	Nem védett / indifferens
Alaobia scapularis	Nem védett / indifferens
Alastor biegelebeni	Nem védett / indifferens
Alastorynerus ludendorffi	Nem védett / indifferens
Alastorynerus microdynerus	Nem védett / indifferens
Alauda arvensis	Védett
Alauda arvensis cantarella	Védett
Alauda arvensis dulcivox	Védett
Albocosta musiva	Védett
Alboglossiphonia heteroclita	Nem védett / indifferens
Alboglossiphonia hyalina	Nem védett / indifferens
Alburnoides bipunctatus	Védett
Alburnus alburnus	Nem védett / indifferens
Alca torda	Védett
Alcedo atthis	Védett
Alcedo atthis ispida	Védett
Alces alces	Nem védett / indifferens
Alcis bastelbergeri	Nem védett / indifferens
Alcis jubata	Nem védett / indifferens
Alcis repandata	Nem védett / indifferens
Alebra albostriella	Nem védett / indifferens
Alebra neglecta	Nem védett / indifferens
Alebra wahlbergi	Nem védett / indifferens
Aleimma loeflingiana	Nem védett / indifferens
Aleochara bellonata	Nem védett / indifferens
Aleochara bilineata	Nem védett / indifferens
Aleochara binotata	Nem védett / indifferens
Aleochara bipustulata	Nem védett / indifferens
Aleochara breiti	Nem védett / indifferens
Aleochara brevipennis	Nem védett / indifferens
Aleochara clavicornis	Nem védett / indifferens
Aleochara crassa	Nem védett / indifferens
Aleochara cuniculorum	Nem védett / indifferens
Aleochara curtula	Nem védett / indifferens
Aleochara egregia	Nem védett / indifferens
Aleochara erythroptera	Nem védett / indifferens
Aleochara fumata	Nem védett / indifferens
Aleochara funebris	Nem védett / indifferens
Aleochara haematoptera	Nem védett / indifferens
Aleochara haemoptera	Nem védett / indifferens
Aleochara inconspicua	Nem védett / indifferens
Aleochara intricata	Nem védett / indifferens
Aleochara laevigata	Nem védett / indifferens
Aleochara lanuginosa	Nem védett / indifferens
Aleochara lata	Nem védett / indifferens
Aleochara laticornis	Nem védett / indifferens
Aleochara lygaea	Nem védett / indifferens
Aleochara milleri	Nem védett / indifferens
Aleochara moerens	Nem védett / indifferens
Aleochara moesta	Nem védett / indifferens
Aleochara peusi	Nem védett / indifferens
Aleochara puberula	Nem védett / indifferens
Aleochara ruficornis	Nem védett / indifferens
Aleochara sanguinea	Nem védett / indifferens
Aleochara sparsa	Nem védett / indifferens
Aleochara spissicornis	Nem védett / indifferens
Aleochara tristis	Nem védett / indifferens
Aleochara vagepunctata	Nem védett / indifferens
Aleochara verna	Nem védett / indifferens
Aleochara villosa	Nem védett / indifferens
Aleuropteryx juniperi	Nem védett / indifferens
Aleuropteryx loewii	Nem védett / indifferens
Aleuropteryx umbrata	Nem védett / indifferens
Alevonota aurantiaca	Nem védett / indifferens
Alevonota egregia	Nem védett / indifferens
Alevonota gracilenta	Nem védett / indifferens
Alevonota rufotestacea	Nem védett / indifferens
Algedonia luctualis	Védett
Alianta incana	Nem védett / indifferens
Allajulus dicentrus	Nem védett / indifferens
Allajulus groedensis	Nem védett / indifferens
Allandrus undulatus	Nem védett / indifferens
Allecula atterima	Nem védett / indifferens
Allecula morio	Nem védett / indifferens
Planococcus vovae	Nem védett / indifferens
Allodynerus delphinalis	Nem védett / indifferens
Allodynerus floricola	Nem védett / indifferens
Allodynerus rossii	Nem védett / indifferens
Alloeonotus fulvipes	Nem védett / indifferens
Alloeorhynchus flavipes	Nem védett / indifferens
Alloeotomus germanicus	Nem védett / indifferens
Alloeotomus gothicus	Nem védett / indifferens
Allomalia quadrivirgata	Nem védett / indifferens
Allomengea vidua	Nem védett / indifferens
Allonyx quadrimaculatus	Nem védett / indifferens
Allophyes oxyacanthae	Nem védett / indifferens
Allotrichia pallicornis	Nem védett / indifferens
Alnetoidia alneti	Nem védett / indifferens
Alocentron curvirostre	Nem védett / indifferens
Alocoderus hydrochaeris	Nem védett / indifferens
Aloconota cambrica	Nem védett / indifferens
Aloconota currax	Nem védett / indifferens
Aloconota gregaria	Nem védett / indifferens
Aloconota insecta	Nem védett / indifferens
Aloconota languida	Nem védett / indifferens
Aloconota longicollis	Nem védett / indifferens
Aloconota subgrandis	Nem védett / indifferens
Aloconota sulcifrons	Nem védett / indifferens
Alona affinis	Nem védett / indifferens
Alona costata	Nem védett / indifferens
Alona elegans	Nem védett / indifferens
Alona guttata	Nem védett / indifferens
Alona intermedia	Nem védett / indifferens
Alona karelica	Nem védett / indifferens
Alona protzi	Nem védett / indifferens
Alona quadrangularis	Nem védett / indifferens
Alona rectangula	Nem védett / indifferens
Alona rustica	Nem védett / indifferens
Alonella excisa	Nem védett / indifferens
Alonella exigua	Nem védett / indifferens
Alonella nana	Nem védett / indifferens
Alopecosa accentuata	Nem védett / indifferens
Alopecosa aculeata	Nem védett / indifferens
Alopecosa cuneata	Nem védett / indifferens
Alopecosa cursor	Nem védett / indifferens
Alopecosa fabrilis	Nem védett / indifferens
Alopecosa inquilina	Nem védett / indifferens
Alopecosa mariae orientalis	Nem védett / indifferens
Alopecosa psammophila	Nem védett / indifferens
Alopecosa pulverulenta	Nem védett / indifferens
Alopecosa reimoseri	Nem védett / indifferens
Alopecosa schmidti	Nem védett / indifferens
Alopecosa solitaria	Nem védett / indifferens
Alopecosa sulzeri	Nem védett / indifferens
Alopecosa trabalis	Nem védett / indifferens
Alopia livida	Nem védett / indifferens
Alopochen aegyptiacus	Nem védett / indifferens
Alosa kessleri	Nem védett / indifferens
Alosa pontica	Védett
Alosimus syriacus	Nem védett / indifferens
Amara infima	Nem védett / indifferens
Alosimus syriacus austriacus	Nem védett / indifferens
Alosterna tabacicolor	Nem védett / indifferens
Alphitobius diaperinus	Nem védett / indifferens
Alphitophagus bifasciatus	Nem védett / indifferens
Alsophila aceraria	Nem védett / indifferens
Alsophila aescularia	Nem védett / indifferens
Altella hungarica	Nem védett / indifferens
Altella orientalis	Nem védett / indifferens
Altenia scriptella	Nem védett / indifferens
Alucita cymatodactyla	Nem védett / indifferens
Alucita desmodactyla	Nem védett / indifferens
Alucita grammodactyla	Nem védett / indifferens
Alucita hexadactyla	Nem védett / indifferens
Alucita huebneri	Nem védett / indifferens
Alydus calcaratus	Nem védett / indifferens
Allygidius abbreviatus	Nem védett / indifferens
Allygidius atomarius	Nem védett / indifferens
Allygidius commutatus	Nem védett / indifferens
Allygidius furcatus	Nem védett / indifferens
Allygidius mayri	Nem védett / indifferens
Allygus maculatus	Nem védett / indifferens
Allygus mixtus	Nem védett / indifferens
Allygus modestus	Nem védett / indifferens
Alysson pertheesi	Nem védett / indifferens
Alysson ratzeburgi	Nem védett / indifferens
Alysson spinosus	Nem védett / indifferens
Alysson tricolor	Nem védett / indifferens
Amalorrhynchus melanarius	Nem védett / indifferens
Amalus scortillum	Nem védett / indifferens
Amara aenea	Nem védett / indifferens
Amara anthobia	Nem védett / indifferens
Amara apricaria	Nem védett / indifferens
Amara aulica	Nem védett / indifferens
Amara bifrons	Nem védett / indifferens
Amara brunnea	Nem védett / indifferens
Amara chaudoiri	Nem védett / indifferens
Amara chaudoiri incognita	Nem védett / indifferens
Amara communis	Nem védett / indifferens
Amara concinna	Nem védett / indifferens
Amara consularis	Nem védett / indifferens
Amara convexior	Nem védett / indifferens
Amara convexiuscula	Nem védett / indifferens
Amara crenata	Nem védett / indifferens
Amara cursitans	Nem védett / indifferens
Amara curta	Nem védett / indifferens
Amara equestris	Nem védett / indifferens
Amara eurynota	Nem védett / indifferens
Amara famelica	Nem védett / indifferens
Amara familiaris	Nem védett / indifferens
Amara fulva	Nem védett / indifferens
Amara fulvipes	Nem védett / indifferens
Amara gebleri	Nem védett / indifferens
Amara ingenua	Nem védett / indifferens
Amara littorea	Nem védett / indifferens
Amara lucida	Nem védett / indifferens
Amara lunicollis	Nem védett / indifferens
Amara majuscula	Nem védett / indifferens
Amara montivaga	Nem védett / indifferens
Amara municipalis	Nem védett / indifferens
Amara nitida	Nem védett / indifferens
Amara ovata	Nem védett / indifferens
Amara plebeja	Nem védett / indifferens
Amara praetermissa	Nem védett / indifferens
Amara proxima	Nem védett / indifferens
Amara pseudostrenua	Nem védett / indifferens
Amara sabulosa	Nem védett / indifferens
Amara saginata	Nem védett / indifferens
Amara saphyrea	Nem védett / indifferens
Amara similata	Nem védett / indifferens
Amara sollicita	Nem védett / indifferens
Amara tibialis	Nem védett / indifferens
Amara tricuspidata	Nem védett / indifferens
Amarochara bonnairei	Nem védett / indifferens
Amarochara forticornis	Nem védett / indifferens
Amarochara umbrosa	Nem védett / indifferens
Amata phegea	Nem védett / indifferens
Amaurobius erberi	Nem védett / indifferens
Amaurobius fenestralis	Nem védett / indifferens
Amaurobius ferox	Nem védett / indifferens
Amaurobius jugorum	Nem védett / indifferens
Amaurobius pallidus	Nem védett / indifferens
Amauronyx maerkelii	Nem védett / indifferens
Amaurophanes stigmosalis	Nem védett / indifferens
Amblyptilia acanthadactyla	Nem védett / indifferens
Amblyptilia punctidactyla	Nem védett / indifferens
Amblystomus metallescens	Nem védett / indifferens
Amblystomus niger	Nem védett / indifferens
Amblytylus albidus	Nem védett / indifferens
Amblytylus brevicollis	Nem védett / indifferens
Amblytylus concolor	Nem védett / indifferens
Amblytylus glaucicollis	Nem védett / indifferens
Amblytylus longiceps	Nem védett / indifferens
Amblytylus nasutus	Nem védett / indifferens
Amegilla albigena	Nem védett / indifferens
Amegilla garrula	Nem védett / indifferens
Amegilla magnilabris	Nem védett / indifferens
Amegilla quadrifasciata	Nem védett / indifferens
Amegilla salviae	Nem védett / indifferens
Ameiurus melas	Inváziós
Ameiurus nebulosus	Inváziós
Ameiurus nebulosus pannonicus	Inváziós
Ametropus fragilis	Védett
Amidobia talpa	Nem védett / indifferens
Amidorus thermicola	Nem védett / indifferens
Amischa analis	Nem védett / indifferens
Amischa bifoveolata	Nem védett / indifferens
Amischa decipiens	Nem védett / indifferens
Amischa filum	Nem védett / indifferens
Amischa forcipata	Nem védett / indifferens
Amischa nigrofusca	Nem védett / indifferens
Ammobates melectoides	Nem védett / indifferens
Ammobates punctatus	Nem védett / indifferens
Ammobates similis	Nem védett / indifferens
Ammobates vinctus	Nem védett / indifferens
Ammobatoides abdominalis	Nem védett / indifferens
Ammoconia caecimacula	Nem védett / indifferens
Ammoecius brevis	Nem védett / indifferens
Ammophila campestris	Nem védett / indifferens
Ammophila heydeni	Nem védett / indifferens
Ammophila hungarica	Nem védett / indifferens
Ammophila sabulosa	Nem védett / indifferens
Ammophila terminata	Nem védett / indifferens
Ammophila terminata mocsaryi	Nem védett / indifferens
Ammoplanus handlirschi	Nem védett / indifferens
Ammoplanus hofferi	Nem védett / indifferens
Ammoplanus wesmaeli	Nem védett / indifferens
Ampedus balteatus	Nem védett / indifferens
Ampedus cardinalis	Nem védett / indifferens
Ampedus cinnaberinus	Nem védett / indifferens
Ampedus elegantulus	Nem védett / indifferens
Ampedus erythrogonus	Nem védett / indifferens
Ampedus forticornis	Nem védett / indifferens
Ampedus glycereus	Nem védett / indifferens
Ampedus hjorti	Nem védett / indifferens
Ampedus nigerrimus	Nem védett / indifferens
Ampedus nigroflavus	Nem védett / indifferens
Ampedus pomonae	Nem védett / indifferens
Ampedus pomorum	Nem védett / indifferens
Ampedus praeustus	Nem védett / indifferens
Ampedus quadrisignatus	Védett
Ampedus rufipennis	Nem védett / indifferens
Ampedus sanguineus	Nem védett / indifferens
Ampedus sanguinolentus	Nem védett / indifferens
Ampedus sinuatus	Nem védett / indifferens
Amphiareus obscuriceps	Nem védett / indifferens
Amphichroum canaliculatum	Nem védett / indifferens
Amphicyllis globiformis	Nem védett / indifferens
Amphicyllis globus	Nem védett / indifferens
Amphimallon assimile	Nem védett / indifferens
Amphimallon burmeisteri	Nem védett / indifferens
Amphimallon solstitiale	Nem védett / indifferens
Amphimelania holandrii	Védett
Amphinemura borealis	Nem védett / indifferens
Amphinemura standfussi	Nem védett / indifferens
Amphinemura sulcicollis	Nem védett / indifferens
Amphipoea fucosa	Nem védett / indifferens
Amphipoea lucens	Védett
Amphipoea oculea	Nem védett / indifferens
Amphipyra berbera	Nem védett / indifferens
Amphipyra berbera svenssoni	Nem védett / indifferens
Amphipyra livida	Nem védett / indifferens
Amphipyra perflua	Nem védett / indifferens
Amphipyra pyramidea	Nem védett / indifferens
Amphipyra tetra	Nem védett / indifferens
Amphipyra tragopoginis	Nem védett / indifferens
Amphisbatis incongruella	Nem védett / indifferens
Amphotis marginata	Nem védett / indifferens
Ampulex fasciata	Nem védett / indifferens
Anabolia brevipennis	Nem védett / indifferens
Anabolia furcata	Nem védett / indifferens
Anacaena globulus	Nem védett / indifferens
Anacaena limbata	Nem védett / indifferens
Anacaena lutescens	Nem védett / indifferens
Anacampsis blattariella	Nem védett / indifferens
Anacampsis obscurella	Nem védett / indifferens
Anacampsis populella	Nem védett / indifferens
Anacampsis scintillella	Nem védett / indifferens
Anacampsis timidella	Nem védett / indifferens
Anaceratagallia laevis	Nem védett / indifferens
Anaceratagallia ribauti	Nem védett / indifferens
Anaceratagallia venosa	Nem védett / indifferens
Aeshna isoceles	Védett
Saperda carcharias	Nem védett / indifferens
Saperda similis	Védett
Anaesthetis testacea	Nem védett / indifferens
Anaglyptus mysticus	Nem védett / indifferens
Anakelisia fasciata	Nem védett / indifferens
Anakelisia perspicillata	Nem védett / indifferens
Anania funebris	Nem védett / indifferens
Anania verbascalis	Nem védett / indifferens
Anaplectoides prasina	Nem védett / indifferens
Anapus longicornis	Nem védett / indifferens
Anarsia lineatella	Nem védett / indifferens
Anarsia spartiella	Nem védett / indifferens
Anarta myrtilli	Védett
Anas acuta	Védett
Anas carolinensis	Védett
Anas clypeata	Védett
Anas crecca	Védett
Anas penelope	Védett
Anas platyrhynchos	Nem védett / indifferens
Anas querquedula	Fokozottan védett
Anas strepera	Védett
Anasimyia contracta	Nem védett / indifferens
Anasimyia interpuncta	Nem védett / indifferens
Anasimyia lineata	Nem védett / indifferens
Anasimyia transfuga	Nem védett / indifferens
Anasphaltis renigerellus	Nem védett / indifferens
Anaspis brunnipes	Nem védett / indifferens
Anaspis costai	Nem védett / indifferens
Anaspis excellens	Nem védett / indifferens
Anaspis flava	Nem védett / indifferens
Anaspis frontalis	Nem védett / indifferens
Anaspis kiesenwetteri	Nem védett / indifferens
Anaspis lurida	Nem védett / indifferens
Anaspis melanostoma	Nem védett / indifferens
Anaspis nigripes	Nem védett / indifferens
Anaspis palpalis	Nem védett / indifferens
Anaspis pulicaria	Nem védett / indifferens
Anaspis ruficollis	Nem védett / indifferens
Anaspis rufilabris	Nem védett / indifferens
Anaspis subtilis	Nem védett / indifferens
Anaspis thoracica	Nem védett / indifferens
Anaspis varians	Nem védett / indifferens
Anaspis viennensis	Nem védett / indifferens
Anastrangalia dubia	Nem védett / indifferens
Anastrangalia reyi	Nem védett / indifferens
Anastrangalia sanguinolenta	Nem védett / indifferens
Anatis ocellata	Nem védett / indifferens
Anaulacaspis laevigata	Nem védett / indifferens
Anaulacaspis nigra	Nem védett / indifferens
Anax imperator	Nem védett / indifferens
Anax parthenope	Nem védett / indifferens
Anchinia cristalis	Nem védett / indifferens
Anchinia daphnella	Nem védett / indifferens
Anchinia laureolella	Nem védett / indifferens
Anchistropus emarginatus	Nem védett / indifferens
Anchomenus dorsalis	Nem védett / indifferens
Ancistrocerus acutus	Nem védett / indifferens
Ancistrocerus antilope	Nem védett / indifferens
Ancistrocerus claripennis	Nem védett / indifferens
Ancistrocerus dusmetiolus	Nem védett / indifferens
Ancistrocerus gazella	Nem védett / indifferens
Ancistrocerus ichneumonideus	Nem védett / indifferens
Ancistrocerus nigricornis	Nem védett / indifferens
Ancistrocerus oviventris	Nem védett / indifferens
Ancistrocerus parietinus	Nem védett / indifferens
Ancistrocerus parietum	Nem védett / indifferens
Ancistrocerus scoticus	Nem védett / indifferens
Ancistrocerus trifasciatus	Nem védett / indifferens
Ancistronycha abdominalis	Nem védett / indifferens
Ancistronycha erichsonii	Nem védett / indifferens
Ancistronycha violacea	Nem védett / indifferens
Ancylis achatana	Nem védett / indifferens
Ancylis apicella	Nem védett / indifferens
Ancylis badiana	Nem védett / indifferens
Ancylis comptana	Nem védett / indifferens
Ancylis diminutana	Nem védett / indifferens
Ancylis geminana	Nem védett / indifferens
Ancylis laetana	Nem védett / indifferens
Ancylis mitterbacheriana	Nem védett / indifferens
Ancylis myrtillana	Nem védett / indifferens
Ancylis obtusana	Nem védett / indifferens
Ancylis paludana	Nem védett / indifferens
Ancylis selenana	Nem védett / indifferens
Ancylis subarcuana	Nem védett / indifferens
Ancylis tineana	Nem védett / indifferens
Ancylis uncella	Nem védett / indifferens
Ancylis unculana	Nem védett / indifferens
Ancylis unguicella	Nem védett / indifferens
Ancylis upupana	Nem védett / indifferens
Ancylolomia disparalis	Nem védett / indifferens
Ancylolomia palpella	Nem védett / indifferens
Ancylolomia pectinatellus	Nem védett / indifferens
Ancylosis albidella	Nem védett / indifferens
Ancylosis cinnamomella	Nem védett / indifferens
Ancylosis oblitella	Nem védett / indifferens
Ancylosis roscidella	Nem védett / indifferens
Ancylosis sareptalla	Nem védett / indifferens
Ancylus fluviatilis	Nem védett / indifferens
Ancyrona japonica	Nem védett / indifferens
Ancyrosoma leucogrammes	Nem védett / indifferens
Andrena aberrans	Nem védett / indifferens
Andrena aciculata	Nem védett / indifferens
Andrena aeneiventris	Nem védett / indifferens
Andrena agilissima	Nem védett / indifferens
Andrena albopunctata	Nem védett / indifferens
Andrena alfkenella	Nem védett / indifferens
Andrena apicata	Nem védett / indifferens
Andrena argentata	Nem védett / indifferens
Andrena assimilis	Nem védett / indifferens
Andrena assimilis gallica	Nem védett / indifferens
Andrena atrata	Nem védett / indifferens
Andrena atrotegularis	Nem védett / indifferens
Andrena barbilabris	Nem védett / indifferens
Andrena bicolor	Nem védett / indifferens
Andrena bimaculata	Nem védett / indifferens
Andrena bisulcata	Nem védett / indifferens
Andrena bluethgeni	Nem védett / indifferens
Andrena braunsiana	Nem védett / indifferens
Andrena bucephala	Nem védett / indifferens
Andrena chrysopus	Nem védett / indifferens
Andrena chrysopyga	Nem védett / indifferens
Andrena chrysosceles	Nem védett / indifferens
Andrena cineraria	Nem védett / indifferens
Andrena clarkella	Nem védett / indifferens
Andrena clypella	Nem védett / indifferens
Andrena clypella hasitata	Nem védett / indifferens
Andrena coitana	Nem védett / indifferens
Andrena combaella	Nem védett / indifferens
Andrena combinata	Nem védett / indifferens
Andrena combinata mehelyi	Nem védett / indifferens
Andrena congruens	Nem védett / indifferens
Andrena cordialis	Nem védett / indifferens
Andrena curvana	Nem védett / indifferens
Andrena curvungula	Nem védett / indifferens
Andrena decipiens	Nem védett / indifferens
Andrena denticulata	Nem védett / indifferens
Andrena distinguenda	Nem védett / indifferens
Andrena dorsalis	Nem védett / indifferens
Andrena dorsata	Nem védett / indifferens
Andrena dorsata propinqua	Nem védett / indifferens
Andrena enslinella	Nem védett / indifferens
Andrena erythrocnemis	Nem védett / indifferens
Andrena falsifica	Nem védett / indifferens
Andrena ferox	Nem védett / indifferens
Andrena figurata	Nem védett / indifferens
Andrena flavipes	Nem védett / indifferens
Andrena florea	Nem védett / indifferens
Andrena floricola	Nem védett / indifferens
Andrena florivaga	Nem védett / indifferens
Andrena fucata	Nem védett / indifferens
Andrena fulva	Nem védett / indifferens
Andrena fulvago	Nem védett / indifferens
Andrena fulvata	Nem védett / indifferens
Andrena fulvicornis	Nem védett / indifferens
Andrena fulvida	Nem védett / indifferens
Andrena fuscipes	Nem védett / indifferens
Andrena fuscosa	Nem védett / indifferens
Andrena gelriae	Nem védett / indifferens
Andrena graecella	Nem védett / indifferens
Andrena granulosa	Nem védett / indifferens
Andrena gravida	Nem védett / indifferens
Andrena haemorrhoa	Nem védett / indifferens
Andrena hattorfiana	Nem védett / indifferens
Andrena hedikae	Nem védett / indifferens
Andrena helvola	Nem védett / indifferens
Andrena hesperia	Nem védett / indifferens
Andrena humilis	Nem védett / indifferens
Andrena hungarica	Nem védett / indifferens
Andrena hypopolia	Nem védett / indifferens
Andrena impunctata	Nem védett / indifferens
Andrena incisa	Nem védett / indifferens
Andrena intermedia	Nem védett / indifferens
Andrena ispida	Nem védett / indifferens
Andrena labialis	Nem védett / indifferens
Andrena labiata	Nem védett / indifferens
Andrena lagopus	Nem védett / indifferens
Andrena lathyri	Nem védett / indifferens
Andrena lepida	Nem védett / indifferens
Andrena limbata	Nem védett / indifferens
Andrena majalis	Nem védett / indifferens
Andrena marginata	Nem védett / indifferens
Andrena minutula	Nem védett / indifferens
Andrena minutuloides	Nem védett / indifferens
Andrena mitis	Nem védett / indifferens
Andrena mocsaryi	Nem védett / indifferens
Andrena morio	Nem védett / indifferens
Andrena mucida	Nem védett / indifferens
Andrena nana	Nem védett / indifferens
Andrena nanaeformis	Nem védett / indifferens
Andrena nanula	Nem védett / indifferens
Andrena nasuta	Nem védett / indifferens
Andrena nigroaenea	Nem védett / indifferens
Andrena nigrospina	Nem védett / indifferens
Andrena nitida	Nem védett / indifferens
Andrena nitida limata	Nem védett / indifferens
Andrena nitidiuscula	Nem védett / indifferens
Andrena niveata	Nem védett / indifferens
Andrena nobilis	Nem védett / indifferens
Andrena nuptialis	Nem védett / indifferens
Andrena nychtemera	Nem védett / indifferens
Andrena oralis	Nem védett / indifferens
Andrena orenburgensis	Nem védett / indifferens
Andrena ovatula	Nem védett / indifferens
Andrena pallitarsis	Nem védett / indifferens
Andrena pandellei	Nem védett / indifferens
Andrena parviceps	Nem védett / indifferens
Andrena paucisquama	Nem védett / indifferens
Andrena pellucens	Nem védett / indifferens
Andrena pillichi	Nem védett / indifferens
Andrena polita	Nem védett / indifferens
Andrena pontica	Nem védett / indifferens
Andrena potentillae	Nem védett / indifferens
Andrena praecox	Nem védett / indifferens
Andrena proxima	Nem védett / indifferens
Andrena pusilla	Nem védett / indifferens
Andrena rosae	Nem védett / indifferens
Andrena roseipes	Nem védett / indifferens
Andrena ruficrus	Nem védett / indifferens
Andrena rufula	Nem védett / indifferens
Andrena rugulosa	Nem védett / indifferens
Andrena sabulosa	Nem védett / indifferens
Andrena sabulosa trimmerana	Nem védett / indifferens
Andrena saxonica	Nem védett / indifferens
Andrena schencki	Nem védett / indifferens
Andrena schlettereri	Nem védett / indifferens
Andrena scita	Nem védett / indifferens
Andrena semilaevis	Nem védett / indifferens
Andrena seminuda	Nem védett / indifferens
Andrena sericata	Nem védett / indifferens
Andrena similis	Nem védett / indifferens
Andrena simontornyella	Nem védett / indifferens
Andrena strohmella	Nem védett / indifferens
Andrena subopaca	Nem védett / indifferens
Andrena suerinensis	Nem védett / indifferens
Andrena susterai	Nem védett / indifferens
Andrena symphyti	Nem védett / indifferens
Andrena synadelpha	Nem védett / indifferens
Andrena taraxaci	Nem védett / indifferens
Andrena tarsata	Nem védett / indifferens
Andrena thoracica	Nem védett / indifferens
Andrena tibialis	Nem védett / indifferens
Andrena tibialis vindobonensis	Nem védett / indifferens
Andrena transitoria	Nem védett / indifferens
Andrena tridentata	Nem védett / indifferens
Andrena truncatilabris	Nem védett / indifferens
Andrena tscheki	Nem védett / indifferens
Andrena ungeri	Nem védett / indifferens
Andrena vaga	Nem védett / indifferens
Andrena variabilis	Nem védett / indifferens
Andrena varians	Nem védett / indifferens
Andrena ventralis	Nem védett / indifferens
Andrena ventricosa	Nem védett / indifferens
Andrena viridescens	Nem védett / indifferens
Andrena wilkella	Nem védett / indifferens
Selimus pulchellus	Nem védett / indifferens
Selimus vittatus	Nem védett / indifferens
Anemadus strigosus	Nem védett / indifferens
Anerastia dubia	Nem védett / indifferens
Anerastia lotella	Nem védett / indifferens
Anergates atratulus	Nem védett / indifferens
Aneurus avenius	Nem védett / indifferens
Aneurus laevis	Nem védett / indifferens
Angerona prunaria	Nem védett / indifferens
Anguilla anguilla	Nem védett / indifferens
Anguis fragilis	Védett
Anguliphantes angulipalpis	Nem védett / indifferens
Anidorus nigrinus	Nem védett / indifferens
Anisarthron barbipes	Nem védett / indifferens
Anisodactylus binotatus	Nem védett / indifferens
Anisodactylus nemorivagus	Nem védett / indifferens
Anisodactylus poeciloides	Nem védett / indifferens
Anisodactylus signatus	Nem védett / indifferens
Anisoplia agricola	Nem védett / indifferens
Anisoplia austriaca	Nem védett / indifferens
Anisoplia bromicola	Nem védett / indifferens
Anisoplia erichsoni	Nem védett / indifferens
Anisoplia tempestiva	Nem védett / indifferens
Anisoplia lata	Nem védett / indifferens
Anisorus quercus	Nem védett / indifferens
Anisosticta novemdecimpunctata	Nem védett / indifferens
Anisotoma axillaris	Nem védett / indifferens
Anisotoma castanea	Nem védett / indifferens
Anisotoma glabra	Nem védett / indifferens
Anisotoma humeralis	Nem védett / indifferens
Anisotoma orbicularis	Nem védett / indifferens
Anisoxya fuscula	Nem védett / indifferens
Anisus calculiformis	Nem védett / indifferens
Anisus leucostoma	Nem védett / indifferens
Anisus spirorbis	Nem védett / indifferens
Anisus vortex	Nem védett / indifferens
Anisus vorticulus	Védett
Annitella obscurata	Nem védett / indifferens
Anobium punctatum	Nem védett / indifferens
Anodonta anatina	Nem védett / indifferens
Anodonta cygnea	Nem védett / indifferens
Anogcodes seladonius	Nem védett / indifferens
Anogcodes seladonius azureus	Nem védett / indifferens
Anogcodes ferrugineus	Nem védett / indifferens
Anogcodes fulvicollis	Nem védett / indifferens
Anogcodes ruficollis	Nem védett / indifferens
Anogcodes rufiventris	Nem védett / indifferens
Anogcodes ustulatus	Nem védett / indifferens
Anomala dubia	Nem védett / indifferens
Anomala solida	Nem védett / indifferens
Anomala vitis	Nem védett / indifferens
Anommatus duodecimstriatus	Nem védett / indifferens
Anommatus hungaricus	Nem védett / indifferens
Anommatus jelinecki	Nem védett / indifferens
Anommatus pannonicus	Nem védett / indifferens
Anommatus reitteri	Nem védett / indifferens
Anommatus stilleri	Nem védett / indifferens
Anomognathus cuspidatus	Nem védett / indifferens
Anopheles algeriensis	Nem védett / indifferens
Anopheles atroparvus	Nem védett / indifferens
Anopheles claviger	Nem védett / indifferens
Anopheles hyrcanus	Nem védett / indifferens
Anopheles maculipennis	Nem védett / indifferens
Anopheles messeae	Nem védett / indifferens
Anopheles plumbeus	Nem védett / indifferens
Anophococcus inermis	Nem védett / indifferens
Anopleta corvina	Nem védett / indifferens
Anopleta kochi	Nem védett / indifferens
Anoplius alpinobalticus	Nem védett / indifferens
Anoplius caviventris	Nem védett / indifferens
Anoplius concinnus	Nem védett / indifferens
Anoplius infuscatus	Nem védett / indifferens
Anoplius nigerrimus	Nem védett / indifferens
Anoplius piliventris	Nem védett / indifferens
Anoplius samariensis	Nem védett / indifferens
Anoplius tenuicornis	Nem védett / indifferens
Anoplius viaticus	Nem védett / indifferens
Anoplius viaticus paganus	Nem védett / indifferens
Anoplodera rufipes	Nem védett / indifferens
Anoplodera sexguttata	Nem védett / indifferens
Anoplotettix fuscovenosus	Nem védett / indifferens
Anoplotettix horvathi	Nem védett / indifferens
Anoplotrupes stercorosus	Nem védett / indifferens
Anoplus plantaris	Nem védett / indifferens
Anoplus roboris	Nem védett / indifferens
Anoplus setulosus	Nem védett / indifferens
Perigrapha munda	Nem védett / indifferens
Anoscopus albifrons	Nem védett / indifferens
Anoscopus albiger	Nem védett / indifferens
Anoscopus flavostriatus	Nem védett / indifferens
Anoscopus histrionicus	Nem védett / indifferens
Anoscopus serratulae	Nem védett / indifferens
Anospilus orbitalis	Nem védett / indifferens
Anostirus castaneus	Nem védett / indifferens
Anostirus purpureus	Nem védett / indifferens
Anoterostemma ivanoffi	Nem védett / indifferens
Anotylus bernhaueri	Nem védett / indifferens
Anotylus clypeonitens	Nem védett / indifferens
Anotylus complanatus	Nem védett / indifferens
Anotylus fairmairei	Nem védett / indifferens
Anotylus hamatus	Nem védett / indifferens
Anotylus hybridus	Nem védett / indifferens
Anotylus insecatus	Nem védett / indifferens
Anotylus intricatus	Nem védett / indifferens
Anotylus inustus	Nem védett / indifferens
Anotylus mendus	Nem védett / indifferens
Anotylus nitidulus	Nem védett / indifferens
Anotylus politus	Nem védett / indifferens
Anotylus pumilus	Nem védett / indifferens
Anotylus rugifrons	Nem védett / indifferens
Anotylus rugosus	Nem védett / indifferens
Anotylus saulcyi	Nem védett / indifferens
Anotylus sculpturatus	Nem védett / indifferens
Anotylus speculifrons	Nem védett / indifferens
Anotylus tetracarinatus	Nem védett / indifferens
Anotylus tetratoma	Nem védett / indifferens
Anoxia orientalis	Nem védett / indifferens
Anoxia pilosa	Nem védett / indifferens
Anser albifrons	Nem védett / indifferens
Anser anser	Védett
Anser anser rubrirostris	Védett
Anser brachyrhynchus	Védett
Anser caerulescens	Védett
Anthidium loti	Nem védett / indifferens
Anser caerulescens atlanticus	Védett
Anser erythropus	Fokozottan védett
Anser fabalis	Nem védett / indifferens
Anser fabalis johanseni	Nem védett / indifferens
Anser fabalis rossicus	Nem védett / indifferens
Anser indicus	Védett
Antepipona deflenda	Nem védett / indifferens
Antepipona orbitalis	Nem védett / indifferens
Anthaxia candens	Védett
Anthaxia cichorii	Nem védett / indifferens
Anthaxia deaurata	Nem védett / indifferens
Anthaxia fulgurans	Nem védett / indifferens
Anthaxia funerula	Nem védett / indifferens
Anthaxia godeti	Nem védett / indifferens
Anthaxia hackeri	Védett
Anthaxia helvetica	Nem védett / indifferens
Anthaxia hungarica	Védett
Anthaxia istriana	Nem védett / indifferens
Anthaxia manca	Nem védett / indifferens
Anthaxia nitidula	Nem védett / indifferens
Anthaxia nitidula signaticollis	Nem védett / indifferens
Anthaxia olympica	Nem védett / indifferens
Anthaxia plicata	Védett
Anthaxia podolica	Nem védett / indifferens
Anthaxia quadripunctata	Nem védett / indifferens
Anthaxia salicis	Nem védett / indifferens
Anthaxia semicuprea	Nem védett / indifferens
Anthaxia similis	Nem védett / indifferens
Anthaxia suzannae	Nem védett / indifferens
Anthaxia tuerki	Védett
Anthelephila pedestris	Nem védett / indifferens
Antheminia lunulata	Nem védett / indifferens
Antheraea yamamai	Nem védett / indifferens
Antherophagus canescens	Nem védett / indifferens
Antherophagus nigricornis	Nem védett / indifferens
Antherophagus pallens	Nem védett / indifferens
Anthicus antherinus	Nem védett / indifferens
Anthicus ater	Nem védett / indifferens
Anthicus axillaris	Nem védett / indifferens
Anthicus bimaculatus	Nem védett / indifferens
Anthicus fenestratus	Nem védett / indifferens
Anthicus flavipes	Nem védett / indifferens
Anthicus schmidtii	Nem védett / indifferens
Anthidium barbatum	Nem védett / indifferens
Anthidium cingulatum	Nem védett / indifferens
Anthidium florentinum	Nem védett / indifferens
Anthidium interruptum	Nem védett / indifferens
Anthidium laterale	Nem védett / indifferens
Anthidium lituratum	Nem védett / indifferens
Anthidium manicatum	Nem védett / indifferens
Anthidium oblongatum	Nem védett / indifferens
Anthidium pubescens	Nem védett / indifferens
Anthidium punctatum	Nem védett / indifferens
Anthidium septemdentatum	Nem védett / indifferens
Anthidium septemspinosum	Nem védett / indifferens
Anthidium strigatum	Nem védett / indifferens
Anthidium tenellum	Nem védett / indifferens
Anthobium atrocephalum	Nem védett / indifferens
Anthobium melanocephalum	Nem védett / indifferens
Anthocharis cardamines	Nem védett / indifferens
Anthocharis gruneri	Nem védett / indifferens
Anthocomus equestris	Nem védett / indifferens
Anthocomus fasciatus	Nem védett / indifferens
Anthocomus rufus	Nem védett / indifferens
Anthocoris amplicollis	Nem védett / indifferens
Anthocoris confusus	Nem védett / indifferens
Anthocoris gallarumulmi	Nem védett / indifferens
Anthocoris limbatus	Nem védett / indifferens
Anthocoris minki	Nem védett / indifferens
Anthocoris nemoralis	Nem védett / indifferens
Anthocoris nemorum	Nem védett / indifferens
Anthocoris pilosus	Nem védett / indifferens
Anthocoris simulans	Nem védett / indifferens
Anthonomus bituberculatus	Nem védett / indifferens
Aulacobaris chevrolati	Nem védett / indifferens
Anthonomus conspersus	Nem védett / indifferens
Anthonomus humeralis	Nem védett / indifferens
Anthonomus kirschi	Nem védett / indifferens
Anthonomus pedicularius	Nem védett / indifferens
Anthonomus phyllocola	Nem védett / indifferens
Anthonomus pinivorax	Nem védett / indifferens
Anthonomus piri	Nem védett / indifferens
Anthonomus pomorum	Nem védett / indifferens
Anthonomus rectirostris	Nem védett / indifferens
Anthonomus rubi	Nem védett / indifferens
Anthonomus rubripes	Nem védett / indifferens
Anthonomus rufus	Nem védett / indifferens
Anthonomus sorbi	Nem védett / indifferens
Anthonomus spilotus	Nem védett / indifferens
Anthonomus ulmi	Nem védett / indifferens
Anthonomus undulatus	Nem védett / indifferens
Anthophagus alpestris	Nem védett / indifferens
Anthophagus alpinus	Nem védett / indifferens
Anthophagus angusticollis	Nem védett / indifferens
Anthophagus bicornis	Nem védett / indifferens
Anthophagus caraboides	Nem védett / indifferens
Anthophila fabriciana	Nem védett / indifferens
Anthophora aestivalis	Nem védett / indifferens
Anthophora bimaculata	Nem védett / indifferens
Anthophora borealis	Nem védett / indifferens
Anthophora crinipes	Nem védett / indifferens
Anthophora furcata	Nem védett / indifferens
Anthophora plagiata	Nem védett / indifferens
Anthophora plumipes	Nem védett / indifferens
Anthophora pubescens	Nem védett / indifferens
Anthophora quadrimaculata	Nem védett / indifferens
Anthophora retusa	Nem védett / indifferens
Anthracus consputus	Nem védett / indifferens
Anthracus longicornis	Nem védett / indifferens
Anthracus transversalis	Nem védett / indifferens
Anthrenus fuscus	Nem védett / indifferens
Anthrenus goliath	Nem védett / indifferens
Anthrenus museorum	Nem védett / indifferens
Anthrenus olgae	Nem védett / indifferens
Anthrenus pimpinellae	Nem védett / indifferens
Anthrenus polonicus	Nem védett / indifferens
Anthrenus scrophulariae	Nem védett / indifferens
Anthrenus verbasci	Nem védett / indifferens
Anthribus fasciatus	Nem védett / indifferens
Anthribus nebulosus	Nem védett / indifferens
Anthropoides virgo	Védett
Anthus campestris	Védett
Anthus cervinus	Védett
Anthus pratensis	Védett
Anthus richardi	Védett
Anthus spinoletta	Védett
Anthus trivialis	Védett
Anticlea badiata	Nem védett / indifferens
Anticlea derivata	Nem védett / indifferens
Anticollix sparsata	Nem védett / indifferens
Antigastra catalaunalis	Nem védett / indifferens
Antispila metallella	Nem védett / indifferens
Antispila treitschkiella	Nem védett / indifferens
Antistea elegans	Nem védett / indifferens
Antitype chi	Nem védett / indifferens
Thetidia smaragdaria	Nem védett / indifferens
Antonina purpurea	Nem védett / indifferens
Antoninella inaudita	Nem védett / indifferens
Anyphaena accentuata	Nem védett / indifferens
Paratinus femoralis	Nem védett / indifferens
Paratinus flavolimbatus	Nem védett / indifferens
Apalus bimaculatus	Nem védett / indifferens
Apalus bipunctatus	Nem védett / indifferens
Apalus necydaleus	Nem védett / indifferens
Apamea anceps	Nem védett / indifferens
Apamea aquila	Nem védett / indifferens
Apamea crenata	Nem védett / indifferens
Apamea epomidion	Nem védett / indifferens
Apamea furva	Nem védett / indifferens
Apamea illyria	Nem védett / indifferens
Apamea lateritia	Nem védett / indifferens
Apamea lithoxylaea	Nem védett / indifferens
Apamea monoglypha	Nem védett / indifferens
Apamea oblonga	Nem védett / indifferens
Apamea ophiogramma	Nem védett / indifferens
Apamea platinea	Védett
Apamea remissa	Nem védett / indifferens
Apamea rubrirena	Nem védett / indifferens
Apamea scolopacina	Nem védett / indifferens
Apamea sicula	Nem védett / indifferens
Apamea sicula tallosi	Védett
Apamea sordens	Nem védett / indifferens
Apamea sublustris	Nem védett / indifferens
Apamea unanimis	Nem védett / indifferens
Aparopion costatum	Nem védett / indifferens
Apatania muliebris	Védett
Apatema mediopallidum	Nem védett / indifferens
Apatema whalleyi	Nem védett / indifferens
Apatetris trivittellum	Nem védett / indifferens
Apatura ilia	Védett
Apatura iris	Védett
Apatura metis	Fokozottan védett
Apaustis rupicola	Védett
Apeira syringaria	Nem védett / indifferens
Aphanisticus elongatus	Nem védett / indifferens
Aphanisticus emarginatus	Nem védett / indifferens
Aphanisticus pusillus	Nem védett / indifferens
Aphantaulax cincta	Nem védett / indifferens
Aphantaulax trifasciata	Nem védett / indifferens
Aphantopus hyperantus	Nem védett / indifferens
Aphanus rolandri	Nem védett / indifferens
Aphelia ferugana	Nem védett / indifferens
Aphelia paleana	Nem védett / indifferens
Aphelia viburnana	Nem védett / indifferens
Aphelocheirus aestivalis	Nem védett / indifferens
Aphelonema quadrivittata	Nem védett / indifferens
Aphenogaster subterranea	Nem védett / indifferens
Aphidecta obliterata	Nem védett / indifferens
Aphodius fimetarius	Nem védett / indifferens
Aphodius foetens	Nem védett / indifferens
Aphodius foetidus	Nem védett / indifferens
Aphomia sociella	Nem védett / indifferens
Aphrodes bicinctus	Nem védett / indifferens
Planaphrodes furcillatus	Nem védett / indifferens
Aphrodes makarovi	Nem védett / indifferens
Aphrophora alni	Nem védett / indifferens
Aphrophora corticea	Nem védett / indifferens
Aphrophora salicina	Nem védett / indifferens
Aphytobius sphaerion	Nem védett / indifferens
Apion cruentatum	Nem védett / indifferens
Apion frumentarium	Nem védett / indifferens
Apion haematodes	Nem védett / indifferens
Apion rubens	Nem védett / indifferens
Apion rubiginosum	Nem védett / indifferens
Apis mellifera	Nem védett / indifferens
Aplasta ononaria	Nem védett / indifferens
Aplexa hypnorum	Nem védett / indifferens
Aplocera efformata	Nem védett / indifferens
Aplocera plagiata	Nem védett / indifferens
Aplocera praeformata	Nem védett / indifferens
Aplocnemus basalis	Nem védett / indifferens
Aplocnemus chalconatus	Nem védett / indifferens
Aplocnemus impressus	Nem védett / indifferens
Aplocnemus integer	Nem védett / indifferens
Aplocnemus kiesenwetteri	Nem védett / indifferens
Aplocnemus nigricornis	Nem védett / indifferens
Aplocnemus pulverulentus	Nem védett / indifferens
Aplocnemus serbicus	Nem védett / indifferens
Aplocnemus tarsalis	Nem védett / indifferens
Aplocnemus virens	Nem védett / indifferens
Aploderus caelatus	Nem védett / indifferens
Aploderus caesus	Nem védett / indifferens
Apocheima hispidaria	Nem védett / indifferens
Phigalia pilosaria	Nem védett / indifferens
Apoda limacodes	Nem védett / indifferens
Apodemus agrarius	Nem védett / indifferens
Apodemus flavicollis	Nem védett / indifferens
Apodemus sylvaticus	Nem védett / indifferens
Apodemus uralensis	Nem védett / indifferens
Apoderus coryli	Nem védett / indifferens
Apoderus erythropterus	Nem védett / indifferens
Apodia bifractella	Nem védett / indifferens
Apolygus limbatus	Nem védett / indifferens
Apolygus lucorum	Nem védett / indifferens
Apolygus spinolae	Nem védett / indifferens
Apomyelois bistriatella nephanes	Nem védett / indifferens
Apomyelois ceratoniae	Nem védett / indifferens
Aporia crataegi	Nem védett / indifferens
Aporinellus moestus	Nem védett / indifferens
Aporinellus moestus sericeomaculatus	Nem védett / indifferens
Aporinellus obtusus	Nem védett / indifferens
Aporinellus sexmaculatus	Nem védett / indifferens
Aporodes floralis	Nem védett / indifferens
Aporophyla lutulenta	Nem védett / indifferens
Dermestoides sanguinicollis	Védett
Aporus bicolor	Nem védett / indifferens
Aporus pollux	Nem védett / indifferens
Aporus unicolor	Nem védett / indifferens
Aposericoderus revelierei	Nem védett / indifferens
Apostenus fuscus	Nem védett / indifferens
Apotomis betuletana	Nem védett / indifferens
Apotomis capreana	Nem védett / indifferens
Apotomis inundana	Nem védett / indifferens
Apotomis lineana	Nem védett / indifferens
Apotomis sauciana	Nem védett / indifferens
Apotomis semifasciana	Nem védett / indifferens
Apotomis sororculana	Nem védett / indifferens
Apotomis turbidana	Nem védett / indifferens
Apristus subaeneus	Nem védett / indifferens
Aproaerema anthyllidella	Nem védett / indifferens
Apsis albolineata	Nem védett / indifferens
Apterola lownii	Nem védett / indifferens
Apterona helicoidella	Nem védett / indifferens
Aptinus bombarda	Nem védett / indifferens
Apus apus	Védett
Tachymarptis melba	Védett
Apus pallidus	Védett
Aquarius najas	Védett
Aquarius paludum	Nem védett / indifferens
Aquila chrysaetos	Fokozottan védett
Aquila clanga	Fokozottan védett
Aquila heliaca	Fokozottan védett
Aquila nipalensis	Fokozottan védett
Aquila nipalensis orientalis	Védett
Aquila pomarina	Fokozottan védett
Arachnospila abnormis	Nem védett / indifferens
Arachnospila alvarabnormis	Nem védett / indifferens
Arachnospila anceps	Nem védett / indifferens
Arachnospila ausa	Nem védett / indifferens
Arachnospila conjungens	Nem védett / indifferens
Arachnospila fumipennis	Nem védett / indifferens
Arachnospila fuscomarginata	Nem védett / indifferens
Arachnospila gibbomima	Nem védett / indifferens
Arachnospila minutula	Nem védett / indifferens
Arachnospila opinata	Nem védett / indifferens
Arachnospila rufa	Nem védett / indifferens
Arachnospila sogdiana	Nem védett / indifferens
Arachnospila spissa	Nem védett / indifferens
Arachnospila trivialis	Nem védett / indifferens
Arachnospila wesmaeli	Nem védett / indifferens
Arachnoteutes rufithorax	Nem védett / indifferens
Aradus betulae	Nem védett / indifferens
Aradus betulinus	Nem védett / indifferens
Aradus bimaculatus	Nem védett / indifferens
Aradus brenskei	Nem védett / indifferens
Aradus brevicollis	Nem védett / indifferens
Aradus cinnamomeus	Nem védett / indifferens
Aradus conspicuus	Nem védett / indifferens
Aradus corticalis	Nem védett / indifferens
Aradus depressus	Nem védett / indifferens
Aradus distinctus	Nem védett / indifferens
Aradus krueperi	Nem védett / indifferens
Aradus kuthyi	Nem védett / indifferens
Aradus lugubris	Nem védett / indifferens
Aradus mirus	Nem védett / indifferens
Aradus pallescens	Nem védett / indifferens
Aradus ribauti	Nem védett / indifferens
Aradus serbicus	Nem védett / indifferens
Aradus signaticornis	Nem védett / indifferens
Aradus truncatus	Nem védett / indifferens
Aradus versicolor	Nem védett / indifferens
Aracerus fasciculatus	Nem védett / indifferens
Melanopsacus grenieri	Nem védett / indifferens
Araeoncus anguineus	Nem védett / indifferens
Araeoncus crassipes	Nem védett / indifferens
Araeoncus humilis	Nem védett / indifferens
Araneus alsine	Nem védett / indifferens
Araneus angulatus	Nem védett / indifferens
Araneus circe	Nem védett / indifferens
Araneus diadematus	Nem védett / indifferens
Araneus grossus	Védett
Araneus marmoreus	Nem védett / indifferens
Araneus quadratus	Nem védett / indifferens
Araneus sturmi	Nem védett / indifferens
Araneus triguttatus	Nem védett / indifferens
Araniella alpica	Nem védett / indifferens
Araniella cucurbitina	Nem védett / indifferens
Araniella displicata	Nem védett / indifferens
Araniella inconspicua	Nem védett / indifferens
Araniella opisthographa	Nem védett / indifferens
Araschnia levana	Nem védett / indifferens
Arboridia erecta	Nem védett / indifferens
Arboridia parvula	Nem védett / indifferens
Arboridia pusilla	Nem védett / indifferens
Arboridia ribauti	Nem védett / indifferens
Arboridia spatulata	Nem védett / indifferens
Arboridia velata	Nem védett / indifferens
Archaeodictyna consecuta	Nem védett / indifferens
Archanara algae	Nem védett / indifferens
Archanara dissoluta	Nem védett / indifferens
Archanara geminipuncta	Nem védett / indifferens
Archanara neurica	Nem védett / indifferens
Archanara sparganii	Nem védett / indifferens
Archarius crux	Nem védett / indifferens
Archarius pyrrhoceras	Nem védett / indifferens
Archarius salicivorus	Nem védett / indifferens
Archiboreoiulus pallidus	Nem védett / indifferens
Archiearis notha	Védett
Archiearis parthenias	Védett
Archiearis puella	Védett
Archinemapogon yildizae	Nem védett / indifferens
Archips betulana	Nem védett / indifferens
Archips crataegana	Nem védett / indifferens
Archips oporana	Nem védett / indifferens
Archips podana	Nem védett / indifferens
Archips rosana	Nem védett / indifferens
Archips xylosteana	Nem védett / indifferens
Arctia caja	Nem védett / indifferens
Arctia festiva	Védett
Arctia villica	Nem védett / indifferens
Arctodiaptomus bacillifer	Nem védett / indifferens
Arctodiaptomus salinus	Nem védett / indifferens
Arctodiaptomus spinosus	Nem védett / indifferens
Arctodiaptomus wierzejskii	Nem védett / indifferens
Arctophila bombiformis	Nem védett / indifferens
Arctophila superbiens	Nem védett / indifferens
Arctornis l-nigrum	Nem védett / indifferens
Arctorthezia cataphracta	Nem védett / indifferens
Arctosa cinerea	Nem védett / indifferens
Arctosa figurata	Nem védett / indifferens
Arctosa leopardus	Nem védett / indifferens
Arctosa lutetiana	Nem védett / indifferens
Arctosa maculata	Nem védett / indifferens
Arctosa perita	Nem védett / indifferens
Arcyptera fusca	Védett
Arcyptera microptera	Védett
Ardea cinerea	Védett
Ardea purpurea	Fokozottan védett
Ardeola bacchus	Védett
Ardeola ralloides	Fokozottan védett
Arenaria interpres	Védett
Arenocoris fallenii	Nem védett / indifferens
Arenostola semicana	Nem védett / indifferens
Arethusana arethusa	Védett
Argenna patula	Nem védett / indifferens
Argenna subnigra	Nem védett / indifferens
Argiope bruennichi	Nem védett / indifferens
Argiope lobata	Védett
Argna bielzi	Nem védett / indifferens
Argogorytes fargeii	Nem védett / indifferens
Argogorytes mystaceus	Nem védett / indifferens
Argolamprotes micella	Nem védett / indifferens
Argulus coregoni	Nem védett / indifferens
Argulus foliaceus	Nem védett / indifferens
Argynnis adippe	Nem védett / indifferens
Argynnis aglaja	Nem védett / indifferens
Argynnis laodice	Védett
Argynnis niobe	Védett
Argynnis pandora	Védett
Argynnis paphia	Védett
Argyresthia abdominalis	Nem védett / indifferens
Argyresthia albistria	Nem védett / indifferens
Argyresthia arceuthina	Nem védett / indifferens
Argyresthia bonnetella	Nem védett / indifferens
Argyresthia brockeella	Nem védett / indifferens
Argyresthia conjugella	Nem védett / indifferens
Argyresthia curvella	Nem védett / indifferens
Argyresthia dilectella	Nem védett / indifferens
Argyresthia glaucinella	Nem védett / indifferens
Argyresthia goedartella	Nem védett / indifferens
Argyresthia ivella	Nem védett / indifferens
Argyresthia laevigatella	Nem védett / indifferens
Argyresthia praecocella	Nem védett / indifferens
Argyresthia pruniella	Nem védett / indifferens
Argyresthia pygmaeella	Nem védett / indifferens
Argyresthia retinella	Nem védett / indifferens
Argyresthia semifusca	Nem védett / indifferens
Argyresthia semitestacella	Nem védett / indifferens
Argyresthia sorbiella	Nem védett / indifferens
Argyresthia spinosella	Nem védett / indifferens
Argyresthia thuiella	Nem védett / indifferens
Argyroneta aquatica	Védett
Argyrotaenia ljungiana	Nem védett / indifferens
Arhopalus ferus	Nem védett / indifferens
Arhopalus rusticus	Nem védett / indifferens
Arianta arbustorum	Nem védett / indifferens
Arichanna melanaria	Védett
Plebeius agestis	Védett
Plebeius artaxerxes	Védett
Plebeius artaxerxes issekutzi	Védett
Plebeius eumedon	Védett
Arion circumscriptus	Nem védett / indifferens
Arion distinctus	Nem védett / indifferens
Arion fasciatus	Nem védett / indifferens
Arion fuscus	Nem védett / indifferens
Arion hortensis	Nem védett / indifferens
Arion lusitanicus	Inváziós
Arion rufus	Inváziós
Arion silvaticus	Nem védett / indifferens
Aristotelia calastomella	Nem védett / indifferens
Aristotelia decurtella	Nem védett / indifferens
Aristotelia ericinella	Nem védett / indifferens
Aristotelia subdecurtella	Nem védett / indifferens
Aristotelia subericinella	Nem védett / indifferens
Arma custos	Nem védett / indifferens
Arma insperata	Nem védett / indifferens
Arocatus longiceps	Nem védett / indifferens
Arocatus melanocephalus	Nem védett / indifferens
Arocatus roeselii	Nem védett / indifferens
Arocephalus languidus	Nem védett / indifferens
Arocephalus longiceps	Nem védett / indifferens
Aroga flavicomella	Nem védett / indifferens
Aroga velocella	Nem védett / indifferens
Aromia moschata	Védett
Arpedium quadrum	Nem védett / indifferens
Arthaldeus arenarius	Nem védett / indifferens
Arthaldeus pascuellus	Nem védett / indifferens
Arthaldeus striifrons	Nem védett / indifferens
Arthrolips convexiuscula	Nem védett / indifferens
Arthrolips hetschkoi	Nem védett / indifferens
Arthrolips nana	Nem védett / indifferens
Arthrolips obscura	Nem védett / indifferens
Arthrolips picea	Nem védett / indifferens
Artianus interstitialis	Nem védett / indifferens
Artianus manderstjernii	Nem védett / indifferens
Artiora evonymaria	Nem védett / indifferens
Arvicola terrestris scherman	Nem védett / indifferens
Arytrura musculus	Fokozottan védett
Asaphidion austriacum	Nem védett / indifferens
Asaphidion caraboides	Nem védett / indifferens
Asaphidion flavipes	Nem védett / indifferens
Asaphidion pallipes	Nem védett / indifferens
Asarta aethiopella	Nem védett / indifferens
Ascalenia vanella	Nem védett / indifferens
Ascotis selenaria	Nem védett / indifferens
Asemum striatum	Nem védett / indifferens
Asianellus festivus	Nem védett / indifferens
Asio flammeus	Fokozottan védett
Asio otus	Védett
Asiraca clavicornis	Nem védett / indifferens
Asphalia ruficollis	Nem védett / indifferens
Aspidapion aeneum	Nem védett / indifferens
Aspidapion radiolus	Nem védett / indifferens
Aspidapion validum	Nem védett / indifferens
Aspidiphorus lareyniei	Nem védett / indifferens
Aspidiphorus orbiculatus	Nem védett / indifferens
Grapholita funebrana	Nem védett / indifferens
Grapholita janthinana	Nem védett / indifferens
Grapholita lobarzewskii	Nem védett / indifferens
Grapholita molesta	Nem védett / indifferens
Grapholita tenebrosana	Nem védett / indifferens
Aspilapteryx limosella	Nem védett / indifferens
Aspilapteryx tringipennella	Nem védett / indifferens
Aspitates gilvaria	Nem védett / indifferens
Aspius aspius	Nem védett / indifferens
Asproparthenis punctiventris	Nem védett / indifferens
Assara terebrella	Nem védett / indifferens
Astacus astacus	Védett
Astacus leptodactylus	Védett
Astata apostata	Nem védett / indifferens
Astata boops	Nem védett / indifferens
Astata brevitarsis	Nem védett / indifferens
Astata costae	Nem védett / indifferens
Astata gallica	Nem védett / indifferens
Astata jucunda	Nem védett / indifferens
Astata kashmirensis	Nem védett / indifferens
Astata minor	Nem védett / indifferens
Astata quettae	Nem védett / indifferens
Astata rufipes	Nem védett / indifferens
Astatopteryx laticollis	Nem védett / indifferens
Astenus bimaculatus	Nem védett / indifferens
Astenus gracilis	Nem védett / indifferens
Astenus immaculatus	Nem védett / indifferens
Astenus laticeps	Nem védett / indifferens
Astenus lyonessius	Nem védett / indifferens
Astenus procerus	Nem védett / indifferens
Astenus pulchellus	Nem védett / indifferens
Astenus rutilipennis	Nem védett / indifferens
Astenus uniformis	Nem védett / indifferens
Asterodiaspis bella	Nem védett / indifferens
Asterodiaspis quercicola	Nem védett / indifferens
Asterodiaspis roboris	Nem védett / indifferens
Asterodiaspis variolosa	Nem védett / indifferens
Asterodiaspis viennae	Nem védett / indifferens
Asteroscopus sphinx	Nem védett / indifferens
Asteroscopus syriaca	Nem védett / indifferens
Asteroscopus syriaca decipulae	Fokozottan védett
Asthena albulata	Nem védett / indifferens
Asthena anseraria	Nem védett / indifferens
Astrapaeus ulmi	Nem védett / indifferens
Atanygnathus terminalis	Nem védett / indifferens
Ateliotum hungaricellum	Nem védett / indifferens
Atemelia torquatella	Nem védett / indifferens
Aterpia corticana	Nem védett / indifferens
Atethmia ambusta	Védett
Atethmia centrago	Nem védett / indifferens
Athene noctua	Fokozottan védett
Atheta aeneicollis	Nem védett / indifferens
Atheta aquatica	Nem védett / indifferens
Atheta autumnalis	Nem védett / indifferens
Atheta basicornis	Nem védett / indifferens
Atheta boletophila	Nem védett / indifferens
Atheta britanniae	Nem védett / indifferens
Atheta castanoptera	Nem védett / indifferens
Atheta crassicornis	Nem védett / indifferens
Atheta divisa	Nem védett / indifferens
Atheta euryptera	Nem védett / indifferens
Atheta fungicola	Nem védett / indifferens
Atheta gagatina	Nem védett / indifferens
Atheta graminicola	Nem védett / indifferens
Atheta harwoodi	Nem védett / indifferens
Atheta hybrida	Nem védett / indifferens
Atheta hypnorum	Nem védett / indifferens
Atheta intermedia	Nem védett / indifferens
Atheta liturata	Nem védett / indifferens
Atheta nidicola	Nem védett / indifferens
Atheta nigricornis	Nem védett / indifferens
Atheta nigritula	Nem védett / indifferens
Atheta oblita	Nem védett / indifferens
Atheta pallidicornis	Nem védett / indifferens
Atheta pilicornis	Nem védett / indifferens
Atheta ravilla	Nem védett / indifferens
Atheta sodalis	Nem védett / indifferens
Atheta spelaea	Nem védett / indifferens
Atheta triangulum	Nem védett / indifferens
Atheta trinotata	Nem védett / indifferens
Atheta xanthopus	Nem védett / indifferens
Athetis furvula	Nem védett / indifferens
Athetis gluteosa	Nem védett / indifferens
Athetis pallustris	Nem védett / indifferens
Atholus bimaculatus	Nem védett / indifferens
Brenthis hecate	Védett
Atholus corvinus	Nem védett / indifferens
Atholus duodecimstriatus	Nem védett / indifferens
Atholus duodecimstriatus quatuordecimstriatus	Nem védett / indifferens
Atholus praetermissus	Nem védett / indifferens
Athous apfelbecki	Nem védett / indifferens
Athous austriacus	Nem védett / indifferens
Athous bicolor	Nem védett / indifferens
Athous haemorrhoidalis	Nem védett / indifferens
Athous kaszabi	Nem védett / indifferens
Athous silicensis	Nem védett / indifferens
Athous subfuscus	Nem védett / indifferens
Athous vittatus	Nem védett / indifferens
Athrips mouffetella	Nem védett / indifferens
Athrips nigricostella	Nem védett / indifferens
Athrips rancidella	Nem védett / indifferens
Athripsodes albifrons	Nem védett / indifferens
Athripsodes aterrimus	Nem védett / indifferens
Athripsodes bilineatus	Nem védett / indifferens
Athripsodes cinereus	Nem védett / indifferens
Athripsodes commutatus	Nem védett / indifferens
Athysanus argentarius	Nem védett / indifferens
Athysanus quadrum	Nem védett / indifferens
Atolmis rubricollis	Nem védett / indifferens
Atomaria affinis	Nem védett / indifferens
Atomaria alpina	Nem védett / indifferens
Atomaria analis	Nem védett / indifferens
Atomaria apicalis	Nem védett / indifferens
Atomaria atra	Nem védett / indifferens
Atomaria atrata	Nem védett / indifferens
Atomaria atricapilla	Nem védett / indifferens
Atomaria attila	Nem védett / indifferens
Atomaria badia	Nem védett / indifferens
Atomaria barani	Nem védett / indifferens
Atomaria basicornis	Nem védett / indifferens
Atomaria bella	Nem védett / indifferens
Atomaria clavigera	Nem védett / indifferens
Atomaria elongatula	Nem védett / indifferens
Atomaria fimetarii	Nem védett / indifferens
Atomaria fuscata	Nem védett / indifferens
Atomaria fuscicollis	Nem védett / indifferens
Atomaria fuscipes	Nem védett / indifferens
Atomaria gibbula	Nem védett / indifferens
Atomaria gravidula	Nem védett / indifferens
Atomaria gutta	Nem védett / indifferens
Atomaria impressa	Nem védett / indifferens
Atomaria jonica	Nem védett / indifferens
Atomaria lewisi	Nem védett / indifferens
Atomaria linearis	Nem védett / indifferens
Atomaria mesomelaena	Nem védett / indifferens
Atomaria morio	Nem védett / indifferens
Atomaria munda	Nem védett / indifferens
Atomaria nigripennis	Nem védett / indifferens
Atomaria nigriventris	Nem védett / indifferens
Atomaria nitidula	Nem védett / indifferens
Atomaria peltata	Nem védett / indifferens
Atomaria pulchra	Nem védett / indifferens
Atomaria punctithorax	Nem védett / indifferens
Atomaria pusilla	Nem védett / indifferens
Atomaria rubella	Nem védett / indifferens
Atomaria rubida	Nem védett / indifferens
Atomaria rubricollis	Nem védett / indifferens
Atomaria slavonica	Nem védett / indifferens
Atomaria soedermanni	Nem védett / indifferens
Atomaria testacea	Nem védett / indifferens
Atomaria umbrina	Nem védett / indifferens
Atomaria unifasciata	Nem védett / indifferens
Atomoscelis onusta	Nem védett / indifferens
Atractotomus magnicornis	Nem védett / indifferens
Atractotomus mali	Nem védett / indifferens
Atractotomus parvulus	Nem védett / indifferens
Atralata albofascialis	Nem védett / indifferens
Atrecus affinis	Nem védett / indifferens
Atrecus longiceps	Nem védett / indifferens
Atremaea lonchoptera	Nem védett / indifferens
Atrococcus achilleae	Nem védett / indifferens
Atrococcus bejbienkoi	Nem védett / indifferens
Atrococcus cracens	Nem védett / indifferens
Atrococcus paludinus	Nem védett / indifferens
Attaephilus arenarius	Nem védett / indifferens
Attagenus brunneus	Nem védett / indifferens
Attagenus pellio	Nem védett / indifferens
Attagenus punctatus	Nem védett / indifferens
Attagenus schaefferi	Nem védett / indifferens
Attagenus unicolor	Nem védett / indifferens
Attalus analis	Nem védett / indifferens
Attalus thalassimus	Nem védett / indifferens
Attelabus nitens	Nem védett / indifferens
Attheyella crassa	Nem védett / indifferens
Attheyella trispinosa	Nem védett / indifferens
Attheyella wierzejski	Nem védett / indifferens
Atylotus fulvus	Nem védett / indifferens
Atylotus loewianus	Nem védett / indifferens
Atylotus rusticus	Nem védett / indifferens
Atypha pulmonaris	Nem védett / indifferens
Atypus affinis	Védett
Atypus muralis	Védett
Atypus piceus	Védett
Auchmis detersa	Nem védett / indifferens
Augasma aeratella	Nem védett / indifferens
Aulacaspis rosae	Nem védett / indifferens
Aulacobaris angusta	Nem védett / indifferens
Aulacobaris chlorizans	Nem védett / indifferens
Aulacobaris coerulescens	Nem védett / indifferens
Aulacobaris gudenusi	Nem védett / indifferens
Aulacobaris kaufmanni	Nem védett / indifferens
Aulacobaris lepidii	Nem védett / indifferens
Aulacobaris picicornis	Nem védett / indifferens
Aulacobaris villae	Nem védett / indifferens
Aulacochthebius exaratus	Nem védett / indifferens
Aulacochthebius narentinus	Nem védett / indifferens
Auletobius sanguisorbae	Nem védett / indifferens
Auleutes epilobii	Nem védett / indifferens
Aulonia albimana	Nem védett / indifferens
Aulonium ruficorne	Nem védett / indifferens
Aulonium trisulcum	Nem védett / indifferens
Aulonogyrus concinnus	Nem védett / indifferens
Aulonothroscus brevicollis	Nem védett / indifferens
Auplopus albifrons	Nem védett / indifferens
Auplopus carbonarius	Nem védett / indifferens
Auplopus rectus	Nem védett / indifferens
Perotis lugubris	Nem védett / indifferens
Austroagallia sinuata	Nem védett / indifferens
Austroasca vittata	Nem védett / indifferens
Austropotamobius torrentium	Védett
Autalia impressa	Nem védett / indifferens
Autalia rivularis	Nem védett / indifferens
Autographa bractea	Védett
Autographa gamma	Nem védett / indifferens
Autographa jota	Védett
Autographa pulchrina	Nem védett / indifferens
Axinopalpis gracilis	Nem védett / indifferens
Axinotarsus marginalis	Nem védett / indifferens
Axinotarsus pulicarius	Nem védett / indifferens
Axinotarsus ruficollis	Nem védett / indifferens
Axylia putris	Nem védett / indifferens
Aythya affinis	Védett
Aythya collaris	Védett
Aythya ferina	Védett
Aythya fuligula	Védett
Aythya marila	Védett
Aythya nyroca	Fokozottan védett
Cyclobacanius soliman	Nem védett / indifferens
Baccha elongata	Nem védett / indifferens
Baccha obscuripennis	Nem védett / indifferens
Bacotia claustrella	Nem védett / indifferens
Bactra furfurana	Nem védett / indifferens
Bactra lacteana	Nem védett / indifferens
Bactra lancealana	Nem védett / indifferens
Bactra robustana	Nem védett / indifferens
Badister bullatus	Nem védett / indifferens
Badister collaris	Nem védett / indifferens
Badister dilatatus	Nem védett / indifferens
Badister dorsiger	Nem védett / indifferens
Badister lacertosus	Nem védett / indifferens
Badister meridionalis	Nem védett / indifferens
Badister peltatus	Nem védett / indifferens
Badister sodalis	Nem védett / indifferens
Badister unipustulatus	Nem védett / indifferens
Badura cauta	Nem védett / indifferens
Badura ischnocera	Nem védett / indifferens
Badura macrocera	Nem védett / indifferens
Baeocrara japonica	Nem védett / indifferens
Baetis alpinus	Nem védett / indifferens
Baetis buceratus	Nem védett / indifferens
Baetis fuscatus	Nem védett / indifferens
Baetis gracilis	Nem védett / indifferens
Baetis lutheri	Nem védett / indifferens
Baetis muticus	Nem védett / indifferens
Baetis niger	Nem védett / indifferens
Baetis nexus	Nem védett / indifferens
Baetis rhodani	Nem védett / indifferens
Baetis scambus	Nem védett / indifferens
Baetis tracheatus	Nem védett / indifferens
Baetis tricolor	Nem védett / indifferens
Baetis vardarensis	Nem védett / indifferens
Baetis vernus	Nem védett / indifferens
Baetopus tenellus	Nem védett / indifferens
Bagoopsis globicollis	Nem védett / indifferens
Bagous alismatis	Nem védett / indifferens
Bagous angustus	Nem védett / indifferens
Bagous argillaceus	Nem védett / indifferens
Bagous bagdatensis	Nem védett / indifferens
Bagous binodulus	Nem védett / indifferens
Bagous collignensis	Nem védett / indifferens
Bagous czwalinae	Nem védett / indifferens
Bagous diglyptus	Nem védett / indifferens
Bagous elegans	Nem védett / indifferens
Bagous frit	Nem védett / indifferens
Bagous frivaldszkyi	Nem védett / indifferens
Bagous geniculatus	Nem védett / indifferens
Bagous glabrirostris	Nem védett / indifferens
Bagous limosus	Nem védett / indifferens
Bagous longitarsis	Nem védett / indifferens
Bagous lutosus	Nem védett / indifferens
Bagous lutulentus	Nem védett / indifferens
Bagous lutulosus	Nem védett / indifferens
Bagous majzlani	Nem védett / indifferens
Bagous nodulosus	Nem védett / indifferens
Bagous petro	Nem védett / indifferens
Bagous puncticollis	Nem védett / indifferens
Bagous robustus	Nem védett / indifferens
Bagous rotundicollis	Nem védett / indifferens
Bagous subcarinatus	Nem védett / indifferens
Bagous tempestivus	Nem védett / indifferens
Bagous tubulus	Nem védett / indifferens
Bagous validus	Nem védett / indifferens
Bagrada stolata	Nem védett / indifferens
Balanococcus boratynskii	Nem védett / indifferens
Balcanocerus larvatus	Nem védett / indifferens
Balclutha calamagrostis	Nem védett / indifferens
Balclutha punctata	Nem védett / indifferens
Balclutha rhenana	Nem védett / indifferens
Balclutha saltuella	Nem védett / indifferens
Balea biplicata	Nem védett / indifferens
Balea perversa	Nem védett / indifferens
Balea stabilis	Nem védett / indifferens
Ballus chalybeius	Nem védett / indifferens
Ballus rufipes	Nem védett / indifferens
Bangasternus orientalis	Nem védett / indifferens
Barbastella barbastellus	Fokozottan védett
Barbatula barbatula	Védett
Barbitistes constrictus	Nem védett / indifferens
Barbitistes serricauda	Nem védett / indifferens
Barbus barbus	Nem védett / indifferens
Barbus meridionalis	Fokozottan védett
Barbus peloponnesius	Fokozottan védett
Barbus carpathicus	Fokozottan védett
Baris analis	Nem védett / indifferens
Baris artemisiae	Nem védett / indifferens
Baris nesapia	Nem védett / indifferens
Baris steppensis	Nem védett / indifferens
Barynotus hungaricus	Nem védett / indifferens
Barynotus moerens	Nem védett / indifferens
Barynotus obscurus	Nem védett / indifferens
Barypeithes araneiformis	Nem védett / indifferens
Barypeithes chevrolati	Nem védett / indifferens
Barypeithes formaneki	Nem védett / indifferens
Barypeithes interpositus	Nem védett / indifferens
Barypeithes interpositus siliciensis	Nem védett / indifferens
Barypeithes liptoviensis	Nem védett / indifferens
Barypeithes mollicomus	Nem védett / indifferens
Barypeithes pellucidus	Nem védett / indifferens
Barypeithes styriacus	Nem védett / indifferens
Barypeithes tenex	Nem védett / indifferens
Basistriga flammatra	Nem védett / indifferens
Batazonellus lacerticida	Védett
Bathynella natans	Nem védett / indifferens
Bathyomphalus contortus	Nem védett / indifferens
Bathyphantes approximatus	Nem védett / indifferens
Bathyphantes gracilis	Nem védett / indifferens
Bathyphantes nigrinus	Nem védett / indifferens
Bathyphantes similis	Nem védett / indifferens
Bathysolen nubilus	Nem védett / indifferens
Batia internella	Nem védett / indifferens
Batia lambdella	Nem védett / indifferens
Batia lunaris	Nem védett / indifferens
Batrachedra pinicolella	Nem védett / indifferens
Batrachedra praeangusta	Nem védett / indifferens
Batracobdelloides moogi	Nem védett / indifferens
Batracomorphus allionii	Nem védett / indifferens
Batracomorphus irroratus	Nem védett / indifferens
Batrisodes adnexus	Nem védett / indifferens
Batrisodes delaporti	Nem védett / indifferens
Batrisodes oculatus	Nem védett / indifferens
Batrisodes sulcaticeps	Nem védett / indifferens
Batrisodes venustus	Nem védett / indifferens
Batrisus formicarius	Nem védett / indifferens
Bedellia ehikella	Nem védett / indifferens
Bedellia somnulentella	Nem védett / indifferens
Belomicroides zimini	Nem védett / indifferens
Belomicrus antennalis	Nem védett / indifferens
Belomicrus italicus	Nem védett / indifferens
Bembecia albanensis	Nem védett / indifferens
Bembecia ichneumoniformis	Nem védett / indifferens
Bembecia megillaeformis	Nem védett / indifferens
Bembecia puella	Nem védett / indifferens
Bembecia scopigera	Nem védett / indifferens
Bembecia uroceriformis	Nem védett / indifferens
Bembecinus hungaricus	Nem védett / indifferens
Bembecinus tridens	Nem védett / indifferens
Odontium argenteolum	Nem védett / indifferens
Trepanes articulatus	Nem védett / indifferens
Trepanes assimilis	Nem védett / indifferens
Emphanes azurescens	Nem védett / indifferens
Philochthus biguttatus	Nem védett / indifferens
Ocydromus cruciatus	Nem védett / indifferens
Ocydromus cruciatus bualei	Nem védett / indifferens
Ocydromus dalmatinus	Nem védett / indifferens
Sinechostictus decoratus	Nem védett / indifferens
Ocydromus decorus	Nem védett / indifferens
Ocydromus deletus	Nem védett / indifferens
Notaphus dentellus	Nem védett / indifferens
Sinechostictus doderoi	Nem védett / indifferens
Trepanes doris	Nem védett / indifferens
Sinechostictus elongatus	Nem védett / indifferens
Notaphus ephippium	Nem védett / indifferens
Ocydromus fasciolatus	Nem védett / indifferens
Ocydromus femoratus	Nem védett / indifferens
Ocydromus fluviatilis	Nem védett / indifferens
Odontium foraminosum	Nem védett / indifferens
Ocydromus fulvipes	Nem védett / indifferens
Trepanes fumigatus	Nem védett / indifferens
Bembidion geniculatum	Nem védett / indifferens
Trepanes gilvipes	Nem védett / indifferens
Philochthus guttula	Nem védett / indifferens
Ocydromus tetragrammus illigeri	Nem védett / indifferens
Philochthus inoptatus	Nem védett / indifferens
Sinechostictus inustum	Nem védett / indifferens
Metallina lampros	Nem védett / indifferens
Odontium laticolle	Nem védett / indifferens
Emphanes latiplaga	Nem védett / indifferens
Odontium litorale	Nem védett / indifferens
Philochthus lunulatus	Nem védett / indifferens
Ocydromus lunulatus	Nem védett / indifferens
Philochthus mannerheimii	Nem védett / indifferens
Emphanes minimus	Nem védett / indifferens
Ocydromus modestus	Nem védett / indifferens
Ocydromus monticola	Nem védett / indifferens
Phyla obtusa	Nem védett / indifferens
Trepanes octomaculatus	Nem védett / indifferens
Plataphus prasinus	Nem védett / indifferens
Metallina properans	Nem védett / indifferens
Princidium punctulatum	Nem védett / indifferens
Metallina pygmaea	Nem védett / indifferens
Bembidion quadrimaculatum	Nem védett / indifferens
Bembidion quadripustulatum	Nem védett / indifferens
Bembidion rivulare	Nem védett / indifferens
Sinechostictus ruficornis	Nem védett / indifferens
Trepanes schueppelii	Nem védett / indifferens
Notaphus semipunctatus	Nem védett / indifferens
Metallina splendida	Nem védett / indifferens
Notaphus starkii	Nem védett / indifferens
Ocydromus stephensii	Nem védett / indifferens
Sinechostictus stomoides	Nem védett / indifferens
Odontium striatum	Nem védett / indifferens
Ocydromus subcostatus	Nem védett / indifferens
Ocydromus subcostatus javurkovae	Nem védett / indifferens
Emphanes tenellus	Nem védett / indifferens
Ocydromus testaceus	Nem védett / indifferens
Ocydromus tetracolus	Nem védett / indifferens
Ocydromus tibialis	Nem védett / indifferens
Notaphus varius	Nem védett / indifferens
Odontium velox	Nem védett / indifferens
Bembix bidentata	Nem védett / indifferens
Bembix megerlei	Nem védett / indifferens
Bembix oculata	Nem védett / indifferens
Bembix olivacea	Nem védett / indifferens
Bembix rostrata	Nem védett / indifferens
Bembix tarsata	Nem védett / indifferens
Bena bicolorana	Nem védett / indifferens
Benibotarus taygetanus	Nem védett / indifferens
Beosus maritimus	Nem védett / indifferens
Beosus quadripunctatus	Nem védett / indifferens
Beraea maurus	Nem védett / indifferens
Beraea pullata	Nem védett / indifferens
Beraeamyia hrabei	Nem védett / indifferens
Beraeodes minutus	Nem védett / indifferens
Berginus tamarisci	Nem védett / indifferens
Berlandina cinerea	Nem védett / indifferens
Berosus affinis	Nem védett / indifferens
Berosus frontifoveatus	Nem védett / indifferens
Berosus fulvus	Nem védett / indifferens
Berosus geminus	Nem védett / indifferens
Berosus luridus	Nem védett / indifferens
Berosus signaticollis	Nem védett / indifferens
Berosus spinosus	Nem védett / indifferens
Berytinus clavipes	Nem védett / indifferens
Berytinus consimilis	Nem védett / indifferens
Berytinus crassipes	Nem védett / indifferens
Berytinus distinguendus	Nem védett / indifferens
Berytinus geniculatus	Nem védett / indifferens
Berytinus hirticornis	Nem védett / indifferens
Berytinus minor	Nem védett / indifferens
Berytinus montivagus	Nem védett / indifferens
Berytinus signoreti	Nem védett / indifferens
Berytinus striola	Nem védett / indifferens
Besdolus ventralis	Védett
Bessobia fungivora	Nem védett / indifferens
Bessobia occulta	Nem védett / indifferens
Bessobia spatula	Nem védett / indifferens
Betarmon bisbimaculatus	Nem védett / indifferens
Betulapion simile	Nem védett / indifferens
Biastes brevicornis	Nem védett / indifferens
Biastes emarginatus	Nem védett / indifferens
Bibloplectus ambiguus	Nem védett / indifferens
Bibloplectus delhermi	Nem védett / indifferens
Bibloplectus hungaricus	Nem védett / indifferens
Bibloplectus minutissimus	Nem védett / indifferens
Bibloplectus obtusus	Nem védett / indifferens
Bibloplectus pusillus	Nem védett / indifferens
Bibloplectus tenebrosus	Nem védett / indifferens
Bibloporus bicolor	Nem védett / indifferens
Bibloporus mayeti	Nem védett / indifferens
Bibloporus minutus	Nem védett / indifferens
Bidessus grossepunctatus	Nem védett / indifferens
Bidessus nasutus	Nem védett / indifferens
Bidessus unistriatus	Nem védett / indifferens
Bielzia coerulans	Védett
Bijugis bombycella	Nem védett / indifferens
Bijugis pectinella	Nem védett / indifferens
Biphyllus frater	Nem védett / indifferens
Biphyllus lunatus	Nem védett / indifferens
Biralus satellitius	Nem védett / indifferens
Elachista utonella	Nem védett / indifferens
Bisigna procerella	Nem védett / indifferens
Bisnius binotatus	Nem védett / indifferens
Bisnius cephalotes	Nem védett / indifferens
Bisnius fimetarius	Nem védett / indifferens
Bisnius nigriventris	Nem védett / indifferens
Bisnius nitidulus	Nem védett / indifferens
Bisnius sparsus	Nem védett / indifferens
Bisnius pseudoparcus	Nem védett / indifferens
Bisnius scribae	Nem védett / indifferens
Bisnius sordidus	Nem védett / indifferens
Bisnius spermophili	Nem védett / indifferens
Bisnius subuliformis	Nem védett / indifferens
Biston betularia	Nem védett / indifferens
Biston strataria	Nem védett / indifferens
Bithynia leachii	Nem védett / indifferens
Bithynia tentaculata	Nem védett / indifferens
Bithynia troschelii	Nem védett / indifferens
Bitoma crenata	Nem védett / indifferens
Blaniulus guttulatus	Nem védett / indifferens
Blaps abbreviata	Védett
Blaps halophila	Nem védett / indifferens
Blaps lethifera	Nem védett / indifferens
Blaps mortisaga	Nem védett / indifferens
Blastobasis huemeri	Nem védett / indifferens
Blastobasis phycidella	Nem védett / indifferens
Blastodacna atra	Nem védett / indifferens
Bothriomyrmex adriaticus	Nem védett / indifferens
Blastodacna hellerella	Nem védett / indifferens
Bledius atricapillus	Nem védett / indifferens
Bledius baudii	Nem védett / indifferens
Bledius bicornis	Nem védett / indifferens
Bledius crassicollis	Nem védett / indifferens
Bledius cribricollis	Nem védett / indifferens
Bledius defensus	Nem védett / indifferens
Bledius dissimilis	Nem védett / indifferens
Bledius erraticus	Nem védett / indifferens
Bledius fergussoni	Nem védett / indifferens
Bledius fossor	Nem védett / indifferens
Bledius furcatus	Nem védett / indifferens
Bledius gallicus	Nem védett / indifferens
Bledius longulus	Nem védett / indifferens
Bledius nanus	Nem védett / indifferens
Bledius opacus	Nem védett / indifferens
Bledius pallipes	Nem védett / indifferens
Bledius procerulus	Nem védett / indifferens
Bledius pusillus	Nem védett / indifferens
Bledius pygmaeus	Nem védett / indifferens
Bledius roubali	Nem védett / indifferens
Bledius spectabilis	Nem védett / indifferens
Bledius subterraneus	Nem védett / indifferens
Bledius tibialis	Nem védett / indifferens
Bledius tricornis	Nem védett / indifferens
Bledius unicornis	Nem védett / indifferens
Bledius verres	Nem védett / indifferens
Blepharidopterus angulatus	Nem védett / indifferens
Blepharidopterus diaphanus	Nem védett / indifferens
Blera fallax	Nem védett / indifferens
Blethisa multipunctata	Nem védett / indifferens
Bobacella corvina	Nem védett / indifferens
Bodilus circumcinctus	Nem védett / indifferens
Bodilus ictericus	Nem védett / indifferens
Bodilus lugens	Nem védett / indifferens
Bodilus punctipennis	Nem védett / indifferens
Boettgerilla pallens	Nem védett / indifferens
Bohemannia pulverosella	Nem védett / indifferens
Bolbelasmus unicornis	Védett
Bolitobius castaneus	Nem védett / indifferens
Bolitobius cingulatus	Nem védett / indifferens
Parabolitobius formosus	Nem védett / indifferens
Parabolitobius inclinans	Nem védett / indifferens
Bolitochara bella	Nem védett / indifferens
Bolitochara lucida	Nem védett / indifferens
Bolitochara mulsanti	Nem védett / indifferens
Bolitochara obliqua	Nem védett / indifferens
Bolitochara pulchra	Nem védett / indifferens
Bolitochara reyi	Nem védett / indifferens
Bolitophagus interruptus	Nem védett / indifferens
Bolitophagus reticulatus	Nem védett / indifferens
Boloria dia	Nem védett / indifferens
Boloria euphrosyne	Védett
Boloria selene	Védett
Bolyphantes alticeps	Nem védett / indifferens
Bolyphantes luteolus	Nem védett / indifferens
Bombina bombina	Védett
Bombina variegata	Védett
Bombus argillaceus	Védett
Bombus confusus	Védett
Bombus distinguendus	Nem védett / indifferens
Bombus elegans	Nem védett / indifferens
Bombus fragrans	Fokozottan védett
Bombus gerstaeckeri	Nem védett / indifferens
Bombus haematurus	Nem védett / indifferens
Bombus hortorum	Nem védett / indifferens
Bombus humilis	Védett
Bombus hypnorum	Nem védett / indifferens
Bombus hypnorum ericetorum	Nem védett / indifferens
Bombus laesus	Védett
Bombus laesus mocsaryi	Nem védett / indifferens
Bombus lapidarius	Nem védett / indifferens
Bombus muscorum	Védett
Bombus paradoxus	Védett
Bombus pascuorum	Nem védett / indifferens
Bombus pomorum	Védett
Bombus pratorum	Nem védett / indifferens
Bombus ruderarius	Nem védett / indifferens
Bombus ruderatus	Védett
Bombus serrisquama	Nem védett / indifferens
Bombus soroeensis	Védett
Bombus subterraneus	Védett
Bombus sylvarum	Védett
Bombus terrestris	Nem védett / indifferens
Bombus wurfleini	Nem védett / indifferens
Bombus wurfleini mastrucatus	Nem védett / indifferens
Bombycilla garrulus	Védett
Bombyx mori	Nem védett / indifferens
Bonasa bonasia	Fokozottan védett
Bonasa bonasia rupestris	Fokozottan védett
Boreococcus ingricus	Nem védett / indifferens
Boreoiulus tenuis	Nem védett / indifferens
Borkhausenia fuscescens	Nem védett / indifferens
Borkhausenia minutella	Nem védett / indifferens
Borysthenia naticina	Védett
Bosmina coregoni	Nem védett / indifferens
Bosmina longirostris	Nem védett / indifferens
Bostrichus capucinus	Nem védett / indifferens
Botaurus stellaris	Fokozottan védett
Bothrideres contractus	Nem védett / indifferens
Bothrostethus annulipes	Nem védett / indifferens
Bothynoderes affinis	Nem védett / indifferens
Bothynoderes declivis	Nem védett / indifferens
Bothynotus pilosus	Nem védett / indifferens
Brachida exigua	Nem védett / indifferens
Brachinus bipustulatus	Védett
Brachinus crepitans	Nem védett / indifferens
Brachinus ejaculans	Nem védett / indifferens
Brachinus explodens	Nem védett / indifferens
Brachinus elegans	Nem védett / indifferens
Brachinus nigricornis	Nem védett / indifferens
Brachinus plagiatus	Nem védett / indifferens
Brachinus psophia	Nem védett / indifferens
Brachionycha nubeculosa	Nem védett / indifferens
Brachmia blandella	Nem védett / indifferens
Brachmia dimidiella	Nem védett / indifferens
Brachmia inornatella	Nem védett / indifferens
Brachmia procursella	Nem védett / indifferens
Brachodes appendiculata	Nem védett / indifferens
Brachodes pumila	Nem védett / indifferens
Brachonyx pineti	Nem védett / indifferens
Brachycarenus tigrinus	Nem védett / indifferens
Brachycentrus subnubilus	Nem védett / indifferens
Tettigometra laeta	Nem védett / indifferens
Brachycercus europaeus	Védett
Brachycercus harrisella	Nem védett / indifferens
Cercobrachys minutus	Nem védett / indifferens
Brachycerus foveicollis	Nem védett / indifferens
Brachycoleus decolor	Nem védett / indifferens
Brachycoleus pilicornis	Nem védett / indifferens
Brachyderes incanus	Nem védett / indifferens
Brachydesmus Attemsii	Nem védett / indifferens
Brachydesmus attemsii tenkesensis	Nem védett / indifferens
Brachydesmus Dadayi	Nem védett / indifferens
Brachydesmus superus	Nem védett / indifferens
Brachydesmus troglobius	Nem védett / indifferens
Brachygluta fossulata	Nem védett / indifferens
Brachygluta haematica	Nem védett / indifferens
Brachygluta sinuata	Nem védett / indifferens
Brachygluta helferi	Nem védett / indifferens
Brachygluta helferi longispina	Nem védett / indifferens
Brachygluta retowskii	Nem védett / indifferens
Brachygonus megerlei	Nem védett / indifferens
Brachygonus ruficeps	Nem védett / indifferens
Brachyiulus bagnalli	Nem védett / indifferens
Brachyiulus pusillus	Nem védett / indifferens
Brachyleptus quadratus	Nem védett / indifferens
Brachylomia viminalis	Nem védett / indifferens
Brachymyia berberina	Nem védett / indifferens
Brachymyia floccosa	Nem védett / indifferens
Brachynotocoris puncticornis	Nem védett / indifferens
Brachyopa bicolor	Nem védett / indifferens
Brachyopa dorsata	Nem védett / indifferens
Brachyopa insensilis	Nem védett / indifferens
Brachyopa maculipennis	Nem védett / indifferens
Brachyopa pilosa	Nem védett / indifferens
Brachyopa scutellaris	Nem védett / indifferens
Brachypalpoides lentus	Nem védett / indifferens
Brachypalpus chrysites	Nem védett / indifferens
Brachypalpus laphriformis	Nem védett / indifferens
Brachypalpus valgus	Nem védett / indifferens
Brachyplax tenuis	Nem védett / indifferens
Brachyptera braueri	Védett
Brachyptera monilicornis	Nem védett / indifferens
Brachyptera risi	Nem védett / indifferens
Brachyptera seticornis	Nem védett / indifferens
Brachyptera trifasciata	Nem védett / indifferens
Brachypterolus antirrhini	Nem védett / indifferens
Brachypterolus linariae	Nem védett / indifferens
Brachypterolus pulicarius	Nem védett / indifferens
Brachypterus fulvipes	Nem védett / indifferens
Brachypterus glaber	Nem védett / indifferens
Brachypterus urticae	Nem védett / indifferens
Brachyschendyla montana	Nem védett / indifferens
Brachysomus dispar	Nem védett / indifferens
Brachysomus echinatus	Nem védett / indifferens
Brachysomus fremuthi	Nem védett / indifferens
Brachysomus frivaldszkyi	Nem védett / indifferens
Brachysomus hirtus	Nem védett / indifferens
Brachysomus hispidus	Nem védett / indifferens
Brachysomus mihoki	Védett
Brachysomus setiger	Nem védett / indifferens
Brachysomus slovacicus	Nem védett / indifferens
Brachysomus styriacus	Nem védett / indifferens
Brachysomus subnudus	Nem védett / indifferens
Brachysomus villosulus	Nem védett / indifferens
Brachysomus zellichi	Nem védett / indifferens
Brachystegus scalaris	Nem védett / indifferens
Brachytemnus porcatus	Nem védett / indifferens
Brachytron pratense	Nem védett / indifferens
Brachyusa concolor	Nem védett / indifferens
Bradleycypris obliqua	Nem védett / indifferens
Bradleystrandesia fuscata	Nem védett / indifferens
Bradleystrandesia hirsuta	Nem védett / indifferens
Bradleystrandesia reticulata	Nem védett / indifferens
Bradybatus creutzeri	Nem védett / indifferens
Bradybatus elongatulus	Nem védett / indifferens
Bradybatus fallax	Nem védett / indifferens
Bradybatus kellneri	Nem védett / indifferens
Bradybatus tomentosus	Nem védett / indifferens
Bradycellus caucasicus	Nem védett / indifferens
Bradycellus csikii	Nem védett / indifferens
Bradycellus harpalinus	Nem védett / indifferens
Bradycellus verbasci	Nem védett / indifferens
Bradyrrhoa trapezella	Nem védett / indifferens
Branchinecta ferox	Nem védett / indifferens
Branchinecta orientalis	Nem védett / indifferens
Branchipus schaefferi	Nem védett / indifferens
Branta bernicla	Védett
Branta canadensis	Nem védett / indifferens
Branta leucopsis	Védett
Branta ruficollis	Fokozottan védett
Brenthis ino	Védett
Brevennia pulveraria	Nem védett / indifferens
Brevennia tetrapora	Nem védett / indifferens
Brintesia circe	Nem védett / indifferens
Brommella falcigera	Nem védett / indifferens
Broscus cephalotes	Nem védett / indifferens
Anaproutia comitella	Nem védett / indifferens
Bruchela cana	Nem védett / indifferens
Bruchela conformis	Nem védett / indifferens
Bruchela muscula	Nem védett / indifferens
Bruchela orientalis	Nem védett / indifferens
Bruchela rufipes	Nem védett / indifferens
Bruchela schusteri	Nem védett / indifferens
Bruchela suturalis	Nem védett / indifferens
Brumus quadripustulatus	Nem védett / indifferens
Brundinia marina	Nem védett / indifferens
Brundinia meridionalis	Nem védett / indifferens
Bryaxis bulbifer	Nem védett / indifferens
Bryaxis carinula	Nem védett / indifferens
Bryaxis curtisii	Nem védett / indifferens
Bryaxis curtisii orientalis	Nem védett / indifferens
Bryaxis femoratus	Nem védett / indifferens
Bryaxis glabricollis	Nem védett / indifferens
Bryaxis nigripennis	Nem védett / indifferens
Bryaxis puncticollis	Nem védett / indifferens
Brychius elevatus	Nem védett / indifferens
Bryocamptus minutus	Nem védett / indifferens
Bryocamptus pygmaeus	Nem védett / indifferens
Bryocamptus weberi	Nem védett / indifferens
Bryocamptus zschokkei	Nem védett / indifferens
Bryocoris pteridis	Nem védett / indifferens
Bryophacis rufus	Nem védett / indifferens
Bryoporus cernuus	Nem védett / indifferens
Bryoporus multipunctus	Nem védett / indifferens
Bryotropha affinis	Nem védett / indifferens
Bryotropha basaltinella	Nem védett / indifferens
Bryotropha desertella	Nem védett / indifferens
Bryotropha domestica	Nem védett / indifferens
Bryotropha galbanella	Nem védett / indifferens
Bryotropha plantariella	Nem védett / indifferens
Bryotropha senectella	Nem védett / indifferens
Bryotropha similis	Nem védett / indifferens
Bryotropha tachyptilella	Nem védett / indifferens
Bryotropha terrella	Nem védett / indifferens
Bryotropha umbrosella	Nem védett / indifferens
Bubo bubo	Fokozottan védett
Bubulcus ibis	Védett
Bucculatrix absinthii	Nem védett / indifferens
Bucculatrix albedinella	Nem védett / indifferens
Bucculatrix artemisiella	Nem védett / indifferens
Bucculatrix bechsteinella	Nem védett / indifferens
Bucculatrix benacicolella	Nem védett / indifferens
Bucculatrix cantabricella	Nem védett / indifferens
Bucculatrix cidarella	Nem védett / indifferens
Bucculatrix cristatella	Nem védett / indifferens
Bucculatrix demaryella	Nem védett / indifferens
Bucculatrix frangutella	Nem védett / indifferens
Bucculatrix gnaphaliella	Nem védett / indifferens
Bucculatrix maritima	Nem védett / indifferens
Bucculatrix nigricomella	Nem védett / indifferens
Bucculatrix noltei	Nem védett / indifferens
Bucculatrix ratisbonensis	Nem védett / indifferens
Bucculatrix rhamniella	Nem védett / indifferens
Bucculatrix thoracella	Nem védett / indifferens
Bucculatrix ulmella	Nem védett / indifferens
Bucculatrix ulmifoliae	Nem védett / indifferens
Bucephala clangula	Védett
Bufo bufo	Védett
Bufo viridis	Védett
Bulgarica cana	Nem védett / indifferens
Bulgarica rugicollis	Nem védett / indifferens
Bulgarica vetusta	Nem védett / indifferens
Bunops serricaudata	Nem védett / indifferens
Bupalus piniaria	Nem védett / indifferens
Buprestis novemmaculata	Nem védett / indifferens
Buprestis octoguttata	Nem védett / indifferens
Buprestis rustica	Nem védett / indifferens
Burhinus oedicnemus	Fokozottan védett
Buteo buteo	Védett
Buteo buteo vulpinus	Védett
Buteo lagopus	Védett
Buteo rufinus	Fokozottan védett
Denisia stroemella	Nem védett / indifferens
Byctiscus betulae	Nem védett / indifferens
Byctiscus populi	Nem védett / indifferens
Byrrhus fasciatus	Nem védett / indifferens
Byrrhus gigas	Nem védett / indifferens
Byrrhus luniger	Nem védett / indifferens
Byrrhus pilula	Nem védett / indifferens
Byrrhus pustulatus	Nem védett / indifferens
Byrsinus fossor	Nem védett / indifferens
Bythinella austriaca	Nem védett / indifferens
Bythinus acutangulus	Nem védett / indifferens
Bythinus burrelli	Nem védett / indifferens
Bythinus macropalpus	Nem védett / indifferens
Bythinus reichenbachi	Nem védett / indifferens
Bythinus securiger	Nem védett / indifferens
Bythiospeum hungaricum	Védett
Bythiospeum oshanovae	Nem védett / indifferens
Byturus ochraceus	Nem védett / indifferens
Byturus tomentosus	Nem védett / indifferens
Cabera exanthemata	Nem védett / indifferens
Cabera pusaria	Nem védett / indifferens
Caccobius histeroides	Nem védett / indifferens
Caccobius schreberi	Nem védett / indifferens
Cacotemnus rufipes	Nem védett / indifferens
Cadra cautella	Nem védett / indifferens
Cadra figulilella	Nem védett / indifferens
Cadra furcatella	Nem védett / indifferens
Caenis beskidensis	Nem védett / indifferens
Caenis horaria	Nem védett / indifferens
Caenis lactea	Nem védett / indifferens
Caenis luctuosa	Nem védett / indifferens
Caenis macrura	Nem védett / indifferens
Caenis martae	Nem védett / indifferens
Caenis pseudorivulorum	Nem védett / indifferens
Caenis robusta	Nem védett / indifferens
Caenocara affine	Nem védett / indifferens
Caenocara bovistae	Nem védett / indifferens
Caenoscelis ferruginea	Nem védett / indifferens
Caenoscelis subdeplanata	Nem védett / indifferens
Calambus bipustulatus	Nem védett / indifferens
Calamia tridens	Nem védett / indifferens
Calamobius filum	Védett
Calamosternus granarius	Nem védett / indifferens
Calamotettix taeniatus	Nem védett / indifferens
Calamotropha aureliellus	Nem védett / indifferens
Calamotropha paludella	Nem védett / indifferens
Calandrella brachydactyla	Fokozottan védett
Calandrella brachydactyla hungarica	Fokozottan védett
Calathus ambiguus	Nem védett / indifferens
Calathus cinctus	Nem védett / indifferens
Calathus erratus	Nem védett / indifferens
Calathus fuscipes	Nem védett / indifferens
Calathus melanocephalus	Nem védett / indifferens
Calcarius lapponicus	Védett
Caliadurgus fasciatellus	Nem védett / indifferens
Calidris alba	Védett
Calidris alpina	Védett
Calidris alpina schinzii	Védett
Calidris bairdii	Védett
Calidris canutus	Védett
Calidris ferruginea	Védett
Calidris fuscicollis	Védett
Calidris maritima	Védett
Calidris melanotos	Védett
Calidris minuta	Védett
Calidris pusilla	Védett
Calidris temminckii	Védett
Caliprobola speciosa	Nem védett / indifferens
Caliscelis wallengreni	Nem védett / indifferens
Callicera aenea	Nem védett / indifferens
Callicera rufa	Nem védett / indifferens
Callicera spinolae	Nem védett / indifferens
Callicerus obscurus	Nem védett / indifferens
Callicerus rigidicornis	Nem védett / indifferens
Callicorixa praeusta	Nem védett / indifferens
Callidium aeneum	Nem védett / indifferens
Callidium coriaceum	Nem védett / indifferens
Callidium violaceum	Nem védett / indifferens
Calliergis ramosa	Nem védett / indifferens
Calligypona reyi	Nem védett / indifferens
Callilepis nocturna	Nem védett / indifferens
Callilepis schuszteri	Nem védett / indifferens
Callimus angulatus	Nem védett / indifferens
Callimorpha dominula	Nem védett / indifferens
Callimoxys gracilis	Nem védett / indifferens
Calliptamus barbarus	Védett
Calliptamus italicus	Nem védett / indifferens
Callisto denticulella	Nem védett / indifferens
Callistus lunatus	Nem védett / indifferens
Calliteara abietis	Nem védett / indifferens
Calliteara pudibunda	Nem védett / indifferens
Callobius claustrarius	Nem védett / indifferens
Callophrys rubi	Nem védett / indifferens
Callopistria juventina	Nem védett / indifferens
Calodera aethiops	Nem védett / indifferens
Calodera ligula	Nem védett / indifferens
Calodera nigrita	Nem védett / indifferens
Calodera riparia	Nem védett / indifferens
Calodera rufescens	Nem védett / indifferens
Calodera uliginosa	Nem védett / indifferens
Calodromius spilotus	Nem védett / indifferens
Calophasia lunula	Nem védett / indifferens
Calophasia opalina	Nem védett / indifferens
Calophasia platyptera	Nem védett / indifferens
Calopteryx splendens	Nem védett / indifferens
Calopteryx virgo	Védett
Caloptilia alchimiella	Nem védett / indifferens
Caloptilia cuculipennella	Nem védett / indifferens
Caloptilia elongella	Nem védett / indifferens
Caloptilia falconipennella	Nem védett / indifferens
Caloptilia fidella	Nem védett / indifferens
Caloptilia fribergensis	Nem védett / indifferens
Calybites hauderi	Nem védett / indifferens
Caloptilia hemidactylella	Nem védett / indifferens
Caloptilia honoratella	Nem védett / indifferens
Caloptilia robustella	Nem védett / indifferens
Caloptilia roscipennella	Nem védett / indifferens
Caloptilia rufipennella	Nem védett / indifferens
Caloptilia semifascia	Nem védett / indifferens
Caloptilia stigmatella	Nem védett / indifferens
Calopus serraticornis	Nem védett / indifferens
Calosirus terminatus	Nem védett / indifferens
Calosoma auropunctatum	Védett
Calosoma inquisitor	Védett
Callisthenes reticulatus	Nem védett / indifferens
Calosoma sycophanta	Védett
Abraxas sylvata	Nem védett / indifferens
Calvia decemguttata	Nem védett / indifferens
Calvia quatuordecimguttata	Nem védett / indifferens
Calvia quindecimguttata	Nem védett / indifferens
Calybites phasianipennella	Nem védett / indifferens
Calybites quadrisignella	Nem védett / indifferens
Calyciphora albodactylus	Nem védett / indifferens
Calyciphora nephelodactyla	Nem védett / indifferens
Calyciphora xanthodactyla	Védett
Calymma communimacula	Nem védett / indifferens
Calyptra thalictri	Nem védett / indifferens
Cameraria ohridella	Nem védett / indifferens
Campaea honoraria	Nem védett / indifferens
Campaea margaritaria	Nem védett / indifferens
Camponotus aethiops	Nem védett / indifferens
Camponotus atricolor	Nem védett / indifferens
Camponotus fallax	Nem védett / indifferens
Camponotus herculeanus	Nem védett / indifferens
Camponotus lateralis	Nem védett / indifferens
Camponotus ligniperda	Nem védett / indifferens
Camponotus piceus	Nem védett / indifferens
Camponotus truncatus	Nem védett / indifferens
Camponotus vagus	Nem védett / indifferens
Camptocercus lilljeborgi	Nem védett / indifferens
Camptocercus rectirostris	Nem védett / indifferens
Camptogramma bilineata	Nem védett / indifferens
Camptopoeum friesei	Nem védett / indifferens
Camptopoeum frontale	Nem védett / indifferens
Camptopus lateralis	Nem védett / indifferens
Camptorhinus simplex	Védett
Camptorhinus statua	Védett
Camptotelus lineolatus	Nem védett / indifferens
Camptozygum aequale	Nem védett / indifferens
Campylomma annulicorne	Nem védett / indifferens
Campylomma verbasci	Nem védett / indifferens
Campyloneura virgula	Nem védett / indifferens
Campylosteira orientalis	Nem védett / indifferens
Campylosteira verna	Nem védett / indifferens
Canariphantes nanus	Nem védett / indifferens
Candidula intersecta	Nem védett / indifferens
Candidula unifasciata	Nem védett / indifferens
Candona balatonica	Nem védett / indifferens
Candona candida	Nem védett / indifferens
Candona improvisa	Nem védett / indifferens
Candona muelleri	Nem védett / indifferens
Candona neglecta	Nem védett / indifferens
Candona sanociensis	Nem védett / indifferens
Candonopsis kingsleii	Nem védett / indifferens
Candonopsis scourfieldi	Nem védett / indifferens
Canephora hirsuta	Nem védett / indifferens
Canis aureus	Nem védett / indifferens
Canis aureus moreoticus	Nem védett / indifferens
Canis lupus	Fokozottan védett
Cantharis annularis	Nem védett / indifferens
Cantharis assimilis	Nem védett / indifferens
Cantharis bicolor	Nem védett / indifferens
Cantharis decipiens	Nem védett / indifferens
Cantharis figurata	Nem védett / indifferens
Cantharis fulvicollis	Nem védett / indifferens
Cantharis fusca	Nem védett / indifferens
Cantharis lateralis	Nem védett / indifferens
Cantharis liburnica	Nem védett / indifferens
Cantharis livida	Nem védett / indifferens
Cordicantharis longicollis	Nem védett / indifferens
Cantharis nigricans	Nem védett / indifferens
Cantharis obscura	Nem védett / indifferens
Cantharis pagana	Nem védett / indifferens
Cantharis pallida	Nem védett / indifferens
Cantharis paludosa	Nem védett / indifferens
Cantharis paradoxa	Nem védett / indifferens
Cantharis pellucida	Nem védett / indifferens
Cantharis pulicaria	Nem védett / indifferens
Cantharis quadripunctata	Nem védett / indifferens
Cantharis rufa	Nem védett / indifferens
Cantharis rustica	Nem védett / indifferens
Cantharis sudetica	Nem védett / indifferens
Canthocamptus microstaphylinus	Nem védett / indifferens
Canthocamptus northumbricus	Nem védett / indifferens
Canthocamptus staphylinus	Nem védett / indifferens
Canthophorus dubius	Nem védett / indifferens
Canthophorus melanopterus	Nem védett / indifferens
Canthophorus mixtus	Nem védett / indifferens
Capnia bifrons	Nem védett / indifferens
Capnia nigra	Nem védett / indifferens
Capnodis tenebrionis	Védett
Capperia britanniodactylus	Nem védett / indifferens
Capperia celeusi	Nem védett / indifferens
Capperia trichodactyla	Nem védett / indifferens
Capreolus capreolus	Nem védett / indifferens
Caprimulgus europaeus	Védett
Caprimulgus europaeus meridionalis	Védett
Capsodes gothicus	Nem védett / indifferens
Capsodes mat	Nem védett / indifferens
Capsus ater	Nem védett / indifferens
Capua vulgana	Nem védett / indifferens
Carabus arvensis	Védett
Carabus arvensis austriae	Védett
Carabus arvensis carpathus	Védett
Carabus auronitens	Védett
Carabus auronitens kraussi	Védett
Carabus cancellatus	Védett
Carabus cancellatus adeptus	Védett
Carabus cancellatus budensis	Védett
Carabus cancellatus durus	Védett
Carabus cancellatus maximus	Védett
Carabus cancellatus muehlfeldi	Védett
Carabus cancellatus soproniensis	Védett
Carabus cancellatus tibiscinus	Védett
Carabus cancellatus ungensis	Védett
Carabus clatratus	Védett
Carabus clatratus auraniensis	Védett
Carabus convexus	Védett
Carabus convexus kiskunensis	Védett
Carabus convexus simplicipennis	Védett
Carabus coriaceus	Védett
Carabus coriaceus praeillyricus	Védett
Carabus coriaceus pseudorugifer	Védett
Carabus coriaceus rugifer	Védett
Carabus germarii	Védett
Carabus germarii exasperatus	Védett
Carabus glabratus	Védett
Carabus granulatus	Védett
Carabus hampei	Fokozottan védett
Carabus hampei ormayi	Fokozottan védett
Carabus hortensis	Védett
Carabus hungaricus	Fokozottan védett
Carabus intricatus	Védett
Carabus irregularis	Védett
Carabus irregularis cephalotes	Védett
Carabus linnaei	Védett
Carabus linnaei transdanubialis	Védett
Carabus marginalis	Védett
Carabus montivagus	Védett
Carabus montivagus blandus	Védett
Carabus nemoralis	Védett
Carabus nodulosus	Fokozottan védett
Carabus obsoletus	Védett
Carabus problematicus	Védett
Carabus problematicus holdhausi	Védett
Carabus scabriusculus	Védett
Carabus scabriusculus lippii	Védett
Carabus scheidleri	Védett
Carabus scheidleri baderlei	Védett
Carabus scheidleri distinguendus	Védett
Carabus scheidleri helleri	Védett
Carabus scheidleri jucundus	Védett
Carabus scheidleri pannonicus	Védett
Carabus scheidleri praescheidleri	Védett
Carabus scheidleri pseudopreyssleri	Védett
Carabus scheidleri vertesensis	Védett
Carabus ullrichii	Védett
Carabus ullrichii baranyensis	Védett
Carabus ullrichii planitiae	Védett
Carabus ullrichii sokolari	Védett
Carabus variolosus	Fokozottan védett
Carabus violaceus	Védett
Carabus violaceus betuliae	Védett
Carabus violaceus pseudoviolaceus	Védett
Carabus violaceus rakosiensis	Védett
Carabus scheidleri zawadzkii	Fokozottan védett
Carabus scheidleri dissimilis	Fokozottan védett
Carabus scheidleri ronayi	Fokozottan védett
Caradrina morpheus	Nem védett / indifferens
Carassius carassius	Nem védett / indifferens
Carassius gibelio	Nem védett / indifferens
Carcharodus alceae	Nem védett / indifferens
Carcharodus floccifera	Nem védett / indifferens
Carcharodus lavatherae	Védett
Carcharodus orientalis	Nem védett / indifferens
Carcina quercana	Nem védett / indifferens
Carcinops pumilio	Nem védett / indifferens
Cardiastethus fasciiventris	Nem védett / indifferens
Cardiocondyla sahlbergi	Nem védett / indifferens
Cardiophorus anticus	Nem védett / indifferens
Cardiophorus asellus	Nem védett / indifferens
Cardiophorus discicollis	Nem védett / indifferens
Cardiophorus dolini	Nem védett / indifferens
Cardiophorus ebeninus	Nem védett / indifferens
Cardiophorus gramineus	Nem védett / indifferens
Cardiophorus maritimus	Nem védett / indifferens
Cardiophorus nigerrimus	Nem védett / indifferens
Cardiophorus ruficollis	Nem védett / indifferens
Cardiophorus vestigialis	Nem védett / indifferens
Metacanthus annulosus	Nem védett / indifferens
Cardoria scutellata	Védett
Carduelis cannabina	Védett
Carduelis carduelis	Védett
Chloris chloris	Védett
Carduelis flammea	Védett
Carduelis flammea cabaret	Védett
Carduelis flavirostris	Védett
Carduelis hornemanni	Védett
Carduelis hornemanni exilipes	Védett
Carduelis spinus	Védett
Carinatodorcadion aethiops	Nem védett / indifferens
Carinatodorcadion fulvum cervae	Védett
Carinatodorcadion fulvum	Fokozottan védett
Carpatolechia aenigma	Nem védett / indifferens
Carpatolechia alburnella	Nem védett / indifferens
Carpatolechia decorella	Nem védett / indifferens
Carpatolechia fugacella	Nem védett / indifferens
Carpatolechia fugitivella	Nem védett / indifferens
Carpatolechia notatella	Nem védett / indifferens
Carpatolechia proximella	Nem védett / indifferens
Carpelimus aceus	Nem védett / indifferens
Carpelimus anthracinus	Nem védett / indifferens
Carpelimus bilineatus	Nem védett / indifferens
Carpelimus corticinus	Nem védett / indifferens
Carpelimus elongatulus	Nem védett / indifferens
Carpelimus erichsoni	Nem védett / indifferens
Carpelimus exiguus	Nem védett / indifferens
Carpelimus foveolatus	Nem védett / indifferens
Carpelimus fuliginosus	Nem védett / indifferens
Carpelimus ganglbaueri	Nem védett / indifferens
Carpelimus gracilis	Nem védett / indifferens
Carpelimus gusarovi	Nem védett / indifferens
Carpelimus heidenreichi	Nem védett / indifferens
Carpelimus impressus	Nem védett / indifferens
Carpelimus lindrothi	Nem védett / indifferens
Carpelimus manchuricus	Nem védett / indifferens
Carpelimus nitidus	Nem védett / indifferens
Carpelimus obesus	Nem védett / indifferens
Carpelimus opacus	Nem védett / indifferens
Carpelimus politus	Nem védett / indifferens
Carpelimus punctatellus	Nem védett / indifferens
Carpelimus pusillus	Nem védett / indifferens
Carpelimus rivularis	Nem védett / indifferens
Carpelimus similis	Nem védett / indifferens
Carpelimus subtilis	Nem védett / indifferens
Carpelimus transversicollis	Nem védett / indifferens
Carphacis striatus	Nem védett / indifferens
Carpocoris fuscispinus	Nem védett / indifferens
Carpocoris mediterraneus	Nem védett / indifferens
Carpocoris pudicus	Nem védett / indifferens
Carpocoris purpureipennis	Nem védett / indifferens
Carpodacus erythrinus	Védett
Carpodacus roseus	Védett
Carpophilus bipustulatus	Nem védett / indifferens
Carpophilus dimidiatus	Nem védett / indifferens
Carpophilus hemipterus	Nem védett / indifferens
Carpophilus marginellus	Nem védett / indifferens
Carpophilus pilosellus	Nem védett / indifferens
Carpophilus quadrisignatus	Nem védett / indifferens
Carpophilus sexpustulatus	Nem védett / indifferens
Carposina berberidella	Nem védett / indifferens
Carposina scirrhosella	Nem védett / indifferens
Carrhotus xanthogramma	Nem védett / indifferens
Carterocephalus palaemon	Nem védett / indifferens
Carterus angustipennis lutshniki	Nem védett / indifferens
Cartodere constricta	Nem védett / indifferens
Cartodere nodifer	Nem védett / indifferens
Carulaspis carueli	Nem védett / indifferens
Carulaspis juniperi	Nem védett / indifferens
Carulaspis visci	Nem védett / indifferens
Carychium minimum	Nem védett / indifferens
Carychium tridentatum	Nem védett / indifferens
Caryocolum alsinella	Nem védett / indifferens
Caryocolum amaurella	Nem védett / indifferens
Caryocolum blandella	Nem védett / indifferens
Caryocolum blandulella	Nem védett / indifferens
Caryocolum cauligenella	Nem védett / indifferens
Caryocolum fischerella	Nem védett / indifferens
Caryocolum huebneri	Nem védett / indifferens
Caryocolum inflativorella	Nem védett / indifferens
Caryocolum junctella	Nem védett / indifferens
Caryocolum leucomelanella	Nem védett / indifferens
Caryocolum leucothoracellum	Nem védett / indifferens
Caryocolum marmoreum	Nem védett / indifferens
Caryocolum petryi	Nem védett / indifferens
Caryocolum proximum	Nem védett / indifferens
Caryocolum trauniella	Nem védett / indifferens
Caryocolum tricolorella	Nem védett / indifferens
Caryocolum vicinella	Nem védett / indifferens
Caryocolum viscariella	Nem védett / indifferens
Caryoscapha limbata	Nem védett / indifferens
Caspiobdella fadejewi	Nem védett / indifferens
Castor canadensis	Nem védett / indifferens
Castor fiber	Védett
Cataclysme riguata	Nem védett / indifferens
Cataclysta lemnata	Nem védett / indifferens
Cataglyphis aenescens	Nem védett / indifferens
Cataglyphis bicolor	Nem védett / indifferens
Cataglyphis nodus	Nem védett / indifferens
Catapion jaffense	Nem védett / indifferens
Catapion meieri	Nem védett / indifferens
Catapion pubescens	Nem védett / indifferens
Catapion seniculus	Nem védett / indifferens
Catarhoe cuculata	Nem védett / indifferens
Catarhoe rubidata	Nem védett / indifferens
Catastia marginea	Nem védett / indifferens
Catephia alchymista	Nem védett / indifferens
Catocala conversa	Védett
Catocala dilecta	Védett
Catocala diversa	Védett
Catocala electa	Nem védett / indifferens
Catocala elocata	Nem védett / indifferens
Catocala fraxini	Védett
Catocala fulminea	Nem védett / indifferens
Catocala hymenaea	Nem védett / indifferens
Catocala nupta	Nem védett / indifferens
Catocala nymphagoga	Nem védett / indifferens
Catocala promissa	Nem védett / indifferens
Catocala puerpera	Nem védett / indifferens
Catocala sponsa	Nem védett / indifferens
Catoplatus carthusianus	Nem védett / indifferens
Catoplatus fabricii	Nem védett / indifferens
Catoplatus horvathi	Nem védett / indifferens
Catoplatus nigriceps	Nem védett / indifferens
Catops chrysomeloides	Nem védett / indifferens
Catops fuliginosus	Nem védett / indifferens
Catops fuscus	Nem védett / indifferens
Catops grandicollis	Nem védett / indifferens
Catops longulus	Nem védett / indifferens
Catops morio	Nem védett / indifferens
Catops neglectus	Nem védett / indifferens
Catops nigricans	Nem védett / indifferens
Catops nigricantoides	Nem védett / indifferens
Catops nigriclavis	Nem védett / indifferens
Catops nigrita	Nem védett / indifferens
Catops picipes	Nem védett / indifferens
Catops subfuscus	Nem védett / indifferens
Catops tristis	Nem védett / indifferens
Catops westi	Nem védett / indifferens
Catopta thrips	Fokozottan védett
Catoptria confusellus	Nem védett / indifferens
Catoptria falsella	Nem védett / indifferens
Catoptria fulgidella	Nem védett / indifferens
Catoptria lythargyrella	Nem védett / indifferens
Catoptria margaritella	Nem védett / indifferens
Catoptria myella	Nem védett / indifferens
Catoptria mytilella	Nem védett / indifferens
Catoptria osthelderi	Nem védett / indifferens
Catoptria permutatellus	Nem védett / indifferens
Catoptria pinella	Nem védett / indifferens
Catoptria verellus	Nem védett / indifferens
Cauchas fibulella	Nem védett / indifferens
Cauchas leucocerella	Nem védett / indifferens
Cauchas reskovitsiella	Nem védett / indifferens
Cauchas rufifrontella	Nem védett / indifferens
Cauchas rufimitrella	Nem védett / indifferens
Cauchas uhrik-meszarosiella	Nem védett / indifferens
Caulastrocecis furfurella	Nem védett / indifferens
Cavernocypris subterranea	Nem védett / indifferens
Cecilioides acicula	Nem védett / indifferens
Cecilioides petitiana	Nem védett / indifferens
Cedestis gysseleniella	Nem védett / indifferens
Cedestis subfasciella	Nem védett / indifferens
Celaena leucostigma	Nem védett / indifferens
Celastrina argiolus	Nem védett / indifferens
Celes variabilis	Védett
Cellariopsis deubeli	Védett
Celonites abbreviatus	Nem védett / indifferens
Celypha capreolana	Nem védett / indifferens
Celypha cespitana	Nem védett / indifferens
Celypha flavipalpana	Nem védett / indifferens
Celypha rufana	Nem védett / indifferens
Celypha rurestrana	Nem védett / indifferens
Celypha striana	Nem védett / indifferens
Celypha woodiana	Nem védett / indifferens
Centricnemus leucogrammus	Nem védett / indifferens
Centrocoris spiniger	Nem védett / indifferens
Centrocoris variegatus	Nem védett / indifferens
Centromerita bicolor	Nem védett / indifferens
Centromerus brevivulvatus	Nem védett / indifferens
Centromerus cavernarum	Nem védett / indifferens
Centromerus incilium	Nem védett / indifferens
Centromerus sellarius	Nem védett / indifferens
Centromerus serratus	Nem védett / indifferens
Centromerus silvicola	Nem védett / indifferens
Centromerus sylvaticus	Nem védett / indifferens
Centroptilum luteolum	Nem védett / indifferens
Pseudocentroptiloides nana	Nem védett / indifferens
Procloeon pennulatum	Nem védett / indifferens
Procloeon pulchrum	Nem védett / indifferens
Bythinus lucifuga	Nem védett / indifferens
Centrotus cornutus	Nem védett / indifferens
Cepaea hortensis	Védett
Cepaea nemoralis	Védett
Cepaea vindobonensis	Nem védett / indifferens
Orophia denisella	Nem védett / indifferens
Orophia ferrugella	Nem védett / indifferens
Orophia sordidella	Nem védett / indifferens
Cephennium affine	Nem védett / indifferens
Cephennium carpathicum	Nem védett / indifferens
Cephennium dariae	Nem védett / indifferens
Cephennium delicatulum	Nem védett / indifferens
Cephennium majus	Nem védett / indifferens
Cephennium paganettii	Nem védett / indifferens
Cephennium slovenicum	Nem védett / indifferens
Cephimallota angusticostella	Nem védett / indifferens
Cepphis advenaria	Nem védett / indifferens
Ceraclea alboguttata	Nem védett / indifferens
Ceraclea annulicornis	Nem védett / indifferens
Ceraclea aurea	Nem védett / indifferens
Ceraclea dissimilis	Nem védett / indifferens
Ceraclea fulva	Nem védett / indifferens
Ceraclea nigronervosa	Védett
Ceraclea riparia	Nem védett / indifferens
Ceraclea senilis	Nem védett / indifferens
Ceraleptus gracilicornis	Nem védett / indifferens
Ceraleptus lividus	Nem védett / indifferens
Ceraleptus obtusus	Nem védett / indifferens
Cerallus rubidus	Nem védett / indifferens
Cerambyx cerdo	Védett
Cerambyx miles	Védett
Cerambyx scopolii	Védett
Cerambyx welensii	Védett
Ceramica pisi	Nem védett / indifferens
Cerapheles terminatus	Nem védett / indifferens
Cerapteryx graminis	Nem védett / indifferens
Cerastis leucographa	Nem védett / indifferens
Cerastis rubricosa	Nem védett / indifferens
Ceratapion armatum	Nem védett / indifferens
Ceratapion austriacum	Nem védett / indifferens
Ceratapion basicorne	Nem védett / indifferens
Ceratapion carduorum	Nem védett / indifferens
Ceratapion cylindricolle	Nem védett / indifferens
Ceratapion gibbirostre	Nem védett / indifferens
Ceratapion onopordi	Nem védett / indifferens
Ceratapion orientale	Nem védett / indifferens
Ceratapion penetrans	Nem védett / indifferens
Ceratapion transsylvanicum	Nem védett / indifferens
Ceratina acuta	Nem védett / indifferens
Ceratina chalcites	Nem védett / indifferens
Ceratina chalybea	Nem védett / indifferens
Ceratina cucurbitina	Nem védett / indifferens
Ceratina cyanea	Nem védett / indifferens
Ceratina gravidula	Nem védett / indifferens
Ceratina nigrolabiata	Nem védett / indifferens
Ceratinella brevipes	Nem védett / indifferens
Ceratinella brevis	Nem védett / indifferens
Ceratinella major	Nem védett / indifferens
Ceratinella scabrosa	Nem védett / indifferens
Ceratocombus coleoptratus	Nem védett / indifferens
Hippodamia notata	Nem védett / indifferens
Hippodamia undecimnotata	Nem védett / indifferens
Cerceris albofasciata	Nem védett / indifferens
Cerceris arenaria	Nem védett / indifferens
Cerceris bicincta	Nem védett / indifferens
Cerceris bracteata	Nem védett / indifferens
Cerceris bupresticida	Nem védett / indifferens
Cerceris circularis	Nem védett / indifferens
Cerceris circularis dacica	Nem védett / indifferens
Cerceris eryngii	Nem védett / indifferens
Cerceris eversmanni	Nem védett / indifferens
Cerceris fimbriata	Nem védett / indifferens
Cerceris flavilabris	Nem védett / indifferens
Cerceris hortivaga	Nem védett / indifferens
Cerceris interrupta	Nem védett / indifferens
Cerceris quadricincta	Nem védett / indifferens
Cerceris quadrifasciata	Nem védett / indifferens
Cerceris quinquefasciata	Nem védett / indifferens
Cerceris rubida	Nem védett / indifferens
Cerceris ruficornis	Nem védett / indifferens
Cerceris rybyensis	Nem védett / indifferens
Cerceris sabulosa	Nem védett / indifferens
Cerceris somotorensis	Nem védett / indifferens
Cerceris stratiotes	Nem védett / indifferens
Cerceris tenuivittata	Nem védett / indifferens
Cerceris tuberculata	Nem védett / indifferens
Cercidia prominens	Nem védett / indifferens
Cercopis arcuata	Nem védett / indifferens
Cercopis sanguinolenta	Nem védett / indifferens
Cercopis vulnerata	Nem védett / indifferens
Cercyon analis	Nem védett / indifferens
Cercyon bifenestratus	Nem védett / indifferens
Cercyon convexiusculus	Nem védett / indifferens
Cercyon granarius	Nem védett / indifferens
Cercyon haemorrhoidalis	Nem védett / indifferens
Cercyon hungaricus	Nem védett / indifferens
Cercyon impressus	Nem védett / indifferens
Cercyon laminatus	Nem védett / indifferens
Cercyon lateralis	Nem védett / indifferens
Cercyon marinus	Nem védett / indifferens
Cercyon melanocephalus	Nem védett / indifferens
Cercyon nigriceps	Nem védett / indifferens
Cercyon obsoletus	Nem védett / indifferens
Cercyon pygmaeus	Nem védett / indifferens
Cercyon quisquilius	Nem védett / indifferens
Cercyon sternalis	Nem védett / indifferens
Cercyon terminatus	Nem védett / indifferens
Cercyon tristis	Nem védett / indifferens
Cercyon unipunctatus	Nem védett / indifferens
Cercyon ustulatus	Nem védett / indifferens
Ceriagrion tenellum	Védett
Ceriana conopsoides	Nem védett / indifferens
Ceriana vespiformis	Nem védett / indifferens
Ceriodaphnia dubia	Nem védett / indifferens
Ceriodaphnia laticaudata	Nem védett / indifferens
Ceriodaphnia megops	Nem védett / indifferens
Ceriodaphnia pulchella	Nem védett / indifferens
Ceriodaphnia quadrangula	Nem védett / indifferens
Ceriodaphnia reticulata	Nem védett / indifferens
Ceriodaphnia rotunda	Nem védett / indifferens
Ceriodaphnia setosa	Nem védett / indifferens
Ceritaxa alluvialis	Nem védett / indifferens
Ceritaxa flavipes	Nem védett / indifferens
Ceritaxa hamoriae	Nem védett / indifferens
Ceritaxa testaceipes	Nem védett / indifferens
Cernuella neglecta	Nem védett / indifferens
Cerococcus cycliger	Nem védett / indifferens
Cerocoma adamovichiana	Nem védett / indifferens
Cerocoma muehlfeldi	Nem védett / indifferens
Cerocoma schaefferi	Nem védett / indifferens
Cerocoma schreberi	Nem védett / indifferens
Ceropales albicincta	Nem védett / indifferens
Ceropales cribrata	Nem védett / indifferens
Ceropales helvetica	Nem védett / indifferens
Ceropales maculata	Nem védett / indifferens
Ceropales pygmaea	Nem védett / indifferens
Ceropales variegata	Nem védett / indifferens
Cerophytum elateroides	Nem védett / indifferens
Certhia brachydactyla	Védett
Certhia familiaris	Védett
Certhia familiaris macrodactyla	Védett
Cerura erminea	Nem védett / indifferens
Cerura vinula	Nem védett / indifferens
Cervus elaphus	Nem védett / indifferens
Cerylon deplanatum	Nem védett / indifferens
Cerylon fagi	Nem védett / indifferens
Cerylon ferrugineum	Nem védett / indifferens
Cerylon histeroides	Nem védett / indifferens
Cetonana laticeps	Nem védett / indifferens
Cetonia aurata	Nem védett / indifferens
Protaetia aeruginosa	Védett
Cettia cetti	Védett
Ceuthonectes hungaricus	Nem védett / indifferens
Ceutorhynchus aeneicollis	Nem védett / indifferens
Ceutorhynchus alliariae	Nem védett / indifferens
Ceutorhynchus arator	Nem védett / indifferens
Ceutorhynchus assimilis	Nem védett / indifferens
Ceutorhynchus atomus	Nem védett / indifferens
Ceutorhynchus barbareae	Nem védett / indifferens
Ceutorhynchus canaliculatus	Nem védett / indifferens
Ceutorhynchus carinatus	Nem védett / indifferens
Ceutorhynchus chalibaeus	Nem védett / indifferens
Ceutorhynchus chlorophanus	Nem védett / indifferens
Ceutorhynchus coarctatus	Nem védett / indifferens
Ceutorhynchus cochleariae	Nem védett / indifferens
Ceutorhynchus constrictus	Nem védett / indifferens
Ceutorhynchus dubius	Nem védett / indifferens
Ceutorhynchus erysimi	Nem védett / indifferens
Ceutorhynchus fallax	Nem védett / indifferens
Ceutorhynchus fulvitarsis	Nem védett / indifferens
Ceutorhynchus gallorhenanus	Nem védett / indifferens
Ceutorhynchus gerhardti	Nem védett / indifferens
Ceutorhynchus griseus	Nem védett / indifferens
Ceutorhynchus hampei	Nem védett / indifferens
Ceutorhynchus hirtulus	Nem védett / indifferens
Ceutorhynchus ignitus	Nem védett / indifferens
Ceutorhynchus inaffectatus	Nem védett / indifferens
Ceutorhynchus interjectus	Nem védett / indifferens
Ceutorhynchus laetus	Nem védett / indifferens
Ceutorhynchus levantinus	Nem védett / indifferens
Ceutorhynchus liliputanus	Nem védett / indifferens
Ceutorhynchus merkli	Nem védett / indifferens
Ceutorhynchus moraviensis	Nem védett / indifferens
Ceutorhynchus nanus	Nem védett / indifferens
Ceutorhynchus napi	Nem védett / indifferens
Ceutorhynchus nigritulus	Nem védett / indifferens
Ceutorhynchus niyazii	Nem védett / indifferens
Ceutorhynchus obstrictus	Nem védett / indifferens
Ceutorhynchus pallidactylus	Nem védett / indifferens
Ceutorhynchus pallipes	Nem védett / indifferens
Ceutorhynchus pandellei	Nem védett / indifferens
Ceutorhynchus parvulus	Nem védett / indifferens
Ceutorhynchus pectoralis	Nem védett / indifferens
Ceutorhynchus pervicax	Nem védett / indifferens
Ceutorhynchus picitarsis	Nem védett / indifferens
Ceutorhynchus posthumus	Nem védett / indifferens
Ceutorhynchus pulvinatus	Nem védett / indifferens
Ceutorhynchus puncticollis	Nem védett / indifferens
Ceutorhynchus pyrrhorhynchus	Nem védett / indifferens
Ceutorhynchus querceti	Nem védett / indifferens
Ceutorhynchus rapae	Nem védett / indifferens
Ceutorhynchus rhenanus	Nem védett / indifferens
Ceutorhynchus roberti	Nem védett / indifferens
Ceutorhynchus scapularis	Nem védett / indifferens
Ceutorhynchus scrobicollis	Nem védett / indifferens
Ceutorhynchus similis	Nem védett / indifferens
Ceutorhynchus sisymbrii	Nem védett / indifferens
Ceutorhynchus sophiae	Nem védett / indifferens
Ceutorhynchus strejceki	Nem védett / indifferens
Ceutorhynchus striatellus	Nem védett / indifferens
Ceutorhynchus sulcatus	Nem védett / indifferens
Ceutorhynchus sulcicollis	Nem védett / indifferens
Ceutorhynchus syrites	Nem védett / indifferens
Ceutorhynchus thlaspi	Nem védett / indifferens
Ceutorhynchus turbatus	Nem védett / indifferens
Ceutorhynchus typhae	Nem védett / indifferens
Ceutorhynchus unguicularis	Nem védett / indifferens
Ceutorhynchus viridanus	Nem védett / indifferens
Ceutorhynchus wagneri	Nem védett / indifferens
Chaetabraeus globulus	Nem védett / indifferens
Chaetarthria seminulum	Nem védett / indifferens
Chaetida longicornis	Nem védett / indifferens
Chaetococcus phragmitis	Nem védett / indifferens
Chaetococcus sulcii	Nem védett / indifferens
Chaetophora spinosa	Nem védett / indifferens
Chaetopteroplia segetum	Nem védett / indifferens
Chaetopteryx fusca	Nem védett / indifferens
Chaetopteryx major	Nem védett / indifferens
Chaetopteryx rugulosa	Fokozottan védett
Chaetopteryx schmidi	Nem védett / indifferens
Chaetopteryx schmidi mecsekensis	Fokozottan védett
Alburnus chalcoides	Nem védett / indifferens
Alburnus chalcoides mento	Védett
Chalcionellus amoenus	Nem védett / indifferens
Chalcionellus decemstriatus	Nem védett / indifferens
Lestes viridis	Nem védett / indifferens
Chalcophora mariana	Védett
Chalcosyrphus curvipes	Nem védett / indifferens
Chalcosyrphus eunotus	Nem védett / indifferens
Chalcosyrphus femoratus	Nem védett / indifferens
Chalcosyrphus nemorum	Nem védett / indifferens
Chalcosyrphus piger	Nem védett / indifferens
Chalicodoma ericetorum	Nem védett / indifferens
Chalicodoma hungaricum	Nem védett / indifferens
Chalicodoma lefebvrei	Nem védett / indifferens
Chalicodoma parietinum	Nem védett / indifferens
Chalybion femoratum	Nem védett / indifferens
Chamaesphecia aerifrons	Nem védett / indifferens
Chamaesphecia alysoniformis	Nem védett / indifferens
Chamaesphecia anatolica	Nem védett / indifferens
Chamaesphecia annellata	Nem védett / indifferens
Chamaesphecia astatiformis	Nem védett / indifferens
Chamaesphecia bibioniformis	Nem védett / indifferens
Chamaesphecia chalciformis	Nem védett / indifferens
Chamaesphecia doleriformis colpiformis	Védett
Chamaesphecia crassicornis	Nem védett / indifferens
Chamaesphecia dumonti	Nem védett / indifferens
Chamaesphecia euceraeformis	Nem védett / indifferens
Chamaesphecia hungarica	Védett
Chamaesphecia leucopsiformis	Nem védett / indifferens
Chamaesphecia masariformis	Nem védett / indifferens
Chamaesphecia nigrifrons	Nem védett / indifferens
Chamaesphecia palustris	Védett
Chamaesphecia tenthrediniformis	Nem védett / indifferens
Chamaesyrphus scaevoides	Nem védett / indifferens
Charadrius alexandrinus	Fokozottan védett
Charadrius dubius	Védett
Charadrius hiaticula	Védett
Charadrius hiaticula tundrae	Védett
Charadrius leschenaultii	Védett
Charadrius leschenaultii columbinus	Védett
Charadrius morinellus	Védett
Charadrius vociferus	Védett
Charagochilus gyllenhalii	Nem védett / indifferens
Charagochilus weberi	Nem védett / indifferens
Charanyca trigrammica	Nem védett / indifferens
Chariaspilates formosaria	Védett
Kemtrognophos ambiguata	Védett
Neognophina intermedia	Védett
Neognophina intermedia budensis	Védett
Charissa obscurata	Nem védett / indifferens
Costignophos pullata	Védett
Euchrognophos variegata	Védett
Charopus concolor	Nem védett / indifferens
Charopus flavipes	Nem védett / indifferens
Charopus graminicola	Nem védett / indifferens
Charopus philoctetes	Nem védett / indifferens
Charopus thoracicus	Nem védett / indifferens
Chartoscirta cincta	Nem védett / indifferens
Chartoscirta cocksii	Nem védett / indifferens
Chartoscirta elegantula	Nem védett / indifferens
Chazara briseis	Védett
Cheilosia aerea	Nem védett / indifferens
Cheilosia albipila	Nem védett / indifferens
Cheilosia albitarsis	Nem védett / indifferens
Cheilosia antiqua	Nem védett / indifferens
Cheilosia barbata	Nem védett / indifferens
Cheilosia bergenstammi	Nem védett / indifferens
Cheilosia brachysoma	Nem védett / indifferens
Cheilosia caerulescens	Nem védett / indifferens
Cheilosia canicularis	Nem védett / indifferens
Cheilosia carbonaria	Nem védett / indifferens
Cheilosia chloris	Nem védett / indifferens
Cheilosia chrysocoma	Nem védett / indifferens
Cheilosia cynocephala	Nem védett / indifferens
Cheilosia fasciata	Nem védett / indifferens
Cheilosia flavipes	Nem védett / indifferens
Cheilosia fraterna	Nem védett / indifferens
Cheilosia frontalis	Nem védett / indifferens
Cheilosia gigantea	Nem védett / indifferens
Cheilosia griseifacies	Nem védett / indifferens
Cheilosia grossa	Nem védett / indifferens
Cheilosia hypena	Nem védett / indifferens
Cheilosia illustrata	Nem védett / indifferens
Cheilosia impressa	Nem védett / indifferens
Cheilosia insignis	Nem védett / indifferens
Cheilosia intonsa	Nem védett / indifferens
Cheilosia lasiopa	Nem védett / indifferens
Cheilosia laticornis	Nem védett / indifferens
Cheilosia lenis	Nem védett / indifferens
Cheilosia lenta	Nem védett / indifferens
Cheilosia melanopa	Nem védett / indifferens
Cheilosia melanura	Nem védett / indifferens
Cheilosia mutabilis	Nem védett / indifferens
Cheilosia nebulosa	Nem védett / indifferens
Cheilosia nigripes	Nem védett / indifferens
Cheilosia orthotricha	Nem védett / indifferens
Cheilosia pagana	Nem védett / indifferens
Cheilosia praecox	Nem védett / indifferens
Cheilosia proxima	Nem védett / indifferens
Cheilosia pubera	Nem védett / indifferens
Cheilosia ranunculi	Nem védett / indifferens
Cheilosia schnabli	Nem védett / indifferens
Cheilosia scutellata	Nem védett / indifferens
Cheilosia semifasciata	Nem védett / indifferens
Cheilosia soror	Nem védett / indifferens
Cheilosia subpictipennis	Nem védett / indifferens
Cheilosia variabilis	Nem védett / indifferens
Cheilosia velutina	Nem védett / indifferens
Cheilosia vernalis	Nem védett / indifferens
Cheilosia vicina	Nem védett / indifferens
Cheilosia vulpina	Nem védett / indifferens
Cheiracanthium angulitarse	Nem védett / indifferens
Cheiracanthium effossum	Nem védett / indifferens
Cheiracanthium elegans	Nem védett / indifferens
Cheiracanthium erraticum	Nem védett / indifferens
Cheiracanthium mildei	Nem védett / indifferens
Cheiracanthium montanum	Nem védett / indifferens
Cheiracanthium oncognathum	Nem védett / indifferens
Cheiracanthium pennyi	Nem védett / indifferens
Cheiracanthium punctorium	Nem védett / indifferens
Cheiracanthium rupestre	Nem védett / indifferens
Cheiracanthium virescens	Nem védett / indifferens
Cheironitis ungaricus	Védett
Chelis maculosa	Nem védett / indifferens
Chelostoma campanularum	Nem védett / indifferens
Chelostoma distinctum	Nem védett / indifferens
Chelostoma emarginatum	Nem védett / indifferens
Chelostoma florisomne	Nem védett / indifferens
Chelostoma foveolatum	Nem védett / indifferens
Chelostoma handlirschi	Nem védett / indifferens
Chelostoma rapunculi	Nem védett / indifferens
Chelostoma styriacum	Nem védett / indifferens
Chelostoma ventrale	Nem védett / indifferens
Bythinus bituberculatum	Nem védett / indifferens
Chersotis cuprea	Védett
Chersotis fimbriola	Védett
Chersotis fimbriola baloghi	Védett
Chersotis margaritacea	Nem védett / indifferens
Chersotis multangula	Nem védett / indifferens
Chersotis rectangula	Nem védett / indifferens
Chesias legatella	Védett
Chesias rufata	Védett
Chettusia gregaria	Fokozottan védett
Chettusia leucura	Védett
Cheumatopsyche lepida	Nem védett / indifferens
Chevrolatia egregia	Nem védett / indifferens
Chiasmia clathrata	Nem védett / indifferens
Chiasmus conspurcatus	Nem védett / indifferens
Chilacis typhae	Nem védett / indifferens
Chilo phragmitella	Nem védett / indifferens
Chilocorus bipustulatus	Nem védett / indifferens
Chilocorus renipustulatus	Nem védett / indifferens
Chilodes maritima	Nem védett / indifferens
Chilomorpha longitarsis	Nem védett / indifferens
Chilopselaphus balneariellus podolicus	Nem védett / indifferens
Chilopselaphus fallax	Nem védett / indifferens
Chilopselaphus lagopellus	Nem védett / indifferens
Chilothorax distinctus	Nem védett / indifferens
Chilothorax melanostictus	Nem védett / indifferens
Chilothorax paykulli	Nem védett / indifferens
Chilothorax pictus	Nem védett / indifferens
Chionaspis austriaca	Nem védett / indifferens
Chionaspis lepineyi	Nem védett / indifferens
Chionaspis salicis	Nem védett / indifferens
Chionodes distinctella	Nem védett / indifferens
Chionodes electella	Nem védett / indifferens
Chionodes fumatella	Nem védett / indifferens
Chionodes ignorantella	Nem védett / indifferens
Chionodes luctuella	Nem védett / indifferens
Chionodes lugubrella	Nem védett / indifferens
Chionodes tragicella	Nem védett / indifferens
Chionodes viduella	Nem védett / indifferens
Chirocephalus carnuntanus	Nem védett / indifferens
Chirocephalus diaphanus	Nem védett / indifferens
Chlaenius festivus	Védett
Chlaeniellus nigricornis	Nem védett / indifferens
Chlaeniellus nitidulus	Nem védett / indifferens
Chlaenius spoliatus	Nem védett / indifferens
Agostenus sulcicollis	Védett
Chlaeniellus terminatus	Nem védett / indifferens
Chlaeniellus tibialis	Nem védett / indifferens
Chlaeniellus tristis	Nem védett / indifferens
Chlaeniellus vestitus	Nem védett / indifferens
Chlamydatus evanescens	Nem védett / indifferens
Chlamydatus pulicarius	Nem védett / indifferens
Chlamydatus pullus	Nem védett / indifferens
Chlamydatus saltitans	Nem védett / indifferens
Chlidonias hybridus	Fokozottan védett
Chlidonias leucopterus	Fokozottan védett
Chlidonias niger	Fokozottan védett
Chloantha hyperici	Nem védett / indifferens
Chloothea zonata	Nem védett / indifferens
Chlorion magnificum	Nem védett / indifferens
Chloriona clavata	Nem védett / indifferens
Chloriona glaucescens	Nem védett / indifferens
Chloriona smaragdula	Nem védett / indifferens
Chloriona unicolor	Nem védett / indifferens
Chloriona vasconica	Nem védett / indifferens
Chlorionidea flava	Nem védett / indifferens
Chlorissa cloraria	Nem védett / indifferens
Chlorissa viridata	Nem védett / indifferens
Chlorita paolii	Nem védett / indifferens
Chlorita tessellata	Nem védett / indifferens
Chlorita viridula	Nem védett / indifferens
Dysstroma citrata	Nem védett / indifferens
Chloroclysta miata	Nem védett / indifferens
Chloroclysta siterata	Nem védett / indifferens
Dysstroma truncata	Nem védett / indifferens
Chloroclystis v-ata	Nem védett / indifferens
Chlorochroa juniperina	Nem védett / indifferens
Chlorochroa pinicola	Nem védett / indifferens
Chloroperla tripunctata	Nem védett / indifferens
Chlorophanus excisus	Nem védett / indifferens
Chlorophanus graminicola	Nem védett / indifferens
Chlorophanus pollinosus	Nem védett / indifferens
Chlorophanus viridis	Nem védett / indifferens
Chlorophanus viridis balcanicus	Nem védett / indifferens
Chlorophorus figuratus	Nem védett / indifferens
Chlorophorus herbstii	Nem védett / indifferens
Chlorophorus hungaricus	Védett
Chlorophorus sartor	Nem védett / indifferens
Chlorophorus trifasciatus	Nem védett / indifferens
Chlorophorus varius	Nem védett / indifferens
Pulvinaria floccifera	Nem védett / indifferens
Chnaurococcus danzigae	Nem védett / indifferens
Chnaurococcus parvus	Nem védett / indifferens
Chnaurococcus subterraneus	Nem védett / indifferens
Choleva angustata	Nem védett / indifferens
Choleva cisteloides	Nem védett / indifferens
Choleva elongata	Nem védett / indifferens
Choleva glauca	Nem védett / indifferens
Choleva oblonga	Nem védett / indifferens
Choleva oresitropha	Nem védett / indifferens
Choleva paskoviensis	Nem védett / indifferens
Choleva reitteri	Nem védett / indifferens
Choleva spadicea	Nem védett / indifferens
Choleva sturmi	Nem védett / indifferens
Chondrina arcadica	Nem védett / indifferens
Chondrina arcadica clienta	Nem védett / indifferens
Chondrosoma fiduciaria	Fokozottan védett
Chondrostoma nasus	Nem védett / indifferens
Chondrula tridens	Nem védett / indifferens
Choneiulus palmatus	Nem védett / indifferens
Chonostropheus tristis	Nem védett / indifferens
Choragus horni	Nem védett / indifferens
Choragus sheppardi	Nem védett / indifferens
Chordeuma sylvestre	Nem védett / indifferens
Choreutis nemorana	Nem védett / indifferens
Choreutis pariana	Nem védett / indifferens
Choristoneura diversana	Nem védett / indifferens
Choristoneura hebenstreitella	Nem védett / indifferens
Choristoneura murinana	Nem védett / indifferens
Chorizococcus rostrellum	Nem védett / indifferens
Chorizococcus viktorina	Nem védett / indifferens
Chorosoma gracile	Nem védett / indifferens
Chorosoma schillingii	Nem védett / indifferens
Choroterpes picteti	Nem védett / indifferens
Chorthippus albomarginatus	Nem védett / indifferens
Chorthippus dichrous	Nem védett / indifferens
Chorthippus dorsatus	Nem védett / indifferens
Chorthippus loratus	Nem védett / indifferens
Chorthippus alliaceus	Nem védett / indifferens
Chorthippus montanus	Nem védett / indifferens
Chortinaspis subterraneus	Nem védett / indifferens
Chortodes extrema	Nem védett / indifferens
Chortodes fluxa	Nem védett / indifferens
Chortodes morrisii	Nem védett / indifferens
Chortodes pygmina	Nem védett / indifferens
Chrysanthia nigricornis	Nem védett / indifferens
Chrysanthia viridissima	Nem védett / indifferens
Chrysidea pumila	Nem védett / indifferens
Chrysis albanica	Nem védett / indifferens
Chrysis angustifrons	Nem védett / indifferens
Chrysis angustula	Nem védett / indifferens
Chrysis brevitarsis	Nem védett / indifferens
Chrysis cerastes	Nem védett / indifferens
Chrysis chrysoprasina	Nem védett / indifferens
Chrysis cingulicornis	Nem védett / indifferens
Chrysis coeruleiventris	Nem védett / indifferens
Chrysis comparata	Nem védett / indifferens
Chrysis compta	Nem védett / indifferens
Chrysis distincta	Nem védett / indifferens
Chrysis fasciata	Nem védett / indifferens
Chrysis fulgida	Nem védett / indifferens
Chrysis germari	Nem védett / indifferens
Chrysis gracillima	Nem védett / indifferens
Chrysis graelsii	Nem védett / indifferens
Chrysis grohmanni	Nem védett / indifferens
Chrysis ignita	Nem védett / indifferens
Chrysis inaequalis	Nem védett / indifferens
Chrysis indigotea	Nem védett / indifferens
Chrysis insperata	Nem védett / indifferens
Chrysis interjecta	Nem védett / indifferens
Chrysis leachii	Nem védett / indifferens
Chrysis longula	Nem védett / indifferens
Chrysis marginata	Nem védett / indifferens
Chrysis millenaris	Nem védett / indifferens
Chrysis phryne	Nem védett / indifferens
Chrysis pseudobrevitarsis	Nem védett / indifferens
Chrysis pulchella	Nem védett / indifferens
Chrysis purpurata	Nem védett / indifferens
Chrysis ragusae	Nem védett / indifferens
Chrysis ramburi	Nem védett / indifferens
Chrysis ruddii	Nem védett / indifferens
Chrysis rufitarsis	Nem védett / indifferens
Chrysis rutilans	Nem védett / indifferens
Chrysis rutiliventris	Nem védett / indifferens
Chrysis scutellaris	Nem védett / indifferens
Chrysis semicincta	Nem védett / indifferens
Chrysis splendidula	Nem védett / indifferens
Chrysis subsinuata	Nem védett / indifferens
Chrysis succincta	Nem védett / indifferens
Chrysis varidens	Nem védett / indifferens
Chrysis variegata	Nem védett / indifferens
Chrysis viridula	Nem védett / indifferens
Chrysis viridula cylindrica	Nem védett / indifferens
Chrysobothris affinis	Nem védett / indifferens
Chrysobothris chrysostigma	Nem védett / indifferens
Chrysobothris igniventris	Nem védett / indifferens
Euthystira brachyptera	Nem védett / indifferens
Chrysochraon dispar	Nem védett / indifferens
Chrysoclista lathamella	Nem védett / indifferens
Chrysoclista linneella	Nem védett / indifferens
Chrysocrambus craterella	Nem védett / indifferens
Chrysocrambus linetella	Nem védett / indifferens
Chrysodeixis chalcites	Nem védett / indifferens
Chrysoesthia drurella	Nem védett / indifferens
Chrysoesthia eppelsheimi	Nem védett / indifferens
Chrysoesthia sexguttella	Nem védett / indifferens
Chrysogaster cemiteriorum	Nem védett / indifferens
Chrysogaster solstitialis	Nem védett / indifferens
Chrysopa abbreviata	Nem védett / indifferens
Chrysopa commata	Nem védett / indifferens
Chrysopa dorsalis	Nem védett / indifferens
Chrysopa formosa	Nem védett / indifferens
Chrysopa hungarica	Nem védett / indifferens
Chrysopa nigricostata	Nem védett / indifferens
Chrysopa pallens	Nem védett / indifferens
Chrysopa perla	Nem védett / indifferens
Chrysopa phyllochroma	Nem védett / indifferens
Chrysopa viridana	Nem védett / indifferens
Chrysopa walkeri	Nem védett / indifferens
Chrysoperla carnea	Nem védett / indifferens
Chrysoperla lucasina	Nem védett / indifferens
Chrysoperla mediterranea	Nem védett / indifferens
Chrysoperla renoni	Nem védett / indifferens
Chrysops caecutiens	Nem védett / indifferens
Chrysops flavipes	Nem védett / indifferens
Chrysops italicus	Nem védett / indifferens
Chrysops melicharii	Nem védett / indifferens
Chrysops parallelogrammus	Nem védett / indifferens
Chrysops relictus	Nem védett / indifferens
Chrysops rufipes	Nem védett / indifferens
Chrysops viduatus	Nem védett / indifferens
Chrysoteuchia culmella	Nem védett / indifferens
Chrysotoxum arcuatum	Nem védett / indifferens
Chrysotoxum bicinctum	Nem védett / indifferens
Chrysotoxum cautum	Nem védett / indifferens
Chrysotoxum elegans	Nem védett / indifferens
Chrysotoxum fasciatum	Nem védett / indifferens
Chrysotoxum fasciolatum	Nem védett / indifferens
Chrysotoxum intermedium	Nem védett / indifferens
Chrysotoxum lineare	Nem védett / indifferens
Chrysotoxum octomaculatum	Nem védett / indifferens
Chrysotoxum vernale	Nem védett / indifferens
Chrysotoxum verralli	Nem védett / indifferens
Chrysotropia ciliata	Nem védett / indifferens
Chrysso nordica	Nem védett / indifferens
Chrysura austriaca	Nem védett / indifferens
Chrysura cuprea	Nem védett / indifferens
Chrysura dichroa	Nem védett / indifferens
Chrysura dichroa socia	Nem védett / indifferens
Chrysura hirsuta	Nem védett / indifferens
Chrysura hybrida	Nem védett / indifferens
Chrysura ignifrons	Nem védett / indifferens
Chrysura laodamia	Nem védett / indifferens
Chrysura radians	Nem védett / indifferens
Chrysura rufiventris	Nem védett / indifferens
Chrysura simplex	Nem védett / indifferens
Chrysura trimaculata	Nem védett / indifferens
Chydorus gibbus	Nem védett / indifferens
Chydorus latus	Nem védett / indifferens
Chydorus ovalis	Nem védett / indifferens
Chydorus piger	Nem védett / indifferens
Chydorus sphaericus	Nem védett / indifferens
Cibiniulus phlepsii	Nem védett / indifferens
Cicada orni	Védett
Cicadella viridis	Nem védett / indifferens
Cicadetta montana	Nem védett / indifferens
Cicadetta transsylvanica	Nem védett / indifferens
Cicadivetta tibialis	Nem védett / indifferens
Cicadula albingensis	Nem védett / indifferens
Cicadula flori	Nem védett / indifferens
Cicadula frontalis	Nem védett / indifferens
Cicadula persimilis	Nem védett / indifferens
Cicadula placida	Nem védett / indifferens
Cicadula quadrinotata	Nem védett / indifferens
Cicadula quinquenotata	Nem védett / indifferens
Cylindera arenaria	Védett
Cylindera arenaria viennensis	Nem védett / indifferens
Cicindela campestris	Védett
Cylindera germanica	Védett
Cicindela hybrida	Védett
Lophyridia littoralis	Védett
Lophyridia littoralis nemoralis	Nem védett / indifferens
Cicindela soluta	Védett
Cicindela soluta pannonica	Nem védett / indifferens
Cicindela sylvicola	Védett
Cicindela hybrida transversalis	Védett
Synchita undata	Nem védett / indifferens
Synchita variegata	Nem védett / indifferens
Ciconia ciconia	Fokozottan védett
Ciconia nigra	Fokozottan védett
Cicurina cicur	Nem védett / indifferens
Cidaria fulvata	Nem védett / indifferens
Cidnopus pilosus	Nem védett / indifferens
Cidnopus platiai	Nem védett / indifferens
Cidnopus ruzenae	Nem védett / indifferens
Cilea silphoides	Nem védett / indifferens
Cilix glaucata	Nem védett / indifferens
Cimberis attelaboides	Nem védett / indifferens
Cimex dissimilis	Nem védett / indifferens
Cimex lectularius	Nem védett / indifferens
Cinclus cinclus	Fokozottan védett
Cinclus cinclus aquaticus	Fokozottan védett
Cionellus gibbifrons	Nem védett / indifferens
Cionus alauda	Nem védett / indifferens
Cionus clairvillei	Nem védett / indifferens
Cionus ganglbaueri	Nem védett / indifferens
Cionus gebleri	Nem védett / indifferens
Cionus hortulanus	Nem védett / indifferens
Cionus leonhardi	Nem védett / indifferens
Cionus longicollis	Nem védett / indifferens
Cionus longicollis montanus	Nem védett / indifferens
Cionus nigritarsis	Nem védett / indifferens
Cionus olens	Nem védett / indifferens
Cionus olivieri	Nem védett / indifferens
Cionus pulverosus	Nem védett / indifferens
Cionus scrophulariae	Nem védett / indifferens
Cionus thapsus	Nem védett / indifferens
Cionus tuberculosus	Nem védett / indifferens
Cionus ungulatus	Nem védett / indifferens
Circaetus gallicus	Fokozottan védett
Circus aeruginosus	Védett
Circus cyaneus	Védett
Circus macrourus	Fokozottan védett
Circus pygargus	Fokozottan védett
Cirrorhynchus arrogans	Nem védett / indifferens
Cirrorhynchus kelecsenyi	Nem védett / indifferens
Cis alter	Nem védett / indifferens
Cis bidentatus	Nem védett / indifferens
Cis boleti	Nem védett / indifferens
Cis castaneus	Nem védett / indifferens
Cis comptus	Nem védett / indifferens
Cis dentatus	Nem védett / indifferens
Cis fagi	Nem védett / indifferens
Cis fissicollis	Nem védett / indifferens
Cis fissicornis	Nem védett / indifferens
Cis glabratus	Nem védett / indifferens
Cis hispidus	Nem védett / indifferens
Cis jacquemartii	Nem védett / indifferens
Cis lineatocribratus	Nem védett / indifferens
Cis micans	Nem védett / indifferens
Cis punctulatus	Nem védett / indifferens
Cis rugulosus	Nem védett / indifferens
Cis setiger	Nem védett / indifferens
Cis striatulus	Nem védett / indifferens
Cixidia marginicollis	Nem védett / indifferens
Cixidia parnassia	Nem védett / indifferens
Cixius cunicularius	Nem védett / indifferens
Cixius distinguendus	Nem védett / indifferens
Cixius dubius	Nem védett / indifferens
Cixius granulatus	Nem védett / indifferens
Cixius haupti	Nem védett / indifferens
Cixius heydenii	Nem védett / indifferens
Cixius nervosus	Nem védett / indifferens
Cixius pallipes	Nem védett / indifferens
Cixius similis	Nem védett / indifferens
Cixius simplex	Nem védett / indifferens
Cixius stigmaticus	Nem védett / indifferens
Clambus armadillo	Nem védett / indifferens
Clambus dux	Nem védett / indifferens
Clambus evae	Nem védett / indifferens
Clambus minutus	Nem védett / indifferens
Clambus nigrellus	Nem védett / indifferens
Clambus pallidulus	Nem védett / indifferens
Clambus pilosellus	Nem védett / indifferens
Clambus pubescens	Nem védett / indifferens
Clambus punctulum	Nem védett / indifferens
Clangula hyemalis	Fokozottan védett
Clanoptilus affinis	Nem védett / indifferens
Clanoptilus ambiguus	Nem védett / indifferens
Clanoptilus elegans	Nem védett / indifferens
Clanoptilus geniculatus	Nem védett / indifferens
Clanoptilus marginellus	Nem védett / indifferens
Clanoptilus spinipennis	Nem védett / indifferens
Clanoptilus strangulatus	Nem védett / indifferens
Clarias gariepinus	Nem védett / indifferens
Clausilia cruciata	Nem védett / indifferens
Clausilia dubia	Nem védett / indifferens
Clausilia dubia vindobonensis	Nem védett / indifferens
Clausilia pumila	Nem védett / indifferens
Clausilia rugosa	Nem védett / indifferens
Clausilia rugosa parvula	Nem védett / indifferens
Claviger longicornis	Nem védett / indifferens
Claviger nitidus	Nem védett / indifferens
Claviger testaceus	Nem védett / indifferens
Clemmus troglodytes	Nem védett / indifferens
Cleoceris scoriacea	Nem védett / indifferens
Cleonis pigra	Nem védett / indifferens
Aulacobaris distinctus	Nem védett / indifferens
Cleopomiarus graminis	Nem védett / indifferens
Cleopomiarus longirostris	Nem védett / indifferens
Cleopomiarus medius	Nem védett / indifferens
Cleopomiarus micros	Nem védett / indifferens
Cleopomiarus plantarum	Nem védett / indifferens
Cleopus pulchellus	Nem védett / indifferens
Cleopus solani	Nem védett / indifferens
Cleora cinctaria	Nem védett / indifferens
Cleorodes lichenaria	Nem védett / indifferens
Clepsis consimilana	Nem védett / indifferens
Clepsis pallidana	Nem védett / indifferens
Clepsis rolandriana	Nem védett / indifferens
Clepsis rurinana	Nem védett / indifferens
Clepsis senecionana	Nem védett / indifferens
Clepsis spectrana	Nem védett / indifferens
Cleptes aerosus	Nem védett / indifferens
Cleptes consimilis	Nem védett / indifferens
Cleptes elegans	Nem védett / indifferens
Cleptes ignitus	Nem védett / indifferens
Cleptes mocsary	Nem védett / indifferens
Cleptes nitidulus	Nem védett / indifferens
Cleptes orientalis	Nem védett / indifferens
Cleptes pallipes	Nem védett / indifferens
Cleptes putoni	Nem védett / indifferens
Cleptes saussurei	Nem védett / indifferens
Cleptes scutellaris	Nem védett / indifferens
Cleptes semiauratus	Nem védett / indifferens
Clerus mutillarius	Nem védett / indifferens
Clethrionomys glareolus	Nem védett / indifferens
Clinopodes flavidus	Nem védett / indifferens
Clitostethus arcuatus	Nem védett / indifferens
Clivina collaris	Nem védett / indifferens
Clivina fossor	Nem védett / indifferens
Clivina ypsilon	Nem védett / indifferens
Cloeon dipterum	Nem védett / indifferens
Cloeon simile	Nem védett / indifferens
Clostera anachoreta	Nem védett / indifferens
Clostera anastomosis	Nem védett / indifferens
Clostera curtula	Nem védett / indifferens
Clostera pigra	Nem védett / indifferens
Calocoris affinis	Nem védett / indifferens
Calocoris alpestris	Nem védett / indifferens
Closterotomus biclavatus	Nem védett / indifferens
Closterotomus fulvomaculatus	Nem védett / indifferens
Closterotomus norwegicus	Nem védett / indifferens
Calocoris roseomaculatus	Nem védett / indifferens
Clubiona alpicola	Nem védett / indifferens
Clubiona brevipes	Nem védett / indifferens
Clubiona caerulescens	Nem védett / indifferens
Clubiona comta	Nem védett / indifferens
Clubiona corticalis concolor	Nem védett / indifferens
Clubiona diversa	Nem védett / indifferens
Clubiona frutetorum	Nem védett / indifferens
Clubiona genevensis	Nem védett / indifferens
Clubiona germanica	Nem védett / indifferens
Clubiona juvenis	Nem védett / indifferens
Clubiona leucaspis	Nem védett / indifferens
Clubiona lutescens	Nem védett / indifferens
Clubiona marmorata	Nem védett / indifferens
Clubiona pallidula	Nem védett / indifferens
Clubiona phragmitis	Nem védett / indifferens
Clubiona pseudoneglecta	Nem védett / indifferens
Clubiona reclusa	Nem védett / indifferens
Clubiona rosserae	Nem védett / indifferens
Clubiona similis	Nem védett / indifferens
Clubiona stagnatilis	Nem védett / indifferens
Clubiona subtilis	Nem védett / indifferens
Clubiona terrestris	Nem védett / indifferens
Clubiona trivialis	Nem védett / indifferens
Clypastrea brunnea	Nem védett / indifferens
Clypastrea orientalis	Nem védett / indifferens
Clypastrea pusilla	Nem védett / indifferens
Clytus arietis	Nem védett / indifferens
Clytus lama	Nem védett / indifferens
Clytus rhamni	Nem védett / indifferens
Clytus tropicus	Védett
Cnaemidophorus rhododactyla	Nem védett / indifferens
Cnemeplatia atropos	Nem védett / indifferens
Cnephasia abrasana	Nem védett / indifferens
Cnephasia alticolana	Nem védett / indifferens
Cnephasia asseclana	Nem védett / indifferens
Cnephasia chrysantheana	Nem védett / indifferens
Cnephasia communana	Nem védett / indifferens
Cnephasia cupressivorana	Nem védett / indifferens
Cnephasia ecullyana	Nem védett / indifferens
Cnephasia genitalana	Nem védett / indifferens
Cnephasia incertana	Nem védett / indifferens
Cnephasia oxyacanthana	Nem védett / indifferens
Cnephasia pasiuana	Nem védett / indifferens
Cnephasia stephensiana	Nem védett / indifferens
Cobitis elongatoides	Védett
Coccidohystrix artemisiae	Nem védett / indifferens
Oxynychus rufa	Nem védett / indifferens
Oxynychus scutellata	Nem védett / indifferens
Coccinella hieroglyphica	Nem védett / indifferens
Coccinella magnifica	Nem védett / indifferens
Coccinella quinquepunctata	Nem védett / indifferens
Coccinella saucerottii	Nem védett / indifferens
Coccinella septempunctata	Nem védett / indifferens
Coccinella undecimpunctata	Nem védett / indifferens
Coccinella undecimpunctata tripunctata	Nem védett / indifferens
Anisosticta quatuordecimpustulata	Nem védett / indifferens
Anisosticta sinuatomarginata	Nem védett / indifferens
Coccothraustes coccothraustes	Védett
Coccotrypes dactyliperda	Nem védett / indifferens
Coccura comari	Nem védett / indifferens
Blastesthia turionella	Nem védett / indifferens
Cochlicopa lubrica	Nem védett / indifferens
Cochlicopa lubricella	Nem védett / indifferens
Cochlicopa nitens	Nem védett / indifferens
Cochlodina cerata	Védett
Cochlodina fimbriata	Nem védett / indifferens
Cochlodina laminata	Nem védett / indifferens
Cochlodina orthostoma	Védett
Cochylidia heydeniana	Nem védett / indifferens
Cochylidia implicitana	Nem védett / indifferens
Cochylidia moguntiana	Nem védett / indifferens
Cochylidia richteriana	Nem védett / indifferens
Cochylidia rupicola	Nem védett / indifferens
Cochylidia subroseana	Nem védett / indifferens
Cochylimorpha alternana	Nem védett / indifferens
Cochylimorpha clavana	Nem védett / indifferens
Cochylimorpha elongana	Nem védett / indifferens
Cochylimorpha hilarana	Nem védett / indifferens
Hysterophora jaculana	Nem védett / indifferens
Cochylimorpha jucundana	Nem védett / indifferens
Cochylimorpha obliquana	Nem védett / indifferens
Cochylimorpha perfusana	Nem védett / indifferens
Cochylimorpha straminea	Nem védett / indifferens
Cochylimorpha woliniana	Nem védett / indifferens
Cochylis atricapitana	Nem védett / indifferens
Cochylis dubitana	Nem védett / indifferens
Cochylis epilinana	Nem védett / indifferens
Cochylis flaviciliana	Nem védett / indifferens
Cochylis hybridella	Nem védett / indifferens
Cochylis nana	Nem védett / indifferens
Cochylis pallidana	Nem védett / indifferens
Cochylis posterana	Nem védett / indifferens
Cochylis roseana	Nem védett / indifferens
Cochylis salebrana	Nem védett / indifferens
Codocera ferruginea	Nem védett / indifferens
Codophila varia	Nem védett / indifferens
Antheminia varicornis	Nem védett / indifferens
Coeliastes lamii	Nem védett / indifferens
Coeliodes proximus	Nem védett / indifferens
Coeliodes rana	Nem védett / indifferens
Coeliodes ruber	Nem védett / indifferens
Coeliodes transversealbofasciatus	Nem védett / indifferens
Coeliodes trifasciatus	Nem védett / indifferens
Coeliodinus rubicundus	Nem védett / indifferens
Coelioys acanthura	Nem védett / indifferens
Coelioys afra	Nem védett / indifferens
Coelioys alata	Nem védett / indifferens
Coelioys aurolimbata	Nem védett / indifferens
Coelioys brevis	Nem védett / indifferens
Coelioys caudata	Nem védett / indifferens
Coelioys conoidea	Nem védett / indifferens
Coelioys elongata	Nem védett / indifferens
Coelioys emarginata	Nem védett / indifferens
Coelioys haemorrhoa	Nem védett / indifferens
Coelioys inermis	Nem védett / indifferens
Coelioys mandibularis	Nem védett / indifferens
Coelioys obtusa	Nem védett / indifferens
Coelioys polycentris	Nem védett / indifferens
Coelioys quadridentata	Nem védett / indifferens
Coelioys rufescens	Nem védett / indifferens
Coelioys rufocaudata	Nem védett / indifferens
Coelostoma orbiculare	Nem védett / indifferens
Coelotes atropos	Nem védett / indifferens
Coelotes solitarius	Nem védett / indifferens
Coelotes terrestris	Nem védett / indifferens
Coenagrion hastulatum	Védett
Coenagrion lunulatum	Védett
Coenagrion ornatum	Védett
Coenagrion puella	Nem védett / indifferens
Coenagrion pulchellum	Nem védett / indifferens
Coenagrion pulchellum interruptum	Nem védett / indifferens
Coenagrion scitulum	Védett
Coenocalpe lapidata	Védett
Coenonympha arcania	Nem védett / indifferens
Coenonympha glycerion	Nem védett / indifferens
Coenonympha hero	Nem védett / indifferens
Coenonympha oedippus	Fokozottan védett
Coenonympha pamphilus	Nem védett / indifferens
Coenonympha tullia	Nem védett / indifferens
Colenis immunda	Nem védett / indifferens
Coleophora absinthii	Nem védett / indifferens
Coleophora acrisella	Nem védett / indifferens
Coleophora adelogrammella	Nem védett / indifferens
Coleophora adjunctella	Nem védett / indifferens
Coleophora adspersella	Nem védett / indifferens
Coleophora ahenella	Nem védett / indifferens
Coleophora albella	Nem védett / indifferens
Coleophora albicostella	Nem védett / indifferens
Coleophora albidella	Nem védett / indifferens
Coleophora albilineella	Nem védett / indifferens
Coleophora albitarsella	Nem védett / indifferens
Coleophora alcyonipennella	Nem védett / indifferens
Coleophora alticolella	Nem védett / indifferens
Coleophora amellivora	Nem védett / indifferens
Coleophora anatipenella	Nem védett / indifferens
Coleophora antennariella	Nem védett / indifferens
Coleophora argentula	Nem védett / indifferens
Coleophora artemisicolella	Nem védett / indifferens
Coleophora albicans	Nem védett / indifferens
Coleophora asteris	Nem védett / indifferens
Coleophora astragalella	Nem védett / indifferens
Coleophora atriplicis	Nem védett / indifferens
Coleophora auricella	Nem védett / indifferens
Coleophora badiipennella	Nem védett / indifferens
Coleophora ballotella	Nem védett / indifferens
Coleophora betulella	Nem védett / indifferens
Coleophora bilineatella	Nem védett / indifferens
Coleophora bilineella	Nem védett / indifferens
Coleophora binderella	Nem védett / indifferens
Coleophora binotapennella	Nem védett / indifferens
Coleophora brevipalpella	Nem védett / indifferens
Coleophora caelebipennella	Nem védett / indifferens
Coleophora caespititiella	Nem védett / indifferens
Coleophora cartilaginella	Nem védett / indifferens
Coleophora cecidophorella	Nem védett / indifferens
Coleophora chamaedriella	Nem védett / indifferens
Coleophora chrysanthemi	Nem védett / indifferens
Coleophora ciconiella	Nem védett / indifferens
Coleophora clypeiferella	Nem védett / indifferens
Coleophora colutella	Nem védett / indifferens
Coleophora congeriella	Nem védett / indifferens
Coleophora conspicuella	Nem védett / indifferens
Coleophora conyzae	Nem védett / indifferens
Coleophora coracipennella	Nem védett / indifferens
Coleophora cornutella	Nem védett / indifferens
Coleophora coronillae	Nem védett / indifferens
Coleophora corsicella	Nem védett / indifferens
Coleophora cracella	Nem védett / indifferens
Coleophora currucipennella	Nem védett / indifferens
Coleophora deauratella	Nem védett / indifferens
Coleophora dentiferella	Nem védett / indifferens
Coleophora dianthi	Nem védett / indifferens
Coleophora directella	Nem védett / indifferens
Coleophora discordella	Nem védett / indifferens
Coleophora ditella	Nem védett / indifferens
Coleophora eurasiatica	Nem védett / indifferens
Coleophora flaviella	Nem védett / indifferens
Coleophora flavipennella	Nem védett / indifferens
Coleophora follicularis	Nem védett / indifferens
Coleophora frankii	Nem védett / indifferens
Coleophora fringillella	Nem védett / indifferens
Coleophora frischella	Nem védett / indifferens
Coleophora fuscociliella	Nem védett / indifferens
Coleophora fuscocuprella	Nem védett / indifferens
Coleophora galatellae	Nem védett / indifferens
Coleophora galbulipennella	Nem védett / indifferens
Coleophora gallipennella	Nem védett / indifferens
Coleophora genistae	Nem védett / indifferens
Coleophora glaseri	Nem védett / indifferens
Coleophora glaucicolella	Nem védett / indifferens
Coleophora gnaphalii	Nem védett / indifferens
Coleophora granulatella	Nem védett / indifferens
Coleophora gryphipennella	Nem védett / indifferens
Coleophora halophilella	Nem védett / indifferens
Coleophora hemerobiella	Nem védett / indifferens
Coleophora hieronella	Nem védett / indifferens
Coleophora hospitiella	Nem védett / indifferens
Coleophora hungariae	Védett
Coleophora hydrolapathella	Nem védett / indifferens
Coleophora ibipennella	Nem védett / indifferens
Coleophora inulae	Nem védett / indifferens
Coleophora juncicolella	Nem védett / indifferens
Coleophora kasyi	Nem védett / indifferens
Coleophora klimeschiella	Nem védett / indifferens
Coleophora kroneella	Nem védett / indifferens
Coleophora kuehnella	Nem védett / indifferens
Coleophora laricella	Nem védett / indifferens
Coleophora limosipennella	Nem védett / indifferens
Coleophora lineolea	Nem védett / indifferens
Coleophora linosyridella	Nem védett / indifferens
Coleophora linosyris	Nem védett / indifferens
Coleophora lithargyrinella	Nem védett / indifferens
Coleophora lixella	Nem védett / indifferens
Coleophora longicornella	Nem védett / indifferens
Coleophora lutipennella	Nem védett / indifferens
Coleophora magyarica	Nem védett / indifferens
Coleophora mayrella	Nem védett / indifferens
Coleophora medelichensis	Nem védett / indifferens
Coleophora millefolii	Nem védett / indifferens
Coleophora milvipennis	Nem védett / indifferens
Coleophora motacillella	Nem védett / indifferens
Coleophora musculella	Nem védett / indifferens
Coleophora narbonensis	Nem védett / indifferens
Coleophora niveiciliella	Nem védett / indifferens
Coleophora niveicostella	Nem védett / indifferens
Coleophora niveistrigella	Nem védett / indifferens
Coleophora nomgona	Nem védett / indifferens
Coleophora nutantella	Nem védett / indifferens
Coleophora obscenella	Nem védett / indifferens
Coleophora obtectella	Nem védett / indifferens
Coleophora obviella	Nem védett / indifferens
Coleophora ochrea	Nem védett / indifferens
Coleophora ochripennella	Nem védett / indifferens
Coleophora odorariella	Nem védett / indifferens
Coleophora onobrychiella	Nem védett / indifferens
Coleophora onopordiella	Nem védett / indifferens
Coleophora orbitella	Nem védett / indifferens
Coleophora oriolella	Nem védett / indifferens
Coleophora ornatipennella	Nem védett / indifferens
Coleophora otidipennella	Nem védett / indifferens
Coleophora paripennella	Nem védett / indifferens
Coleophora partitella	Nem védett / indifferens
Coleophora peisoniella	Nem védett / indifferens
Coleophora pennella	Nem védett / indifferens
Coleophora peribenanderi	Nem védett / indifferens
Coleophora pratella	Nem védett / indifferens
Coleophora preisseckeri	Nem védett / indifferens
Coleophora prunifoliae	Nem védett / indifferens
Coleophora pseudociconiella	Nem védett / indifferens
Coleophora pseudoditella	Nem védett / indifferens
Coleophora pseudolinosyris	Nem védett / indifferens
Coleophora pseudorepentis	Nem védett / indifferens
Coleophora ptarmicia	Nem védett / indifferens
Coleophora pulmonariella	Nem védett / indifferens
Coleophora punctulatella	Nem védett / indifferens
Coleophora ramosella	Nem védett / indifferens
Coleophora remizella	Nem védett / indifferens
Coleophora riffelensis	Nem védett / indifferens
Coleophora salicorniae	Nem védett / indifferens
Coleophora salinella	Nem védett / indifferens
Coleophora saponariella	Nem védett / indifferens
Coleophora saturatella	Nem védett / indifferens
Coleophora saxicolella	Nem védett / indifferens
Coleophora serpylletorum	Nem védett / indifferens
Coleophora serratella	Nem védett / indifferens
Coleophora siccifolia	Nem védett / indifferens
Coleophora silenella	Nem védett / indifferens
Coleophora solitariella	Nem védett / indifferens
Coleophora spiraeella	Nem védett / indifferens
Coleophora squalorella	Nem védett / indifferens
Coleophora squamosella	Nem védett / indifferens
Coleophora sternipennella	Nem védett / indifferens
Coleophora stramentella	Nem védett / indifferens
Coleophora striatipennella	Nem védett / indifferens
Coleophora striolatella	Nem védett / indifferens
Coleophora sylvaticella	Nem védett / indifferens
Coleophora taeniipennella	Nem védett / indifferens
Coleophora tamesis	Nem védett / indifferens
Coleophora tanaceti	Nem védett / indifferens
Coleophora therinella	Nem védett / indifferens
Coleophora thymi	Nem védett / indifferens
Coleophora trifariella	Nem védett / indifferens
Coleophora trifolii	Nem védett / indifferens
Coleophora trigeminella	Nem védett / indifferens
Coleophora trochilella	Nem védett / indifferens
Coleophora tyrrhaenica	Nem védett / indifferens
Coleophora unipunctella	Nem védett / indifferens
Coleophora uralensis	Nem védett / indifferens
Coleophora versurella	Nem védett / indifferens
Coleophora vestianella	Nem védett / indifferens
Coleophora vibicella	Nem védett / indifferens
Coleophora vibicigerella	Nem védett / indifferens
Coleophora vicinella	Nem védett / indifferens
Coleophora virgatella	Nem védett / indifferens
Coleophora vulnerariae	Nem védett / indifferens
Coleophora vulpecula	Nem védett / indifferens
Coleophora wockeella	Nem védett / indifferens
Coleophora zelleriella	Nem védett / indifferens
Coleotechnites piceaella	Nem védett / indifferens
Colias alfacariensis	Nem védett / indifferens
Colias chrysotheme	Fokozottan védett
Colias croceus	Nem védett / indifferens
Colias erate	Nem védett / indifferens
Colias hyale	Nem védett / indifferens
Colias myrmidone	Fokozottan védett
Colias palaeno	Nem védett / indifferens
Colladonus torneellus	Nem védett / indifferens
Colletes anchusae	Nem védett / indifferens
Colletes collaris	Nem védett / indifferens
Colletes cunicularius	Nem védett / indifferens
Colletes daviesanus	Nem védett / indifferens
Colletes floralis	Nem védett / indifferens
Colletes fodiens	Nem védett / indifferens
Colletes gallicus	Nem védett / indifferens
Colletes graeffei	Nem védett / indifferens
Colletes hylaeiformis	Nem védett / indifferens
Colletes inexpectatus	Nem védett / indifferens
Colletes lebedewi	Nem védett / indifferens
Colletes marginatus	Nem védett / indifferens
Colletes mlokossewiczi	Nem védett / indifferens
Colletes nasutus	Nem védett / indifferens
Colletes nigicans	Nem védett / indifferens
Colletes pallescens	Nem védett / indifferens
Colletes punctatus	Nem védett / indifferens
Colletes similis	Nem védett / indifferens
Colletes spectabilis	Nem védett / indifferens
Colletes succinctus	Nem védett / indifferens
Colobicus hirtus	Nem védett / indifferens
Colobochyla salicalis	Nem védett / indifferens
Colobopterus erraticus	Nem védett / indifferens
Colocasia coryli	Nem védett / indifferens
Colon affine	Nem védett / indifferens
Colon appendiculatum	Nem védett / indifferens
Colon armipes	Nem védett / indifferens
Colon brunneum	Nem védett / indifferens
Colon claviger	Nem védett / indifferens
Colon dentipes	Nem védett / indifferens
Colon fuscicorne	Nem védett / indifferens
Colon latum	Nem védett / indifferens
Colon murinum	Nem védett / indifferens
Colon rufescens	Nem védett / indifferens
Colon serripes	Nem védett / indifferens
Colon viennense	Nem védett / indifferens
Colostygia olivata	Nem védett / indifferens
Colostygia pectinataria	Nem védett / indifferens
Colotes hampei	Nem védett / indifferens
Colotois pennaria	Nem védett / indifferens
Colpa interrupta	Nem védett / indifferens
Colpa quinquecincta	Nem védett / indifferens
Colpa quinquecincta abdominalis	Nem védett / indifferens
Colposis mutilatus	Nem védett / indifferens
Coluber caspius	Fokozottan védett
Coluber jugularis	Nem védett / indifferens
Columba livia	Nem védett / indifferens
Columba livia f. domestica	Nem védett / indifferens
Columba oenas	Védett
Columba palumbus	Nem védett / indifferens
Columella edentula	Nem védett / indifferens
Colydium elongatum	Nem védett / indifferens
Colydium filiforme	Nem védett / indifferens
Colymbetes fuscus	Nem védett / indifferens
Colymbetes striatus	Nem védett / indifferens
Comaroma simoni	Nem védett / indifferens
Combocerus glaber	Nem védett / indifferens
Comibaena bajularia	Nem védett / indifferens
Saperda populnea	Nem védett / indifferens
Compsidolon absinthii	Nem védett / indifferens
Compsidolon pumilum	Nem védett / indifferens
Compsidolon salicellum	Nem védett / indifferens
Conalia baudii	Nem védett / indifferens
Coniocleonus cicatricosus	Nem védett / indifferens
Coniocleonus excoriatus	Nem védett / indifferens
Coniocleonus hollbergii	Nem védett / indifferens
Coniocleonus nigrosuturatus	Nem védett / indifferens
Coniocleonus turbatus	Nem védett / indifferens
Coniopteryx arcuata	Nem védett / indifferens
Coniopteryx aspoecki	Nem védett / indifferens
Coniopteryx borealis	Nem védett / indifferens
Coniopteryx drammonti	Nem védett / indifferens
Coniopteryx esbenpeterseni	Nem védett / indifferens
Coniopteryx haematica	Nem védett / indifferens
Coniopteryx hoelzeli	Nem védett / indifferens
Coniopteryx lentiae	Nem védett / indifferens
Coniopteryx pygmaea	Nem védett / indifferens
Coniopteryx renate	Nem védett / indifferens
Coniopteryx tineiformis	Nem védett / indifferens
Coniopteryx tjederi	Nem védett / indifferens
Conisania leineri	Nem védett / indifferens
Conisania luteago	Nem védett / indifferens
Conistra erythrocephala	Nem védett / indifferens
Conistra ligula	Nem védett / indifferens
Conistra rubiginea	Nem védett / indifferens
Conistra rubiginosa	Nem védett / indifferens
Conistra vaccinii	Nem védett / indifferens
Conistra veronicae	Nem védett / indifferens
Conobathra repandana	Nem védett / indifferens
Conobathra tumidana	Nem védett / indifferens
Conocephalus fuscus	Nem védett / indifferens
Conocephalus dorsalis	Nem védett / indifferens
Conomelus anceps	Nem védett / indifferens
Conomelus dehneli	Nem védett / indifferens
Conopalpus testaceus	Nem védett / indifferens
Conosanus obsoletus	Nem védett / indifferens
Conostethus hungaricus	Nem védett / indifferens
Conostethus roseus	Nem védett / indifferens
Conwentzia pineticola	Nem védett / indifferens
Conwentzia psociformis	Nem védett / indifferens
Copelatus haemorrhoidalis	Nem védett / indifferens
Copium clavicorne	Nem védett / indifferens
Copium teucrii	Nem védett / indifferens
Coprimorphus scrutator	Nem védett / indifferens
Copris lunaris	Védett
Copris umbilicatus	Védett
Coproceramius atramentarius	Nem védett / indifferens
Coproceramius cinnamopterus	Nem védett / indifferens
Coproceramius europaeus	Nem védett / indifferens
Coproceramius laevanus	Nem védett / indifferens
Coproceramius marcidus	Nem védett / indifferens
Coproceramius parapicipennis	Nem védett / indifferens
Coproceramius putridus	Nem védett / indifferens
Coproceramius setigerus	Nem védett / indifferens
Coprophilus piceus	Nem védett / indifferens
Coprophilus striatulus	Nem védett / indifferens
Coproporus colchicus	Nem védett / indifferens
Coprothassa melanaria	Nem védett / indifferens
Coptosoma mucronatum	Nem védett / indifferens
Coptosoma scutellatum	Nem védett / indifferens
Coquillettidia richiardii	Nem védett / indifferens
Coracias garrulus	Fokozottan védett
Coraebus elatus	Nem védett / indifferens
Coraebus florentinus	Védett
Coraebus rubi	Nem védett / indifferens
Coraebus undatus	Védett
Coranus contrarius	Nem védett / indifferens
Coranus griseus	Nem védett / indifferens
Coranus kerzhneri	Nem védett / indifferens
Coranus laticeps	Nem védett / indifferens
Coranus subapterus	Nem védett / indifferens
Corbicula fluminalis	Nem védett / indifferens
Corbicula fluminea	Nem védett / indifferens
Cordalia obscura	Nem védett / indifferens
Cordicomus gracilis	Nem védett / indifferens
Cordicomus sellatus	Nem védett / indifferens
Cordulegaster bidentata	Fokozottan védett
Cordulegaster heros	Fokozottan védett
Cordulia aenea	Nem védett / indifferens
Cordylepherus viridis	Nem védett / indifferens
Coregonus albula	Nem védett / indifferens
Coregonus lavaretus	Nem védett / indifferens
Coreus marginatus	Nem védett / indifferens
Coriarachne depressa	Nem védett / indifferens
Coriomeris denticulatus	Nem védett / indifferens
Coriomeris hirticornis	Nem védett / indifferens
Coriomeris scabricornis	Nem védett / indifferens
Corixa affinis	Nem védett / indifferens
Corixa panzeri	Nem védett / indifferens
Corixa punctata	Nem védett / indifferens
Corizus hyoscyami	Nem védett / indifferens
Cornu aspersum	Nem védett / indifferens
Cornutiplusia circumflexa	Nem védett / indifferens
Coronella austriaca	Védett
Corticaria bella	Nem védett / indifferens
Corticaria crenulata	Nem védett / indifferens
Corticaria elongata	Nem védett / indifferens
Corticaria fagi	Nem védett / indifferens
Corticaria ferruginea	Nem védett / indifferens
Corticaria fulva	Nem védett / indifferens
Corticaria impressa	Nem védett / indifferens
Corticaria lapponica	Nem védett / indifferens
Corticaria linearis	Nem védett / indifferens
Corticaria longicollis	Nem védett / indifferens
Corticaria longicornis	Nem védett / indifferens
Corticaria obscura	Nem védett / indifferens
Corticaria polypori	Nem védett / indifferens
Corticaria pubescens	Nem védett / indifferens
Corticaria saginata	Nem védett / indifferens
Corticaria serrata	Nem védett / indifferens
Corticaria umbilicata	Nem védett / indifferens
Corticarina fuscula	Nem védett / indifferens
Corticarina obfuscata	Nem védett / indifferens
Corticarina similata	Nem védett / indifferens
Corticarina truncatella	Nem védett / indifferens
Hypophloeus bicolor	Nem védett / indifferens
Hypophloeus fasciatus	Nem védett / indifferens
Hypophloeus fraxini	Nem védett / indifferens
Hypophloeus linearis	Nem védett / indifferens
Hypophloeus longulus	Nem védett / indifferens
Hypophloeus pini	Nem védett / indifferens
Hypophloeus suberis	Nem védett / indifferens
Hypophloeus unicolor	Nem védett / indifferens
Hypophloeus versipellis	Nem védett / indifferens
Cortinicara gibbosa	Nem védett / indifferens
Cortodera femorata	Nem védett / indifferens
Cortodera flavimana	Védett
Cortodera holosericea	Védett
Cortodera humeralis	Nem védett / indifferens
Cortodera villosa	Nem védett / indifferens
Corvus corax	Védett
Corvus cornix	Nem védett / indifferens
Corvus corone	Védett
Corvus corone sardonius	Nem védett / indifferens
Corvus frugilegus	Védett
Corvus monedula	Védett
Corvus monedula soemmeringi	Védett
Corvus monedula spermologus	Védett
Corylophus cassidoides	Nem védett / indifferens
Coryphaelus gyllenhalii	Nem védett / indifferens
Coryssomerus capucinus	Nem védett / indifferens
Corythucha ciliata	Nem védett / indifferens
Coscinia cribaria	Védett
Cosmardia moritzella	Nem védett / indifferens
Cosmia affinis	Nem védett / indifferens
Cosmia diffinis	Nem védett / indifferens
Cosmia pyralina	Nem védett / indifferens
Cosmia trapezina	Nem védett / indifferens
Elachista freyerella	Nem védett / indifferens
Elachista stabilella	Nem védett / indifferens
Cosmobaris scolopacea	Nem védett / indifferens
Cosmopterix lienigiella	Nem védett / indifferens
Cosmopterix orichalcea	Nem védett / indifferens
Cosmopterix scribaiella	Nem védett / indifferens
Cosmopterix zieglerella	Nem védett / indifferens
Cosmorhoe ocellata	Nem védett / indifferens
Cosmotettix caudatus	Nem védett / indifferens
Cossonus cylindricus	Nem védett / indifferens
Cossonus linearis	Nem védett / indifferens
Cossonus parallelepipedus	Nem védett / indifferens
Cossus cossus	Nem védett / indifferens
Costaconvexa polygrammata	Nem védett / indifferens
Cotaster uncipes	Nem védett / indifferens
Cottus gobio	Védett
Cottus poecilopus	Védett
Coturnix coturnix	Védett
Coxelus pictus	Nem védett / indifferens
Crabro cribrarius	Nem védett / indifferens
Crabro loewi	Nem védett / indifferens
Crabro peltarius	Nem védett / indifferens
Crabro scutellatus	Nem védett / indifferens
Crambus ericella	Nem védett / indifferens
Crambus hamella	Nem védett / indifferens
Crambus lathoniellus	Nem védett / indifferens
Crambus pascuella	Nem védett / indifferens
Crambus perlella	Nem védett / indifferens
Crambus pratella	Nem védett / indifferens
Crambus silvella	Nem védett / indifferens
Crambus uliginosellus	Nem védett / indifferens
Craniophora ligustri	Nem védett / indifferens
Craspedosoma rawlinsii	Nem védett / indifferens
Craspedosoma transsilvanicum	Nem védett / indifferens
Craspedosoma transsilvanicum austriacum	Nem védett / indifferens
Craspedosoma transsilvanicum barcsicum	Nem védett / indifferens
Craspedosoma transsilvanicum pakozdense	Nem védett / indifferens
Crassa tinctella	Nem védett / indifferens
Crassa unitella	Nem védett / indifferens
Crataraea suturalis	Nem védett / indifferens
Cratosilis denticollis	Nem védett / indifferens
Crematogaster schmidti	Nem védett / indifferens
Crematogaster scutellartis	Nem védett / indifferens
Cremnocephalus alpestris	Nem védett / indifferens
Creoleon plumbeus	Védett
Creophilus maxillosus	Nem védett / indifferens
Crepidophorus mutilatus	Nem védett / indifferens
Crex crex	Fokozottan védett
Cricetus cricetus	Nem védett / indifferens
Criocoris crassicornis	Nem védett / indifferens
Criocoris nigricornis	Nem védett / indifferens
Criocoris nigripes	Nem védett / indifferens
Criocoris sulcicornis	Nem védett / indifferens
Criomorphus albomarginatus	Nem védett / indifferens
Criomorphus williamsi	Nem védett / indifferens
Criorhina asilica	Nem védett / indifferens
Criorhina pachymera	Nem védett / indifferens
Criorhina ranunculi	Nem védett / indifferens
Crocallis elinguaria	Nem védett / indifferens
Crocallis tusciaria	Nem védett / indifferens
Crocidosema plebejana	Nem védett / indifferens
Crocidura leucodon	Védett
Crocidura suaveolens	Védett
Crocothemis erythraea	Nem védett / indifferens
Crombrugghia distans	Nem védett / indifferens
Crombrugghia tristis	Nem védett / indifferens
Crossobela trinotella	Nem védett / indifferens
Crossocerus acanthophorus	Nem védett / indifferens
Crossocerus annulipes	Nem védett / indifferens
Crossocerus assimilis	Nem védett / indifferens
Crossocerus barbipes	Nem védett / indifferens
Crossocerus binotatus	Nem védett / indifferens
Crossocerus capitosus	Nem védett / indifferens
Crossocerus cetratus	Nem védett / indifferens
Crossocerus congener	Nem védett / indifferens
Crossocerus denticoxa	Nem védett / indifferens
Crossocerus denticrus	Nem védett / indifferens
Crossocerus dimidiatus	Nem védett / indifferens
Crossocerus distinguendus	Nem védett / indifferens
Crossocerus elongatulus	Nem védett / indifferens
Crossocerus exiguus	Nem védett / indifferens
Crossocerus megacephalus	Nem védett / indifferens
Crossocerus nigritus	Nem védett / indifferens
Crossocerus ovalis	Nem védett / indifferens
Crossocerus palmipes	Nem védett / indifferens
Crossocerus podagricus	Nem védett / indifferens
Crossocerus pusillus	Nem védett / indifferens
Crossocerus quadrimaculatus	Nem védett / indifferens
Crossocerus tarsatus	Nem védett / indifferens
Crossocerus vagabundus	Nem védett / indifferens
Crossocerus walkeri	Nem védett / indifferens
Crossocerus wesmaeli	Nem védett / indifferens
Crunoecia irrorata	Nem védett / indifferens
Crunoecia kempnyi	Nem védett / indifferens
Crustulina guttata	Nem védett / indifferens
Crustulina sticta	Nem védett / indifferens
Cryphaeus cornutus	Védett
Cryphalus abietis	Nem védett / indifferens
Cryphalus piceae	Nem védett / indifferens
Cryphalus saltuarius	Nem védett / indifferens
Cryphia algae	Nem védett / indifferens
Cryphia domestica	Nem védett / indifferens
Cryphia ereptricula	Nem védett / indifferens
Cryphia fraudatricula	Nem védett / indifferens
Cryphia muralis	Nem védett / indifferens
Cryphia raptricula	Nem védett / indifferens
Cryphia receptricula	Nem védett / indifferens
Cryphoeca silvicola	Nem védett / indifferens
Crypsedra gemmea	Nem védett / indifferens
Crypsinus angustatus	Nem védett / indifferens
Cryptarcha strigata	Nem védett / indifferens
Cryptarcha undata	Nem védett / indifferens
Crypticus quisquilius	Nem védett / indifferens
Cryptobium collare	Nem védett / indifferens
Cryptobium fracticorne	Nem védett / indifferens
Cryptoblabes bistriga	Nem védett / indifferens
Cryptocandona dudichi	Nem védett / indifferens
Cryptocandona matris	Nem védett / indifferens
Cryptocandona vavrai	Nem védett / indifferens
Cryptocheilus confinis	Nem védett / indifferens
Cryptocheilus egregius	Nem védett / indifferens
Cryptocheilus elegans	Nem védett / indifferens
Cryptocheilus fabricii	Nem védett / indifferens
Cryptocheilus freygessneri	Nem védett / indifferens
Cryptocheilus guttulatus	Nem védett / indifferens
Cryptocheilus notatus	Nem védett / indifferens
Cryptocheilus notatus affinis	Nem védett / indifferens
Cryptocheilus richardsi	Nem védett / indifferens
Cryptocheilus simillimus	Nem védett / indifferens
Cryptocheilus variabilis	Nem védett / indifferens
Cryptocheilus versicolor	Nem védett / indifferens
Cryptococcus aceris	Nem védett / indifferens
Cryptococcus fagisuga	Nem védett / indifferens
Cryptocochylis conjunctana	Nem védett / indifferens
Cryptocyclops bicolor	Nem védett / indifferens
Cryptodrassus hungaricus	Nem védett / indifferens
Cryptolestes capensis	Nem védett / indifferens
Cryptolestes corticinus	Nem védett / indifferens
Cryptolestes duplicatus	Nem védett / indifferens
Cryptolestes ferrugineus	Nem védett / indifferens
Cryptolestes pusilloides	Nem védett / indifferens
Cryptolestes pusillus	Nem védett / indifferens
Cryptolestes spartii	Nem védett / indifferens
Cryptolestes turcicus	Nem védett / indifferens
Cryptophagus acutangulus	Nem védett / indifferens
Cryptophagus affinis	Nem védett / indifferens
Cryptophagus badius	Nem védett / indifferens
Cryptophagus baldensis	Nem védett / indifferens
Cryptophagus cellaris	Nem védett / indifferens
Cryptophagus confusus	Nem védett / indifferens
Cryptophagus croaticus	Nem védett / indifferens
Cryptophagus dentatus	Nem védett / indifferens
Cryptophagus deubeli	Nem védett / indifferens
Cryptophagus distinguendus	Nem védett / indifferens
Cryptophagus fallax	Nem védett / indifferens
Cryptophagus fuscicornis	Nem védett / indifferens
Cryptophagus hexagonalis	Nem védett / indifferens
Cryptophagus immixtus	Nem védett / indifferens
Cryptophagus labilis	Nem védett / indifferens
Cryptophagus lycoperdi	Nem védett / indifferens
Cryptophagus micaceus	Nem védett / indifferens
Cryptophagus nitidulus	Nem védett / indifferens
Cryptophagus pallidus	Nem védett / indifferens
Cryptophagus pilosus	Nem védett / indifferens
Cryptophagus pseudodentatus	Nem védett / indifferens
Cryptophagus pubescens	Nem védett / indifferens
Cryptophagus quercinus	Nem védett / indifferens
Cryptophagus rotundatus	Nem védett / indifferens
Cryptophagus saginatus	Nem védett / indifferens
Cryptophagus scanicus	Nem védett / indifferens
Cryptophagus schmidti	Nem védett / indifferens
Cryptophagus scutellatus	Nem védett / indifferens
Cryptophagus setulosus	Nem védett / indifferens
Cryptophagus shroetteri	Nem védett / indifferens
Cryptophagus simplex	Nem védett / indifferens
Cryptophagus sporadum	Nem védett / indifferens
Cryptophagus subdepressus	Nem védett / indifferens
Cryptophagus subfumatus	Nem védett / indifferens
Cryptophagus thomsoni	Nem védett / indifferens
Cryptophilus integer	Nem védett / indifferens
Cryptophonus melancholicus	Nem védett / indifferens
Cryptophonus tenebrosus	Nem védett / indifferens
Cryptophonus tenebrosus centralis	Nem védett / indifferens
Cryptopleurum crenatum	Nem védett / indifferens
Cryptopleurum minutum	Nem védett / indifferens
Cryptopleurum subtile	Nem védett / indifferens
Cryptopone ochraceum	Nem védett / indifferens
Cryptops anomalans	Nem védett / indifferens
Cryptops hortensis	Nem védett / indifferens
Cryptops parisi	Nem védett / indifferens
Cryptorhynchus lapathi	Nem védett / indifferens
Cryptostemma alienum	Nem védett / indifferens
Cryptostemma pusillimum	Nem védett / indifferens
Cryptostemma waltli	Nem védett / indifferens
Crypturgus cinereus	Nem védett / indifferens
Crypturgus hispidulus	Nem védett / indifferens
Crypturgus pusillus	Nem védett / indifferens
Ctenicera cuprea	Nem védett / indifferens
Ctenicera pectinicornis	Nem védett / indifferens
Ctenicera virens	Nem védett / indifferens
Cteniopus sulphureus	Nem védett / indifferens
Cteniopus sulphuripes	Nem védett / indifferens
Ctenistes palpalis	Nem védett / indifferens
Ctenopharyngodon idella	Nem védett / indifferens
Ctesias serra	Nem védett / indifferens
Cucujus cinnaberinus	Védett
Cucullia absinthii	Nem védett / indifferens
Cucullia argentea	Védett
Cucullia artemisiae	Nem védett / indifferens
Cucullia asteris	Védett
Cucullia balsamitae	Védett
Cucullia campanulae	Védett
Cucullia chamomillae	Védett
Cucullia dracunculi	Védett
Cucullia formosa	Fokozottan védett
Cucullia fraudatrix	Nem védett / indifferens
Cucullia gnaphalii	Védett
Cucullia lactucae	Nem védett / indifferens
Cucullia lucifuga	Védett
Cucullia mixta lorica	Fokozottan védett
Cucullia scopariae	Védett
Cucullia tanaceti	Védett
Cucullia umbratica	Nem védett / indifferens
Cucullia xeranthemi	Védett
Cuculus canorus	Védett
Culex hortensis	Nem védett / indifferens
Culex martinii	Nem védett / indifferens
Culex mimeticus	Nem védett / indifferens
Culex modestus	Nem védett / indifferens
Culex pipiens	Nem védett / indifferens
Culex pipiens molestus	Nem védett / indifferens
Culex territans	Nem védett / indifferens
Culex theileri	Nem védett / indifferens
Culex torrentium	Nem védett / indifferens
Culiseta alaskaensis	Nem védett / indifferens
Culiseta annulata	Nem védett / indifferens
Culiseta glaphyroptera	Nem védett / indifferens
Culiseta longiareolata	Nem védett / indifferens
Culiseta morsitans	Nem védett / indifferens
Culiseta subochrea	Nem védett / indifferens
Cunctochrysa albolineata	Nem védett / indifferens
Cupido alcetas	Védett
Cupido argiades	Nem védett / indifferens
Cupido decolorata	Védett
Cupido minimus	Nem védett / indifferens
Cupido osiris	Védett
Curculio betulae	Nem védett / indifferens
Curculio elephas	Nem védett / indifferens
Curculio glandium	Nem védett / indifferens
Curculio nucum	Nem védett / indifferens
Curculio pellitus	Nem védett / indifferens
Curculio propinquus	Nem védett / indifferens
Curculio rubidus	Nem védett / indifferens
Curculio venosus	Nem védett / indifferens
Curculio villosus	Nem védett / indifferens
Curelius dilutus	Nem védett / indifferens
Curelius exiguus	Nem védett / indifferens
Curimopsis austriaca	Nem védett / indifferens
Curimopsis paleata	Nem védett / indifferens
Curimus erichsoni	Nem védett / indifferens
Curimus erinaceus	Nem védett / indifferens
Curtimorda bisignata	Nem védett / indifferens
Curtimorda maculosa	Nem védett / indifferens
Cyanapion afer	Nem védett / indifferens
Cyanapion alcyoneum	Nem védett / indifferens
Cyanapion columbinum	Nem védett / indifferens
Cyanapion gyllenhalii	Nem védett / indifferens
Cyanapion platalea	Nem védett / indifferens
Cyanapion spencii	Nem védett / indifferens
Cyanostolus aeneus	Nem védett / indifferens
Cybaeus angustiarum	Nem védett / indifferens
Cybister lateralimarginalis	Nem védett / indifferens
Cybocephalus fodori	Nem védett / indifferens
Cybocephalus politus	Nem védett / indifferens
Cybocephalus pulchellus	Nem védett / indifferens
Cybosia mesomella	Nem védett / indifferens
Cychramus luteus	Nem védett / indifferens
Cychramus variegatus	Nem védett / indifferens
Cychrus attenuatus	Védett
Cychrus caraboides	Védett
Cyclocypris globosa	Nem védett / indifferens
Cyclocypris helocrenica	Nem védett / indifferens
Cyclocypris laevis	Nem védett / indifferens
Cyclocypris ovum	Nem védett / indifferens
Cycloderes pilosulus	Nem védett / indifferens
Cyclodinus dentatus	Nem védett / indifferens
Cyclodinus dentatus transdanubianus	Nem védett / indifferens
Cyclodinus humilis	Nem védett / indifferens
Cyclophora albiocellaria	Nem védett / indifferens
Cyclophora albipunctata	Nem védett / indifferens
Cyclophora annularia	Nem védett / indifferens
Cyclophora linearia	Nem védett / indifferens
Cyclophora pendularia	Nem védett / indifferens
Cyclophora porata	Nem védett / indifferens
Cyclophora punctaria	Nem védett / indifferens
Cyclophora puppillaria	Nem védett / indifferens
Cyclophora quercimontaria	Nem védett / indifferens
Cyclophora ruficiliaria	Nem védett / indifferens
Cyclophora suppunctaria	Nem védett / indifferens
Cyclops furcifer	Nem védett / indifferens
Cyclops insignis	Nem védett / indifferens
Cyclops kikuchii	Nem védett / indifferens
Cyclops scutifer	Nem védett / indifferens
Cyclops strenuus	Nem védett / indifferens
Cyclops vicinus	Nem védett / indifferens
Cyclosa conica	Nem védett / indifferens
Cyclosa oculata	Nem védett / indifferens
Diaphora luctuosa	Védett
Diaphora sordida	Nem védett / indifferens
Cydia amplana	Nem védett / indifferens
Cydia conicolana	Nem védett / indifferens
Cydia coniferana	Nem védett / indifferens
Cydia corollana	Nem védett / indifferens
Cydia cosmophorana	Nem védett / indifferens
Cydia duplicana	Nem védett / indifferens
Cydia exquisitana	Nem védett / indifferens
Cydia fagiglandana	Nem védett / indifferens
Cydia illutana	Nem védett / indifferens
Cydia inquinatana	Nem védett / indifferens
Cydia leguminana	Nem védett / indifferens
Cydia medicaginis	Nem védett / indifferens
Cydia microgrammana	Nem védett / indifferens
Cydia nigricana	Nem védett / indifferens
Cydia oxytropidis	Nem védett / indifferens
Cydia pactolana	Nem védett / indifferens
Cydia pomonella	Nem védett / indifferens
Cydia pyrivora	Nem védett / indifferens
Cydia servillana	Nem védett / indifferens
Cydia strobilella	Nem védett / indifferens
Cydia succedana	Nem védett / indifferens
Cydia splendana	Nem védett / indifferens
Cydia zebeana	Nem védett / indifferens
Cydnus aterrimus	Nem védett / indifferens
Cygnus columbianus	Védett
Cygnus bewickii	Védett
Cygnus cygnus	Védett
Cygnus olor	Védett
Cylindroiulus abaligetanus	Nem védett / indifferens
Cylindroiulus arborum	Nem védett / indifferens
Cylindroiulus boleti	Nem védett / indifferens
Cylindroiulus horvathi	Nem védett / indifferens
Cylindroiulus latestriatus	Nem védett / indifferens
Cylindroiulus luridus	Nem védett / indifferens
Cylindroiulus Meinerti	Nem védett / indifferens
Cylindroiulus parisiorum	Nem védett / indifferens
Cylindroiulus truncorum	Nem védett / indifferens
Cylindromorphus filum	Nem védett / indifferens
Cylister angustatus	Nem védett / indifferens
Cylister linearis	Nem védett / indifferens
Cylister oblongum	Nem védett / indifferens
Cyllecoris histrionius	Nem védett / indifferens
Cyllodes ater	Nem védett / indifferens
Cymatia coleoptrata	Nem védett / indifferens
Cymatia rogenhoferi	Nem védett / indifferens
Cymatophorina diluta	Nem védett / indifferens
Cymbiodyta marginella	Nem védett / indifferens
Cymindis angularis	Nem védett / indifferens
Cymindis axillaris	Nem védett / indifferens
Cymindis budensis	Nem védett / indifferens
Cymindis cingulata	Nem védett / indifferens
Cymindis humeralis	Nem védett / indifferens
Cymindis lineata	Nem védett / indifferens
Cymindis scapularis	Nem védett / indifferens
Cymindis miliaris	Védett
Cymolomia hartigiana	Nem védett / indifferens
Cymus aurescens	Nem védett / indifferens
Cymus claviculus	Nem védett / indifferens
Cymus glandicolor	Nem védett / indifferens
Cymus melanocephalus	Nem védett / indifferens
Cynaeda dentalis	Nem védett / indifferens
Cynaeda gigantea	Nem védett / indifferens
Cynegetis impunctata	Nem védett / indifferens
Cypha discoidea	Nem védett / indifferens
Cypha longicornis	Nem védett / indifferens
Cypha seminulum	Nem védett / indifferens
Cypha tarsalis	Nem védett / indifferens
Cyphea curtula	Nem védett / indifferens
Cyphocleonus achates	Nem védett / indifferens
Cyphocleonus dealbatus	Nem védett / indifferens
Cyphocleonus dealbatus tigrinus	Nem védett / indifferens
Cyphocleonus trisulcatus	Nem védett / indifferens
Cyphon coarctatus	Nem védett / indifferens
Cyphon laevipennis	Nem védett / indifferens
Cyphon ochraceus	Nem védett / indifferens
Cyphon padi	Nem védett / indifferens
Cyphon palustris	Nem védett / indifferens
Cyphon pubescens	Nem védett / indifferens
Cyphon ruficeps	Nem védett / indifferens
Cyphon variabilis	Nem védett / indifferens
Cyphostethus tristriatus	Nem védett / indifferens
Cypria exsculpta	Nem védett / indifferens
Cypria lata	Nem védett / indifferens
Cypria ophthalmica	Nem védett / indifferens
Cypria reptans	Nem védett / indifferens
Cypridopsis hartwigi	Nem védett / indifferens
Cypridopsis vidua	Nem védett / indifferens
Cyprinus carpio	Nem védett / indifferens
Cypris pubera	Nem védett / indifferens
Cypris striata	Nem védett / indifferens
Cyprois marginata	Nem védett / indifferens
Cyrnus crenaticornis	Nem védett / indifferens
Cyrnus flavidus	Nem védett / indifferens
Cyrnus trimaculatus	Nem védett / indifferens
Cyrtanaspis phalerata	Nem védett / indifferens
Cyrtusa subtestacea	Nem védett / indifferens
Cystobranchus fasciatus	Nem védett / indifferens
Cystobranchus respirans	Nem védett / indifferens
Cystomutilla ruficeps	Nem védett / indifferens
Cytilus sericeus	Nem védett / indifferens
Cyzicus tetracerus	Nem védett / indifferens
Dacne bipustulata	Nem védett / indifferens
Dacne notata	Nem védett / indifferens
Dacne rufifrons	Nem védett / indifferens
Dacrila fallax	Nem védett / indifferens
Dadobia immersa	Nem védett / indifferens
Dahlica inconspicuella	Nem védett / indifferens
Dahlica lichenella	Nem védett / indifferens
Dahlica nickerlii	Nem védett / indifferens
Dahlica triquetrella	Nem védett / indifferens
Dalopius marginatus	Nem védett / indifferens
Dalotia coriaria	Nem védett / indifferens
Dama dama	Nem védett / indifferens
Danacea marginata	Nem védett / indifferens
Danacea morosa	Nem védett / indifferens
Danacea nigritarsis	Nem védett / indifferens
Danacea pallidipalpis	Nem védett / indifferens
Danacea pallipes	Nem védett / indifferens
Danacea serbica	Nem védett / indifferens
Daphnia ambigua	Nem védett / indifferens
Daphnia atkinsoni	Nem védett / indifferens
Daphnia cristata	Nem védett / indifferens
Daphnia cucullata	Nem védett / indifferens
Daphnia curvirostris	Nem védett / indifferens
Daphnia galeata	Nem védett / indifferens
Daphnia hyalina	Nem védett / indifferens
Daphnia longispina	Nem védett / indifferens
Daphnia magna	Nem védett / indifferens
Daphnia obtusa	Nem védett / indifferens
Daphnia parvula	Nem védett / indifferens
Daphnia pulex	Nem védett / indifferens
Daphnia pulicaria	Nem védett / indifferens
Daphnia schoedleri	Nem védett / indifferens
Daphnia similis	Nem védett / indifferens
Daphnis nerii	Nem védett / indifferens
Dapsa denticollis	Nem védett / indifferens
Dapsa fodori	Nem védett / indifferens
Darwinula stevensoni	Nem védett / indifferens
Dascillus cervinus	Nem védett / indifferens
Dasumia canestrinii	Nem védett / indifferens
Dasycerus sulcatus	Nem védett / indifferens
Dasygnypeta velata	Nem védett / indifferens
Dasylabris maura	Nem védett / indifferens
Dasylabris regalis	Nem védett / indifferens
Dasypoda argentata	Nem védett / indifferens
Dasypoda braccata	Nem védett / indifferens
Dasypoda hirtipes	Nem védett / indifferens
Dasypoda suripes	Védett
Dasysyrphus albostriatus	Nem védett / indifferens
Dasysyrphus friuliensis	Nem védett / indifferens
Dasysyrphus hilaris	Nem védett / indifferens
Dasysyrphus pinastri	Nem védett / indifferens
Dasysyrphus tricinctus	Nem védett / indifferens
Dasysyrphus venustus	Nem védett / indifferens
Dasytes aerosus	Nem védett / indifferens
Dasytes buphtalmus	Nem védett / indifferens
Dasytes cyaneus	Nem védett / indifferens
Dasytes fusculus	Nem védett / indifferens
Dasytes griseus	Nem védett / indifferens
Dasytes hickeri	Nem védett / indifferens
Dasytes niger	Nem védett / indifferens
Dasytes nigrocyaneus	Nem védett / indifferens
Dasytes obscurus	Nem védett / indifferens
Dasytes plumbeus	Nem védett / indifferens
Dasytes subaeneus	Nem védett / indifferens
Dasytes subalpinus	Nem védett / indifferens
Dasytes tardus	Nem védett / indifferens
Dasytes virens	Nem védett / indifferens
Dasystoma salicella	Nem védett / indifferens
Datomicra canescens	Nem védett / indifferens
Datomicra dadopora	Nem védett / indifferens
Datomicra nigra	Nem védett / indifferens
Datomicra sordidula	Nem védett / indifferens
Datomicra zosterae	Nem védett / indifferens
Datonychus angulosus	Nem védett / indifferens
Datonychus arquata	Nem védett / indifferens
Datonychus derennei	Nem védett / indifferens
Datonychus melanostictus	Nem védett / indifferens
Datonychus paszlavszkyi	Nem védett / indifferens
Datonychus scabrirostris	Nem védett / indifferens
Datonychus transsylvanicus	Nem védett / indifferens
Datonychus urticae	Nem védett / indifferens
Daudebardia brevipes	Nem védett / indifferens
Daudebardia rufa	Nem védett / indifferens
Decantha borkhausenii	Nem védett / indifferens
Decticus verrucivorus	Nem védett / indifferens
Deilephila elpenor	Nem védett / indifferens
Deilephila porcellus	Nem védett / indifferens
Deileptenia ribeata	Nem védett / indifferens
Deilus fugax	Nem védett / indifferens
Deinopsis erosa	Nem védett / indifferens
Deleaster dichrous	Nem védett / indifferens
Delichon urbicum	Védett
Deliphrosoma prolongatum	Nem védett / indifferens
Delphacinus mesomelas	Nem védett / indifferens
Delphacodes audrasi	Nem védett / indifferens
Delphacodes capnodes	Nem védett / indifferens
Delphacodes mulsanti	Nem védett / indifferens
Delphacodes venosus	Nem védett / indifferens
Delphax crassicornis	Nem védett / indifferens
Delta unguiculatum	Nem védett / indifferens
Deltocephalus pulicaris	Nem védett / indifferens
Deltote bankiana	Nem védett / indifferens
Deltote deceptoria	Nem védett / indifferens
Lithacodia uncula	Nem védett / indifferens
Demetrias imperialis	Nem védett / indifferens
Demetrias atricapillus	Nem védett / indifferens
Demetrias monostigma	Nem védett / indifferens
Dendrocopos leucotos	Fokozottan védett
Dendrocopos major	Védett
Dendrocopos major pinetorum	Védett
Dendrocopos medius	Védett
Dendrocopos minor	Védett
Dendrocopos minor hortorum	Védett
Dendrocopos syriacus	Védett
Dendroleon pantherinus	Védett
Dendrolimus pini	Nem védett / indifferens
Dendrophilus punctatus	Nem védett / indifferens
Dendrophilus pygmaeus	Nem védett / indifferens
Dendroxena quadrimaculata	Nem védett / indifferens
Dendryphantes rudis	Nem védett / indifferens
Denisia augustella	Nem védett / indifferens
Denisia luctuosella	Nem védett / indifferens
Denisia similella	Nem védett / indifferens
Denisia stipella	Nem védett / indifferens
Denops albofasciatus	Nem védett / indifferens
Denticollis linearis	Nem védett / indifferens
Denticollis rubens	Nem védett / indifferens
Deporaus betulae	Nem védett / indifferens
Deporaus mannerheimii	Nem védett / indifferens
Depressaria absynthiella	Nem védett / indifferens
Depressaria albipunctella	Nem védett / indifferens
Depressaria artemisiae	Nem védett / indifferens
Depressaria badiella	Nem védett / indifferens
Depressaria cervicella	Nem védett / indifferens
Depressaria chaerophylli	Nem védett / indifferens
Glareola pratincola	Fokozottan védett
Depressaria corticinella	Nem védett / indifferens
Depressaria daucella	Nem védett / indifferens
Depressaria depressana	Nem védett / indifferens
Depressaria douglasella	Nem védett / indifferens
Depressaria emeritella	Nem védett / indifferens
Depressaria libanotidella	Nem védett / indifferens
Depressaria marcella	Nem védett / indifferens
Depressaria olerella	Nem védett / indifferens
Depressaria heraclei	Nem védett / indifferens
Depressaria pimpinellae	Nem védett / indifferens
Depressaria pulcherrimella	Nem védett / indifferens
Depressaria ultimella	Nem védett / indifferens
Deraeocoris annulipes	Nem védett / indifferens
Deraeocoris lutescens	Nem védett / indifferens
Deraeocoris morio	Nem védett / indifferens
Deraeocoris olivaceus	Nem védett / indifferens
Deraeocoris punctulatus	Nem védett / indifferens
Deraeocoris putoni	Nem védett / indifferens
Deraeocoris ruber	Nem védett / indifferens
Deraeocoris rutilus	Nem védett / indifferens
Deraeocoris serenus	Nem védett / indifferens
Deraeocoris trifasciatus	Nem védett / indifferens
Deraeocoris ventralis	Nem védett / indifferens
Derephysia cristata	Nem védett / indifferens
Derephysia foliacea	Nem védett / indifferens
Dermestes ater	Nem védett / indifferens
Dermestes bicolor	Nem védett / indifferens
Dermestes carnivorus	Nem védett / indifferens
Dermestes erichsoni	Nem védett / indifferens
Dermestes frischi	Nem védett / indifferens
Dermestes fuliginosus	Nem védett / indifferens
Dermestes gyllenhali	Nem védett / indifferens
Dermestes intermedius	Nem védett / indifferens
Dermestes kaszabi	Nem védett / indifferens
Dermestes laniarius	Nem védett / indifferens
Dermestes lardarius	Nem védett / indifferens
Dermestes maculatus	Nem védett / indifferens
Dermestes murinus	Nem védett / indifferens
Dermestes olivieri	Nem védett / indifferens
Dermestes szekessyi	Nem védett / indifferens
Dermestes undulatus	Nem védett / indifferens
Deroceras agreste	Nem védett / indifferens
Deroceras klemmi	Nem védett / indifferens
Deroceras laeve	Nem védett / indifferens
Deroceras reticulatum	Nem védett / indifferens
Deroceras rodnae	Nem védett / indifferens
Deroceras sturanyi	Nem védett / indifferens
Deroceras turcicum	Nem védett / indifferens
Derodontus macularis	Nem védett / indifferens
Deronectes latus	Nem védett / indifferens
Deroplia genei	Védett
Deroxena venosulella	Nem védett / indifferens
Derula flavoguttata	Nem védett / indifferens
Deutoleon lineatus	Nem védett / indifferens
Deuterogonia pudorina	Nem védett / indifferens
Devia prospera	Nem védett / indifferens
Dexiogyia corticina	Nem védett / indifferens
Diachromus germanus	Nem védett / indifferens
Diachrysia chrysitis	Nem védett / indifferens
Diachrysia chryson	Védett
Diachrysia nadeja	Védett
Diachrysia stenochrysis	Nem védett / indifferens
Diachrysia zosimi	Védett
Diaclina fagi	Nem védett / indifferens
Diaclina testudinea	Nem védett / indifferens
Diacrisia sannio	Nem védett / indifferens
Diacyclops bicuspidatus	Nem védett / indifferens
Diacyclops bisetosus	Nem védett / indifferens
Diacyclops crassicaudis	Nem védett / indifferens
Diacyclops languidoides	Nem védett / indifferens
Diacyclops languidus	Nem védett / indifferens
Diacyclops nanus	Nem védett / indifferens
Diaea dorsata	Nem védett / indifferens
Diaea livens	Nem védett / indifferens
Dialectica imperialella	Nem védett / indifferens
Dialectica soffneri	Nem védett / indifferens
Dianous coerulescens	Nem védett / indifferens
Diaperis boleti	Nem védett / indifferens
Diaphanosoma brachyurum	Nem védett / indifferens
Diaphanosoma lacustris	Nem védett / indifferens
Diaphanosoma mongolianum	Nem védett / indifferens
Diaphora mendica	Nem védett / indifferens
Diaptomus castor	Nem védett / indifferens
Diarsia brunnea	Nem védett / indifferens
Diarsia dahlii	Védett
Diarsia mendica	Nem védett / indifferens
Diarsia rubi	Nem védett / indifferens
Diasemia reticularis	Nem védett / indifferens
Diaspidiotus alni	Nem védett / indifferens
Diaspidiotus bavaricus	Nem védett / indifferens
Diaspidiotus distinctus	Nem védett / indifferens
Diaspidiotus hungaricus	Nem védett / indifferens
Diaspidiotus osborni	Nem védett / indifferens
Diaspidiotus wuenni	Nem védett / indifferens
Diastictus vulneratus	Nem védett / indifferens
Elachista kalki	Nem védett / indifferens
Calliteara fascelina	Nem védett / indifferens
Diceratura ostrinana	Nem védett / indifferens
Diceratura roseofasciana	Nem védett / indifferens
Dicerca aenea	Védett
Dicerca alni	Védett
Dicerca berolinensis	Védett
Dicerca furcata	Védett
Dichagyris candelisequa	Védett
Dicheirotrichus gustavii	Nem védett / indifferens
Dicheirotrichus lacustris	Nem védett / indifferens
Dicheirotrichus rufithorax	Nem védett / indifferens
Dichelia histrionana	Nem védett / indifferens
Dichochrysa abdominalis	Nem védett / indifferens
Dichochrysa flavifrons	Nem védett / indifferens
Dichochrysa inornata	Nem védett / indifferens
Dichochrysa prasina	Nem védett / indifferens
Dichochrysa ventralis	Nem védett / indifferens
Dichochrysa zelleri	Nem védett / indifferens
Dichomeris barbella	Nem védett / indifferens
Dichomeris derasella	Nem védett / indifferens
Dichomeris limosellus	Nem védett / indifferens
Dichomeris marginella	Nem védett / indifferens
Dichomeris rasilella	Nem védett / indifferens
Dichomeris ustalella	Nem védett / indifferens
Dichonia aeruginea	Védett
Griposia aprilina	Nem védett / indifferens
Dichonia convergens	Nem védett / indifferens
Dichrooscytus gustavi	Nem védett / indifferens
Dichrooscytus rufipennis	Nem védett / indifferens
Dichrooscytus valesianus	Nem védett / indifferens
Dichrorampha acuminatana	Nem védett / indifferens
Dichrorampha aeratana	Nem védett / indifferens
Dichrorampha agilana	Nem védett / indifferens
Dichrorampha alpinana	Nem védett / indifferens
Dichrorampha cacaleana	Nem védett / indifferens
Dichrorampha cinerascens	Nem védett / indifferens
Dichrorampha cinerosana	Nem védett / indifferens
Dichrorampha consortana	Nem védett / indifferens
Dichrorampha distinctana	Nem védett / indifferens
Dichrorampha flavidorsana	Nem védett / indifferens
Dichrorampha gruneriana	Nem védett / indifferens
Dichrorampha vancouverana	Nem védett / indifferens
Dichrorampha heegerana	Nem védett / indifferens
Dichrorampha montanana	Nem védett / indifferens
Dichrorampha obscuratana	Nem védett / indifferens
Dichrorampha petiverella	Nem védett / indifferens
Dichrorampha plumbana	Nem védett / indifferens
Dichrorampha podoliensis	Nem védett / indifferens
Dichrorampha senectana	Nem védett / indifferens
Dichrorampha sequana	Nem védett / indifferens
Dichrorampha simpliciana	Nem védett / indifferens
Dichrostigma flavipes	Nem védett / indifferens
Dicranocephalus agilis	Nem védett / indifferens
Dicranocephalus albipes	Nem védett / indifferens
Dicranocephalus medius	Nem védett / indifferens
Dicranotropis divergens	Nem védett / indifferens
Dicranotropis hamata	Nem védett / indifferens
Dicranura ulmi	Védett
Dicronychus cinereus	Nem védett / indifferens
Dicronychus equiseti	Nem védett / indifferens
Dryudella tricolor	Nem védett / indifferens
Dicronychus equisetoides	Nem védett / indifferens
Dicronychus rubripes	Nem védett / indifferens
Dictyla convergens	Nem védett / indifferens
Dictyla echii	Nem védett / indifferens
Dictyla humuli	Nem védett / indifferens
Dictyla lupuli	Nem védett / indifferens
Dictyla nassata	Nem védett / indifferens
Dictyla platyoma	Nem védett / indifferens
Dictyla rotundata	Nem védett / indifferens
Dictyna arundinacea	Nem védett / indifferens
Dictyna civica	Nem védett / indifferens
Dictyna latens	Nem védett / indifferens
Dictyna pusilla	Nem védett / indifferens
Dictyna szaboi	Nem védett / indifferens
Dictyna uncinata	Nem védett / indifferens
Dictyna vicina	Nem védett / indifferens
Dictyonota strichnocera	Nem védett / indifferens
Dictyophara europaea	Nem védett / indifferens
Dictyophara multireticulata	Nem védett / indifferens
Dictyophara pannonica	Nem védett / indifferens
Dictyoptera aurora	Nem védett / indifferens
Dicycla oo	Nem védett / indifferens
Dicymbium nigrum	Nem védett / indifferens
Dicyphus constrictus	Nem védett / indifferens
Dicyphus epilobii	Nem védett / indifferens
Dicyphus errans	Nem védett / indifferens
Dicyphus geniculatus	Nem védett / indifferens
Dicyphus globulifer	Nem védett / indifferens
Dicyphus hyalinipennis	Nem védett / indifferens
Dicyphus pallidus	Nem védett / indifferens
Dicyphus stachydis	Nem védett / indifferens
Dicyrtomellus luctuosus	Nem védett / indifferens
Didea alneti	Nem védett / indifferens
Didea fasciata	Nem védett / indifferens
Didea intermedia	Nem védett / indifferens
Didineis crassicornis	Nem védett / indifferens
Didineis lunicornis	Nem védett / indifferens
Didineis wuestneii	Nem védett / indifferens
Dieckmanniellus gracilis	Nem védett / indifferens
Dieckmanniellus helveticus	Nem védett / indifferens
Dieckmanniellus nitidulus	Nem védett / indifferens
Dienerella argus	Nem védett / indifferens
Dienerella clathrata	Nem védett / indifferens
Dienerella costulata	Nem védett / indifferens
Dienerella elegans	Nem védett / indifferens
Dienerella elongata	Nem védett / indifferens
Dienerella filiformis	Nem védett / indifferens
Dienerella filum	Nem védett / indifferens
Dienerella ruficollis	Nem védett / indifferens
Digitivalva arnicella	Nem védett / indifferens
Dufourea dentiventris	Nem védett / indifferens
Digitivalva granitella	Nem védett / indifferens
Digitivalva pulicariae	Nem védett / indifferens
Digitivalva reticulella	Nem védett / indifferens
Digitivalva valeriella	Nem védett / indifferens
Dignathodon microcephalus	Nem védett / indifferens
Dignomus nitidus	Nem védett / indifferens
Dikraneura variata	Nem védett / indifferens
Dilacra fleischeri	Nem védett / indifferens
Dilacra luteipes	Nem védett / indifferens
Dilacra vilis	Nem védett / indifferens
Diloba caeruleocephala	Nem védett / indifferens
Dimetrota cadaverina	Nem védett / indifferens
Dimorphopterus doriae	Nem védett / indifferens
Dimorphopterus spinolae	Nem védett / indifferens
Dina apathyi	Nem védett / indifferens
Dina lineata	Nem védett / indifferens
Dina punctata	Nem védett / indifferens
Dinaraea aequata	Nem védett / indifferens
Dinaraea angustula	Nem védett / indifferens
Dinaraea arcana	Nem védett / indifferens
Dinaraea linearis	Nem védett / indifferens
Dinaraea petraea	Nem védett / indifferens
Dinarda dentata	Nem védett / indifferens
Dinarda maerkelii	Nem védett / indifferens
Dinarda pygmaea	Nem védett / indifferens
Dinetus pictus	Nem védett / indifferens
Dinodes decipiens	Védett
Dinodes decipiens ambiguus	Nem védett / indifferens
Dinoptera collaris	Nem védett / indifferens
Dinothenarus fossor	Nem védett / indifferens
Dinothenarus pubescens	Nem védett / indifferens
Diodesma subterranea	Nem védett / indifferens
Diodontus brevilabris	Nem védett / indifferens
Diodontus insidiosus	Nem védett / indifferens
Diodontus luperus	Nem védett / indifferens
Diodontus major	Nem védett / indifferens
Diodontus medius	Nem védett / indifferens
Diodontus minutus	Nem védett / indifferens
Diodontus tristis	Nem védett / indifferens
Diomphalus hispidulus	Nem védett / indifferens
Dionconotus confluens	Nem védett / indifferens
Dioryctria abietella	Nem védett / indifferens
Dioryctria schuetzeella	Nem védett / indifferens
Dioryctria simplicella	Nem védett / indifferens
Dioryctria sylvestrella	Nem védett / indifferens
Dioszeghyana schmidti	Fokozottan védett
Dioxys cincta	Nem védett / indifferens
Dioxys pannonica	Nem védett / indifferens
Dioxys tridentata	Nem védett / indifferens
Diplapion confluens	Nem védett / indifferens
Diplapion detritum	Nem védett / indifferens
Diplapion stolidum	Nem védett / indifferens
Eudonia lacustrata	Nem védett / indifferens
Diplocephalus alpinus strandi	Nem védett / indifferens
Diplocephalus cristatus	Nem védett / indifferens
Diplocephalus latifrons	Nem védett / indifferens
Diplocephalus picinus	Nem védett / indifferens
Diplocoelus fagi	Nem védett / indifferens
Diplocolenus bohemani	Nem védett / indifferens
Diplocolenus frauenfeldi	Nem védett / indifferens
Diplodoma adspersella	Nem védett / indifferens
Diplodoma laichartingella	Nem védett / indifferens
Diplostyla concolor	Nem védett / indifferens
Dipoena braccata	Nem védett / indifferens
Dipoena coracina	Nem védett / indifferens
Dipoena erythropus	Nem védett / indifferens
Phycosoma inornatum	Nem védett / indifferens
Dipoena melanogaster	Nem védett / indifferens
Dipoena nigroreticulata	Nem védett / indifferens
Dipoena torva	Nem védett / indifferens
Dipogon bifasciatus	Nem védett / indifferens
Dipogon monticolus	Nem védett / indifferens
Dipogon subintermedius	Nem védett / indifferens
Dipogon variegatus	Nem védett / indifferens
Dipogon vechti	Nem védett / indifferens
Dircaea australis	Nem védett / indifferens
Dirhinosia cervinella	Nem védett / indifferens
Discoelius dufourii	Nem védett / indifferens
Discoelius zonalis	Nem védett / indifferens
Venusia blomeri	Védett
Discus perspectivus	Nem védett / indifferens
Discus rotundatus	Nem védett / indifferens
Discus ruderatus	Védett
Dismodicus bifrons	Nem védett / indifferens
Dismodicus elevatus	Nem védett / indifferens
Disparalona rostrata	Nem védett / indifferens
Dissoleucas niveirostris	Nem védett / indifferens
Distoleon tetragrammicus	Védett
Ditropis pteridis	Nem védett / indifferens
Ditropsis flavipes	Nem védett / indifferens
Diura bicaudata	Nem védett / indifferens
Diurnea fagella	Nem védett / indifferens
Diurnea lipsiella	Nem védett / indifferens
Divaena haywardi	Védett
Dixus clypeatus	Védett
Dochmonota clancula	Nem védett / indifferens
Dochmonota rudiventris	Nem védett / indifferens
Dociostaurus brevicollis	Nem védett / indifferens
Dociostaurus maroccanus	Nem védett / indifferens
Dufourea inermis	Nem védett / indifferens
Dodecastichus geniculatus	Nem védett / indifferens
Dodecastichus inflatus	Nem védett / indifferens
Dodecastichus mastix	Nem védett / indifferens
Dodecastichus pulverulentus	Nem védett / indifferens
Dolerocypris fasciata	Nem védett / indifferens
Dolicharthria punctalis	Nem védett / indifferens
Dolichoderus quadripunctatus	Nem védett / indifferens
Dolichosoma femorale	Védett
Dolichosoma lineare	Nem védett / indifferens
Dolichosoma simile	Nem védett / indifferens
Dolichovespula adulterina	Nem védett / indifferens
Dolichovespula media	Nem védett / indifferens
Dolichovespula norwegica	Nem védett / indifferens
Dolichovespula omissa	Nem védett / indifferens
Dolichovespula saxonica	Nem védett / indifferens
Dolichovespula sylvestris	Nem védett / indifferens
Dolichurus corniculus	Nem védett / indifferens
Dolichus halensis	Nem védett / indifferens
Dolomedes fimbriatus	Védett
Dolomedes plantarius	Védett
Doloploca punctulana	Nem védett / indifferens
Dolycoris baccarum	Nem védett / indifferens
Domene scabricollis	Nem védett / indifferens
Donacaula forficella	Nem védett / indifferens
Donacaula mucronella	Nem védett / indifferens
Donacochara speciosa	Nem védett / indifferens
Donaspastus pannonicus	Nem védett / indifferens
Donus comatus	Nem védett / indifferens
Neoglanis cyrtus	Nem védett / indifferens
Donus dauci	Nem védett / indifferens
Neoglanis elegans	Nem védett / indifferens
Neoglanis intermedius	Nem védett / indifferens
Pachypera kraatzi	Nem védett / indifferens
Neoglanis maculatus	Nem védett / indifferens
Neoglanis oxalidis	Nem védett / indifferens
Neoglanis palumbarius	Nem védett / indifferens
Neoglanis rubi	Nem védett / indifferens
Neoglanis velutinus	Nem védett / indifferens
Neoglanis viennensis	Nem védett / indifferens
Donus zoilus	Nem védett / indifferens
Doratulina pallifrons	Nem védett / indifferens
Doratura concors	Nem védett / indifferens
Doratura exilis	Nem védett / indifferens
Doratura heterophyla	Nem védett / indifferens
Doratura homophyla	Nem védett / indifferens
Doratura impudica	Nem védett / indifferens
Doratura salina	Nem védett / indifferens
Doratura stylata	Nem védett / indifferens
Dorcatoma chrysomelina	Nem védett / indifferens
Dorcatoma dresdensis	Nem védett / indifferens
Dorcatoma flavicornis	Nem védett / indifferens
Dorcatoma robusta	Nem védett / indifferens
Dorcatoma serra	Nem védett / indifferens
Dorcatoma setosella	Nem védett / indifferens
Dorcus parallelipipedus	Védett
Doros profuges	Nem védett / indifferens
Dorycephalus baeri	Nem védett / indifferens
Dorypetalum degenerans	Nem védett / indifferens
Dorytomus dejeani	Nem védett / indifferens
Dorytomus dorsalis	Nem védett / indifferens
Dorytomus edoughensis	Nem védett / indifferens
Dorytomus filirostris	Nem védett / indifferens
Dorytomus hirtipennis	Nem védett / indifferens
Dorytomus ictor	Nem védett / indifferens
Dorytomus longimanus	Nem védett / indifferens
Dorytomus majalis	Nem védett / indifferens
Dorytomus melanophthalmus	Nem védett / indifferens
Dorytomus minutus	Nem védett / indifferens
Dorytomus nebulosus	Nem védett / indifferens
Dorytomus nordenskioldi	Nem védett / indifferens
Dorytomus occalescens	Nem védett / indifferens
Dorytomus puberulus	Nem védett / indifferens
Dorytomus rufatus	Nem védett / indifferens
Dorytomus salicinus	Nem védett / indifferens
Dorytomus salicis	Nem védett / indifferens
Dorytomus schoenherri	Nem védett / indifferens
Dorytomus suratus	Nem védett / indifferens
Dorytomus taeniatus	Nem védett / indifferens
Dorytomus tortrix	Nem védett / indifferens
Dorytomus tremulae	Nem védett / indifferens
Dorytomus villosulus	Nem védett / indifferens
Doydirhynchus austriacus	Nem védett / indifferens
Drapetes cinctus	Nem védett / indifferens
Drapetisca socialis	Nem védett / indifferens
Drassodes cupreus	Nem védett / indifferens
Drassodes heeri	Nem védett / indifferens
Drassodes hypocrita	Nem védett / indifferens
Drassodes lapidosus	Nem védett / indifferens
Drassodes pubescens	Nem védett / indifferens
Drassodes villosus	Nem védett / indifferens
Drassyllus lutetianus	Nem védett / indifferens
Drassyllus praeficus	Nem védett / indifferens
Drassyllus pumilus	Nem védett / indifferens
Drassyllus pusillus	Nem védett / indifferens
Drassyllus villicus	Nem védett / indifferens
Drassyllus vinealis	Nem védett / indifferens
Drasterius bimaculatus	Nem védett / indifferens
Dreissena polymorpha	Nem védett / indifferens
Drepana curvatula	Nem védett / indifferens
Drepana falcataria	Nem védett / indifferens
Drepanepteryx algida	Nem védett / indifferens
Drepanepteryx phalaenoides	Nem védett / indifferens
Dreposcia umbrina	Nem védett / indifferens
Drilus concolor	Nem védett / indifferens
Drobacia banatica	Védett
Dromaeolus barnabita	Nem védett / indifferens
Dromius agilis	Nem védett / indifferens
Dromius angustus	Nem védett / indifferens
Dromius fenestratus	Nem védett / indifferens
Dromius quadraticollis	Nem védett / indifferens
Dromius quadrimaculatus	Nem védett / indifferens
Dromius schneideri	Nem védett / indifferens
Phyllodrepa ioptera	Nem védett / indifferens
Phyllodrepa vilis	Nem védett / indifferens
Drusilla canaliculata	Nem védett / indifferens
Drusus annulatus	Nem védett / indifferens
Drusus biguttatus	Nem védett / indifferens
Drusus trifidus	Védett
Drymonia dodonaea	Nem védett / indifferens
Drymonia obliterata	Nem védett / indifferens
Drymonia querna	Nem védett / indifferens
Drymonia ruficornis	Nem védett / indifferens
Drymonia velitaris	Védett
Drymus brunneus	Nem védett / indifferens
Drymus latus	Nem védett / indifferens
Drymus pilicornis	Nem védett / indifferens
Drymus ryeii	Nem védett / indifferens
Drymus sylvaticus	Nem védett / indifferens
Dryobotodes eremita	Nem védett / indifferens
Dryobotodes monochroma	Nem védett / indifferens
Dryocoetes autographus	Nem védett / indifferens
Dryocoetes hectographus	Nem védett / indifferens
Dryocoetes villosus	Nem védett / indifferens
Dryocopus martius	Védett
Dryodurgades dlabolai	Nem védett / indifferens
Dryodurgades reticulatus	Nem védett / indifferens
Dryomys nitedula	Fokozottan védett
Dryophilocoris flavoquadrimaculatus	Nem védett / indifferens
Dryophilocoris luteus	Nem védett / indifferens
Dryophilus pusillus	Nem védett / indifferens
Dryophthorus corticalis	Nem védett / indifferens
Dryops anglicanus	Nem védett / indifferens
Dryops auriculatus	Nem védett / indifferens
Dryops ernesti	Nem védett / indifferens
Dryops griseus	Nem védett / indifferens
Dryops nitidulus	Nem védett / indifferens
Dryops rufipes	Nem védett / indifferens
Dryops similaris	Nem védett / indifferens
Dryops viennensis	Nem védett / indifferens
Drypta dentata	Nem védett / indifferens
Dufourea minuta	Nem védett / indifferens
Dufourea vulgaris	Nem védett / indifferens
Dufouriellus ater	Nem védett / indifferens
Dunhevedia crassa	Nem védett / indifferens
Duponchelia fovealis	Nem védett / indifferens
Duvalius gebhardti	Védett
Duvalius hungaricus	Védett
Dypterygia scabriuscula	Nem védett / indifferens
Dyroderes umbraculatus	Nem védett / indifferens
Dysauxes ancilla	Nem védett / indifferens
Dyschiriodes aeneus	Nem védett / indifferens
Dyschiriodes agnatus	Nem védett / indifferens
Dyschirius angustatus	Nem védett / indifferens
Dyschirius arenosus	Nem védett / indifferens
Dyschirius benedikti	Nem védett / indifferens
Dyschiriodes bonellii	Nem védett / indifferens
Dyschiriodes chalceus	Nem védett / indifferens
Dyschiriodes chalybaeus	Nem védett / indifferens
Dyschiriodes chalybaeus gibbifrons	Nem védett / indifferens
Dyschirius digitatus	Nem védett / indifferens
Dyschiriodes extensus	Nem védett / indifferens
Dyschiriodes globosus	Nem védett / indifferens
Dyschiriodes intermedius	Nem védett / indifferens
Dyschiriodes laeviusculus	Nem védett / indifferens
Dyschiriodes lafertei	Nem védett / indifferens
Dyschirius latipennis	Nem védett / indifferens
Dyschirius luticola	Nem védett / indifferens
Dyschiriodes nitidus	Nem védett / indifferens
Dyschirius obscurus	Nem védett / indifferens
Dyschiriodes parallelus	Nem védett / indifferens
Dyschiriodes parallelus ruficornis	Nem védett / indifferens
Dyschiriodes politus	Nem védett / indifferens
Dyschiriodes pusillus	Nem védett / indifferens
Dyschiriodes rufipes	Nem védett / indifferens
Dyschiriodes salinus	Nem védett / indifferens
Dyschiriodes salinus striatopunctatus	Nem védett / indifferens
Dyschiriodes strumosus	Nem védett / indifferens
Dyschiriodes substriatus	Nem védett / indifferens
Dyschiriodes substriatus priscus	Nem védett / indifferens
Dyschiriodes tristis	Nem védett / indifferens
Dyscia conspersaria	Védett
Dysdera crocata	Nem védett / indifferens
Dysdera erythrina lantosquensis	Nem védett / indifferens
Dysdera hungarica	Nem védett / indifferens
Dysdera longirostris	Nem védett / indifferens
Dysdera ninnii	Nem védett / indifferens
Dysdera westringi	Nem védett / indifferens
Dysepicritus rufescens	Nem védett / indifferens
Dysgonia algira	Nem védett / indifferens
Dysmicoccus walkeri	Nem védett / indifferens
Dyspessa ulula	Nem védett / indifferens
Dystebenna stephensi	Nem védett / indifferens
Dytiscus circumcinctus	Nem védett / indifferens
Dytiscus circumflexus	Nem védett / indifferens
Dytiscus dimidiatus	Nem védett / indifferens
Dytiscus latissimus	Védett
Dytiscus marginalis	Nem védett / indifferens
Eana argentana	Nem védett / indifferens
Eana canescana	Nem védett / indifferens
Eana derivana	Nem védett / indifferens
Eana incanana	Nem védett / indifferens
Eana osseana	Nem védett / indifferens
Earias clorana	Nem védett / indifferens
Earias vernana	Nem védett / indifferens
Ebaeus appendiculatus	Nem védett / indifferens
Ebaeus ater	Nem védett / indifferens
Ebaeus coerulescens	Nem védett / indifferens
Ebaeus flavicornis	Nem védett / indifferens
Ebaeus gibbus	Nem védett / indifferens
Ebaeus pedicularius	Nem védett / indifferens
Ebarrius cognatus	Nem védett / indifferens
Ebarrius interstinctus	Nem védett / indifferens
Eblisia minor	Nem védett / indifferens
Ebulea crocealis	Nem védett / indifferens
Ebulea testacealis	Nem védett / indifferens
Ecclisopteryx dalecarlica	Nem védett / indifferens
Ecclisopteryx madida	Nem védett / indifferens
Eccopisa effractella	Nem védett / indifferens
Ecdyonurus aurantiacus	Nem védett / indifferens
Ecdyonurus dispar	Nem védett / indifferens
Ecdyonurus insignis	Nem védett / indifferens
Ecdyonurus siveci	Nem védett / indifferens
Ecdyonurus starmachi	Nem védett / indifferens
Ecdyonurus subalpinus	Nem védett / indifferens
Ecdyonurus submontanus	Nem védett / indifferens
Ecdyonurus torrentis	Nem védett / indifferens
Ecdyonurus venosus	Nem védett / indifferens
Echemus angustifrons	Nem védett / indifferens
Echinocamptus echinatus	Nem védett / indifferens
Echinocamptus georgevitchi	Nem védett / indifferens
Echinocamptus luenensis	Nem védett / indifferens
Echinodera valida	Nem védett / indifferens
Ecliptopera capitata	Nem védett / indifferens
Ecliptopera silaceata	Nem védett / indifferens
Ecnomus tenellus	Nem védett / indifferens
Ecpyrrhorrhoe rubiginalis	Nem védett / indifferens
Ectemnius borealis	Nem védett / indifferens
Ectemnius cavifrons	Nem védett / indifferens
Ectemnius cephalotes	Nem védett / indifferens
Ectemnius confinis	Nem védett / indifferens
Ectemnius continuus	Nem védett / indifferens
Ectemnius crassicornis	Nem védett / indifferens
Ectemnius dives	Nem védett / indifferens
Ectemnius fossorius	Nem védett / indifferens
Ectemnius guttatus	Nem védett / indifferens
Ectemnius lapidarius	Nem védett / indifferens
Ectemnius lituratus	Nem védett / indifferens
Ectemnius meridionalis	Nem védett / indifferens
Ectemnius nigritarsus	Nem védett / indifferens
Ectemnius rubicola	Nem védett / indifferens
Ectemnius ruficornis	Nem védett / indifferens
Ectemnius rugifer	Nem védett / indifferens
Ectemnius schlettereri	Nem védett / indifferens
Ectemnius spinipes	Nem védett / indifferens
Ectinosoma abrau	Nem védett / indifferens
Ectocyclops phaleratus	Nem védett / indifferens
Ectoedemia agrimoniae	Nem védett / indifferens
Ectoedemia albifasciella	Nem védett / indifferens
Ectoedemia angulifasciella	Nem védett / indifferens
Ectoedemia arcuatella	Nem védett / indifferens
Ectoedemia argyropeza	Nem védett / indifferens
Ectoedemia atricollis	Nem védett / indifferens
Ectoedemia atrifrontella	Nem védett / indifferens
Ectoedemia caradjai	Nem védett / indifferens
Ectoedemia cerris	Nem védett / indifferens
Ectoedemia contorta	Nem védett / indifferens
Ectoedemia decentella	Nem védett / indifferens
Ectoedemia gilvipennella	Nem védett / indifferens
Ectoedemia hannoverella	Nem védett / indifferens
Ectoedemia heringi	Nem védett / indifferens
Ectoedemia hexapetalae	Nem védett / indifferens
Ectoedemia intimella	Nem védett / indifferens
Ectoedemia klimeschi	Nem védett / indifferens
Ectoedemia liebwerdella	Nem védett / indifferens
Ectoedemia liechtensteini	Nem védett / indifferens
Ectoedemia longicaudella	Nem védett / indifferens
Ectoedemia louisella	Nem védett / indifferens
Ectoedemia mahalebella	Nem védett / indifferens
Ectoedemia occultella	Nem védett / indifferens
Ectoedemia preisseckeri	Nem védett / indifferens
Ectoedemia rubivora	Nem védett / indifferens
Ectoedemia rufifrontella	Nem védett / indifferens
Ectoedemia septembrella	Nem védett / indifferens
Ectoedemia sericopeza	Nem védett / indifferens
Ectoedemia spinosella	Nem védett / indifferens
Ectoedemia spiraeae	Nem védett / indifferens
Ectoedemia subbimaculella	Nem védett / indifferens
Ectoedemia turbidella	Nem védett / indifferens
Ectohomoeosoma kasyellum	Nem védett / indifferens
Ectropis crepuscularia	Nem védett / indifferens
Edaphus beszedesi	Nem védett / indifferens
Edwardsiana ampliata	Nem védett / indifferens
Edwardsiana avellanae	Nem védett / indifferens
Edwardsiana candidula	Nem védett / indifferens
Edwardsiana crataegi	Nem védett / indifferens
Edwardsiana diversa	Nem védett / indifferens
Edwardsiana flavescens	Nem védett / indifferens
Edwardsiana geometrica	Nem védett / indifferens
Edwardsiana gratiosa	Nem védett / indifferens
Edwardsiana hippocastani	Nem védett / indifferens
Edwardsiana lamellaris	Nem védett / indifferens
Edwardsiana lethierryi	Nem védett / indifferens
Edwardsiana plebeja	Nem védett / indifferens
Edwardsiana prunicola	Nem védett / indifferens
Edwardsiana rosae	Nem védett / indifferens
Edwardsiana ruthenica	Nem védett / indifferens
Edwardsiana salicicola	Nem védett / indifferens
Edwardsiana spinigera	Nem védett / indifferens
Edwardsiana staminata	Nem védett / indifferens
Edwardsiana stehliki	Nem védett / indifferens
Egira conspicillaris	Nem védett / indifferens
Casmerodius albus	Fokozottan védett
Egretta garzetta	Fokozottan védett
Egretta gularis	Védett
Eidophasia messingiella	Nem védett / indifferens
Eidophasia syenitella	Nem védett / indifferens
Eilema caniola	Nem védett / indifferens
Eilema complana	Nem védett / indifferens
Eilema depressa	Nem védett / indifferens
Eilema griseola	Nem védett / indifferens
Eilema lurideola	Nem védett / indifferens
Eilema lutarella	Nem védett / indifferens
Eilema palliatella	Nem védett / indifferens
Eilema pseudocomplana	Nem védett / indifferens
Eilema pygmaeola	Nem védett / indifferens
Eilema pygmaeola pallifrons	Nem védett / indifferens
Eilema sororcula	Nem védett / indifferens
Eilicrinia cordiaria	Nem védett / indifferens
Eilicrinia trinotata	Nem védett / indifferens
Elachista adscitella	Nem védett / indifferens
Elachista albidella	Nem védett / indifferens
Elachista alpinella	Nem védett / indifferens
Elachista anserinella	Nem védett / indifferens
Elachista apicipunctella	Nem védett / indifferens
Elachista argentella	Nem védett / indifferens
Elachista atricomella	Nem védett / indifferens
Elachista bedellella	Nem védett / indifferens
Elachista biatomella	Nem védett / indifferens
Elachista bisulcella	Nem védett / indifferens
Elachista canapennella	Nem védett / indifferens
Elachista chrysodesmella	Nem védett / indifferens
Elachista cingillella	Nem védett / indifferens
Elachista collitella	Nem védett / indifferens
Elachista contaminatella	Nem védett / indifferens
Elachista disemiella	Nem védett / indifferens
Elachista dispilella	Nem védett / indifferens
Elachista dispunctella	Nem védett / indifferens
Elachista elegans	Nem védett / indifferens
Elachista gangabella	Nem védett / indifferens
Elachista gleichenella	Nem védett / indifferens
Elachista gormella	Nem védett / indifferens
Elachista griseella	Nem védett / indifferens
Elachista hedemanni	Nem védett / indifferens
Elachista heringi	Nem védett / indifferens
Elachista herrichii	Nem védett / indifferens
Elachista humilis	Nem védett / indifferens
Elachista juliensis	Nem védett / indifferens
Elachista kilmunella	Nem védett / indifferens
Elachista klimeschiella	Nem védett / indifferens
Elachista luticomella	Nem védett / indifferens
Elachista martinii	Nem védett / indifferens
Elachista monosemiella	Nem védett / indifferens
Elachista nitidulella	Nem védett / indifferens
Elachista poae	Nem védett / indifferens
Elachista pollinariella	Nem védett / indifferens
Elachista pollutella	Nem védett / indifferens
Elachista pomerana	Nem védett / indifferens
Elachista pullicomella	Nem védett / indifferens
Elachista quadripunctella	Nem védett / indifferens
Elachista revinctella	Nem védett / indifferens
Elachista rudectella	Nem védett / indifferens
Elachista rufocinerea	Nem védett / indifferens
Elachista scirpi	Nem védett / indifferens
Elachista serricornis	Nem védett / indifferens
Elachista spumella	Nem védett / indifferens
Elachista squamosella	Nem védett / indifferens
Elachista subalbidella	Nem védett / indifferens
Elachista subnigrella	Nem védett / indifferens
Elachista subocellea	Nem védett / indifferens
Elachista szocsi	Nem védett / indifferens
Elachista triatomea	Nem védett / indifferens
Elachista triseriatella	Nem védett / indifferens
Elachista unifasciella	Nem védett / indifferens
Elampus albipennis	Nem védett / indifferens
Elampus ambiguus	Nem védett / indifferens
Elampus bidens	Nem védett / indifferens
Elampus constrictus	Nem védett / indifferens
Elampus foveatus	Nem védett / indifferens
Elampus pyrosomus	Nem védett / indifferens
Elampus sanzii	Nem védett / indifferens
Elampus scutellaris	Nem védett / indifferens
Elampus spinus	Nem védett / indifferens
Elaphe longissima	Védett
Elaphoidella elaphoides	Nem védett / indifferens
Elaphoidella gracilis	Nem védett / indifferens
Elaphoidella jeanneli	Nem védett / indifferens
Elaphoidella pseudojeanneli	Nem védett / indifferens
Elaphoidella pseudojeanneli aggtelekiensis	Nem védett / indifferens
Elaphoidella simplex	Nem védett / indifferens
Elaphoidella simplex szegedensis	Nem védett / indifferens
Elaphria venustula	Nem védett / indifferens
Tachyura diabrachys	Nem védett / indifferens
Sphaerotachys hoemorrhoidalis	Nem védett / indifferens
Tachyura parvula	Nem védett / indifferens
Tachyura quadrisignata	Nem védett / indifferens
Elaphrus aureus	Nem védett / indifferens
Elaphrus cupreus	Nem védett / indifferens
Elaphrus riparius	Nem védett / indifferens
Elaphrus uliginosus	Nem védett / indifferens
Elaphrus ullrichii	Nem védett / indifferens
Elasmostethus interstinctus	Nem védett / indifferens
Elasmostethus minor	Nem védett / indifferens
Elasmotropis testacea	Nem védett / indifferens
Elasmucha ferrugata	Nem védett / indifferens
Elasmucha fieberi	Nem védett / indifferens
Elasmucha grisea	Nem védett / indifferens
Elater ferrugineus	Védett
Elatobia fuliginosella	Nem védett / indifferens
Electrogena affinis	Nem védett / indifferens
Electrogena lateralis	Nem védett / indifferens
Electrogena ujhelyii	Nem védett / indifferens
Electrophaes corylata	Nem védett / indifferens
Eledona agricola	Nem védett / indifferens
Eledonoprius armatus	Nem védett / indifferens
Elegia atrifasciella	Nem védett / indifferens
Elegia fallax	Nem védett / indifferens
Elegia similella	Nem védett / indifferens
Eliomys quercinus	Nem védett / indifferens
Ellescus bipunctatus	Nem védett / indifferens
Ellescus infirmus	Nem védett / indifferens
Ellescus scanicus	Nem védett / indifferens
Elmis aenea	Nem védett / indifferens
Elmis latreillei	Nem védett / indifferens
Elmis maugetii	Nem védett / indifferens
Elmis obscura	Nem védett / indifferens
Elmis rioloides	Nem védett / indifferens
Elodes hausmanni	Nem védett / indifferens
Elodes johni	Nem védett / indifferens
Elodes marginata	Nem védett / indifferens
Elodes minuta	Nem védett / indifferens
Elophila nymphaeata	Nem védett / indifferens
Elophila rivulalis	Nem védett / indifferens
Elymana sulphurella	Nem védett / indifferens
Ematheudes punctella	Nem védett / indifferens
Ematurga atomaria	Nem védett / indifferens
Emberiza cia	Fokozottan védett
Emberiza cirlus	Védett
Emberiza citrinella	Védett
Emberiza hortulana	Fokozottan védett
Emberiza leucocephalos	Védett
Emberiza melanocephala	Védett
Emberiza pusilla	Védett
Emberiza schoeniclus	Védett
Emberiza schoeniclus intermedia	Védett
Emberiza schoeniclus stresemanni	Védett
Emberiza schoeniclus tschusii	Védett
Emberiza schoeniclus ukrainae	Védett
Emblethis brachynotus	Nem védett / indifferens
Emblethis ciliatus	Nem védett / indifferens
Emblethis denticollis	Nem védett / indifferens
Emblethis griseus	Nem védett / indifferens
Emblethis verbasci	Nem védett / indifferens
Emblyna brevidens	Nem védett / indifferens
Emblyna mitis	Nem védett / indifferens
Emelyanoviana mollicula	Nem védett / indifferens
Emmelia trabealis	Nem védett / indifferens
Emmelina argoteles	Nem védett / indifferens
Emmelina monodactyla	Nem védett / indifferens
Coptotriche angusticollella	Nem védett / indifferens
Coptotriche gaunacella	Nem védett / indifferens
Coptotriche heinemanni	Nem védett / indifferens
Coptotriche marginea	Nem védett / indifferens
Coptotriche szoecsi	Nem védett / indifferens
Empicoris baerensprungi	Nem védett / indifferens
Empicoris culiciformis	Nem védett / indifferens
Empicoris gracilentus	Nem védett / indifferens
Empicoris mediterraneus	Nem védett / indifferens
Empicoris tabellarius	Nem védett / indifferens
Empicoris vagabundus	Nem védett / indifferens
Empoasca affinis	Nem védett / indifferens
Empoasca decipiens	Nem védett / indifferens
Empoasca pteridis	Nem védett / indifferens
Empoasca vitis	Nem védett / indifferens
Emus hirtus	Nem védett / indifferens
Emys orbicularis	Védett
Ena montana	Védett
Enallagma cyathigerum	Nem védett / indifferens
Enalodroma hepatica	Nem védett / indifferens
Enantiocephalus cornutus	Nem védett / indifferens
Enantiulus nanus	Nem védett / indifferens
Enantiulus tatranus	Nem védett / indifferens
Enantiulus tatranus evae	Nem védett / indifferens
Enargia abluta	Védett
Enargia paleacea	Nem védett / indifferens
Enarmonia formosana	Nem védett / indifferens
Encephalus complicans	Nem védett / indifferens
Endomia tenuicollis	Nem védett / indifferens
Endomychus coccineus	Nem védett / indifferens
Endophloeus marcovichianus	Nem védett / indifferens
Endothenia gentianaeana	Nem védett / indifferens
Endothenia lapideana	Nem védett / indifferens
Endothenia marginana	Nem védett / indifferens
Endothenia nigricostana	Nem védett / indifferens
Endothenia oblongana	Nem védett / indifferens
Endothenia quadrimaculana	Nem védett / indifferens
Endothenia sororiana	Nem védett / indifferens
Endothenia ustulana	Nem védett / indifferens
Endotricha flammealis	Nem védett / indifferens
Endromis versicolora	Védett
Endrosis sarcitrella	Nem védett / indifferens
Enedreytes hilaris	Nem védett / indifferens
Enedreytes sepicola	Nem védett / indifferens
Enicmus brevicornis	Nem védett / indifferens
Enicmus fungicola	Nem védett / indifferens
Enicmus histrio	Nem védett / indifferens
Enicmus rugosus	Nem védett / indifferens
Enicmus testaceus	Nem védett / indifferens
Enicmus transversus	Nem védett / indifferens
Enicopus hirtus	Nem védett / indifferens
Ennearthron cornutum	Nem védett / indifferens
Ennearthron pruinosulum	Nem védett / indifferens
Ennomos alniaria	Nem védett / indifferens
Ennomos autumnaria	Nem védett / indifferens
Ennomos erosaria	Nem védett / indifferens
Ennomos fuscantaria	Nem védett / indifferens
Ennomos quercaria	Védett
Ennomos quercinaria	Nem védett / indifferens
Enochrus affinis	Nem védett / indifferens
Enochrus ater	Nem védett / indifferens
Enochrus bicolor	Nem védett / indifferens
Enochrus coarctatus	Nem védett / indifferens
Enochrus fuscipennis	Nem védett / indifferens
Enochrus halophilus	Nem védett / indifferens
Enochrus hamifer	Nem védett / indifferens
Enochrus melanocephalus	Nem védett / indifferens
Enochrus ochropterus	Nem védett / indifferens
Enochrus quadripunctatus	Nem védett / indifferens
Enochrus testaceus	Nem védett / indifferens
Enoplognatha latimana	Nem védett / indifferens
Enoplognatha mandibularis	Nem védett / indifferens
Enoplognatha mordax	Nem védett / indifferens
Enoplognatha oelandica	Nem védett / indifferens
Enoplognatha ovata	Nem védett / indifferens
Enoplognatha serratosignata	Nem védett / indifferens
Enoplognatha thoracica	Nem védett / indifferens
Enoplops scapha	Nem védett / indifferens
Enoplopus dentipes	Nem védett / indifferens
Entelecara acuminata	Nem védett / indifferens
Entelecara congenera	Nem védett / indifferens
Entelecara flavipes	Nem védett / indifferens
Entelecara omissa	Nem védett / indifferens
Entephria caesiata	Védett
Entephria cyanata	Fokozottan védett
Enteucha acetosae	Nem védett / indifferens
Entomobora crassitarsis	Nem védett / indifferens
Entomognathus brevis	Nem védett / indifferens
Entomognathus dentifer	Nem védett / indifferens
Entomosericus concinnus	Nem védett / indifferens
Entomosericus kaufmanni	Nem védett / indifferens
Eobania vermiculata	Nem védett / indifferens
Eoferreola manticata	Nem védett / indifferens
Eoferreola rhombica	Nem védett / indifferens
Eohardya fraudulentus	Nem védett / indifferens
Eoleptestheria ticinensis	Nem védett / indifferens
Eosolenobia manni	Nem védett / indifferens
Epacromius coerulipes	Védett
Epacromius tergestinus	Védett
Epagoge grotiana	Nem védett / indifferens
Epaphius secalis	Nem védett / indifferens
Epascestria pustulalis	Nem védett / indifferens
Epauloecus unicolor	Nem védett / indifferens
Epeoloides coecutiens	Nem védett / indifferens
Epeolus cruciger	Nem védett / indifferens
Epeolus fasciatus	Nem védett / indifferens
Epeolus schummeli	Nem védett / indifferens
Epeolus variegatus	Nem védett / indifferens
Epeorus assimilis	Nem védett / indifferens
Epermenia aequidentellus	Nem védett / indifferens
Epermenia chaerophyllella	Nem védett / indifferens
Epermenia illigerella	Nem védett / indifferens
Epermenia insecurellus	Nem védett / indifferens
Epermenia petrusellus	Nem védett / indifferens
Epermenia pontificella	Nem védett / indifferens
Epermenia strictellus	Nem védett / indifferens
Ephemera danica	Nem védett / indifferens
Ephemera glaucops	Nem védett / indifferens
Ephemera lineata	Nem védett / indifferens
Ephemera vulgata	Nem védett / indifferens
Serratella ignita	Nem védett / indifferens
Serratella mesoleuca	Védett
Ephemerella mucronata	Nem védett / indifferens
Ephemerella notata	Nem védett / indifferens
Ephestia elutella	Nem védett / indifferens
Ephestia kuehniella	Nem védett / indifferens
Ephestia unicolorella woodiella	Nem védett / indifferens
Ephestia welseriella	Nem védett / indifferens
Ephippiger ephippiger	Nem védett / indifferens
Ephistemus globulus	Nem védett / indifferens
Ephistemus reitteri	Nem védett / indifferens
Ephoron virgo	Védett
Ephysteris inustella	Nem védett / indifferens
Ephysteris promptella	Nem védett / indifferens
Epibactra sareptana	Nem védett / indifferens
Epiblema cnicicolana	Nem védett / indifferens
Epiblema confusana	Nem védett / indifferens
Epiblema costipunctana	Nem védett / indifferens
Epiblema foenella	Nem védett / indifferens
Epiblema grandaevana	Nem védett / indifferens
Epiblema graphana	Nem védett / indifferens
Epiblema hepaticana	Nem védett / indifferens
Epiblema junctana	Nem védett / indifferens
Epiblema mendiculana	Nem védett / indifferens
Epiblema obscurana	Nem védett / indifferens
Epiblema scutulana	Nem védett / indifferens
Epiblema similana	Nem védett / indifferens
Epiblema sticticana	Nem védett / indifferens
Epiblema turbidana	Nem védett / indifferens
Epicallima bruandella	Nem védett / indifferens
Epicallima formosella	Nem védett / indifferens
Epicauta rufidorsum	Nem védett / indifferens
Epichnopterix kovacsi	Nem védett / indifferens
Epichnopterix plumella	Nem védett / indifferens
Tropinota hirta	Nem védett / indifferens
Epidiaspis leperii	Nem védett / indifferens
Epierus comptus	Nem védett / indifferens
Epilecta linogrisea	Nem védett / indifferens
Epimecia ustula	Védett
Epimyrma ravouxi	Nem védett / indifferens
Epinotia abbreviana	Nem védett / indifferens
Epinotia bilunana	Nem védett / indifferens
Epinotia brunnichana	Nem védett / indifferens
Epinotia cruciana	Nem védett / indifferens
Epinotia demarniana	Nem védett / indifferens
Epinotia festivana	Nem védett / indifferens
Epinotia fraternana	Nem védett / indifferens
Epinotia granitana	Nem védett / indifferens
Capricornia boisduvaliana	Nem védett / indifferens
Epinotia kochiana	Nem védett / indifferens
Epinotia maculana	Nem védett / indifferens
Epinotia nanana	Nem védett / indifferens
Epinotia nigricana	Nem védett / indifferens
Epinotia nisella	Nem védett / indifferens
Epinotia pusillana	Nem védett / indifferens
Epinotia pygmaeana	Nem védett / indifferens
Epinotia ramella	Nem védett / indifferens
Epinotia rhomboidella	Nem védett / indifferens
Epinotia rubiginosana	Nem védett / indifferens
Epinotia signatana	Nem védett / indifferens
Epinotia solandriana	Nem védett / indifferens
Epinotia sordidana	Nem védett / indifferens
Epinotia subocellana	Nem védett / indifferens
Epinotia tedella	Nem védett / indifferens
Epinotia tenerana	Nem védett / indifferens
Epinotia tetraquetrana	Nem védett / indifferens
Epinotia thapsiana	Nem védett / indifferens
Epinotia trigonella	Nem védett / indifferens
Epione repandaria	Nem védett / indifferens
Epione vespertaria	Nem védett / indifferens
Epipsilia latens	Nem védett / indifferens
Epirranthis diversata	Védett
Epirrhoe alternata	Nem védett / indifferens
Epirrhoe galiata	Nem védett / indifferens
Epirrhoe hastulata	Nem védett / indifferens
Epirrhoe molluginata	Nem védett / indifferens
Epirrhoe pupillata	Nem védett / indifferens
Epirrhoe rivata	Nem védett / indifferens
Epirrhoe tristata	Nem védett / indifferens
Epirrita autumnata	Nem védett / indifferens
Epirrita christyi	Nem védett / indifferens
Epirrita dilutata	Nem védett / indifferens
Epischnia prodromella	Nem védett / indifferens
Episcythrastis tetricella	Nem védett / indifferens
Episema glaucina	Nem védett / indifferens
Episema tersa	Nem védett / indifferens
Episernus granulatus	Nem védett / indifferens
Episinus angulatus	Nem védett / indifferens
Episinus truncatus	Nem védett / indifferens
Epistrophe cryptica	Nem védett / indifferens
Epistrophe diaphana	Nem védett / indifferens
Epistrophe eligans	Nem védett / indifferens
Epistrophe flava	Nem védett / indifferens
Epistrophe grossulariae	Nem védett / indifferens
Epistrophe melanostoma	Nem védett / indifferens
Epistrophe nitidicollis	Nem védett / indifferens
Epistrophe ochrostoma	Nem védett / indifferens
Epistrophella euchroma	Nem védett / indifferens
Episyron albonotatus	Nem védett / indifferens
Episyron arrogans	Nem védett / indifferens
Episyron candiotus	Nem védett / indifferens
Episyron gallicus	Nem védett / indifferens
Episyron gallicus tertius	Nem védett / indifferens
Episyron rufipes	Nem védett / indifferens
Episyrphus balteatus	Nem védett / indifferens
Epitheca bimaculata	Védett
Epomis dejeanii	Nem védett / indifferens
Eptesicus nilssoni	Védett
Eptesicus serotinus	Védett
Epuraea aestiva	Nem védett / indifferens
Epuraea angustula	Nem védett / indifferens
Epuraea biguttata	Nem védett / indifferens
Epuraea distincta	Nem védett / indifferens
Epuraea fageticola	Nem védett / indifferens
Epuraea fuscicollis	Nem védett / indifferens
Epuraea guttata	Nem védett / indifferens
Epuraea limbata	Nem védett / indifferens
Epuraea longula	Nem védett / indifferens
Epuraea marseuli	Nem védett / indifferens
Epuraea melanocephala	Nem védett / indifferens
Epuraea melina	Nem védett / indifferens
Epuraea neglecta	Nem védett / indifferens
Epuraea pallescens	Nem védett / indifferens
Epuraea pygmaea	Nem védett / indifferens
Epuraea rufomarginata	Nem védett / indifferens
Epuraea silacea	Nem védett / indifferens
Epuraea terminalis	Nem védett / indifferens
Epuraea unicolor	Nem védett / indifferens
Epuraea variegata	Nem védett / indifferens
Erannis ankeraria	Fokozottan védett
Erannis defoliaria	Nem védett / indifferens
Erebia aethiops	Védett
Erebia ligea	Védett
Erebia medusa	Védett
Eremobia ochroleuca	Nem védett / indifferens
Eremobina pabulatricula	Nem védett / indifferens
Eremochlorita hungarica	Nem védett / indifferens
Eremocoris abietis	Nem védett / indifferens
Eremocoris fenestratus	Nem védett / indifferens
Eremocoris plebejus	Nem védett / indifferens
Eremocoris podagricus	Nem védett / indifferens
Eremodrina gilva	Védett
Eremophila alpestris	Védett
Eremophila alpestris flava	Védett
Eresus kollari	Védett
Eretes sticticus	Nem védett / indifferens
Ergasilus briani	Nem védett / indifferens
Ergasilus gibbus	Nem védett / indifferens
Ergasilus nanus	Nem védett / indifferens
Ergasilus sieboldi	Nem védett / indifferens
Ergates faber	Védett
Erichsonius cinerascens	Nem védett / indifferens
Erichsonius signaticornis	Nem védett / indifferens
Erichsonius subopacus	Nem védett / indifferens
Erigone atra	Nem védett / indifferens
Erigone dentipalpis	Nem védett / indifferens
Erigonella ignobilis	Nem védett / indifferens
Erigonoplus globipes	Nem védett / indifferens
Erinaceus concolor	Védett
Erinaceus concolor roumanicus	Védett
Erinaceus europaeus	Nem védett / indifferens
Eriococcus buxi	Nem védett / indifferens
Eriocrania semipurpurella	Nem védett / indifferens
Eriocrania sparrmannella	Nem védett / indifferens
Dyseriocrania subpurpurella	Nem védett / indifferens
Eriogaster catax	Védett
Eriogaster lanestris	Védett
Eriogaster rimicola	Védett
Eriopeltis festucae	Nem védett / indifferens
Eriopeltis lichtensteini	Nem védett / indifferens
Eriopeltis stammeri	Nem védett / indifferens
Eriopsela quadrana	Nem védett / indifferens
Eriozona syrphoides	Nem védett / indifferens
Eristalinus aeneus	Nem védett / indifferens
Eristalinus sepulchralis	Nem védett / indifferens
Eristalis abusiva	Nem védett / indifferens
Eristalis alpina	Nem védett / indifferens
Eristalis arbustorum	Nem védett / indifferens
Eristalis horticola	Nem védett / indifferens
Eristalis interrupta	Nem védett / indifferens
Eristalis intricaria	Nem védett / indifferens
Eristalis jugorum	Nem védett / indifferens
Eristalis pertinax	Nem védett / indifferens
Eristalis rupium	Nem védett / indifferens
Eristalis similis	Nem védett / indifferens
Eristalis tenax	Nem védett / indifferens
Eristalis vitripennis	Nem védett / indifferens
Erithacus rubecula	Védett
Ernobius abietinus	Nem védett / indifferens
Ernobius abietis	Nem védett / indifferens
Ernobius angusticollis	Nem védett / indifferens
Ernobius kiesenwetteri	Nem védett / indifferens
Ernobius longicornis	Nem védett / indifferens
Ernobius mollis	Nem védett / indifferens
Ernobius nigrinus	Nem védett / indifferens
Ernobius pini	Nem védett / indifferens
Ernodes articularis	Nem védett / indifferens
Ernoporicus caucasicus	Nem védett / indifferens
Ernoporicus fagi	Nem védett / indifferens
Ernoporus tilliae	Nem védett / indifferens
Ero aphana	Nem védett / indifferens
Ero cambridgei	Nem védett / indifferens
Ero furcata	Nem védett / indifferens
Ero tuberculata	Nem védett / indifferens
Erotesis baltica	Nem védett / indifferens
Erotettix cyane	Nem védett / indifferens
Erpobdella nigricollis	Nem védett / indifferens
Erpobdella octoculata	Nem védett / indifferens
Erpobdella testacea	Nem védett / indifferens
Erpobdella vilnensis	Nem védett / indifferens
Errastunus notatifrons	Nem védett / indifferens
Errastunus ocellaris	Nem védett / indifferens
Errhomenus brachypterus	Nem védett / indifferens
Erynnis tages	Nem védett / indifferens
Erythria aureola	Nem védett / indifferens
Erythria montandoni	Nem védett / indifferens
Erythromma najas	Nem védett / indifferens
Erythromma viridulum	Nem védett / indifferens
Esolus angustatus	Nem védett / indifferens
Esolus parallelepipedus	Nem védett / indifferens
Esox lucius	Nem védett / indifferens
Dasycera krueperella	Nem védett / indifferens
Dasycera oliviella	Nem védett / indifferens
Fagotia daudebartii thermalis	Nem védett / indifferens
Fagotia daudebartii acicularis	Védett
Fagotia esperi	Védett
Esymus merdarius	Nem védett / indifferens
Eteobalea albiapicella	Nem védett / indifferens
Eteobalea anonymella	Nem védett / indifferens
Eteobalea beata	Nem védett / indifferens
Eteobalea serratella	Nem védett / indifferens
Eteobalea intermediella	Nem védett / indifferens
Eteobalea isabellella	Nem védett / indifferens
Eteobalea tririvella	Nem védett / indifferens
Ethelcus denticulatus	Nem védett / indifferens
Ethmia bipunctella	Nem védett / indifferens
Ethmia candidella	Nem védett / indifferens
Ethmia dodecea	Nem védett / indifferens
Ethmia fumidella	Nem védett / indifferens
Ethmia haemorrhoidella	Nem védett / indifferens
Ethmia iranella	Nem védett / indifferens
Ethmia pusiella	Nem védett / indifferens
Ethmia quadrillella	Nem védett / indifferens
Ethmia terminella	Nem védett / indifferens
Etiella zinckenella	Nem védett / indifferens
Euaesthetus bipunctatus	Nem védett / indifferens
Euaesthetus laeviusculus	Nem védett / indifferens
Euaesthetus ruficapillus	Nem védett / indifferens
Euaesthetus superlatus	Nem védett / indifferens
Eublemma amoena	Nem védett / indifferens
Eublemma minutata	Nem védett / indifferens
Eublemma ostrina	Nem védett / indifferens
Eublemma panonica	Fokozottan védett
Eublemma parva	Nem védett / indifferens
Eublemma purpurina	Nem védett / indifferens
Eublemma rosea	Védett
Eubrachium pusillum	Nem védett / indifferens
Eubranchipus grubei	Nem védett / indifferens
Eubria palustris	Nem védett / indifferens
Eubrychius velutus	Nem védett / indifferens
Euspilapteryx auroguttella	Nem védett / indifferens
Eucarphia vinetella	Nem védett / indifferens
Eucarta amethystina	Nem védett / indifferens
Eucarta virgo	Nem védett / indifferens
Eucera caspica	Nem védett / indifferens
Eucera caspica perezi	Nem védett / indifferens
Eucera cinerea	Nem védett / indifferens
Eucera clypeata	Nem védett / indifferens
Eucera crysopyga	Nem védett / indifferens
Eucera curvitarsis	Nem védett / indifferens
Eucera dalmatica	Nem védett / indifferens
Eucera excisa	Nem védett / indifferens
Eucera iterrupta	Nem védett / indifferens
Eucera longicornis	Nem védett / indifferens
Eucera nigrifacies	Nem védett / indifferens
Eucera nitidiventris	Nem védett / indifferens
Eucera parvicornis	Nem védett / indifferens
Eucera parvula	Nem védett / indifferens
Eucera seminuda	Nem védett / indifferens
Eucera similis	Nem védett / indifferens
Eucera spectabilis	Nem védett / indifferens
Eucera taurica	Nem védett / indifferens
Eucera tuberculata	Nem védett / indifferens
Euchaetias egle	Nem védett / indifferens
Euchalcia consona	Nem védett / indifferens
Euchalcia modestoides	Védett
Euchalcia variabilis	Védett
Euchoeca nebulata	Nem védett / indifferens
Euchorthippus declivus	Nem védett / indifferens
Euchorthippus pulvinatus	Nem védett / indifferens
Euchroeus purpuratus	Nem védett / indifferens
Euchromius bella	Nem védett / indifferens
Euchromius ocellea	Nem védett / indifferens
Euchromius superbellus	Nem védett / indifferens
Eucinetus haemorrhoidalis	Nem védett / indifferens
Euclidia glyphica	Nem védett / indifferens
Callistege mi	Nem védett / indifferens
Gonospileia triquetra	Nem védett / indifferens
Eucnemis capucina	Nem védett / indifferens
Eucoeliodes mirabilis	Nem védett / indifferens
Euconnus chrysocomus	Nem védett / indifferens
Euconnus claviger	Nem védett / indifferens
Euconnus denticornis	Nem védett / indifferens
Euconnus fimetarius	Nem védett / indifferens
Euconnus hirticollis	Nem védett / indifferens
Euconnus pragensis	Nem védett / indifferens
Euconnus pubicollis	Nem védett / indifferens
Euconnus rutilipennis	Nem védett / indifferens
Euconnus wetterhallii	Nem védett / indifferens
Euconomelus lepidus	Nem védett / indifferens
Euconulus fulvus	Nem védett / indifferens
Euconulus praticola	Nem védett / indifferens
Eucosma aemulana	Nem védett / indifferens
Eucosma albidulana	Nem védett / indifferens
Eucosma aspidiscana	Nem védett / indifferens
Eucosma balatonana	Nem védett / indifferens
Eucosma campoliliana	Nem védett / indifferens
Eucosma cana	Nem védett / indifferens
Eucosma conformana	Nem védett / indifferens
Eucosma conterminana	Nem védett / indifferens
Eucosma cumulana	Nem védett / indifferens
Eucosma fervidana	Nem védett / indifferens
Eucosma hohenwartiana	Nem védett / indifferens
Eucosma lacteana	Nem védett / indifferens
Eucosma lugubrana	Nem védett / indifferens
Eucosma messingiana	Nem védett / indifferens
Eucosma metzneriana	Nem védett / indifferens
Eucosma obumbratana	Nem védett / indifferens
Eucosma pupillana	Nem védett / indifferens
Eucosma tripoliana	Nem védett / indifferens
Eucosma tundrana	Nem védett / indifferens
Eucosma wimmerana	Nem védett / indifferens
Eucosmomorpha albersana	Nem védett / indifferens
Eucrostes indigenata	Védett
Eucyclops denticulatus	Nem védett / indifferens
Eucyclops macruroides	Nem védett / indifferens
Eucyclops macrurus	Nem védett / indifferens
Eucyclops serrulatus	Nem védett / indifferens
Eucyclops speratus	Nem védett / indifferens
Eucypris ornata	Nem védett / indifferens
Eucypris pigra	Nem védett / indifferens
Eucypris virens	Nem védett / indifferens
Eudarcia pagenstecherella	Nem védett / indifferens
Eudemis porphyrana	Nem védett / indifferens
Eudemis profundana	Nem védett / indifferens
Eudiaptomus gracilis	Nem védett / indifferens
Eudiaptomus graciloides	Nem védett / indifferens
Eudiaptomus transsylvanicus	Nem védett / indifferens
Eudiaptomus vulgaris	Nem védett / indifferens
Eudiaptomus zachariasi	Nem védett / indifferens
Eudiplister planulus	Nem védett / indifferens
Eudonia laetella	Nem védett / indifferens
Eudonia mercurella	Nem védett / indifferens
Eudonia murana	Nem védett / indifferens
Eudonia sudetica	Nem védett / indifferens
Eudonia truncicolella	Nem védett / indifferens
Eudontomyzon danfordi	Fokozottan védett
Eudontomyzon mariae	Fokozottan védett
Euglenes oculatus	Nem védett / indifferens
Euglenes pygmaeus	Nem védett / indifferens
Eugnorisma depuncta	Nem védett / indifferens
Eugnosta lathoniana	Nem védett / indifferens
Eugnosta magnificana	Nem védett / indifferens
Eugraphe sigma	Nem védett / indifferens
Euheptaulacus porcellus	Nem védett / indifferens
Euheptaulacus sus	Nem védett / indifferens
Euheptaulacus villosus	Nem védett / indifferens
Euhyponomeuta stannella	Nem védett / indifferens
Euides speciosa	Nem védett / indifferens
Eulamprotes atrella	Nem védett / indifferens
Eulamprotes superbella	Nem védett / indifferens
Eulamprotes unicolorella	Nem védett / indifferens
Eulamprotes wilkella	Nem védett / indifferens
Glaresis rufa	Védett
Eulecanium caraganae	Nem védett / indifferens
Eulecanium ciliatum	Nem védett / indifferens
Eulecanium douglasi	Nem védett / indifferens
Eulecanium franconicum	Nem védett / indifferens
Eulecanium sericeum	Nem védett / indifferens
Eulecanium tiliae	Nem védett / indifferens
Eulia ministrana	Nem védett / indifferens
Eulithis mellinata	Nem védett / indifferens
Eulithis populata	Nem védett / indifferens
Eulithis prunata	Nem védett / indifferens
Eulithis pyraliata	Nem védett / indifferens
Eulithis testata	Nem védett / indifferens
Eumannia lepraria	Védett
Eumenes coarctatus	Nem védett / indifferens
Eumenes coronatus	Nem védett / indifferens
Eumenes dubius	Nem védett / indifferens
Eumenes lunulatus	Nem védett / indifferens
Eumenes mediterraneus	Nem védett / indifferens
Eumenes papillarius	Nem védett / indifferens
Eumenes pedunculatus	Nem védett / indifferens
Eumenes pomiformis	Nem védett / indifferens
Eumenes sareptanus	Nem védett / indifferens
Eumenes sareptanus insolatus	Nem védett / indifferens
Eumenes subpomiformis	Nem védett / indifferens
Eumerus flavitarsis	Nem védett / indifferens
Eumerus grandis	Nem védett / indifferens
Eumerus hungaricus	Nem védett / indifferens
Eumerus longicornis	Nem védett / indifferens
Eumerus ornatus	Nem védett / indifferens
Eumerus ovatus	Nem védett / indifferens
Eumerus ruficornis	Nem védett / indifferens
Eumerus sabulonum	Nem védett / indifferens
Eumerus sinuatus	Nem védett / indifferens
Eumerus sogdianus	Nem védett / indifferens
Eumerus strigatus	Nem védett / indifferens
Eumerus tarsalis	Nem védett / indifferens
Eumerus tauricus	Nem védett / indifferens
Eumerus tricolor	Nem védett / indifferens
Eumerus tuberculatus	Nem védett / indifferens
Euodynerus curictensis	Nem védett / indifferens
Euodynerus dantici	Nem védett / indifferens
Euodynerus egregius	Nem védett / indifferens
Euodynerus egregius unimaculatus	Nem védett / indifferens
Euodynerus notatus	Nem védett / indifferens
Euodynerus notatus pubescens	Nem védett / indifferens
Euodynerus posticus	Nem védett / indifferens
Euodynerus quadrifasciatus	Nem védett / indifferens
Euodynerus quadrifasciatus simplex	Nem védett / indifferens
Euomphalia strigella	Nem védett / indifferens
Euoniticellus fulvus	Nem védett / indifferens
Euoniticellus pallipes	Nem védett / indifferens
Euonthophagus amyntas	Nem védett / indifferens
Euonthophagus amyntas alces	Nem védett / indifferens
Euophrys frontalis	Nem védett / indifferens
Eupelix cuspidata	Nem védett / indifferens
Eupeodes corollae	Nem védett / indifferens
Eupeodes flaviceps	Nem védett / indifferens
Eupeodes lapponicus	Nem védett / indifferens
Eupeodes latifasciatus	Nem védett / indifferens
Eupeodes latilunulatus	Nem védett / indifferens
Eupeodes luniger	Nem védett / indifferens
Eupeodes nitens	Nem védett / indifferens
Euphydryas aurinia	Védett
Euphydryas maturna	Védett
Euphyia biangulata	Nem védett / indifferens
Euphyia frustata	Nem védett / indifferens
Euphyia scripturata	Védett
Euphyia unangulata	Nem védett / indifferens
Eupithecia abbreviata	Nem védett / indifferens
Eupithecia abietaria	Nem védett / indifferens
Eupithecia absinthiata	Nem védett / indifferens
Eupithecia actaeata	Védett
Eupithecia addictata	Nem védett / indifferens
Eupithecia alliaria	Nem védett / indifferens
Eupithecia analoga	Nem védett / indifferens
Eupithecia assimilata	Nem védett / indifferens
Eupithecia breviculata	Nem védett / indifferens
Eupithecia catharinae	Nem védett / indifferens
Eupithecia cauchiata	Nem védett / indifferens
Eupithecia centaureata	Nem védett / indifferens
Eupithecia denotata	Nem védett / indifferens
Eupithecia denticulata	Védett
Eupithecia distinctaria	Nem védett / indifferens
Eupithecia dodoneata	Nem védett / indifferens
Eupithecia egenaria	Nem védett / indifferens
Eupithecia ericeata	Nem védett / indifferens
Eupithecia exiguata	Nem védett / indifferens
Eupithecia expallidata	Nem védett / indifferens
Eupithecia extraversaria	Nem védett / indifferens
Eupithecia extremata	Nem védett / indifferens
Eupithecia gemellata	Nem védett / indifferens
Eupithecia graphata	Védett
Eupithecia gueneata	Nem védett / indifferens
Eupithecia haworthiata	Nem védett / indifferens
Eupithecia icterata	Nem védett / indifferens
Eupithecia immundata	Nem védett / indifferens
Eupithecia impurata	Nem védett / indifferens
Eupithecia indigata	Nem védett / indifferens
Eupithecia innotata	Nem védett / indifferens
Eupithecia insigniata	Nem védett / indifferens
Eupithecia intricata	Nem védett / indifferens
Eupithecia inturbata	Nem védett / indifferens
Eupithecia irriguata	Nem védett / indifferens
Eupithecia lanceata	Nem védett / indifferens
Eupithecia laquaearia	Nem védett / indifferens
Eupithecia lariciata	Nem védett / indifferens
Eupithecia linariata	Nem védett / indifferens
Eupithecia millefoliata	Nem védett / indifferens
Eupithecia nanata	Nem védett / indifferens
Eupithecia ochridata	Nem védett / indifferens
Eupithecia orphnata	Nem védett / indifferens
Eupithecia pauxillaria	Nem védett / indifferens
Eupithecia pernotata	Nem védett / indifferens
Eupithecia pimpinellata	Nem védett / indifferens
Eupithecia plumbeolata	Nem védett / indifferens
Eupithecia pusillata	Nem védett / indifferens
Eupithecia pygmaeata	Nem védett / indifferens
Eupithecia pyreneata	Nem védett / indifferens
Eupithecia satyrata	Nem védett / indifferens
Eupithecia schiefereri	Nem védett / indifferens
Eupithecia selinata	Nem védett / indifferens
Eupithecia semigraphata	Nem védett / indifferens
Eupithecia silenicolata	Nem védett / indifferens
Eupithecia simpliciata	Nem védett / indifferens
Eupithecia sinuosaria	Nem védett / indifferens
Eupithecia spadiceata	Nem védett / indifferens
Eupithecia subfuscata	Nem védett / indifferens
Eupithecia subumbrata	Nem védett / indifferens
Eupithecia succenturiata	Nem védett / indifferens
Eupithecia tantillaria	Nem védett / indifferens
Eupithecia tenuiata	Nem védett / indifferens
Eupithecia tripunctaria	Nem védett / indifferens
Eupithecia trisignaria	Nem védett / indifferens
Eupithecia unedonata	Nem védett / indifferens
Eupithecia valerianata	Nem védett / indifferens
Eupithecia venosata	Nem védett / indifferens
Eupithecia veratraria	Nem védett / indifferens
Eupithecia virgaureata	Nem védett / indifferens
Eupithecia vulgata	Nem védett / indifferens
Euplagia quadripunctaria	Védett
Euplectus bescidicus	Nem védett / indifferens
Euplectus bonvouloiri	Nem védett / indifferens
Euplectus bonvouloiri rosae	Nem védett / indifferens
Euplectus brunneus	Nem védett / indifferens
Euplectus decipiens	Nem védett / indifferens
Euplectus frater	Nem védett / indifferens
Euplectus frivaldszkyi	Nem védett / indifferens
Euplectus karstenii	Nem védett / indifferens
Euplectus kirbii	Nem védett / indifferens
Euplectus nanus	Nem védett / indifferens
Euplectus piceus	Nem védett / indifferens
Euplectus punctatus	Nem védett / indifferens
Euplectus sanguineus	Nem védett / indifferens
Euplectus signatus	Nem védett / indifferens
Eupleurus subterraneus	Nem védett / indifferens
Euplexia lucipara	Nem védett / indifferens
Euplocamus anthracinalis	Nem védett / indifferens
Eupoecilia ambiguella	Nem védett / indifferens
Eupoecilia angustana	Nem védett / indifferens
Eupoecilia sanguisorbana	Nem védett / indifferens
Eupolybothrus transsylvanicus	Nem védett / indifferens
Eupolybothrus tridentinus	Nem védett / indifferens
Protaetia affinis	Védett
Euproctis chrysorrhoea	Nem védett / indifferens
Euproctis similis	Nem védett / indifferens
Eupsilia transversa	Nem védett / indifferens
Eupterycyba jucunda	Nem védett / indifferens
Eupteryx adspersa	Nem védett / indifferens
Eupteryx artemisiae	Nem védett / indifferens
Eupteryx atropunctata	Nem védett / indifferens
Eupteryx aurata	Nem védett / indifferens
Eupteryx calcarata	Nem védett / indifferens
Eupteryx collina	Nem védett / indifferens
Eupteryx cyclops	Nem védett / indifferens
Eupteryx florida	Nem védett / indifferens
Eupteryx heydenii	Nem védett / indifferens
Eupteryx immaculatifrons	Nem védett / indifferens
Eupteryx lelievrei	Nem védett / indifferens
Eupteryx melissae	Nem védett / indifferens
Eupteryx notata	Nem védett / indifferens
Eupteryx stachydearum	Nem védett / indifferens
Eupteryx tenella	Nem védett / indifferens
Eupteryx thoulessi	Nem védett / indifferens
Eupteryx urticae	Nem védett / indifferens
Eupteryx vittata	Nem védett / indifferens
Eurhadina concinna	Nem védett / indifferens
Eurhadina kirschbaumi	Nem védett / indifferens
Eurhadina loewii	Nem védett / indifferens
Eurhadina pulchella	Nem védett / indifferens
Eurhodope cirrigerella	Nem védett / indifferens
Eurhodope rosella	Nem védett / indifferens
Euripersia europaea	Nem védett / indifferens
Euripersia tomlini	Nem védett / indifferens
Eurocoelotes inermis	Nem védett / indifferens
Eurois occulta	Nem védett / indifferens
Euroleon nostras	Nem védett / indifferens
Agonum antennarium	Nem védett / indifferens
Agonum fuliginosum	Nem védett / indifferens
Agonum gracile	Nem védett / indifferens
Agonum micans	Nem védett / indifferens
Agonum piceum	Nem védett / indifferens
Agonum scitulum	Nem védett / indifferens
Agonum thoreyi	Nem védett / indifferens
Europiella albipennis	Nem védett / indifferens
Europiella alpina	Nem védett / indifferens
Europiella artemisiae	Nem védett / indifferens
Europiella decolor	Nem védett / indifferens
Eurrhypara hortulata	Nem védett / indifferens
Eurrhypis pollinalis	Nem védett / indifferens
Eurybregma nigrolineata	Nem védett / indifferens
Eurycercus lamellatus	Nem védett / indifferens
Eurycolpus flaveolus	Nem védett / indifferens
Eurydema dominulus	Nem védett / indifferens
Eurydema fieberi	Nem védett / indifferens
Eurydema oleracea	Nem védett / indifferens
Eurydema ornata	Nem védett / indifferens
Eurydema ventralis	Nem védett / indifferens
Eurygaster austriaca	Nem védett / indifferens
Eurygaster dilaticollis	Nem védett / indifferens
Eurygaster maura	Nem védett / indifferens
Eurygaster testudinaria	Nem védett / indifferens
Eurylophella karelica	Fokozottan védett
Euryopicoris nitidus	Nem védett / indifferens
Euryopis flavomaculata	Nem védett / indifferens
Euryopis laeta	Nem védett / indifferens
Euryopis quinqueguttata	Nem védett / indifferens
Euryopis saukea	Nem védett / indifferens
Euryporus picipes	Nem védett / indifferens
Euryptilium gillmeisteri	Nem védett / indifferens
Euryptilium saxonicum	Nem védett / indifferens
Eurysella brunnea	Nem védett / indifferens
Eurysa lineata	Nem védett / indifferens
Eurysula lurida	Nem védett / indifferens
Eurytemora velox	Nem védett / indifferens
Eurythyrea aurata	Védett
Eurythyrea austriaca	Nem védett / indifferens
Eurythyrea quercus	Fokozottan védett
Euryusa brachelytra	Nem védett / indifferens
Euryusa castanoptera	Nem védett / indifferens
Euryusa coarctata	Nem védett / indifferens
Euryusa optabilis	Nem védett / indifferens
Euryusa sinuata	Nem védett / indifferens
Eysarcoris aeneus	Nem védett / indifferens
Eysarcoris fabricii	Nem védett / indifferens
Eysarcoris ventralis	Nem védett / indifferens
Euscelidius schenckii	Nem védett / indifferens
Euscelidius variegatus	Nem védett / indifferens
Euscelis distinguendus	Nem védett / indifferens
Euscelis incisus	Nem védett / indifferens
Euscelis lineolatus	Nem védett / indifferens
Euscelis venosus	Nem védett / indifferens
Eusomus ovulum	Nem védett / indifferens
Eusphalerum limbatum	Nem védett / indifferens
Eusphalerum longipenne	Nem védett / indifferens
Eusphalerum luteum	Nem védett / indifferens
Eusphalerum marshami	Nem védett / indifferens
Eusphalerum minutum	Nem védett / indifferens
Eusphalerum pallens	Nem védett / indifferens
Eusphalerum primulae	Nem védett / indifferens
Eusphalerum rectangulum	Nem védett / indifferens
Eusphalerum semicoleoptratum	Nem védett / indifferens
Eusphalerum signatum	Nem védett / indifferens
Eusphalerum sorbi	Nem védett / indifferens
Eusphalerum tenenbaumi	Nem védett / indifferens
Euspilotus perrisi	Nem védett / indifferens
Eustenancistrocerus transitorius	Nem védett / indifferens
Eustroma reticulata	Nem védett / indifferens
Eustrophus dermestoides	Nem védett / indifferens
Eutelia adulatrix	Nem védett / indifferens
Eutheia wetterhalli	Nem védett / indifferens
Eutheia plicata	Nem védett / indifferens
Eutheia scydmaenoides orientalis	Nem védett / indifferens
Euthrix potatoria	Nem védett / indifferens
Eutrichapion ervi	Nem védett / indifferens
Eutrichapion facetum	Nem védett / indifferens
Eutrichapion gribodoi	Nem védett / indifferens
Eutrichapion melancholicum	Nem védett / indifferens
Eutrichapion punctiger	Nem védett / indifferens
Eutrichapion viciae	Nem védett / indifferens
Eutrichapion vorax	Nem védett / indifferens
Euxoa aquilina	Nem védett / indifferens
Euxoa birivia	Védett
Euxoa conspicua	Nem védett / indifferens
Euxoa decora	Védett
Euxoa distinguenda	Védett
Euxoa eruta	Nem védett / indifferens
Euxoa hastifera pomazensis	Védett
Euxoa nigricans	Nem védett / indifferens
Euxoa nigrofusca	Nem védett / indifferens
Euxoa obelisca	Nem védett / indifferens
Euxoa recussa	Nem védett / indifferens
Euxoa segnilis	Nem védett / indifferens
Euxoa temera	Nem védett / indifferens
Euxoa tritici	Nem védett / indifferens
Euxoa vitta	Védett
Euzonitis auricoma	Nem védett / indifferens
Euzonitis fulvipennis	Nem védett / indifferens
Euzonitis quadrimaculata	Nem védett / indifferens
Euzonitis sexmaculata	Nem védett / indifferens
Euzophera bigella	Nem védett / indifferens
Euzophera cinerosella	Nem védett / indifferens
Euzophera fuliginosella	Nem védett / indifferens
Euzophera pinguis	Nem védett / indifferens
Euzopherodes charlottae	Nem védett / indifferens
Euzopherodes vapidella	Nem védett / indifferens
Evacanthus acuminatus	Nem védett / indifferens
Evacanthus interruptus	Nem védett / indifferens
Evagetes alamannicus	Nem védett / indifferens
Evagetes crassicornis	Nem védett / indifferens
Evagetes dubius	Nem védett / indifferens
Evagetes elongatus	Nem védett / indifferens
Evagetes gibbulus	Nem védett / indifferens
Evagetes littoralis	Nem védett / indifferens
Evagetes pectinipes	Nem védett / indifferens
Evagetes pontomoravicus	Nem védett / indifferens
Evagetes proximus	Nem védett / indifferens
Evagetes sahlbergi	Nem védett / indifferens
Glaucidium passerinum	Védett
Evagetes siculus	Nem védett / indifferens
Evagetes subglaber	Nem védett / indifferens
Evagetes subnudus	Nem védett / indifferens
Evagetes tumidosus	Nem védett / indifferens
Evarcha arcuata	Nem védett / indifferens
Evarcha falcata	Nem védett / indifferens
Evarcha laetabunda	Nem védett / indifferens
Evergestis aenealis	Nem védett / indifferens
Evergestis extimalis	Nem védett / indifferens
Evergestis forficalis	Nem védett / indifferens
Evergestis frumentalis	Nem védett / indifferens
Evergestis limbata	Nem védett / indifferens
Evergestis pallidata	Nem védett / indifferens
Evergestis politalis	Nem védett / indifferens
Evodinus clathratus	Nem védett / indifferens
Exaeretia culcitella	Nem védett / indifferens
Exaeretia preisseckeri	Nem védett / indifferens
Exaesiopus grossipes	Nem védett / indifferens
Exapion compactum	Nem védett / indifferens
Exapion corniculatum	Nem védett / indifferens
Exapion difficile	Nem védett / indifferens
Exapion elongatulum	Nem védett / indifferens
Exapion formaneki	Nem védett / indifferens
Exapion fuscirostre	Nem védett / indifferens
Exitianus capicola	Nem védett / indifferens
Exitianus taeniaticeps	Nem védett / indifferens
Exocentrus adspersus	Nem védett / indifferens
Exocentrus lusitanus	Nem védett / indifferens
Exocentrus punctipennis	Nem védett / indifferens
Exocentrus stierlini	Nem védett / indifferens
Exochomus nigromaculatus	Nem védett / indifferens
Exoteleia dodecella	Nem védett / indifferens
Fabaeformis wegelini	Nem védett / indifferens
Fabaeformiscandona acuminata	Nem védett / indifferens
Fabaeformiscandona brevicornis	Nem védett / indifferens
Fabaeformiscandona fabaeformis	Nem védett / indifferens
Fabaeformiscandona fragilis	Nem védett / indifferens
Fabaeformiscandona holzkampfi	Nem védett / indifferens
Fabaeformiscandona protzi	Nem védett / indifferens
Fabiola pokornyi	Nem védett / indifferens
Fagivorina arenaria	Nem védett / indifferens
Fagocyba cruenta	Nem védett / indifferens
Fagocyba douglasi	Nem védett / indifferens
Fagocyba inquinata	Nem védett / indifferens
Falagria caesa	Nem védett / indifferens
Falagria splendens	Nem védett / indifferens
Falagria sulcatula	Nem védett / indifferens
Falagrioma thoracica	Nem védett / indifferens
Falcaria lacertinaria	Nem védett / indifferens
Falco cherrug	Fokozottan védett
Falco cherrug cyanopus	Fokozottan védett
Falco columbarius	Védett
Falco columbarius aesalon	Védett
Falco eleonorae	Fokozottan védett
Falco naumanni	Fokozottan védett
Falco peregrinus	Fokozottan védett
Falco peregrinus calidus	Fokozottan védett
Falco subbuteo	Védett
Falco tinnunculus	Védett
Falco vespertinus	Fokozottan védett
Falcotoya minuscula	Nem védett / indifferens
Falseuncaria degreyana	Nem védett / indifferens
Falseuncaria ruficiliana	Nem védett / indifferens
Farsus dubius	Nem védett / indifferens
Faustina faustina	Nem védett / indifferens
Faustina illyrica	Nem védett / indifferens
Felis silvestris	Fokozottan védett
Ferdinandea cuprea	Nem védett / indifferens
Ferdinandea ruficornis	Nem védett / indifferens
Ferreola diffinis	Nem védett / indifferens
Ferreroaspis hungarica	Nem védett / indifferens
Ferrissia clessiniana	Nem védett / indifferens
Ficedula albicollis	Védett
Ficedula hypoleuca	Védett
Ficedula parva	Fokozottan védett
Fieberiella florii	Nem védett / indifferens
Filatima spurcella	Nem védett / indifferens
Filatima tephritidella	Nem védett / indifferens
Flavohelodes flavicollis	Nem védett / indifferens
Florodelphax leptosoma	Nem védett / indifferens
Floronia bucculenta	Nem védett / indifferens
Forcipata citrinella	Nem védett / indifferens
Forcipata forcipata	Nem védett / indifferens
Formica cinerea	Nem védett / indifferens
Formica cunicularia	Nem védett / indifferens
Formica exsecta	Nem védett / indifferens
Formica fusca	Nem védett / indifferens
Formica gagates	Nem védett / indifferens
Formica pratensis	Nem védett / indifferens
Formica pressilabris	Nem védett / indifferens
Formica rufa	Nem védett / indifferens
Formica rufibarbis	Nem védett / indifferens
Formica sanguinea	Nem védett / indifferens
Formica truncorum	Nem védett / indifferens
Formicoxenus nitidulus	Nem védett / indifferens
Formiphantes lepthyphantiformis	Nem védett / indifferens
Foucartia liturata	Nem védett / indifferens
Foucartia ptochioides	Nem védett / indifferens
Fratercula arctica	Védett
Fratercula arctica grabae	Védett
Friedlanderia cicatricella	Nem védett / indifferens
Fringilla coelebs	Védett
Fringilla montifringilla	Védett
Frontinellina frutetorum	Nem védett / indifferens
Fruticicola fruticum	Nem védett / indifferens
Fulica atra	Nem védett / indifferens
Fulvoclysia nerminae	Nem védett / indifferens
Furcula bicuspis	Védett
Furcula bifida	Nem védett / indifferens
Furcula furcula	Nem védett / indifferens
Gabrius appendiculatus	Nem védett / indifferens
Gabrius astutus	Nem védett / indifferens
Gabrius breviventer	Nem védett / indifferens
Gabrius exiguus	Nem védett / indifferens
Gabrius exspectatus	Nem védett / indifferens
Gabrius femoralis	Nem védett / indifferens
Gabrius lividipes	Nem védett / indifferens
Gabrius nigritulus	Nem védett / indifferens
Gabrius osseticus	Nem védett / indifferens
Gabrius piliger	Nem védett / indifferens
Gabrius ravasinii	Nem védett / indifferens
Gabrius splendidulus	Nem védett / indifferens
Gabrius suffragani	Nem védett / indifferens
Gabrius toxotes	Nem védett / indifferens
Gabrius trossulus	Nem védett / indifferens
Gabronthus limbatus	Nem védett / indifferens
Gabronthus thermarum	Nem védett / indifferens
Gagitodes sagittata	Védett
Galba truncatula	Nem védett / indifferens
Galeatus affinis	Nem védett / indifferens
Galeatus decorus	Nem védett / indifferens
Galeatus maculatus	Nem védett / indifferens
Galeatus sinuatus	Nem védett / indifferens
Galerida cristata	Védett
Galerida cristata tenuirostris	Védett
Galleria mellonella	Nem védett / indifferens
Gallinago gallinago	Fokozottan védett
Gallinago media	Fokozottan védett
Gallinula chloropus	Védett
Gambusia affinis	Nem védett / indifferens
Gambusia holbrooki	Nem védett / indifferens
Gampsocleis glabra	Védett
Gampsocoris culicinus	Nem védett / indifferens
Gampsocoris punctipes	Nem védett / indifferens
Gargara genistae	Nem védett / indifferens
Garrulus glandarius	Nem védett / indifferens
Garrulus glandarius albipectus	Nem védett / indifferens
Gasterocercus depressirostris	Védett
Gasterosteus aculeatus	Nem védett / indifferens
Gastrallus immarginatus	Nem védett / indifferens
Tituboea macropus	Védett
Gastrallus laevigatus	Nem védett / indifferens
Gastrodes abietum	Nem védett / indifferens
Gastrodes grossipes	Nem védett / indifferens
Gastropacha populifolia	Nem védett / indifferens
Gastropacha quercifolia	Nem védett / indifferens
Gauropterus fulgidus	Nem védett / indifferens
Gaurotes virginea	Nem védett / indifferens
Gavia arctica	Védett
Gavia immer	Védett
Gavia stellata	Védett
Geina didactyla	Nem védett / indifferens
Gelechia asinella	Nem védett / indifferens
Gelechia basipunctella	Nem védett / indifferens
Gelechia muscosella	Nem védett / indifferens
Gelechia nigra	Nem védett / indifferens
Gelechia rhombella	Nem védett / indifferens
Gelechia rhombelliformis	Nem védett / indifferens
Gelechia sabinellus	Nem védett / indifferens
Gelechia scotinella	Nem védett / indifferens
Gelechia senticetella	Nem védett / indifferens
Gelechia sestertiella	Nem védett / indifferens
Gelechia sororculella	Nem védett / indifferens
Gelechia turpella	Nem védett / indifferens
Gelochelidon nilotica	Védett
Geocoris ater	Nem védett / indifferens
Geocoris dispar	Nem védett / indifferens
Geocoris erythrocephalus	Nem védett / indifferens
Geocoris grylloides	Nem védett / indifferens
Geocoris megacephalus	Nem védett / indifferens
Geodromicus plagiatus	Nem védett / indifferens
Geolycosa vultuosa	Védett
Geometra papilionaria	Nem védett / indifferens
Geophilus carpophagus	Nem védett / indifferens
Geophilus electricus	Nem védett / indifferens
Geophilus flavus	Nem védett / indifferens
Harmonia axyridis	Inváziós
Geophilus insculptus	Nem védett / indifferens
Geophilus linearis	Nem védett / indifferens
Geophilus proximus	Nem védett / indifferens
Georissus crenulatus	Nem védett / indifferens
Georissus laesicollis	Nem védett / indifferens
Geostiba chyzeri	Nem védett / indifferens
Geostiba circellaris	Nem védett / indifferens
Geostiba gyorffyi	Nem védett / indifferens
Geotomus elongatus	Nem védett / indifferens
Geotomus punctulatus	Nem védett / indifferens
Geotrupes mutator	Nem védett / indifferens
Geotrupes spiniger	Nem védett / indifferens
Geotrupes stercorarius	Nem védett / indifferens
Gerris argentatus	Nem védett / indifferens
Gerris asper	Nem védett / indifferens
Gerris gibbifer	Nem védett / indifferens
Gerris lacustris	Nem védett / indifferens
Gerris odontogaster	Nem védett / indifferens
Gerris thoracicus	Nem védett / indifferens
Gesneria centuriella	Nem védett / indifferens
Gibbaranea bituberculata strandiana	Nem védett / indifferens
Gibbaranea gibbosa	Nem védett / indifferens
Gibbaranea omoeda	Nem védett / indifferens
Gibbaranea ullrichi	Nem védett / indifferens
Gibberifera simplana	Nem védett / indifferens
Gibbium psylloides	Nem védett / indifferens
Glaphyra kiesenwetteri	Nem védett / indifferens
Glaphyra schmidti	Nem védett / indifferens
Glaphyra umbellatarum	Nem védett / indifferens
Glareola nordmanni	Fokozottan védett
Glaucopsyche alexis	Védett
Glis glis	Védett
Glischrochilus hortensis	Nem védett / indifferens
Glischrochilus quadriguttatus	Nem védett / indifferens
Glischrochilus quadripunctatus	Nem védett / indifferens
Glischrochilus quadrisignatus	Nem védett / indifferens
Globiceps flavomaculatus	Nem védett / indifferens
Globiceps fulvicollis	Nem védett / indifferens
Globiceps horvathi	Nem védett / indifferens
Globiceps sordidus	Nem védett / indifferens
Globiceps sphaegiformis	Nem védett / indifferens
Globicornis corticalis	Nem védett / indifferens
Globicornis emarginata	Nem védett / indifferens
Globicornis nigripes	Nem védett / indifferens
Glocianus distinctus	Nem védett / indifferens
Glocianus fennicus	Nem védett / indifferens
Glocianus incisus	Nem védett / indifferens
Glocianus inhumeralis	Nem védett / indifferens
Glocianus lethierryi	Nem védett / indifferens
Glocianus moelleri	Nem védett / indifferens
Glocianus pilosellus	Nem védett / indifferens
Glocianus punctiger	Nem védett / indifferens
Glocianus sparsutus	Nem védett / indifferens
Glomeridella minima	Nem védett / indifferens
Glomeris connexa	Nem védett / indifferens
Glomeris conspersa	Nem védett / indifferens
Glomeris hexasticha	Nem védett / indifferens
Glomeris ornata	Nem védett / indifferens
Glomeris pustulata	Nem védett / indifferens
Glossiphonia complanata	Nem védett / indifferens
Glossiphonia concolor	Nem védett / indifferens
Glossiphonia nebulosa	Nem védett / indifferens
Glossiphonia paludosa	Nem védett / indifferens
Glossiphonia verrucata	Nem védett / indifferens
Glossocratus foveolatus	Nem védett / indifferens
Glossocratus kuthyi	Nem védett / indifferens
Glossosoma boltoni	Nem védett / indifferens
Glossosoma conforme	Nem védett / indifferens
Gluphisia crenata	Nem védett / indifferens
Glyphesis servulus	Nem védett / indifferens
Glyphesis taoplesius	Nem védett / indifferens
Glyphipterix bergstraesserella	Nem védett / indifferens
Glyphipterix equitella	Nem védett / indifferens
Glyphipterix forsterella	Nem védett / indifferens
Glyphipterix haworthana	Nem védett / indifferens
Glyphipterix loricatella	Fokozottan védett
Glyphipterix nattani	Nem védett / indifferens
Glyphipterix pygmaeella	Nem védett / indifferens
Glyphipterix simpliciella	Nem védett / indifferens
Glyphipterix thrasonella	Nem védett / indifferens
Glyphotaelius pellucidus	Nem védett / indifferens
Chorthippus apricarius	Nem védett / indifferens
Chorthippus biguttulus	Nem védett / indifferens
Chorthippus brunneus	Nem védett / indifferens
Chorthippus mollis	Nem védett / indifferens
Chorthippus vagans	Nem védett / indifferens
Glyptoteles leucacrinella	Nem védett / indifferens
Gnaphosa alpica	Nem védett / indifferens
Gnaphosa bicolor	Nem védett / indifferens
Gnaphosa lucifuga	Nem védett / indifferens
Gnaphosa lugubris	Nem védett / indifferens
Gnaphosa microps	Nem védett / indifferens
Gnaphosa modestior	Nem védett / indifferens
Gnaphosa mongolica	Nem védett / indifferens
Gnaphosa opaca	Nem védett / indifferens
Gnaptor spinimanus	Nem védett / indifferens
Gnathocerus cornutus	Nem védett / indifferens
Gnathonarium dentatum	Nem védett / indifferens
Gnathoncus buyssoni	Nem védett / indifferens
Gnathoncus nannetensis	Nem védett / indifferens
Gnathoncus communis	Nem védett / indifferens
Gnathoncus schmidti	Nem védett / indifferens
Gnathoncus disjunctus suturifer	Nem védett / indifferens
Gnophos furvata	Nem védett / indifferens
Gnorimoschema antiquum	Nem védett / indifferens
Gnorimoschema herbichii	Nem védett / indifferens
Gnorimus nobilis	Nem védett / indifferens
Gnorimus variabilis	Védett
Gnypeta carbonaria	Nem védett / indifferens
Gnypeta ripicola	Nem védett / indifferens
Gnypeta rubrior	Nem védett / indifferens
Romanogobio vladykovi	Védett
Gobio gobio	Védett
Romanogobio kesslerii	Fokozottan védett
Romanogobio uranoscopus	Fokozottan védett
Goera pilosa	Nem védett / indifferens
Gomphocerippus rufus	Nem védett / indifferens
Gomphus flavipes	Védett
Gomphus vulgatissimus	Védett
Gonatium hilare	Nem védett / indifferens
Gonatium paradoxum	Nem védett / indifferens
Gonatium rubellum	Nem védett / indifferens
Gonatium rubens	Nem védett / indifferens
Gonepteryx rhamni	Védett
Gongylidiellum latebricola	Nem védett / indifferens
Gongylidiellum murcidum	Nem védett / indifferens
Gongylidium rufipes	Nem védett / indifferens
Goniagnathus brevis	Nem védett / indifferens
Goniagnathus guttulinervis	Nem védett / indifferens
Gonianotus marginepunctatus	Nem védett / indifferens
Goniodoma auroguttella	Nem védett / indifferens
Gonocephalum granulatum pusillum	Nem védett / indifferens
Gonocephalum pygmaeum	Nem védett / indifferens
Gonocerus acuteangulatus	Nem védett / indifferens
Gonocerus juniperi	Nem védett / indifferens
Gonodera luperus	Nem védett / indifferens
Gortyna borelii	Fokozottan védett
Gortyna borelii lunata	Fokozottan védett
Gortyna flavago	Nem védett / indifferens
Gorytes albidulus	Nem védett / indifferens
Gorytes fallax	Nem védett / indifferens
Gorytes laticinctus	Nem védett / indifferens
Gorytes nigrifacies	Nem védett / indifferens
Gorytes planifrons	Nem védett / indifferens
Gorytes pleuripunctatus	Nem védett / indifferens
Gorytes procrustes	Nem védett / indifferens
Gorytes quadrifasciatus	Nem védett / indifferens
Gorytes quinquecinctus	Nem védett / indifferens
Gorytes quinquefasciatus	Nem védett / indifferens
Gorytes sulcifrons	Nem védett / indifferens
Gossyparia spuria	Nem védett / indifferens
Gracilia minuta	Nem védett / indifferens
Graciliaria inserta	Nem védett / indifferens
Gracillaria loriolella	Nem védett / indifferens
Gracillaria syringella	Nem védett / indifferens
Grammoptera abdominalis	Nem védett / indifferens
Grammoptera ruficornis	Nem védett / indifferens
Grammoptera ustulata	Nem védett / indifferens
Grammotaulius nigropunctatus	Nem védett / indifferens
Grammotaulius nitidus	Nem védett / indifferens
Granaria frumentum	Nem védett / indifferens
Graphiphora augur	Nem védett / indifferens
Graphocraerus ventralis	Nem védett / indifferens
Graphoderus austriacus	Nem védett / indifferens
Graphoderus bilineatus	Fokozottan védett
Graphoderus cinereus	Nem védett / indifferens
Graphoderus zonatus	Nem védett / indifferens
Grapholita caecana	Nem védett / indifferens
Grapholita compositella	Nem védett / indifferens
Grapholita coronillana	Nem védett / indifferens
Grapholita delineana	Nem védett / indifferens
Grapholita difficilana	Nem védett / indifferens
Grapholita discretana	Nem védett / indifferens
Grapholita fissana	Nem védett / indifferens
Grapholita gemmiferana	Nem védett / indifferens
Grapholita jungiella	Nem védett / indifferens
Grapholita larseni	Nem védett / indifferens
Grapholita lathyrana	Nem védett / indifferens
Grapholita lunulana	Nem védett / indifferens
Grapholita nebritana	Nem védett / indifferens
Grapholita orobana	Nem védett / indifferens
Grapholita pallifrontana	Nem védett / indifferens
Graphosoma lineatum	Nem védett / indifferens
Graptodytes bilineatus	Nem védett / indifferens
Graptodytes granularis	Nem védett / indifferens
Graptodytes pictus	Nem védett / indifferens
Graptoleberis testudinaria	Nem védett / indifferens
Graptopeltus lynceus	Nem védett / indifferens
Graptopeltus validus	Nem védett / indifferens
Graptus kaufmanni	Nem védett / indifferens
Graptus triguttatus	Nem védett / indifferens
Gravesteiniella boldi	Nem védett / indifferens
Gravitarmata margarotana	Nem védett / indifferens
Greenisca brachypodii	Nem védett / indifferens
Greenisca gouxi	Nem védett / indifferens
Greenisca placida	Nem védett / indifferens
Gronops lunatus	Nem védett / indifferens
Grus grus	Védett
Gryllotalpa gryllotalpa	Nem védett / indifferens
Gryllus campestris	Nem védett / indifferens
Grynocharis oblonga	Nem védett / indifferens
Grypocoris sexguttatus	Nem védett / indifferens
Tetartostylus illyricus	Nem védett / indifferens
Grypotes puncticollis	Nem védett / indifferens
Grypus brunnirostris	Nem védett / indifferens
Grypus equiseti	Nem védett / indifferens
Gymnancyla canella	Nem védett / indifferens
Gymnancyla hornigi	Nem védett / indifferens
Gymnetron aper	Nem védett / indifferens
Gymnetron beccabungae	Nem védett / indifferens
Gymnetron furcatum	Nem védett / indifferens
Gymnetron melanarium	Nem védett / indifferens
Gymnetron rostellum	Nem védett / indifferens
Gymnetron stimulosum	Nem védett / indifferens
Gymnetron veronicae	Nem védett / indifferens
Gymnetron villosulum	Nem védett / indifferens
Gymnocephalus baloni	Védett
Gymnocephalus cernuus	Nem védett / indifferens
Gymnocephalus schraetser	Védett
Gymnomerus laevipes	Nem védett / indifferens
Gymnopleurus geoffroyi	Nem védett / indifferens
Gymnopleurus mopsus	Nem védett / indifferens
Gymnoscelis rufifasciata	Nem védett / indifferens
Gymnusa brevicollis	Nem védett / indifferens
Gynandromorphus etruscus	Nem védett / indifferens
Gynnidomorpha luridana	Nem védett / indifferens
Gynnidomorpha minimana	Nem védett / indifferens
Gynnidomorpha permixtana	Nem védett / indifferens
Gynnidomorpha vectisana	Nem védett / indifferens
Gyps fulvus	Fokozottan védett
Gypsonoma aceriana	Nem védett / indifferens
Gypsonoma dealbana	Nem védett / indifferens
Gypsonoma minutana	Nem védett / indifferens
Gypsonoma nitidulana	Nem védett / indifferens
Gypsonoma oppressana	Nem védett / indifferens
Gypsonoma sociana	Nem védett / indifferens
Gyraulus albus	Nem védett / indifferens
Gyraulus crista	Nem védett / indifferens
Gyraulus laevis	Nem védett / indifferens
Gyraulus parvus	Nem védett / indifferens
Gyraulus riparius	Nem védett / indifferens
Gyraulus rossmaessleri	Nem védett / indifferens
Gyrinus colymbus	Nem védett / indifferens
Gyrinus distinctus	Nem védett / indifferens
Gyrinus marinus	Nem védett / indifferens
Gyrinus minutus	Nem védett / indifferens
Gyrinus paykulli	Nem védett / indifferens
Gyrinus substriatus	Nem védett / indifferens
Gyrinus suffriani	Nem védett / indifferens
Gyrohypnus angustatus	Nem védett / indifferens
Gyrohypnus atratus	Nem védett / indifferens
Gyrohypnus fracticornis	Nem védett / indifferens
Gyrohypnus punctulatus	Nem védett / indifferens
Gyrophaena affinis	Nem védett / indifferens
Gyrophaena bihamata	Nem védett / indifferens
Gyrophaena boleti	Nem védett / indifferens
Gyrophaena fasciata	Nem védett / indifferens
Gyrophaena gentilis	Nem védett / indifferens
Gyrophaena joyi	Nem védett / indifferens
Gyrophaena joyioides	Nem védett / indifferens
Gyrophaena lucidula	Nem védett / indifferens
Gyrophaena manca	Nem védett / indifferens
Gyrophaena minima	Nem védett / indifferens
Gyrophaena nana	Nem védett / indifferens
Gyrophaena nitidula	Nem védett / indifferens
Gyrophaena polita	Nem védett / indifferens
Gyrophaena poweri	Nem védett / indifferens
Gyrophaena pulchella	Nem védett / indifferens
Gyrophaena rugipennis	Nem védett / indifferens
Gyrophaena strictula	Nem védett / indifferens
Gyrophaena transversalis	Nem védett / indifferens
Gyrophaena williamsi	Nem védett / indifferens
Haasea flavescens	Nem védett / indifferens
Haasea hungarica	Nem védett / indifferens
Habrocerus capillaricornis	Nem védett / indifferens
Habroleptoides confusa	Nem védett / indifferens
Habroloma geranii	Nem védett / indifferens
Habrophlebia fusca	Nem védett / indifferens
Habrophlebia lauta	Nem védett / indifferens
Habropoda zonatula	Nem védett / indifferens
Habrosyne pyritoides	Nem védett / indifferens
Hada plebeja	Nem védett / indifferens
Hadena albimacula	Nem védett / indifferens
Hadena bicruris	Nem védett / indifferens
Hadena capsincola	Nem védett / indifferens
Hadena christophi	Nem védett / indifferens
Hadena compta	Nem védett / indifferens
Hadena confusa	Nem védett / indifferens
Hadena filograna	Nem védett / indifferens
Hadena irregularis	Nem védett / indifferens
Hadena magnolii	Nem védett / indifferens
Hadena perplexa	Nem védett / indifferens
Hadena silenes	Nem védett / indifferens
Hadreule elongatulum	Nem védett / indifferens
Hadrobregmus denticollis	Nem védett / indifferens
Hadrobregmus pertinax	Nem védett / indifferens
Hadroplontus litura	Nem védett / indifferens
Hadroplontus trimaculatus	Nem védett / indifferens
Hadula dianthi	Nem védett / indifferens
Hadula dianthi hungarica	Nem védett / indifferens
Hadula trifolii	Nem védett / indifferens
Haematopota bigoti	Nem védett / indifferens
Haematopota crassicornis	Nem védett / indifferens
Haematopota grandis	Nem védett / indifferens
Haematopota italica	Nem védett / indifferens
Haematopota pandazisi	Nem védett / indifferens
Haematopota pluvialis	Nem védett / indifferens
Haematopota scutellata	Nem védett / indifferens
Haematopota subcylindrica	Nem védett / indifferens
Haematopus ostralegus	Védett
Haematopus ostralegus longipes	Védett
Haemopis sanguisuga	Nem védett / indifferens
Hagenella clathrata	Nem védett / indifferens
Hahnia helveola	Nem védett / indifferens
Hahnia microphthalma	Nem védett / indifferens
Hahnia montana	Nem védett / indifferens
Hahnia nava	Nem védett / indifferens
Hahnia ononidum	Nem védett / indifferens
Hahnia picta	Nem védett / indifferens
Hahnia pusilla	Nem védett / indifferens
Halesus digitatus	Nem védett / indifferens
Halesus radiatus	Nem védett / indifferens
Halesus tesselatus	Nem védett / indifferens
Haliaeetus albicilla	Fokozottan védett
Halictus asperulus	Nem védett / indifferens
Halictus brunnescens	Nem védett / indifferens
Halictus cochlearitarsis	Nem védett / indifferens
Halictus confusus	Nem védett / indifferens
Halictus confusus perkinsi	Nem védett / indifferens
Halictus eurygnathus	Nem védett / indifferens
Halictus gavarnicus	Nem védett / indifferens
Halictus gavarnicus tataricus	Nem védett / indifferens
Halictus kessleri	Nem védett / indifferens
Halictus langobardicus	Nem védett / indifferens
Halictus leucaheneus	Nem védett / indifferens
Halictus leucaheneus arenosus	Nem védett / indifferens
Halictus maculatus	Nem védett / indifferens
Halictus patellatus	Nem védett / indifferens
Halictus patellatus taorminicus	Nem védett / indifferens
Halictus pollinosus	Nem védett / indifferens
Halictus pollinosus cariniventris	Nem védett / indifferens
Halictus quadricinctus	Nem védett / indifferens
Halictus rubicundus	Nem védett / indifferens
Halictus sajoi	Nem védett / indifferens
Halictus scabiosae	Nem védett / indifferens
Halictus seladonius	Nem védett / indifferens
Halictus semitectus	Nem védett / indifferens
Halictus sexcinctus	Nem védett / indifferens
Halictus simplex	Nem védett / indifferens
Halictus smaragdulus	Nem védett / indifferens
Halictus subauratus	Nem védett / indifferens
Halictus tectus	Nem védett / indifferens
Halictus tetrazonius	Nem védett / indifferens
Halictus tumulorum	Nem védett / indifferens
Haliplus apicalis	Nem védett / indifferens
Haliplus flavicollis	Nem védett / indifferens
Haliplus fluviatilis	Nem védett / indifferens
Haliplus fulvicollis	Nem védett / indifferens
Haliplus fulvus	Nem védett / indifferens
Haliplus furcatus	Nem védett / indifferens
Haliplus heydeni	Nem védett / indifferens
Haliplus immaculatus	Nem védett / indifferens
Haliplus laminatus	Nem védett / indifferens
Haliplus lineatocollis	Nem védett / indifferens
Haliplus maculatus	Nem védett / indifferens
Haliplus obliquus	Nem védett / indifferens
Haliplus ruficollis	Nem védett / indifferens
Haliplus variegatus	Nem védett / indifferens
Hallodapus montandoni	Nem védett / indifferens
Hallodapus rufescens	Nem védett / indifferens
Hallodapus suturalis	Nem védett / indifferens
Hallomenus axillaris	Nem védett / indifferens
Hallomenus binotatus	Nem védett / indifferens
Halticus apterus	Nem védett / indifferens
Halticus luteicollis	Nem védett / indifferens
Halticus pusillus	Nem védett / indifferens
Halticus saltator	Nem védett / indifferens
Halyzia sedecimguttata	Nem védett / indifferens
Hamearis lucina	Nem védett / indifferens
Hammerschmidtia ferruginea	Nem védett / indifferens
Handianus cerasi	Nem védett / indifferens
Handianus flavovarius	Nem védett / indifferens
Handianus ignoscus	Nem védett / indifferens
Handianus modestus	Nem védett / indifferens
Handianus procerus	Nem védett / indifferens
Hapalaraea pygmaea	Nem védett / indifferens
Haplodrassus cognatus	Nem védett / indifferens
Haplodrassus dalmatensis	Nem védett / indifferens
Haplodrassus kulczynskii	Nem védett / indifferens
Haplodrassus minor	Nem védett / indifferens
Haplodrassus moderatus	Nem védett / indifferens
Haplodrassus signifer	Nem védett / indifferens
Haplodrassus silvestris	Nem védett / indifferens
Haplodrassus umbratilis	Nem védett / indifferens
Haploglomeris multistriata	Nem védett / indifferens
Haploglossa gentilis	Nem védett / indifferens
Haploglossa marginalis	Nem védett / indifferens
Haploglossa nidicola	Nem védett / indifferens
Haploglossa picipennis	Nem védett / indifferens
Haploglossa villosula	Nem védett / indifferens
Haploporatia eremita	Nem védett / indifferens
Haplorhynchites pubescens	Nem védett / indifferens
Haplotinea ditella	Nem védett / indifferens
Haplotinea insectella	Nem védett / indifferens
Hardya tenuis	Nem védett / indifferens
Harmonia quadripunctata	Nem védett / indifferens
Harpactea hombergi	Nem védett / indifferens
Harpactea lepida	Nem védett / indifferens
Harpactea rubicunda	Nem védett / indifferens
Harpactea saeva	Nem védett / indifferens
Harpactus affinis	Nem védett / indifferens
Harpactus consanguineus	Nem védett / indifferens
Harpactus elegans	Nem védett / indifferens
Harpactus exiguus	Nem védett / indifferens
Harpactus laevis	Nem védett / indifferens
Harpactus lunatus	Nem védett / indifferens
Harpactus moravicus	Nem védett / indifferens
Harpactus tauricus	Nem védett / indifferens
Harpactus tumidus	Nem védett / indifferens
Harpadispar diffusalis	Nem védett / indifferens
Harpagoxenus sublaevis	Nem védett / indifferens
Harpalus affinis	Nem védett / indifferens
Harpalus albanicus	Nem védett / indifferens
Harpalus angulatus	Nem védett / indifferens
Harpalus angulatus scytha	Nem védett / indifferens
Harpalus anxius	Nem védett / indifferens
Harpalus atratus	Nem védett / indifferens
Harpalus attenuatus	Nem védett / indifferens
Harpalus autumnalis	Nem védett / indifferens
Harpalus caspius	Nem védett / indifferens
Harpalus caspius roubali	Nem védett / indifferens
Harpalus cupreus	Nem védett / indifferens
Harpalus cupreus fastuosus	Nem védett / indifferens
Harpalus dimidiatus	Nem védett / indifferens
Harpalus distinguendus	Nem védett / indifferens
Harpalus flavescens	Nem védett / indifferens
Harpalus flavicornis	Nem védett / indifferens
Harpalus froelichii	Nem védett / indifferens
Harpalus fuscicornis	Nem védett / indifferens
Harpalus fuscipalpis	Nem védett / indifferens
Harpalus hirtipes	Nem védett / indifferens
Harpalus honestus	Nem védett / indifferens
Harpalus hospes	Nem védett / indifferens
Harpalus inexpectatus	Nem védett / indifferens
Harpalus latus	Nem védett / indifferens
Harpalus luteicornis	Nem védett / indifferens
Harpalus marginellus	Nem védett / indifferens
Harpalus modestus	Nem védett / indifferens
Harpalus neglectus	Nem védett / indifferens
Harpalus oblitus	Nem védett / indifferens
Harpalus picipennis	Nem védett / indifferens
Harpalus progrediens	Nem védett / indifferens
Harpalus pumilus	Nem védett / indifferens
Harpalus pygmaeus	Nem védett / indifferens
Harpalus quadripunctatus	Nem védett / indifferens
Harpalus rubripes	Nem védett / indifferens
Harpalus rufipalpis	Nem védett / indifferens
Harpalus saxicola	Nem védett / indifferens
Harpalus serripes	Nem védett / indifferens
Harpalus servus	Nem védett / indifferens
Semiophonus signaticornis	Nem védett / indifferens
Harpalus smaragdinus	Nem védett / indifferens
Harpalus subcylindricus	Nem védett / indifferens
Harpalus tardus	Nem védett / indifferens
Harpalus xanthopus	Nem védett / indifferens
Harpalus xanthopus winkleri	Nem védett / indifferens
Harpalus zabroides	Nem védett / indifferens
Harpella forficella	Nem védett / indifferens
Harpocera thoracica	Nem védett / indifferens
Harpolithobius anodus	Nem védett / indifferens
Harpyia milhauseri	Nem védett / indifferens
Hasarius adansoni	Nem védett / indifferens
Hebetancylus excentricus	Nem védett / indifferens
Hebrus pusillus	Nem védett / indifferens
Hebrus ruficeps	Nem védett / indifferens
Hecalus glaucescens	Nem védett / indifferens
Hecatera bicolorata	Nem védett / indifferens
Hecatera cappa	Nem védett / indifferens
Hecatera dysodea	Nem védett / indifferens
Hedobia pubescens	Nem védett / indifferens
Hedya dimidiana	Nem védett / indifferens
Hedya nubiferana	Nem védett / indifferens
Hedya ochroleucana	Nem védett / indifferens
Hedya pruniana	Nem védett / indifferens
Hedya salicella	Nem védett / indifferens
Hedychridium adventicium	Nem védett / indifferens
Hedychridium aereolum	Nem védett / indifferens
Hedychridium aheneum	Nem védett / indifferens
Hedychridium ardens	Nem védett / indifferens
Hedychridium coriaceum	Nem védett / indifferens
Hedychridium elegantulum	Nem védett / indifferens
Hedychridium femoratum	Nem védett / indifferens
Hedychridium flavipes	Nem védett / indifferens
Hedychridium hungaricum	Nem védett / indifferens
Hedychridium irregulare	Nem védett / indifferens
Hedychridium jazygicum	Nem védett / indifferens
Hedychridium jucundum	Nem védett / indifferens
Hedychridium krajniki	Nem védett / indifferens
Hedychridium monochroum	Nem védett / indifferens
Hedychridium parkanense	Nem védett / indifferens
Hedychridium plagiatum	Nem védett / indifferens
Hedychridium roseum	Nem védett / indifferens
Hedychridium sculpturatum	Nem védett / indifferens
Hedychridium scutellare	Nem védett / indifferens
Hedychridium valesiense	Nem védett / indifferens
Hedychridium zelleri	Nem védett / indifferens
Hedychrum aureicolle	Nem védett / indifferens
Hedychrum aureicolle niemelai	Nem védett / indifferens
Hedychrum gerstaeckeri	Nem védett / indifferens
Hedychrum longicolle	Nem védett / indifferens
Hedychrum nobile	Nem védett / indifferens
Hedychrum rutilans	Nem védett / indifferens
Heinemannia festivella	Nem védett / indifferens
Heinemannia laspeyrella	Nem védett / indifferens
Helcystogramma albinervis	Nem védett / indifferens
Helcystogramma arulensis	Nem védett / indifferens
Helcystogramma lineolella	Nem védett / indifferens
Helcystogramma lutatella	Nem védett / indifferens
Helcystogramma rufescens	Nem védett / indifferens
Helcystogramma triannulella	Nem védett / indifferens
Helianthemapion aciculare	Nem védett / indifferens
Helianthemapion velatum	Nem védett / indifferens
Helicoconis lutea	Nem védett / indifferens
Helicoconis pseudolutea	Nem védett / indifferens
Helicoconis transsylvanica	Nem védett / indifferens
Helicodonta obvoluta	Nem védett / indifferens
Helicopsis instabilis	Nem védett / indifferens
Helicopsis striata	Nem védett / indifferens
Helicopsyche bacescui	Nem védett / indifferens
Helicoverpa armigera	Nem védett / indifferens
Heliococcus bohemicus	Nem védett / indifferens
Heliococcus cydoniae	Nem védett / indifferens
Heliococcus danzigae	Nem védett / indifferens
Heliococcus nivearum	Nem védett / indifferens
Heliococcus radicicola	Nem védett / indifferens
Heliococcus sulcii	Nem védett / indifferens
Heliodines roesella	Nem védett / indifferens
Heliomata glarearia	Nem védett / indifferens
Heliophanus aeneus	Nem védett / indifferens
Heliophanus auratus	Nem védett / indifferens
Heliophanus cupreus	Nem védett / indifferens
Heliophanus dubius	Nem védett / indifferens
Heliophanus flavipes	Nem védett / indifferens
Heliophanus kochii	Nem védett / indifferens
Heliophanus lineiventris	Nem védett / indifferens
Heliophanus patagiatus	Nem védett / indifferens
Heliophanus simplex	Nem védett / indifferens
Heliophanus tribulosus	Nem védett / indifferens
Heliothela wulfeniana	Nem védett / indifferens
Heliothis maritima	Nem védett / indifferens
Heliothis maritima bulgarica	Nem védett / indifferens
Heliothis nubigera	Nem védett / indifferens
Heliothis ononis	Nem védett / indifferens
Heliothis peltigera	Nem védett / indifferens
Heliothis viriplaca	Nem védett / indifferens
Heliozela resplendella	Nem védett / indifferens
Heliozela sericiella	Nem védett / indifferens
Helix lucorum	Nem védett / indifferens
Helix lutescens	Védett
Helix pomatia	Védett
Hellinsia osteodactylus	Nem védett / indifferens
Hellula undalis	Nem védett / indifferens
Helobdella stagnalis	Nem védett / indifferens
Helochares lividus	Nem védett / indifferens
Helochares obscurus	Nem védett / indifferens
Helophilus hybridus	Nem védett / indifferens
Helophilus pendulus	Nem védett / indifferens
Helophilus trivittatus	Nem védett / indifferens
Helophora insignis	Nem védett / indifferens
Helophorus aequalis	Nem védett / indifferens
Helophorus aquaticus	Nem védett / indifferens
Helophorus arvernicus	Nem védett / indifferens
Helophorus asperatus	Nem védett / indifferens
Helophorus brevipalpis	Nem védett / indifferens
Helophorus croaticus	Nem védett / indifferens
Helophorus discrepans	Nem védett / indifferens
Helophorus dorsalis	Nem védett / indifferens
Helophorus flavipes	Nem védett / indifferens
Helophorus granularis	Nem védett / indifferens
Helophorus griseus	Nem védett / indifferens
Helophorus liguricus	Nem védett / indifferens
Helophorus longitarsis	Nem védett / indifferens
Helophorus micans	Nem védett / indifferens
Helophorus minutus	Nem védett / indifferens
Helophorus montenegrinus	Nem védett / indifferens
Helophorus nanus	Nem védett / indifferens
Helophorus nubilus	Nem védett / indifferens
Helophorus obscurus	Nem védett / indifferens
Helophorus paraminutus	Nem védett / indifferens
Helophorus porculus	Nem védett / indifferens
Helophorus redtenbacheri	Nem védett / indifferens
Helophorus rufipes	Nem védett / indifferens
Helophorus strigifrons	Nem védett / indifferens
Helophorus villosus	Nem védett / indifferens
Hemaris fuciformis	Védett
Hemaris tityus	Védett
Hemerobius atrifrons	Nem védett / indifferens
Hemerobius contumax	Nem védett / indifferens
Hemerobius fenestratus	Nem védett / indifferens
Hemerobius gilvus	Nem védett / indifferens
Hemerobius handschini	Nem védett / indifferens
Hemerobius humulinus	Nem védett / indifferens
Hemerobius lutescens	Nem védett / indifferens
Hemerobius marginatus	Nem védett / indifferens
Hemerobius micans	Nem védett / indifferens
Hemerobius nitidulus	Nem védett / indifferens
Hemerobius perelegans	Nem védett / indifferens
Hemerobius pini	Nem védett / indifferens
Hemerobius simulans	Nem védett / indifferens
Hemerobius stigma	Nem védett / indifferens
Hemianax ephippiger	Nem védett / indifferens
Hemiclepsis marginata	Nem védett / indifferens
Hemicoelus costatus	Nem védett / indifferens
Hemicoelus fulvicornis	Nem védett / indifferens
Hemicoelus nitidus	Nem védett / indifferens
Hemicoelus rufipennis	Nem védett / indifferens
Hemicrepidius hirtus	Nem védett / indifferens
Hemicrepidius niger	Nem védett / indifferens
Hemidiaptomus amblyodon	Nem védett / indifferens
Hemidiaptomus hungaricus	Nem védett / indifferens
Hemipterochilus bembiciformis	Nem védett / indifferens
Hemipterochilus bembiciformis terricola	Nem védett / indifferens
Hemistola chrysoprasaria	Nem védett / indifferens
Hemithea aestivaria	Nem védett / indifferens
Hemitrichapion pavidum	Nem védett / indifferens
Hemitrichapion reflexum	Nem védett / indifferens
Hemitrichapion waltoni	Nem védett / indifferens
Endecatomus reticulatus	Nem védett / indifferens
Henestaris halophilus	Nem védett / indifferens
Henia bicarinata	Nem védett / indifferens
Henia illyrica	Nem védett / indifferens
Henosepilachna argus	Nem védett / indifferens
Henosepilachna elaterii	Nem védett / indifferens
Henoticus serratus	Nem védett / indifferens
Henschia acuta	Nem védett / indifferens
Hephathus freyi	Nem védett / indifferens
Hephathus nanus	Nem védett / indifferens
Hepialus humuli	Nem védett / indifferens
Heptagenia coerulans	Nem védett / indifferens
Heptagenia flava	Nem védett / indifferens
Kageronia fuscogrisea	Nem védett / indifferens
Heptagenia longicauda	Nem védett / indifferens
Heptagenia sulphurea	Nem védett / indifferens
Heptatoma pellucens	Nem védett / indifferens
Heptaulacus testudinarius	Nem védett / indifferens
Ocrasa fulvocilialis	Nem védett / indifferens
Herculia incarnatalis	Nem védett / indifferens
Herculia rubidalis	Nem védett / indifferens
Heriades crenulatus	Nem védett / indifferens
Heriades rubicolus	Nem védett / indifferens
Heriades truncorum	Nem védett / indifferens
Heriaeus graminicola	Nem védett / indifferens
Heriaeus hirtus	Nem védett / indifferens
Heriaeus melloteei	Nem védett / indifferens
Herilla ziegleri	Nem védett / indifferens
Herilla ziegleri dacida	Nem védett / indifferens
Heringia heringi	Nem védett / indifferens
Heringia senilis	Nem védett / indifferens
Herminia grisealis	Nem védett / indifferens
Herminia tarsicrinalis	Nem védett / indifferens
Herminia tenuialis	Nem védett / indifferens
Herophila tristis	Védett
Herotilapia multispinosa	Nem védett / indifferens
Herpes porcellus	Védett
Herpetocypris chevreuxi	Nem védett / indifferens
Herpetocypris reptans	Nem védett / indifferens
Hesium domino	Nem védett / indifferens
Hesperia comma	Nem védett / indifferens
Hesperocorixa linnaei	Nem védett / indifferens
Hesperocorixa sahlbergi	Nem védett / indifferens
Hesperus rufipennis	Nem védett / indifferens
Haeterius ferrugineus	Nem védett / indifferens
Heterhelus scutellaris	Nem védett / indifferens
Heterhelus solani	Nem védett / indifferens
Heterocapillus tigripes	Nem védett / indifferens
Heterocerus crinitus	Nem védett / indifferens
Heterocerus fenestratus	Nem védett / indifferens
Heterocerus flexuosus	Nem védett / indifferens
Heterocerus fossor	Nem védett / indifferens
Heterocerus fusculus	Nem védett / indifferens
Augyles hispidulus	Nem védett / indifferens
Heterocerus marginatus	Nem védett / indifferens
Heterocerus obsoletus	Nem védett / indifferens
Heterocerus parallelus	Nem védett / indifferens
Augyles pruinosus	Nem védett / indifferens
Augyles sericans	Nem védett / indifferens
Heterococcus nudus	Nem védett / indifferens
Heterococcus tritici	Nem védett / indifferens
Heterocordylus erythropthalmus	Nem védett / indifferens
Heterocordylus genistae	Nem védett / indifferens
Heterocordylus leptocerus	Nem védett / indifferens
Heterocordylus tibialis	Nem védett / indifferens
Heterocordylus tumidicornis	Nem védett / indifferens
Heterocypris barbara	Nem védett / indifferens
Heterocypris incongruens	Nem védett / indifferens
Heterocypris rotundata	Nem védett / indifferens
Heterocypris salina	Nem védett / indifferens
Heterogaster affinis	Nem védett / indifferens
Heterogaster artemisiae	Nem védett / indifferens
Heterogaster cathariae	Nem védett / indifferens
Heterogaster urticae	Nem védett / indifferens
Heterogenea asella	Nem védett / indifferens
Heterogynis penella	Nem védett / indifferens
Heteroporatia simile	Nem védett / indifferens
Heteropterus morpheus	Védett
Heterothops dissimilis	Nem védett / indifferens
Heterothops praevius niger	Nem védett / indifferens
Heterothops praevius	Nem védett / indifferens
Heterothops stiglundbergi	Nem védett / indifferens
Heterotoma merioptera	Nem védett / indifferens
Heterotoma planicornis	Nem védett / indifferens
Hexarthrum exiguum	Nem védett / indifferens
Hieraaetus fasciatus	Fokozottan védett
Hieraaetus pennatus	Fokozottan védett
Himacerus apterus	Nem védett / indifferens
Himacerus boops	Nem védett / indifferens
Himacerus mirmicoides	Nem védett / indifferens
Himantopus himantopus	Fokozottan védett
Hipparchia fagi	Nem védett / indifferens
Hipparchia semele	Védett
Hipparchia statilinus	Védett
Hippeutis complanatus	Nem védett / indifferens
Hippodamia septemmaculata	Nem védett / indifferens
Hippodamia tredecimpunctata	Nem védett / indifferens
Hippodamia variegata	Nem védett / indifferens
Hippolais icterina	Védett
Iduna pallida	Védett
Iduna pallida elaeica	Védett
Hippotion celerio	Nem védett / indifferens
Hirticomus hispidus	Nem védett / indifferens
Hirudo medicinalis	Nem védett / indifferens
Hirudo verbana	Nem védett / indifferens
Hirundo daurica	Védett
Hirundo rustica	Védett
Hister bissexstriatus	Nem védett / indifferens
Hister funestus	Nem védett / indifferens
Hister helluo	Nem védett / indifferens
Hister illigeri	Nem védett / indifferens
Hister lugubris	Nem védett / indifferens
Hister quadrimaculatus	Nem védett / indifferens
Hister quadrinotatus	Nem védett / indifferens
Hister sepulchralis	Nem védett / indifferens
Hister unicolor	Nem védett / indifferens
Histopona luxurians	Nem védett / indifferens
Histopona torpida	Nem védett / indifferens
Hofmannophila pseudospretella	Nem védett / indifferens
Hogna radiata	Nem védett / indifferens
Holcocranum saturejae	Nem védett / indifferens
Holcophora statices	Nem védett / indifferens
Holcopogon bubulcellus helveolellus	Nem védett / indifferens
Holcostethus sphacelatus	Nem védett / indifferens
Holcostethus strictus vernalis	Nem védett / indifferens
Holobus apicatus	Nem védett / indifferens
Holobus flavicornis	Nem védett / indifferens
Holocentropus dubius	Nem védett / indifferens
Holocentropus picicornis	Nem védett / indifferens
Holocentropus stagnalis	Nem védett / indifferens
Hololepta plana	Nem védett / indifferens
Holoparamecus caularum	Nem védett / indifferens
Holoparamecus ragusae	Nem védett / indifferens
Holopedium gibberum	Nem védett / indifferens
Holopyga amoenula	Nem védett / indifferens
Holopyga chrysonota	Nem védett / indifferens
Holopyga fervida	Nem védett / indifferens
Holopyga hortobagyensis	Nem védett / indifferens
Holopyga inflammata	Nem védett / indifferens
Holopyga minuma	Nem védett / indifferens
Holoscolia huebneri	Nem védett / indifferens
Holotrichapion ononis	Nem védett / indifferens
Holotrichapion aethiops	Nem védett / indifferens
Holotrichapion gracilicolle	Nem védett / indifferens
Holotrichapion pisi	Nem védett / indifferens
Homalota plana	Nem védett / indifferens
Homaloxestis briantiella	Nem védett / indifferens
Homoeosoma inustella	Nem védett / indifferens
Homoeosoma nebulella	Nem védett / indifferens
Homoeosoma nimbella	Nem védett / indifferens
Homoeosoma sinuella	Nem védett / indifferens
Homoeosoma subalbatella	Nem védett / indifferens
Homoeusa acuminata	Nem védett / indifferens
Homonotus balcanicus	Nem védett / indifferens
Homonotus sangiunolentus	Nem védett / indifferens
Ruspolia nitidula	Nem védett / indifferens
Homorosoma validirostre	Nem védett / indifferens
Hoplia argentea	Nem védett / indifferens
Hoplia dilutipes	Nem védett / indifferens
Hoplia hungarica	Nem védett / indifferens
Hoplia philanthus	Nem védett / indifferens
Hoplia praticola	Nem védett / indifferens
Hoplisoides craverii	Nem védett / indifferens
Hoplisoides latifrons	Nem védett / indifferens
Hoplisoides punctuosus	Nem védett / indifferens
Hoplodrina ambigua	Nem védett / indifferens
Hoplodrina blanda	Nem védett / indifferens
Hoplodrina octogenaria	Nem védett / indifferens
Hoplodrina respersa	Nem védett / indifferens
Hoplodrina superstes	Nem védett / indifferens
Hoplomachus thunbergii	Nem védett / indifferens
Hoplopholcus forskali	Nem védett / indifferens
Hoplopterus spinosus	Védett
Horisme aquata	Nem védett / indifferens
Horisme corticata	Nem védett / indifferens
Horisme radicaria	Nem védett / indifferens
Horisme tersata	Nem védett / indifferens
Horisme vitalbata	Nem védett / indifferens
Horistus orientalis	Nem védett / indifferens
Depressaria dictamnella	Nem védett / indifferens
Horvathianella palliceps	Nem védett / indifferens
Horvathiolus superbus	Nem védett / indifferens
Hoshihananomia perlata	Nem védett / indifferens
Hucho hucho	Fokozottan védett
Hungarocypris madaraszi	Nem védett / indifferens
Hungarosoma bokori	Nem védett / indifferens
Huso huso	Védett
Hyalesthes luteipes	Nem védett / indifferens
Hyalesthes obsoletus	Nem védett / indifferens
Hyalesthes philesakis	Nem védett / indifferens
Hyalesthes scotti	Nem védett / indifferens
Hyalochiton komaroffii	Nem védett / indifferens
Hybomitra acuminata	Nem védett / indifferens
Hybomitra aterrima	Nem védett / indifferens
Hybomitra auripila	Nem védett / indifferens
Hybomitra bimaculata	Nem védett / indifferens
Hybomitra borealis	Nem védett / indifferens
Hybomitra ciureai	Nem védett / indifferens
Hybomitra distinguenda	Nem védett / indifferens
Hybomitra expollicata	Nem védett / indifferens
Hybomitra lundbecki	Nem védett / indifferens
Hybomitra lurida	Nem védett / indifferens
Hybomitra media	Nem védett / indifferens
Hybomitra micans	Nem védett / indifferens
Hybomitra morgani	Nem védett / indifferens
Hybomitra muehlfeldi	Nem védett / indifferens
Hybomitra nitidifrons	Nem védett / indifferens
Hybomitra pilosa	Nem védett / indifferens
Hybomitra solstitialis	Nem védett / indifferens
Hybomitra tropica	Nem védett / indifferens
Hybomitra ukrainica	Nem védett / indifferens
Hycleus polymorphus	Nem védett / indifferens
Hydaticus aruspex	Nem védett / indifferens
Hydaticus continentalis	Nem védett / indifferens
Hydaticus grammicus	Nem védett / indifferens
Hydaticus seminiger	Nem védett / indifferens
Hydaticus transversalis	Nem védett / indifferens
Hydnobius claviger	Nem védett / indifferens
Hydnobius latifrons	Nem védett / indifferens
Hydnobius multistriatus	Nem védett / indifferens
Hydnobius punctatus	Nem védett / indifferens
Hydnobius spinipes	Nem védett / indifferens
Hydraecia micacea	Nem védett / indifferens
Hydraecia petasitis	Védett
Hydraecia ultima	Nem védett / indifferens
Hydraena belgica	Nem védett / indifferens
Hydraena britteni	Nem védett / indifferens
Hydraena excisa	Nem védett / indifferens
Hydraena flavipes	Nem védett / indifferens
Hydraena gracilis	Nem védett / indifferens
Hydraena hungarica	Nem védett / indifferens
Hydraena melas	Nem védett / indifferens
Hydraena morio	Nem védett / indifferens
Hydraena nigrita	Nem védett / indifferens
Hydraena paganettii	Nem védett / indifferens
Hydraena palustris	Nem védett / indifferens
Hydraena pulchella	Nem védett / indifferens
Hydraena pygmaea	Nem védett / indifferens
Hydraena reyi	Nem védett / indifferens
Hydraena riparia	Nem védett / indifferens
Hydraena saga	Nem védett / indifferens
Hydrelia flammeolaria	Nem védett / indifferens
Hydrelia sylvata	Védett
Hydriomena furcata	Nem védett / indifferens
Hydriomena impluviata	Nem védett / indifferens
Hydrobius fuscipes	Nem védett / indifferens
Hydrochara caraboides	Nem védett / indifferens
Hydrochara dichroma	Nem védett / indifferens
Hydrochara flavipes	Nem védett / indifferens
Hydrochus angustatus	Nem védett / indifferens
Hydrochus brevis	Nem védett / indifferens
Hydrochus crenatus	Nem védett / indifferens
Hydrochus elongatus	Nem védett / indifferens
Hydrochus flavipennis	Nem védett / indifferens
Hydrochus ignicollis	Nem védett / indifferens
Hydrochus megaphallus	Nem védett / indifferens
Hydrocyphon deflexicollis	Nem védett / indifferens
Hydroglyphus geminus	Nem védett / indifferens
Hydrometra gracilenta	Nem védett / indifferens
Hydrometra stagnorum	Nem védett / indifferens
Hydrophilus aterrimus	Nem védett / indifferens
Hydrophilus piceus	Nem védett / indifferens
Hydroporus angustatus	Nem védett / indifferens
Hydroporus discretus	Nem védett / indifferens
Hydroporus discretus ponticus	Nem védett / indifferens
Hydroporus dobrogeanus	Nem védett / indifferens
Hydroporus erythrocephalus	Nem védett / indifferens
Hydroporus ferrugineus	Nem védett / indifferens
Hydroporus foveolatus	Nem védett / indifferens
Hydroporus fuscipennis	Nem védett / indifferens
Hydroporus hebaueri	Nem védett / indifferens
Hydroporus incognitus	Nem védett / indifferens
Hydroporus longicornis	Nem védett / indifferens
Hydroporus marginatus	Nem védett / indifferens
Hydroporus melanarius	Nem védett / indifferens
Hydroporus memnonius	Nem védett / indifferens
Hydroporus neglectus	Nem védett / indifferens
Hydroporus nigrita	Nem védett / indifferens
Hydroporus notatus	Nem védett / indifferens
Hydroporus palustris	Nem védett / indifferens
Hydroporus planus	Nem védett / indifferens
Hydroporus rufifrons	Nem védett / indifferens
Hydroporus scalesianus	Nem védett / indifferens
Hydroporus striola	Nem védett / indifferens
Hydroporus tristis	Nem védett / indifferens
Hydroporus umbrosus	Nem védett / indifferens
Hydropsyche angustipennis	Nem védett / indifferens
Hydropsyche bulbifera	Nem védett / indifferens
Hydropsyche bulgaromanorum	Nem védett / indifferens
Hydropsyche contubernalis	Nem védett / indifferens
Hydropsyche exocellata	Nem védett / indifferens
Hydropsyche fulvipes	Nem védett / indifferens
Hydropsyche guttata	Nem védett / indifferens
Hydropsyche instabilis	Nem védett / indifferens
Hydropsyche modesta	Nem védett / indifferens
Hydropsyche ornatula	Nem védett / indifferens
Hydropsyche pellucidula	Nem védett / indifferens
Hydropsyche saxonica	Nem védett / indifferens
Hydropsyche siltalai	Nem védett / indifferens
Hydroptila angustata	Nem védett / indifferens
Hydroptila cornuta	Nem védett / indifferens
Hydroptila dampfi	Nem védett / indifferens
Hydroptila forcipata	Nem védett / indifferens
Hydroptila lotensis	Nem védett / indifferens
Hydroptila occulta	Nem védett / indifferens
Hydroptila pulchricornis	Nem védett / indifferens
Hydroptila simulans	Nem védett / indifferens
Hydroptila sparsa	Nem védett / indifferens
Hydroptila tineoides	Nem védett / indifferens
Hydroptila vectis	Nem védett / indifferens
Hydrosmecta delicatula	Nem védett / indifferens
Hydrosmecta longula	Nem védett / indifferens
Hydrovatus cuspidatus	Nem védett / indifferens
Hygrobia hermanni	Nem védett / indifferens
Hygrolycosa rubrofasciata	Nem védett / indifferens
Hygromia cinctella	Nem védett / indifferens
Hygronoma dimidiata	Nem védett / indifferens
Hygrotus confluens	Nem védett / indifferens
Hygrotus decoratus	Nem védett / indifferens
Hygrotus enneagrammus	Nem védett / indifferens
Hygrotus impressopunctatus	Nem védett / indifferens
Hygrotus inaequalis	Nem védett / indifferens
Hygrotus pallidulus	Nem védett / indifferens
Hygrotus parallellogrammus	Nem védett / indifferens
Hygrotus versicolor	Nem védett / indifferens
Hyla arborea	Védett
Hylaea fasciaria	Nem védett / indifferens
Hylaeus angustatus	Nem védett / indifferens
Hylaeus annularis	Nem védett / indifferens
Hylaeus annulatus	Nem védett / indifferens
Hylaeus brevicornis	Nem védett / indifferens
Hylaeus clypearis	Nem védett / indifferens
Hylaeus communis	Nem védett / indifferens
Hylaeus cornutus	Nem védett / indifferens
Hylaeus difformis	Nem védett / indifferens
Hylaeus duckei	Nem védett / indifferens
Hylaeus euryscapus	Nem védett / indifferens
Hylaeus gibbus	Nem védett / indifferens
Hylaeus gibbus confusus	Nem védett / indifferens
Hylaeus gracilicornis	Nem védett / indifferens
Hylaeus hyalinatus	Nem védett / indifferens
Hylaeus leptocephalus	Nem védett / indifferens
Hylaeus lineolatus	Nem védett / indifferens
Hylaeus moricei	Nem védett / indifferens
Hylaeus nigritus	Nem védett / indifferens
Hylaeus pectoralis	Nem védett / indifferens
Hylaeus pfankuchi	Nem védett / indifferens
Hylaeus pictipes	Nem védett / indifferens
Hylaeus punctatus	Nem védett / indifferens
Hylaeus punctulatissimus	Nem védett / indifferens
Hylaeus punctus	Nem védett / indifferens
Hylaeus rinki	Nem védett / indifferens
Hylaeus signatus	Nem védett / indifferens
Hylaeus sinuatus	Nem védett / indifferens
Hylaeus styriacus	Nem védett / indifferens
Hylaeus trinotatus	Nem védett / indifferens
Hylaeus variegatus	Nem védett / indifferens
Hylastes angustatus	Nem védett / indifferens
Hylastes ater	Nem védett / indifferens
Hylastes attenuatus	Nem védett / indifferens
Hylastes brunneus	Nem védett / indifferens
Hylastes cunicularius	Nem védett / indifferens
Hylastes linearis	Nem védett / indifferens
Hylastes opacus	Nem védett / indifferens
Hylastinus obscurus	Nem védett / indifferens
Hylebainosoma tatranum	Nem védett / indifferens
Hylebainosoma tatranum josvaense	Nem védett / indifferens
Hylecoetus dermestoides	Nem védett / indifferens
Hyledelphax elegantulus	Nem védett / indifferens
Hyles euphorbiae	Nem védett / indifferens
Hyles gallii	Védett
Hyles livornica	Nem védett / indifferens
Hyles vespertilio	Nem védett / indifferens
Hylesinus crenatus	Nem védett / indifferens
Hylesinus toranio	Nem védett / indifferens
Hylis cariniceps	Nem védett / indifferens
Hylis foveicollis	Nem védett / indifferens
Hylis simonae	Nem védett / indifferens
Hylobius abietis	Nem védett / indifferens
Hylobius excavatus	Nem védett / indifferens
Hylobius pinastri	Nem védett / indifferens
Hylobius transversovittatus	Nem védett / indifferens
Sphinx pinastri	Nem védett / indifferens
Hylotrupes bajulus	Nem védett / indifferens
Hylurgops glabratus	Nem védett / indifferens
Hylurgops palliatus	Nem védett / indifferens
Hylurgus ligniperda	Nem védett / indifferens
Hylyphantes graminicola	Nem védett / indifferens
Hylyphantes nigritus	Nem védett / indifferens
Hymenalia morio	Védett
Hymenalia rufipes	Nem védett / indifferens
Hyoidea notaticeps	Nem védett / indifferens
Hypatima rhomboidella	Nem védett / indifferens
Hypatopa binotella	Nem védett / indifferens
Hypatopa inunctella	Nem védett / indifferens
Hypebaeus flavipes	Nem védett / indifferens
Hypena crassalis	Nem védett / indifferens
Hypena obesalis	Nem védett / indifferens
Hypena proboscidalis	Nem védett / indifferens
Hypena rostralis	Nem védett / indifferens
Hypenodes humidalis	Nem védett / indifferens
Hypenodes orientalis	Védett
Hypera arator	Nem védett / indifferens
Hypera arundinis	Nem védett / indifferens
Hypera contaminata	Nem védett / indifferens
Hypera cumana	Nem védett / indifferens
Hypera denominanda	Nem védett / indifferens
Hypera diversipunctata	Nem védett / indifferens
Hypera fornicata	Nem védett / indifferens
Hypera fuscocinerea	Nem védett / indifferens
Hypera meles	Nem védett / indifferens
Hypera nigrirostris	Nem védett / indifferens
Hypera ononidis	Nem védett / indifferens
Hypera pastinacae	Nem védett / indifferens
Hypera plantaginis	Nem védett / indifferens
Hypera pollux	Nem védett / indifferens
Hypera postica	Nem védett / indifferens
Hypera rumicis	Nem védett / indifferens
Hypera striata	Nem védett / indifferens
Hypera suspiciosa	Nem védett / indifferens
Hypera venusta	Nem védett / indifferens
Hypera viciae	Nem védett / indifferens
Hyperaspis campestris	Nem védett / indifferens
Hyperaspis concolor	Nem védett / indifferens
Hyperaspis erytrocephala	Nem védett / indifferens
Hyperaspis inexpectata	Nem védett / indifferens
Hyperaspis pseudopustulata	Nem védett / indifferens
Hyperaspis reppensis quadrimaculata	Nem védett / indifferens
Hyperaspis reppensis	Nem védett / indifferens
Hypercallia citrinalis	Nem védett / indifferens
Hyperlais dulcinalis	Nem védett / indifferens
Hyphantria cunea	Nem védett / indifferens
Hyphoraia aulica	Nem védett / indifferens
Hyphydrus anatolicus	Nem védett / indifferens
Hyphydrus ovatus	Nem védett / indifferens
Hypnogyra angularis	Nem védett / indifferens
Hypoborus ficus	Nem védett / indifferens
Hypocacculus metallescens	Nem védett / indifferens
Hypocacculus rubripes	Nem védett / indifferens
Hypocacculus rufipes	Nem védett / indifferens
Hypocacculus spretulus	Nem védett / indifferens
Hypocaccus metallicus	Nem védett / indifferens
Hypocaccus rugiceps	Nem védett / indifferens
Hypocaccus rugifrons	Nem védett / indifferens
Hypochalcia ahenella	Nem védett / indifferens
Hypochalcia bruandella	Nem védett / indifferens
Hypochalcia decorella	Nem védett / indifferens
Hypochalcia dignella	Nem védett / indifferens
Hypochalcia griseoaenella	Nem védett / indifferens
Hypochalcia lignella	Nem védett / indifferens
Hypochrysa elegans	Nem védett / indifferens
Hypocoprus latridioides	Nem védett / indifferens
Hypoganus inunctus	Nem védett / indifferens
Hypomecis punctinalis	Nem védett / indifferens
Hypomecis roboraria	Nem védett / indifferens
Hypomma bituberculatum	Nem védett / indifferens
Hypomma cornutum	Nem védett / indifferens
Hypomma fulvum	Nem védett / indifferens
Hyponephele lupinus	Védett
Hyponephele lycaon	Védett
Hypophthalmichthys molitrix	Nem védett / indifferens
Hypophthalmichthys nobilis	Nem védett / indifferens
Hypoponera punctatissima	Nem védett / indifferens
Hyporatasa allotriella	Nem védett / indifferens
Hypothenemus hampei	Nem védett / indifferens
Hypoxystis pluviaria	Nem védett / indifferens
Hyppa rectilinea	Nem védett / indifferens
Hypseloecus visci	Nem védett / indifferens
Hypsopygia costalis	Nem védett / indifferens
Hypsosinga albovittata	Nem védett / indifferens
Hypsosinga heri	Nem védett / indifferens
Hypsosinga pygmaea	Nem védett / indifferens
Hypsosinga sanguinea	Nem védett / indifferens
Hypsotropa unipunctella	Nem védett / indifferens
Hypsugo savii	Védett
Hyptiotes paradoxus	Nem védett / indifferens
Hypulus bifasciatus	Nem védett / indifferens
Hypulus quercinus	Nem védett / indifferens
Hyssia cavernosa	Védett
Hysterophora maculosana	Nem védett / indifferens
Latematium latifrons	Nem védett / indifferens
Iassus lanio	Nem védett / indifferens
Iassus mirabilis	Nem védett / indifferens
Iassus scutellaris	Nem védett / indifferens
Icaris sparganii	Nem védett / indifferens
Icodema infuscata	Nem védett / indifferens
Ictalurus punctatus	Nem védett / indifferens
Ictiobus bubalus	Nem védett / indifferens
Icus angularis	Nem védett / indifferens
Idaea aureolaria	Nem védett / indifferens
Idaea aversata	Nem védett / indifferens
Idaea bilinearia	Nem védett / indifferens
Idaea biselata	Nem védett / indifferens
Idaea degeneraria	Nem védett / indifferens
Idaea dilutaria	Nem védett / indifferens
Idaea dimidiata	Nem védett / indifferens
Idaea elongaria	Nem védett / indifferens
Idaea elongaria pecharia	Nem védett / indifferens
Idaea emarginata	Nem védett / indifferens
Idaea filicata	Nem védett / indifferens
Idaea fuscovenosa	Nem védett / indifferens
Idaea humiliata	Nem védett / indifferens
Idaea inquinata	Nem védett / indifferens
Idaea laevigata	Nem védett / indifferens
Idaea deversaria	Nem védett / indifferens
Idaea moniliata	Nem védett / indifferens
Idaea muricata	Nem védett / indifferens
Idaea nitidata	Nem védett / indifferens
Idaea obsoletaria	Nem védett / indifferens
Idaea ochrata	Nem védett / indifferens
Idaea pallidata	Nem védett / indifferens
Idaea politaria	Nem védett / indifferens
Idaea rufaria	Nem védett / indifferens
Idaea rusticata	Nem védett / indifferens
Idaea seriata	Nem védett / indifferens
Idaea sericeata	Nem védett / indifferens
Idaea serpentata	Nem védett / indifferens
Idaea straminata	Nem védett / indifferens
Idaea subsericeata	Nem védett / indifferens
Idaea sylvestraria	Nem védett / indifferens
Idaea trigeminata	Nem védett / indifferens
Idia calvaria	Védett
Populicerus albicans	Nem védett / indifferens
Idiocerus confusus	Nem védett / indifferens
Tremulicerus distinguendus	Nem védett / indifferens
Idiocerus fasciatus	Nem védett / indifferens
Tremulicerus fulgidus	Nem védett / indifferens
Idiocerus herrichii	Nem védett / indifferens
Idiocerus heydenii	Nem védett / indifferens
Idiocerus humilis	Nem védett / indifferens
Idiocerus impressifrons	Nem védett / indifferens
Idiocerus laminatus	Nem védett / indifferens
Idiocerus latifrons	Nem védett / indifferens
Idiocerus lituratus	Nem védett / indifferens
Stenidiocerus poecilus	Nem védett / indifferens
Populicerus populi	Nem védett / indifferens
Idiocerus rotundifrons	Nem védett / indifferens
Idiocerus similis	Nem védett / indifferens
Idiocerus stigmaticalis	Nem védett / indifferens
Tremulicerus tremulae	Nem védett / indifferens
Viridicerus ustulatus	Nem védett / indifferens
Idiocerus vittifrons	Nem védett / indifferens
Idiodonus cruentatus	Nem védett / indifferens
Idolus picipennis	Nem védett / indifferens
Iliocryptus agilis	Nem védett / indifferens
Iliocryptus sordidus	Nem védett / indifferens
Ilybius ater	Nem védett / indifferens
Ilybius chalconatus	Nem védett / indifferens
Ilybius crassus	Nem védett / indifferens
Ilybius erichsoni	Nem védett / indifferens
Ilybius fenestratus	Nem védett / indifferens
Ilybius fuliginosus	Nem védett / indifferens
Ilybius guttiger	Nem védett / indifferens
Ilybius neglectus	Nem védett / indifferens
Ilybius quadriguttatus	Nem védett / indifferens
Ilybius similis	Nem védett / indifferens
Ilybius subaeneus	Nem védett / indifferens
Ilybius subtilis	Nem védett / indifferens
Ilyobates bennetti	Nem védett / indifferens
Ilyobates mech	Nem védett / indifferens
Ilyobates nigricollis	Nem védett / indifferens
Ilyobates propinquus	Nem védett / indifferens
Ilyocoris cimicoides	Nem védett / indifferens
Ilyocypris bradyi	Nem védett / indifferens
Ilyocypris gibba	Nem védett / indifferens
Ilyocypris monstrifica	Nem védett / indifferens
Imnadia yeyetta	Nem védett / indifferens
Improphantes geniculatus	Nem védett / indifferens
Inachis io	Védett
Incestophantes crucifer	Nem védett / indifferens
Incurvaria koerneriella	Nem védett / indifferens
Incurvaria masculella	Nem védett / indifferens
Incurvaria oehlmanniella	Nem védett / indifferens
Incurvaria pectinea	Nem védett / indifferens
Incurvaria praelatella	Nem védett / indifferens
Infurcitinea albicomella	Nem védett / indifferens
Infurcitinea argentimaculella	Nem védett / indifferens
Infurcitinea finalis	Nem védett / indifferens
Infurcitinea roesslerella	Nem védett / indifferens
Inocellia crassicornis	Nem védett / indifferens
Involvulus cupreus	Nem védett / indifferens
Glaucopsyche iolas	Fokozottan védett
Iphiclides podalirius	Védett
Ipidia binotata	Nem védett / indifferens
Ipimorpha retusa	Nem védett / indifferens
Ipimorpha subtusa	Nem védett / indifferens
Ips acuminatus	Nem védett / indifferens
Ips cembrae	Nem védett / indifferens
Ips duplicatus	Nem védett / indifferens
Ips mannsfeldi	Nem védett / indifferens
Ips sexdentatus	Nem védett / indifferens
Ips typographus	Nem védett / indifferens
Ironoquia dubia	Nem védett / indifferens
Isauria dilucidella	Nem védett / indifferens
Ischnocoris angustulus	Nem védett / indifferens
Ischnocoris hemipterus	Nem védett / indifferens
Ischnocoris punctulatus	Nem védett / indifferens
Ischnodemus sabuleti	Nem védett / indifferens
Ischnodes sanguinicollis	Nem védett / indifferens
Ischnoglossa obscura	Nem védett / indifferens
Ischnoglossa prolixa	Nem védett / indifferens
Ischnomera caerulea	Nem védett / indifferens
Ischnomera cinerascens	Nem védett / indifferens
Ischnomera cyanea	Nem védett / indifferens
Ischnomera sanguinicollis	Nem védett / indifferens
Ischnopoda leucopus	Nem védett / indifferens
Ischnopoda umbratica	Nem védett / indifferens
Ischnopterapion aeneomicans	Nem védett / indifferens
Ischnopterapion fallens	Nem védett / indifferens
Ischnopterapion loti	Nem védett / indifferens
Ischnopterapion modestum	Nem védett / indifferens
Ischnopterapion virens	Nem védett / indifferens
Ischnosoma longicorne	Nem védett / indifferens
Ischnosoma splendidum	Nem védett / indifferens
Ischnura elegans	Nem védett / indifferens
Ischnura elegans pontica	Nem védett / indifferens
Ischnura pumilio	Nem védett / indifferens
Ischyrosyrphus glaucius	Nem védett / indifferens
Ischyrosyrphus laternarius	Nem védett / indifferens
Isidiella nickerlii	Nem védett / indifferens
Isochnus angustifrons	Nem védett / indifferens
Isochnus foliorum	Nem védett / indifferens
Isochnus populicola	Nem védett / indifferens
Isodontia mexicana	Nem védett / indifferens
Isogenus nubecula	Védett
Isognomostoma isognomostomos	Védett
Isometopus intrusus	Nem védett / indifferens
Isometopus mirificus	Nem védett / indifferens
Isomira antennata	Nem védett / indifferens
Isomira murina	Nem védett / indifferens
Isonychia ignota	Védett
Isoperla difformis	Nem védett / indifferens
Isoperla grammatica	Nem védett / indifferens
Isoperla obscura	Nem védett / indifferens
Isoperla pawlowskii	Nem védett / indifferens
Isoperla tripartita	Nem védett / indifferens
Isophrictis anthemidella	Nem védett / indifferens
Isophrictis striatella	Nem védett / indifferens
Isophya brevipennis	Védett
Isophya costata	Fokozottan védett
Isophya kraussii	Nem védett / indifferens
Isophya modesta	Védett
Isophya modestior	Védett
Isophya stysi	Fokozottan védett
Isoptena serricornis	Védett
Isoriphis marmottani	Nem védett / indifferens
Isoriphis melasoides	Nem védett / indifferens
Isotomus speciosus	Nem védett / indifferens
Isotrias hybridana	Nem védett / indifferens
Isotrias rectifasciana	Nem védett / indifferens
Issoria lathonia	Nem védett / indifferens
Issus coleoptratus	Nem védett / indifferens
Issus muscaeformis	Nem védett / indifferens
Isturgia roraria	Nem védett / indifferens
Italobdella ciosi	Nem védett / indifferens
Macaria brunneata	Nem védett / indifferens
Ithytrichia lamellaris	Nem védett / indifferens
Ityocara rubens	Nem védett / indifferens
Iwaruna klimeschi	Nem védett / indifferens
Ixapion variegatum	Nem védett / indifferens
Ixobrychus minutus	Fokozottan védett
Jalla dumosa	Nem védett / indifferens
Japananus hyalinus	Nem védett / indifferens
Jassargus distinquendus	Nem védett / indifferens
Jassargus flori	Nem védett / indifferens
Jassargus obtusivalvis	Nem védett / indifferens
Jassargus sursumflexus	Nem védett / indifferens
Jassidaeus lugubris	Nem védett / indifferens
Jassus varipennis	Nem védett / indifferens
Javesella discolor	Nem védett / indifferens
Misgurnus fossilis	Védett
Javesella dubia	Nem védett / indifferens
Javesella obscurella	Nem védett / indifferens
Javesella pellucida	Nem védett / indifferens
Javesella salina	Nem védett / indifferens
Javesella stali	Nem védett / indifferens
Jodia croceago	Nem védett / indifferens
Jodis lactearia	Nem védett / indifferens
Jordanita budensis	Nem védett / indifferens
Jordanita chloros	Nem védett / indifferens
Jordanita fazekasi	Nem védett / indifferens
Jordanita globulariae	Nem védett / indifferens
Jordanita graeca	Védett
Jordanita notata	Nem védett / indifferens
Jordanita subsolana	Nem védett / indifferens
Jucancistrocerus jucundus	Nem védett / indifferens
Judolia sexmaculata	Nem védett / indifferens
Julus scandinavius	Nem védett / indifferens
Julus scanicus	Nem védett / indifferens
Julus terrestris	Nem védett / indifferens
Julus terrestris balatonensis	Nem védett / indifferens
Jynx torquilla	Védett
Kaestneria dorsalis	Nem védett / indifferens
Kaestneria pullata	Nem védett / indifferens
Kalama henschi	Nem védett / indifferens
Kalama tricornis	Nem védett / indifferens
Kalcapion pallipes	Nem védett / indifferens
Kalcapion semivittatum	Nem védett / indifferens
Katamenes arbustorum	Nem védett / indifferens
Kateretes mixtus	Nem védett / indifferens
Kateretes pedicularius	Nem védett / indifferens
Kateretes pusillus	Nem védett / indifferens
Kateretes rufilabris	Nem védett / indifferens
Greenisca glyceriae	Nem védett / indifferens
Greenisca laeticoris	Nem védett / indifferens
Kaweckia rubra	Nem védett / indifferens
Keijia tincta	Nem védett / indifferens
Kelisia brucki	Nem védett / indifferens
Kelisia confusa	Nem védett / indifferens
Kelisia guttula	Nem védett / indifferens
Kelisia henschii	Nem védett / indifferens
Kelisia irregulata	Nem védett / indifferens
Kelisia melanops	Nem védett / indifferens
Kelisia monoceros	Nem védett / indifferens
Kelisia pallidula	Nem védett / indifferens
Kelisia pannonica	Nem védett / indifferens
Kelisia perrieri	Nem védett / indifferens
Kelisia praecox	Nem védett / indifferens
Kelisia punctulum	Nem védett / indifferens
Kelisia vittipennis	Nem védett / indifferens
Sciota adelphella	Nem védett / indifferens
Sciota fumella	Nem védett / indifferens
Sciota hostilis	Nem védett / indifferens
Sciota rhenella	Nem védett / indifferens
Kermes bacciformis	Nem védett / indifferens
Kermes corticalis	Nem védett / indifferens
Kermes gibbosus	Nem védett / indifferens
Kermes quercus	Nem védett / indifferens
Kermes roboris	Nem védett / indifferens
Khorassania compositella	Nem védett / indifferens
Kisanthobia ariasi	Védett
Kishidaia conspicua	Nem védett / indifferens
Kissophagus hederae	Nem védett / indifferens
Kleidocerys privignus	Nem védett / indifferens
Kleidocerys resedae	Nem védett / indifferens
Klimeschia transversella	Nem védett / indifferens
Klimeschiopsis kiningerella	Nem védett / indifferens
Kochiura aulica	Nem védett / indifferens
Korscheltellus lupulinus	Nem védett / indifferens
Korynetes caeruleus	Nem védett / indifferens
Korynetes ruficornis	Nem védett / indifferens
Kosswigianella exigua	Nem védett / indifferens
Kovacsia kovacsi	Fokozottan védett
Kovalevskiella phreaticola	Nem védett / indifferens
Kryphioiulus occultus	Nem védett / indifferens
Kurzia latissima	Nem védett / indifferens
Kyboasca bipunctata	Nem védett / indifferens
Kybos abstrusus	Nem védett / indifferens
Kybos butleri	Nem védett / indifferens
Kybos limpidus	Nem védett / indifferens
Kybos paraltaica	Nem védett / indifferens
Kybos populi	Nem védett / indifferens
Kybos rufescens	Nem védett / indifferens
Kybos smaragdulus	Nem védett / indifferens
Kybos strigilifer	Nem védett / indifferens
Kybos strobli	Nem védett / indifferens
Kybos virgator	Nem védett / indifferens
Kyklioacalles suturatus	Nem védett / indifferens
Labarrus lividus	Nem védett / indifferens
Labiaticola atricolor	Nem védett / indifferens
Laburrus handlirschi	Nem védett / indifferens
Laburrus impictifrons	Nem védett / indifferens
Laburrus pellax	Nem védett / indifferens
Lacanobia aliena	Nem védett / indifferens
Lacanobia blenna	Nem védett / indifferens
Lacanobia contigua	Nem védett / indifferens
Lacanobia oleracea	Nem védett / indifferens
Lacanobia splendens	Nem védett / indifferens
Lacanobia suasa	Nem védett / indifferens
Lacanobia thalassina	Nem védett / indifferens
Lacanobia w-latinum	Nem védett / indifferens
Laccobius albipes	Nem védett / indifferens
Laccobius alternus	Nem védett / indifferens
Laccobius bipunctatus	Nem védett / indifferens
Laccobius colon	Nem védett / indifferens
Laccobius gracilis	Nem védett / indifferens
Laccobius minutus	Nem védett / indifferens
Laccobius obscuratus	Nem védett / indifferens
Laccobius simulatrix	Nem védett / indifferens
Laccobius sinuatus	Nem védett / indifferens
Laccobius striatulus	Nem védett / indifferens
Laccobius syriacus	Nem védett / indifferens
Laccophilus hyalinus	Nem védett / indifferens
Laccophilus minutus	Nem védett / indifferens
Laccophilus poecilus	Nem védett / indifferens
Laccornis kocae	Nem védett / indifferens
Laccornis oblongus	Nem védett / indifferens
Lacerta agilis	Védett
Lacerta viridis	Védett
Lachnaeus crinitus	Nem védett / indifferens
Laciniaria plicata	Nem védett / indifferens
Laelia coenosa	Nem védett / indifferens
Laemophloeus kraussi	Nem védett / indifferens
Laemophloeus monilis	Nem védett / indifferens
Laemostenus terricola	Nem védett / indifferens
Laena reitteri	Nem védett / indifferens
Laena viennensis	Nem védett / indifferens
Lagria atripes	Nem védett / indifferens
Lagria hirta	Nem védett / indifferens
Lamellaxis mauritianus	Nem védett / indifferens
Lamellocossus terebra	Védett
Lamia textor	Nem védett / indifferens
Lamoria anella	Nem védett / indifferens
Lampides boeticus	Nem védett / indifferens
Ovalisia dives	Védett
Ovalisia mirifica	Védett
Ovalisia rutilans	Védett
Lamprinodes haematopterus	Nem védett / indifferens
Lamprinodes saginatus	Nem védett / indifferens
Lamprinus erythropterus	Nem védett / indifferens
Lamprobyrrhulus nitidus	Nem védett / indifferens
Lamprodema maura	Nem védett / indifferens
Lamproglena pulchella	Nem védett / indifferens
Lamprohiza splendidula	Nem védett / indifferens
Lampronia corticella	Nem védett / indifferens
Lampronia flavimitrella	Nem védett / indifferens
Lampronia fuscatella	Nem védett / indifferens
Lampronia morosa	Nem védett / indifferens
Lampronia pubicornis	Nem védett / indifferens
Lampronia rupella	Nem védett / indifferens
Lamproplax picea	Nem védett / indifferens
Lampropteryx suffumata	Nem védett / indifferens
Lamprosticta culta	Nem védett / indifferens
Lamprotes c-aureum	Védett
Lamprotettix nitidulus	Nem védett / indifferens
Lampyris noctiluca	Nem védett / indifferens
Lamyctes fulvicornis	Nem védett / indifferens
Langelandia anophtalma	Nem védett / indifferens
Lanius collurio	Védett
Lanius excubitor	Védett
Lanius excubitor homeyeri	Védett
Lanius minor	Védett
Lanius senator	Védett
Laodelphax striatellus	Nem védett / indifferens
Laothoe populi	Nem védett / indifferens
Larentia clavaria	Védett
Larinia bonneti	Nem védett / indifferens
Larinia elegans	Nem védett / indifferens
Larinia jeskovi	Nem védett / indifferens
Larinioides cornutus	Nem védett / indifferens
Larinioides ixobolus	Nem védett / indifferens
Larinioides patagiatus	Nem védett / indifferens
Larinioides sclopetarius	Nem védett / indifferens
Larinioides suspicax	Nem védett / indifferens
Larinus beckeri	Nem védett / indifferens
Larinus brevis	Nem védett / indifferens
Larinus canescens	Nem védett / indifferens
Larinus jaceae	Nem védett / indifferens
Larinus latus	Nem védett / indifferens
Larinus minutus	Nem védett / indifferens
Larinus obtusus	Nem védett / indifferens
Larinus planus	Nem védett / indifferens
Larinus rugulosus	Nem védett / indifferens
Larinus sturnus	Nem védett / indifferens
Larinus turbinatus	Nem védett / indifferens
Larra anathema	Nem védett / indifferens
Larus argentatus	Védett
Larus cachinnans	Védett
Larus michahellis	Védett
Larus canus	Védett
Larus canus heinei	Védett
Larus delawarensis	Védett
Larus fuscus	Védett
Larus fuscus heuglini	Védett
Larus genei	Védett
Larus glaucoides	Védett
Larus hyperboreus	Védett
Larus ichthyaetus	Védett
Larus marinus	Védett
Larus melanocephalus	Fokozottan védett
Larus minutus	Védett
Larus pipixcan	Védett
Larus ridibundus	Védett
Larus sabini	Védett
Lasaeola prona	Nem védett / indifferens
Lasaeola tristis	Nem védett / indifferens
Lasiacantha capucina	Nem védett / indifferens
Lasiacantha gracilis	Nem védett / indifferens
Lasiacantha hermani	Nem védett / indifferens
Lasiocampa quercus	Nem védett / indifferens
Lasiocampa trifolii	Nem védett / indifferens
Lasiocephala basalis	Nem védett / indifferens
Lasioderma obscurum	Nem védett / indifferens
Lasioderma redtenbacheri	Nem védett / indifferens
Lasioderma serricorne	Nem védett / indifferens
Lasioderma thoracicum	Nem védett / indifferens
Lasioglossum aeratum	Nem védett / indifferens
Lasioglossum albipes	Nem védett / indifferens
Lasioglossum albocinctum	Nem védett / indifferens
Lasioglossum angusticeps	Nem védett / indifferens
Lasioglossum bluethgeni	Nem védett / indifferens
Lasioglossum brevicorne	Nem védett / indifferens
Lasioglossum brevicorne aciculatum	Nem védett / indifferens
Lasioglossum buccale	Nem védett / indifferens
Lasioglossum calceatum	Nem védett / indifferens
Lasioglossum clypeare	Nem védett / indifferens
Lasioglossum convexiusculum	Nem védett / indifferens
Lasioglossum corvinum	Nem védett / indifferens
Lasioglossum costulatum	Nem védett / indifferens
Lasioglossum crassepunctatum	Nem védett / indifferens
Lasioglossum damascenum	Nem védett / indifferens
Lasioglossum discum	Nem védett / indifferens
Lasioglossum elegans	Nem védett / indifferens
Lasioglossum euboeense	Nem védett / indifferens
Lasioglossum fratellum	Nem védett / indifferens
Lasioglossum fulvicorne	Nem védett / indifferens
Lasioglossum glabriusculum	Nem védett / indifferens
Lasioglossum griseolum	Nem védett / indifferens
Lasioglossum intermedium	Nem védett / indifferens
Lasioglossum interruptum	Nem védett / indifferens
Lasioglossum kussariense	Nem védett / indifferens
Lasioglossum laeve	Nem védett / indifferens
Lasioglossum laevigatum	Nem védett / indifferens
Lasioglossum laterale	Nem védett / indifferens
Lasioglossum laticeps	Nem védett / indifferens
Lasioglossum lativentre	Nem védett / indifferens
Lasioglossum leucopum	Nem védett / indifferens
Lasioglossum leucozonium	Nem védett / indifferens
Lasioglossum limbellum	Nem védett / indifferens
Lasioglossum lineare	Nem védett / indifferens
Lasioglossum lissonotum	Nem védett / indifferens
Lasioglossum lucidulum	Nem védett / indifferens
Lasioglossum majus	Nem védett / indifferens
Lasioglossum malachurum	Nem védett / indifferens
Lasioglossum mandibulare	Nem védett / indifferens
Lasioglossum marginatum	Nem védett / indifferens
Lasioglossum marginellum	Nem védett / indifferens
Lasioglossum mesosclerum	Nem védett / indifferens
Lasioglossum minutissimum	Nem védett / indifferens
Lasioglossum minutulum	Nem védett / indifferens
Lasioglossum morio	Nem védett / indifferens
Lasioglossum nigripes	Nem védett / indifferens
Lasioglossum nitidiusculum	Nem védett / indifferens
Lasioglossum nitidulum	Nem védett / indifferens
Lasioglossum nitidulum aeneidorsum	Nem védett / indifferens
Lasioglossum obscuratum	Nem védett / indifferens
Lasioglossum obscuratum acerbum	Nem védett / indifferens
Lasioglossum pallens	Nem védett / indifferens
Lasioglossum parvulum	Nem védett / indifferens
Lasioglossum pauxillum	Nem védett / indifferens
Lasioglossum podolicum	Nem védett / indifferens
Lasioglossum politum	Nem védett / indifferens
Lasioglossum pseudocaspicum	Nem védett / indifferens
Lasioglossum punctatissimum	Nem védett / indifferens
Lasioglossum puncticolle	Nem védett / indifferens
Lasioglossum pygmaeum	Nem védett / indifferens
Lasioglossum quadrinotatulum	Nem védett / indifferens
Lasioglossum quadrinotatum	Nem védett / indifferens
Lasioglossum quadrisignatum	Nem védett / indifferens
Lasioglossum rufitarse	Nem védett / indifferens
Lasioglossum samaricum	Nem védett / indifferens
Lasioglossum semilucens	Nem védett / indifferens
Lasioglossum setulellum	Nem védett / indifferens
Lasioglossum setulosum	Nem védett / indifferens
Lasioglossum sexnotatum	Nem védett / indifferens
Lasioglossum sexstrigatum	Nem védett / indifferens
Lasioglossum subfasciatum	Nem védett / indifferens
Lasioglossum subhirtum	Nem védett / indifferens
Lasioglossum tarsatum	Nem védett / indifferens
Lasioglossum trichopygum	Nem védett / indifferens
Lasioglossum tricinctum	Nem védett / indifferens
Lasioglossum truncaticolle	Nem védett / indifferens
Lasioglossum villosulum	Nem védett / indifferens
Lasioglossum xanthopum	Nem védett / indifferens
Lasioglossum zonulum	Nem védett / indifferens
Lasiommata maera	Nem védett / indifferens
Lasiommata megera	Nem védett / indifferens
Lasiommata petropolitana	Nem védett / indifferens
Lasionycta imbecilla	Nem védett / indifferens
Lasionycta proxima	Nem védett / indifferens
Lasiorhynchites cavifrons	Nem védett / indifferens
Lasiorhynchites coeruleocephalus	Nem védett / indifferens
Lasiorhynchites olivaceus	Nem védett / indifferens
Lasiorhynchites praeustus	Nem védett / indifferens
Lasiorhynchites sericeus	Nem védett / indifferens
Lasiosomus enervis	Nem védett / indifferens
Blemus discus	Nem védett / indifferens
Lasius alienus	Nem védett / indifferens
Lasius balcanicus	Nem védett / indifferens
Lasius bicornis	Nem védett / indifferens
Lasius brunneus	Nem védett / indifferens
Lasius carniolicus	Nem védett / indifferens
Lasius citrinus	Nem védett / indifferens
Lasius emarginatus	Nem védett / indifferens
Lasius flavus	Nem védett / indifferens
Lasius fuliginosus	Nem védett / indifferens
Lasius meridionalis	Nem védett / indifferens
Lasius mixtus	Nem védett / indifferens
Lasius myops	Nem védett / indifferens
Lasius neglectus	Nem védett / indifferens
Lasius niger	Nem védett / indifferens
Lasius paralienus	Nem védett / indifferens
Lasius platythorax	Nem védett / indifferens
Lasius psammophilus	Nem védett / indifferens
Lasius umbratus	Nem védett / indifferens
Laspeyria flexula	Nem védett / indifferens
Latheticus oryzae	Nem védett / indifferens
Lathonura rectirostris	Nem védett / indifferens
Lathrobium brunnipes	Nem védett / indifferens
Lathrobium castaneipenne	Nem védett / indifferens
Lathrobium crassipes	Nem védett / indifferens
Lathrobium elegantulum	Nem védett / indifferens
Lathrobium elongatum	Nem védett / indifferens
Lathrobium fovulum	Nem védett / indifferens
Lathrobium fulvipenne	Nem védett / indifferens
Lathrobium furcatum	Nem védett / indifferens
Lathrobium impressum	Nem védett / indifferens
Lathrobium laevipenne	Nem védett / indifferens
Lathrobium longulum	Nem védett / indifferens
Lathrobium pallidum	Nem védett / indifferens
Lathrobium ripicola	Nem védett / indifferens
Lathrobium rufipenne	Nem védett / indifferens
Lathrobium spadiceum	Nem védett / indifferens
Lathrobium testaceum	Nem védett / indifferens
Lathrobium volgense	Nem védett / indifferens
Lathronympha strigana	Nem védett / indifferens
Lathropus sepicola	Nem védett / indifferens
Lathys humilis	Nem védett / indifferens
Lathys stigmatisata	Nem védett / indifferens
Latona setifera	Nem védett / indifferens
Latridius anthracinus	Nem védett / indifferens
Latridius brevicollis	Nem védett / indifferens
Latridius consimilis	Nem védett / indifferens
Latridius hirtus	Nem védett / indifferens
Latridius minutus	Nem védett / indifferens
Lamprias chlorocephalus	Nem védett / indifferens
Lebia cruxminor	Nem védett / indifferens
Lamprias cyanocephalus	Nem védett / indifferens
Lebia humeralis	Nem védett / indifferens
Lebia marginata	Nem védett / indifferens
Lebia scapularis	Nem védett / indifferens
Lebia trimaculata	Nem védett / indifferens
Lecanodiaspis sardoa	Nem védett / indifferens
Lecanopsis festucae	Nem védett / indifferens
Lecanopsis formicarum	Nem védett / indifferens
Lecanopsis porifera	Nem védett / indifferens
Lecanopsis terrestris	Nem védett / indifferens
Lecithocera nigrana	Nem védett / indifferens
Ledra aurita	Nem védett / indifferens
Legnotus limbosus	Nem védett / indifferens
Legnotus picipes	Nem védett / indifferens
Lehmannia marginata	Nem védett / indifferens
Lehmannia nyctelia	Nem védett / indifferens
Lehmannia valentiana	Nem védett / indifferens
Leichenum pictum	Nem védett / indifferens
Leiestes seminiger	Nem védett / indifferens
Leioderes kollari	Nem védett / indifferens
Leiodes austriaca	Nem védett / indifferens
Leiodes carpathica	Nem védett / indifferens
Leiodes bicolor	Nem védett / indifferens
Leiodes brunneus	Nem védett / indifferens
Leiodes ciliaris	Nem védett / indifferens
Leiodes cinnamomeus	Nem védett / indifferens
Leiodes dubius	Nem védett / indifferens
Leiodes ferruginea	Nem védett / indifferens
Leiodes flavescens	Nem védett / indifferens
Leiodes furva	Nem védett / indifferens
Leiodes gallicus	Nem védett / indifferens
Leiodes gyllenhalii	Nem védett / indifferens
Leiodes hybrida	Nem védett / indifferens
Leiodes longipes	Nem védett / indifferens
Leiodes lucens	Nem védett / indifferens
Leiodes lunicollis	Nem védett / indifferens
Leiodes macropus	Nem védett / indifferens
Leiodes obesa	Nem védett / indifferens
Leiodes oblonga	Nem védett / indifferens
Leiodes pallens	Nem védett / indifferens
Leiodes polita	Nem védett / indifferens
Leiodes rotundatus	Nem védett / indifferens
Leiodes rubiginosus	Nem védett / indifferens
Leiodes rugosa	Nem védett / indifferens
Leiodes strigipenne	Nem védett / indifferens
Leiodes subtilis	Nem védett / indifferens
Leiodes triepkei	Nem védett / indifferens
Leiodes vladimiri	Nem védett / indifferens
Leiopsammodius haruspex	Nem védett / indifferens
Leiopus nebulosus	Nem védett / indifferens
Leiopus punctulatus	Védett
Leiosoma scrobifer baudii	Nem védett / indifferens
Leiosoma concinnum	Nem védett / indifferens
Leiosoma cribrum	Nem védett / indifferens
Leiosoma deflexum	Nem védett / indifferens
Leiosoma oblongulum	Nem védett / indifferens
Leistus ferrugineus	Nem védett / indifferens
Leistus piceus	Nem védett / indifferens
Leistus piceus kaszabi	Nem védett / indifferens
Leistus rufomarginatus	Nem védett / indifferens
Leistus terminatus	Védett
Lejogaster metallina	Nem védett / indifferens
Lejogaster tarsata	Nem védett / indifferens
Lejops vittatus	Nem védett / indifferens
Lemonia dumi	Védett
Lemonia taraxaci	Védett
Hylesinus wachtli orni	Nem védett / indifferens
Hylesinus fraxini	Nem védett / indifferens
Lepidosaphes conchiformis	Nem védett / indifferens
Lepidosaphes granati	Nem védett / indifferens
Lepidosaphes juniperi	Nem védett / indifferens
Lepidosaphes minimus	Nem védett / indifferens
Lepidosaphes newsteadi	Nem védett / indifferens
Lepidosaphes populi	Nem védett / indifferens
Lepidosaphes tiliae	Nem védett / indifferens
Lepidosaphes ulmi	Nem védett / indifferens
Lepidostoma hirtum	Nem védett / indifferens
Lepidurus apus	Nem védett / indifferens
Lepomis gibbosus	Inváziós
Leptacinus batychrus	Nem védett / indifferens
Leptacinus formicetorum	Nem védett / indifferens
Leptacinus intermedius	Nem védett / indifferens
Leptacinus merkli	Nem védett / indifferens
Leptacinus pusillus	Nem védett / indifferens
Leptacinus sulcifrons	Nem védett / indifferens
Leptestheria dahalacensis	Nem védett / indifferens
Ipa keyserlingi	Nem védett / indifferens
Lepthyphantes leprosus	Nem védett / indifferens
Lepthyphantes minutus	Nem védett / indifferens
Leptidea morsei	Védett
Leptidea morsei major	Védett
Leptidea reali	Nem védett / indifferens
Leptidea sinapis	Nem védett / indifferens
Leptinus testaceus	Nem védett / indifferens
Leptobium gracile	Nem védett / indifferens
Leptocerus interruptus	Nem védett / indifferens
Leptocerus tineiformis	Nem védett / indifferens
Leptochilus alpestris	Nem védett / indifferens
Leptochilus regulus	Nem védett / indifferens
Leptochilus tarsatus	Nem védett / indifferens
Leptodora kindti	Nem védett / indifferens
Leptoiulus baconyensis	Nem védett / indifferens
Leptoiulus cibdellus	Nem védett / indifferens
Leptoiulus proximus	Nem védett / indifferens
Leptoiulus proximus noaranus	Nem védett / indifferens
Leptoiulus saltuvagus	Nem védett / indifferens
Leptoiulus simplex	Nem védett / indifferens
Leptoiulus simplex attenuatus	Nem védett / indifferens
Leptoiulus trilobatus	Nem védett / indifferens
Leptoiulus tussilaginis	Nem védett / indifferens
Leptophius flavocinctus	Nem védett / indifferens
Leptophlebia marginata	Nem védett / indifferens
Leptophloeus alternans	Nem védett / indifferens
Leptophloeus clematidis	Nem védett / indifferens
Leptophloeus hypobori	Nem védett / indifferens
Leptophloeus juniperi	Nem védett / indifferens
Leptophyes albovittata	Nem védett / indifferens
Leptophyes boscii	Nem védett / indifferens
Leptophyes discoidalis	Védett
Leptophyes laticauda	Nem védett / indifferens
Leptophyes punctatissima	Nem védett / indifferens
Leptopterna dolabrata	Nem védett / indifferens
Leptopterna ferrugata	Nem védett / indifferens
Leptopus marmoratus	Nem védett / indifferens
Leptorchestes berolinensis	Nem védett / indifferens
Leptotes pirithous	Nem védett / indifferens
Leptothorax acervorum	Nem védett / indifferens
Leptothorax affinis	Nem védett / indifferens
Leptothorax clypeatus	Nem védett / indifferens
Leptothorax corticalis	Nem védett / indifferens
Leptothorax crassispinus	Nem védett / indifferens
Leptothorax gredleri	Nem védett / indifferens
Leptothorax interruptus	Nem védett / indifferens
Leptothorax muscorum	Nem védett / indifferens
Leptothorax nigriceps	Nem védett / indifferens
Leptothorax parvulus	Nem védett / indifferens
Leptothorax rabaudi	Nem védett / indifferens
Leptothorax sordidulus	Nem védett / indifferens
Leptothorax sordidulus saxonicus	Nem védett / indifferens
Leptothorax tuberum	Nem védett / indifferens
Leptothorax unifasciatus	Nem védett / indifferens
Leptura aethiops	Nem védett / indifferens
Leptura annularis	Védett
Leptura aurulenta	Nem védett / indifferens
Leptura quadrifasciata	Nem védett / indifferens
Lepturobosca virens	Nem védett / indifferens
Leptusa flavicornis	Nem védett / indifferens
Leptusa fuliginosa	Nem védett / indifferens
Leptusa fumida	Nem védett / indifferens
Leptusa pulchella	Nem védett / indifferens
Leptusa ruficollis	Nem védett / indifferens
Lepus europaeus	Nem védett / indifferens
Lepyronia coleoptrata	Nem védett / indifferens
Lepyrus armatus	Nem védett / indifferens
Lepyrus capucinus	Nem védett / indifferens
Lepyrus palustris	Nem védett / indifferens
Lernaea cyprinacea	Nem védett / indifferens
Lernaea esocina	Nem védett / indifferens
Lestes barbarus	Nem védett / indifferens
Lestes dryas	Védett
Lestes macrostigma	Védett
Lestes sponsa	Nem védett / indifferens
Lestes virens	Nem védett / indifferens
Lestes virens vestalis	Nem védett / indifferens
Lesteva longoelytrata	Nem védett / indifferens
Lesteva luctuosa	Nem védett / indifferens
Lesteva pubescens	Nem védett / indifferens
Lesteva punctata	Nem védett / indifferens
Lestica alata	Nem védett / indifferens
Lestica clypeata	Nem védett / indifferens
Lestica subterranea	Nem védett / indifferens
Lestiphorus bicinctus	Nem védett / indifferens
Lestiphorus bilunulatus	Nem védett / indifferens
Lethrus apterus	Védett
Leucania comma	Nem védett / indifferens
Leucania loreyi	Nem védett / indifferens
Leucania obsoleta	Nem védett / indifferens
Leucaspis lowi	Nem védett / indifferens
Leucaspis pini	Nem védett / indifferens
Leucaspis pusilla	Nem védett / indifferens
Leucaspis signoreti	Nem védett / indifferens
Leucaspius delineatus	Védett
Squalius cephalus	Nem védett / indifferens
Leuciscus idus	Nem védett / indifferens
Leuciscus leuciscus	Védett
Telestes souffia	Védett
Leucodonta bicoloria	Védett
Leucohimatium jakowlewi	Nem védett / indifferens
Leucohimatium langei	Nem védett / indifferens
Leucoma salicis	Nem védett / indifferens
Leucophyes pedestris	Nem védett / indifferens
Leucoptera aceris	Nem védett / indifferens
Leucoptera cytisiphagella	Nem védett / indifferens
Leucoptera genistae	Nem védett / indifferens
Leucoptera heringiella	Nem védett / indifferens
Leucoptera laburnella	Nem védett / indifferens
Leucoptera lotella	Nem védett / indifferens
Leucoptera lustratella	Nem védett / indifferens
Leucoptera malifoliella	Nem védett / indifferens
Leucoptera onobrychidella	Nem védett / indifferens
Leucoptera spartifoliella	Nem védett / indifferens
Leucorrhinia caudalis	Fokozottan védett
Leucorrhinia pectoralis	Fokozottan védett
Leucospilapteryx omissella	Nem védett / indifferens
Leucozona lucorum	Nem védett / indifferens
Leuctra albida	Nem védett / indifferens
Leuctra autumnalis	Nem védett / indifferens
Leuctra braueri	Nem védett / indifferens
Leuctra carpathica	Nem védett / indifferens
Leuctra digitata	Nem védett / indifferens
Leuctra fusca	Nem védett / indifferens
Leuctra hippopus	Nem védett / indifferens
Leuctra inermis	Nem védett / indifferens
Leuctra major	Nem védett / indifferens
Leuctra mortoni	Nem védett / indifferens
Leuctra nigra	Nem védett / indifferens
Leuctra prima	Nem védett / indifferens
Leuctra pseudosignifera	Nem védett / indifferens
Leviellus thorelli	Nem védett / indifferens
Leydigia acanthocercoides	Nem védett / indifferens
Leydigia leydigi	Nem védett / indifferens
Libelloides macaronius	Fokozottan védett
Libellula depressa	Nem védett / indifferens
Libellula fulva	Védett
Libellula quadrimaculata	Nem védett / indifferens
Libythea celtis	Védett
Lichenophanes varius	Nem védett / indifferens
Lichtensia viburni	Nem védett / indifferens
Licinus cassideus	Nem védett / indifferens
Licinus depressus	Nem védett / indifferens
Licinus hoffmanseggii	Nem védett / indifferens
Ligdia adustata	Nem védett / indifferens
Lignyodes bischoffi	Nem védett / indifferens
Lignyodes enucleator	Nem védett / indifferens
Lignyodes suturatus	Nem védett / indifferens
Lignyoptera fumidaria	Fokozottan védett
Limacus flavus	Nem védett / indifferens
Limarus maculatus	Nem védett / indifferens
Limax cinereoniger	Nem védett / indifferens
Limax maximus	Nem védett / indifferens
Limenitis camilla	Védett
Limenitis populi	Fokozottan védett
Limenitis reducta	Védett
Limicola falcinellus	Védett
Limnadia lenticularis	Nem védett / indifferens
Limnaecia phragmitella	Nem védett / indifferens
Limnebius aluta	Nem védett / indifferens
Limnebius atomus	Nem védett / indifferens
Limnebius crinifer	Nem védett / indifferens
Limnebius nitidus	Nem védett / indifferens
Limnebius papposus	Nem védett / indifferens
Limnebius parvulus	Nem védett / indifferens
Limnebius stagnalis	Nem védett / indifferens
Limnebius truncatellus	Nem védett / indifferens
Limnephilus affinis	Nem védett / indifferens
Limnephilus auricula	Nem védett / indifferens
Limnephilus bipunctatus	Nem védett / indifferens
Limnephilus centralis	Nem védett / indifferens
Limnephilus decipiens	Nem védett / indifferens
Limnephilus elegans	Védett
Limnephilus extricatus	Nem védett / indifferens
Limnephilus flavicornis	Nem védett / indifferens
Limnephilus fuscicornis	Nem védett / indifferens
Limnephilus griseus	Nem védett / indifferens
Limnephilus hirsutus	Nem védett / indifferens
Limnephilus ignavus	Nem védett / indifferens
Limnephilus incisus	Nem védett / indifferens
Limnephilus lunatus	Nem védett / indifferens
Limnephilus nigriceps	Nem védett / indifferens
Limnephilus politus	Nem védett / indifferens
Limnephilus rhombicus	Nem védett / indifferens
Limnephilus sparsus	Nem védett / indifferens
Limnephilus stigma	Nem védett / indifferens
Limnephilus subcentralis	Nem védett / indifferens
Limnephilus tauricus	Nem védett / indifferens
Limnephilus vittatus	Nem védett / indifferens
Limnephilus borealis	Nem védett / indifferens
Limnichus incanus	Nem védett / indifferens
Limnichus pygmaeus	Nem védett / indifferens
Limnichus sericeus	Nem védett / indifferens
Limnius intermedius	Nem védett / indifferens
Limnius muelleri	Nem védett / indifferens
Limnius perrisi	Nem védett / indifferens
Limnius volckmari	Nem védett / indifferens
Limnobaris dolorosa	Nem védett / indifferens
Limnobaris t-album	Nem védett / indifferens
Limnocamptus hoferi	Nem védett / indifferens
Limnocythere inopinata	Nem védett / indifferens
Limnocythere sanctipatricii	Nem védett / indifferens
Limnodromus scolopaceus	Védett
Limnomysis benedeni	Nem védett / indifferens
Limnoporus rufoscutellatus	Nem védett / indifferens
Limnoxenus niger	Nem védett / indifferens
Limobius borealis	Nem védett / indifferens
Limoniscus violaceus	Fokozottan védett
Limonius minutus	Nem védett / indifferens
Limosa lapponica	Védett
Limosa limosa	Fokozottan védett
Limotettix striola	Nem védett / indifferens
Lindenius albilabris	Nem védett / indifferens
Lindenius laevis	Nem védett / indifferens
Lindenius mesopleuralis	Nem védett / indifferens
Lindenius panzeri	Nem védett / indifferens
Lindenius parkanensis	Nem védett / indifferens
Lindenius pygmaeus	Nem védett / indifferens
Lindenius pygmaeus armatus	Nem védett / indifferens
Linnavuoriana decempunctata	Nem védett / indifferens
Linnavuoriana sexmaculata	Nem védett / indifferens
Linyphia hortensis	Nem védett / indifferens
Linyphia tenuipalpis	Nem védett / indifferens
Linyphia triangularis juniperina	Nem védett / indifferens
Protaetia lugubris	Védett
Liocoris tripustulatus	Nem védett / indifferens
Liocranoeca striata gracilior	Nem védett / indifferens
Liocranum rupicola	Nem védett / indifferens
Sagana rutilans	Nem védett / indifferens
Liocyrtusa minuta	Nem védett / indifferens
Liocyrtusa nigriclavis	Nem védett / indifferens
Liocyrtusa vittata	Nem védett / indifferens
Lioderina linearis	Védett
Liogluta alpestris	Nem védett / indifferens
Liogluta granigera	Nem védett / indifferens
Liogluta longiuscula	Nem védett / indifferens
Liogluta microptera	Nem védett / indifferens
Liogluta pagana	Nem védett / indifferens
Liometopum microcephalum	Nem védett / indifferens
Lionychus quadrillum	Nem védett / indifferens
Liophloeus gibbus	Nem védett / indifferens
Liophloeus lentus	Nem védett / indifferens
Liophloeus liptoviensis	Nem védett / indifferens
Liophloeus pupillatus	Nem védett / indifferens
Liophloeus tessulatus	Nem védett / indifferens
Liorhyssus hyalinus	Nem védett / indifferens
Liothorax niger	Nem védett / indifferens
Liothorax plagiatus	Nem védett / indifferens
Liparthrum bartschti	Nem védett / indifferens
Liparus coronatus	Nem védett / indifferens
Liparus dirus	Nem védett / indifferens
Liparus germanus	Nem védett / indifferens
Liparus glabrirostris	Nem védett / indifferens
Liparus transsylvanicus	Nem védett / indifferens
Liris nigra	Nem védett / indifferens
Lissodema cursor	Nem védett / indifferens
Lissodema denticolle	Nem védett / indifferens
Litargus balteatus	Nem védett / indifferens
Litargus connexus	Nem védett / indifferens
Lithax niger	Nem védett / indifferens
Lithax obscurus	Nem védett / indifferens
Lithobius aeruginosus	Nem védett / indifferens
Lithobius aeruginosus batorligetensis	Nem védett / indifferens
Lithobius agilis	Nem védett / indifferens
Lithobius agilis pannonicus	Nem védett / indifferens
Lithobius agilis tricalcaratus	Nem védett / indifferens
Lithobius austriacus	Nem védett / indifferens
Lithobius borealis	Nem védett / indifferens
Lithobius crassipes	Nem védett / indifferens
Lithobius curtipes	Nem védett / indifferens
Lithobius dentatus	Nem védett / indifferens
Lithobius erythrocephalus	Nem védett / indifferens
Lithobius erythrocephalus schuleri	Nem védett / indifferens
Lithobius forficatus	Nem védett / indifferens
Lithobius lapidicola	Nem védett / indifferens
Lithobius luteus	Nem védett / indifferens
Lithobius macilentus	Nem védett / indifferens
Lithobius melanops	Nem védett / indifferens
Lithobius microps	Nem védett / indifferens
Lithobius mutabilis	Nem védett / indifferens
Lithobius mutabilis carpathicus	Nem védett / indifferens
Lithobius muticus	Nem védett / indifferens
Lithobius nodulipes	Nem védett / indifferens
Lithobius nodulipes scarabanciae	Nem védett / indifferens
Lithobius parietum	Nem védett / indifferens
Lithobius parietum mecsekensis	Nem védett / indifferens
Lithobius pelidnus	Nem védett / indifferens
Lithobius piceus	Nem védett / indifferens
Lithobius punctulatus	Nem védett / indifferens
Lithobius pusillus	Nem védett / indifferens
Lithobius pusillus novemaculatus	Nem védett / indifferens
Lithobius stygius	Nem védett / indifferens
Lithobius stygius infernus	Nem védett / indifferens
Lithobius tenebrosus	Nem védett / indifferens
Lithobius tenebrosus sulcatipes	Nem védett / indifferens
Lithobius tricuspis	Nem védett / indifferens
Lithocharis nigriceps	Nem védett / indifferens
Lithocharis ochracea	Nem védett / indifferens
Lithoglyphus naticoides	Nem védett / indifferens
Lithophane consocia	Nem védett / indifferens
Lithophane furcifera	Nem védett / indifferens
Lithophane ornitopus	Nem védett / indifferens
Lithophane semibrunnea	Védett
Lithophane socia	Nem védett / indifferens
Lithosia quadra	Nem védett / indifferens
Lithostege farinata	Nem védett / indifferens
Lithostege griseata	Nem védett / indifferens
Lithurgus chrysurus	Nem védett / indifferens
Lithurgus cornutus	Nem védett / indifferens
Lithurgus cornutus fuscipennis	Nem védett / indifferens
Lixus albomarginatus	Nem védett / indifferens
Lixus angustatus	Nem védett / indifferens
Lixus angustus	Nem védett / indifferens
Lixus apfelbecki	Nem védett / indifferens
Lixus bardanae	Nem védett / indifferens
Lixus bituberculatus	Nem védett / indifferens
Lixus brevipes	Nem védett / indifferens
Lixus cardui	Nem védett / indifferens
Lixus cribricollis	Nem védett / indifferens
Lixus cylindrus	Nem védett / indifferens
Lixus elegantulus	Nem védett / indifferens
Lixus euphorbiae	Nem védett / indifferens
Lixus fasciculatus	Nem védett / indifferens
Lixus filiformis	Nem védett / indifferens
Lixus iridis	Nem védett / indifferens
Lixus lateralis	Nem védett / indifferens
Lixus myagri	Nem védett / indifferens
Lixus neglectus	Nem védett / indifferens
Lixus ochraceus	Nem védett / indifferens
Lixus paraplecticus	Nem védett / indifferens
Lixus punctirostris	Nem védett / indifferens
Lixus punctiventris	Nem védett / indifferens
Lixus rubicundus	Nem védett / indifferens
Lixus scabricollis	Nem védett / indifferens
Lixus subtilis	Nem védett / indifferens
Lixus tibialis	Nem védett / indifferens
Lixus vilis	Nem védett / indifferens
Lobesia abscisana	Nem védett / indifferens
Lobesia artemisiana	Nem védett / indifferens
Lobesia bicinctana	Nem védett / indifferens
Lobesia botrana	Nem védett / indifferens
Lobesia confinitana	Nem védett / indifferens
Lobesia euphorbiana	Nem védett / indifferens
Lobesia reliquana	Nem védett / indifferens
Lobophora halterata	Nem védett / indifferens
Lobrathium multipunctum	Nem védett / indifferens
Locusta migratoria	Védett
Locustella fluviatilis	Védett
Locustella luscinioides	Védett
Locustella naevia	Védett
Lomaspilis marginata	Nem védett / indifferens
Lomaspilis opis	Nem védett / indifferens
Lomechusa emarginata	Nem védett / indifferens
Lomechusa paradoxa	Nem védett / indifferens
Lomechusa pubicollis	Nem védett / indifferens
Lomechusoides strumosus	Nem védett / indifferens
Lomographa bimaculata	Nem védett / indifferens
Lomographa temerata	Nem védett / indifferens
Longicoccus festucae	Nem védett / indifferens
Longicoccus psammophilus	Nem védett / indifferens
Lopheros rubens	Nem védett / indifferens
Lophomma punctatum	Nem védett / indifferens
Lopinga achine	Fokozottan védett
Lopus decolor	Nem védett / indifferens
Loraphodius suarius	Nem védett / indifferens
Loraspis frater	Nem védett / indifferens
Lordithon bimaculatus	Nem védett / indifferens
Lordithon exoletus	Nem védett / indifferens
Lordithon lunulatus	Nem védett / indifferens
Lordithon pulchellus	Nem védett / indifferens
Lordithon speciosus	Nem védett / indifferens
Lordithon thoracicus	Nem védett / indifferens
Lordithon trimaculatus	Nem védett / indifferens
Lordithon trinotatus	Nem védett / indifferens
Loricera pilicornis	Nem védett / indifferens
Loricula elegantula	Nem védett / indifferens
Loricula pselaphiformis	Nem védett / indifferens
Loricula ruficeps	Nem védett / indifferens
Lota lota	Nem védett / indifferens
Lovenula alluaudi	Nem védett / indifferens
Loxia curvirostra	Védett
Loxia leucoptera	Védett
Loxia leucoptera bifasciata	Védett
Loxostege aeruginalis	Nem védett / indifferens
Loxostege deliblatica	Nem védett / indifferens
Loxostege manualis	Nem védett / indifferens
Loxostege sticticalis	Nem védett / indifferens
Loxostege turbidalis	Nem védett / indifferens
Celypha doubledayana	Nem védett / indifferens
Celypha lacunana	Nem védett / indifferens
Celypha rivulana	Nem védett / indifferens
Celypha siderana	Nem védett / indifferens
Lozekia transsylvanica	Védett
Lozotaenia forsterana	Nem védett / indifferens
Lucanus cervus	Védett
Lucilla singleyana	Nem védett / indifferens
Lullula arborea	Védett
Luperina pozzii	Nem védett / indifferens
Luperina testacea	Nem védett / indifferens
Luperina zollikoferi	Védett
Luquetia lobella	Nem védett / indifferens
Luscinia luscinia	Fokozottan védett
Luscinia megarhynchos	Védett
Luscinia svecica	Védett
Luscinia svecica cyanecula	Védett
Lutra lutra	Fokozottan védett
Luzea graeca	Nem védett / indifferens
Luzulaspis dactylis	Nem védett / indifferens
Luzulaspis frontalis	Nem védett / indifferens
Luzulaspis kosztarabi	Nem védett / indifferens
Luzulaspis luzulae	Nem védett / indifferens
Luzulaspis nemorosa	Nem védett / indifferens
Luzulaspis pieninnica	Nem védett / indifferens
Luzulaspis rajae	Nem védett / indifferens
Luzulaspis scotica	Nem védett / indifferens
Lycaena alciphron	Védett
Lycaena dispar	Védett
Lycaena dispar rutila	Védett
Lycaena helle	Nem védett / indifferens
Lycaena hippothoe	Védett
Lycaena phlaeas	Nem védett / indifferens
Lycaena thersamon	Védett
Lycaena tityrus	Nem védett / indifferens
Lycaena virgaureae	Nem védett / indifferens
Lycia hanoviensis	Nem védett / indifferens
Lycia hirtaria	Nem védett / indifferens
Lycia pomonaria	Nem védett / indifferens
Lycia zonaria	Nem védett / indifferens
Lycoperdina bovistae	Nem védett / indifferens
Lycoperdina succincta	Nem védett / indifferens
Lycophotia porphyrea	Védett
Lycosa singoriensis	Védett
Lyctocoris campestris	Nem védett / indifferens
Lyctocoris dimidiatus	Nem védett / indifferens
Lyctus linearis	Nem védett / indifferens
Lyctus pubescens	Nem védett / indifferens
Lydus europeus	Nem védett / indifferens
Lydus trimaculatus	Nem védett / indifferens
Lygaeosoma anatolicum	Nem védett / indifferens
Lygaeosoma sardeum	Nem védett / indifferens
Lygaeus equestris	Nem védett / indifferens
Lygaeus simulans	Nem védett / indifferens
Lygephila craccae	Nem védett / indifferens
Lygephila ludicra	Védett
Lygephila lusoria	Nem védett / indifferens
Lygephila pastinum	Nem védett / indifferens
Lygephila procax	Nem védett / indifferens
Lygephila viciae	Nem védett / indifferens
Lygistopterus sanguineus	Nem védett / indifferens
Lygocoris contaminatus	Nem védett / indifferens
Lygocoris pabulinus	Nem védett / indifferens
Neolygus viridis	Nem védett / indifferens
Lygus gemellatus	Nem védett / indifferens
Lygus pratensis	Nem védett / indifferens
Lygus punctatus	Nem védett / indifferens
Lygus rugulipennis	Nem védett / indifferens
Lymantor coryli	Nem védett / indifferens
Lymantria dispar	Nem védett / indifferens
Lymantria monacha	Nem védett / indifferens
Lymexylon navale	Nem védett / indifferens
Lymnaea stagnalis	Nem védett / indifferens
Lymnastis dieneri	Nem védett / indifferens
Lymnastis galilaeus	Nem védett / indifferens
Lymnocryptes minimus	Védett
Lynceus brachyurus	Nem védett / indifferens
Lynx lynx	Fokozottan védett
Lyonetia clerkella	Nem védett / indifferens
Lyonetia ledi	Nem védett / indifferens
Lyonetia prunifoliella	Nem védett / indifferens
Lype phaeopa	Nem védett / indifferens
Lype reducta	Nem védett / indifferens
Lyprocorrhe anceps	Nem védett / indifferens
Lypusa maurella	Nem védett / indifferens
Lythria cruentaria	Nem védett / indifferens
Lythria purpuraria	Nem védett / indifferens
Lytta vesicatoria	Nem védett / indifferens
Macaria alternata	Nem védett / indifferens
Macaria artesiaria	Nem védett / indifferens
Macaria liturata	Nem védett / indifferens
Macaria notata	Nem védett / indifferens
Macaria signaria	Nem védett / indifferens
Macaria wauaria	Nem védett / indifferens
Macaroeris nidicolens	Nem védett / indifferens
Maccevethus errans caucasicus	Nem védett / indifferens
Macdunnoughia confusa	Nem védett / indifferens
Macrargus rufus	Nem védett / indifferens
Macratria hungarica	Nem védett / indifferens
Macrochilo cribrumalis	Nem védett / indifferens
Macrocyclops albidus	Nem védett / indifferens
Macrocyclops distinctus	Nem védett / indifferens
Macrocyclops fuscus	Nem védett / indifferens
Macrogastra borealis	Nem védett / indifferens
Macrogastra borealis bielzi	Nem védett / indifferens
Macrogastra densestriata	Nem védett / indifferens
Macrogastra plicatula	Nem védett / indifferens
Macrogastra plicatula rusiostoma	Nem védett / indifferens
Macrogastra ventricosa	Nem védett / indifferens
Macroglossum stellatarum	Nem védett / indifferens
Macrolophus glaucescens	Nem védett / indifferens
Macrolophus melanotoma	Nem védett / indifferens
Macrolophus pygmaeus	Nem védett / indifferens
Macronemurus bilineatus	Nem védett / indifferens
Macronychus quadrituberculatus	Védett
Macrophagus robustus	Nem védett / indifferens
Macropis frivaldszkyi	Nem védett / indifferens
Macropis fulvipes	Nem védett / indifferens
Macropis labiata	Nem védett / indifferens
Macroplax fasciata	Nem védett / indifferens
Macroplax preyssleri	Nem védett / indifferens
Macropsidius dispar	Nem védett / indifferens
Macropsidius sahlbergi	Nem védett / indifferens
Macropsis cerea	Nem védett / indifferens
Macropsis eleagni	Nem védett / indifferens
Macropsis fuscinervis	Nem védett / indifferens
Macropsis fuscula	Nem védett / indifferens
Macropsis glandacea	Nem védett / indifferens
Macropsis graminea	Nem védett / indifferens
Macropsis haupti	Nem védett / indifferens
Macropsis impura	Nem védett / indifferens
Macropsis infuscata	Nem védett / indifferens
Macropsis marginata	Nem védett / indifferens
Macropsis megerlei	Nem védett / indifferens
Macropsis mendax	Nem védett / indifferens
Macropsis notata	Nem védett / indifferens
Macropsis prasina	Nem védett / indifferens
Macropsis scutellata	Nem védett / indifferens
Macropsis vestita	Nem védett / indifferens
Macropsis vicina	Nem védett / indifferens
Macropsis viridinervis	Nem védett / indifferens
Macrosaldula scotica	Nem védett / indifferens
Macrosaldula variabilis	Nem védett / indifferens
Macrosiagon tricuspidatum	Védett
Macrosteles cristatus	Nem védett / indifferens
Macrosteles fascifrons	Nem védett / indifferens
Macrosteles fieberi	Nem védett / indifferens
Macrosteles forficulus	Nem védett / indifferens
Macrosteles frontalis	Nem védett / indifferens
Macrosteles horvathi	Nem védett / indifferens
Macrosteles laevis	Nem védett / indifferens
Macrosteles maculosus	Nem védett / indifferens
Macrosteles oshanini	Nem védett / indifferens
Macrosteles quadripunctulatus	Nem védett / indifferens
Macrosteles sexnotatus	Nem védett / indifferens
Macrosteles sordidipennis	Nem védett / indifferens
Macrosteles variatus	Nem védett / indifferens
Macrosteles viridigriseus	Nem védett / indifferens
Macrothrix hirsuticornis	Nem védett / indifferens
Macrothrix laticornis	Nem védett / indifferens
Macrothrix rosea	Nem védett / indifferens
Macrothylacia rubi	Nem védett / indifferens
Macrotylus elevatus	Nem védett / indifferens
Macrotylus herrichi	Nem védett / indifferens
Macrotylus horvathi	Nem védett / indifferens
Macrotylus paykullii	Nem védett / indifferens
Macrotylus solitarius	Nem védett / indifferens
Glaucopsyche alcon	Védett
Glaucopsyche arion	Védett
Glaucopsyche arion ligurica	Védett
Glaucopsyche nausithous	Védett
Glaucopsyche teleius	Védett
Macustus grisescens	Nem védett / indifferens
Magdalis armigera	Nem védett / indifferens
Magdalis barbicornis	Nem védett / indifferens
Magdalis carbonaria	Nem védett / indifferens
Magdalis caucasica	Nem védett / indifferens
Magdalis cerasi	Nem védett / indifferens
Magdalis duplicata	Nem védett / indifferens
Magdalis exarata	Nem védett / indifferens
Magdalis flavicornis	Nem védett / indifferens
Magdalis frontalis	Nem védett / indifferens
Magdalis fuscicornis	Nem védett / indifferens
Magdalis linearis	Nem védett / indifferens
Magdalis memnonia	Nem védett / indifferens
Magdalis nitida	Nem védett / indifferens
Magdalis nitidipennis	Nem védett / indifferens
Magdalis opaca	Nem védett / indifferens
Magdalis phlegmatica	Nem védett / indifferens
Magdalis rufa	Nem védett / indifferens
Magdalis ruficornis	Nem védett / indifferens
Magdalis violacea	Nem védett / indifferens
Malachius aeneus	Nem védett / indifferens
Malachius bipustulatus	Nem védett / indifferens
Malachius rubidus	Nem védett / indifferens
Malachius scutellaris	Nem védett / indifferens
Malacocoris chlorizans	Nem védett / indifferens
Malacolimax tenellus	Nem védett / indifferens
Malacosoma castrense	Nem védett / indifferens
Malacosoma neustria	Nem védett / indifferens
Maladera holosericea	Nem védett / indifferens
Macrocerus demissus	Nem védett / indifferens
Macrocerus nigrinus	Nem védett / indifferens
Mallota cimbiciformis	Nem védett / indifferens
Mallota fuciformis	Nem védett / indifferens
Malthinus balteatus	Nem védett / indifferens
Malthinus biguttatus	Nem védett / indifferens
Malthinus facialis	Nem védett / indifferens
Malthinus fasciatus	Nem védett / indifferens
Malthinus flaveolus	Nem védett / indifferens
Malthinus frontalis	Nem védett / indifferens
Malthinus glabellus	Nem védett / indifferens
Malthinus seriepunctatus	Nem védett / indifferens
Malthinus turcicus	Nem védett / indifferens
Malthodes brevicollis	Nem védett / indifferens
Malthodes caudatus	Nem védett / indifferens
Malthodes crassicornis	Nem védett / indifferens
Malthodes debilis	Nem védett / indifferens
Malthodes dieneri	Nem védett / indifferens
Malthodes dimidiaticollis	Nem védett / indifferens
Malthodes dispar	Nem védett / indifferens
Malthodes fibulatus	Nem védett / indifferens
Malthodes flavoguttatus	Nem védett / indifferens
Malthodes fuscus	Nem védett / indifferens
Malthodes guttifer	Nem védett / indifferens
Malthodes hexacanthus	Nem védett / indifferens
Malthodes holdhausi	Nem védett / indifferens
Malthodes lobatus	Nem védett / indifferens
Malthodes marginatus	Nem védett / indifferens
Malthodes maurus	Nem védett / indifferens
Malthodes minimus	Nem védett / indifferens
Malthodes mysticus	Nem védett / indifferens
Malthodes pumilus	Nem védett / indifferens
Malthodes spathifer	Nem védett / indifferens
Malthodes spretus	Nem védett / indifferens
Malvaevora timida	Nem védett / indifferens
Malvapion malvae	Nem védett / indifferens
Mamestra brassicae	Nem védett / indifferens
Manda mandibularis	Nem védett / indifferens
Mangora acalypha	Nem védett / indifferens
Manica rubida	Nem védett / indifferens
Maniola jurtina	Nem védett / indifferens
Mansuphantes mansuetus	Nem védett / indifferens
Mantis religiosa	Védett
Mantispa aphavexelte	Védett
Mantispa perla	Védett
Mantispa styriaca	Védett
Marasmarcha lunaedactyla	Nem védett / indifferens
Margarinotus bipustulatus	Nem védett / indifferens
Margarinotus brunneus	Nem védett / indifferens
Margarinotus carbonarius	Nem védett / indifferens
Margarinotus ignobilis	Nem védett / indifferens
Margarinotus marginatus	Nem védett / indifferens
Margarinotus merdarius	Nem védett / indifferens
Margarinotus neglectus	Nem védett / indifferens
Margarinotus obscurus	Nem védett / indifferens
Margarinotus punctiventer	Nem védett / indifferens
Margarinotus purpurascens	Nem védett / indifferens
Margarinotus ruficornis	Nem védett / indifferens
Margarinotus striola	Nem védett / indifferens
Margarinotus striola succicola	Nem védett / indifferens
Margarinotus terricola	Nem védett / indifferens
Margarinotus ventralis	Nem védett / indifferens
Marmaronetta angustirostris	Fokozottan védett
Marmaropus besseri	Nem védett / indifferens
Maro minutus	Nem védett / indifferens
Marpissa muscosa	Nem védett / indifferens
Marpissa nivoyi	Nem védett / indifferens
Marpissa pomatia	Nem védett / indifferens
Marpissa radiata	Nem védett / indifferens
Martes foina	Nem védett / indifferens
Martes martes	Védett
Marthamea vitripennis	Védett
Megachile dorsalis	Nem védett / indifferens
Marumba quercus	Védett
Maso sundevalli	Nem védett / indifferens
Masoreus wetterhallii	Nem védett / indifferens
Mastigona bosniensis	Nem védett / indifferens
Mastigona vihorlatica	Nem védett / indifferens
Mastigusa arietina	Nem védett / indifferens
Mastigusa macrophthalma	Nem védett / indifferens
Mastus bielzi	Nem védett / indifferens
Matratinea rufulicaput	Nem védett / indifferens
Matsucoccus matsumurae	Nem védett / indifferens
Mecaspis alternans	Nem védett / indifferens
Mecaspis striatella	Nem védett / indifferens
Mecinus caucasicus	Nem védett / indifferens
Mecinus circulatus	Nem védett / indifferens
Mecinus collaris	Nem védett / indifferens
Mecinus heydeni	Nem védett / indifferens
Mecinus ictericus	Nem védett / indifferens
Mecinus janthinus	Nem védett / indifferens
Mecinus labilis	Nem védett / indifferens
Mecinus pascuorum	Nem védett / indifferens
Mecinus pirazzolii	Nem védett / indifferens
Mecinus plantaginis	Nem védett / indifferens
Mecinus pyraster	Nem védett / indifferens
Meconema meridionale	Nem védett / indifferens
Meconema thalassinum	Nem védett / indifferens
Mecopisthes silus	Nem védett / indifferens
Mecorhis ungarica	Nem védett / indifferens
Stethophyma grossum	Nem védett / indifferens
Mecyna flavalis	Nem védett / indifferens
Mecyna lutealis	Nem védett / indifferens
Mecyna trinalis	Nem védett / indifferens
Mecynotarsus serricornis	Nem védett / indifferens
Mediterranea depressa	Védett
Mediterranea hydatina	Nem védett / indifferens
Mediterranea inopinata	Nem védett / indifferens
Medon apicalis	Nem védett / indifferens
Medon brunneus	Nem védett / indifferens
Medon castaneus	Nem védett / indifferens
Medon dilutus	Nem védett / indifferens
Medon ferrugineus	Nem védett / indifferens
Medon fusculus	Nem védett / indifferens
Medon ripicola	Nem védett / indifferens
Medon rufiventris	Nem védett / indifferens
Megachile albisecta	Nem védett / indifferens
Megachile apicalis	Nem védett / indifferens
Megachile bicoloriventris	Nem védett / indifferens
Megachile bombycina	Nem védett / indifferens
Megachile centuncularis	Nem védett / indifferens
Megachile circumcincta	Nem védett / indifferens
Megachile deceptoria	Nem védett / indifferens
Megachile flabellipes	Nem védett / indifferens
Megachile genalis	Nem védett / indifferens
Megachile lagopoda	Nem védett / indifferens
Megachile leachella	Nem védett / indifferens
Megachile leucomalla	Nem védett / indifferens
Megachile lignisca	Nem védett / indifferens
Megachile maacki	Nem védett / indifferens
Megachile maritima	Nem védett / indifferens
Megachile melanopyga	Nem védett / indifferens
Megachile nigriventris	Nem védett / indifferens
Megachile octosignata	Nem védett / indifferens
Megachile pilicrus	Nem védett / indifferens
Megachile pilidens	Nem védett / indifferens
Megachile pyrenea	Nem védett / indifferens
Megachile rotundata	Nem védett / indifferens
Megachile versicolor	Nem védett / indifferens
Megachile willoughbiella	Nem védett / indifferens
Megacoelum beckeri	Nem védett / indifferens
Megacoelum infusum	Nem védett / indifferens
Megacraspedus binotella	Nem védett / indifferens
Megacraspedus dolosellus	Nem védett / indifferens
Megacraspedus imparellus	Nem védett / indifferens
Megacraspedus separatellus	Nem védett / indifferens
Megacyclops gigas	Nem védett / indifferens
Megacyclops latipes	Nem védett / indifferens
Megacyclops viridis	Nem védett / indifferens
Megadelphax sordidulus	Nem védett / indifferens
Megafenestra aurita	Nem védett / indifferens
Megalepthyphantes collinus	Nem védett / indifferens
Megalepthyphantes nebulosus	Nem védett / indifferens
Megalepthyphantes pseudocollinus	Nem védett / indifferens
Megalinus glabratus	Nem védett / indifferens
Megaloceroea recticornis	Nem védett / indifferens
Megalocoleus confusus	Nem védett / indifferens
Megalocoleus dissimilis	Nem védett / indifferens
Megalocoleus exsanguis	Nem védett / indifferens
Megalocoleus hungaricus	Nem védett / indifferens
Megalocoleus molliculus	Nem védett / indifferens
Megalocoleus tanaceti	Nem védett / indifferens
Megalomus hirtus	Nem védett / indifferens
Megalomus tortricoides	Nem védett / indifferens
Megalonotus antennatus	Nem védett / indifferens
Megalonotus chiragra	Nem védett / indifferens
Megalonotus dilatatus	Nem védett / indifferens
Megalonotus emarginatus	Nem védett / indifferens
Megalonotus hirsutus	Nem védett / indifferens
Megalonotus praetextatus	Nem védett / indifferens
Megalonotus sabulicola	Nem védett / indifferens
Megalophanes viciella	Nem védett / indifferens
Megalotomus junceus	Nem védett / indifferens
Megamelodes lequesnei	Nem védett / indifferens
Megamelodes quadrimaculatus	Nem védett / indifferens
Megamelus notulus	Nem védett / indifferens
Meganephria bimaculosa	Nem védett / indifferens
Meganola albula	Nem védett / indifferens
Meganola kolbi	Nem védett / indifferens
Meganola strigula	Nem védett / indifferens
Meganola togatulalis	Nem védett / indifferens
Megapenthes lugens	Nem védett / indifferens
Megaphyllum bosniense	Nem védett / indifferens
Megaphyllum bosniense cotinophilum	Nem védett / indifferens
Megaphyllum projectum	Nem védett / indifferens
Megaphyllum projectum dioritanum	Nem védett / indifferens
Megaphyllum projectum kochi	Nem védett / indifferens
Megaphyllum transsylvanicum	Nem védett / indifferens
Megaphyllum transsylvanicum transdanubicum	Nem védett / indifferens
Megaphyllum unilineatum	Nem védett / indifferens
Megarthrus bellevoyei	Nem védett / indifferens
Megarthrus denticollis	Nem védett / indifferens
Megarthrus depressus	Nem védett / indifferens
Megarthrus hemipterus	Nem védett / indifferens
Megarthrus proseni	Nem védett / indifferens
Megascolia maculata	Védett
Megascolia maculata flavifrons	Védett
Megasternum concinnum	Nem védett / indifferens
Megasyrphus erraticus	Nem védett / indifferens
Megatoma ruficornis	Nem védett / indifferens
Megatoma undata	Nem védett / indifferens
Megistopus flavicornis	Védett
Megophthalmus scabripennis	Nem védett / indifferens
Megophthalmus scanicus	Nem védett / indifferens
Meioneta affinis	Nem védett / indifferens
Meioneta fuscipalpa	Nem védett / indifferens
Meioneta mollis	Nem védett / indifferens
Meioneta rurestris	Nem védett / indifferens
Meioneta saxatilis	Nem védett / indifferens
Meioneta simplicitarsis	Nem védett / indifferens
Melaleucus picturatus	Nem védett / indifferens
Melampophylax nepos	Védett
Melanapion minimum	Nem védett / indifferens
Melanargia galathea	Nem védett / indifferens
Melanargia russiae	Nem védett / indifferens
Melanchra persicariae	Nem védett / indifferens
Melandrya barbata	Nem védett / indifferens
Melandrya caraboides	Nem védett / indifferens
Melandrya dubia	Nem védett / indifferens
Melangyna barbifrons	Nem védett / indifferens
Melangyna compositarum	Nem védett / indifferens
Melangyna labiatarum	Nem védett / indifferens
Melangyna lasiophthalma	Nem védett / indifferens
Melangyna quadrimaculata	Nem védett / indifferens
Melangyna umbellatarum	Nem védett / indifferens
Melanimon tibiale	Nem védett / indifferens
Melanitta fusca	Fokozottan védett
Melanitta nigra	Védett
Melanobaris atramentaria	Nem védett / indifferens
Melanobaris carbonaria	Nem védett / indifferens
Melanobaris dalmatina	Nem védett / indifferens
Melanobaris laticollis	Nem védett / indifferens
Melanocorypha calandra	Védett
Melanocoryphus albomaculatus	Nem védett / indifferens
Melanocoryphus tristrami	Nem védett / indifferens
Melanogaster aerosa	Nem védett / indifferens
Melanogaster hirtella	Nem védett / indifferens
Melanogaster nuda	Nem védett / indifferens
Melanogryllus desertus	Nem védett / indifferens
Melanoides tuberculatus	Nem védett / indifferens
Melanophila acuminata	Nem védett / indifferens
Melanophthalma curticollis	Nem védett / indifferens
Melanophthalma distinguenda	Nem védett / indifferens
Melanophthalma fuscipennis	Nem védett / indifferens
Melanophthalma maura	Nem védett / indifferens
Melanophthalma suturalis	Nem védett / indifferens
Melanophthalma taurica	Nem védett / indifferens
Melanopsis parreyssii	Nem védett / indifferens
Melanostoma dubium	Nem védett / indifferens
Nargus velox	Nem védett / indifferens
Melanostoma mellinum	Nem védett / indifferens
Melanostoma scalare	Nem védett / indifferens
Melanotus brunnipes	Nem védett / indifferens
Melanotus castanipes	Nem védett / indifferens
Melanotus crassicollis	Nem védett / indifferens
Melanotus punctolineatus	Nem védett / indifferens
Melanotus tenebrosus	Nem védett / indifferens
Melanotus villosus	Nem védett / indifferens
Melanthia procellata	Nem védett / indifferens
Melasis buprestoides	Nem védett / indifferens
Melecta albifrons	Nem védett / indifferens
Melecta funeraria	Nem védett / indifferens
Melecta luctuosa	Nem védett / indifferens
Meles meles	Nem védett / indifferens
Meliboeus amethystinus	Nem védett / indifferens
Meliboeus graminis	Nem védett / indifferens
Meliboeus subulatus	Nem védett / indifferens
Melicius cylindrus	Nem védett / indifferens
Meligethes acicularis	Nem védett / indifferens
Meligethes aeneus	Nem védett / indifferens
Meligethes assimilis	Nem védett / indifferens
Meligethes atramentarius	Nem védett / indifferens
Meligethes atratus	Nem védett / indifferens
Meligethes bidens	Nem védett / indifferens
Meligethes bidentatus	Nem védett / indifferens
Meligethes brachialis	Nem védett / indifferens
Meligethes brevis	Nem védett / indifferens
Meligethes brunnicornis	Nem védett / indifferens
Meligethes buyssoni	Nem védett / indifferens
Meligethes carinulatus	Nem védett / indifferens
Meligethes coeruleovirens	Nem védett / indifferens
Meligethes coracinus	Nem védett / indifferens
Meligethes corvinus	Nem védett / indifferens
Meligethes czwalinai	Nem védett / indifferens
Meligethes denticulatus	Nem védett / indifferens
Meligethes difficilis	Nem védett / indifferens
Meligethes discoideus	Nem védett / indifferens
Meligethes distinctus	Nem védett / indifferens
Meligethes egenus	Nem védett / indifferens
Meligethes erichsoni	Nem védett / indifferens
Meligethes exilis	Nem védett / indifferens
Meligethes flavimanus	Nem védett / indifferens
Meligethes gagathinus	Nem védett / indifferens
Meligethes haemorrhoidalis	Nem védett / indifferens
Meligethes hoffmanni	Nem védett / indifferens
Meligethes incanus	Nem védett / indifferens
Meligethes jejunus	Nem védett / indifferens
Meligethes jelineki	Nem védett / indifferens
Meligethes kraatzi	Nem védett / indifferens
Meligethes kunzei	Nem védett / indifferens
Meligethes lepidii	Nem védett / indifferens
Meligethes lugubris	Nem védett / indifferens
Meligethes matronalis	Nem védett / indifferens
Meligethes maurus	Nem védett / indifferens
Meligethes morosus	Nem védett / indifferens
Meligethes nanus	Nem védett / indifferens
Meligethes nigrescens	Nem védett / indifferens
Meligethes ochropus	Nem védett / indifferens
Meligethes ovatus	Nem védett / indifferens
Meligethes pedicularius	Nem védett / indifferens
Meligethes persicus	Nem védett / indifferens
Meligethes planiusculus	Nem védett / indifferens
Meligethes rosenhaueri	Nem védett / indifferens
Meligethes rotundicollis	Nem védett / indifferens
Meligethes ruficornis	Nem védett / indifferens
Meligethes serripes	Nem védett / indifferens
Meligethes solidus	Nem védett / indifferens
Meligethes subaeneus	Nem védett / indifferens
Meligethes submetallicus	Nem védett / indifferens
Meligethes subrugosus	Nem védett / indifferens
Meligethes sulcatus	Nem védett / indifferens
Meligethes symphyti	Nem védett / indifferens
Meligethes tristis	Nem védett / indifferens
Meligethes umbrosus	Nem védett / indifferens
Meligethes variolosus	Nem védett / indifferens
Meligethes villosus	Nem védett / indifferens
Meligethes viridescens	Nem védett / indifferens
Meligramma cincta	Nem védett / indifferens
Meligramma guttata	Nem védett / indifferens
Meligramma triangulifera	Nem védett / indifferens
Melinopterus consputus	Nem védett / indifferens
Melinopterus prodromus	Nem védett / indifferens
Melinopterus pubescens	Nem védett / indifferens
Melinopterus punctatosulcatus	Nem védett / indifferens
Melinopterus reyi	Nem védett / indifferens
Melinopterus sphacelatus	Nem védett / indifferens
Meliscaeva auricollis	Nem védett / indifferens
Meliscaeva cinctella	Nem védett / indifferens
Aphomia foedella	Nem védett / indifferens
Aphomia zelleri	Nem védett / indifferens
Melitaea athalia	Nem védett / indifferens
Melitaea aurelia	Védett
Melitaea britomartis	Védett
Melitaea cinxia	Nem védett / indifferens
Melitaea diamina	Védett
Melitaea didyma	Nem védett / indifferens
Melitaea phoebe	Nem védett / indifferens
Melitaea punica	Nem védett / indifferens
Melitaea punica telona	Védett
Melitaea trivia	Védett
Melitta budensis	Nem védett / indifferens
Melitta dimidiata	Nem védett / indifferens
Melitta haemorrhoidalis	Nem védett / indifferens
Melitta leporina	Nem védett / indifferens
Melitta moczari	Nem védett / indifferens
Melitta nigricans	Nem védett / indifferens
Melitta tricincta	Nem védett / indifferens
Melitturga clavicornis	Nem védett / indifferens
Mellinus arvensis	Nem védett / indifferens
Mellinus crabroneus	Nem védett / indifferens
Meloe autumnalis	Védett
Meloe brevicollis	Védett
Meloe cicatricosus	Védett
Meloe decorus	Védett
Meloe hungarus	Védett
Meloe mediterraneus	Védett
Meloe proscarabaeus	Nem védett / indifferens
Meloe rufiventris	Védett
Meloe rugosus	Védett
Meloe scabriusculus	Védett
Meloe tuccius	Védett
Meloe uralensis	Védett
Meloe variegatus	Védett
Meloe violaceus	Nem védett / indifferens
Melogona broelemanni	Nem védett / indifferens
Melogona broelemanni gebhardti	Nem védett / indifferens
Melogona transsilvanica	Nem védett / indifferens
Melogona transsilvanica hungarica	Nem védett / indifferens
Melolontha hippocastani	Nem védett / indifferens
Melolontha melolontha	Nem védett / indifferens
Melolontha pectoralis	Nem védett / indifferens
Menaccarus arenicola	Nem védett / indifferens
Perittia farinella	Nem védett / indifferens
Perittia huemeri	Nem védett / indifferens
Mendoza canestrinii	Nem védett / indifferens
Mendrausus pauxillus	Nem védett / indifferens
Menephilus cylindricus	Nem védett / indifferens
Menesia bipunctata	Nem védett / indifferens
Meotica exilis	Nem védett / indifferens
Meotica filiformis	Nem védett / indifferens
Meotica pallens	Nem védett / indifferens
Meotica parasita	Nem védett / indifferens
Meotica szeli	Nem védett / indifferens
Merdigera obscura	Nem védett / indifferens
Mergellus albellus	Védett
Mergus merganser	Védett
Mergus serrator	Védett
Meria tripunctata	Nem védett / indifferens
Meridiophila fascialis	Nem védett / indifferens
Mermitelocerus schmidtii	Nem védett / indifferens
Merodon aberrans	Nem védett / indifferens
Merodon aeneus	Nem védett / indifferens
Merodon albifrons	Nem védett / indifferens
Merodon armipes	Nem védett / indifferens
Merodon auripes	Nem védett / indifferens
Merodon avidus	Nem védett / indifferens
Merodon cinereus	Nem védett / indifferens
Merodon clavipes	Nem védett / indifferens
Merodon clunipes	Nem védett / indifferens
Merodon constans	Nem védett / indifferens
Merodon equestris	Nem védett / indifferens
Merodon nigritarsis	Nem védett / indifferens
Merodon ruficornis	Nem védett / indifferens
Merodon rufus	Nem védett / indifferens
Merodon tricinctus	Nem védett / indifferens
Merops apiaster	Fokozottan védett
Merrifieldia baliodactylus	Nem védett / indifferens
Merrifieldia leucodactyla	Nem védett / indifferens
Merrifieldia malacodactylus	Nem védett / indifferens
Merrifieldia tridactyla	Nem védett / indifferens
Mesagroicus obscurus	Nem védett / indifferens
Mesapamea didyma	Nem védett / indifferens
Mesapamea secalis	Nem védett / indifferens
Mesembrius peregrinus	Nem védett / indifferens
Mesiotelus annulipes	Nem védett / indifferens
Mesocoelopus niger	Nem védett / indifferens
Mesocrambus candiellus	Nem védett / indifferens
Mesocyclops leuckarti	Nem védett / indifferens
Mesogona acetosellae	Nem védett / indifferens
Mesogona oxalina	Nem védett / indifferens
Mesoiulus paradoxus	Nem védett / indifferens
Mesoleuca albicillata	Nem védett / indifferens
Mesoligia furuncula	Nem védett / indifferens
Mesoligia literosa	Nem védett / indifferens
Mesophleps silacella	Nem védett / indifferens
Mesosa curculionoides	Nem védett / indifferens
Mesosa nebulosa	Nem védett / indifferens
Mesothes ferrugineus	Nem védett / indifferens
Loborhynchapion amethystinum	Nem védett / indifferens
Mesotrichapion punctirostre	Nem védett / indifferens
Mesotrosta signalis	Védett
Mesotype didymata	Védett
Mesotype parallelolineata	Nem védett / indifferens
Mesovelia furcata	Nem védett / indifferens
Mesovelia thermalis	Nem védett / indifferens
Messor structor	Nem védett / indifferens
Meta menardi	Nem védett / indifferens
Metacantharis discoidea	Nem védett / indifferens
Metacantharis clypeata	Nem védett / indifferens
Metacanthus meridionalis	Nem védett / indifferens
Metachrostis dardouini	Védett
Metaclisa azurea	Nem védett / indifferens
Metacrambus carectellus	Nem védett / indifferens
Metacyclops gracilis	Nem védett / indifferens
Metacyclops minutus	Nem védett / indifferens
Metacyclops planus	Nem védett / indifferens
Metacypris cordata	Nem védett / indifferens
Metadenopus festucae	Nem védett / indifferens
Metadonus distinguendus	Nem védett / indifferens
Metalampra cinnamomea	Nem védett / indifferens
Metalampra diminutella	Nem védett / indifferens
Metalimnus formosus	Nem védett / indifferens
Metalimnus steini	Nem védett / indifferens
Metanomus infuscatus	Nem védett / indifferens
Metapterus caspicus	Nem védett / indifferens
Metasia ophialis	Nem védett / indifferens
Metatropis rufescens	Nem védett / indifferens
Metaxmeste phrygialis	Nem védett / indifferens
Metcalfa pruinosa	Nem védett / indifferens
Metellina mengei	Nem védett / indifferens
Metellina merianae	Nem védett / indifferens
Metellina segmentata	Nem védett / indifferens
Metendothenia atropunctana	Nem védett / indifferens
Methocha ichneumonides	Nem védett / indifferens
Methorasa latreillei	Védett
Metoecus paradoxus	Nem védett / indifferens
Metopobactrus ascitus	Nem védett / indifferens
Metopobactrus deserticola	Nem védett / indifferens
Metopobactrus prominulus	Nem védett / indifferens
Metopoplax origani	Nem védett / indifferens
Metopsia similis	Nem védett / indifferens
Metreletus balcanicus	Nem védett / indifferens
Metrioptera bicolor	Nem védett / indifferens
Metrioptera brachyptera	Nem védett / indifferens
Metrioptera roeselii	Nem védett / indifferens
Metriotes lutarea	Nem védett / indifferens
Metropis inermis	Nem védett / indifferens
Metropis latifrons	Nem védett / indifferens
Metropis mayri	Nem védett / indifferens
Metzneria aestivella	Nem védett / indifferens
Metzneria aprilella	Nem védett / indifferens
Metzneria artificella	Nem védett / indifferens
Metzneria ehikeella	Nem védett / indifferens
Metzneria intestinella	Nem védett / indifferens
Metzneria lappella	Nem védett / indifferens
Metzneria metzneriella	Nem védett / indifferens
Metzneria neuropterella	Nem védett / indifferens
Metzneria paucipunctella	Nem védett / indifferens
Metzneria santolinella	Nem védett / indifferens
Metzneria subflavella	Nem védett / indifferens
Metzneria tristella	Nem védett / indifferens
Mezira tremulae	Nem védett / indifferens
Mezium americanum	Nem védett / indifferens
Miarus ajugae	Nem védett / indifferens
Miarus banaticus	Nem védett / indifferens
Miarus monticola	Nem védett / indifferens
Miarus ursinus	Nem védett / indifferens
Micantulina stigmatipennis	Nem védett / indifferens
Micaria albovittata	Nem védett / indifferens
Micaria dives	Nem védett / indifferens
Micaria formicaria	Nem védett / indifferens
Micaria fulgens	Nem védett / indifferens
Micaria guttulata	Nem védett / indifferens
Micaria nivosa	Nem védett / indifferens
Micaria pulicaria	Nem védett / indifferens
Micaria rossica	Nem védett / indifferens
Micaria silesiaca	Nem védett / indifferens
Micaria sociabilis	Nem védett / indifferens
Micaria subopaca	Nem védett / indifferens
Micilus murinus	Nem védett / indifferens
Micrambe abietis	Nem védett / indifferens
Micrambe bimaculata	Nem védett / indifferens
Micrambe lindbergorum	Nem védett / indifferens
Micrargus herbigradus	Nem védett / indifferens
Micrargus laudatus	Nem védett / indifferens
Micrasema setiferum	Nem védett / indifferens
Micrelus ericae	Nem védett / indifferens
Micridium vittatum	Nem védett / indifferens
Microcara testacea	Nem védett / indifferens
Microcyclops rubellus	Nem védett / indifferens
Microcyclops varicans	Nem védett / indifferens
Microdon devius	Nem védett / indifferens
Microdon eggeri	Nem védett / indifferens
Microdon mutabilis	Nem védett / indifferens
Microdota aegra	Nem védett / indifferens
Microdota amicula	Nem védett / indifferens
Microdota benickiella	Nem védett / indifferens
Microdota contractipennis	Nem védett / indifferens
Microdota excisa	Nem védett / indifferens
Microdota ganglbaueri	Nem védett / indifferens
Microdota indubia	Nem védett / indifferens
Microdota inquinula	Nem védett / indifferens
Microdota liliputana	Nem védett / indifferens
Microdota palleola	Nem védett / indifferens
Microdota pittionii	Nem védett / indifferens
Microdota subtilis	Nem védett / indifferens
Microdynerus exilis	Nem védett / indifferens
Microdynerus longicollis	Nem védett / indifferens
Microdynerus nugdunensis	Nem védett / indifferens
Microdynerus timidus	Nem védett / indifferens
Microhoria nectarina	Nem védett / indifferens
Microhoria unicolor	Nem védett / indifferens
Microlestes corticalis	Nem védett / indifferens
Microlestes corticalis escorialensis	Nem védett / indifferens
Microlestes fissuralis	Nem védett / indifferens
Microlestes fulvibasis	Nem védett / indifferens
Microlestes maurus	Nem védett / indifferens
Microlestes minutulus	Nem védett / indifferens
Microlestes negrita	Nem védett / indifferens
Microlestes plagiatus	Nem védett / indifferens
Microlestes schroederi	Nem védett / indifferens
Microlinyphia impigra	Nem védett / indifferens
Microlinyphia pusilla	Nem védett / indifferens
Tettigometra diminuta	Nem védett / indifferens
Micrommata virescens	Nem védett / indifferens
Micromus angulatus	Nem védett / indifferens
Micromus lanosus	Nem védett / indifferens
Micromus paganus	Nem védett / indifferens
Micromus variegatus	Nem védett / indifferens
Micromys minutus	Védett
Micronecta griseola	Nem védett / indifferens
Micronecta minutissima	Nem védett / indifferens
Micronecta poweri	Nem védett / indifferens
Micronecta pusilla	Nem védett / indifferens
Micronecta scholtzi	Nem védett / indifferens
Microneta viaria	Nem védett / indifferens
Microon sahlbergi	Nem védett / indifferens
Micropeplus caelatus	Nem védett / indifferens
Micropeplus fulvus	Nem védett / indifferens
Micropeplus longipennis	Nem védett / indifferens
Micropeplus marietti	Nem védett / indifferens
Micropeplus porcatus	Nem védett / indifferens
Micropeplus tesserula	Nem védett / indifferens
Microplax interrupta	Nem védett / indifferens
Microplontus campestris	Nem védett / indifferens
Microplontus edentulus	Nem védett / indifferens
Microplontus millefolii	Nem védett / indifferens
Microplontus molitor	Nem védett / indifferens
Microplontus rugulosus	Nem védett / indifferens
Microplontus triangulum	Nem védett / indifferens
Microporus nigrita	Nem védett / indifferens
Micropterix aruncella	Nem védett / indifferens
Micropterix aureatella	Nem védett / indifferens
Micropterix calthella	Nem védett / indifferens
Micropterix mansuetella	Nem védett / indifferens
Micropterix myrtetella	Nem védett / indifferens
Micropterix schaefferi	Nem védett / indifferens
Micropterix tunbergella	Nem védett / indifferens
Micropterna caesareica	Nem védett / indifferens
Micropterna lateralis	Nem védett / indifferens
Micropterna nycterobia	Nem védett / indifferens
Micropterna sequax	Nem védett / indifferens
Micropterna testacea	Nem védett / indifferens
Micropterus salmoides	Nem védett / indifferens
Microptilium palustre	Nem védett / indifferens
Microptilium pulchellum	Nem védett / indifferens
Microrhagus emyi	Nem védett / indifferens
Microrhagus lepidus	Nem védett / indifferens
Microrhagus palmi	Nem védett / indifferens
Microrhagus pygmaeus	Nem védett / indifferens
Microscydmus nanus	Nem védett / indifferens
Microtus agrestis	Védett
Microtus arvalis	Nem védett / indifferens
Microtus oeconomus	Fokozottan védett
Microtus oeconomus mehelyi	Fokozottan védett
Microtus subterraneus	Nem védett / indifferens
Microvelia buenoi	Nem védett / indifferens
Microvelia pygmaea	Nem védett / indifferens
Microvelia reticulata	Nem védett / indifferens
Micrurapteryx kollariella	Nem védett / indifferens
Migneauxia crassiuscula	Nem védett / indifferens
Milesia crabroniformis	Nem védett / indifferens
Milesia semiluctifera	Nem védett / indifferens
Emberiza calandra	Védett
Millieria dolosalis	Nem védett / indifferens
Miltochrista miniata	Nem védett / indifferens
Holochelus aequinoctialis	Nem védett / indifferens
Holochelus nocturnus	Nem védett / indifferens
Holochelus pilicollis	Nem védett / indifferens
Holochelus vernus	Nem védett / indifferens
Milvus migrans	Fokozottan védett
Milvus milvus	Fokozottan védett
Mimas tiliae	Nem védett / indifferens
Mimela aurata	Nem védett / indifferens
Mimesa bicolor	Nem védett / indifferens
Mimesa brevis	Nem védett / indifferens
Mimesa bruxellensis	Nem védett / indifferens
Mimesa caucasica	Nem védett / indifferens
Mimesa crassipes	Nem védett / indifferens
Mimesa equestris	Nem védett / indifferens
Mimesa lutaria	Nem védett / indifferens
Mimetus laevigatus	Nem védett / indifferens
Mimumesa atratina	Nem védett / indifferens
Mimumesa beaumonti	Nem védett / indifferens
Mimumesa dahlbomi	Nem védett / indifferens
Mimumesa littoralis	Nem védett / indifferens
Mimumesa unicolor	Nem védett / indifferens
Mimumesa wuestneii	Nem védett / indifferens
Minetia adamczewskii	Nem védett / indifferens
Minetia criella	Nem védett / indifferens
Minetia crinitus	Nem védett / indifferens
Minetia labiosella	Nem védett / indifferens
Minicia marginella	Nem védett / indifferens
Miniopterus schreibersii	Fokozottan védett
Minoa murinata	Nem védett / indifferens
Minois dryas	Nem védett / indifferens
Minucia lunaris	Nem védett / indifferens
Minyops carinatus	Nem védett / indifferens
Minyriolus pusillus	Nem védett / indifferens
Mirabella albifrons	Nem védett / indifferens
Miramella alpina	Nem védett / indifferens
Mirificarma cytisella	Nem védett / indifferens
Mirificarma eburnella	Nem védett / indifferens
Mirificarma lentiginosella	Nem védett / indifferens
Mirificarma maculatella	Nem védett / indifferens
Mirificarma mulinella	Nem védett / indifferens
Miris striatus	Nem védett / indifferens
Mirococcopsis elongatus	Nem védett / indifferens
Mirococcopsis nagyi	Nem védett / indifferens
Mirococcopsis stipae	Nem védett / indifferens
Mirococcus inermis	Nem védett / indifferens
Miscophus ater	Nem védett / indifferens
Miscophus bicolor	Nem védett / indifferens
Miscophus concolor	Nem védett / indifferens
Miscophus helveticus	Nem védett / indifferens
Miscophus helveticus rubriventris	Nem védett / indifferens
Miscophus spurius	Nem védett / indifferens
Misumena vatia	Nem védett / indifferens
Ebrechtella tricuspidata	Nem védett / indifferens
Tettigometra macrocephalus	Nem védett / indifferens
Mixodiaptomus kupelwieseri	Nem védett / indifferens
Mixodiaptomus lilljeborgi	Nem védett / indifferens
Mixodiaptomus tatricus	Nem védett / indifferens
Mixtacandona transleithanica	Nem védett / indifferens
Mniotype adusta	Nem védett / indifferens
Mniotype satura	Nem védett / indifferens
Mocuellus collinus	Nem védett / indifferens
Mocuellus metrius	Nem védett / indifferens
Mocuellus quadricornis	Nem védett / indifferens
Mocydia crocea	Nem védett / indifferens
Mocydiopsis attenuata	Nem védett / indifferens
Mocydiopsis intermedia	Nem védett / indifferens
Mocydiopsis longicauda	Nem védett / indifferens
Mocydiopsis monticola	Nem védett / indifferens
Mocydiopsis parvicauda	Nem védett / indifferens
Mocyta clientula	Nem védett / indifferens
Mocyta fungi	Nem védett / indifferens
Mocyta fussi	Nem védett / indifferens
Mocyta negligens	Nem védett / indifferens
Mocyta orbata	Nem védett / indifferens
Mocyta orphana	Nem védett / indifferens
Modicogryllus frontalis	Nem védett / indifferens
Moebelia penicillata	Nem védett / indifferens
Mogulones abbreviatulus	Nem védett / indifferens
Mogulones albosignatus	Nem védett / indifferens
Mogulones amplipennis	Nem védett / indifferens
Mogulones andreae	Nem védett / indifferens
Mogulones angulicollis	Nem védett / indifferens
Mogulones aratridens	Nem védett / indifferens
Mogulones asperifoliarum	Nem védett / indifferens
Mogulones aubei	Nem védett / indifferens
Mogulones austriacus	Nem védett / indifferens
Mogulones beckeri	Nem védett / indifferens
Mogulones borraginis	Nem védett / indifferens
Mogulones crucifer	Nem védett / indifferens
Mogulones cynoglossi	Nem védett / indifferens
Mogulones diecki	Nem védett / indifferens
Mogulones dimidiatus	Nem védett / indifferens
Mogulones euphorbiae	Nem védett / indifferens
Mogulones geographicus	Nem védett / indifferens
Mogulones gibbicollis	Nem védett / indifferens
Mogulones hungaricus	Nem védett / indifferens
Mogulones javetii	Nem védett / indifferens
Mogulones larvatus	Nem védett / indifferens
Mogulones lineatus	Nem védett / indifferens
Mogulones pallidicornis	Nem védett / indifferens
Mogulones pannonicus	Nem védett / indifferens
Mogulones raphani	Nem védett / indifferens
Mogulones sublineellus	Nem védett / indifferens
Mogulones venedicus	Nem védett / indifferens
Mogulonoides radula	Nem védett / indifferens
Mohelnaspis massiliensis	Nem védett / indifferens
Moina brachiata	Nem védett / indifferens
Moina macrocopa	Nem védett / indifferens
Moina micrura	Nem védett / indifferens
Moina salina	Nem védett / indifferens
Moina weismanni	Nem védett / indifferens
Molops elatus	Nem védett / indifferens
Molops piceus	Nem védett / indifferens
Molops piceus austriacus	Nem védett / indifferens
Molorchus minor	Nem védett / indifferens
Moma alpium	Nem védett / indifferens
Mompha bradleyi	Nem védett / indifferens
Mompha divisella	Nem védett / indifferens
Mompha epilobiella	Nem védett / indifferens
Mompha idaei	Nem védett / indifferens
Mompha lacteella	Nem védett / indifferens
Mompha langiella	Nem védett / indifferens
Mompha locupletella	Nem védett / indifferens
Mompha miscella	Nem védett / indifferens
Mompha ochraceella	Nem védett / indifferens
Mompha propinquella	Nem védett / indifferens
Mompha raschkiella	Nem védett / indifferens
Mompha sturnipennella	Nem védett / indifferens
Mompha subbistrigella	Nem védett / indifferens
Mompha terminella	Nem védett / indifferens
Monacha cartusiana	Nem védett / indifferens
Monachoides incarnatus	Nem védett / indifferens
Monachoides vicinus	Védett
Monalocoris filicis	Nem védett / indifferens
Monanus signatus	Nem védett / indifferens
Monochamus galloprovincialis	Nem védett / indifferens
Monochamus galloprovincialis pistor	Nem védett / indifferens
Monochamus rosenmuelleri	Nem védett / indifferens
Monochamus saltuarius	Nem védett / indifferens
Monochamus sartor	Nem védett / indifferens
Monochamus sutor	Nem védett / indifferens
Monochroa arundinetella	Nem védett / indifferens
Monochroa conspersella	Nem védett / indifferens
Monochroa cytisella	Nem védett / indifferens
Monochroa divisella	Nem védett / indifferens
Monochroa elongella	Nem védett / indifferens
Monochroa hornigi	Nem védett / indifferens
Monochroa lucidella	Nem védett / indifferens
Monochroa lutulentella	Nem védett / indifferens
Monochroa niphognatha	Nem védett / indifferens
Monochroa nomadella	Nem védett / indifferens
Monochroa palustrellus	Nem védett / indifferens
Monochroa parvulata	Nem védett / indifferens
Monochroa rumicetella	Nem védett / indifferens
Monochroa sepicolella	Nem védett / indifferens
Monochroa servella	Nem védett / indifferens
Monochroa simplicella	Nem védett / indifferens
Monochroa tenebrella	Nem védett / indifferens
Monomorium pharaonis	Nem védett / indifferens
Mononychus punctumalbum	Nem védett / indifferens
Monopis crocicapitella	Nem védett / indifferens
Monopis fenestratella	Nem védett / indifferens
Monopis imella	Nem védett / indifferens
Monopis laevigella	Nem védett / indifferens
Monopis monachella	Nem védett / indifferens
Monopis obviella	Nem védett / indifferens
Monopis weaverella	Nem védett / indifferens
Monospilus dispar	Nem védett / indifferens
Monosteira unicostata	Nem védett / indifferens
Monosynamma bohemanni	Nem védett / indifferens
Monotoma angusticollis	Nem védett / indifferens
Monotoma bicolor	Nem védett / indifferens
Monotoma brevicollis	Nem védett / indifferens
Monotoma conicicollis	Nem védett / indifferens
Monotoma longicollis	Nem védett / indifferens
Monotoma picipes	Nem védett / indifferens
Monotoma quadrifoveolata	Nem védett / indifferens
Monotoma spinicollis	Nem védett / indifferens
Monotoma testacea	Nem védett / indifferens
Montescardia tessulatellus	Nem védett / indifferens
Monticola saxatilis	Fokozottan védett
Montifringilla nivalis	Védett
Mordella aculeata	Nem védett / indifferens
Mordella adnexa	Nem védett / indifferens
Mordella brachyura	Nem védett / indifferens
Mordella holomelaena	Nem védett / indifferens
Mordella huetheri	Nem védett / indifferens
Mordella leucaspis	Nem védett / indifferens
Mordella meridionalis	Nem védett / indifferens
Mordella purpurascens	Nem védett / indifferens
Mordella velutina panonica	Nem védett / indifferens
Mordella viridescens	Nem védett / indifferens
Mordellaria aurofasciata	Nem védett / indifferens
Mordellistena acuticollis	Nem védett / indifferens
Mordellistena aequalica	Nem védett / indifferens
Mordellistena angustula	Nem védett / indifferens
Mordellistena apicerufa	Nem védett / indifferens
Mordellistena bicoloripilosa	Nem védett / indifferens
Mordellistena biroi	Nem védett / indifferens
Mordellistena brevicauda	Nem védett / indifferens
Mordellistena brunneispinosa	Nem védett / indifferens
Mordellistena confinis	Nem védett / indifferens
Mordellistena connata	Nem védett / indifferens
Mordellistena consobrina	Nem védett / indifferens
Mordellistena curticornis	Nem védett / indifferens
Mordellistena csiki	Nem védett / indifferens
Mordellistena dalmatica	Nem védett / indifferens
Mordellistena dieckmanni	Nem védett / indifferens
Mordellistena dives	Nem védett / indifferens
Mordellistena dvoraki	Nem védett / indifferens
Mordellistena erdoesi	Nem védett / indifferens
Mordellistena exclusa	Nem védett / indifferens
Mordellistena falsoparvula	Nem védett / indifferens
Mordellistena falsoparvuliformis	Nem védett / indifferens
Mordellistena feigei	Nem védett / indifferens
Mordellistena festiva	Nem védett / indifferens
Mordellistena flavospinulosa	Nem védett / indifferens
Mordellistena fuscogemellatoides	Nem védett / indifferens
Mordellistena horvathi	Nem védett / indifferens
Mordellistena humeralis	Nem védett / indifferens
Mordellistena hungarica	Nem védett / indifferens
Mordellistena intersecta	Nem védett / indifferens
Mordellistena klapperichi	Nem védett / indifferens
Mordellistena koelleri	Nem védett / indifferens
Mordellistena kraatzi	Nem védett / indifferens
Mordellistena lichtneckerti	Nem védett / indifferens
Mordellistena magyarica	Nem védett / indifferens
Mordellistena mediana	Nem védett / indifferens
Mordellistena mediogemellata	Nem védett / indifferens
Mordellistena meuseli	Nem védett / indifferens
Mordellistena micans	Nem védett / indifferens
Mordellistena micantoides	Nem védett / indifferens
Mordellistena microscopica	Nem védett / indifferens
Mordellistena mihoki	Nem védett / indifferens
Mordellistena minutula	Nem védett / indifferens
Mordellistena minutuloides	Nem védett / indifferens
Mordellistena neglecta	Nem védett / indifferens
Mordellistena neuwaldeggiana	Nem védett / indifferens
Mordellistena paraintersecta	Nem védett / indifferens
Mordellistena paranana	Nem védett / indifferens
Mordellistena pararhenana	Nem védett / indifferens
Mordellistena parvula	Nem védett / indifferens
Mordellistena parvuliformis	Nem védett / indifferens
Mordellistena parvuloides	Nem védett / indifferens
Mordellistena pentas	Nem védett / indifferens
Mordellistena perparvula	Nem védett / indifferens
Mordellistena pfefferi	Nem védett / indifferens
Mordellistena pontica	Nem védett / indifferens
Mordellistena pseudobrevicauda	Nem védett / indifferens
Mordellistena pseudonana	Nem védett / indifferens
Mordellistena pseudoparvula	Nem védett / indifferens
Mordellistena pseudorhenana	Nem védett / indifferens
Mordellistena pumila	Nem védett / indifferens
Mordellistena purpureonigrans	Nem védett / indifferens
Mordellistena pygmaeola	Nem védett / indifferens
Mordellistena reichei	Nem védett / indifferens
Mordellistena reitteri	Nem védett / indifferens
Mordellistena rhenana	Nem védett / indifferens
Mordellistena sajoi	Nem védett / indifferens
Mordellistena secreta	Nem védett / indifferens
Mordellistena semiferruginea	Nem védett / indifferens
Mordellistena stenidea	Nem védett / indifferens
Mordellistena stoeckleini	Nem védett / indifferens
Mordellistena subepisternalis	Nem védett / indifferens
Mordellistena tarsata	Nem védett / indifferens
Mordellistena thuringiaca	Nem védett / indifferens
Mordellistena variegata	Nem védett / indifferens
Mordellistena wankai	Nem védett / indifferens
Mordellistena weisei	Nem védett / indifferens
Mordellistena zoltani	Nem védett / indifferens
Mordellistenula perrisi	Nem védett / indifferens
Mordellistenula planifrons	Nem védett / indifferens
Mordellochroa abdiminalis	Nem védett / indifferens
Mordellochroa humerosa	Nem védett / indifferens
Mordellochroa milleri	Nem védett / indifferens
Mordellochroa tournieri	Nem védett / indifferens
Morimus asper funereus	Védett
Morlina glabra	Nem védett / indifferens
Morlina glabra striaria	Nem védett / indifferens
Mormo maura	Védett
Morophaga choragella	Nem védett / indifferens
Morychus aeneus	Nem védett / indifferens
Motacilla alba	Védett
Motacilla cinerea	Védett
Motacilla citreola	Védett
Motacilla citreola werae	Védett
Motacilla flava	Védett
Motacilla flava feldegg	Védett
Motacilla flava thunbergi	Védett
Muellerianella brevipennis	Nem védett / indifferens
Muellerianella extrusa	Nem védett / indifferens
Muellerianella fairmairei	Nem védett / indifferens
Muirodelphax aubei	Nem védett / indifferens
Mughiphantes mughi	Nem védett / indifferens
Mus musculus	Nem védett / indifferens
Mus spicilegus	Nem védett / indifferens
Musaria affinis	Nem védett / indifferens
Musaria argus	Védett
Muscardinus avellanarius	Védett
Muscicapa striata	Védett
Musculium lacustre	Nem védett / indifferens
Mustela erminea	Védett
Mustela eversmanii	Nem védett / indifferens
Mustela eversmanii hungarica	Védett
Mustela nivalis vulgaris	Védett
Mustela putorius	Nem védett / indifferens
Mutilla brutia	Nem védett / indifferens
Mutilla differens	Nem védett / indifferens
Mutilla europaea	Nem védett / indifferens
Algedonia terrealis	Nem védett / indifferens
Myathropa florea	Nem védett / indifferens
Mycetaea subterranea	Nem védett / indifferens
Mycetina cruciata	Nem védett / indifferens
Mycetochara axillaris	Nem védett / indifferens
Mycetochara flavipes	Nem védett / indifferens
Mycetochara humeralis	Nem védett / indifferens
Mycetochara linearis	Nem védett / indifferens
Mycetochara pygmaea	Nem védett / indifferens
Mycetochara quadrimaculata	Nem védett / indifferens
Mycetochara roubali	Nem védett / indifferens
Mycetoma suturale	Nem védett / indifferens
Mycetophagus ater	Nem védett / indifferens
Mycetophagus atomarius	Nem védett / indifferens
Mycetophagus decempunctatus	Nem védett / indifferens
Mycetophagus fulvicollis	Nem védett / indifferens
Mycetophagus multipunctatus	Nem védett / indifferens
Mycetophagus piceus	Nem védett / indifferens
Mycetophagus populi	Nem védett / indifferens
Mycetophagus quadriguttatus	Nem védett / indifferens
Mycetophagus quadripustulatus	Nem védett / indifferens
Mycetophagus salicis	Nem védett / indifferens
Mycetoporus aequalis	Nem védett / indifferens
Mycetoporus ambiguus	Nem védett / indifferens
Mycetoporus bimaculatus	Nem védett / indifferens
Mycetoporus brucki	Nem védett / indifferens
Mycetoporus clavicornis	Nem védett / indifferens
Mycetoporus corpulentus	Nem védett / indifferens
Mycetoporus dispersus	Nem védett / indifferens
Mycetoporus eppelsheimianus	Nem védett / indifferens
Mycetoporus erichsonanus	Nem védett / indifferens
Mycetoporus forticornis	Nem védett / indifferens
Mycetoporus gracilis	Nem védett / indifferens
Mycetoporus imperialis	Nem védett / indifferens
Mycetoporus lepidus	Nem védett / indifferens
Mycetoporus longulus	Nem védett / indifferens
Mycetoporus mulsanti	Nem védett / indifferens
Mycetoporus niger	Nem védett / indifferens
Mycetoporus nigricollis	Nem védett / indifferens
Mycetoporus piceolus	Nem védett / indifferens
Mycetoporus punctipennis	Nem védett / indifferens
Mycetoporus punctus	Nem védett / indifferens
Mycetoporus rufescens	Nem védett / indifferens
Mycetoporus solidicornis	Nem védett / indifferens
Mycetoporus solidicornis reichei	Nem védett / indifferens
Mycetota fimorum	Nem védett / indifferens
Mycetota laticollis	Nem védett / indifferens
Mychothenus minutus	Nem védett / indifferens
Mycterodus confusus	Nem védett / indifferens
Mycterodus immaculatus	Nem védett / indifferens
Mycterus tibialis	Nem védett / indifferens
Myelois circumvoluta	Nem védett / indifferens
Mylabris crocata	Nem védett / indifferens
Mylabris pannonica	Védett
Hycleus tenerus	Védett
Mylabris variabilis	Nem védett / indifferens
Myllaena brevicornis	Nem védett / indifferens
Myllaena dubia	Nem védett / indifferens
Myllaena gracilis	Nem védett / indifferens
Myllaena infuscata	Nem védett / indifferens
Myllaena intermedia	Nem védett / indifferens
Myllaena minuta	Nem védett / indifferens
Mylopharyngodon piceus	Nem védett / indifferens
Myndus musivus	Nem védett / indifferens
Myolepta dubia	Nem védett / indifferens
Myolepta nigritarsis	Nem védett / indifferens
Myolepta obscura	Nem védett / indifferens
Myolepta potens	Nem védett / indifferens
Myolepta vara	Nem védett / indifferens
Myotis alcathoe	Védett
Myotis bechsteinii	Fokozottan védett
Myotis blythii oxygnathus	Védett
Myotis brandtii	Védett
Myotis dasycneme	Fokozottan védett
Myotis daubentonii	Védett
Myotis emarginatus	Fokozottan védett
Myotis myotis	Védett
Myotis mystacinus	Védett
Myotis nattereri	Védett
Myrmarachne formicaria	Nem védett / indifferens
Myrmecaelurus trigrammus	Nem védett / indifferens
Myrmechixenus subterraneus	Nem védett / indifferens
Myrmechixenus vaporarium	Nem védett / indifferens
Myrmecina graminicola	Nem védett / indifferens
Myrmecophilus acervorum	Nem védett / indifferens
Myrmecoris gracilis	Nem védett / indifferens
Myrmecozela ochraceella	Nem védett / indifferens
Myrmedobia coleoptrata	Nem védett / indifferens
Myrmedobia exilis	Nem védett / indifferens
Myrmeleon bore	Védett
Myrmeleon formicarius	Védett
Myrmeleon inconspicuus	Védett
Myrmeleotettix antennatus	Nem védett / indifferens
Myrmeleotettix maculatus	Nem védett / indifferens
Myrmetes paykulli	Nem védett / indifferens
Myrmica deplanata	Nem védett / indifferens
Myrmica gallienii	Nem védett / indifferens
Myrmica lobicornis	Nem védett / indifferens
Myrmica microrubra	Nem védett / indifferens
Myrmica rubra	Nem védett / indifferens
Myrmica ruginodis	Nem védett / indifferens
Myrmica rugulosa	Nem védett / indifferens
Myrmica sabuleti	Nem védett / indifferens
Myrmica salina	Nem védett / indifferens
Myrmica scabrinodis	Nem védett / indifferens
Myrmica schencki	Nem védett / indifferens
Myrmica specioides	Nem védett / indifferens
Myrmica sulcinodis	Nem védett / indifferens
Myrmilla calva	Nem védett / indifferens
Myrmilla capitata	Nem védett / indifferens
Myrmilla glabrata	Nem védett / indifferens
Myrmilla lezginica	Nem védett / indifferens
Myrmilla mutica	Nem védett / indifferens
Myrmoecia perezi	Nem védett / indifferens
Myrmoecia plicata	Nem védett / indifferens
Myrmosa atra	Nem védett / indifferens
Myrmosa ephippium	Nem védett / indifferens
Myrmosa melanocephala	Nem védett / indifferens
Myrmus miriformis	Nem védett / indifferens
Myrrha octodecimguttata	Nem védett / indifferens
Mysmenella jobi	Nem védett / indifferens
Mystacides azureus	Nem védett / indifferens
Mystacides longicornis	Nem védett / indifferens
Mystacides niger	Nem védett / indifferens
Mythimna albipuncta	Nem védett / indifferens
Mythimna conigera	Nem védett / indifferens
Mythimna ferrago	Nem védett / indifferens
Mythimna impura	Nem védett / indifferens
Mythimna l-album	Nem védett / indifferens
Mythimna pallens	Nem védett / indifferens
Mythimna pudorina	Nem védett / indifferens
Mythimna straminea	Nem védett / indifferens
Mythimna turca	Nem védett / indifferens
Mythimna unipuncta	Nem védett / indifferens
Mythimna vitellina	Nem védett / indifferens
Myzia oblongoguttata	Nem védett / indifferens
Nabis flavomarginatus	Nem védett / indifferens
Nabis brevis	Nem védett / indifferens
Nabis capsiformis	Nem védett / indifferens
Nabis ferus	Nem védett / indifferens
Nabis limbatus	Nem védett / indifferens
Nabis lineata	Nem védett / indifferens
Nabis pseudoferus	Nem védett / indifferens
Nabis punctatus	Nem védett / indifferens
Nabis rugosus	Nem védett / indifferens
Nacerdes carniolica	Nem védett / indifferens
Nacerdes melanura	Nem védett / indifferens
Naenia typica	Védett
Nagusta goedelii	Nem védett / indifferens
Meliboeus fulgidicollis	Nem védett / indifferens
Nalassus convexus	Nem védett / indifferens
Nalassus dermestoides	Nem védett / indifferens
Nannopus palustris	Nem védett / indifferens
Nannospalax leucodon	Fokozottan védett
Nanoclavelia leucoptera	Nem védett / indifferens
Nanodiscus transversus	Nem védett / indifferens
Nanomimus anulatus	Nem védett / indifferens
Nanomimus circumscriptus	Nem védett / indifferens
Nanomimus hemisphaericus	Nem védett / indifferens
Nanophyes brevis	Nem védett / indifferens
Nanophyes globiformis	Nem védett / indifferens
Nanophyes globulus	Nem védett / indifferens
Nargus anisotomoides	Nem védett / indifferens
Nargus badius	Nem védett / indifferens
Nargus brunneus	Nem védett / indifferens
Nargus wilkini	Nem védett / indifferens
Narraga fasciolaria	Nem védett / indifferens
Narraga tessularia	Nem védett / indifferens
Narycia astrella	Nem védett / indifferens
Narycia duplicella	Nem védett / indifferens
Nascia cilialis	Nem védett / indifferens
Nathrius brevipennis	Nem védett / indifferens
Natrix natrix	Védett
Natrix tessellata	Védett
Neatus picipes	Nem védett / indifferens
Nebria brevicollis	Nem védett / indifferens
Nebria livida	Nem védett / indifferens
Nebria rufescens	Nem védett / indifferens
Nebrioporus canaliculatus	Nem védett / indifferens
Nebrioporus depressus	Nem védett / indifferens
Nebrioporus elegans	Nem védett / indifferens
Nebula salicata	Nem védett / indifferens
Necrobia ruficollis	Nem védett / indifferens
Necrobia rufipes	Nem védett / indifferens
Necrobia violacea	Nem védett / indifferens
Necrodes littoralis	Nem védett / indifferens
Necrophilus subterraneus	Nem védett / indifferens
Necydalis major	Védett
Necydalis ulmi	Védett
Nedyus quadrimaculatus	Nem védett / indifferens
Negastrius pulchellus	Nem védett / indifferens
Negastrius sabulicola	Nem védett / indifferens
Nehalennia speciosa	Nem védett / indifferens
Nehemitropia lividipennis	Nem védett / indifferens
Neides tipularius	Nem védett / indifferens
Strophosoma faber	Nem védett / indifferens
Nemadus colonoides	Nem védett / indifferens
Nemapogon clematella	Nem védett / indifferens
Nemapogon cloacella	Nem védett / indifferens
Nemapogon falstriella	Nem védett / indifferens
Nemapogon granella	Nem védett / indifferens
Nemapogon gravosaellus	Nem védett / indifferens
Nemapogon hungarica	Nem védett / indifferens
Nemapogon inconditella	Nem védett / indifferens
Nemapogon nigralbella	Nem védett / indifferens
Nemapogon picarella	Nem védett / indifferens
Nemapogon variatella	Nem védett / indifferens
Nemapogon wolffiella	Nem védett / indifferens
Nemasoma varicorne	Nem védett / indifferens
Nematodes filum	Nem védett / indifferens
Nematogmus sanguinolentus	Nem védett / indifferens
Nematopogon adansoniella	Nem védett / indifferens
Nematopogon metaxella	Nem védett / indifferens
Nematopogon pilella	Nem védett / indifferens
Nematopogon robertella	Nem védett / indifferens
Nematopogon schwarziellus	Nem védett / indifferens
Nematopogon swammerdamella	Nem védett / indifferens
Nemaxera betulinella	Nem védett / indifferens
Nemesia pannonica budensis	Védett
Nemobius sylvestris	Védett
Nemocoris fallenii	Nem védett / indifferens
Nemolecanium graniformis	Nem védett / indifferens
Nemonyx lepturoides	Nem védett / indifferens
Nemophora prodigellus	Nem védett / indifferens
Nemophora cupriacella	Nem védett / indifferens
Nemophora degeerella	Nem védett / indifferens
Nemophora dumerilella	Nem védett / indifferens
Nemophora fasciella	Nem védett / indifferens
Nemophora metallica	Nem védett / indifferens
Nemophora minimella	Nem védett / indifferens
Nemophora mollella	Nem védett / indifferens
Nemophora ochsenheimerella	Nem védett / indifferens
Nemophora pfeifferella	Nem védett / indifferens
Nemophora raddaella	Nem védett / indifferens
Nemophora violellus	Nem védett / indifferens
Nemoura avicularis	Nem védett / indifferens
Nemoura cambrica	Nem védett / indifferens
Nemoura cinerea	Nem védett / indifferens
Nemoura dubitans	Nem védett / indifferens
Nemoura flexuosa	Nem védett / indifferens
Nemoura fusca	Nem védett / indifferens
Nemoura longicauda	Nem védett / indifferens
Nemoura marginata	Nem védett / indifferens
Nemoura sciurus	Nem védett / indifferens
Nemozoma elongatum	Nem védett / indifferens
Nemurella pictetii	Nem védett / indifferens
Neoaliturus fenestratus	Nem védett / indifferens
Circulifer haematoceps	Nem védett / indifferens
Neoascia annexa	Nem védett / indifferens
Neoascia geniculata	Nem védett / indifferens
Neoascia interrupta	Nem védett / indifferens
Neoascia meticulosa	Nem védett / indifferens
Neoascia obliqua	Nem védett / indifferens
Neoascia podagrica	Nem védett / indifferens
Neoascia tenur	Nem védett / indifferens
Neoascia unifasciata	Nem védett / indifferens
Neobisnius lathrobioides	Nem védett / indifferens
Neobisnius procerulus	Nem védett / indifferens
Neobisnius villosulus	Nem védett / indifferens
Neoclytus acuminatus	Nem védett / indifferens
Neocnemodon brevidens	Nem védett / indifferens
Neocnemodon latitarsis	Nem védett / indifferens
Neocnemodon pubescens	Nem védett / indifferens
Neocnemodon vitripennis	Nem védett / indifferens
Neocoenorrhinus aeneovirens	Nem védett / indifferens
Neocoenorrhinus germanicus	Nem védett / indifferens
Neocoenorrhinus interpunctatus	Nem védett / indifferens
Neocoenorrhinus pauxillus	Nem védett / indifferens
Neodorcadion bilineatum	Védett
Neoephemera maxima	Védett
Neoergasilus japonicus	Nem védett / indifferens
Parus major	Védett
Neofaculta ericetella	Nem védett / indifferens
Neofaculta infernella	Nem védett / indifferens
Neofriseria singula	Nem védett / indifferens
Neoglocianus albovittatus	Nem védett / indifferens
Neoglocianus maculaalba	Nem védett / indifferens
Neogobius fluviatilis	Nem védett / indifferens
Neogobius gymnotrachelus	Nem védett / indifferens
Neogobius kessleri	Nem védett / indifferens
Neogobius melanostomus	Nem védett / indifferens
Neogobius syrman	Nem védett / indifferens
Neohilara subterranea	Nem védett / indifferens
Neomargarodes festucae	Nem védett / indifferens
Neomida haemorrhoidalis	Nem védett / indifferens
Neomys anomalus	Védett
Neomys fodiens	Védett
Neon pictus	Nem védett / indifferens
Neon rayi	Nem védett / indifferens
Neon reticulatus	Nem védett / indifferens
Neon valentulus	Nem védett / indifferens
Neophilaenus albipennis	Nem védett / indifferens
Neophilaenus campestris	Nem védett / indifferens
Neophilaenus exclamationis	Nem védett / indifferens
Neophilaenus infumatus	Nem védett / indifferens
Neophilaenus lineatus	Nem védett / indifferens
Neophilaenus minor	Nem védett / indifferens
Neophilaenus modestus	Nem védett / indifferens
Neophron percnopterus	Fokozottan védett
Neophytobius granatus	Nem védett / indifferens
Neophytobius muricatus	Nem védett / indifferens
Neophytobius quadrinodosus	Nem védett / indifferens
Neoplinthus tigratus porcatus	Nem védett / indifferens
Neopristilophus insitivus	Nem védett / indifferens
Neoscona adianta	Nem védett / indifferens
Neosphaleroptera nubilana	Nem védett / indifferens
Neottiglossa leporina	Nem védett / indifferens
Neottiglossa lineolata	Nem védett / indifferens
Neottiglossa pusilla	Nem védett / indifferens
Neottiura bimaculata	Nem védett / indifferens
Neottiura suaveolens	Nem védett / indifferens
Neozephyrus quercus	Védett
Nepa cinerea	Nem védett / indifferens
Nephanes titan	Nem védett / indifferens
Nephopterix angustella	Nem védett / indifferens
Nephus anomus	Nem védett / indifferens
Nephus bipunctatus	Nem védett / indifferens
Nephus bisignatus	Nem védett / indifferens
Nephus bisignatus claudiae	Nem védett / indifferens
Nephus horioni	Nem védett / indifferens
Nephus nigricans	Nem védett / indifferens
Nephus quadrimaculatus	Nem védett / indifferens
Nephus redtenbacheri	Nem védett / indifferens
Nephus semirufus	Nem védett / indifferens
Nephus ulbrichi	Nem védett / indifferens
Neptis rivularis	Védett
Neptis sappho	Védett
Neriene clathrata	Nem védett / indifferens
Neriene emphana	Nem védett / indifferens
Neriene furtiva	Nem védett / indifferens
Neriene montana	Nem védett / indifferens
Neriene peltata	Nem védett / indifferens
Neriene radiata	Nem védett / indifferens
Nesovitrea hammonis	Nem védett / indifferens
Nesticus cellulanus	Nem védett / indifferens
Protaetia cuprea	Nem védett / indifferens
Protaetia cuprea obscura	Nem védett / indifferens
Protaetia fieberi	Védett
Protaetia ungarica	Védett
Netta rufina	Védett
Nevraphes angulatus	Nem védett / indifferens
Neuraphes carinatus	Nem védett / indifferens
Neuraphes elongatulus	Nem védett / indifferens
Neuraphes parallelus	Nem védett / indifferens
Neuraphes plicicollis	Nem védett / indifferens
Neuraphes talparum	Nem védett / indifferens
Neureclipsis bimaculata	Nem védett / indifferens
Neuroleon nemausiensis	Védett
Neurothaumasia ankerella	Nem védett / indifferens
Newsteadia floccosa	Nem védett / indifferens
Nezara viridula	Nem védett / indifferens
Liothorax kraatzi	Nem védett / indifferens
Nialus varians	Nem védett / indifferens
Nicrophorus antennatus	Nem védett / indifferens
Nicrophorus interruptus	Nem védett / indifferens
Nicrophorus germanicus	Nem védett / indifferens
Nicrophorus humator	Nem védett / indifferens
Nicrophorus investigator	Nem védett / indifferens
Nicrophorus sepultor	Nem védett / indifferens
Nicrophorus vespillo	Nem védett / indifferens
Nicrophorus vespilloides	Nem védett / indifferens
Nicrophorus vestigator	Nem védett / indifferens
Niditinea fuscella	Nem védett / indifferens
Niditinea striolella	Nem védett / indifferens
Nigma flavescens	Nem védett / indifferens
Nigma walckenaeri	Nem védett / indifferens
Nimbus affinis	Nem védett / indifferens
Nimbus contaminatus	Nem védett / indifferens
Nimbus obliteratus	Nem védett / indifferens
Nineta carinthiaca	Nem védett / indifferens
Nineta flava	Nem védett / indifferens
Nineta guadarramensis	Nem védett / indifferens
Nineta guadarramensis principiae	Nem védett / indifferens
Nineta inpunctata	Nem védett / indifferens
Nineta pallida	Nem védett / indifferens
Nineta vittata	Nem védett / indifferens
Niphonympha dealbatella	Nem védett / indifferens
Niptus hololeucus	Nem védett / indifferens
Nitela fallax	Nem védett / indifferens
Nitela spinolae	Nem védett / indifferens
Nithecus jacobaeae	Nem védett / indifferens
Nitidula bipunctata	Nem védett / indifferens
Nitidula carnaria	Nem védett / indifferens
Nitidula flavomaculata	Nem védett / indifferens
Nitidula rufipes	Nem védett / indifferens
Nitocra inuber	Nem védett / indifferens
Nitocrella hibernica	Nem védett / indifferens
Nivellia sanguinosa	Nem védett / indifferens
Nobius serotinus	Nem védett / indifferens
Noctua comes	Nem védett / indifferens
Noctua fimbriata	Nem védett / indifferens
Noctua interjecta	Nem védett / indifferens
Noctua interposita	Nem védett / indifferens
Noctua janthe	Nem védett / indifferens
Noctua janthina	Nem védett / indifferens
Noctua orbona	Nem védett / indifferens
Noctua pronuba	Nem védett / indifferens
Nohoveus punctulatus	Védett
Nola aerugula	Nem védett / indifferens
Nola chlamitulalis	Nem védett / indifferens
Nola cicatricalis	Nem védett / indifferens
Nola confusalis	Nem védett / indifferens
Nola cristatula	Nem védett / indifferens
Nola cucullatella	Nem védett / indifferens
Nomada alboguttata	Nem védett / indifferens
Nomada argentata	Nem védett / indifferens
Nomada armata	Nem védett / indifferens
Nomada atroscutellaris	Nem védett / indifferens
Nomada baccata	Nem védett / indifferens
Nomada baccata hrubanti	Nem védett / indifferens
Nomada basalis	Nem védett / indifferens
Nomada bifasciata	Nem védett / indifferens
Nomada bifasciata fucata	Nem védett / indifferens
Nomada bifasciata lepeletieri	Nem védett / indifferens
Nomada bifida	Nem védett / indifferens
Nomada bispinosa	Nem védett / indifferens
Nomada blepharipes	Nem védett / indifferens
Nomada bluethgeni	Nem védett / indifferens
Nomada braunsiana	Nem védett / indifferens
Nomada calimorpha	Nem védett / indifferens
Nomada castellana	Nem védett / indifferens
Nomada chrysopyga	Nem védett / indifferens
Nomada cojungens	Nem védett / indifferens
Nomada concolor	Nem védett / indifferens
Nomada confinis	Nem védett / indifferens
Nomada corcyraea	Nem védett / indifferens
Nomada cruenta	Nem védett / indifferens
Nomada dira	Nem védett / indifferens
Nomada distinguenda	Nem védett / indifferens
Nomada emarginata	Nem védett / indifferens
Nomada errans	Nem védett / indifferens
Nomada erythrocephala	Nem védett / indifferens
Nomada fabriciana	Nem védett / indifferens
Nomada facilis	Nem védett / indifferens
Nomada femoralis	Nem védett / indifferens
Nomada ferruginata	Nem védett / indifferens
Nomada flava	Nem védett / indifferens
Nomada flavoguttata	Nem védett / indifferens
Nomada flavopicta	Nem védett / indifferens
Nomada fulvicornis	Nem védett / indifferens
Nomada furva	Nem védett / indifferens
Nomada furvoides	Nem védett / indifferens
Nomada fuscicornis	Nem védett / indifferens
Nomada glaucopis	Nem védett / indifferens
Nomada guttulata	Nem védett / indifferens
Nomada hirtipes	Nem védett / indifferens
Nomada hungarica	Nem védett / indifferens
Nomada immaculata	Nem védett / indifferens
Nomada imperialis	Nem védett / indifferens
Nomada incisa	Nem védett / indifferens
Nomada insignipes	Nem védett / indifferens
Nomada integra	Nem védett / indifferens
Nomada kohli	Nem védett / indifferens
Nomada lathburiana	Nem védett / indifferens
Nomada leucopthalma	Nem védett / indifferens
Nomada marshamella	Nem védett / indifferens
Nomada mauritanica	Nem védett / indifferens
Nomada mauritanica manni	Nem védett / indifferens
Nomada melanopyga	Nem védett / indifferens
Nomada melathoracica	Nem védett / indifferens
Nomada mocsaryi	Nem védett / indifferens
Nomada mutabilis	Nem védett / indifferens
Nomada mutica	Nem védett / indifferens
Nomada nobilis	Nem védett / indifferens
Nomada noskiewiczi	Nem védett / indifferens
Nomada obscura	Nem védett / indifferens
Nomada obtusifrons	Nem védett / indifferens
Nomada oculata	Nem védett / indifferens
Nomada opaca	Nem védett / indifferens
Nomada panzeri	Nem védett / indifferens
Nomada panzeri glabella	Nem védett / indifferens
Nomada pectoralis	Nem védett / indifferens
Nomada piccioliana	Nem védett / indifferens
Nomada platythorax	Nem védett / indifferens
Nomada pleurosticta	Nem védett / indifferens
Nomada posthuma	Nem védett / indifferens
Nomada pulchra	Nem védett / indifferens
Nomada pygidialis	Nem védett / indifferens
Nomada rhenana	Nem védett / indifferens
Nomada rostrata	Nem védett / indifferens
Nomada rufipes	Nem védett / indifferens
Nomada sexfasciata	Nem védett / indifferens
Nomada sheppardana	Nem védett / indifferens
Nomada sheppardana minuscula	Nem védett / indifferens
Nomada signata	Nem védett / indifferens
Nomada stigma	Nem védett / indifferens
Nomada stoeckherti	Nem védett / indifferens
Nomada striata	Nem védett / indifferens
Nomada succincta	Nem védett / indifferens
Nomada sybarita	Nem védett / indifferens
Nomada symphyti	Nem védett / indifferens
Nomada tenella	Nem védett / indifferens
Nomada thersites	Nem védett / indifferens
Nomada trapeziformis	Nem védett / indifferens
Nomada tridentirostris	Nem védett / indifferens
Nomada trispinosa	Nem védett / indifferens
Nomada verna	Nem védett / indifferens
Nomada villosa	Nem védett / indifferens
Nomada zonata	Nem védett / indifferens
Nomioides minutissimus	Nem védett / indifferens
Nomioides variegatus	Nem védett / indifferens
Nomisia exornata	Nem védett / indifferens
Nomius pygmaeus	Nem védett / indifferens
Nomophila noctuella	Nem védett / indifferens
Nonagria typhae	Nem védett / indifferens
Nopoiulus kochii	Nem védett / indifferens
Nosodendron fasciculare	Nem védett / indifferens
Nossidium pilosellum	Nem védett / indifferens
Notaris acridula	Nem védett / indifferens
Notaris aterrima	Nem védett / indifferens
Notaris maerkeli	Nem védett / indifferens
Notaris scirpi	Nem védett / indifferens
Noterus clavicornis	Nem védett / indifferens
Noterus crassicornis	Nem védett / indifferens
Nothocasis sertata	Nem védett / indifferens
Nothochrysa fulviceps	Nem védett / indifferens
Nothodes parvulus	Nem védett / indifferens
Nothris lemniscellus	Nem védett / indifferens
Nothris verbascella	Nem védett / indifferens
Notidobia ciliaris	Nem védett / indifferens
Notiophilus biguttatus	Nem védett / indifferens
Notiophilus germinyi	Nem védett / indifferens
Notiophilus laticollis	Nem védett / indifferens
Notiophilus palustris	Nem védett / indifferens
Notiophilus pusillus	Nem védett / indifferens
Notiophilus rufipes	Nem védett / indifferens
Nyctereutes procyonoides	Inváziós
Notocelia roborana	Nem védett / indifferens
Notocelia cynosbatella	Nem védett / indifferens
Notocelia incarnatana	Nem védett / indifferens
Notocelia trimaculana	Nem védett / indifferens
Notocelia uddmanniana	Nem védett / indifferens
Notodonta dromedarius	Nem védett / indifferens
Notodonta torva	Védett
Notodonta tritophus	Nem védett / indifferens
Notodonta ziczac	Nem védett / indifferens
Notodromas monacha	Nem védett / indifferens
Notolaemus castaneus	Nem védett / indifferens
Notolaemus unifasciatus	Nem védett / indifferens
Notonecta glauca	Nem védett / indifferens
Notonecta lutea	Védett
Notonecta meridionalis	Nem védett / indifferens
Notonecta obliqua	Nem védett / indifferens
Notonecta viridis	Nem védett / indifferens
Notostira elongata	Nem védett / indifferens
Notostira erratica	Nem védett / indifferens
Notothecta flavipes	Nem védett / indifferens
Notoxus cavifrons appendicinus	Nem védett / indifferens
Notoxus brachycerus	Nem védett / indifferens
Notoxus monoceros	Nem védett / indifferens
Notoxus trifasciatus	Nem védett / indifferens
Notus flavipennis	Nem védett / indifferens
Noxius curtirostris	Nem védett / indifferens
Nucifraga caryocatactes	Védett
Nucifraga caryocatactes macrorhynchos	Védett
Nuctenea umbratica	Nem védett / indifferens
Dynaspidiotus abietis	Nem védett / indifferens
Nudaria mundana	Védett
Nudobius lentus	Nem védett / indifferens
Numenius arquata	Fokozottan védett
Numenius arquata orientalis	Fokozottan védett
Numenius phaeopus	Védett
Numenius tenuirostris	Fokozottan védett
Nychiodes dalmatina	Nem védett / indifferens
Nychiodes obscuraria	Nem védett / indifferens
Nyctalus lasiopterus	Fokozottan védett
Nyctalus leisleri	Védett
Nyctalus noctula	Védett
Bubo scandiaca	Fokozottan védett
Nyctegretis lineana	Nem védett / indifferens
Nyctegretis triangulella	Nem védett / indifferens
Nycteola asiatica	Nem védett / indifferens
Nycteola degenerana	Nem védett / indifferens
Nycteola revayana	Nem védett / indifferens
Nycteola siculana	Nem védett / indifferens
Nycticorax nycticorax	Fokozottan védett
Nymphalis antiopa	Védett
Nymphalis polychloros	Védett
Nymphalis vaualbum	Védett
Nymphalis xanthomelas	Védett
Nymphula nitidulata	Nem védett / indifferens
Nysius cymoides	Nem védett / indifferens
Nysius ericae	Nem védett / indifferens
Nysius graminicola	Nem védett / indifferens
Nysius helveticus	Nem védett / indifferens
Nysius senecionis	Nem védett / indifferens
Nysius thymi	Nem védett / indifferens
Nysson chevrieri	Nem védett / indifferens
Nysson dimidiatus	Nem védett / indifferens
Nysson fulvipes	Nem védett / indifferens
Nysson interruptus	Nem védett / indifferens
Nysson maculosus	Nem védett / indifferens
Nysson niger	Nem védett / indifferens
Nysson roubali	Nem védett / indifferens
Nysson spinosus	Nem védett / indifferens
Nysson tridens	Nem védett / indifferens
Parus montanus	Védett
Nysson trimaculatus	Nem védett / indifferens
Nysson variabilis	Nem védett / indifferens
Oberea erythrocephala	Nem védett / indifferens
Oberea euphorbiae	Védett
Oberea linearis	Nem védett / indifferens
Oberea moravica	Nem védett / indifferens
Oberea oculata	Nem védett / indifferens
Oberea pedemontana	Védett
Oberea pupillata	Nem védett / indifferens
Obrium brunneum	Nem védett / indifferens
Obrium cantharinum	Nem védett / indifferens
Ocalea badia	Nem védett / indifferens
Ocalea picata	Nem védett / indifferens
Ocalea puncticeps	Nem védett / indifferens
Ocalea rivularis	Nem védett / indifferens
Ochetostethus opacus	Nem védett / indifferens
Ochina latrellii	Nem védett / indifferens
Ochlerotatus annulipes	Nem védett / indifferens
Ochlerotatus cantans	Nem védett / indifferens
Ochlerotatus caspius	Nem védett / indifferens
Ochlerotatus cataphylla	Nem védett / indifferens
Ochlerotatus communis	Nem védett / indifferens
Ochlerotatus detritus	Nem védett / indifferens
Ochlerotatus dorsalis	Nem védett / indifferens
Ochlerotatus excrucians	Nem védett / indifferens
Ochlerotatus flavescens	Nem védett / indifferens
Ochlerotatus geniculatus	Nem védett / indifferens
Ochlerotatus hungaricus	Nem védett / indifferens
Ochlerotatus leucomelas	Nem védett / indifferens
Ochlerotatus nigrinus	Nem védett / indifferens
Ochlerotatus pulchritarsis	Nem védett / indifferens
Ochlerotatus punctor	Nem védett / indifferens
Ochlerotatus refiki	Nem védett / indifferens
Ochlerotatus rusticus	Nem védett / indifferens
Ochlerotatus sticticus	Nem védett / indifferens
Ochlodes sylvanus	Nem védett / indifferens
Ochodaeus chrysomeloides	Nem védett / indifferens
Ochodaeus thalycroides	Nem védett / indifferens
Ochogona caroli	Nem védett / indifferens
Ochogona caroli hungaricum	Nem védett / indifferens
Ochogona caroli somloense	Nem védett / indifferens
Ochogona triaina	Nem védett / indifferens
Ochromolopis ictella	Nem védett / indifferens
Ochropacha duplaris	Nem védett / indifferens
Ochropleura leucogaster	Nem védett / indifferens
Ochropleura plecta	Nem védett / indifferens
Ochsenheimeria capella	Nem védett / indifferens
Ochsenheimeria taurella	Nem védett / indifferens
Ochsenheimeria urella	Nem védett / indifferens
Ochsenheimeria vacculella	Nem védett / indifferens
Ochthebius caudatus	Nem védett / indifferens
Ochthebius colveranus	Nem védett / indifferens
Ochthebius flavipes	Nem védett / indifferens
Ochthebius hungaricus	Nem védett / indifferens
Ochthebius lividipennis	Nem védett / indifferens
Ochthebius metallescens	Nem védett / indifferens
Ochthebius meridionalis	Nem védett / indifferens
Ochthebius minimus	Nem védett / indifferens
Ochthebius peisonis	Nem védett / indifferens
Ochthebius pusillus	Nem védett / indifferens
Ochthebius striatus	Nem védett / indifferens
Ochthebius viridis	Nem védett / indifferens
Ochthephilus omalinus	Nem védett / indifferens
Ocneria rubea	Védett
Ocnogyna parasita	Védett
Octotemnus glabriculus	Nem védett / indifferens
Ocypus aeneocephalus	Nem védett / indifferens
Ocypus biharicus	Nem védett / indifferens
Ocypus brevipennis	Nem védett / indifferens
Ocypus brunnipes	Nem védett / indifferens
Ocypus fulvipennis	Nem védett / indifferens
Ocypus fuscatus	Nem védett / indifferens
Ocypus macrocephalus	Nem védett / indifferens
Ocypus mus	Nem védett / indifferens
Ocypus nitens	Nem védett / indifferens
Ocypus olens	Nem védett / indifferens
Ocypus ophthalmicus	Nem védett / indifferens
Ocypus picipennis	Nem védett / indifferens
Ocypus serotinus	Nem védett / indifferens
Ocypus tenebricosus	Nem védett / indifferens
Ocyusa maura	Nem védett / indifferens
Ocyusa picina	Nem védett / indifferens
Odacantha melanura	Nem védett / indifferens
Odezia atrata	Védett
Odice arcuinna	Védett
Odites kollarella	Nem védett / indifferens
Odonestis pruni	Nem védett / indifferens
Bolboceras armiger	Nem védett / indifferens
Odontocerum albicorne	Nem védett / indifferens
Odontognophos dumetata	Védett
Odontopera bidentata	Nem védett / indifferens
Odontopodisma decipiens	Nem védett / indifferens
Odontopodisma rubripes	Védett
Odontopodisma schmidtii	Nem védett / indifferens
Odontoscelis fuliginosa	Nem védett / indifferens
Odontoscelis hispidula	Védett
Odontoscelis lineola	Nem védett / indifferens
Odontosia carmelita	Védett
Odontotarsus purpureolineatus	Nem védett / indifferens
Odontotarsus robustus	Nem védett / indifferens
Odynerus femoratus	Nem védett / indifferens
Odynerus melanocephalus	Nem védett / indifferens
Odynerus poecilus	Nem védett / indifferens
Odynerus reniformis	Nem védett / indifferens
Odynerus simillimus	Nem védett / indifferens
Odynerus spinipes	Nem védett / indifferens
Oecanthus pellucens	Nem védett / indifferens
Oecetis furva	Nem védett / indifferens
Oecetis lacustris	Nem védett / indifferens
Oecetis notata	Nem védett / indifferens
Oecetis ochracea	Nem védett / indifferens
Oecetis testacea	Nem védett / indifferens
Oecetis tripunctata	Nem védett / indifferens
Oeciacus hirundinis	Nem védett / indifferens
Oecismus monedula	Nem védett / indifferens
Oecophora bractella	Nem védett / indifferens
Oedaleus decorus	Nem védett / indifferens
Oedecnemidius pictus	Nem védett / indifferens
Oedemera croceicollis	Nem védett / indifferens
Oedemera femoralis	Nem védett / indifferens
Oedemera femorata	Nem védett / indifferens
Oedemera flavipes	Nem védett / indifferens
Oedemera lateralis	Nem védett / indifferens
Oedemera lurida	Nem védett / indifferens
Oedemera podagrariae	Nem védett / indifferens
Oedemera pthysica	Nem védett / indifferens
Oedemera subrobusta	Nem védett / indifferens
Oedemera virescens	Nem védett / indifferens
Oedipoda caerulescens	Nem védett / indifferens
Oedostethus quadripustulatus	Nem védett / indifferens
Oedostethus tenuicornis	Nem védett / indifferens
Oedothorax agrestis	Nem védett / indifferens
Oedothorax apicatus	Nem védett / indifferens
Oedothorax fuscus	Nem védett / indifferens
Oedothorax gibbosus	Nem védett / indifferens
Oedothorax retusus	Nem védett / indifferens
Oegoconia caradjai	Nem védett / indifferens
Oegoconia deauratella	Nem védett / indifferens
Oegoconia uralskella	Nem védett / indifferens
Oemopteryx loewii	Nem védett / indifferens
Oenanthe deserti	Védett
Oenanthe hispanica	Védett
Oenanthe hispanica melanoleuca	Védett
Oenanthe isabellina	Védett
Oenanthe oenanthe	Védett
Oenanthe pleschanka	Védett
Oenas crassicornis	Nem védett / indifferens
Oenopia conglobata	Nem védett / indifferens
Oenopia impustulata	Nem védett / indifferens
Oenopia lyncea	Nem védett / indifferens
Oenopia lyncea agnata	Nem védett / indifferens
Oiceoptoma thoracicum	Nem védett / indifferens
Hellinsia carphodactyla	Nem védett / indifferens
Oidaematophorus constanti	Nem védett / indifferens
Hellinsia didactylites	Nem védett / indifferens
Hellinsia distinctus	Nem védett / indifferens
Hellinsia inulae	Nem védett / indifferens
Oidaematophorus lithodactyla	Nem védett / indifferens
Hellinsia tephradactyla	Nem védett / indifferens
Oinophila v-flava	Nem védett / indifferens
Olethreutes arcuella	Nem védett / indifferens
Argyroploce roseomaculana	Nem védett / indifferens
Olibrus aeneus	Nem védett / indifferens
Olibrus affinis	Nem védett / indifferens
Olibrus baudueri	Nem védett / indifferens
Olibrus bicolor	Nem védett / indifferens
Olibrus bimaculatus	Nem védett / indifferens
Olibrus bisignatus	Nem védett / indifferens
Olibrus corticalis	Nem védett / indifferens
Olibrus gerhardti	Nem védett / indifferens
Olibrus liquidus	Nem védett / indifferens
Olibrus millefolii	Nem védett / indifferens
Olibrus pygmaeus	Nem védett / indifferens
Oligella foveolata	Nem védett / indifferens
Oligia dubia	Nem védett / indifferens
Oligia fasciuncula	Nem védett / indifferens
Oligia latruncula	Nem védett / indifferens
Oligia strigilis	Nem védett / indifferens
Oligia versicolor	Nem védett / indifferens
Oligolimax annularis	Védett
Oligomerus brunneus	Nem védett / indifferens
Oligomerus ptilinoides	Nem védett / indifferens
Oligomerus retowskii	Nem védett / indifferens
Oligoneuriella keffermuellerae	Védett
Oligoneuriella pallida	Védett
Oligoneuriella polonica	Nem védett / indifferens
Oligoneuriella rhenana	Védett
Oligostomis reticulata	Nem védett / indifferens
Oligota granaria	Nem védett / indifferens
Oligota inflata	Nem védett / indifferens
Oligota parva	Nem védett / indifferens
Oligota pumilio	Nem védett / indifferens
Oligota pusillima	Nem védett / indifferens
Oligota rufipennis	Nem védett / indifferens
Oligotricha striata	Védett
Olindia schumacherana	Nem védett / indifferens
Olisthopus rotundatus	Nem védett / indifferens
Olisthopus sturmii	Nem védett / indifferens
Olophrum assimile	Nem védett / indifferens
Olophrum austriacum	Nem védett / indifferens
Olophrum piceum	Nem védett / indifferens
Olophrum puncticolle	Nem védett / indifferens
Olophrum viennense	Nem védett / indifferens
Omalisus fontisbellaquaei	Nem védett / indifferens
Omalium caesum	Nem védett / indifferens
Omalium cinnamomeum	Nem védett / indifferens
Omalium excavatum	Nem védett / indifferens
Omalium exiguum	Nem védett / indifferens
Omalium ferrugineum	Nem védett / indifferens
Omalium imitator	Nem védett / indifferens
Omalium littorale	Nem védett / indifferens
Omalium oxyacanthae	Nem védett / indifferens
Omalium rivulare	Nem védett / indifferens
Omalium rugatum	Nem védett / indifferens
Omalium strigicolle	Nem védett / indifferens
Omalium validum	Nem védett / indifferens
Omaloplia marginata	Nem védett / indifferens
Omaloplia ruricola	Nem védett / indifferens
Omaloplia spireae	Nem védett / indifferens
Omalus aeneus	Nem védett / indifferens
Omalus biaccinctus	Nem védett / indifferens
Omiamima concinna	Nem védett / indifferens
Omiamima mollina	Nem védett / indifferens
Omiamima vindobonensis	Nem védett / indifferens
Omias globulus	Nem védett / indifferens
Omias punctatus	Nem védett / indifferens
Omias rotundatus	Nem védett / indifferens
Omias seminulum	Nem védett / indifferens
Ommatidiotus concinnus	Nem védett / indifferens
Ommatidiotus dissimilis	Nem védett / indifferens
Ommatidiotus falleni	Nem védett / indifferens
Ommatidiotus inconspicuus	Nem védett / indifferens
Ommatoiulus sabulosus	Nem védett / indifferens
Omocestus haemorrhoidalis	Nem védett / indifferens
Omocestus petraeus	Nem védett / indifferens
Omocestus rufipes	Nem védett / indifferens
Omocestus viridulus	Nem védett / indifferens
Omonadus bifasciatus	Nem védett / indifferens
Omonadus floralis	Nem védett / indifferens
Omonadus formicarius	Nem védett / indifferens
Omophlus betulae	Nem védett / indifferens
Omophlus lividipes	Nem védett / indifferens
Omophlus picipes	Nem védett / indifferens
Omophlus proteus	Nem védett / indifferens
Omophlus rugosicollis	Nem védett / indifferens
Omophron limbatum	Nem védett / indifferens
Omosita colon	Nem védett / indifferens
Omosita depressa	Nem védett / indifferens
Omosita discoidea	Nem védett / indifferens
Omphalapion dispar	Nem védett / indifferens
Omphalapion hookerorum	Nem védett / indifferens
Omphalapion pseudodispar	Nem védett / indifferens
Omphalapion laevigatum	Nem védett / indifferens
Omphalonotus quadriguttatus	Nem védett / indifferens
Omphalophana antirrhinii	Nem védett / indifferens
Alophia combustella	Nem védett / indifferens
Laodamia faecella	Nem védett / indifferens
Oncocera semirubella	Nem védett / indifferens
Oncochila scapularis	Nem védett / indifferens
Oncochila simplex	Nem védett / indifferens
Oncodelphax pullulus	Nem védett / indifferens
Oncopsis alni	Nem védett / indifferens
Oncopsis flavicollis	Nem védett / indifferens
Oncopsis tristis	Nem védett / indifferens
Oncorhynchus mykiss	Nem védett / indifferens
Oncotylus setulosus	Nem védett / indifferens
Oncotylus viridiflavus	Nem védett / indifferens
Ondatra zibethicus	Inváziós
Ontholestes haroldi	Nem védett / indifferens
Ontholestes murinus	Nem védett / indifferens
Ontholestes tessellatus	Nem védett / indifferens
Onthophagus coenobita	Nem védett / indifferens
Onthophagus fracticornis	Nem védett / indifferens
Onthophagus furcatus	Nem védett / indifferens
Onthophagus furciceps	Nem védett / indifferens
Onthophagus gibbulus	Nem védett / indifferens
Onthophagus grossepunctatus	Nem védett / indifferens
Onthophagus illyricus	Nem védett / indifferens
Onthophagus joannae	Nem védett / indifferens
Onthophagus lemur	Nem védett / indifferens
Onthophagus lucidus	Nem védett / indifferens
Onthophagus nuchicornis	Nem védett / indifferens
Onthophagus ovatus	Nem védett / indifferens
Onthophagus ruficapillus	Nem védett / indifferens
Onthophagus semicornis	Nem védett / indifferens
Onthophagus similis	Nem védett / indifferens
Onthophagus taurus	Nem védett / indifferens
Onthophagus tesquorum	Nem védett / indifferens
Onthophagus vacca	Nem védett / indifferens
Onthophagus verticicornis	Nem védett / indifferens
Onthophagus vitulus	Nem védett / indifferens
Onthophilus affinis	Nem védett / indifferens
Onthophilus punctatus	Nem védett / indifferens
Onthophilus striatus	Nem védett / indifferens
Onychogomphus forcipatus	Védett
Onyxacalles croaticus	Nem védett / indifferens
Onyxacalles luigionii	Nem védett / indifferens
Onyxacalles pyrenaeus	Nem védett / indifferens
Oodes gracilis	Nem védett / indifferens
Oodes helopioides	Nem védett / indifferens
Platyscelis melas	Védett
Platyscelis polita	Védett
Oonops pulcher	Nem védett / indifferens
Ootypus globosus	Nem védett / indifferens
Opanthribus tessellatus	Nem védett / indifferens
Opatrum riparium	Nem védett / indifferens
Opatrum sabulosum	Nem védett / indifferens
Opeas pumilum	Nem védett / indifferens
Operophtera brumata	Nem védett / indifferens
Operophtera fagata	Nem védett / indifferens
Opetiopalpus scutellaris	Nem védett / indifferens
Ophiogomphus cecilia	Védett
Ophiola cornicula	Nem védett / indifferens
Ophiola decumana	Nem védett / indifferens
Ophiola russeola	Nem védett / indifferens
Ophiola transversa	Nem védett / indifferens
Ophonus ardosiacus	Nem védett / indifferens
Ophonus azureus	Nem védett / indifferens
Cephalophonus cephalotes	Nem védett / indifferens
Ophonus cordatus	Nem védett / indifferens
Ophonus cribricollis	Nem védett / indifferens
Ophonus diffinis	Nem védett / indifferens
Ophonus gammeli	Nem védett / indifferens
Ophonus melletii	Nem védett / indifferens
Ophonus laticollis	Nem védett / indifferens
Ophonus parallelus	Nem védett / indifferens
Ophonus puncticeps	Nem védett / indifferens
Ophonus puncticollis	Nem védett / indifferens
Ophonus rufibarbis	Nem védett / indifferens
Ophonus rupicola	Nem védett / indifferens
Ophonus sabulicola	Védett
Ophonus sabulicola ponticus	Nem védett / indifferens
Ophonus schaubergerianus	Nem védett / indifferens
Ophonus stictus	Nem védett / indifferens
Ophonus subquadratus	Nem védett / indifferens
Ophonus subsinuatus	Nem védett / indifferens
Ophyiulus pilosus	Nem védett / indifferens
Opigena polygona	Nem védett / indifferens
Opilo domesticus	Nem védett / indifferens
Opilo mollis	Nem védett / indifferens
Opilo pallidus	Nem védett / indifferens
Opisthograptis luteolata	Nem védett / indifferens
Oplosia cinerea	Nem védett / indifferens
Oporopsamma wertheimsteini	Nem védett / indifferens
Opostega salaciella	Nem védett / indifferens
Opostega spatulella	Nem védett / indifferens
Oprohinus consputus	Nem védett / indifferens
Oprohinus suturalis	Nem védett / indifferens
Opsibotys fuscalis	Nem védett / indifferens
Opsilia coerulescens	Nem védett / indifferens
Opsilia molybdaena	Nem védett / indifferens
Opsilia uncinata	Nem védett / indifferens
Opsius lethierryi	Nem védett / indifferens
Opsius stactogalus	Nem védett / indifferens
Orbona fragariae	Védett
Orchesia fasciata	Nem védett / indifferens
Orchesia grandicollis	Nem védett / indifferens
Orchesia micans	Nem védett / indifferens
Orchesia minor	Nem védett / indifferens
Orchesia undulata	Nem védett / indifferens
Orchestes fagi	Nem védett / indifferens
Orchestes hortorum	Nem védett / indifferens
Orchestes rusci	Nem védett / indifferens
Orchestes subfasciatus	Nem védett / indifferens
Orchestes testaceus	Nem védett / indifferens
Orconectes limosus	Inváziós
Orcula dolium	Védett
Orcula jetschini	Nem védett / indifferens
Orectis proboscidata	Nem védett / indifferens
Orectochilus villosus	Nem védett / indifferens
Oreochromis niloticus	Nem védett / indifferens
Orgyia antiqua	Nem védett / indifferens
Orgyia antiquoides	Nem védett / indifferens
Orgyia recens	Nem védett / indifferens
Oria musculosa	Védett
Oriolus oriolus	Védett
Orius horvathi	Nem védett / indifferens
Orius laticollis	Nem védett / indifferens
Orius majusculus	Nem védett / indifferens
Orius minutus	Nem védett / indifferens
Orius niger	Nem védett / indifferens
Orius vicinus	Nem védett / indifferens
Ornativalva plutelliformis	Nem védett / indifferens
Ornatoraphidia flavilabris	Nem védett / indifferens
Ornixola caudulatella	Nem védett / indifferens
Orobitis cyanea	Nem védett / indifferens
Orobitis nigrina	Nem védett / indifferens
Orochares angustatus	Nem védett / indifferens
Parammoecius corvinus	Nem védett / indifferens
Orphilus niger	Nem védett / indifferens
Orsillus depressus	Nem védett / indifferens
Orthetrum albistylum	Nem védett / indifferens
Orthetrum brunneum	Védett
Orthetrum cancellatum	Nem védett / indifferens
Orthetrum coerulescens	Nem védett / indifferens
Orthezia urticae	Nem védett / indifferens
Ortheziola vejdovskyi	Nem védett / indifferens
Orthocephalus bivittatus	Nem védett / indifferens
Orthocephalus brevis	Nem védett / indifferens
Orthocephalus saltator	Nem védett / indifferens
Orthocephalus vittipennis	Nem védett / indifferens
Orthocerus clavicornis	Nem védett / indifferens
Orthocerus crassicornis	Nem védett / indifferens
Orthochaetes setiger	Nem védett / indifferens
Orthocis alni	Nem védett / indifferens
Orthocis coluber	Nem védett / indifferens
Orthocis festivus	Nem védett / indifferens
Orthocis lucasi	Nem védett / indifferens
Orthocis pseudolinearis	Nem védett / indifferens
Orthocis pygmaeus	Nem védett / indifferens
Orthocis vestitus	Nem védett / indifferens
Ortholepis betulae	Nem védett / indifferens
Ortholomus punctipennis	Nem védett / indifferens
Nycterosea obstipata	Nem védett / indifferens
Orthonama vittata	Nem védett / indifferens
Orthonevra brevicornis	Nem védett / indifferens
Orthonevra elegans	Nem védett / indifferens
Orthonevra frontalis	Nem védett / indifferens
Orthonevra geniculata	Nem védett / indifferens
Orthonevra incisa	Nem védett / indifferens
Orthonevra intermedia	Nem védett / indifferens
Orthonevra nobilis	Nem védett / indifferens
Orthonevra plumbago	Nem védett / indifferens
Orthonevra splendens	Nem védett / indifferens
Orthonotus cylindricollis	Nem védett / indifferens
Orthonotus rufifrons	Nem védett / indifferens
Orthoperus atomus	Nem védett / indifferens
Orthoperus corticalis	Nem védett / indifferens
Orthoperus mundus	Nem védett / indifferens
Orthoperus nigrescens	Nem védett / indifferens
Orthoperus punctatus	Nem védett / indifferens
Orthoperus rogeri	Nem védett / indifferens
Orthopodomyia pulchripalpis	Nem védett / indifferens
Orthops basalis	Nem védett / indifferens
Orthops campestris	Nem védett / indifferens
Orthops kalmii	Nem védett / indifferens
Ocrasa glaucinalis	Nem védett / indifferens
Orthosia cerasi	Nem védett / indifferens
Orthosia cruda	Nem védett / indifferens
Orthosia gothica	Nem védett / indifferens
Orthosia gracilis	Nem védett / indifferens
Orthosia incerta	Nem védett / indifferens
Orthosia miniosa	Nem védett / indifferens
Orthosia opima	Nem védett / indifferens
Orthosia populeti	Nem védett / indifferens
Orthostixis cribraria	Védett
Orthotaenia undulana	Nem védett / indifferens
Orthotelia sparganella	Nem védett / indifferens
Orthotomicus erosus	Nem védett / indifferens
Orthotomicus laricis	Nem védett / indifferens
Orthotomicus longicollis	Nem védett / indifferens
Orthotomicus proximus	Nem védett / indifferens
Orthotomicus robustus	Nem védett / indifferens
Orthotomicus suturalis	Nem védett / indifferens
Orthotrichia angustella	Nem védett / indifferens
Orthotrichia costalis	Nem védett / indifferens
Orthotrichia tragetti	Nem védett / indifferens
Orthotylus bilineatus	Nem védett / indifferens
Orthotylus ericetorum	Nem védett / indifferens
Orthotylus flavinervis	Nem védett / indifferens
Orthotylus flavosparsus	Nem védett / indifferens
Orthotylus fuscescens	Nem védett / indifferens
Orthotylus marginalis	Nem védett / indifferens
Orthotylus nassatus	Nem védett / indifferens
Orthotylus prasinus	Nem védett / indifferens
Orthotylus schoberiae	Nem védett / indifferens
Orthotylus tenellus	Nem védett / indifferens
Orthotylus virens	Nem védett / indifferens
Orthotylus viridinervis	Nem védett / indifferens
Oryctes nasicornis	Védett
Oryctolagus cuniculus	Nem védett / indifferens
Oryphantes angulatus	Nem védett / indifferens
Oryxolaemus flavifemoratus	Nem védett / indifferens
Oryzaephilus mercator	Nem védett / indifferens
Oryzaephilus surinamensis	Nem védett / indifferens
Osmia acuticornis	Nem védett / indifferens
Osmia adunca	Nem védett / indifferens
Osmia andrenoides	Nem védett / indifferens
Osmia anthocopoides	Nem védett / indifferens
Osmia aurulenta	Nem védett / indifferens
Osmia bicolor	Nem védett / indifferens
Osmia bidentata	Nem védett / indifferens
Osmia bisulca	Nem védett / indifferens
Osmia brevicornis	Nem védett / indifferens
Osmia caerulescens	Nem védett / indifferens
Osmia cerinthidis	Nem védett / indifferens
Osmia claviventris	Nem védett / indifferens
Osmia cornuta	Nem védett / indifferens
Osmia dives	Nem védett / indifferens
Osmia fulva	Nem védett / indifferens
Osmia gallarum	Nem védett / indifferens
Osmia laevifrons	Nem védett / indifferens
Osmia leaiana	Nem védett / indifferens
Osmia leucomelaena	Nem védett / indifferens
Osmia ligurica	Nem védett / indifferens
Osmia loti	Nem védett / indifferens
Osmia manicata	Nem védett / indifferens
Osmia melanogaster	Nem védett / indifferens
Osmia mitis	Nem védett / indifferens
Osmia mocsaryi	Nem védett / indifferens
Osmia mustelina	Nem védett / indifferens
Osmia niveata	Nem védett / indifferens
Osmia papaveris	Nem védett / indifferens
Osmia parietina	Nem védett / indifferens
Osmia pilicornis	Nem védett / indifferens
Osmia praestans	Nem védett / indifferens
Osmia princeps	Nem védett / indifferens
Osmia ravouxi	Nem védett / indifferens
Osmia rufa	Nem védett / indifferens
Osmia rufohirta	Nem védett / indifferens
Osmia scutellaris	Nem védett / indifferens
Osmia spinulosa	Nem védett / indifferens
Osmia tenuispina	Nem védett / indifferens
Osmia tergestensis	Nem védett / indifferens
Osmia tridentata	Nem védett / indifferens
Osmoderma eremita	Fokozottan védett
Osmylus fulvicephalus	Védett
Osphya bipunctata	Nem védett / indifferens
Ossiannilssonola callosa	Nem védett / indifferens
Ostoma ferruginea	Nem védett / indifferens
Ostrinia nubilalis	Nem védett / indifferens
Ostrinia palustralis	Védett
Ostrinia quadripunctalis	Nem védett / indifferens
Othius brevipennis	Nem védett / indifferens
Othius laeviusculus	Nem védett / indifferens
Othius lapidicola	Nem védett / indifferens
Othius punctulatus	Nem védett / indifferens
Othius subuliformis	Nem védett / indifferens
Otho sphondyloides	Védett
Otiorhynchus armadillo	Nem védett / indifferens
Otiorhynchus auricapillus	Nem védett / indifferens
Otiorhynchus austriacus	Nem védett / indifferens
Otiorhynchus bisulcatus	Nem védett / indifferens
Otiorhynchus brunneus	Nem védett / indifferens
Otiorhynchus cardiniger	Nem védett / indifferens
Otiorhynchus coarctatus	Nem védett / indifferens
Otiorhynchus coecus	Nem védett / indifferens
Parus montanus borealis	Védett
Otiorhynchus conspersus	Nem védett / indifferens
Otiorhynchus corruptor	Nem védett / indifferens
Otiorhynchus crataegi	Nem védett / indifferens
Otiorhynchus duinensis	Nem védett / indifferens
Otiorhynchus equestris	Nem védett / indifferens
Otiorhynchus frescati	Nem védett / indifferens
Otiorhynchus fullo	Nem védett / indifferens
Otiorhynchus gemmatus	Nem védett / indifferens
Otiorhynchus hungaricus	Nem védett / indifferens
Otiorhynchus juglandis	Nem védett / indifferens
Otiorhynchus kollari	Nem védett / indifferens
Otiorhynchus labilis	Nem védett / indifferens
Otiorhynchus laevigatus	Nem védett / indifferens
Otiorhynchus lasius	Nem védett / indifferens
Otiorhynchus lavandus	Nem védett / indifferens
Otiorhynchus lepidopterus	Nem védett / indifferens
Otiorhynchus ligustici	Nem védett / indifferens
Otiorhynchus lutosus	Nem védett / indifferens
Otiorhynchus mandibularis	Nem védett / indifferens
Otiorhynchus maxillosus	Nem védett / indifferens
Otiorhynchus morio	Nem védett / indifferens
Otiorhynchus multipunctatus	Nem védett / indifferens
Otiorhynchus opulentus	Nem védett / indifferens
Otiorhynchus orbicularis	Nem védett / indifferens
Otiorhynchus ovatus	Nem védett / indifferens
Otiorhynchus pauxillus	Nem védett / indifferens
Otiorhynchus perdix	Nem védett / indifferens
Otiorhynchus pinastri	Nem védett / indifferens
Otiorhynchus populeti	Nem védett / indifferens
Otiorhynchus porcatus	Nem védett / indifferens
Otiorhynchus raucus	Nem védett / indifferens
Otiorhynchus reichei	Nem védett / indifferens
Otiorhynchus repletus	Nem védett / indifferens
Otiorhynchus roubali	Védett
Otiorhynchus rugifrons	Nem védett / indifferens
Otiorhynchus rugosostriatus	Nem védett / indifferens
Otiorhynchus sabulosus	Nem védett / indifferens
Otiorhynchus scaber	Nem védett / indifferens
Otiorhynchus sensitivus	Nem védett / indifferens
Otiorhynchus similis	Nem védett / indifferens
Otiorhynchus sulcatus	Nem védett / indifferens
Otiorhynchus tenebricosus	Nem védett / indifferens
Otiorhynchus tristis	Nem védett / indifferens
Otiorhynchus velutinus	Nem védett / indifferens
Otiorhynchus winkleri	Nem védett / indifferens
Otis tarda	Fokozottan védett
Otolelus pruinosus	Nem védett / indifferens
Otophorus haemorrhoidalis	Nem védett / indifferens
Otus scops	Fokozottan védett
Ourapteryx sambucaria	Nem védett / indifferens
Hellinsia lienigianus	Nem védett / indifferens
Ovis aries	Nem védett / indifferens
Ovis aries musimon	Nem védett / indifferens
Oxicesta geographica	Nem védett / indifferens
Oxidus gracilis	Nem védett / indifferens
Oxybelus argentatus	Nem védett / indifferens
Oxybelus argentatus gerstaeckeri	Nem védett / indifferens
Oxybelus argentatus treforti	Nem védett / indifferens
Oxybelus aurantiacus	Nem védett / indifferens
Oxybelus bipunctatus	Nem védett / indifferens
Oxybelus dissectus	Nem védett / indifferens
Oxybelus dissectus elegans	Nem védett / indifferens
Oxybelus latidens	Nem védett / indifferens
Oxybelus latro	Nem védett / indifferens
Oxybelus lineatus	Nem védett / indifferens
Oxybelus maculipes	Nem védett / indifferens
Oxybelus mandibularis	Nem védett / indifferens
Oxybelus mucronatus	Nem védett / indifferens
Oxybelus quattuordecimnotatus	Nem védett / indifferens
Oxybelus subspinosus	Nem védett / indifferens
Oxybelus trispinosus	Nem védett / indifferens
Oxybelus uniglumis	Nem védett / indifferens
Oxybelus variegatus	Nem védett / indifferens
Oxybelus victor	Nem védett / indifferens
Oxycarenus lavaterae	Nem védett / indifferens
Oxycarenus modestus	Nem védett / indifferens
Oxycarenus pallens	Nem védett / indifferens
Oxychilus draparnaudi	Nem védett / indifferens
Oxychilus translucidus	Nem védett / indifferens
Oxyethira falcata	Nem védett / indifferens
Oxyethira flavicornis	Nem védett / indifferens
Oxyethira tristella	Nem védett / indifferens
Oxylaemus cylindricus	Nem védett / indifferens
Oxyloma elegans	Nem védett / indifferens
Oxymirus cursor	Nem védett / indifferens
Oxyomus sylvestris	Nem védett / indifferens
Oxyopes heterophthalmus	Nem védett / indifferens
Oxyopes lineatus	Nem védett / indifferens
Oxyopes ramosus	Nem védett / indifferens
Oxypoda abdominalis	Nem védett / indifferens
Oxypoda acuminata	Nem védett / indifferens
Oxypoda alternans	Nem védett / indifferens
Oxypoda annularis	Nem védett / indifferens
Oxypoda arborea	Nem védett / indifferens
Oxypoda brachyptera	Nem védett / indifferens
Oxypoda brevicornis	Nem védett / indifferens
Oxypoda carbonaria	Nem védett / indifferens
Oxypoda doderoi	Nem védett / indifferens
Oxypoda exoleta	Nem védett / indifferens
Oxypoda ferruginea	Nem védett / indifferens
Oxypoda filiformis	Nem védett / indifferens
Oxypoda flavicornis	Nem védett / indifferens
Oxypoda formiceticola	Nem védett / indifferens
Oxypoda formosa	Nem védett / indifferens
Oxypoda haemorrhoa	Nem védett / indifferens
Oxypoda induta	Nem védett / indifferens
Oxypoda longipes	Nem védett / indifferens
Oxypoda miranda	Nem védett / indifferens
Oxypoda mutata	Nem védett / indifferens
Oxypoda opaca	Nem védett / indifferens
Oxypoda praecox	Nem védett / indifferens
Oxypoda rufa	Nem védett / indifferens
Oxypoda soror	Nem védett / indifferens
Oxypoda spaethi	Nem védett / indifferens
Oxypoda spectabilis	Nem védett / indifferens
Oxypoda togata	Nem védett / indifferens
Oxypoda vicina	Nem védett / indifferens
Oxypoda vittata	Nem védett / indifferens
Oxyporus maxillosus	Nem védett / indifferens
Oxyporus rufus	Nem védett / indifferens
Oxypselaphus obscurus	Nem védett / indifferens
Oxyptilus chrysodactyla	Nem védett / indifferens
Oxyptilus parvidactyla	Nem védett / indifferens
Oxyptilus pilosellae	Nem védett / indifferens
Oxystoma cerdo	Nem védett / indifferens
Oxystoma craccae	Nem védett / indifferens
Oxystoma dimidiatum	Nem védett / indifferens
Oxystoma ochropus	Nem védett / indifferens
Oxystoma opeticum	Nem védett / indifferens
Oxystoma pomonae	Nem védett / indifferens
Oxystoma subulatum	Nem védett / indifferens
Oxytelus fulvipes	Nem védett / indifferens
Oxytelus laqueatus	Nem védett / indifferens
Oxytelus migrator	Nem védett / indifferens
Oxytelus piceus	Nem védett / indifferens
Oxytelus sculptus	Nem védett / indifferens
Oxythyrea funesta	Nem védett / indifferens
Oxytripia orbiculosa	Fokozottan védett
Oxyura jamaicensis	Nem védett / indifferens
Oxyura leucocephala	Fokozottan védett
Oxyurella tenuicaudis	Nem védett / indifferens
Ozyptila atomaria	Nem védett / indifferens
Cozyptila blackwalli	Nem védett / indifferens
Ozyptila brevipes	Nem védett / indifferens
Ozyptila claveata	Nem védett / indifferens
Ozyptila praticola	Nem védett / indifferens
Ozyptila pullata	Nem védett / indifferens
Ozyptila rauda	Nem védett / indifferens
Ozyptila sanctuaria	Nem védett / indifferens
Ozyptila scabricula	Nem védett / indifferens
Ozyptila simplex	Nem védett / indifferens
Ozyptila trux	Nem védett / indifferens
Pachetra sagittigera	Nem védett / indifferens
Pachnida nigella	Nem védett / indifferens
Pachyatheta cribrata	Nem védett / indifferens
Pachybrachius fracticollis	Nem védett / indifferens
Pachybrachius luridus	Nem védett / indifferens
Pachycerus madidus	Nem védett / indifferens
Pachycnemia hippocastanaria	Nem védett / indifferens
Pachygnatha clercki	Nem védett / indifferens
Pachygnatha degeeri	Nem védett / indifferens
Pachygnatha listeri	Nem védett / indifferens
Pachylister inaequalis	Nem védett / indifferens
Pachymerium ferrugineum	Nem védett / indifferens
Pachypodoiulus eurypus	Nem védett / indifferens
Pachyrhinus squamulosus	Nem védett / indifferens
Pachyta lamed	Nem védett / indifferens
Pachyta quadrimaculata	Nem védett / indifferens
Pachythelia villosella	Nem védett / indifferens
Pachytodes cerambyciformis	Nem védett / indifferens
Pachytodes erraticus	Nem védett / indifferens
Pachytomella parallela	Nem védett / indifferens
Pachytrachis gracilis	Nem védett / indifferens
Pachytychius sparsutus	Nem védett / indifferens
Pactolinus major	Nem védett / indifferens
Paederidus carpathicola	Nem védett / indifferens
Paederidus rubrothoracicus	Nem védett / indifferens
Paederidus ruficollis	Nem védett / indifferens
Paederus balcanicus	Nem védett / indifferens
Paederus brevipennis	Nem védett / indifferens
Paederus caligatus	Nem védett / indifferens
Paederus fuscipes	Nem védett / indifferens
Paederus limnophilus	Nem védett / indifferens
Paederus littoralis	Nem védett / indifferens
Paederus riparius	Nem védett / indifferens
Paederus schoenherri	Nem védett / indifferens
Pagiphora annulata	Nem védett / indifferens
Pagodulina pagodula	Védett
Pagodulina pagodula altilis	Nem védett / indifferens
Paidia rica	Védett
Paidiscura pallens	Nem védett / indifferens
Palaeolecanium bituberculatum	Nem védett / indifferens
Palarus variegatus	Nem védett / indifferens
Paleococcus fuscipennis	Nem védett / indifferens
Palingenia longicauda	Védett
Palliduphantes alutacius	Nem védett / indifferens
Palliduphantes istrianus	Nem védett / indifferens
Palliduphantes liguricus	Nem védett / indifferens
Palliduphantes pallidus	Nem védett / indifferens
Palliduphantes pillichi	Nem védett / indifferens
Ovalisia festiva	Védett
Palmitia massilialis	Védett
Palmodes occitanicus	Nem védett / indifferens
Palomena prasina	Nem védett / indifferens
Palomena viridissima	Nem védett / indifferens
Palorus depressus	Nem védett / indifferens
Palorus ratzeburgi	Nem védett / indifferens
Palorus subdepressus	Nem védett / indifferens
Palpares libelluloides	Nem védett / indifferens
Palpita vitrealis	Nem védett / indifferens
Paluda flaveola	Nem védett / indifferens
Pammene agnotana	Nem védett / indifferens
Pammene albuginana	Nem védett / indifferens
Pammene amygdalana	Nem védett / indifferens
Pammene argyrana	Nem védett / indifferens
Pammene aurana	Nem védett / indifferens
Pammene aurita	Nem védett / indifferens
Pammene christophana	Nem védett / indifferens
Pammene fasciana	Nem védett / indifferens
Pammene gallicana	Nem védett / indifferens
Pammene gallicolana	Nem védett / indifferens
Pammene germmana	Nem védett / indifferens
Pammene giganteana	Nem védett / indifferens
Pammene insulana	Nem védett / indifferens
Pammene obscurana	Nem védett / indifferens
Pammene ochsenheimeriana	Nem védett / indifferens
Pammene querceti	Védett
Pammene regiana	Nem védett / indifferens
Pammene rhediella	Nem védett / indifferens
Pammene spiniana	Nem védett / indifferens
Pammene splendidulana	Nem védett / indifferens
Pammene suspectana	Nem védett / indifferens
Pammene trauniana	Nem védett / indifferens
Panagaeus bipustulatus	Nem védett / indifferens
Panagaeus cruxmajor	Nem védett / indifferens
Panamomops affinis	Nem védett / indifferens
Panamomops fagei	Nem védett / indifferens
Panamomops mengei	Nem védett / indifferens
Panamomops sulcifrons	Nem védett / indifferens
Panaorus adspersus	Nem védett / indifferens
Pancalia leuwenhoekella	Nem védett / indifferens
Pancalia schwarzella	Nem védett / indifferens
Panchrysia deaurata	Védett
Pandemis cerasana	Nem védett / indifferens
Pandemis cinnamomeana	Nem védett / indifferens
Pandemis corylana	Nem védett / indifferens
Pandemis dumetana	Nem védett / indifferens
Pandemis heparana	Nem védett / indifferens
Pandion haliaetus	Fokozottan védett
Panemeria tenebrata	Nem védett / indifferens
Pangonius pyritosus	Nem védett / indifferens
Pangus scaritides	Nem védett / indifferens
Panolis flammea	Nem védett / indifferens
Pantacordis pales	Nem védett / indifferens
Pantallus alboniger	Nem védett / indifferens
Panthea coenobita	Nem védett / indifferens
Pantilius tunicatus	Nem védett / indifferens
Panurginus labiatus	Nem védett / indifferens
Panurgus calcaratus	Nem védett / indifferens
Panurgus dentipes	Nem védett / indifferens
Panurus biarmicus	Védett
Panurus biarmicus russicus	Védett
Paophilus afflatus	Nem védett / indifferens
Paophilus hampei	Nem védett / indifferens
Papilio machaon	Védett
Paraboarmia viertlii	Védett
Paracaloptenus caloptenoides	Fokozottan védett
Paracandona euplectella	Nem védett / indifferens
Paracardiophorus musculus	Nem védett / indifferens
Parachiona picicornis	Nem védett / indifferens
Parachronistis albiceps	Nem védett / indifferens
Paracolax tristalis	Nem védett / indifferens
Paracorixa concinna	Nem védett / indifferens
Paracorsia repandalis	Nem védett / indifferens
Paracorymbia fulva	Nem védett / indifferens
Paracorymbia maculicornis	Nem védett / indifferens
Paracyclops affinis	Nem védett / indifferens
Paracyclops fimbriatus	Nem védett / indifferens
Paracyclops imminutus	Nem védett / indifferens
Paracyclops poppei	Nem védett / indifferens
Paracylindromorphus subuliformis	Nem védett / indifferens
Paradarisa consonaria	Nem védett / indifferens
Paradelphacodes paludosus	Nem védett / indifferens
Paradorydium paradoxum	Nem védett / indifferens
Paradrina clavipalpis	Nem védett / indifferens
Paradromius linearis	Nem védett / indifferens
Paradromius longiceps	Nem védett / indifferens
Parafairmairia bipartita	Nem védett / indifferens
Parafairmairia gracilis	Nem védett / indifferens
Parafomoria helianthemella	Nem védett / indifferens
Parafoucartia squamulata	Nem védett / indifferens
Paragus albifrons	Nem védett / indifferens
Paragus bicolor	Nem védett / indifferens
Paragus cinctus	Nem védett / indifferens
Paragus finitimus	Nem védett / indifferens
Paragus haemorrhous	Nem védett / indifferens
Paragus majoranae	Nem védett / indifferens
Paragus medeae	Nem védett / indifferens
Paragus quadrifasciatus	Nem védett / indifferens
Paragus tibialis	Nem védett / indifferens
Paragymnomerus spiricornis	Nem védett / indifferens
Parahypopta caestrum	Nem védett / indifferens
Parainocellia braueri	Védett
Paraleptophlebia cincta	Nem védett / indifferens
Paraleptophlebia submarginata	Nem védett / indifferens
Paraleptophlebia werneri	Nem védett / indifferens
Paraliburnia adela	Nem védett / indifferens
Paralimnus phragmitis	Nem védett / indifferens
Paralimnus rotundiceps	Nem védett / indifferens
Paralimnus zachvatkini	Nem védett / indifferens
Paralipsa gularis	Nem védett / indifferens
Leucoptera sinuella	Nem védett / indifferens
Paramecosoma melanocephalum	Nem védett / indifferens
Parameotica hydra	Nem védett / indifferens
Parameotica laticeps	Nem védett / indifferens
Paramesia gnomana	Nem védett / indifferens
Paramesus major	Nem védett / indifferens
Paramesus obtusifrons	Nem védett / indifferens
Paramesus taeniatus	Nem védett / indifferens
Parammobatodes minutus	Nem védett / indifferens
Paranchus albipes	Nem védett / indifferens
Paranthrene insolita polonica	Nem védett / indifferens
Paranthrene tabaniformis	Nem védett / indifferens
Paraphotistus impressus	Nem védett / indifferens
Paraphotistus nigricornis	Nem védett / indifferens
Mecostethus parapleurus	Nem védett / indifferens
Parapotes reticulatus	Nem védett / indifferens
Parapoynx nivalis	Nem védett / indifferens
Parapoynx stratiotata	Nem védett / indifferens
Parapsallus vitellinus	Nem védett / indifferens
Pararge aegeria	Nem védett / indifferens
Parascotia fuliginaria	Nem védett / indifferens
Parascythris muelleri	Nem védett / indifferens
Parasemia plantaginis	Védett
Parasemidalis fuscipennis	Nem védett / indifferens
Parasetodes respersellus	Nem védett / indifferens
Parastenocaris budapestiensis	Nem védett / indifferens
Parastenocaris entzii	Nem védett / indifferens
Parastenocaris pannonicus	Nem védett / indifferens
Parastenocaris similis	Nem védett / indifferens
Parastenocaris törökae	Nem védett / indifferens
Parastichtis suspecta	Nem védett / indifferens
Parastichtis ypsillon	Nem védett / indifferens
Paraswammerdamia nebulella	Nem védett / indifferens
Parasyrphus annulatus	Nem védett / indifferens
Parasyrphus lineola	Nem védett / indifferens
Parasyrphus macularis	Nem védett / indifferens
Parasyrphus malinellus	Nem védett / indifferens
Parasyrphus nigritarsis	Nem védett / indifferens
Parasyrphus punctulatus	Nem védett / indifferens
Parasyrphus vittiger	Nem védett / indifferens
Paratachys bistriatus	Nem védett / indifferens
Paratachys fulvicollis	Nem védett / indifferens
Paratachys micros	Nem védett / indifferens
Paratachys turkestanicus	Nem védett / indifferens
Paratalanta hyalinalis	Nem védett / indifferens
Paratalanta pandalis	Nem védett / indifferens
Parazuphium chevrolati praepannonicum	Nem védett / indifferens
Parazygiella montana	Nem védett / indifferens
Pardosa agrestis	Nem védett / indifferens
Pardosa agricola	Nem védett / indifferens
Pardosa alacris	Nem védett / indifferens
Pardosa amentata	Nem védett / indifferens
Pardosa bifasciata	Nem védett / indifferens
Pardosa cribrata	Nem védett / indifferens
Pardosa hortensis	Nem védett / indifferens
Pardosa lugubris	Nem védett / indifferens
Pardosa maisa	Nem védett / indifferens
Pardosa monticola	Nem védett / indifferens
Pardosa morosa	Nem védett / indifferens
Pardosa nebulosa	Nem védett / indifferens
Pardosa paludicola	Nem védett / indifferens
Pardosa palustris	Nem védett / indifferens
Pardosa prativaga	Nem védett / indifferens
Pardosa proxima	Nem védett / indifferens
Pardosa pullata	Nem védett / indifferens
Pardosa riparia	Nem védett / indifferens
Parectopa ononidis	Nem védett / indifferens
Parectopa robiniella	Nem védett / indifferens
Parectropis similaria	Nem védett / indifferens
Paredrocoris pectoralis	Nem védett / indifferens
Parergasilus rylovi	Nem védett / indifferens
Parethelcus pollinarius	Nem védett / indifferens
Pareulype berberata	Nem védett / indifferens
Parexarnis fugax	Védett
Parhelophilus frutetorum	Nem védett / indifferens
Parhelophilus versicolor	Nem védett / indifferens
Parlatoria oleae	Nem védett / indifferens
Parnassius apollo	Nem védett / indifferens
Parnassius mnemosyne	Védett
Parnopes grandior	Védett
Parocyusa longitarsis	Nem védett / indifferens
Parocyusa rubicunda	Nem védett / indifferens
Parodontodynerus ephippium	Nem védett / indifferens
Oecetis struckii	Nem védett / indifferens
Paromalus flavicornis	Nem védett / indifferens
Paromalus parallelepipedus	Nem védett / indifferens
Parophonus complanatus	Nem védett / indifferens
Parophonus hirsutulus	Nem védett / indifferens
Parophonus maculicornis	Nem védett / indifferens
Parophonus mendax	Nem védett / indifferens
Parornix anglicella	Nem védett / indifferens
Parornix anguliferella	Nem védett / indifferens
Parornix betulae	Nem védett / indifferens
Parornix carpinella	Nem védett / indifferens
Parornix devoniella	Nem védett / indifferens
Parornix fagivora	Nem védett / indifferens
Parornix finitimella	Nem védett / indifferens
Parornix petiolella	Nem védett / indifferens
Parornix scoticella	Nem védett / indifferens
Parornix szocsi	Nem védett / indifferens
Parornix tenella	Nem védett / indifferens
Parornix torquillella	Nem védett / indifferens
Parthenolecanium corni	Nem védett / indifferens
Parthenolecanium fletcheri	Nem védett / indifferens
Parthenolecanium persicae	Nem védett / indifferens
Parthenolecanium pomeranicum	Nem védett / indifferens
Parthenolecanium rufulum	Nem védett / indifferens
Parthenolecanium smreczynskii	Nem védett / indifferens
Parus ater	Védett
Parus caeruleus	Védett
Parus cristatus	Védett
Parus palustris	Védett
Parus palustris stagnatilis	Védett
Pasiphila chloerata	Nem védett / indifferens
Pasiphila debiliata	Nem védett / indifferens
Pasiphila rectangulata	Nem védett / indifferens
Pasites maculatus	Nem védett / indifferens
Passaloecus clypealis	Nem védett / indifferens
Passaloecus corniger	Nem védett / indifferens
Passaloecus gracilis	Nem védett / indifferens
Passaloecus insignis	Nem védett / indifferens
Passaloecus singularis	Nem védett / indifferens
Passer domesticus	Védett
Passer montanus	Védett
Pastiroma clypeata	Nem védett / indifferens
Pastiroma odessana	Nem védett / indifferens
Patrobus atrorufus	Nem védett / indifferens
Patrobus styriacus	Nem védett / indifferens
Polypogon strigilata	Nem védett / indifferens
Pedestredorcadion decipiens	Védett
Pedestredorcadion pedestre	Nem védett / indifferens
Pedestredorcadion scopolii	Nem védett / indifferens
Pediacus depressus	Nem védett / indifferens
Pediacus dermestoides	Nem védett / indifferens
Pediasia aridella	Nem védett / indifferens
Pediasia contaminella	Nem védett / indifferens
Pediasia fascelinella	Nem védett / indifferens
Pediasia jucundellus	Nem védett / indifferens
Pediasia luteella	Nem védett / indifferens
Pediasia matricella	Nem védett / indifferens
Pediculota montandoni	Nem védett / indifferens
Pedilophorus auratus	Nem védett / indifferens
Pedilophorus obscurus	Nem védett / indifferens
Pedilophorus senii	Nem védett / indifferens
Pedinus fallax	Nem védett / indifferens
Pedinus fallax gracilis	Nem védett / indifferens
Pedinus femoralis	Nem védett / indifferens
Pedinus hungaricus	Védett
Pediopsis tiliae	Nem védett / indifferens
Pedostrangalia revestita	Nem védett / indifferens
Pelatea klugiana	Nem védett / indifferens
Pelecanus crispus	Fokozottan védett
Pelecanus onocrotalus	Fokozottan védett
Pelecanus rufescens	Nem védett / indifferens
Pelecocera latifrons	Nem védett / indifferens
Pelecocera tricincta	Nem védett / indifferens
Pelecopsis elongata	Nem védett / indifferens
Pelecopsis loksai	Nem védett / indifferens
Pelecopsis mengei	Nem védett / indifferens
Pelecopsis parallela	Nem védett / indifferens
Pelecopsis radicicola	Nem védett / indifferens
Pelecotoma fennica	Nem védett / indifferens
Pelecus cultratus	Nem védett / indifferens
Pelenomus canaliculatus	Nem védett / indifferens
Pelenomus commari	Nem védett / indifferens
Pelenomus quadricorniger	Nem védett / indifferens
Pelenomus quadrituberculatus	Nem védett / indifferens
Pelenomus velaris	Nem védett / indifferens
Pelenomus waltoni	Nem védett / indifferens
Peliococcopsis parviceraria	Nem védett / indifferens
Peliococcus balteatus	Nem védett / indifferens
Peliococcus perfidiosus	Nem védett / indifferens
Peliococcus slavonicus	Nem védett / indifferens
Pellenes nigrociliatus	Nem védett / indifferens
Pellenes tripunctatus	Nem védett / indifferens
Pelobates fuscus	Védett
Pelochares versicolor	Nem védett / indifferens
Pelochrista arabescana	Nem védett / indifferens
Pelochrista caecimaculana	Nem védett / indifferens
Pelochrista decolorana	Nem védett / indifferens
Pelochrista hepatariana	Nem védett / indifferens
Pelochrista infidana	Nem védett / indifferens
Pelochrista latericiana	Nem védett / indifferens
Pelochrista modicana	Nem védett / indifferens
Pelochrista mollitana	Nem védett / indifferens
Pelochrista subtiliana	Nem védett / indifferens
Pelosia muscerda	Nem védett / indifferens
Pelosia obtusa	Nem védett / indifferens
Peltodytes caesus	Nem védett / indifferens
Pelurga comitata	Nem védett / indifferens
Pempelia albariella	Nem védett / indifferens
Pempelia formosa	Nem védett / indifferens
Pempelia geminella	Nem védett / indifferens
Pempelia obductella	Nem védett / indifferens
Pempelia palumbella	Nem védett / indifferens
Pempeliella dilutella	Nem védett / indifferens
Pempeliella ornatella	Nem védett / indifferens
Pempeliella sororiella	Nem védett / indifferens
Pemphredon austriacus	Nem védett / indifferens
Pemphredon balticus	Nem védett / indifferens
Pemphredon clypealis	Nem védett / indifferens
Pemphredon inornatus	Nem védett / indifferens
Pemphredon lethifer	Nem védett / indifferens
Pemphredon lugens	Nem védett / indifferens
Pemphredon lugubris	Nem védett / indifferens
Pemphredon montanus	Nem védett / indifferens
Pemphredon morio	Nem védett / indifferens
Pemphredon podagricus	Nem védett / indifferens
Pemphredon rugifer	Nem védett / indifferens
Pennisetia hylaeiformis	Nem védett / indifferens
Pentaphyllus chrysomeloides	Nem védett / indifferens
Pentaphyllus testaceus	Nem védett / indifferens
Pentaria badia	Nem védett / indifferens
Pentastira major	Nem védett / indifferens
Pentastira rorida	Nem védett / indifferens
Pentastiridius fumatipennis	Nem védett / indifferens
Pentastiridius leporinus	Nem védett / indifferens
Pentastiridius pallens	Nem védett / indifferens
Pentatoma rufipes	Nem védett / indifferens
Penthimia nigra	Nem védett / indifferens
Penthophera morio	Nem védett / indifferens
Pentodon idiota	Nem védett / indifferens
Peponocranium orbiculatum	Nem védett / indifferens
Perapion affine	Nem védett / indifferens
Perapion curtirostre	Nem védett / indifferens
Perapion lemoroi	Nem védett / indifferens
Perapion marchicum	Nem védett / indifferens
Perapion oblongum	Nem védett / indifferens
Perapion violaceum	Nem védett / indifferens
Perca fluviatilis	Nem védett / indifferens
Perccottus glenii	Inváziós
Perconia strigillaria	Védett
Perdix perdix	Nem védett / indifferens
Perforatella bidentata	Védett
Perforatella dibothrion	Védett
Peribatodes rhomboidaria	Nem védett / indifferens
Peribatodes secundaria	Nem védett / indifferens
Peribatodes umbraria	Védett
Pericallia matronula	Fokozottan védett
Pericartiellus telephii	Nem védett / indifferens
Periclepsis cinctana	Nem védett / indifferens
Peridea anceps	Nem védett / indifferens
Peridroma saucia	Nem védett / indifferens
Perigona nigriceps	Nem védett / indifferens
Perigrapha i-cinctum	Nem védett / indifferens
Perileptus areolatus	Nem védett / indifferens
Perinephela lancealis	Nem védett / indifferens
Periphanes delphinii	Védett
Peritelus familiaris	Nem védett / indifferens
Peritrechus geniculatus	Nem védett / indifferens
Peritrechus gracilicornis	Nem védett / indifferens
Peritrechus lundii	Nem védett / indifferens
Peritrechus meridionalis	Nem védett / indifferens
Peritrechus nubilus	Nem védett / indifferens
Perittia herrichiella	Nem védett / indifferens
Perizoma affinitata	Nem védett / indifferens
Perizoma albulata	Nem védett / indifferens
Perizoma alchemillata	Nem védett / indifferens
Perizoma bifaciata	Nem védett / indifferens
Perizoma blandiata	Nem védett / indifferens
Perizoma flavofasciata	Nem védett / indifferens
Perizoma hydrata	Nem védett / indifferens
Perizoma lugdunaria	Nem védett / indifferens
Perizoma minorata	Védett
Perla bipunctata	Nem védett / indifferens
Perla burmeisteriana	Nem védett / indifferens
Perla marginata	Nem védett / indifferens
Perla pallida	Nem védett / indifferens
Perlodes dispar	Nem védett / indifferens
Perlodes microcephalus	Nem védett / indifferens
Pernis apivorus	Fokozottan védett
Petasina bakowskii	Nem védett / indifferens
Petasina filicina	Nem védett / indifferens
Petasina unidentata	Védett
Petrophora chlorosata	Nem védett / indifferens
Pexicopia malvella	Nem védett / indifferens
Peyerimhoffina gracilis	Nem védett / indifferens
Pezotettix giornae	Nem védett / indifferens
Phacophallus parumpunctatus	Nem védett / indifferens
Phaenops cyanea	Nem védett / indifferens
Phaenops formaneki	Nem védett / indifferens
Phaeocedus braccatus	Nem védett / indifferens
Phaeochrotes pudens	Nem védett / indifferens
Phaeostigma major	Nem védett / indifferens
Phaeostigma notata	Nem védett / indifferens
Phaeostigma setulosa	Nem védett / indifferens
Phaiogramma etruscaria	Nem védett / indifferens
Phalacrocorax carbo	Védett
Phalacrocorax carbo sinensis	Védett
Phalacrocorax pygmeus	Fokozottan védett
Phalacronothus biguttatus	Nem védett / indifferens
Phalacronothus citellorum	Nem védett / indifferens
Euorodalus paracoenosus	Nem védett / indifferens
Esymus pusillus	Nem védett / indifferens
Eudolus quadriguttatus	Nem védett / indifferens
Phalacronothus quadrimaculatus	Nem védett / indifferens
Phalacrus caricis	Nem védett / indifferens
Phalacrus championi	Nem védett / indifferens
Phalacrus corruscus	Nem védett / indifferens
Phalacrus fimetarius	Nem védett / indifferens
Phalacrus grossus	Nem védett / indifferens
Phalacrus substriatus	Nem védett / indifferens
Phalaropus fulicarius	Védett
Phalaropus lobatus	Védett
Phalera bucephala	Nem védett / indifferens
Phalera bucephaloides	Védett
Phalonidia affinitana	Nem védett / indifferens
Phalonidia albipalpana	Nem védett / indifferens
Phalonidia contractana	Nem védett / indifferens
Phalonidia curvistrigana	Nem védett / indifferens
Phalonidia gilvicomana	Nem védett / indifferens
Phalonidia manniana	Nem védett / indifferens
Phaneroptera falcata	Nem védett / indifferens
Phaneroptera nana	Nem védett / indifferens
Phaneta pauperana	Nem védett / indifferens
Phantia subquadrata	Nem védett / indifferens
Pharmacis carna	Nem védett / indifferens
Pharmacis fusconebulosa	Védett
Phasianus colchicus	Nem védett / indifferens
Phasianus colchicus torquatus	Nem védett / indifferens
Phaulernis rebeliella	Nem védett / indifferens
Pheletes aeneoniger	Nem védett / indifferens
Pheletes quercus	Nem védett / indifferens
Phenacoccus aceris	Nem védett / indifferens
Phenacoccus avenae	Nem védett / indifferens
Phenacoccus bicerarius	Nem védett / indifferens
Phenacoccus evelinae	Nem védett / indifferens
Phenacoccus hordei	Nem védett / indifferens
Phenacoccus incertus	Nem védett / indifferens
Phenacoccus interruptus	Nem védett / indifferens
Phenacoccus mespili	Nem védett / indifferens
Phenacoccus phenacoccoides	Nem védett / indifferens
Phenacoccus piceae	Nem védett / indifferens
Phenacoccus pumilus	Nem védett / indifferens
Pheosia gnoma	Védett
Pheosia tremula	Nem védett / indifferens
Phiaris metallicana	Nem védett / indifferens
Phiaris micana	Nem védett / indifferens
Phiaris obsoletana	Nem védett / indifferens
Phiaris scoriana	Nem védett / indifferens
Phiaris stibiana	Nem védett / indifferens
Phiaris umbrosana	Nem védett / indifferens
Phibalapteryx virgata	Nem védett / indifferens
Philaenus spumarius	Nem védett / indifferens
Philaeus chrysops	Nem védett / indifferens
Philaia jassargiforma	Nem védett / indifferens
Philanthus coronatus	Nem védett / indifferens
Philanthus triangulum	Nem védett / indifferens
Philanthus venustus	Nem védett / indifferens
Philedone gerningana	Nem védett / indifferens
Philedonides lunana	Nem védett / indifferens
Philedonides rhombicana	Nem védett / indifferens
Philereme transversata	Nem védett / indifferens
Philereme vetulata	Nem védett / indifferens
Philhygra balcanicola	Nem védett / indifferens
Philhygra elongatula	Nem védett / indifferens
Philhygra hygrobia	Nem védett / indifferens
Philhygra hygrotopora	Nem védett / indifferens
Philhygra kaiseriana	Nem védett / indifferens
Philhygra luridipennis	Nem védett / indifferens
Philhygra malleus	Nem védett / indifferens
Philhygra melanocera	Nem védett / indifferens
Philhygra nannion	Nem védett / indifferens
Philhygra palustris	Nem védett / indifferens
Philhygra scotica	Nem védett / indifferens
Philhygra sequanica	Nem védett / indifferens
Philhygra terminalis	Nem védett / indifferens
Philhygra tmolosensis	Nem védett / indifferens
Philhygra vindobonsensis	Nem védett / indifferens
Philhygra volans	Nem védett / indifferens
Philipomyia aprica	Nem védett / indifferens
Philipomyia graeca	Nem védett / indifferens
Philodromus albidus	Nem védett / indifferens
Philodromus aureolus	Nem védett / indifferens
Philodromus buxi	Nem védett / indifferens
Philodromus cespitum	Nem védett / indifferens
Philodromus collinus	Nem védett / indifferens
Philodromus dispar	Nem védett / indifferens
Philodromus emarginatus	Nem védett / indifferens
Philodromus fallax	Nem védett / indifferens
Philodromus fuscomargitatus	Nem védett / indifferens
Philodromus histrio	Nem védett / indifferens
Philodromus longipalpis	Nem védett / indifferens
Philodromus margaritatus	Nem védett / indifferens
Philodromus poecilus	Nem védett / indifferens
Philodromus praedatus	Nem védett / indifferens
Philodromus rufus	Nem védett / indifferens
Philomachus pugnax	Védett
Philonthus addendus	Nem védett / indifferens
Philonthus alberti	Nem védett / indifferens
Philonthus albipes	Nem védett / indifferens
Philonthus alpinus	Nem védett / indifferens
Philonthus atratus	Nem védett / indifferens
Philonthus carbonarius	Nem védett / indifferens
Philonthus caucasicus	Nem védett / indifferens
Philonthus cochleatus	Nem védett / indifferens
Philonthus cognatus	Nem védett / indifferens
Philonthus concinnus	Nem védett / indifferens
Philonthus confinis	Nem védett / indifferens
Philonthus coprophilus	Nem védett / indifferens
Philonthus corruscus	Nem védett / indifferens
Philonthus corvinus	Nem védett / indifferens
Philonthus cruentatus	Nem védett / indifferens
Philonthus cyanipennis	Nem védett / indifferens
Philonthus debilis	Nem védett / indifferens
Philonthus decorus	Nem védett / indifferens
Philonthus discoideus	Nem védett / indifferens
Philonthus diversiceps	Nem védett / indifferens
Philonthus ebeninus	Nem védett / indifferens
Philonthus frigidus	Nem védett / indifferens
Philonthus fumarius	Nem védett / indifferens
Philonthus intermedius	Nem védett / indifferens
Philonthus laevicollis	Nem védett / indifferens
Philonthus laminatus	Nem védett / indifferens
Philonthus lepidus	Nem védett / indifferens
Philonthus longicornis	Nem védett / indifferens
Philonthus mannerheimi	Nem védett / indifferens
Philonthus marginatus	Nem védett / indifferens
Philonthus micans	Nem védett / indifferens
Philonthus nigrita	Nem védett / indifferens
Philonthus nitidicollis	Nem védett / indifferens
Philonthus nitidus	Nem védett / indifferens
Philonthus parvicornis	Nem védett / indifferens
Philonthus politus	Nem védett / indifferens
Philonthus puella	Nem védett / indifferens
Philonthus punctus	Nem védett / indifferens
Philonthus quisquiliarius	Nem védett / indifferens
Philonthus rectangulus	Nem védett / indifferens
Philonthus rotundicollis	Nem védett / indifferens
Philonthus rubripennis	Nem védett / indifferens
Philonthus rufimanus	Nem védett / indifferens
Philonthus rufipes	Nem védett / indifferens
Philonthus salinus	Nem védett / indifferens
Philonthus sanguinolentus	Nem védett / indifferens
Philonthus spinipes	Nem védett / indifferens
Philonthus splendens	Nem védett / indifferens
Philonthus succicola	Nem védett / indifferens
Philonthus tenuicornis	Nem védett / indifferens
Philonthus umbratilis	Nem védett / indifferens
Philonthus varians	Nem védett / indifferens
Philonthus ventralis	Nem védett / indifferens
Philonthus viridipennis	Nem védett / indifferens
Philopedon plagiatum	Nem védett / indifferens
Philopotamus ludificatus	Nem védett / indifferens
Philopotamus montanus	Nem védett / indifferens
Philopotamus variegatus	Nem védett / indifferens
Philorhizus melanocephalus	Nem védett / indifferens
Philorhizus notatus	Nem védett / indifferens
Philorhizus quadrisignatus	Nem védett / indifferens
Philorhizus sigma	Nem védett / indifferens
Philothermus evanescens	Nem védett / indifferens
Philothermus semistriatus	Nem védett / indifferens
Phimodera amblygonia	Nem védett / indifferens
Phimodera flori	Nem védett / indifferens
Phimodera humeralis	Nem védett / indifferens
Phintella castriesiana	Nem védett / indifferens
Phlegra cinereofasciata	Nem védett / indifferens
Phlegra fasciata	Nem védett / indifferens
Phlepsius intricatus	Nem védett / indifferens
Phloeocharis subtilissima	Nem védett / indifferens
Phloeonomus minimus	Nem védett / indifferens
Phloeonomus punctipennis	Nem védett / indifferens
Phloeonomus pusillus	Nem védett / indifferens
Phloeophagus lignarius	Nem védett / indifferens
Phloeophagus thompsoni	Nem védett / indifferens
Phloeophagus turbatus	Nem védett / indifferens
Phloeotribus rhododactylus	Nem védett / indifferens
Phloeopora corticalis	Nem védett / indifferens
Phloeopora nitidiventris	Nem védett / indifferens
Phloeopora opaca	Nem védett / indifferens
Phloeopora scribae	Nem védett / indifferens
Phloeopora teres	Nem védett / indifferens
Phloeopora testacea	Nem védett / indifferens
Phloeosinus aubei	Nem védett / indifferens
Phloeosinus thujae	Nem védett / indifferens
Phloeostiba lapponica	Nem védett / indifferens
Phloeostiba plana	Nem védett / indifferens
Phloeostichus denticollis	Nem védett / indifferens
Phloeotribus caucasicus	Nem védett / indifferens
Phloeotribus scarabaeoides	Nem védett / indifferens
Phlogophora meticulosa	Nem védett / indifferens
Phlogophora scita	Védett
Phlogotettix cyclops	Nem védett / indifferens
Phloiophilus edwardsi	Nem védett / indifferens
Phloiotrya tenuis	Nem védett / indifferens
Phlyctaenia coronata	Nem védett / indifferens
Phlyctaenia perlucidalis	Nem védett / indifferens
Phlyctaenia stachydalis	Nem védett / indifferens
Phlyctaenodes cruentalis	Nem védett / indifferens
Phoenicocoris modestus	Nem védett / indifferens
Phoenicocoris obscurellus	Nem védett / indifferens
Phoenicopterus minor	Nem védett / indifferens
Phoenicopterus ruber	Védett
Phoenicopterus roseus	Védett
Phoenicurus ochruros	Védett
Phoenicurus ochruros gibraltariensis	Védett
Phoenicurus phoenicurus	Védett
Platnickina tincta	Nem védett / indifferens
Pholcus opilionoides	Nem védett / indifferens
Pholcus phalangioides	Nem védett / indifferens
Pholidoptera aptera	Nem védett / indifferens
Pholidoptera fallax	Nem védett / indifferens
Pholidoptera griseoaptera	Nem védett / indifferens
Pholidoptera littoralis	Védett
Pholidoptera transsylvanica	Védett
Pholioxenus schatzmayri	Nem védett / indifferens
Phosphaenus hemipterus	Nem védett / indifferens
Phosphuga atrata	Nem védett / indifferens
Photedes captiuncula	Védett
Photedes captiuncula delattini	Védett
Photedes minima	Nem védett / indifferens
Phoxinus phoxinus	Védett
Phradonoma villosulum	Nem védett / indifferens
Phragmataecia castaneae	Nem védett / indifferens
Phragmatiphila nexa	Védett
Phragmatobia fuliginosa	Nem védett / indifferens
Phragmatobia luctifera	Védett
Phrissotrichum rugicolle	Nem védett / indifferens
Phrurolithus festivus	Nem védett / indifferens
Phrurolithus minimus	Nem védett / indifferens
Phrurolithus pullatus	Nem védett / indifferens
Phrurolithus szilyi	Nem védett / indifferens
Phrydiuchus speiseri	Nem védett / indifferens
Phrydiuchus tau	Nem védett / indifferens
Phrydiuchus topiarius	Nem védett / indifferens
Phryganea bipunctata	Nem védett / indifferens
Phryganea grandis	Nem védett / indifferens
Phtheochroa annae	Nem védett / indifferens
Phtheochroa duponchelana	Nem védett / indifferens
Phtheochroa fulvicinctana	Nem védett / indifferens
Phtheochroa inopiana	Nem védett / indifferens
Phtheochroa procerana	Nem védett / indifferens
Phtheochroa pulvillana	Nem védett / indifferens
Phtheochroa purana	Nem védett / indifferens
Phtheochroa rugosana	Nem védett / indifferens
Phtheochroa schreibersiana	Nem védett / indifferens
Phtheochroa sodaliana	Nem védett / indifferens
Phthorimaea operculella	Nem védett / indifferens
Phycita meliella	Nem védett / indifferens
Phycita metzneri	Nem védett / indifferens
Phycita roborella	Nem védett / indifferens
Phycitodes albatella	Nem védett / indifferens
Phycitodes binaevella	Nem védett / indifferens
Phycitodes inquinatella	Nem védett / indifferens
Phycitodes lacteella	Nem védett / indifferens
Phycitodes maritima	Nem védett / indifferens
Phycitodes saxicola	Nem védett / indifferens
Phyllobius arborator	Nem védett / indifferens
Phyllobius argentatus	Nem védett / indifferens
Phyllobius betulinus	Nem védett / indifferens
Phyllobius brevis	Nem védett / indifferens
Phyllobius chloropus	Nem védett / indifferens
Phyllobius dispar	Nem védett / indifferens
Phyllobius glaucus	Nem védett / indifferens
Phyllobius maculicornis	Nem védett / indifferens
Phyllobius montanus	Nem védett / indifferens
Phyllobius oblongus	Nem védett / indifferens
Phyllobius pallidus	Nem védett / indifferens
Phyllobius pilicornis	Nem védett / indifferens
Phyllobius pomaceus	Nem védett / indifferens
Phyllobius pyri	Nem védett / indifferens
Phyllobius seladonius	Nem védett / indifferens
Phyllobius subdentatus	Nem védett / indifferens
Phyllobius thalassinus	Nem védett / indifferens
Phyllobius vespertinus	Nem védett / indifferens
Phyllobius virideaeris	Nem védett / indifferens
Phyllocnistis labyrinthella	Nem védett / indifferens
Phyllocnistis saligna	Nem védett / indifferens
Phyllocnistis unipunctella	Nem védett / indifferens
Phyllocnistis xenia	Nem védett / indifferens
Phyllodesma ilicifolium	Védett
Phyllodesma tremulifolium	Nem védett / indifferens
Phyllodrepa floralis	Nem védett / indifferens
Phyllodrepa melanocephala	Nem védett / indifferens
Phyllodrepa nigra	Nem védett / indifferens
Phyllodrepa puberula	Nem védett / indifferens
Phyllodrepa salicis	Nem védett / indifferens
Phyllodrepoidea crenata	Nem védett / indifferens
Phyllognathopus viguieri	Nem védett / indifferens
Phyllometra culminaria	Fokozottan védett
Phyllophya laciniata	Védett
Phyllonorycter abrasella	Nem védett / indifferens
Phyllonorycter acaciella	Nem védett / indifferens
Phyllonorycter acerifoliella	Nem védett / indifferens
Phyllonorycter agilella	Nem védett / indifferens
Phyllonorycter apparella	Nem védett / indifferens
Phyllonorycter blancardella	Nem védett / indifferens
Phyllonorycter cavella	Nem védett / indifferens
Phyllonorycter cerasinella	Nem védett / indifferens
Phyllonorycter comparella	Nem védett / indifferens
Phyllonorycter connexella	Nem védett / indifferens
Phyllonorycter coryli	Nem védett / indifferens
Phyllonorycter corylifoliella	Nem védett / indifferens
Phyllonorycter cydoniella	Nem védett / indifferens
Phyllonorycter delitella	Nem védett / indifferens
Phyllonorycter distentella	Nem védett / indifferens
Phyllonorycter dubitella	Nem védett / indifferens
Phyllonorycter emberizaepenella	Nem védett / indifferens
Phyllonorycter esperella	Nem védett / indifferens
Phyllonorycter fraxinella	Nem védett / indifferens
Phyllonorycter froelichiella	Nem védett / indifferens
Phyllonorycter pyrifoliella	Nem védett / indifferens
Phyllonorycter harrisella	Nem védett / indifferens
Phyllonorycter heegeriella	Nem védett / indifferens
Phyllonorycter helianthemella	Nem védett / indifferens
Phyllonorycter hilarella	Nem védett / indifferens
Phyllonorycter ilicifoliella	Nem védett / indifferens
Phyllonorycter insignitella	Nem védett / indifferens
Phyllonorycter kleemannella	Nem védett / indifferens
Phyllonorycter lantanella	Nem védett / indifferens
Phyllonorycter lautella	Nem védett / indifferens
Phyllonorycter leucographella	Nem védett / indifferens
Phyllonorycter maestingella	Nem védett / indifferens
Phyllonorycter medicaginella	Nem védett / indifferens
Phyllonorycter mespilella	Nem védett / indifferens
Phyllonorycter messaniella	Nem védett / indifferens
Phyllonorycter muelleriella	Nem védett / indifferens
Phyllonorycter nicellii	Nem védett / indifferens
Phyllonorycter nigrescentella	Nem védett / indifferens
Phyllonorycter oxyacanthae	Nem védett / indifferens
Phyllonorycter parisiella	Nem védett / indifferens
Phyllonorycter pastorella	Nem védett / indifferens
Phyllonorycter platani	Nem védett / indifferens
Phyllonorycter populifoliella	Nem védett / indifferens
Phyllonorycter quercifoliella	Nem védett / indifferens
Phyllonorycter quinqueguttella	Nem védett / indifferens
Phyllonorycter rajella	Nem védett / indifferens
Phyllonorycter robiniella	Nem védett / indifferens
Phyllonorycter roboris	Nem védett / indifferens
Phyllonorycter sagitella	Nem védett / indifferens
Phyllonorycter salicicolella	Nem védett / indifferens
Phyllonorycter salictella	Nem védett / indifferens
Phyllonorycter kuhlweiniella	Nem védett / indifferens
Phyllonorycter schreberella	Nem védett / indifferens
Phyllonorycter scitulella	Nem védett / indifferens
Phyllonorycter sorbi	Nem védett / indifferens
Phyllonorycter spinicolella	Nem védett / indifferens
Phyllonorycter staintoniella	Nem védett / indifferens
Phyllonorycter stettinensis	Nem védett / indifferens
Phyllonorycter tenerella	Nem védett / indifferens
Phyllonorycter tristrigella	Nem védett / indifferens
Phyllonorycter ulmifoliella	Nem védett / indifferens
Phyllopertha horticola	Nem védett / indifferens
Phyllophila obliterata	Nem védett / indifferens
Phylloporia bistrigella	Nem védett / indifferens
Phylloscopus bonelli	Védett
Phylloscopus collybita	Védett
Phylloscopus collybita abietinus	Védett
Phylloscopus collybita tristis	Védett
Phylloscopus fuscatus	Védett
Phylloscopus inornatus	Védett
Phylloscopus proregulus	Védett
Phylloscopus schwarzi	Védett
Phylloscopus sibilatrix	Védett
Phylloscopus trochilus	Védett
Phylloscopus trochilus acredula	Védett
Phyllostroma myrtilli	Nem védett / indifferens
Phyloctetes bidentulus	Nem védett / indifferens
Phyloctetes horvathi	Nem védett / indifferens
Phyloctetes truncatus	Nem védett / indifferens
Phylus coryli	Nem védett / indifferens
Phylus melanocephalus	Nem védett / indifferens
Phylus palliceps	Nem védett / indifferens
Phymata crassipes	Nem védett / indifferens
Phymatodes testaceus	Nem védett / indifferens
Phymatopus hecta	Nem védett / indifferens
Phymatura brevicollis	Nem védett / indifferens
Physa fontinalis	Nem védett / indifferens
Physatocheila confinis	Nem védett / indifferens
Physatocheila costata	Nem védett / indifferens
Physatocheila dumetorum	Nem védett / indifferens
Physatocheila smreczynskii	Nem védett / indifferens
Physella acuta	Nem védett / indifferens
Physocypria kraepelini	Nem védett / indifferens
Physokermes hemicryphus	Nem védett / indifferens
Physokermes inopinatus	Nem védett / indifferens
Physokermes piceae	Nem védett / indifferens
Phytobaenus amabilis	Nem védett / indifferens
Phytobius leucogaster	Nem védett / indifferens
Phytocoris austriacus	Nem védett / indifferens
Phytocoris dimidiatus	Nem védett / indifferens
Phytocoris incanus	Nem védett / indifferens
Phytocoris insignis	Nem védett / indifferens
Phytocoris longipennis	Nem védett / indifferens
Phytocoris meridionalis	Nem védett / indifferens
Phytocoris nowickyi	Nem védett / indifferens
Phytocoris parvulus	Nem védett / indifferens
Phytocoris pini	Nem védett / indifferens
Phytocoris populi	Nem védett / indifferens
Phytocoris reuteri	Nem védett / indifferens
Phytocoris singeri	Nem védett / indifferens
Phytocoris thrax	Nem védett / indifferens
Phytocoris tiliae	Nem védett / indifferens
Phytocoris ulmi	Nem védett / indifferens
Phytocoris ustulatus	Nem védett / indifferens
Phytocoris varipes	Nem védett / indifferens
Phytoecia caerulea	Nem védett / indifferens
Phytoecia cylindrica	Nem védett / indifferens
Phytoecia icterica	Nem védett / indifferens
Phytoecia nigricornis	Nem védett / indifferens
Phytoecia pustulata	Nem védett / indifferens
Phytoecia virgula	Nem védett / indifferens
Phytometra viridaria	Nem védett / indifferens
Pica pica	Nem védett / indifferens
Picromerus bidens	Nem védett / indifferens
Picromerus conformis	Nem védett / indifferens
Picus canus	Védett
Picus viridis	Védett
Pidonia lurida	Nem védett / indifferens
Pieris brassicae	Nem védett / indifferens
Pieris bryoniae	Védett
Pieris bryoniae marani	Védett
Pieris ergane	Védett
Pieris mannii	Védett
Pieris napi	Nem védett / indifferens
Pieris rapae	Nem védett / indifferens
Piesma capitatum	Nem védett / indifferens
Parapiesma kochiae	Nem védett / indifferens
Piesma maculatum	Nem védett / indifferens
Parapiesma quadratum	Nem védett / indifferens
Parapiesma salsolae	Nem védett / indifferens
Parapiesma silenes	Nem védett / indifferens
Piesma variabile	Nem védett / indifferens
Piezocranum simulans	Nem védett / indifferens
Piezodorus lituratus	Nem védett / indifferens
Pilemia hirsutula	Védett
Pilemia tigrina	Fokozottan védett
Pilophorus cinnamopterus	Nem védett / indifferens
Pilophorus clavatus	Nem védett / indifferens
Pilophorus confusus	Nem védett / indifferens
Pilophorus perplexus	Nem védett / indifferens
Pilophorus simulans	Nem védett / indifferens
Pima boisduvaliella	Nem védett / indifferens
Pinalitus coccineus	Nem védett / indifferens
Pinalitus rubricatus	Nem védett / indifferens
Pinalitus viscicola	Nem védett / indifferens
Pinicola enucleator	Védett
Piniphila bifasciana	Nem védett / indifferens
Pinthaeus sanguinipes	Nem védett / indifferens
Pinumius areatus	Nem védett / indifferens
Pionosomus opacellus	Nem védett / indifferens
Pipistrellus kuhlii	Védett
Pipistrellus nathusii	Védett
Pipistrellus pipistrellus	Védett
Pipistrellus pygmaeus	Védett
Pipiza austriaca	Nem védett / indifferens
Pipiza bimaculata	Nem védett / indifferens
Pipiza fasciata	Nem védett / indifferens
Pipiza festiva	Nem védett / indifferens
Pipiza lugubris	Nem védett / indifferens
Pipiza luteitarsis	Nem védett / indifferens
Pipiza noctiluca	Nem védett / indifferens
Pipiza quadrimaculata	Nem védett / indifferens
Pipizella annulata	Nem védett / indifferens
Pipizella divicoi	Nem védett / indifferens
Pipizella maculipennis	Nem védett / indifferens
Pipizella viduata	Nem védett / indifferens
Pipizella virens	Nem védett / indifferens
Pipizella zeneggenensis	Nem védett / indifferens
Pirapion immune	Nem védett / indifferens
Pirapion redemptum	Nem védett / indifferens
Pirata hygrophilus	Nem védett / indifferens
Pirata insularis	Nem védett / indifferens
Pirata knorri	Nem védett / indifferens
Pirata latitans	Nem védett / indifferens
Pirata piraticus	Nem védett / indifferens
Pirata piscatorius	Nem védett / indifferens
Pirata tenuitarsis	Nem védett / indifferens
Pirata uliginosus	Nem védett / indifferens
Peirates hybridus	Nem védett / indifferens
Pisaura mirabilis	Nem védett / indifferens
Piscicola geometra	Nem védett / indifferens
Piscicola haranti	Nem védett / indifferens
Pisidium amnicum	Nem védett / indifferens
Pisidium casertanum	Nem védett / indifferens
Pisidium henslowanum	Nem védett / indifferens
Pisidium milium	Nem védett / indifferens
Pisidium moitessierianum	Nem védett / indifferens
Pisidium nitidum	Nem védett / indifferens
Pisidium obtusale	Nem védett / indifferens
Pisidium personatum	Nem védett / indifferens
Pisidium pseudosphaerium	Nem védett / indifferens
Pisidium pulchellum	Nem védett / indifferens
Pisidium subtruncatum	Nem védett / indifferens
Pisidium supinum	Nem védett / indifferens
Pisidium tenuilineatum	Nem védett / indifferens
Pison atrum	Nem védett / indifferens
Pissodes castaneus	Nem védett / indifferens
Pissodes harcyniae	Nem védett / indifferens
Pissodes piceae	Nem védett / indifferens
Pissodes pini	Nem védett / indifferens
Pissodes piniphilus	Nem védett / indifferens
Pissodes scabricollis	Nem védett / indifferens
Pissodes validirostris	Nem védett / indifferens
Pistius truncatus	Nem védett / indifferens
Pithanus maerkelii	Nem védett / indifferens
Pithyotettix abietinus	Nem védett / indifferens
Pityogenes bidentatus	Nem védett / indifferens
Pityogenes bistridentatus	Nem védett / indifferens
Pityogenes chalcographus	Nem védett / indifferens
Pityogenes conjunctus	Nem védett / indifferens
Pityogenes quadridens	Nem védett / indifferens
Pityogenes trepanatus	Nem védett / indifferens
Pityohyphantes phrygianus	Nem védett / indifferens
Pityokteines curvidens	Nem védett / indifferens
Pityokteines vorontzowi	Nem védett / indifferens
Pityophagus ferrugineus	Nem védett / indifferens
Pityophagus laevior	Nem védett / indifferens
Pityophagus quercus	Nem védett / indifferens
Pityophthorus carniolicus	Nem védett / indifferens
Pityophthorus exsculptus	Nem védett / indifferens
Pityophthorus henscheli	Nem védett / indifferens
Pityophthorus lichtensteinii	Nem védett / indifferens
Pityophthorus micrographus	Nem védett / indifferens
Pityophthorus pityographus	Nem védett / indifferens
Pityophthorus pubescens	Nem védett / indifferens
Placobdella costata	Nem védett / indifferens
Placonotus testaceus	Nem védett / indifferens
Placusa adscita	Nem védett / indifferens
Placusa atrata	Nem védett / indifferens
Placusa complanata	Nem védett / indifferens
Placusa depressa	Nem védett / indifferens
Placusa incompleta	Nem védett / indifferens
Placusa pumilio	Nem védett / indifferens
Placusa tachyporoides	Nem védett / indifferens
Plagiognathus arbustorum	Nem védett / indifferens
Plagiognathus bipunctatus	Nem védett / indifferens
Plagiognathus chrysanthemi	Nem védett / indifferens
Plagiognathus fulvipennis	Nem védett / indifferens
Plagiogonus putridus	Nem védett / indifferens
Plagiolepis pygmaea	Nem védett / indifferens
Plagiolepis vindobonensis	Nem védett / indifferens
Plagiolepis xene	Nem védett / indifferens
Plagionotus arcuatus	Nem védett / indifferens
Plagionotus detritus	Nem védett / indifferens
Plagionotus floralis	Nem védett / indifferens
Plagiorrhamma suturalis	Nem védett / indifferens
Plagodis dolabraria	Nem védett / indifferens
Plagodis pulveraria	Nem védett / indifferens
Planaphrodes bifasciatus	Nem védett / indifferens
Planaphrodes elongatus	Nem védett / indifferens
Planaphrodes trifasciatus	Nem védett / indifferens
Planchonia arabidis	Nem védett / indifferens
Planeustomus heydeni	Nem védett / indifferens
Planeustomus kahrii	Nem védett / indifferens
Planeustomus palpalis	Nem védett / indifferens
Planolinus uliginosus	Nem védett / indifferens
Planorbarius corneus	Nem védett / indifferens
Planorbella duryi	Nem védett / indifferens
Planorbella nigricans	Nem védett / indifferens
Planorbis carinatus	Nem védett / indifferens
Planorbis planorbis	Nem védett / indifferens
Platalea leucorodia	Fokozottan védett
Platambus maculatus	Nem védett / indifferens
Plataraea dubiosa	Nem védett / indifferens
Plataraea nigriceps	Nem védett / indifferens
Plataraea nigrifrons	Nem védett / indifferens
Plataraea sordida	Nem védett / indifferens
Plataraea spaethi	Nem védett / indifferens
Platycerus caprea	Védett
Platycerus caraboides	Védett
Platycheirus albimanus	Nem védett / indifferens
Platycheirus ambiguus	Nem védett / indifferens
Platycheirus angustatus	Nem védett / indifferens
Platycheirus clypeatus	Nem védett / indifferens
Platycheirus discimanus	Nem védett / indifferens
Platycheirus europaeus	Nem védett / indifferens
Platycheirus fulviventris	Nem védett / indifferens
Platycheirus immarginatus	Nem védett / indifferens
Platycheirus manicatus	Nem védett / indifferens
Platycheirus parmatus	Nem védett / indifferens
Platycheirus peltatus	Nem védett / indifferens
Platycheirus perpallidus	Nem védett / indifferens
Platycheirus podagratus	Nem védett / indifferens
Platycheirus scambus	Nem védett / indifferens
Platycheirus scutatus	Nem védett / indifferens
Platycheirus sticticus	Nem védett / indifferens
Platycheirus tarsalis	Nem védett / indifferens
Platycis cosnardi	Nem védett / indifferens
Platycis minutus	Nem védett / indifferens
Platycleis affinis	Nem védett / indifferens
Platycleis albopunctata	Nem védett / indifferens
Platycleis montana	Nem védett / indifferens
Platycleis veyseli	Nem védett / indifferens
Platycnemis pennipes	Nem védett / indifferens
Platydema dejeani	Nem védett / indifferens
Platydema violaceum	Nem védett / indifferens
Platyderus rufus transalpinus	Nem védett / indifferens
Platydomene bicolor	Nem védett / indifferens
Platydracus chalcocephalus	Nem védett / indifferens
Platydracus flavopunctatus	Nem védett / indifferens
Platydracus fulvipes	Nem védett / indifferens
Platydracus latebricola	Nem védett / indifferens
Platydracus stercorarius	Nem védett / indifferens
Platyedra subcinerea	Nem védett / indifferens
Platyla banatica	Nem védett / indifferens
Platyla perpusilla	Nem védett / indifferens
Platyla polita	Nem védett / indifferens
Platylomalus complanatus	Nem védett / indifferens
Platymetopius curvatus	Nem védett / indifferens
Platymetopius filigranus	Nem védett / indifferens
Platymetopius guttatus	Nem védett / indifferens
Platymetopius major	Nem védett / indifferens
Platymetopius rostratus	Nem védett / indifferens
Platymetopius undatus	Nem védett / indifferens
Platymyrmilla quinquefasciata	Nem védett / indifferens
Platynaspis luteorubra	Nem védett / indifferens
Limodromus assimilis	Nem védett / indifferens
Limodromus krynickii	Nem védett / indifferens
Platynus livens	Nem védett / indifferens
Limodromus longiventris	Nem védett / indifferens
Platynus scrobiculatus	Nem védett / indifferens
Platyola austriaca	Nem védett / indifferens
Platyperigea aspersa	Nem védett / indifferens
Platyperigea kadenii	Nem védett / indifferens
Platyperigea terrea	Nem védett / indifferens
Platyphylax frauenfeldi	Fokozottan védett
Platyplax inermis	Nem védett / indifferens
Platyplax salviae	Nem védett / indifferens
Platyptilia calodactyla	Nem védett / indifferens
Buzkoiana capnodactylus	Nem védett / indifferens
Platyptilia farfarellus	Nem védett / indifferens
Platyptilia gonodactyla	Nem védett / indifferens
Gillmeria miantodactylus	Nem védett / indifferens
Platyptilia nemoralis	Nem védett / indifferens
Gillmeria pallidactyla	Nem védett / indifferens
Platyptilia tesseradactyla	Nem védett / indifferens
Gillmeria ochrodactyla	Nem védett / indifferens
Platypus cylindrus	Nem védett / indifferens
Platypus oxyurus	Nem védett / indifferens
Platyrhinus resinosus	Nem védett / indifferens
Platyscelis hungarica	Védett
Platysoma compressum	Nem védett / indifferens
Platystethus alutaceus	Nem védett / indifferens
Platystethus arenarius	Nem védett / indifferens
Platystethus capito	Nem védett / indifferens
Platystethus cornutus	Nem védett / indifferens
Platystethus degener	Nem védett / indifferens
Platystethus nitens	Nem védett / indifferens
Platystethus rufospinus	Nem védett / indifferens
Platystethus spinosus	Nem védett / indifferens
Platystomos albinus	Nem védett / indifferens
Platytes alpinella	Nem védett / indifferens
Platytes cerussella	Nem védett / indifferens
Plea minutissima	Nem védett / indifferens
Pleargus pygmaeus	Nem védett / indifferens
Plebeius argus	Nem védett / indifferens
Plebeius argyrognomon	Nem védett / indifferens
Plebeius idas	Védett
Plebeius sephirus	Fokozottan védett
Plecotus auritus	Védett
Plecotus austriacus	Védett
Plectophloeus erichsoni	Nem védett / indifferens
Plectophloeus fischeri	Nem védett / indifferens
Rupicapra rupicapra	Védett
Plectophloeus nitidus	Nem védett / indifferens
Plectophloeus zoufali	Nem védett / indifferens
Plectrocnemia brevis	Nem védett / indifferens
Plectrocnemia conspersa	Nem védett / indifferens
Plectrocnemia geniculata	Nem védett / indifferens
Plectrocnemia minima	Védett
Plectrophenax nivalis	Védett
Plectrophenax nivalis vlasowae	Védett
Plegaderus caesus	Nem védett / indifferens
Plegaderus discisus	Nem védett / indifferens
Plegaderus dissectus	Nem védett / indifferens
Plegaderus saucius	Nem védett / indifferens
Plegaderus vulneratus	Nem védett / indifferens
Plegadis falcinellus	Fokozottan védett
Pleganophorus bispinosus	Nem védett / indifferens
Plemyria rubiginata	Nem védett / indifferens
Plesiocypridopsis newtoni	Nem védett / indifferens
Plesiodema pinetella	Nem védett / indifferens
Pleurophorus caesus	Nem védett / indifferens
Pleurophorus pannonicus	Nem védett / indifferens
Pleuroptya balteata	Nem védett / indifferens
Pleuroptya ruralis	Nem védett / indifferens
Pleurota aristella	Nem védett / indifferens
Pleurota bicostella	Nem védett / indifferens
Pleurota brevispinella	Nem védett / indifferens
Pleurota marginella	Nem védett / indifferens
Pleurota pungitiella	Nem védett / indifferens
Pleurota pyropella	Nem védett / indifferens
Pleurotobia magnifica	Nem védett / indifferens
Pleuroxus aduncus	Nem védett / indifferens
Pleuroxus denticulatus	Nem védett / indifferens
Pleuroxus laevis	Nem védett / indifferens
Pleuroxus striatus	Nem védett / indifferens
Pleuroxus trigonellus	Nem védett / indifferens
Pleuroxus truncatus	Nem védett / indifferens
Pleuroxus uncinatus	Nem védett / indifferens
Plinthisus brevipennis	Nem védett / indifferens
Plinthisus longicollis	Nem védett / indifferens
Plinthisus pusillus	Nem védett / indifferens
Plinthus findeli	Nem védett / indifferens
Plinthus megerlei	Nem védett / indifferens
Plinthus sturmii	Nem védett / indifferens
Plinthus tischeri	Nem védett / indifferens
Plodia interpunctella	Nem védett / indifferens
Plusia festucae	Nem védett / indifferens
Plutella porrectella	Nem védett / indifferens
Plutella xylostella	Nem védett / indifferens
Pluvialis apricaria	Védett
Pluvialis fulva	Védett
Pluvialis squatarola	Védett
Luzulaspis jahandiezi	Nem védett / indifferens
Pocadicnemis juncea	Nem védett / indifferens
Pocadicnemis pumila	Nem védett / indifferens
Pocadius adustus	Nem védett / indifferens
Pocadius ferrugineus	Nem védett / indifferens
Pocota personata	Nem védett / indifferens
Podabrus alpinus	Nem védett / indifferens
Podalonia affinis	Nem védett / indifferens
Podalonia hirsuta	Nem védett / indifferens
Podalonia luffi	Nem védett / indifferens
Podalonia tydei	Nem védett / indifferens
Podarcis muralis	Védett
Podarcis taurica	Védett
Podeonius acuticornis	Nem védett / indifferens
Podiceps auritus	Védett
Podiceps cristatus	Védett
Podiceps grisegena	Fokozottan védett
Podiceps nigricollis	Fokozottan védett
Podisma pedestris	Védett
Podonta nigrita	Nem védett / indifferens
Podops curvidens	Nem védett / indifferens
Podops inuncta	Nem védett / indifferens
Podops rectidens	Nem védett / indifferens
Poecilagenia rubricans	Nem védett / indifferens
Poecilagenia sculpturata	Nem védett / indifferens
Poecilia reticulata	Nem védett / indifferens
Poecilia sphenops	Nem védett / indifferens
Poecilimon affinis	Nem védett / indifferens
Poecilimon fussii	Védett
Poecilimon intermedius	Védett
Poecilimon schmidtii	Védett
Poecilium alni	Nem védett / indifferens
Poecilium fasciatum	Nem védett / indifferens
Poecilium glabratum	Nem védett / indifferens
Poecilium puncticolle	Nem védett / indifferens
Poecilium pusillum	Nem védett / indifferens
Poecilium rufipes	Nem védett / indifferens
Poecilocampa populi	Nem védett / indifferens
Poecilochroa variana	Nem védett / indifferens
Poeciloneta variegata	Nem védett / indifferens
Poecilonota variolosa	Nem védett / indifferens
Poecilus cupreus	Nem védett / indifferens
Poecilus kekesiensis	Védett
Poecilus lepidus	Nem védett / indifferens
Poecilus puncticollis	Nem védett / indifferens
Poecilus punctulatus	Nem védett / indifferens
Poecilus sericeus	Nem védett / indifferens
Poecilus striatopunctatus	Nem védett / indifferens
Poecilus versicolor	Nem védett / indifferens
Pogonocherus decoratus	Nem védett / indifferens
Pogonocherus fasciculatus	Nem védett / indifferens
Pogonocherus hispidulus	Nem védett / indifferens
Pogonocherus hispidus	Nem védett / indifferens
Pogonocherus ovatus	Nem védett / indifferens
Pogonus luridipennis	Nem védett / indifferens
Pogonus peisonis	Nem védett / indifferens
Polemistus abnormis	Nem védett / indifferens
Polia bombycina	Nem védett / indifferens
Polia hepatica	Nem védett / indifferens
Polia nebulosa	Nem védett / indifferens
Polistes associus	Nem védett / indifferens
Polistes biglumis	Nem védett / indifferens
Polistes biglumis bimaculatus	Nem védett / indifferens
Polistes bischoffi	Nem védett / indifferens
Polistes dominulus	Nem védett / indifferens
Polistes gallicus	Nem védett / indifferens
Polistes nimpha	Nem védett / indifferens
Polistes omissus	Nem védett / indifferens
Polistichus connexus	Nem védett / indifferens
Polochrum repandum	Nem védett / indifferens
Polycentropus flavomaculatus	Nem védett / indifferens
Polycentropus irroratus	Nem védett / indifferens
Polycentropus schmidi	Nem védett / indifferens
Polychrysia moneta	Védett
Polydesmus collaris	Nem védett / indifferens
Polydesmus complanatus	Nem védett / indifferens
Polydesmus denticulatus	Nem védett / indifferens
Polydesmus edentulus	Nem védett / indifferens
Polydesmus edentulus bidentatus	Nem védett / indifferens
Polydesmus germanicus	Nem védett / indifferens
Polydesmus monticolus	Nem védett / indifferens
Polydesmus monticolus koszegensis	Nem védett / indifferens
Polydesmus polonicus	Nem védett / indifferens
Polydesmus schassburgensis	Nem védett / indifferens
Polydrusus amoenus	Nem védett / indifferens
Polydrusus cervinus	Nem védett / indifferens
Polydrusus confluens	Nem védett / indifferens
Polydrusus corruscus	Nem védett / indifferens
Polydrusus flavipes	Nem védett / indifferens
Polydrusus formosus	Nem védett / indifferens
Polydrusus fulvicornis	Nem védett / indifferens
Polydrusus impar	Nem védett / indifferens
Polydrusus impressifrons	Nem védett / indifferens
Polydrusus marginatus	Nem védett / indifferens
Polydrusus mollis	Nem védett / indifferens
Polydrusus pallidus	Nem védett / indifferens
Polydrusus picus	Nem védett / indifferens
Polydrusus pilosus	Nem védett / indifferens
Polydrusus pterygomalis	Nem védett / indifferens
Polydrusus sparsus	Nem védett / indifferens
Polydrusus tereticollis	Nem védett / indifferens
Polydrusus thalassinus	Nem védett / indifferens
Polydrusus tibialis	Nem védett / indifferens
Polydrusus viridicinctus	Nem védett / indifferens
Polyergus rufescens	Nem védett / indifferens
Polygonia c-album	Védett
Polygraphus grandiclava	Nem védett / indifferens
Polygraphus poligraphus	Nem védett / indifferens
Polygraphus subopacus	Nem védett / indifferens
Polymerus asperulae	Nem védett / indifferens
Polymerus brevicornis	Nem védett / indifferens
Polymerus carpathicus	Nem védett / indifferens
Polymerus cognatus	Nem védett / indifferens
Polymerus holosericeus	Nem védett / indifferens
Polymerus microphthalmus	Nem védett / indifferens
Polymerus nigrita	Nem védett / indifferens
Polymerus palustris	Nem védett / indifferens
Polymerus unifasciatus	Nem védett / indifferens
Polymerus vulneratus	Nem védett / indifferens
Polymixis polymita	Nem védett / indifferens
Polymixis rufocincta	Fokozottan védett
Polymixis rufocincta isolata	Fokozottan védett
Polymixis xanthomista	Nem védett / indifferens
Polyommatus admetus	Védett
Polyommatus amandus	Védett
Polyommatus bellargus	Nem védett / indifferens
Polyommatus coridon	Nem védett / indifferens
Polyommatus damon	Fokozottan védett
Polyommatus daphnis	Nem védett / indifferens
Polyommatus dorylas	Védett
Polyommatus escheri	Nem védett / indifferens
Polyommatus icarus	Nem védett / indifferens
Polyommatus semiargus	Nem védett / indifferens
Polyommatus thersites	Védett
Polyphaenis sericata	Nem védett / indifferens
Polyphemus pediculus	Nem védett / indifferens
Polyphylla fullo	Nem védett / indifferens
Polyploca ridens	Nem védett / indifferens
Polypogon gryphalis	Védett
Polypogon tentacularia	Nem védett / indifferens
Polysarcus denticauda	Védett
Polysticta stelleri	Fokozottan védett
Polystomophora ostiaplurima	Nem védett / indifferens
Polyxenus lagurus	Nem védett / indifferens
Polyzonium germanicum	Nem védett / indifferens
Pomatias elegans	Védett
Pomatias rivularis	Védett
Pomatinus substriatus	Nem védett / indifferens
Pompilus cinereus	Nem védett / indifferens
Ponera coarctata	Nem védett / indifferens
Ponera testacea	Nem védett / indifferens
Pontia edusa	Nem védett / indifferens
Poophagus hopffgarteni	Nem védett / indifferens
Poophagus robustus	Nem védett / indifferens
Poophagus sisymbrii	Nem védett / indifferens
Porcinolus murinus	Nem védett / indifferens
Porhydrus lineatus	Nem védett / indifferens
Porhydrus obliquesignatus	Nem védett / indifferens
Poromniusa crassa	Nem védett / indifferens
Poromniusa procidua	Nem védett / indifferens
Porotachys bisulcatus	Nem védett / indifferens
Porphyrio porphyrio	Védett
Porphyrio porphyrio seistanicus	Védett
Porphyrophora polonica	Védett
Porrhomma convexum	Nem védett / indifferens
Porrhomma errans	Nem védett / indifferens
Zygina rorida	Nem védett / indifferens
Porrhomma microphthalmum	Nem védett / indifferens
Porrhomma montanum	Nem védett / indifferens
Porrhomma profundum	Nem védett / indifferens
Porrhomma pygmaeum	Nem védett / indifferens
Porrhomma rosenhaueri	Nem védett / indifferens
Porrittia galactodactyla	Nem védett / indifferens
Porthmidius austriacus	Nem védett / indifferens
Porzana parva	Védett
Porzana porzana	Védett
Porzana pusilla	Fokozottan védett
Porzana pusilla intermedia	Fokozottan védett
Postsolenobia banatica	Nem védett / indifferens
Postsolenobia thomanni	Nem védett / indifferens
Potamanthus luteus	Nem védett / indifferens
Potamocypris arcuata	Nem védett / indifferens
Potamocypris fallax	Nem védett / indifferens
Potamocypris fulva	Nem védett / indifferens
Potamocypris pallida	Nem védett / indifferens
Potamocypris unicaudata	Nem védett / indifferens
Potamocypris variegata	Nem védett / indifferens
Potamocypris villosa	Nem védett / indifferens
Potamocypris zschokkei	Nem védett / indifferens
Potamophilus acuminatus	Védett
Potamophylax cingulatus	Nem védett / indifferens
Potamophylax luctuosus	Nem védett / indifferens
Potamophylax nigricornis	Nem védett / indifferens
Potamophylax rotundipennis	Nem védett / indifferens
Potamopyrgus antipodarum	Nem védett / indifferens
Povolnya leucapennella	Nem védett / indifferens
Praesolenobia clathrella	Nem védett / indifferens
Praestochrysis megerlei	Nem védett / indifferens
Praganus hofferi	Nem védett / indifferens
Prays fraxinella	Nem védett / indifferens
Prays ruficeps	Nem védett / indifferens
Prenolepis imparis	Nem védett / indifferens
Prenolepis nitens	Nem védett / indifferens
Pria dulcamarae	Nem védett / indifferens
Prinerigone vagans	Nem védett / indifferens
Priobium carpini	Nem védett / indifferens
Priocnemis agilis	Nem védett / indifferens
Priocnemis cordivalvata	Nem védett / indifferens
Priocnemis coriacea	Nem védett / indifferens
Priocnemis enslini	Nem védett / indifferens
Priocnemis exaltata	Nem védett / indifferens
Priocnemis fastigiata	Nem védett / indifferens
Priocnemis fennica	Nem védett / indifferens
Priocnemis gracilis	Nem védett / indifferens
Priocnemis hankoi	Nem védett / indifferens
Priocnemis hauptia	Nem védett / indifferens
Priocnemis hyalinata	Nem védett / indifferens
Priocnemis melanosoma	Nem védett / indifferens
Priocnemis mesobrometi	Nem védett / indifferens
Priocnemis mimula	Nem védett / indifferens
Priocnemis minuta	Nem védett / indifferens
Priocnemis minutalis	Nem védett / indifferens
Priocnemis occipitalis	Nem védett / indifferens
Priocnemis parvula	Nem védett / indifferens
Priocnemis perturbator	Nem védett / indifferens
Priocnemis pillichi	Nem védett / indifferens
Priocnemis propinqua	Nem védett / indifferens
Priocnemis pusilla	Nem védett / indifferens
Priocnemis rugosa	Nem védett / indifferens
Priocnemis schioedtei	Nem védett / indifferens
Priocnemis sulci	Nem védett / indifferens
Priocnemis susterai	Nem védett / indifferens
Prionocyphon serricornis	Nem védett / indifferens
Prionocypris zenkeri	Nem védett / indifferens
Prionus coriarius	Nem védett / indifferens
Prionychus ater	Nem védett / indifferens
Prionychus melanarius	Nem védett / indifferens
Prionyx kirbyi	Nem védett / indifferens
Prionyx subfuscatus	Nem védett / indifferens
Prisistus obsoletus	Nem védett / indifferens
Prisistus suturalba	Nem védett / indifferens
Pristerognatha fuligana	Nem védett / indifferens
Pristerognatha penthinana	Nem védett / indifferens
Pristicephalus shadini	Nem védett / indifferens
Probaticus subrugosus	Fokozottan védett
Proceratium melinum	Nem védett / indifferens
Prochlidonia amiantana	Nem védett / indifferens
Prochoreutis myllerana	Nem védett / indifferens
Prochoreutis sehestediana	Nem védett / indifferens
Prochoreutis stellaris	Nem védett / indifferens
Procloeon bifidum	Nem védett / indifferens
Procloeon macronyx	Nem védett / indifferens
Procraerus tibialis	Nem védett / indifferens
Procyon lotor	Nem védett / indifferens
Grammodes stolida	Nem védett / indifferens
Prolita solutella	Nem védett / indifferens
Pronocera angusta	Védett
Pronomaea korgei	Nem védett / indifferens
Propylea quatuordecimpunctata	Nem védett / indifferens
Proserpinus proserpina	Védett
Prosopistoma pennigerum	Nem védett / indifferens
Prostemma aeneicolle	Nem védett / indifferens
Prostemma guttula	Nem védett / indifferens
Prostemma sanguineum	Nem védett / indifferens
Prosternon chrysocomum	Nem védett / indifferens
Prosternon tessellatum	Nem védett / indifferens
Prostomis mandibularis	Védett
Protapion apricans	Nem védett / indifferens
Protapion assimile	Nem védett / indifferens
Protapion difforme	Nem védett / indifferens
Protapion dissimile	Nem védett / indifferens
Protapion filirostre	Nem védett / indifferens
Protapion fulvipes	Nem védett / indifferens
Protapion gracilipes	Nem védett / indifferens
Protapion interjectum	Nem védett / indifferens
Protapion nigritarse	Nem védett / indifferens
Protapion ononidis	Nem védett / indifferens
Protapion ruficrus	Nem védett / indifferens
Protapion schoenherri	Nem védett / indifferens
Protapion trifolii	Nem védett / indifferens
Protapion varipes	Nem védett / indifferens
Proteinus atomarius	Nem védett / indifferens
Proteinus brachypterus	Nem védett / indifferens
Proteinus crenulatus	Nem védett / indifferens
Proteinus laevigatus	Nem védett / indifferens
Proteinus ovalis	Nem védett / indifferens
Proteroiulus fuscus	Nem védett / indifferens
Proterorhinus marmoratus	Védett
Protodeltote pygarga	Nem védett / indifferens
Protolampra sobrina	Nem védett / indifferens
Protonemura aestiva	Nem védett / indifferens
Protonemura auberti	Nem védett / indifferens
Protonemura intricata	Nem védett / indifferens
Protonemura nitida	Nem védett / indifferens
Protonemura praecox	Nem védett / indifferens
Protopirapion atratulum	Nem védett / indifferens
Protopirapion kraatzii	Nem védett / indifferens
Proutia betulina	Nem védett / indifferens
Proxenus lepigone	Nem védett / indifferens
Prunella collaris	Védett
Prunella modularis	Védett
Psacasta exanthematica	Nem védett / indifferens
Psacasta neglecta	Nem védett / indifferens
Psacasta pallida	Nem védett / indifferens
Psallidium maxillosum	Nem védett / indifferens
Psallus albicinctus	Nem védett / indifferens
Psallus ambiguus	Nem védett / indifferens
Psallus anaemicus	Nem védett / indifferens
Psallus betuleti	Nem védett / indifferens
Psallus confusus	Nem védett / indifferens
Psallus falleni	Nem védett / indifferens
Psallus flavellus	Nem védett / indifferens
Psallus lepidus	Nem védett / indifferens
Psallus mollis	Nem védett / indifferens
Psallus nigripilis	Nem védett / indifferens
Psallus ocularis	Nem védett / indifferens
Psallus pardalis	Nem védett / indifferens
Psallus perrisi	Nem védett / indifferens
Psallus quercus	Nem védett / indifferens
Psallus salicis	Nem védett / indifferens
Psallus variabilis	Nem védett / indifferens
Psallus varians	Nem védett / indifferens
Psallus vittatus	Nem védett / indifferens
Psamathocrita dalmatinella	Nem védett / indifferens
Psammaecius punctulatus	Nem védett / indifferens
Psammodius asper	Nem védett / indifferens
Psammodius canaliculatus	Nem védett / indifferens
Psammodius danubialis	Nem védett / indifferens
Psammodius laevipennis	Nem védett / indifferens
Psammoecus bipuncatatus	Nem védett / indifferens
Psammotettix alienus	Nem védett / indifferens
Psammotettix asper	Nem védett / indifferens
Psammotettix cephalotes	Nem védett / indifferens
Psammotettix comitans	Nem védett / indifferens
Psammotettix confinis	Nem védett / indifferens
Psammotettix excisus	Nem védett / indifferens
Psammotettix helvolus	Nem védett / indifferens
Psammotettix koeleriae	Nem védett / indifferens
Psammotettix kolosvarensis	Nem védett / indifferens
Psammotettix nodosus	Nem védett / indifferens
Psammotettix notatus	Nem védett / indifferens
Psammotettix ornaticeps	Nem védett / indifferens
Psammotettix pallidinervis	Nem védett / indifferens
Psammotettix pictipennis	Nem védett / indifferens
Psammotettix poecilus	Nem védett / indifferens
Psammotettix provincialis	Nem védett / indifferens
Psammotettix remanei	Nem védett / indifferens
Psammotettix slovacus	Nem védett / indifferens
Psammotis pulveralis	Nem védett / indifferens
Psarus abdominalis	Nem védett / indifferens
Psectra diptera	Nem védett / indifferens
Pselactus spadix	Nem védett / indifferens
Pselaphaulax dresdensis	Nem védett / indifferens
Pselaphus heisei	Nem védett / indifferens
Pselnophorus heterodactyla	Nem védett / indifferens
Psen ater	Nem védett / indifferens
Psen exaratus	Nem védett / indifferens
Psenulus concolor	Nem védett / indifferens
Psenulus fuscipennis	Nem védett / indifferens
Psenulus laevigatus	Nem védett / indifferens
Psenulus meridionalis	Nem védett / indifferens
Psenulus pallipes	Nem védett / indifferens
Psenulus schencki	Nem védett / indifferens
Pseudanodonta complanata	Védett
Pseudanostirus globicollis	Nem védett / indifferens
Pseudapion fulvirostre	Nem védett / indifferens
Pseudapion rufirostre	Nem védett / indifferens
Pseudapis diversipes	Nem védett / indifferens
Pseudapis femoralis	Nem védett / indifferens
Pseudapis unidentata	Nem védett / indifferens
Pseudargyrotoza conwagana	Nem védett / indifferens
Pseudatemelia flavifrontella	Nem védett / indifferens
Pseudatemelia josephinae	Nem védett / indifferens
Pseudatemelia subochreella	Nem védett / indifferens
Pseudaulacaspis pentagona	Nem védett / indifferens
Pseudepierus italicus	Nem védett / indifferens
Pseudepipona angusta	Nem védett / indifferens
Pseudepipona herrichii	Nem védett / indifferens
Pseudeulia asinana	Nem védett / indifferens
Pseudeuophrys erratica	Nem védett / indifferens
Pseudeuophrys lanigera	Nem védett / indifferens
Pseudeuophrys obsoleta	Nem védett / indifferens
Pseudeuophrys vafra	Nem védett / indifferens
Pseudeustrotia candidula	Nem védett / indifferens
Pseudicius encarpatus	Nem védett / indifferens
Pseudocandona albicans	Nem védett / indifferens
Pseudocandona compressa	Nem védett / indifferens
Pseudocandona eremita	Nem védett / indifferens
Pseudocandona insculpta	Nem védett / indifferens
Pseudocandona marchica	Nem védett / indifferens
Pseudocandona pratensis	Nem védett / indifferens
Pseudocandona rostrata	Nem védett / indifferens
Pseudocandona sarsi	Nem védett / indifferens
Pseudocandona sucki	Nem védett / indifferens
Pseudocandona szoecsi	Nem védett / indifferens
Pseudochermes fraxini	Nem védett / indifferens
Pseudochoragus piceus	Nem védett / indifferens
Pseudochydorus globosus	Nem védett / indifferens
Pseudocistela ceramboides	Nem védett / indifferens
Pseudocleonus cinereus	Nem védett / indifferens
Pseudocleonus grammicus	Nem védett / indifferens
Pseudocoeliodes rubricus	Nem védett / indifferens
Pseudofusulus varians	Nem védett / indifferens
Pseudohermenias abietana	Nem védett / indifferens
Pseudoips prasinana	Nem védett / indifferens
Pseudoloxops coccineus	Nem védett / indifferens
Pseudomalus auratus	Nem védett / indifferens
Pseudomalus bogdanovi	Nem védett / indifferens
Pseudomalus pusillus	Nem védett / indifferens
Pseudomalus violaceus	Nem védett / indifferens
Pseudomedon huetheri	Nem védett / indifferens
Pseudomedon obscurellus	Nem védett / indifferens
Pseudomedon obsoletus	Nem védett / indifferens
Pseudomicrodota jelineki	Nem védett / indifferens
Pseudomicrodynerus parvulus	Nem védett / indifferens
Pseudomyllocerus bisignatus	Nem védett / indifferens
Pseudomyllocerus magnanoi	Nem védett / indifferens
Pseudomyllocerus periteloides	Nem védett / indifferens
Pseudomyllocerus sinuatus	Nem védett / indifferens
Pseudomyllocerus subsignatus	Nem védett / indifferens
Pseudomyllocerus viridilimbatus	Nem védett / indifferens
Pseudoophonus calceatus	Nem védett / indifferens
Pseudoophonus griseus	Nem védett / indifferens
Pseudoophonus rufipes	Nem védett / indifferens
Pseudopanthera macularia	Nem védett / indifferens
Pseudoperapion brevirostre	Nem védett / indifferens
Pseudopodisma fieberi	Nem védett / indifferens
Pseudopodisma nagyi	Nem védett / indifferens
Pseudopostega auritella	Nem védett / indifferens
Pseudopostega crepusculella	Nem védett / indifferens
Pseudoprotapion astragali	Nem védett / indifferens
Pseudoprotapion elegantulum	Nem védett / indifferens
Pseudoprotapion ergenense	Nem védett / indifferens
Pseudopsis sulcata	Nem védett / indifferens
Pseudoptilinus fissicollis	Nem védett / indifferens
Pseudorasbora parva	Inváziós
Pseudorchestes cinereus	Nem védett / indifferens
Pseudorchestes ermischi	Nem védett / indifferens
Pseudorchestes horioni	Nem védett / indifferens
Pseudorchestes kostali	Nem védett / indifferens
Pseudorchestes persimilis	Nem védett / indifferens
Pseudorchestes pratensis	Nem védett / indifferens
Pseudorchestes smreczynskii	Nem védett / indifferens
Pseudosciaphila branderiana	Nem védett / indifferens
Pseudosemiris kaufmanni	Nem védett / indifferens
Pseudospinolia incrassata	Nem védett / indifferens
Pseudospinolia neglecta	Nem védett / indifferens
Pseudospinolia uniformis	Nem védett / indifferens
Pseudostenapion simum	Nem védett / indifferens
Pseudostyphlus pillumus	Nem védett / indifferens
Pseudosuccinea columella	Nem védett / indifferens
Pseudoswammerdamia combinella	Nem védett / indifferens
Pseudotelphusa paripunctella	Nem védett / indifferens
Pseudotelphusa scalella	Nem védett / indifferens
Pseudotelphusa tessella	Nem védett / indifferens
Pseudoterpna pruinata	Nem védett / indifferens
Pseudotrichia rubiginosa	Nem védett / indifferens
Pseudotriphyllus suturalis	Nem védett / indifferens
Pseudovadonia livida	Nem védett / indifferens
Pseudovadonia livida pecta	Nem védett / indifferens
Psilochorus simoni	Nem védett / indifferens
Psilococcus ruber	Nem védett / indifferens
Psilota anthracina	Nem védett / indifferens
Psilota innupta	Nem védett / indifferens
Psithyrus barbutellus	Nem védett / indifferens
Psithyrus barbutellus maxillosus	Nem védett / indifferens
Psithyrus bohemicus	Nem védett / indifferens
Psithyrus campestris	Nem védett / indifferens
Psithyrus quadricolor	Nem védett / indifferens
Psithyrus rupestris	Nem védett / indifferens
Psithyrus sylvestris	Nem védett / indifferens
Psithyrus vestalis	Nem védett / indifferens
Psoa viennensis	Nem védett / indifferens
Psophus stridulus	Nem védett / indifferens
Psoricoptera gibbosella	Nem védett / indifferens
Psorosa dahliella	Nem védett / indifferens
Psyche casta	Nem védett / indifferens
Psyche crassiorella	Nem védett / indifferens
Psychidea nudella	Nem védett / indifferens
Acentra vestalis	Nem védett / indifferens
Psychoides verhuella	Nem védett / indifferens
Psychomyia pusilla	Nem védett / indifferens
Psychrodromus olivaceus	Nem védett / indifferens
Psyllobora vigintiduopunctata	Nem védett / indifferens
Pteleobius kraatzii	Nem védett / indifferens
Pteleobius vittatus	Nem védett / indifferens
Ptenidium formicetorum	Nem védett / indifferens
Ptenidium fuscicorne	Nem védett / indifferens
Ptenidium gressneri	Nem védett / indifferens
Ptenidium intermedium	Nem védett / indifferens
Ptenidium laevigatum	Nem védett / indifferens
Ptenidium longicorne	Nem védett / indifferens
Ptenidium nitidum	Nem védett / indifferens
Ptenidium pusillum	Nem védett / indifferens
Ptenidium turgidum	Nem védett / indifferens
Pterapherapteryx sexalata	Nem védett / indifferens
Pterocheilus phaleratus	Nem védett / indifferens
Pterocheilus phaleratus chevrieranus	Nem védett / indifferens
Pterocles exustus	Védett
Pterolonche albescens	Nem védett / indifferens
Pterolonche inspersa	Nem védett / indifferens
Pteronemobius heydenii	Nem védett / indifferens
Pterophorus ischnodactyla	Nem védett / indifferens
Pterophorus pentadactyla	Nem védett / indifferens
Pterostichus aethiops	Nem védett / indifferens
Pterostichus anthracinus biimpressus	Nem védett / indifferens
Pterostichus aterrimus	Nem védett / indifferens
Pterostichus burmeisteri	Nem védett / indifferens
Pterostichus calvitarsis	Nem védett / indifferens
Pterostichus chameleon	Nem védett / indifferens
Pterostichus cursor	Nem védett / indifferens
Pterostichus cylindricus	Nem védett / indifferens
Pterostichus diligens	Nem védett / indifferens
Pterostichus elongatus	Nem védett / indifferens
Pterostichus fasciatopunctatus	Nem védett / indifferens
Pterostichus gracilis	Nem védett / indifferens
Pterostichus hungaricus	Nem védett / indifferens
Pterostichus incommodus	Nem védett / indifferens
Pedius inquinatus	Nem védett / indifferens
Pterostichus leonisi	Nem védett / indifferens
Pedius longicollis	Nem védett / indifferens
Pterostichus macer	Nem védett / indifferens
Pterostichus melanarius	Nem védett / indifferens
Pterostichus melas	Nem védett / indifferens
Pterostichus minor	Nem védett / indifferens
Pterostichus niger	Nem védett / indifferens
Pterostichus nigrita	Nem védett / indifferens
Pterostichus oblongopunctatus	Nem védett / indifferens
Pterostichus ovoideus	Nem védett / indifferens
Pterostichus piceolus	Nem védett / indifferens
Pterostichus piceolus latoricaensis	Nem védett / indifferens
Pterostichus rhaeticus	Nem védett / indifferens
Pterostichus strenuus	Nem védett / indifferens
Pterostichus subsinuatus	Nem védett / indifferens
Pterostichus taksonyis	Nem védett / indifferens
Pterostichus transversalis	Nem védett / indifferens
Pterostichus unctulatus	Nem védett / indifferens
Pterostichus vernalis	Nem védett / indifferens
Pterostoma palpina	Nem védett / indifferens
Pterothrixidia impurella	Nem védett / indifferens
Pterothrixidia rufella	Nem védett / indifferens
Pterotmetus staphyliniformis	Nem védett / indifferens
Pterotopteryx dodecadactyla	Nem védett / indifferens
Pteryx suturalis	Nem védett / indifferens
Ptilinus fuscus	Nem védett / indifferens
Ptilinus pectinicornis	Nem védett / indifferens
Ptiliola brevicollis	Nem védett / indifferens
Ptiliola kunzei	Nem védett / indifferens
Ptiliolum caledonicum	Nem védett / indifferens
Ptiliolum fuscum	Nem védett / indifferens
Ptiliolum marginatum	Nem védett / indifferens
Ptiliolum schwarzi	Nem védett / indifferens
Ptiliolum spencei	Nem védett / indifferens
Ptilium affine	Nem védett / indifferens
Ptilium caesum	Nem védett / indifferens
Ptilium exaratum	Nem védett / indifferens
Ptilium minutissimum	Nem védett / indifferens
Ptilium modestum	Nem védett / indifferens
Ptilium myrmecophilum	Nem védett / indifferens
Ptilocephala muscella	Nem védett / indifferens
Ptilocephala plumifera	Nem védett / indifferens
Ptilocolepus granulatus	Nem védett / indifferens
Ptilodon capucina	Nem védett / indifferens
Ptilodon cucullina	Nem védett / indifferens
Ptilophora plumigera	Nem védett / indifferens
Ptilophorus dufouri	Védett
Ptinella aptera	Nem védett / indifferens
Ptinella britannica	Nem védett / indifferens
Ptinella limbata	Nem védett / indifferens
Ptinella tenella	Nem védett / indifferens
Ptinomorphus imperialis	Nem védett / indifferens
Ptinomorphus regalis	Nem védett / indifferens
Ptinus bicinctus	Nem védett / indifferens
Ptinus calcaratus	Nem védett / indifferens
Ptinus capellae	Nem védett / indifferens
Ptinus clavipes	Nem védett / indifferens
Ptinus coarcticollis	Nem védett / indifferens
Ptinus dubius	Nem védett / indifferens
Ptinus fur	Nem védett / indifferens
Ptinus latro	Nem védett / indifferens
Ptinus pilosus	Nem védett / indifferens
Ptinus podolicus	Nem védett / indifferens
Ptinus rufipes	Nem védett / indifferens
Ptinus schlerethi	Nem védett / indifferens
Ptinus sexpunctatus	Nem védett / indifferens
Ptinus subpilosus	Nem védett / indifferens
Ptinus tectus	Nem védett / indifferens
Ptinus variegatus	Nem védett / indifferens
Ptinus villiger	Nem védett / indifferens
Ptocheuusa abnormella	Nem védett / indifferens
Ptocheuusa inopella	Nem védett / indifferens
Ptocheuusa paupella	Nem védett / indifferens
Ptomaphagus sericatus	Nem védett / indifferens
Ptomaphagus subvillosus	Nem védett / indifferens
Ptomaphagus validus	Nem védett / indifferens
Ptomaphagus varicornis	Nem védett / indifferens
Ptosima undecimmaculata	Nem védett / indifferens
Ptycholoma lecheana	Nem védett / indifferens
Ptycholomoides aeriferana	Nem védett / indifferens
Pulvinaria betulae	Nem védett / indifferens
Pulvinaria populi	Nem védett / indifferens
Pulvinaria ribesiae	Nem védett / indifferens
Pulvinaria vitis	Nem védett / indifferens
Puncha ratzeburgi	Nem védett / indifferens
Punctum pygmaeum	Nem védett / indifferens
Pungeleria capreolaria	Nem védett / indifferens
Pupilla muscorum	Nem védett / indifferens
Pupilla sterrii	Nem védett / indifferens
Pupilla triplicata	Nem védett / indifferens
Purpuricenus budensis	Védett
Purpuricenus globulicollis	Védett
Purpuricenus kaehleri	Védett
Puto antennatus	Nem védett / indifferens
Puto janetscheki	Nem védett / indifferens
Puto pilosellae	Nem védett / indifferens
Puto superbus	Nem védett / indifferens
Pycnomerus terebrans	Nem védett / indifferens
Pycnota paradoxa	Nem védett / indifferens
Pygolampis bidentata	Nem védett / indifferens
Pygolampis laticaput	Nem védett / indifferens
Matilella fusca	Nem védett / indifferens
Pyncostola bohemiella	Nem védett / indifferens
Pyralis farinalis	Nem védett / indifferens
Pyralis perversalis	Nem védett / indifferens
Pyralis regalis	Nem védett / indifferens
Pyramidula pusilla	Nem védett / indifferens
Pyrausta aurata	Nem védett / indifferens
Pyrausta castalis	Nem védett / indifferens
Pyrausta cingulata	Nem védett / indifferens
Pyrausta coracinalis	Nem védett / indifferens
Pyrausta despicata	Nem védett / indifferens
Pyrausta falcatalis	Nem védett / indifferens
Pyrausta ledereri	Nem védett / indifferens
Pyrausta nigrata	Nem védett / indifferens
Pyrausta obfuscata	Nem védett / indifferens
Pyrausta ostrinalis	Nem védett / indifferens
Pyrausta porphyralis	Nem védett / indifferens
Pyrausta purpuralis	Nem védett / indifferens
Pyrausta rectefascialis	Nem védett / indifferens
Pyrausta sanguinalis	Nem védett / indifferens
Pyrausta virginalis	Nem védett / indifferens
Pyrgus alveus	Védett
Pyrgus armoricanus	Nem védett / indifferens
Pyrgus carthami	Nem védett / indifferens
Pyrgus malvae	Nem védett / indifferens
Pyrgus serratulae	Védett
Pyrochroa coccinea	Nem védett / indifferens
Pyrochroa serraticornis	Nem védett / indifferens
Pyroderces argyrogrammos	Nem védett / indifferens
Pyroderces klimeschi	Nem védett / indifferens
Pyrois cinnamomea	Védett
Pyronia tithonus	Védett
Pyrophaena granditarsa	Nem védett / indifferens
Pyrophaena rosarum	Nem védett / indifferens
Pyropterus nigroruber	Nem védett / indifferens
Pyrrhia purpurina	Védett
Pyrrhia umbra	Nem védett / indifferens
Pyrrhidium sanguineum	Nem védett / indifferens
Pyrrhocorax graculus	Védett
Pyrrhocorax pyrrhocorax	Védett
Pyrrhocorax pyrrhocorax erythrorhamphus	Védett
Pyrrhocoris apterus	Nem védett / indifferens
Pyrrhocoris marginatus	Nem védett / indifferens
Pyrrhosoma nymphula	Nem védett / indifferens
Pyrrhosoma nymphula interposita	Nem védett / indifferens
Pyrrhula pyrrhula	Védett
Pytho depressus	Védett
Diaspidiotus gigas	Nem védett / indifferens
Quadraspidiotus labiatarum	Nem védett / indifferens
Diaspidiotus lenticularis	Nem védett / indifferens
Diaspidiotus marani	Nem védett / indifferens
Diaspidiotus ostreaeformis	Nem védett / indifferens
Diaspidiotus perniciosus	Nem védett / indifferens
Diaspidiotus pyri	Nem védett / indifferens
Diaspidiotus sulci	Nem védett / indifferens
Diaspidiotus zonatus	Nem védett / indifferens
Kervillea conspurcata	Nem védett / indifferens
Quasimus minutissimus	Nem védett / indifferens
Quedius balticus	Nem védett / indifferens
Quedius boops	Nem védett / indifferens
Quedius brevicornis	Nem védett / indifferens
Quedius brevis	Nem védett / indifferens
Quedius cincticollis	Nem védett / indifferens
Quedius cinctus	Nem védett / indifferens
Quedius cruentus	Nem védett / indifferens
Quedius curtipennis	Nem védett / indifferens
Quedius dubius	Nem védett / indifferens
Quedius dubius fimbriatus	Nem védett / indifferens
Quedius fulgidus	Nem védett / indifferens
Quedius fuliginosus	Nem védett / indifferens
Quedius fumatus	Nem védett / indifferens
Quedius lateralis	Nem védett / indifferens
Quedius levicollis	Nem védett / indifferens
Quedius limbatus	Nem védett / indifferens
Quedius longicornis	Nem védett / indifferens
Quedius lucidulus	Nem védett / indifferens
Quedius maurorufus	Nem védett / indifferens
Quedius maurus	Nem védett / indifferens
Quedius meridiocarpathicus	Nem védett / indifferens
Quedius mesomelinus	Nem védett / indifferens
Quedius microps	Nem védett / indifferens
Quedius molochinus	Nem védett / indifferens
Quedius nemoralis	Nem védett / indifferens
Quedius nigriceps	Nem védett / indifferens
Quedius nitipennis	Nem védett / indifferens
Quedius ochripennis	Nem védett / indifferens
Quedius ochropterus	Nem védett / indifferens
Quedius paradisianus	Nem védett / indifferens
Quedius picipes	Nem védett / indifferens
Quedius plagiatus	Nem védett / indifferens
Quedius puncticollis	Nem védett / indifferens
Quedius riparius	Nem védett / indifferens
Quedius scintillans	Nem védett / indifferens
Quedius scitus	Nem védett / indifferens
Quedius semiaeneus	Nem védett / indifferens
Quedius semiobscurus	Nem védett / indifferens
Quedius skoraszewskyi	Nem védett / indifferens
Quedius suturalis	Nem védett / indifferens
Quedius truncicola	Nem védett / indifferens
Quedius umbrinus	Nem védett / indifferens
Quedius vexans	Nem védett / indifferens
Quedius xanthopus	Nem védett / indifferens
Rabigus pullus	Nem védett / indifferens
Rabigus tenuis	Nem védett / indifferens
Rabdocerus foveolatus	Nem védett / indifferens
Rabdocerus gabrieli	Nem védett / indifferens
Radix ampla	Nem védett / indifferens
Radix auricularia	Nem védett / indifferens
Radix balthica	Nem védett / indifferens
Radix labiata	Nem védett / indifferens
Raglius alboacuminatus	Nem védett / indifferens
Raglius confusus	Nem védett / indifferens
Rhyparochromus vulgaris	Nem védett / indifferens
Rallus aquaticus	Védett
Rana arvalis	Védett
Rana arvalis wolterstorffi	Védett
Rana dalmatina	Védett
Rana kl. esculenta	Védett
Rana lessonae	Védett
Rana ridibunda	Védett
Rana temporaria	Védett
Ranatra linearis	Nem védett / indifferens
Ranunculiphilus faeculentus	Nem védett / indifferens
Ranunculiphilus italicus	Nem védett / indifferens
Raphidia ophiopsis	Nem védett / indifferens
Raphidia ophiopsis mediterranea	Nem védett / indifferens
Raphidia ulrikae	Nem védett / indifferens
Rattus norvegicus	Nem védett / indifferens
Rattus rattus	Nem védett / indifferens
Rebelia herrichiella	Nem védett / indifferens
Rebelia sapho	Nem védett / indifferens
Rebelia surientella	Nem védett / indifferens
Recilia coronifera	Nem védett / indifferens
Recilia schmidtgeni	Nem védett / indifferens
Recurvaria leucatella	Nem védett / indifferens
Recurvaria nanella	Nem védett / indifferens
Recurvirostra avosetta	Fokozottan védett
Reduvius personatus	Nem védett / indifferens
Reesa vespulae	Nem védett / indifferens
Regulus ignicapillus	Védett
Regulus regulus	Védett
Reisserita relicinella	Nem védett / indifferens
Reitterelater bouyoni	Nem védett / indifferens
Reitterelater dubius	Nem védett / indifferens
Remiz pendulinus	Védett
Reptalus apiculatus	Nem védett / indifferens
Reptalus melanochaetus	Nem védett / indifferens
Reptalus panzeri	Nem védett / indifferens
Reptalus quinquecostatus	Nem védett / indifferens
Evergestis alborivulalis	Védett
Retinia resinella	Nem védett / indifferens
Reuteria marqueti	Nem védett / indifferens
Rhabdiopteryx acuminata	Védett
Rhabdiopteryx hamulata	Fokozottan védett
Rhabdomiris striatellus	Nem védett / indifferens
Rhabdorrhynchus varius	Nem védett / indifferens
Pterolepis germanica	Nem védett / indifferens
Rhacognathus punctatus	Nem védett / indifferens
Dirrhagofarsus attenuatus	Nem védett / indifferens
Rhacopus sahlbergi	Nem védett / indifferens
Rhadicoleptus alpestris	Nem védett / indifferens
Rhadicoleptus alpestris sylvanocarpathicus	Nem védett / indifferens
Rhagades pruni	Nem védett / indifferens
Rhagium bifasciatum	Nem védett / indifferens
Rhagium inquisitor	Nem védett / indifferens
Rhagium mordax	Nem védett / indifferens
Rhagium sycophanta	Nem védett / indifferens
Rhagocneme subsinuata	Nem védett / indifferens
Rhagonycha atra	Nem védett / indifferens
Rhagonycha elongata	Nem védett / indifferens
Rhagonycha femoralis	Nem védett / indifferens
Rhagonycha fulva	Nem védett / indifferens
Rhagonycha gallica	Nem védett / indifferens
Rhagonycha lignosa	Nem védett / indifferens
Rhagonycha limbata	Nem védett / indifferens
Rhagonycha lutea	Nem védett / indifferens
Rhagonycha nigriceps	Nem védett / indifferens
Rhagonycha nigripes	Nem védett / indifferens
Rhagonycha rorida	Nem védett / indifferens
Rhagonycha testacea	Nem védett / indifferens
Rhagonycha translucida	Nem védett / indifferens
Rhamnusium bicolor	Védett
Rhamphus oxyacanthae	Nem védett / indifferens
Rhamphus pulicarius	Nem védett / indifferens
Rhamphus subaeneus	Nem védett / indifferens
Rhantus bistriatus	Nem védett / indifferens
Rhantus consputus	Nem védett / indifferens
Rhantus frontalis	Nem védett / indifferens
Rhantus grapii	Nem védett / indifferens
Rhantus latitans	Nem védett / indifferens
Rhantus suturalis	Nem védett / indifferens
Rhantus suturellus	Nem védett / indifferens
Rhaphigaster nebulosa	Nem védett / indifferens
Rhaphitropis marchica	Nem védett / indifferens
Hydria cervinalis	Nem védett / indifferens
Rheumaptera hastata	Nem védett / indifferens
Hydria undulata	Védett
Rhigognostis hufnagelii	Nem védett / indifferens
Rhigognostis incarnatella	Nem védett / indifferens
Rhigognostis kovacsi	Nem védett / indifferens
Rhigognostis senilella	Nem védett / indifferens
Rhingia campestris	Nem védett / indifferens
Rhingia rostrata	Nem védett / indifferens
Rhinocyllus conicus	Nem védett / indifferens
Rhinolophus euryale	Fokozottan védett
Rhinolophus ferrumequinum	Fokozottan védett
Rhinolophus hipposideros	Védett
Rhinomias austriacus	Nem védett / indifferens
Rhinomias forticornis	Nem védett / indifferens
Rhinomias maxillosus	Nem védett / indifferens
Rhinomias viertli	Nem védett / indifferens
Rhinoncus albicinctus	Nem védett / indifferens
Rhinoncus bosnicus	Nem védett / indifferens
Rhinoncus bruchoides	Nem védett / indifferens
Rhinoncus castor	Nem védett / indifferens
Rhinoncus inconspectus	Nem védett / indifferens
Rhinoncus pericarpius	Nem védett / indifferens
Rhinoncus perpendicularis	Nem védett / indifferens
Rhinusa antirrhini	Nem védett / indifferens
Rhinusa asellus	Nem védett / indifferens
Rhinusa bipustulata	Nem védett / indifferens
Rhinusa collinum	Nem védett / indifferens
Rhinusa herbarum	Nem védett / indifferens
Rhinusa hispidum	Nem védett / indifferens
Rhinusa linariae	Nem védett / indifferens
Rhinusa littoreum	Nem védett / indifferens
Rhinusa melas	Nem védett / indifferens
Aulacobaris neta	Nem védett / indifferens
Rhinusa smreczynskii	Nem védett / indifferens
Rhinusa tetra	Nem védett / indifferens
Rhinusa thapsicola	Nem védett / indifferens
Rhithrogena beskidensis	Nem védett / indifferens
Rhithrogena germanica	Nem védett / indifferens
Rhithrogena iridina	Nem védett / indifferens
Rhithrogena picteti	Nem védett / indifferens
Rhithrogena puytoraci	Nem védett / indifferens
Rhithrogena semicolorata	Nem védett / indifferens
Rhizaspidiotus canariensis	Nem védett / indifferens
Rhizedra lutosa	Nem védett / indifferens
Acanthococcus agropyri	Nem védett / indifferens
Rhizococcus cingulatus	Nem védett / indifferens
Acanthococcus cynodontis	Nem védett / indifferens
Rhizococcus herbaceus	Nem védett / indifferens
Acanthococcus insignis	Nem védett / indifferens
Rhizococcus palustris	Nem védett / indifferens
Acanthococcus pseudinsignis	Nem védett / indifferens
Rhizoecus albidus	Nem védett / indifferens
Rhizoecus caesii	Nem védett / indifferens
Rhizoecus franconiae	Nem védett / indifferens
Rhizoecus halophilus	Nem védett / indifferens
Rhizoecus poltavae	Nem védett / indifferens
Rhizophagus bipustulatus	Nem védett / indifferens
Rhizophagus brancsiki	Nem védett / indifferens
Rhizophagus cribratus	Nem védett / indifferens
Rhizophagus depressus	Nem védett / indifferens
Rhizophagus dispar	Nem védett / indifferens
Rhizophagus ferrugineus	Nem védett / indifferens
Rhizophagus nitidulus	Nem védett / indifferens
Rhizophagus parallelocollis	Nem védett / indifferens
Rhizophagus parvulus	Nem védett / indifferens
Rhizophagus perforatus	Nem védett / indifferens
Rhizophagus picipes	Nem védett / indifferens
Rhizopulvinaria artemisiae	Nem védett / indifferens
Rhizopulvinaria spinifera	Nem védett / indifferens
Holochelus aestivus	Nem védett / indifferens
Rhoananus hypochlorus	Nem védett / indifferens
Rhodania occulta	Nem védett / indifferens
Rhodania porifera	Nem védett / indifferens
Rhodeus amarus	Védett
Rhodococcus perornatus	Nem védett / indifferens
Rhodococcus spiraeae	Nem védett / indifferens
Rhodometra sacraria	Nem védett / indifferens
Rhodostrophia vibicaria	Nem védett / indifferens
Rhopalapion longirostre	Nem védett / indifferens
Rhopalocerina clavigera	Nem védett / indifferens
Rhopalocerus rondanii	Nem védett / indifferens
Rhopalodontus baudueri	Nem védett / indifferens
Rhopalodontus perforatus	Nem védett / indifferens
Rhopalopyx brachyanus	Nem védett / indifferens
Rhopalopyx preyssleri	Nem védett / indifferens
Rhopalopyx vitripennis	Nem védett / indifferens
Rhopalotella validiuscula	Nem védett / indifferens
Rhopalum austriacum	Nem védett / indifferens
Rhopalum beaumonti	Nem védett / indifferens
Rhopalum clavipes	Nem védett / indifferens
Rhopalum coarctatum	Nem védett / indifferens
Rhopalum gracile	Nem védett / indifferens
Rhopalus conspersus	Nem védett / indifferens
Rhopalus distinctus	Nem védett / indifferens
Rhopalus maculatus	Nem védett / indifferens
Rhopalus parumpunctatus	Nem védett / indifferens
Rhopalus subrufus	Nem védett / indifferens
Rhophitoides canus	Nem védett / indifferens
Rhopobota myrtillana	Nem védett / indifferens
Rhopobota naevana	Nem védett / indifferens
Rhopobota stagnana	Nem védett / indifferens
Rhyacia lucipeta	Nem védett / indifferens
Rhyacia simulans	Nem védett / indifferens
Rhyacionia buoliana	Nem védett / indifferens
Rhyacionia duplana	Nem védett / indifferens
Rhyacionia piniana	Nem védett / indifferens
Rhyacionia pinicolana	Nem védett / indifferens
Rhyacionia pinivorana	Nem védett / indifferens
Rhyacophila dorsalis persimilis	Nem védett / indifferens
Rhyacophila fasciata	Nem védett / indifferens
Rhyacophila hirticornis	Védett
Rhyacophila laevis	Nem védett / indifferens
Rhyacophila nubila	Nem védett / indifferens
Rhyacophila obliterata	Nem védett / indifferens
Rhyacophila pascoei	Nem védett / indifferens
Rhyacophila polonica	Nem védett / indifferens
Rhyacophila pubescens	Nem védett / indifferens
Rhyacophila tristis	Nem védett / indifferens
Rhynchaenus alni	Nem védett / indifferens
Aulacobaris erythropus	Nem védett / indifferens
Orchestes hungaricus	Nem védett / indifferens
Rhynchaenus jota	Nem védett / indifferens
Rhynchaenus lonicerae	Nem védett / indifferens
Rhynchaenus pilosus	Nem védett / indifferens
Orchestes quedenfeldtii	Nem védett / indifferens
Orchestes quercus	Nem védett / indifferens
Rhynchaenus rufus	Nem védett / indifferens
Rhynchaenus sparsus	Nem védett / indifferens
Rhynchites auratus	Nem védett / indifferens
Rhynchites bacchus	Nem védett / indifferens
Rhynchites giganteus	Nem védett / indifferens
Rhynchites lenaeus	Nem védett / indifferens
Rhynchotalona falcata	Nem védett / indifferens
Rhyncolus ater	Nem védett / indifferens
Rhyncolus elongatus	Nem védett / indifferens
Rhyncolus punctatulus	Nem védett / indifferens
Rhyncolus reflexus	Nem védett / indifferens
Rhyncolus sculpturatus	Nem védett / indifferens
Rhynocoris annulatus	Nem védett / indifferens
Rhynocoris iracundus	Nem védett / indifferens
Rhynocoris niger	Nem védett / indifferens
Rhyparia purpurata	Nem védett / indifferens
Rhyparioides metelkana	Nem védett / indifferens
Rhyparochromus pini	Nem védett / indifferens
Rhyparochromus sanguineus	Nem védett / indifferens
Rhyssemus germanus	Nem védett / indifferens
Rhytidodus decimusquartus	Nem védett / indifferens
Rhytidodus nobilis	Nem védett / indifferens
Oxynychus chrysomeloides	Nem védett / indifferens
Oxynychus litura	Nem védett / indifferens
Rhyzopertha dominica	Nem védett / indifferens
Ribautiana alces	Nem védett / indifferens
Ribautiana ognevi	Nem védett / indifferens
Ribautiana scalaris	Nem védett / indifferens
Ribautiana tenerrima	Nem védett / indifferens
Ribautiana ulmi	Nem védett / indifferens
Ribautodelphax affinis	Nem védett / indifferens
Ribautodelphax albostriatus	Nem védett / indifferens
Ribautodelphax angulosus	Nem védett / indifferens
Ribautodelphax collinus	Nem védett / indifferens
Ribautodelphax imitans	Nem védett / indifferens
Ribautodelphax pungens	Nem védett / indifferens
Rileyiana fovea	Védett
Riolus cupreus	Nem védett / indifferens
Riolus subviolaceus	Nem védett / indifferens
Riparia riparia	Védett
Rissa tridactyla	Védett
Ritsemia pupifera	Nem védett / indifferens
Rivula sericealis	Nem védett / indifferens
Robertus arundineti	Nem védett / indifferens
Robertus lividus	Nem védett / indifferens
Robertus neglectus	Nem védett / indifferens
Roeslerstammia erxlebella	Nem védett / indifferens
Roeslerstammia pronubella	Nem védett / indifferens
Ropalopus clavipes	Nem védett / indifferens
Ropalopus femoratus	Nem védett / indifferens
Ropalopus insubricus	Védett
Ropalopus macropus	Nem védett / indifferens
Ropalopus ungaricus	Védett
Ropalopus varini	Védett
Rophites algirus	Nem védett / indifferens
Rophites algirus trispinosus	Nem védett / indifferens
Rophites hartmanni	Nem védett / indifferens
Rophites quinquespinosus	Nem védett / indifferens
Rosalia alpina	Védett
Rubiconia intermedia	Nem védett / indifferens
Rugathodes bellicosus	Nem védett / indifferens
Rugathodes instabilis	Nem védett / indifferens
Rugilus angustatus	Nem védett / indifferens
Rugilus erichsonii	Nem védett / indifferens
Rugilus mixtus	Nem védett / indifferens
Rugilus orbiculatus	Nem védett / indifferens
Rugilus rufipes	Nem védett / indifferens
Rugilus similis	Nem védett / indifferens
Rugilus subtilis	Nem védett / indifferens
Runcinia grammica	Nem védett / indifferens
Rushia parreyssii	Nem védett / indifferens
Rusina ferruginea	Nem védett / indifferens
Ruteria hypocrita	Nem védett / indifferens
Ruthenica filograna	Védett
Rutidosoma fallax	Nem védett / indifferens
Rutidosoma globulus	Nem védett / indifferens
Rutilus frisii	Védett
Rutilus virgo	Védett
Rutilus rutilus	Nem védett / indifferens
Rutpela maculata	Nem védett / indifferens
Rybaxis longicornis	Nem védett / indifferens
Rypobius praetermissus	Nem védett / indifferens
Sabanejewia aurata	Védett
Sabanejewia aurata balcanica	Védett
Sabanejewia aurata bulgarica	Védett
Sabra harpagula	Nem védett / indifferens
Saccharicoccus penium	Nem védett / indifferens
Bythinella pannonica	Védett
Saga pedo	Védett
Sagatus punctifrons	Nem védett / indifferens
Sahlbergotettix salicicola	Nem védett / indifferens
Salamandra salamandra	Védett
Salda muelleri	Nem védett / indifferens
Saldula arenicola	Nem védett / indifferens
Saldula c-album	Nem védett / indifferens
Saldula melanoscela	Nem védett / indifferens
Saldula nitidula	Nem védett / indifferens
Saldula opacula	Nem védett / indifferens
Saldula orthochila	Nem védett / indifferens
Saldula pallipes	Nem védett / indifferens
Saldula palustris	Nem védett / indifferens
Saldula pilosella	Nem védett / indifferens
Saldula saltatoria	Nem védett / indifferens
Saldula xanthochila	Nem védett / indifferens
Salebriopsis albicilla	Nem védett / indifferens
Salicarus roseri	Nem védett / indifferens
Salmincola salmonea	Nem védett / indifferens
Salmo trutta	Nem védett / indifferens
Saloca diceros	Nem védett / indifferens
Saloca kulczynskii	Nem védett / indifferens
Salpingus aeneus	Nem védett / indifferens
Salpingus planirostris	Nem védett / indifferens
Salpingus ruficollis	Nem védett / indifferens
Salticus cingulatus	Nem védett / indifferens
Salticus quagga	Nem védett / indifferens
Salticus scenicus	Nem védett / indifferens
Salticus zebraneus	Nem védett / indifferens
Salvelinus fontinalis	Nem védett / indifferens
Sander lucioperca	Nem védett / indifferens
Sander volgensis	Nem védett / indifferens
Saperda octopunctata	Védett
Saperda perforata	Védett
Saperda punctata	Védett
Saperda scalaris	Védett
Saphanus piceus	Nem védett / indifferens
Saprinus aegialius	Nem védett / indifferens
Saprinus aeneus	Nem védett / indifferens
Saprinus cribellatus	Nem védett / indifferens
Saprinus furvus	Nem védett / indifferens
Saprinus georgicus	Nem védett / indifferens
Saprinus immundus	Nem védett / indifferens
Saprinus lautus	Nem védett / indifferens
Saprinus maculatus	Nem védett / indifferens
Saprinus pharao	Nem védett / indifferens
Saprinus planiusculus	Nem védett / indifferens
Saprinus quadristriatus	Nem védett / indifferens
Saprinus semipunctatus	Nem védett / indifferens
Saprinus semistriatus	Nem védett / indifferens
Saprinus subnitescens	Nem védett / indifferens
Saprinus tenuistrius	Nem védett / indifferens
Saprinus tenuistrius sparsutus	Nem védett / indifferens
Saprinus vermiculatus	Nem védett / indifferens
Saprinus virescens	Nem védett / indifferens
Sapyga clavicornis	Nem védett / indifferens
Sapyga quinquepunctata	Nem védett / indifferens
Sapygina decemguttata	Nem védett / indifferens
Saragossa porosa	Védett
Saragossa porosa kenderesiensis	Védett
Sarscypridopsis aculeata	Nem védett / indifferens
Satrapes sartorii	Nem védett / indifferens
Saturnia pavonia	Védett
Saturnia pyri	Védett
Saturnia spini	Védett
Satyrium acaciae	Nem védett / indifferens
Satyrium ilicis	Védett
Satyrium pruni	Védett
Satyrium spini	Védett
Satyrium w-album	Védett
Sauron rayi	Nem védett / indifferens
Sauterina hofmanniella	Nem védett / indifferens
Saxicola rubetra	Védett
Saxicola torquata	Védett
Saxicola rubicola	Védett
Scaeva dignota	Nem védett / indifferens
Scaeva pyrastri	Nem védett / indifferens
Scaeva selenitica	Nem védett / indifferens
Scaphidema metallicum	Nem védett / indifferens
Scaphidium quadrimaculatum	Nem védett / indifferens
Scaphisoma agaricinum	Nem védett / indifferens
Scaphisoma assimile	Nem védett / indifferens
Scaphisoma balcanicum	Nem védett / indifferens
Scaphisoma boleti	Nem védett / indifferens
Scaphisoma boreale	Nem védett / indifferens
Scaphisoma inopinatum	Nem védett / indifferens
Scaphisoma obenbergeri	Nem védett / indifferens
Scaphisoma subalpinum	Nem védett / indifferens
Scaphium immaculatum	Nem védett / indifferens
Scapholeberis erinaceus	Nem védett / indifferens
Scapholeberis mucronata	Nem védett / indifferens
Scapholeberis rammneri	Nem védett / indifferens
Scarabaeus pius	Védett
Scarabaeus typhon	Védett
Scardia boletella	Nem védett / indifferens
Scardinius erythrophthalmus	Nem védett / indifferens
Parallelomorphus terricola	Védett
Scarodytes halensis	Nem védett / indifferens
Sceliphron curvatum	Nem védett / indifferens
Sceliphron destillatorium	Nem védett / indifferens
Sceliphron spirifex	Nem védett / indifferens
Schellencandona insueta	Nem védett / indifferens
Schendyla nemorensis	Nem védett / indifferens
Schendyla zonalis	Nem védett / indifferens
Schiffermuelleria schaefferella	Nem védett / indifferens
Schiffermuelleria grandis	Nem védett / indifferens
Schinia cardui	Védett
Schinia cognata	Védett
Protoschinia scutosa	Nem védett / indifferens
Schistoglossa gemina	Nem védett / indifferens
Schistoglossa viduata	Nem védett / indifferens
Schistostege decussata	Védett
Schizotus pectinicornis	Védett
Schoenobius gigantella	Nem védett / indifferens
Schrankia costaestrigalis	Nem védett / indifferens
Schrankia taenialis	Nem védett / indifferens
Schreckensteinia festaliella	Nem védett / indifferens
Sciaphilus asperatus	Nem védett / indifferens
Sciaphobus barbatulus	Nem védett / indifferens
Sciaphobus caesius	Nem védett / indifferens
Sciaphobus scitulus	Nem védett / indifferens
Sciaphobus squalidus	Nem védett / indifferens
Scymbalium anale	Nem védett / indifferens
Sciocoris cursitans	Nem védett / indifferens
Sciocoris deltocephalus	Nem védett / indifferens
Sciocoris distinctus	Nem védett / indifferens
Sciocoris homalonotus	Nem védett / indifferens
Sciocoris macrocephalus	Nem védett / indifferens
Sciocoris microphthalmus	Nem védett / indifferens
Sciocoris sulcatus	Nem védett / indifferens
Sciodrepoides fumatus	Nem védett / indifferens
Sciodrepoides watsoni	Nem védett / indifferens
Scirpophaga praelata	Nem védett / indifferens
Scirtes hemisphaericus	Nem védett / indifferens
Scirtes orbicularis	Nem védett / indifferens
Sciurus vulgaris	Védett
Sclerocona acutella	Nem védett / indifferens
Scleropterus serratus	Nem védett / indifferens
Scobicia chevrieri	Nem védett / indifferens
Scolia hirta	Nem védett / indifferens
Scolia insubrica	Nem védett / indifferens
Scolia sexmaculata	Nem védett / indifferens
Scolia sexmaculata quadripunctata	Nem védett / indifferens
Scoliopteryx libatrix	Nem védett / indifferens
Scolitantides orion	Védett
Pseudophilotes vicrama schiffermuelleri	Védett
Scolopax rusticola	Nem védett / indifferens
Scolopendra cingulata	Védett
Scolopostethus affinis	Nem védett / indifferens
Scolopostethus cognatus	Nem védett / indifferens
Scolopostethus decoratus	Nem védett / indifferens
Scolopostethus grandis	Nem védett / indifferens
Scolopostethus lethierryi	Nem védett / indifferens
Scolopostethus pictus	Nem védett / indifferens
Scolopostethus pilosus	Nem védett / indifferens
Scolopostethus puberulus	Nem védett / indifferens
Scolopostethus thomsoni	Nem védett / indifferens
Scolytus carpini	Nem védett / indifferens
Scolytus ensifer	Nem védett / indifferens
Scolytus intricatus	Nem védett / indifferens
Scolytus kirschii	Nem védett / indifferens
Scolytus koenigi	Nem védett / indifferens
Scolytus laevis	Nem védett / indifferens
Scolytus mali	Nem védett / indifferens
Scolytus multistriatus	Nem védett / indifferens
Scolytus pygmaeus	Nem védett / indifferens
Scolytus ratzeburgii	Nem védett / indifferens
Scolytus rugulosus	Nem védett / indifferens
Scolytus scolytus	Nem védett / indifferens
Scopaeus bicolor	Nem védett / indifferens
Scopaeus debilis	Nem védett / indifferens
Scopaeus didymus	Nem védett / indifferens
Scopaeus furcatus	Nem védett / indifferens
Scopaeus gracilis	Nem védett / indifferens
Scopaeus laevigatus	Nem védett / indifferens
Scopaeus minimus	Nem védett / indifferens
Scopaeus minutus	Nem védett / indifferens
Scopaeus pusillus	Nem védett / indifferens
Scopaeus ryei	Nem védett / indifferens
Scopaeus sulcicollis	Nem védett / indifferens
Scoparia ambigualis	Nem védett / indifferens
Scoparia ancipitella	Nem védett / indifferens
Scoparia basistrigalis	Nem védett / indifferens
Scoparia conicella	Nem védett / indifferens
Scoparia ingratella	Nem védett / indifferens
Scoparia luteolaris	Nem védett / indifferens
Scoparia manifestella	Nem védett / indifferens
Scoparia pyralella	Nem védett / indifferens
Scoparia subfusca	Nem védett / indifferens
Scopula caricaria	Nem védett / indifferens
Scopula corrivalaria	Nem védett / indifferens
Scopula decorata	Nem védett / indifferens
Scopula flaccidaria	Nem védett / indifferens
Scopula floslactata	Nem védett / indifferens
Scopula immorata	Nem védett / indifferens
Scopula immutata	Nem védett / indifferens
Scopula incanata	Nem védett / indifferens
Scopula marginepunctata	Nem védett / indifferens
Scopula nemoraria	Védett
Scopula nigropunctata	Nem védett / indifferens
Scopula ornata	Nem védett / indifferens
Scopula rubiginata	Nem védett / indifferens
Scopula subpunctaria	Nem védett / indifferens
Scopula umbelaria	Nem védett / indifferens
Scopula virgulata	Nem védett / indifferens
Scotina celans	Nem védett / indifferens
Scotochrosta pulla	Védett
Scotophaeus blackwalli	Nem védett / indifferens
Scotophaeus quadripunctatus	Nem védett / indifferens
Scotophaeus scutulatus	Nem védett / indifferens
Scotopteryx bipunctaria	Nem védett / indifferens
Scotopteryx chenopodiata	Nem védett / indifferens
Scotopteryx coarctaria	Nem védett / indifferens
Scotopteryx luridata	Nem védett / indifferens
Scotopteryx moeniata	Nem védett / indifferens
Scotopteryx mucronata	Nem védett / indifferens
Scottia pseudobrowniana	Nem védett / indifferens
Scraptia dubia	Nem védett / indifferens
Scraptia ferruginea	Nem védett / indifferens
Scraptia fuscula	Nem védett / indifferens
Scrobipalpa acuminatella	Nem védett / indifferens
Scrobipalpa artemisiella	Nem védett / indifferens
Scrobipalpa atriplicella	Nem védett / indifferens
Scrobipalpa chrysanthemella	Nem védett / indifferens
Scrobipalpa erichi	Nem védett / indifferens
Scrobipalpa gallicella	Nem védett / indifferens
Scrobipalpa halonella	Nem védett / indifferens
Scrobipalpa hungariae	Nem védett / indifferens
Scrobipalpa instabilella	Nem védett / indifferens
Scrobipalpa pauperella	Nem védett / indifferens
Scrobipalpa nitentella	Nem védett / indifferens
Scrobipalpa obsoletella	Nem védett / indifferens
Scrobipalpa ocellatella	Nem védett / indifferens
Scrobipalpa proclivella	Nem védett / indifferens
Scrobipalpa reiprichi	Nem védett / indifferens
Scrobipalpa salinella	Nem védett / indifferens
Scrobipalpa samadensis plantaginella	Nem védett / indifferens
Scrobipalpula psilella	Nem védett / indifferens
Scrobipalpula tussilaginis	Nem védett / indifferens
Scutigera coleoptrata	Nem védett / indifferens
Scydmaenus hellwigi	Nem védett / indifferens
Scydmaenus perrisi	Nem védett / indifferens
Scydmaenus rufus	Nem védett / indifferens
Scydmaenus tarsatus	Nem védett / indifferens
Scydmoraphes geticus	Nem védett / indifferens
Scydmoraphes helvolus	Nem védett / indifferens
Scymnus abietis	Nem védett / indifferens
Scymnus apetzi	Nem védett / indifferens
Scymnus ater	Nem védett / indifferens
Scymnus auritus	Nem védett / indifferens
Scymnus doriai	Nem védett / indifferens
Scymnus femoralis	Nem védett / indifferens
Scymnus ferrugatus	Nem védett / indifferens
Scymnus fraxini	Nem védett / indifferens
Scymnus frontalis	Nem védett / indifferens
Scymnus haemorrhoidalis	Nem védett / indifferens
Scymnus impexus	Nem védett / indifferens
Scymnus interruptus	Nem védett / indifferens
Scymnus limbatus	Nem védett / indifferens
Scymnus marginalis	Nem védett / indifferens
Scymnus mediterraneus	Nem védett / indifferens
Scymnus mimulus	Nem védett / indifferens
Scymnus nigrinus	Nem védett / indifferens
Scymnus pallipediformis	Nem védett / indifferens
Scymnus pallipediformis apetzoides	Nem védett / indifferens
Scymnus quadriguttatus	Nem védett / indifferens
Scymnus rubromaculatus	Nem védett / indifferens
Scymnus sacium	Nem védett / indifferens
Scymnus silesiacus	Nem védett / indifferens
Scymnus subvillosus	Nem védett / indifferens
Scymnus suturalis	Nem védett / indifferens
Scythia craniumequinum	Nem védett / indifferens
Scythia festuceti	Nem védett / indifferens
Scythris aerariella	Nem védett / indifferens
Scythris bengtssoni	Nem védett / indifferens
Scythris bifissella	Nem védett / indifferens
Scythris crassiuscula	Nem védett / indifferens
Scythris cuspidella	Nem védett / indifferens
Scythris emichi	Nem védett / indifferens
Scythris fallacella	Nem védett / indifferens
Scythris flaviventrella	Nem védett / indifferens
Scythris fuscoaenea	Nem védett / indifferens
Scythris gozmanyi	Nem védett / indifferens
Scythris hungaricella	Nem védett / indifferens
Scythris laminella	Nem védett / indifferens
Scythris limbella	Nem védett / indifferens
Scythris obscurella	Nem védett / indifferens
Scythris palustris	Nem védett / indifferens
Scythris pascuella	Nem védett / indifferens
Scythris paullella	Nem védett / indifferens
Scythris picaepennis	Nem védett / indifferens
Scythris podoliensis	Nem védett / indifferens
Scythris productella	Nem védett / indifferens
Scythris seliniella	Nem védett / indifferens
Scythris subseliniella	Nem védett / indifferens
Scythris tabidella	Nem védett / indifferens
Scythris tributella	Nem védett / indifferens
Scythris vittella	Nem védett / indifferens
Scythropia crataegella	Nem védett / indifferens
Scytodes thoracica	Nem védett / indifferens
Sedina buettneri	Nem védett / indifferens
Segestria bavarica	Nem védett / indifferens
Segestria florentina	Nem védett / indifferens
Segestria senoculata	Nem védett / indifferens
Segmentina nitida	Nem védett / indifferens
Sehirus luctuosus	Nem védett / indifferens
Sehirus morio	Nem védett / indifferens
Sehirus ovatus	Nem védett / indifferens
Sehirus parens	Nem védett / indifferens
Selagia argyrella	Nem védett / indifferens
Selagia spadicella	Nem védett / indifferens
Selania leplastriana	Nem védett / indifferens
Selatosomus aeneus	Nem védett / indifferens
Selatosomus cruciatus	Nem védett / indifferens
Selatosomus gravidus	Nem védett / indifferens
Selenia dentaria	Nem védett / indifferens
Selenia lunularia	Nem védett / indifferens
Selenia tetralunaria	Nem védett / indifferens
Selenocephalus obsoletus	Nem védett / indifferens
Selenocephalus stenopterus	Nem védett / indifferens
Selenodes karelica	Nem védett / indifferens
Selidosema brunnearia	Nem védett / indifferens
Selidosema plumaria	Nem védett / indifferens
Semanotus russicus	Védett
Semanotus undatus	Nem védett / indifferens
Semidalis aleyrodiformis	Nem védett / indifferens
Semilimax semilimax	Nem védett / indifferens
Semioscopis avellanella	Nem védett / indifferens
Semioscopis oculella	Nem védett / indifferens
Semioscopis steinkellneriana	Nem védett / indifferens
Semioscopis strigulana	Nem védett / indifferens
Senta flammea	Nem védett / indifferens
Sepedophilus binotatus	Nem védett / indifferens
Sepedophilus bipunctatus	Nem védett / indifferens
Sepedophilus bipustulatus	Nem védett / indifferens
Sepedophilus constans	Nem védett / indifferens
Sepedophilus immaculatus	Nem védett / indifferens
Sepedophilus littoreus	Nem védett / indifferens
Sepedophilus lividus	Nem védett / indifferens
Sepedophilus marshami	Nem védett / indifferens
Sepedophilus obtusus	Nem védett / indifferens
Sepedophilus pedicularius	Nem védett / indifferens
Sepedophilus testaceus	Nem védett / indifferens
Serica brunnea	Nem védett / indifferens
Sericoderus lateralis	Nem védett / indifferens
Sericomyia silentis	Nem védett / indifferens
Sericostoma flavicorne	Nem védett / indifferens
Sericostoma personatum	Nem védett / indifferens
Sericus brunneus	Nem védett / indifferens
Serinus serinus	Védett
Serropalpus barbatus	Nem védett / indifferens
Insalebria gregella	Nem védett / indifferens
Insalebria serraticornella	Nem védett / indifferens
Sesia apiformis	Nem védett / indifferens
Sesia bembeciformis	Nem védett / indifferens
Sesia melanocephala	Nem védett / indifferens
Reptalus cuspidatus	Nem védett / indifferens
Setina irrorella	Nem védett / indifferens
Setina roscida	Nem védett / indifferens
Setodes punctatus	Nem védett / indifferens
Setodes viridis	Nem védett / indifferens
Shargacucullia gozmanyi	Védett
Shargacucullia lychnitis	Nem védett / indifferens
Shargacucullia prenanthis	Védett
Shargacucullia scrophulariae	Nem védett / indifferens
Shargacucullia lanceolata	Védett
Shargacucullia verbasci	Nem védett / indifferens
Siagonium humerale	Nem védett / indifferens
Siagonium quadricorne	Nem védett / indifferens
Sialis fuliginosa	Nem védett / indifferens
Sialis lutaria	Nem védett / indifferens
Sialis morio	Nem védett / indifferens
Sialis nigripes	Nem védett / indifferens
Sibianor aurocinctus	Nem védett / indifferens
Sibinia beckeri	Nem védett / indifferens
Sibinia femoralis	Nem védett / indifferens
Sibinia hopffgarteni	Nem védett / indifferens
Sibinia pellucens	Nem védett / indifferens
Sibinia phalerata	Nem védett / indifferens
Sibinia primita	Nem védett / indifferens
Sibinia pyrrhodactyla	Nem védett / indifferens
Sibinia subelliptica	Nem védett / indifferens
Sibinia tibialis	Nem védett / indifferens
Sibinia unicolor	Nem védett / indifferens
Sibinia variata	Nem védett / indifferens
Sibinia viscariae	Nem védett / indifferens
Sibinia vittata	Nem védett / indifferens
Sicista subtilis	Fokozottan védett
Sicista subtilis trizona	Fokozottan védett
Sida crystallina	Nem védett / indifferens
Sideridis implexa	Védett
Sideridis lampra	Nem védett / indifferens
Sideridis reticulata	Nem védett / indifferens
Sideridis rivularis	Nem védett / indifferens
Sideridis turbida	Nem védett / indifferens
Siederia listerella	Nem védett / indifferens
Sigara assimilis	Nem védett / indifferens
Sigara distincta	Nem védett / indifferens
Sigara falleni	Nem védett / indifferens
Sigara fossarum	Nem védett / indifferens
Sigara lateralis	Nem védett / indifferens
Sigara limitata	Nem védett / indifferens
Sigara nigrolineata	Nem védett / indifferens
Sigara semistriata	Nem védett / indifferens
Sigara striata	Nem védett / indifferens
Sigorus porcus	Nem védett / indifferens
Silis nitidula	Nem védett / indifferens
Crudosilis ruficollis	Nem védett / indifferens
Silo nigricornis	Nem védett / indifferens
Silo pallipes	Nem védett / indifferens
Silo piceus	Nem védett / indifferens
Silometopus curtus	Nem védett / indifferens
Silometopus elegans	Nem védett / indifferens
Silometopus reussi	Nem védett / indifferens
Silpha carinata	Nem védett / indifferens
Silpha obscura	Nem védett / indifferens
Silpha tristis	Nem védett / indifferens
Silurus glanis	Nem védett / indifferens
Silusa rubiginosa	Nem védett / indifferens
Silusa rubra	Nem védett / indifferens
Silvanoprus fagi	Nem védett / indifferens
Silvanus bidentatus	Nem védett / indifferens
Silvanus unidentatus	Nem védett / indifferens
Silvius alpinus	Nem védett / indifferens
Simitidion simile	Nem védett / indifferens
Simo hirticornis	Nem védett / indifferens
Simo variegatus	Nem védett / indifferens
Simocephalus exspinosus	Nem védett / indifferens
Simocephalus serrulatus	Nem védett / indifferens
Simocephalus vetulus	Nem védett / indifferens
Simplicia rectalis	Nem védett / indifferens
Simplimorpha promissa	Nem védett / indifferens
Simplocaria semistriata	Nem védett / indifferens
Simyra albovenosa	Nem védett / indifferens
Simyra nervosa	Nem védett / indifferens
Sinanodonta woodiana	Nem védett / indifferens
Singa hamata	Nem védett / indifferens
Singa lucina	Nem védett / indifferens
Singa nitidula	Nem védett / indifferens
Sinodendron cylindricum	Védett
Sinoxylon perforans	Nem védett / indifferens
Sinoxylon sexdentatum	Nem védett / indifferens
Sintula corniger	Nem védett / indifferens
Sintula retroversus	Nem védett / indifferens
Sintula spiniger	Nem védett / indifferens
Siona lineata	Nem védett / indifferens
Siphlonurus aestivalis	Nem védett / indifferens
Siphlonurus armatus	Nem védett / indifferens
Siphlonurus lacustris	Nem védett / indifferens
Siphonoperla burmeisteri	Nem védett / indifferens
Siphonoperla neglecta	Nem védett / indifferens
Siphonoperla taurica	Nem védett / indifferens
Siphonoperla torrentium	Nem védett / indifferens
Siphonoperla transsylvanica	Nem védett / indifferens
Sirocalodes depressicollis	Nem védett / indifferens
Sirocalodes quercicola	Nem védett / indifferens
Sisyphus schaefferi	Nem védett / indifferens
Sisyra jutlandica	Nem védett / indifferens
Sisyra nigra	Nem védett / indifferens
Sisyra terminalis	Nem védett / indifferens
Sitaris muralis	Nem védett / indifferens
Sitochroa palealis	Nem védett / indifferens
Sitochroa verticalis	Nem védett / indifferens
Sitona ambiguus	Nem védett / indifferens
Sitona callosus	Nem védett / indifferens
Sitona cambricus	Nem védett / indifferens
Sitona cinerascens	Nem védett / indifferens
Sitona cylindricollis	Nem védett / indifferens
Sitona gressorius	Nem védett / indifferens
Sitona griseus	Nem védett / indifferens
Sitona hispidulus	Nem védett / indifferens
Sitona humeralis	Nem védett / indifferens
Sitona inops	Nem védett / indifferens
Sitona languidus	Nem védett / indifferens
Sitona lateralis	Nem védett / indifferens
Sitona lepidus	Nem védett / indifferens
Sitona lineatus	Nem védett / indifferens
Sitona longulus	Nem védett / indifferens
Sitona macularius	Nem védett / indifferens
Sitona puncticollis	Nem védett / indifferens
Sitona striatellus	Nem védett / indifferens
Sitona sulcifrons	Nem védett / indifferens
Sitona suturalis	Nem védett / indifferens
Sitona waterhousei	Nem védett / indifferens
Sitophilus granarius	Nem védett / indifferens
Sitophilus oryzae	Nem védett / indifferens
Sitophilus zeamais	Nem védett / indifferens
Sitotroga cerealella	Nem védett / indifferens
Sitta europaea	Védett
Sitta europaea caesia	Védett
Sitticus caricis	Nem védett / indifferens
Sitticus distinguendus	Nem védett / indifferens
Sitticus dzieduszyckii	Nem védett / indifferens
Sitticus floricola	Nem védett / indifferens
Sitticus inexpectus	Nem védett / indifferens
Sitticus penicillatus	Nem védett / indifferens
Sitticus pubescens	Nem védett / indifferens
Sitticus rupicola	Nem védett / indifferens
Sitticus saltator	Nem védett / indifferens
Sitticus saxicola	Nem védett / indifferens
Sitticus zimmermanni	Nem védett / indifferens
Smerinthus ocellatus	Nem védett / indifferens
Smicromyrme catanensis	Nem védett / indifferens
Smicromyrme cingulata	Nem védett / indifferens
Smicromyrme curtiventris	Nem védett / indifferens
Smicromyrme halensis	Nem védett / indifferens
Smicromyrme padella	Nem védett / indifferens
Smicromyrme pliginskiji	Nem védett / indifferens
Smicromyrme pusilla	Nem védett / indifferens
Smicromyrme ruficollis	Nem védett / indifferens
Smicromyrme rufipes	Nem védett / indifferens
Smicromyrme scutellaris	Nem védett / indifferens
Smicromyrme sericeiceps	Nem védett / indifferens
Smicromyrme sicana	Nem védett / indifferens
Smicromyrme subcomata	Nem védett / indifferens
Smicromyrme viduata	Nem védett / indifferens
Smicronyx brevicornis	Nem védett / indifferens
Smicronyx cyaneus	Nem védett / indifferens
Zygina rosea	Nem védett / indifferens
Smicronyx jungermanniae	Nem védett / indifferens
Smicronyx pygmaeus	Nem védett / indifferens
Smicronyx reichii	Nem védett / indifferens
Smicronyx smreczynskii	Nem védett / indifferens
Smicronyx striatipennis	Nem védett / indifferens
Smicronyx swertiae	Nem védett / indifferens
Smicrus filicornis	Nem védett / indifferens
Smithistruma baudueri	Nem védett / indifferens
Solenopsis fugax	Nem védett / indifferens
Solenoxyphus fuscovenosus	Nem védett / indifferens
Solierella compedita	Nem védett / indifferens
Somateria mollissima	Védett
Somateria spectabilis	Védett
Somatochlora flavomaculata	Védett
Somatochlora metallica	Nem védett / indifferens
Sophronia ascalis	Nem védett / indifferens
Sophronia chilonella	Nem védett / indifferens
Sophronia consanguinella	Nem védett / indifferens
Sophronia humerella	Nem védett / indifferens
Sophronia illustrella	Nem védett / indifferens
Sophronia marginella	Nem védett / indifferens
Sophronia semicostella	Nem védett / indifferens
Sophronia sicariellus	Nem védett / indifferens
Sorex alpinus	Védett
Sorex fodiens	Védett
Sorex minutus	Védett
Sorhagenia janiszewskae	Nem védett / indifferens
Sorhagenia lophyrella	Nem védett / indifferens
Sorhagenia rhamniella	Nem védett / indifferens
Sorhoanus assimilis	Nem védett / indifferens
Soronia grisea	Nem védett / indifferens
Soronia punctatissima	Nem védett / indifferens
Anisosticta vigintiguttata	Nem védett / indifferens
Sosticus loricatus	Nem védett / indifferens
Spaelotis ravida	Nem védett / indifferens
Spaniophaenus laticollis	Nem védett / indifferens
Sparedrus testaceus	Nem védett / indifferens
Spargania luctuata	Nem védett / indifferens
Sparganothis pilleriana	Nem védett / indifferens
Spatalia argentina	Nem védett / indifferens
Spatalistis bifasciana	Nem védett / indifferens
Spathocera laticornis	Nem védett / indifferens
Spathocera lobata	Nem védett / indifferens
Spathocera obscura	Nem védett / indifferens
Spathocera tuberculata	Nem védett / indifferens
Spavius glaber	Nem védett / indifferens
Spazigaster ambulans	Nem védett / indifferens
Specodes albilabris	Nem védett / indifferens
Specodes alternatus	Nem védett / indifferens
Specodes crassus	Nem védett / indifferens
Specodes cristatus	Nem védett / indifferens
Specodes croaticus	Nem védett / indifferens
Specodes ephippius	Nem védett / indifferens
Specodes ferruginatus	Nem védett / indifferens
Specodes geofrellus	Nem védett / indifferens
Specodes gibbus	Nem védett / indifferens
Specodes hyalinatus	Nem védett / indifferens
Specodes intermedius	Nem védett / indifferens
Specodes longulus	Nem védett / indifferens
Specodes majalis	Nem védett / indifferens
Specodes miniatus	Nem védett / indifferens
Specodes monilicornis	Nem védett / indifferens
Specodes niger	Nem védett / indifferens
Specodes pellucidus	Nem védett / indifferens
Specodes pseudofasciatus	Nem védett / indifferens
Specodes puncticeps	Nem védett / indifferens
Specodes reticulatus	Nem védett / indifferens
Specodes rubicundus	Nem védett / indifferens
Specodes rufiventris	Nem védett / indifferens
Specodes scabricollis	Nem védett / indifferens
Specodes spinulosus	Nem védett / indifferens
Spelaeodiscus triarius	Védett
Spercheus emarginatus	Nem védett / indifferens
Spermophilus citellus	Fokozottan védett
Spermophora senoculata	Nem védett / indifferens
Speudotettix subfusculus	Nem védett / indifferens
Sphaeridium bipustulatum	Nem védett / indifferens
Sphaeridium lunatum	Nem védett / indifferens
Sphaeridium marginatum	Nem védett / indifferens
Sphaeridium scarabaeoides	Nem védett / indifferens
Sphaeridium substriatum	Nem védett / indifferens
Sphaeriestes castaneus	Nem védett / indifferens
Sphaeriestes stockmanni	Nem védett / indifferens
Sphaerium corneum	Nem védett / indifferens
Sphaerium nucleus	Nem védett / indifferens
Sphaerium rivicola	Nem védett / indifferens
Sphaerium solidum	Nem védett / indifferens
Sphaerolecanium prunastri	Nem védett / indifferens
Sphaerophoria batava	Nem védett / indifferens
Sphaerophoria fatarum	Nem védett / indifferens
Sphaerophoria interrupta	Nem védett / indifferens
Sphaerophoria loewi	Nem védett / indifferens
Sphaerophoria philanthus	Nem védett / indifferens
Sphaerophoria rueppelli	Nem védett / indifferens
Sphaerophoria scripta	Nem védett / indifferens
Sphaerophoria shirchan	Nem védett / indifferens
Sphaerophoria taeniata	Nem védett / indifferens
Sphaerophoria virgata	Nem védett / indifferens
Sphaerosoma globosum	Nem védett / indifferens
Sphaerosoma pilosum	Nem védett / indifferens
Sphegina clavata	Nem védett / indifferens
Sphegina clunipes	Nem védett / indifferens
Sphegina elegans	Nem védett / indifferens
Sphegina latifrons	Nem védett / indifferens
Sphegina montana	Nem védett / indifferens
Sphegina sibirica	Nem védett / indifferens
Sphegina verecunda	Nem védett / indifferens
Sphenophorus abbreviatus	Nem védett / indifferens
Sphenophorus piceus	Nem védett / indifferens
Sphenophorus striatopunctatus	Nem védett / indifferens
Sphenoptera antiqua	Nem védett / indifferens
Sphenoptera basalis	Nem védett / indifferens
Sphenoptera parvula	Nem védett / indifferens
Sphenoptera petriceki	Nem védett / indifferens
Sphenoptera substriata	Nem védett / indifferens
Sphex atropilosus	Nem védett / indifferens
Sphex flavipennis	Nem védett / indifferens
Sphex rufocinctus	Védett
Sphindus dubius	Nem védett / indifferens
Sphindus grandis	Nem védett / indifferens
Sphinginus coarctatus	Nem védett / indifferens
Sphingonotus caerulans	Nem védett / indifferens
Sphinx ligustri	Nem védett / indifferens
Sphiximorpha binominata	Nem védett / indifferens
Sphiximorpha subsessilis	Nem védett / indifferens
Sphodrus leucophthalmus	Nem védett / indifferens
Sphragisticus nebulosus	Nem védett / indifferens
Sphyradium doliolum	Nem védett / indifferens
Spialia orbifer	Védett
Spialia sertorius	Védett
Spilococcus nanae	Nem védett / indifferens
Spilomena mocsaryi	Nem védett / indifferens
Spilomena troglodytes	Nem védett / indifferens
Spilomyia diophthalma	Nem védett / indifferens
Spilomyia manicata	Nem védett / indifferens
Spilomyia saltuum	Nem védett / indifferens
Spilonota laricana	Nem védett / indifferens
Spilonota ocellana	Nem védett / indifferens
Spilosoma lubricipeda	Nem védett / indifferens
Spilosoma lutea	Nem védett / indifferens
Spilosoma urticae	Nem védett / indifferens
Spilosoma virginica	Nem védett / indifferens
Spilostethus pandurus	Nem védett / indifferens
Spilostethus saxatilis	Nem védett / indifferens
Spinococcus callunetti	Nem védett / indifferens
Spinococcus marrubii	Nem védett / indifferens
Spinococcus morrisoni	Nem védett / indifferens
Spinolia insignis	Nem védett / indifferens
Spinolia unicolor	Nem védett / indifferens
Spintharina versicolor	Nem védett / indifferens
Coscinia striata	Nem védett / indifferens
Spodoptera exigua	Nem védett / indifferens
Spondylis buprestoides	Nem védett / indifferens
Spudaea ruticilla	Védett
Spuleria flavicaput	Nem védett / indifferens
Spulerina simploniella	Nem védett / indifferens
Squamapion atomarium	Nem védett / indifferens
Squamapion cineraceum	Nem védett / indifferens
Squamapion elongatum	Nem védett / indifferens
Squamapion flavimanum	Nem védett / indifferens
Squamapion hoffmanni	Nem védett / indifferens
Squamapion oblivium	Nem védett / indifferens
Squamapion vicinum	Nem védett / indifferens
Stactobiella risi	Nem védett / indifferens
Stagetus pilula	Nem védett / indifferens
Stagmatophora heydeniella	Nem védett / indifferens
Stagnicola corvus	Nem védett / indifferens
Stagnicola fuscus	Nem védett / indifferens
Stagnicola palustris	Nem védett / indifferens
Stagnicola turricula	Nem védett / indifferens
Stagonomus amoenus	Nem védett / indifferens
Stagonomus bipunctatus	Nem védett / indifferens
Stagonomus pusillus	Nem védett / indifferens
Stangeia siceliota	Nem védett / indifferens
Staphylinus caesareus	Nem védett / indifferens
Staphylinus dimidiaticornis	Nem védett / indifferens
Staphylinus erythropterus	Nem védett / indifferens
Staphylinus rubricornis	Nem védett / indifferens
Staria lunata	Nem védett / indifferens
Stathmopoda pedella	Nem védett / indifferens
Staudingeria deserticola	Nem védett / indifferens
Stauroderus scalaris	Nem védett / indifferens
Staurophora celsia	Védett
Stauropus fagi	Nem védett / indifferens
Steatoda albomaculata	Nem védett / indifferens
Steatoda bipunctata	Nem védett / indifferens
Steatoda castanea	Nem védett / indifferens
Steatoda grossa	Nem védett / indifferens
Steatoda meridionalis	Nem védett / indifferens
Steatoda phalerata	Nem védett / indifferens
Steatoda triangulosa	Nem védett / indifferens
Stegania cararia	Nem védett / indifferens
Stegania dilectaria	Nem védett / indifferens
Stegobium paniceum	Nem védett / indifferens
Steingelia gorodetskia	Nem védett / indifferens
Stelis annulata	Nem védett / indifferens
Stelis breviuscula	Nem védett / indifferens
Stelis jugae	Nem védett / indifferens
Stelis minuta	Nem védett / indifferens
Stelis nasuta	Nem védett / indifferens
Stelis odontopyga	Nem védett / indifferens
Stelis ornatula	Nem védett / indifferens
Stelis phaeoptera	Nem védett / indifferens
Stelis punctulatissima	Nem védett / indifferens
Stelis signata	Nem védett / indifferens
Stemonyphantes lineatus	Nem védett / indifferens
Stenagostus rhombeus	Nem védett / indifferens
Stenagostus rufus	Nem védett / indifferens
Stenalia escherichi	Nem védett / indifferens
Stenamma debile	Nem védett / indifferens
Stenhomalus bicolor	Nem védett / indifferens
Stenichnus carpathicus	Nem védett / indifferens
Stenichnus collaris	Nem védett / indifferens
Stenichnus godarti	Nem védett / indifferens
Stenichnus scutellaris	Nem védett / indifferens
Stenistoderus nothus	Nem védett / indifferens
Stenobothrus crassipes	Nem védett / indifferens
Stenobothrus eurasius	Védett
Stenobothrus Fischeri	Nem védett / indifferens
Stenobothrus lineatus	Nem védett / indifferens
Stenobothrus nigromaculatus	Nem védett / indifferens
Stenobothrus stigmaticus	Nem védett / indifferens
Stenocarus cardui	Nem védett / indifferens
Stenocarus ruficornis	Nem védett / indifferens
Stenocorus meridianus	Nem védett / indifferens
Stenocranus fuscovittatus	Nem védett / indifferens
Stenocranus major	Nem védett / indifferens
Stenocranus minutus	Nem védett / indifferens
Stenocypria fischeri	Nem védett / indifferens
Stenodema calcarata	Nem védett / indifferens
Stenodema holsata	Nem védett / indifferens
Stenodema laevigata	Nem védett / indifferens
Stenodema sericans	Nem védett / indifferens
Stenodema virens	Nem védett / indifferens
Stenodynerus bluethgeni	Nem védett / indifferens
Stenodynerus chevrieranus	Nem védett / indifferens
Stenodynerus clypeopictus	Nem védett / indifferens
Stenodynerus orenburgensis	Nem védett / indifferens
Stenodynerus punctifrons	Nem védett / indifferens
Stenodynerus steckianus	Nem védett / indifferens
Stenodynerus xanthomelas	Nem védett / indifferens
Stenolechia gemmella	Nem védett / indifferens
Parastenolechia nigrinotella	Nem védett / indifferens
Stenolechiodes pseudogemmellus	Nem védett / indifferens
Stenolophus discophorus	Nem védett / indifferens
Stenolophus mixtus	Nem védett / indifferens
Stenolophus persicus	Nem védett / indifferens
Stenolophus proximus	Nem védett / indifferens
Stenolophus skrimshiranus	Nem védett / indifferens
Stenolophus steveni	Védett
Stenolophus teutonus	Nem védett / indifferens
Stenomax aeneus incurvus	Nem védett / indifferens
Stenopelmus rufinasus	Nem védett / indifferens
Stenophylax meridiorientalis	Nem védett / indifferens
Stenophylax permistus	Nem védett / indifferens
Stenophylax vibex	Nem védett / indifferens
Stenopterapion intermedium	Nem védett / indifferens
Stenopterapion meliloti	Nem védett / indifferens
Stenopterapion tenue	Nem védett / indifferens
Stenopterus flavicornis	Nem védett / indifferens
Stenopterus rufus	Nem védett / indifferens
Stenoptilia annadactyla	Nem védett / indifferens
Stenoptilia bipunctidactyla	Nem védett / indifferens
Stenoptilia coprodactylus	Nem védett / indifferens
Stenoptilia graphodactyla	Nem védett / indifferens
Stenoptilia gratiolae	Nem védett / indifferens
Stenoptilia pelidnodactyla	Nem védett / indifferens
Stenoptilia pneumonanthes	Nem védett / indifferens
Stenoptilia pterodactyla	Nem védett / indifferens
Stenoptilia stigmatodactylus	Nem védett / indifferens
Stenoptilia stigmatoides	Nem védett / indifferens
Stenoptilia zophodactylus	Nem védett / indifferens
Stenoptinea cyaneimarmorella	Nem védett / indifferens
Stenoria apicalis	Védett
Stenostola dubia	Nem védett / indifferens
Stenostola ferrea	Nem védett / indifferens
Stenotus binotatus	Nem védett / indifferens
Stenurella bifasciata	Nem védett / indifferens
Stenurella melanura	Nem védett / indifferens
Stenurella nigra	Nem védett / indifferens
Stenurella septempunctata	Nem védett / indifferens
Stenus ampliventris	Nem védett / indifferens
Stenus annulipes	Nem védett / indifferens
Stenus argus	Nem védett / indifferens
Stenus asphaltinus	Nem védett / indifferens
Stenus assequens	Nem védett / indifferens
Stenus ater	Nem védett / indifferens
Stenus aterrimus	Nem védett / indifferens
Stenus atratulus	Nem védett / indifferens
Stenus bifoveolatus	Nem védett / indifferens
Stenus biguttatus	Nem védett / indifferens
Stenus bimaculatus	Nem védett / indifferens
Stenus binotatus	Nem védett / indifferens
Stenus boops	Nem védett / indifferens
Stenus brunnipes	Nem védett / indifferens
Stenus canaliculatus	Nem védett / indifferens
Stenus carbonarius	Nem védett / indifferens
Stenus carpathicus	Nem védett / indifferens
Stenus cautus	Nem védett / indifferens
Stenus cicindeloides	Nem védett / indifferens
Stenus circularis	Nem védett / indifferens
Stenus claritarsis	Nem védett / indifferens
Stenus clavicornis	Nem védett / indifferens
Stenus comma	Nem védett / indifferens
Stenus crassus	Nem védett / indifferens
Stenus doderoi	Nem védett / indifferens
Stenus eumerus	Nem védett / indifferens
Stenus europaeus	Nem védett / indifferens
Stenus excubitor	Nem védett / indifferens
Stenus exspectatus	Nem védett / indifferens
Stenus flavipalpis	Nem védett / indifferens
Stenus flavipes	Nem védett / indifferens
Stenus formicetorum	Nem védett / indifferens
Stenus fornicatus	Nem védett / indifferens
Stenus fossulatus	Nem védett / indifferens
Stenus fuscicornis	Nem védett / indifferens
Stenus fuscipes	Nem védett / indifferens
Stenus glabellus	Nem védett / indifferens
Stenus guttula	Nem védett / indifferens
Stenus horioni	Nem védett / indifferens
Stenus humilis	Nem védett / indifferens
Stenus hypoproditor	Nem védett / indifferens
Stenus impressus	Nem védett / indifferens
Stenus incanus	Nem védett / indifferens
Stenus incrassatus	Nem védett / indifferens
Stenus indifferens	Nem védett / indifferens
Stenus intermedius	Nem védett / indifferens
Stenus intricatus	Nem védett / indifferens
Stenus intricatus zoufali	Nem védett / indifferens
Stenus josefkrali	Nem védett / indifferens
Stenus juno	Nem védett / indifferens
Stenus kiesenwetteri	Nem védett / indifferens
Stenus kolbei	Nem védett / indifferens
Stenus latifrons	Nem védett / indifferens
Stenus longipes	Nem védett / indifferens
Stenus longitarsis	Nem védett / indifferens
Stenus ludyi	Nem védett / indifferens
Stenus lustrator	Nem védett / indifferens
Stenus maculiger	Nem védett / indifferens
Stenus melanarius	Nem védett / indifferens
Stenus melanopus	Nem védett / indifferens
Stenus morio	Nem védett / indifferens
Stenus nanus	Nem védett / indifferens
Stenus nigritulus	Nem védett / indifferens
Stenus nitens	Nem védett / indifferens
Stenus obscuripalpis	Nem védett / indifferens
Stenus ochropus	Nem védett / indifferens
Stenus opticus	Nem védett / indifferens
Stenus pallipes	Nem védett / indifferens
Stenus pallitarsis	Nem védett / indifferens
Stenus palposus	Nem védett / indifferens
Stenus palustris	Nem védett / indifferens
Stenus phyllobates	Nem védett / indifferens
Stenus picipennis	Nem védett / indifferens
Stenus picipes brevipennis	Nem védett / indifferens
Stenus planifrons	Nem védett / indifferens
Stenus proditor	Nem védett / indifferens
Stenus providus	Nem védett / indifferens
Stenus pseudoboops	Nem védett / indifferens
Stenus pubescens	Nem védett / indifferens
Stenus pumilio	Nem védett / indifferens
Stenus pusillus	Nem védett / indifferens
Stenus ruralis	Nem védett / indifferens
Stenus scrutator	Nem védett / indifferens
Stenus similis	Nem védett / indifferens
Stenus solutus	Nem védett / indifferens
Stenus stigmula	Nem védett / indifferens
Stenus subaeneus	Nem védett / indifferens
Stenus sylvester	Nem védett / indifferens
Stenus tarsalis	Nem védett / indifferens
Stenus vastus	Nem védett / indifferens
Stephanitis pyri	Nem védett / indifferens
Stephanocleonus tetragrammus	Nem védett / indifferens
Stephensia brunnichella	Nem védett / indifferens
Stephostethus alternans	Nem védett / indifferens
Stephostethus angusticollis	Nem védett / indifferens
Stephostethus lardarius	Nem védett / indifferens
Stephostethus pandellei	Nem védett / indifferens
Stephostethus rugicollis	Nem védett / indifferens
Stephostethus rybinskii	Nem védett / indifferens
Stephostethus sinuatocollis	Nem védett / indifferens
Stercorarius longicaudus	Védett
Stercorarius parasiticus	Védett
Stercorarius pomarinus	Védett
Catharacta skua	Védett
Stereocorynes truncorum	Nem védett / indifferens
Stereonychus fraxini	Nem védett / indifferens
Sterna albifrons	Fokozottan védett
Sterna caspia	Védett
Sterna hirundo	Fokozottan védett
Sterna paradisaea	Védett
Sterna sandvicensis	Védett
Sternodontus binodulus	Nem védett / indifferens
Sternodontus obtusus	Nem védett / indifferens
Sterrhopterix fusca	Nem védett / indifferens
Stethoconus pyri	Nem védett / indifferens
Stethorus punctillum	Nem védett / indifferens
Sthenarus rotermundi	Nem védett / indifferens
Stibaropus henkei	Nem védett / indifferens
Stichoglossa semirufa	Nem védett / indifferens
Stictocephala bisonia	Nem védett / indifferens
Stictocoris picturatus	Nem védett / indifferens
Stictoleptura erythroptera	Nem védett / indifferens
Stictoleptura rubra	Nem védett / indifferens
Stictoleptura scutellata	Nem védett / indifferens
Stictopleurus abutilon	Nem védett / indifferens
Stictopleurus crassicornis	Nem védett / indifferens
Stictopleurus pictus	Nem védett / indifferens
Stictopleurus punctatonervosus	Nem védett / indifferens
Stictopleurus subtomentosus	Nem védett / indifferens
Stigmella aceris	Nem védett / indifferens
Stigmella aeneofasciella	Nem védett / indifferens
Stigmella alnetella	Nem védett / indifferens
Stigmella anomalella	Nem védett / indifferens
Stigmella assimilella	Nem védett / indifferens
Stigmella atricapitella	Nem védett / indifferens
Stigmella aurella	Nem védett / indifferens
Stigmella basiguttella	Nem védett / indifferens
Stigmella benanderella	Nem védett / indifferens
Stigmella betulicola	Nem védett / indifferens
Stigmella carpinella	Nem védett / indifferens
Stigmella catharticella	Nem védett / indifferens
Stigmella centifoliella	Nem védett / indifferens
Stigmella confusella	Nem védett / indifferens
Stigmella continuella	Nem védett / indifferens
Stigmella crataegella	Nem védett / indifferens
Stigmella desperatella	Nem védett / indifferens
Stigmella dorsiguttella	Nem védett / indifferens
Stigmella eberhardi	Nem védett / indifferens
Stigmella filipendulae	Nem védett / indifferens
Stigmella floslactella	Nem védett / indifferens
Stigmella freyella	Nem védett / indifferens
Stigmella glutinosae	Nem védett / indifferens
Stigmella hahniella	Nem védett / indifferens
Stigmella hemargyrella	Nem védett / indifferens
Stigmella hybnerella	Nem védett / indifferens
Stigmella incognitella	Nem védett / indifferens
Stigmella lemniscella	Nem védett / indifferens
Stigmella lonicerarum	Nem védett / indifferens
Stigmella luteella	Nem védett / indifferens
Stigmella magdalenae	Nem védett / indifferens
Stigmella malella	Nem védett / indifferens
Stigmella mespilicola	Nem védett / indifferens
Stigmella microtheriella	Nem védett / indifferens
Stigmella minusculella	Nem védett / indifferens
Stigmella naturnella	Nem védett / indifferens
Stigmella nivenburgensis	Nem védett / indifferens
Stigmella nylandriella	Nem védett / indifferens
Stigmella obliquella	Nem védett / indifferens
Stigmella oxyacanthella	Nem védett / indifferens
Stigmella paradoxa	Nem védett / indifferens
Stigmella perpygmaeella	Nem védett / indifferens
Stigmella plagicolella	Nem védett / indifferens
Stigmella poterii	Nem védett / indifferens
Stigmella prunetorum	Nem védett / indifferens
Stigmella pyri	Nem védett / indifferens
Stigmella regiella	Nem védett / indifferens
Stigmella rhamnella	Nem védett / indifferens
Stigmella roborella	Nem védett / indifferens
Stigmella rolandi	Nem védett / indifferens
Stigmella ruficapitella	Nem védett / indifferens
Stigmella sakhalinella	Nem védett / indifferens
Stigmella salicis	Nem védett / indifferens
Stigmella samiatella	Nem védett / indifferens
Stigmella sanguisorbae	Nem védett / indifferens
Stigmella speciosa	Nem védett / indifferens
Stigmella splendidissimella	Nem védett / indifferens
Stigmella svenssoni	Nem védett / indifferens
Stigmella szoecsiella	Nem védett / indifferens
Stigmella thuringiaca	Nem védett / indifferens
Stigmella tiliae	Nem védett / indifferens
Stigmella tityrella	Nem védett / indifferens
Stigmella tormentillella	Nem védett / indifferens
Stigmella torminalis	Nem védett / indifferens
Stigmella trimaculella	Nem védett / indifferens
Stigmella ulmiphaga	Nem védett / indifferens
Stigmella ulmivora	Nem védett / indifferens
Stigmella viscerella	Nem védett / indifferens
Stigmella zangherii	Nem védett / indifferens
Stigmus pendulus	Nem védett / indifferens
Stigmus solskyi	Nem védett / indifferens
Stilbum calens	Nem védett / indifferens
Stilbum calens zimmermanni	Nem védett / indifferens
Stilbum cyanurum	Védett
Stilbus atomarius	Nem védett / indifferens
Stilbus oblongus	Nem védett / indifferens
Stilbus pannonicus	Nem védett / indifferens
Stilbus testaceus	Nem védett / indifferens
Stiroma affinis	Nem védett / indifferens
Stiroma bicarinata	Nem védett / indifferens
Stiromoides maculiceps	Nem védett / indifferens
Stizoides tridentatus	Nem védett / indifferens
Stizus perrisii	Nem védett / indifferens
Stomis pumicatus	Nem védett / indifferens
Stomodes gyrosicollis	Nem védett / indifferens
Stomopteryx detersella	Nem védett / indifferens
Stomopteryx hungaricella	Nem védett / indifferens
Stomopteryx remissella	Nem védett / indifferens
Stosatea italica	Nem védett / indifferens
Strangalia attenuata	Nem védett / indifferens
Streblocerus serricaudatus	Nem védett / indifferens
Streptanus aemulans	Nem védett / indifferens
Streptanus marginatus	Nem védett / indifferens
Streptanus sordidus	Nem védett / indifferens
Streptocephalus torvicornis	Nem védett / indifferens
Streptopelia decaocto	Nem védett / indifferens
Streptopelia orientalis	Védett
Streptopelia orientalis meena	Védett
Streptopelia turtur	Védett
Streyella anguinella	Nem védett / indifferens
Stricticomus tobias	Nem védett / indifferens
Strigamia acuminata	Nem védett / indifferens
Strigamia crassipes	Nem védett / indifferens
Strigamia trannsylvanica	Nem védett / indifferens
Strix aluco	Védett
Strix uralensis	Fokozottan védett
Strix uralensis macroura	Fokozottan védett
Stroemiellus stroemi	Nem védett / indifferens
Stroggylocephalus agrestis	Nem védett / indifferens
Stroggylocephalus livens	Nem védett / indifferens
Stromatium unicolor	Nem védett / indifferens
Strongylocoris leucocephalus	Nem védett / indifferens
Strongylocoris luridus	Nem védett / indifferens
Strongylocoris niger	Nem védett / indifferens
Strongylognathus testaceus	Nem védett / indifferens
Strongylosoma stigmatosum	Nem védett / indifferens
Strophedra nitidana	Nem védett / indifferens
Strophedra weirana	Nem védett / indifferens
Strophomorphus porcellus	Nem védett / indifferens
Strophosoma capitatum	Nem védett / indifferens
Strophosoma melanogrammum	Nem védett / indifferens
Struebingianella lugubrina	Nem védett / indifferens
Sturnus roseus	Védett
Sturnus vulgaris	Védett
Stygnocoris faustus	Nem védett / indifferens
Stygnocoris fuligineus	Nem védett / indifferens
Stygnocoris pygmaeus	Nem védett / indifferens
Stygnocoris rusticus	Nem védett / indifferens
Stygnocoris sabulosus	Nem védett / indifferens
Styloctetor romanus	Nem védett / indifferens
Styloctetor stativus	Nem védett / indifferens
Styrioiulus pelidnus	Nem védett / indifferens
Styrioiulus pelidnus orientalis	Nem védett / indifferens
Styrioiulus styricus	Nem védett / indifferens
Subcoccinella vigintiquatuorpunctata	Nem védett / indifferens
Subilla confinis	Nem védett / indifferens
Subrinus sturmi	Nem védett / indifferens
Succinea putris	Nem védett / indifferens
Succinella oblonga	Nem védett / indifferens
Sulcacis affinis	Nem védett / indifferens
Sulcacis bidentulus	Nem védett / indifferens
Sulcacis fronticornis	Nem védett / indifferens
Sulcopolistes atrimandibularis	Nem védett / indifferens
Sulcopolistes semenowi	Nem védett / indifferens
Sulcopolistes sulcifer	Nem védett / indifferens
Sunius fallax	Nem védett / indifferens
Sunius melanocephalus	Nem védett / indifferens
Suphrodytes dorsalis	Nem védett / indifferens
Surnia ulula	Védett
Sus scrofa	Nem védett / indifferens
Swammerdamia caesiella	Nem védett / indifferens
Swammerdamia compunctella	Nem védett / indifferens
Swammerdamia pyrella	Nem védett / indifferens
Syedra gracilis	Nem védett / indifferens
Sylvia atricapilla	Védett
Sylvia borin	Védett
Sylvia cantillans	Védett
Sylvia cantillans albistriata	Védett
Sylvia communis	Védett
Sylvia curruca	Védett
Sylvia melanocephala	Védett
Sylvia nisoria	Védett
Symbiomyrma karawajevi	Nem védett / indifferens
Symbiotes gibberosus	Nem védett / indifferens
Symmorphus angustatus	Nem védett / indifferens
Symmorphus bifasciatus	Nem védett / indifferens
Symmorphus connexus	Nem védett / indifferens
Symmorphus crassicornis	Nem védett / indifferens
Symmorphus debilitatus	Nem védett / indifferens
Symmorphus declivis	Nem védett / indifferens
Symmorphus gracilis	Nem védett / indifferens
Symmorphus murarius	Nem védett / indifferens
Sympecma fusca	Nem védett / indifferens
Sympetrum danae	Nem védett / indifferens
Sympetrum depressiusculum	Védett
Sympetrum flaveolum	Nem védett / indifferens
Sympetrum fonscolombii	Nem védett / indifferens
Sympetrum meridionale	Nem védett / indifferens
Sympetrum pedemontanum	Nem védett / indifferens
Sympetrum sanguineum	Nem védett / indifferens
Sympetrum striolatum	Nem védett / indifferens
Sympetrum vulgatum	Nem védett / indifferens
Sympherobius elegans	Nem védett / indifferens
Sympherobius fuscescens	Nem védett / indifferens
Sympherobius klapaleki	Nem védett / indifferens
Sympherobius pellucidus	Nem védett / indifferens
Sympherobius pygmaeus	Nem védett / indifferens
Synema globosum	Nem védett / indifferens
Synema ornatum	Nem védett / indifferens
Synagapetus armatus	Nem védett / indifferens
Synagapetus iridipennis	Nem védett / indifferens
Synagapetus krawanyi	Nem védett / indifferens
Synagapetus moselyi	Nem védett / indifferens
Sitticus hilarulus	Nem védett / indifferens
Sitticus subcingulatus	Nem védett / indifferens
Sitticus venator	Nem védett / indifferens
Pyropteron affine	Védett
Pyropteron muscaeforme	Nem védett / indifferens
Pyropteron triannuliforme	Nem védett / indifferens
Synanthedon andrenaeformis	Nem védett / indifferens
Synanthedon cephiformis	Nem védett / indifferens
Synanthedon conopiformis	Nem védett / indifferens
Synanthedon culiciformis	Nem védett / indifferens
Synanthedon flaviventris	Nem védett / indifferens
Synanthedon formicaeformis	Nem védett / indifferens
Synanthedon loranthi	Nem védett / indifferens
Synanthedon melliniformis	Nem védett / indifferens
Synanthedon mesiaeformis	Nem védett / indifferens
Synanthedon myopaeformis	Nem védett / indifferens
Synanthedon spheciformis	Nem védett / indifferens
Synanthedon spuleri	Nem védett / indifferens
Synanthedon stomoxiformis	Nem védett / indifferens
Synanthedon tipuliformis	Nem védett / indifferens
Synanthedon vespiformis	Nem védett / indifferens
Synaphe antennalis	Nem védett / indifferens
Synaphe bombycalis	Nem védett / indifferens
Synaphe moldavica	Nem védett / indifferens
Synaphe punctalis	Nem védett / indifferens
Synapion ebeninum	Nem védett / indifferens
Synaptus filiformis	Nem védett / indifferens
Synchita humeralis	Nem védett / indifferens
Synchita mediolanensis	Nem védett / indifferens
Synchita separanda	Nem védett / indifferens
Synclisis baetica	Nem védett / indifferens
Syncopacma albifrontella	Nem védett / indifferens
Syncopacma albipalpella	Nem védett / indifferens
Syncopacma captivella	Nem védett / indifferens
Syncopacma cinctella	Nem védett / indifferens
Syncopacma cincticulella	Nem védett / indifferens
Syncopacma coronillella	Nem védett / indifferens
Syncopacma linella	Nem védett / indifferens
Syncopacma ochrofasciella	Nem védett / indifferens
Syncopacma patruella	Nem védett / indifferens
Syncopacma sangiella	Nem védett / indifferens
Syncopacma taeniolella	Nem védett / indifferens
Syncopacma vinella	Nem védett / indifferens
Syndemis musculana	Nem védett / indifferens
Parlatoria parlatoriae	Nem védett / indifferens
Syngrapha ain	Nem védett / indifferens
Syngrapha interrogationis	Nem védett / indifferens
Synopsia sociaria	Nem védett / indifferens
Syntomium aeneum	Nem védett / indifferens
Syntomus foveatus	Nem védett / indifferens
Syntomus obscuroguttatus	Nem védett / indifferens
Syntomus pallipes	Nem védett / indifferens
Syntomus truncatellus	Nem védett / indifferens
Synuchus vivalis	Nem védett / indifferens
Syritta pipiens	Nem védett / indifferens
Syromastus rhombeus	Nem védett / indifferens
Syrphus ribesii	Nem védett / indifferens
Syrphus sexmaculatus	Nem védett / indifferens
Syrphus torvus	Nem védett / indifferens
Syrphus vitripennis	Nem védett / indifferens
Syrrhaptes paradoxus	Védett
Systellonotus triguttatus	Nem védett / indifferens
Systropha curvicornis	Nem védett / indifferens
Systropha planidens	Nem védett / indifferens
Tabanus autumnalis	Nem védett / indifferens
Tabanus bifarius	Nem védett / indifferens
Tabanus bovinus	Nem védett / indifferens
Tabanus bromius	Nem védett / indifferens
Tabanus cordiger	Nem védett / indifferens
Tabanus exclusus	Nem védett / indifferens
Tabanus glaucopis	Nem védett / indifferens
Tabanus maculicornis	Nem védett / indifferens
Tabanus miki	Nem védett / indifferens
Tabanus paradoxus	Nem védett / indifferens
Tabanus quatuornotatus	Nem védett / indifferens
Tabanus regularis	Nem védett / indifferens
Tabanus spectabilis	Nem védett / indifferens
Tabanus spodopterus	Nem védett / indifferens
Tabanus sudeticus	Nem védett / indifferens
Tabanus tergestinus	Nem védett / indifferens
Tabanus unifasciatus	Nem védett / indifferens
Tachinus bipustulatus	Nem védett / indifferens
Tachinus bonvouloiri	Nem védett / indifferens
Tachinus corticinus	Nem védett / indifferens
Tachinus discoideus	Nem védett / indifferens
Tachinus fimetarius	Nem védett / indifferens
Tachinus humeralis	Nem védett / indifferens
Tachinus laticollis	Nem védett / indifferens
Tachinus lignorum	Nem védett / indifferens
Tachinus marginatus	Nem védett / indifferens
Tachinus marginellus	Nem védett / indifferens
Tachinus pallipes	Nem védett / indifferens
Tachinus rufipennis	Nem védett / indifferens
Tachinus scapularis	Nem védett / indifferens
Tachinus signatus	Nem védett / indifferens
Tachinus subterraneus	Nem védett / indifferens
Tachyagetes dudichi	Nem védett / indifferens
Tachyagetes filicornis	Nem védett / indifferens
Tachybaptus ruficollis	Védett
Tachycixius desertorum	Nem védett / indifferens
Tachycixius pilosus	Nem védett / indifferens
Tachycixius soosi	Nem védett / indifferens
Tachyerges decoratus	Nem védett / indifferens
Tachyerges pseudostigma	Nem védett / indifferens
Tachyerges rufitarsis	Nem védett / indifferens
Tachyerges salicis	Nem védett / indifferens
Tachyerges stigma	Nem védett / indifferens
Tachyporus abdominalis	Nem védett / indifferens
Tachyporus atriceps	Nem védett / indifferens
Tachyporus caucasicus	Nem védett / indifferens
Tachyporus chrysomelinus	Nem védett / indifferens
Tachyporus corpulentus	Nem védett / indifferens
Tachyporus dispar	Nem védett / indifferens
Tachyporus formosus	Nem védett / indifferens
Tachyporus hypnorum	Nem védett / indifferens
Tachyporus nitidulus	Nem védett / indifferens
Tachyporus obtusus	Nem védett / indifferens
Tachyporus pallidus	Nem védett / indifferens
Tachyporus pulchellus	Nem védett / indifferens
Tachyporus pusillus	Nem védett / indifferens
Tachyporus quadriscopulatus	Nem védett / indifferens
Tachyporus ruficollis	Nem védett / indifferens
Tachyporus scitulus	Nem védett / indifferens
Tachyporus solutus	Nem védett / indifferens
Tachyporus tersus	Nem védett / indifferens
Tachyporus transversalis	Nem védett / indifferens
Tachyporus vafer	Nem védett / indifferens
Tachys scutellaris	Nem védett / indifferens
Tachysphex bicolor	Nem védett / indifferens
Tachysphex fugax	Nem védett / indifferens
Tachysphex fulvitarsis	Nem védett / indifferens
Tachysphex grandii	Nem védett / indifferens
Tachysphex helveticus	Nem védett / indifferens
Tachysphex incertus	Nem védett / indifferens
Tachysphex mediterraneus	Nem védett / indifferens
Tachysphex mocsaryi	Nem védett / indifferens
Tachysphex nitidus	Nem védett / indifferens
Zygina suavis	Nem védett / indifferens
Tachysphex obscuripennis	Nem védett / indifferens
Tachysphex panzeri	Nem védett / indifferens
Tachysphex plicosus	Nem védett / indifferens
Tachysphex pompiliformis	Nem védett / indifferens
Tachysphex psammobius	Nem védett / indifferens
Tachysphex rugosus	Nem védett / indifferens
Tachysphex tarsinus	Nem védett / indifferens
Tachyta nana	Nem védett / indifferens
Tachytes etruscus	Nem védett / indifferens
Tachytes europaeus	Nem védett / indifferens
Tachytes obsoletus	Nem védett / indifferens
Tachyusa coarctata	Nem védett / indifferens
Tachyusa concinna	Nem védett / indifferens
Tachyusa constricta	Nem védett / indifferens
Tachyusa exarata	Nem védett / indifferens
Tachyusa nitella	Nem védett / indifferens
Tachyusa objecta	Nem védett / indifferens
Tachyusa scitula	Nem védett / indifferens
Tadorna ferruginea	Védett
Tadorna tadorna	Védett
Taeniapion rufulum	Nem védett / indifferens
Taeniapion urticarium	Nem védett / indifferens
Taeniopteryx araneoides	Nem védett / indifferens
Taeniopteryx nebulosa	Nem védett / indifferens
Taeniopteryx schoenemundi	Védett
Sitticus aequipes	Nem védett / indifferens
Sitticus monticola	Nem védett / indifferens
Sitticus petrensis	Nem védett / indifferens
Sitticus thorelli	Nem védett / indifferens
Taleporia politella	Nem védett / indifferens
Taleporia tubulosa	Nem védett / indifferens
Talis quercella	Nem védett / indifferens
Tallusia experta	Nem védett / indifferens
Tallusia vindobonensis	Nem védett / indifferens
Talpa europaea	Védett
Tandonia budapestensis	Nem védett / indifferens
Tandonia rustica	Nem védett / indifferens
Tanymastix lacunae	Nem védett / indifferens
Tanymecus dilaticollis	Nem védett / indifferens
Tanymecus palliatus	Nem védett / indifferens
Tanysphyrus ater	Nem védett / indifferens
Tanysphyrus lemnae	Nem védett / indifferens
Tapeinotus sellatus	Nem védett / indifferens
Taphropeltus contractus	Nem védett / indifferens
Taphropeltus hamulatus	Nem védett / indifferens
Taphrorychus bicolor	Nem védett / indifferens
Taphrocoetes hirtellus	Nem védett / indifferens
Taphrorychus mecedanus	Nem védett / indifferens
Taphrorychus villifrons	Nem védett / indifferens
Taphrotopium sulcifrons	Nem védett / indifferens
Tapinocyba affinis	Nem védett / indifferens
Tapinocyba insecta	Nem védett / indifferens
Tapinocyboides pygmaeus	Nem védett / indifferens
Tapinoma ambiguum	Nem védett / indifferens
Tapinoma erraticum	Nem védett / indifferens
Tapinopa longidens	Nem védett / indifferens
Taranucnus setosus	Nem védett / indifferens
Targionia vitis	Nem védett / indifferens
Tarsostenus univittatus	Nem védett / indifferens
Eumodicogryllus bordigalensis	Nem védett / indifferens
Tasgius ater	Nem védett / indifferens
Tasgius melanarius	Nem védett / indifferens
Tasgius morsitans	Nem védett / indifferens
Tasgius pedator	Nem védett / indifferens
Tasgius winkleri	Nem védett / indifferens
Tatianaerhynchites aequatus	Nem védett / indifferens
Taxicera deplanata	Nem védett / indifferens
Taxicera sericophila	Nem védett / indifferens
Tebenna bjerkandrella	Nem védett / indifferens
Tebenna micalis	Nem védett / indifferens
Tecmerium perplexum	Nem védett / indifferens
Tegenaria agrestis	Nem védett / indifferens
Malthonica campestris	Nem védett / indifferens
Tegenaria domestica	Nem védett / indifferens
Malthonica ferruginea	Nem védett / indifferens
Malthonica nemorosa	Nem védett / indifferens
Malthonica silvestris	Nem védett / indifferens
Telechrysis tripuncta	Nem védett / indifferens
Teleiodes flavimaculella	Nem védett / indifferens
Teleiodes luculella	Nem védett / indifferens
Teleiodes saltuum	Nem védett / indifferens
Teleiodes sequax	Nem védett / indifferens
Teleiodes vulgella	Nem védett / indifferens
Teleiodes wagae	Nem védett / indifferens
Teleiopsis diffinis	Nem védett / indifferens
Telmatophilus brevicollis	Nem védett / indifferens
Telmatophilus caricis	Nem védett / indifferens
Telmatophilus schoenherri	Nem védett / indifferens
Telmatophilus sparganii	Nem védett / indifferens
Telmatophilus typhae	Nem védett / indifferens
Teloleuca pellucens	Nem védett / indifferens
Telostegus inermis	Nem védett / indifferens
Temnocerus longiceps	Nem védett / indifferens
Temnocerus nanus	Nem védett / indifferens
Temnocerus tomentosus	Nem védett / indifferens
Temnochila coerulea	Nem védett / indifferens
Temnostethus dacicus	Nem védett / indifferens
Temnostethus gracilis	Nem védett / indifferens
Temnostethus longirostris	Nem védett / indifferens
Temnostethus pusillus	Nem védett / indifferens
Temnostethus reduvinus	Nem védett / indifferens
Temnostoma apiforme	Nem védett / indifferens
Temnostoma bombylans	Nem védett / indifferens
Temnostoma meridionale	Nem védett / indifferens
Temnostoma vespiforme	Nem védett / indifferens
Tenebrio molitor	Nem védett / indifferens
Tenebrio obscurus	Nem védett / indifferens
Tenebrio opacus	Védett
Tenebroides fuscus	Nem védett / indifferens
Tenebroides mauritanicus	Nem védett / indifferens
Tenuiphantes alacris	Nem védett / indifferens
Tenuiphantes cristatus	Nem védett / indifferens
Tenuiphantes flavipes	Nem védett / indifferens
Tenuiphantes mengei	Nem védett / indifferens
Tenuiphantes tenebricola	Nem védett / indifferens
Tenuiphantes tenuis	Nem védett / indifferens
Tenuiphantes zimmermanni	Nem védett / indifferens
Isturgia arenacearia	Nem védett / indifferens
Isturgia murinaria	Nem védett / indifferens
Tephronia sepiaria	Nem védett / indifferens
Teplinus velatus	Nem védett / indifferens
Teratocoris antennatus	Nem védett / indifferens
Involvulus aethiops	Nem védett / indifferens
Haplorhynchites caeruleus	Nem védett / indifferens
Teretrius fabricii	Nem védett / indifferens
Haplochrois albanica	Nem védett / indifferens
Haplochrois ochraceella	Nem védett / indifferens
Tetartopeus quadratus	Nem védett / indifferens
Tetartopeus rufonitidus	Nem védett / indifferens
Tetartopeus scutellaris	Nem védett / indifferens
Tetartopeus terminatus	Nem védett / indifferens
Tetartostylus pellucidus	Nem védett / indifferens
Tethea ocularis	Nem védett / indifferens
Tethea or	Nem védett / indifferens
Tetheella fluctuosa	Nem védett / indifferens
Tetrabrachys connatus	Nem védett / indifferens
Tetragnatha dearmata	Nem védett / indifferens
Tetragnatha extensa pulchra	Nem védett / indifferens
Tetragnatha montana	Nem védett / indifferens
Tetragnatha nigrita	Nem védett / indifferens
Tetragnatha obtusa	Nem védett / indifferens
Tetragnatha pinicola	Nem védett / indifferens
Tetragnatha reimoseri	Védett
Tetragnatha shoshone	Védett
Tetragnatha striata	Védett
Tetralonia alternans	Nem védett / indifferens
Tetralonia alticincta	Nem védett / indifferens
Tetralonia armeniaca	Nem védett / indifferens
Tetralonia dentata	Nem védett / indifferens
Tetralonia fulvescens	Nem védett / indifferens
Tetralonia graja	Nem védett / indifferens
Tetralonia hungarica	Nem védett / indifferens
Tetralonia lyncea	Nem védett / indifferens
Tetralonia macroglossa	Nem védett / indifferens
Tetralonia nana	Nem védett / indifferens
Tetralonia pollinosa	Nem védett / indifferens
Tetralonia salicariae	Nem védett / indifferens
Tetralonia scabiosae	Nem védett / indifferens
Tetralonia tricincta	Nem védett / indifferens
Tetramorium caespitum	Nem védett / indifferens
Tetramorium hungaricum	Nem védett / indifferens
Tetramorium impurum	Nem védett / indifferens
Tetramorium semilaeve	Nem védett / indifferens
Tetrao tetrix	Védett
Tetrao urogallus	Védett
Tetrao urogallus major	Védett
Tetraphleps bicuspis	Nem védett / indifferens
Tetratoma ancora	Nem védett / indifferens
Tetratoma desmarestii	Nem védett / indifferens
Tetratoma fungorum	Nem védett / indifferens
Tetrax tetrax	Fokozottan védett
Tetrix bipunctata	Nem védett / indifferens
Tetrix depressa	Nem védett / indifferens
Tetrix tenuicornis	Nem védett / indifferens
Tetrix subulata	Nem védett / indifferens
Tetrix undulata	Nem védett / indifferens
Tetropium castaneum	Nem védett / indifferens
Tetropium fuscum	Nem védett / indifferens
Tetropium gabrieli	Nem védett / indifferens
Tetrops praeustus	Nem védett / indifferens
Tetrops starkii	Nem védett / indifferens
Tettigometra atra	Nem védett / indifferens
Tettigometra atrata	Nem védett / indifferens
Tettigometra concolor	Nem védett / indifferens
Tettigometra depressa	Nem védett / indifferens
Tettigometra fusca	Nem védett / indifferens
Tettigometra impressopunctata	Nem védett / indifferens
Tettigometra obliqua	Nem védett / indifferens
Tettigometra sordida	Nem védett / indifferens
Tettigometra sulphurea	Nem védett / indifferens
Tettigometra virescens	Nem védett / indifferens
Tettigonia cantans	Nem védett / indifferens
Tettigonia caudata	Védett
Tettigonia viridissima	Nem védett / indifferens
Teuchestes fossor	Nem védett / indifferens
Textrix denticulata	Nem védett / indifferens
Thalassophilus longicornis	Nem védett / indifferens
Thalera fimbrialis	Nem védett / indifferens
Thalpophila matura	Nem védett / indifferens
Thalycra fervida	Nem védett / indifferens
Thambus frivaldskyi	Nem védett / indifferens
Thamiaraea cinnamomea	Nem védett / indifferens
Thamiaraea hospita	Nem védett / indifferens
Thamiocolus imperialis	Nem védett / indifferens
Thamiocolus kraatzi	Nem védett / indifferens
Thamiocolus nubeculosus	Nem védett / indifferens
Thamiocolus pubicollis	Nem védett / indifferens
Thamiocolus signatus	Nem védett / indifferens
Thamiocolus sinapis	Nem védett / indifferens
Thamiocolus viduatus	Nem védett / indifferens
Thamiocolus virgatus	Nem védett / indifferens
Thamnotettix confinis	Nem védett / indifferens
Thamnotettix dilutior	Nem védett / indifferens
Thamnotettix exemptus	Nem védett / indifferens
Thamnotettix liberatus	Nem védett / indifferens
Thamnurgus kaltenbachii	Nem védett / indifferens
Thamnurgus varipes	Nem védett / indifferens
Thanasimus formicarius	Nem védett / indifferens
Thanasimus rufipes	Nem védett / indifferens
Thanatophilus dispar	Nem védett / indifferens
Thanatophilus rugosus	Nem védett / indifferens
Thanatophilus sinuatus	Nem védett / indifferens
Thanatus arenarius	Nem védett / indifferens
Thanatus atratus	Nem védett / indifferens
Thanatus formicinus	Nem védett / indifferens
Thanatus pictus	Nem védett / indifferens
Thanatus sabulosus	Nem védett / indifferens
Thanatus striatus	Nem védett / indifferens
Thanatus vulgaris	Nem védett / indifferens
Thaumetopoea processionea	Nem védett / indifferens
Theba pisana	Nem védett / indifferens
Thecla betulae	Védett
Thecturota marchii	Nem védett / indifferens
Theodoxus danubialis	Védett
Theodoxus danubialis stragulatus	Nem védett / indifferens
Theodoxus fluviatilis	Nem védett / indifferens
Theodoxus prevostianus	Fokozottan védett
Theodoxus transversalis	Védett
Theonina cornix	Nem védett / indifferens
Theophilea subcylindricollis	Védett
Thera britannica	Nem védett / indifferens
Thera cognata	Nem védett / indifferens
Thera firmata	Nem védett / indifferens
Thera juniperata	Nem védett / indifferens
Thera obeliscata	Nem védett / indifferens
Thera variata	Nem védett / indifferens
Thera vetustata	Nem védett / indifferens
Therapis flavicaria	Nem védett / indifferens
Theresimima ampellophaga	Nem védett / indifferens
Theria rupicapraria	Nem védett / indifferens
Sardinidion blackwalli	Nem védett / indifferens
Theridion familiare	Nem védett / indifferens
Theridion hemerobius	Nem védett / indifferens
Phylloneta impressum	Nem védett / indifferens
Theridion melanurum	Nem védett / indifferens
Theridion mystaceum	Nem védett / indifferens
Heterotheridion nigrovariegatum	Nem védett / indifferens
Theridion pictum	Nem védett / indifferens
Theridion pinastri	Nem védett / indifferens
Phylloneta sisyphia	Nem védett / indifferens
Theridion varians	Nem védett / indifferens
Theridiosoma gemmosum	Nem védett / indifferens
Therioplectes gigas	Nem védett / indifferens
Thermocyclops crassus	Nem védett / indifferens
Thermocyclops dybowskii	Nem védett / indifferens
Thermocyclops oithonoides	Nem védett / indifferens
Theromyzon tessulatum	Nem védett / indifferens
Thes bergrothi	Nem védett / indifferens
Thiasophila angulata	Nem védett / indifferens
Thinobius brevipennis	Nem védett / indifferens
Thinobius ciliatus	Nem védett / indifferens
Thinobius rambouseki	Nem védett / indifferens
Thinodromus arcuatus	Nem védett / indifferens
Thinodromus dilatatus	Nem védett / indifferens
Thinodromus hirticollis	Nem védett / indifferens
Thinonoma atra	Nem védett / indifferens
Thiodia citrana	Nem védett / indifferens
Thiodia lerneana	Nem védett / indifferens
Thiodia torridana	Nem védett / indifferens
Thiodia trochilana	Nem védett / indifferens
Thiotricha subocellea	Nem védett / indifferens
Thisanotia chrysonuchella	Nem védett / indifferens
Tholera cespitis	Nem védett / indifferens
Tholera decimalis	Nem védett / indifferens
Thomisus onustus	Nem védett / indifferens
Thoracophorus corticinus	Nem védett / indifferens
Thryogenes atrirostris	Nem védett / indifferens
Thryogenes festucae	Nem védett / indifferens
Thryogenes nereis	Nem védett / indifferens
Thryogenes scirrhosus	Nem védett / indifferens
Thumatha senex	Nem védett / indifferens
Thyatira batis	Nem védett / indifferens
Thymallus thymallus	Védett
Thymalus limbatus	Nem védett / indifferens
Thymelicus acteon	Védett
Thymelicus lineola	Nem védett / indifferens
Thymelicus sylvestris	Nem védett / indifferens
Thyreocoris fulvipennis	Nem védett / indifferens
Thyreocoris scarabaeoides	Nem védett / indifferens
Thyreosthenius biovatus	Nem védett / indifferens
Thyreosthenius parasiticus	Nem védett / indifferens
Thyreus affinis	Nem védett / indifferens
Thyreus histrionicus	Nem védett / indifferens
Thyreus orbatus	Nem védett / indifferens
Thyreus ramosus	Nem védett / indifferens
Thyreus truncatus	Nem védett / indifferens
Thyris fenestrella	Nem védett / indifferens
Tibellus macellus	Nem védett / indifferens
Tibellus maritimus	Nem védett / indifferens
Tibellus oblongus	Nem védett / indifferens
Lyristes plebejus	Nem védett / indifferens
Tibicina haematodes	Védett
Tichodroma muraria	Védett
Tiliacea aurago	Nem védett / indifferens
Tiliacea citrago	Nem védett / indifferens
Tiliacea sulphurago	Nem védett / indifferens
Tilloidea unifasciata	Nem védett / indifferens
Tillus elongatus	Nem védett / indifferens
Tillus pallidipennis	Nem védett / indifferens
Timandra comae	Nem védett / indifferens
Tinagma balteolella	Nem védett / indifferens
Tinagma ocnerostomella	Nem védett / indifferens
Tinagma perdicella	Nem védett / indifferens
Tinca tinca	Nem védett / indifferens
Tinea columbariella	Nem védett / indifferens
Tinea dubiella	Nem védett / indifferens
Tinea nonimella	Nem védett / indifferens
Tinea pallescentella	Nem védett / indifferens
Tinea pellionella	Nem védett / indifferens
Tinea semifulvella	Nem védett / indifferens
Tinea translucens	Nem védett / indifferens
Tinea trinotella	Nem védett / indifferens
Tineola bisselliella	Nem védett / indifferens
Tingis ampliata	Nem védett / indifferens
Tingis angustata	Nem védett / indifferens
Tingis auriculata	Nem védett / indifferens
Tingis cardui	Nem védett / indifferens
Tingis caucasica	Nem védett / indifferens
Tingis crispata	Nem védett / indifferens
Tingis geniculata	Nem védett / indifferens
Tingis grisea	Nem védett / indifferens
Tingis maculata	Nem védett / indifferens
Tingis marrubii	Nem védett / indifferens
Tingis pilosa	Nem védett / indifferens
Tingis ragusana	Nem védett / indifferens
Tingis reticulata	Nem védett / indifferens
Tinicephalus hortulanus	Nem védett / indifferens
Tinodes pallidulus	Nem védett / indifferens
Tinodes rostocki	Nem védett / indifferens
Tinodes unicolor	Nem védett / indifferens
Tinodes waeneri	Nem védett / indifferens
Tinotus morion	Nem védett / indifferens
Tinthia brosiformis	Nem védett / indifferens
Tinthia tineiformis	Nem védett / indifferens
Tiphia femorata	Nem védett / indifferens
Tiphia fulvipennis	Nem védett / indifferens
Tiphia lativentris	Nem védett / indifferens
Tiphia minuta	Nem védett / indifferens
Tiphia morio	Nem védett / indifferens
Tiphia ruficornis	Nem védett / indifferens
Tischeria decidua	Nem védett / indifferens
Tischeria dodonaea	Nem védett / indifferens
Tischeria ekebladella	Nem védett / indifferens
Tiso aestivus	Nem védett / indifferens
Tiso vagans	Nem védett / indifferens
Titanio normalis	Nem védett / indifferens
Titanoeca quadriguttata	Nem védett / indifferens
Titanoeca schineri	Nem védett / indifferens
Titanoeca tristis	Nem védett / indifferens
Titanoeca veteranica	Nem védett / indifferens
Tmarus piger	Nem védett / indifferens
Tmarus stellio	Nem védett / indifferens
Tolypotheca emese	Nem védett / indifferens
Tomicus minor	Nem védett / indifferens
Tomicus piniperda	Nem védett / indifferens
Tomoglossa luteicornis	Nem védett / indifferens
Tomoxia bucephala	Nem védett / indifferens
Tonnacypris lutaria	Nem védett / indifferens
Torleya major	Nem védett / indifferens
Tortricodes alternella	Nem védett / indifferens
Tortrix viridana	Nem védett / indifferens
Tournotaris bimaculata	Nem védett / indifferens
Tournotaris granulipennis	Nem védett / indifferens
Toya propinqua	Nem védett / indifferens
Trachea atriplicis	Nem védett / indifferens
Trachelas maculatus	Nem védett / indifferens
Tracheliastes chondrostomi	Nem védett / indifferens
Tracheliastes maculatus	Nem védett / indifferens
Tracheliastes polycolpus	Nem védett / indifferens
Tracheliodes curvitarsis	Nem védett / indifferens
Trachodes hispidus	Nem védett / indifferens
Trachonitis cristella	Nem védett / indifferens
Trachycera advenella	Nem védett / indifferens
Trachycera dulcella	Nem védett / indifferens
Trachycera legatea	Nem védett / indifferens
Trachycera marmorea	Nem védett / indifferens
Trachycera suavella	Nem védett / indifferens
Trachyphloeus alternans	Nem védett / indifferens
Trachyphloeus angustisetulus	Nem védett / indifferens
Trachyphloeus aristatus	Nem védett / indifferens
Trachyphloeus asperatus	Nem védett / indifferens
Trachyphloeus bifoveolatus	Nem védett / indifferens
Trachyphloeus frivaldszkyi	Nem védett / indifferens
Trachyphloeus inermis	Nem védett / indifferens
Trachyphloeus parallelus	Nem védett / indifferens
Trachyphloeus scabriculus	Nem védett / indifferens
Trachyphloeus spinimanus	Nem védett / indifferens
Trachyphloeus ventricosus	Nem védett / indifferens
Trachypteris picta	Nem védett / indifferens
Trachypteris picta decostigma	Nem védett / indifferens
Trachys coruscus	Nem védett / indifferens
Trachys fragariae	Nem védett / indifferens
Trachys minutus	Nem védett / indifferens
Trachys problematicus	Nem védett / indifferens
Trachys puncticollis	Nem védett / indifferens
Trachys puncticollis rectilineatus	Nem védett / indifferens
Trachys scrobiculatus	Nem védett / indifferens
Trachys troglodytes	Nem védett / indifferens
Trachysphaera costata	Nem védett / indifferens
Trachysphaera gibbula	Nem védett / indifferens
Trachysphaera noduligera	Nem védett / indifferens
Trachysphaera noduligera hungarica	Nem védett / indifferens
Trachyzelotes pedestris	Nem védett / indifferens
Tragosoma depsarium	Nem védett / indifferens
Trajancypris clavata	Nem védett / indifferens
Trajancypris laevis	Nem védett / indifferens
Trajancypris serrata	Nem védett / indifferens
Trapezonotus arenarius	Nem védett / indifferens
Trapezonotus dispar	Nem védett / indifferens
Trapezonotus ullrichi	Nem védett / indifferens
Traumoecia picipes	Nem védett / indifferens
Trechoblemus micros	Nem védett / indifferens
Trechus austriacus	Nem védett / indifferens
Trechus obtusus	Nem védett / indifferens
Trechus pilisensis	Nem védett / indifferens
Trechus quadristriatus	Nem védett / indifferens
Trematocephalus cristatus	Nem védett / indifferens
Tretocephala ambigua	Nem védett / indifferens
Triaenodes bicolor	Nem védett / indifferens
Triarthron maerkeli	Nem védett / indifferens
Triaxomasia caprimulgella	Nem védett / indifferens
Triaxomera fulvimitrella	Nem védett / indifferens
Triaxomera parasitella	Nem védett / indifferens
Tribolium castaneum	Nem védett / indifferens
Tribolium confusum	Nem védett / indifferens
Tribolium destructor	Nem védett / indifferens
Tribolium madens	Nem védett / indifferens
Trichia erjaveci	Nem védett / indifferens
Trichia hispida	Nem védett / indifferens
Trichia lubomirskii	Védett
Trichia striolata	Védett
Trichia striolata danubialis	Nem védett / indifferens
Trichiura crataegi	Nem védett / indifferens
Trichius fasciatus	Nem védett / indifferens
Trichius sexualis	Nem védett / indifferens
Trichiusa immigrata	Nem védett / indifferens
Trichoceble floralis	Nem védett / indifferens
Dicheirotrichus placidus	Nem védett / indifferens
Trichodes apiarius	Nem védett / indifferens
Trichodes favarius	Nem védett / indifferens
Trichoferus holosericeus	Nem védett / indifferens
Trichoferus pallidus	Védett
Tricholeiochiton fagesii	Nem védett / indifferens
Trichoncoides piscator	Nem védett / indifferens
Trichoncus affinis	Nem védett / indifferens
Trichoncus auritus	Nem védett / indifferens
Trichoncus scrofa	Nem védett / indifferens
Trichonotulus scrofa	Nem védett / indifferens
Trichonyx sulcicollis	Nem védett / indifferens
Trichophaga tapetzella	Nem védett / indifferens
Trichophya pilicornis	Nem védett / indifferens
Trichoplusia ni	Nem védett / indifferens
Trichopsomyia carbonaria	Nem védett / indifferens
Trichopsomyia flavitarsis	Nem védett / indifferens
Trichopterapion holosericeum	Nem védett / indifferens
Trichopterna cito	Nem védett / indifferens
Trichopternoides thorelli	Nem védett / indifferens
Trichopteryx carpinata	Nem védett / indifferens
Trichopteryx polycommata	Nem védett / indifferens
Trichosirocalus barnevillei	Nem védett / indifferens
Trichosirocalus campanella	Nem védett / indifferens
Trichosirocalus horridus	Nem védett / indifferens
Trichosirocalus rufulus	Nem védett / indifferens
Trichosirocalus thalammeri	Nem védett / indifferens
Trichosirocalus troglodytes	Nem védett / indifferens
Trichostegia minor	Nem védett / indifferens
Trichotichnus laevicollis	Nem védett / indifferens
Trichrysis cyanea	Nem védett / indifferens
Xya pfaendleri	Nem védett / indifferens
Xya variegata	Nem védett / indifferens
Triepeolus tristis	Nem védett / indifferens
Trifurcula beirnei	Nem védett / indifferens
Trifurcula chamaecytisi	Nem védett / indifferens
Trifurcula cryptella	Nem védett / indifferens
Trifurcula eurema	Nem védett / indifferens
Trifurcula headleyella	Nem védett / indifferens
Trifurcula josefklimeschi	Nem védett / indifferens
Trifurcula melanoptera	Nem védett / indifferens
Trifurcula ortneri	Nem védett / indifferens
Trifurcula pallidella	Nem védett / indifferens
Trifurcula thymi	Nem védett / indifferens
Triglyphus primus	Nem védett / indifferens
Trigonogenius globosus	Nem védett / indifferens
Trigonotylus caelestialium	Nem védett / indifferens
Trigonotylus pulchellus	Nem védett / indifferens
Trigonotylus ruficornis	Nem védett / indifferens
Trimium brevicorne	Nem védett / indifferens
Trimium carpathicum	Nem védett / indifferens
Tringa erythropus	Védett
Tringa flavipes	Védett
Tringa glareola	Védett
Tringa nebularia	Védett
Tringa ochropus	Védett
Tringa stagnatilis	Fokozottan védett
Tringa totanus	Fokozottan védett
Trinodes hirtus	Nem védett / indifferens
Triodia amasina	Védett
Triodia sylvina	Nem védett / indifferens
Trionymus aberrans	Nem védett / indifferens
Trionymus elymi	Nem védett / indifferens
Trionymus hamberdi	Nem védett / indifferens
Trionymus isfarensis	Nem védett / indifferens
Trionymus luzensis	Nem védett / indifferens
Trionymus multivorus	Nem védett / indifferens
Trionymus newsteadi	Nem védett / indifferens
Trionymus perrisii	Nem védett / indifferens
Trionymus phalaridis	Nem védett / indifferens
Trionymus phragmitis	Nem védett / indifferens
Trionymus placatus	Nem védett / indifferens
Trionymus radicum	Nem védett / indifferens
Trionymus singularis	Nem védett / indifferens
Trionymus thulensis	Nem védett / indifferens
Trionymus tomlini	Nem védett / indifferens
Triops cancriformis	Nem védett / indifferens
Triphosa dubitata	Nem védett / indifferens
Triphyllus bicolor	Nem védett / indifferens
Triplax aenea	Nem védett / indifferens
Triplax collaris	Nem védett / indifferens
Triplax elongata	Nem védett / indifferens
Triplax lacordairii	Nem védett / indifferens
Triplax lepida	Nem védett / indifferens
Triplax melanocephala	Nem védett / indifferens
Triplax pygmaea	Nem védett / indifferens
Triplax rufipes	Nem védett / indifferens
Triplax russica	Nem védett / indifferens
Triplax scutellaris	Nem védett / indifferens
Trisateles emortualis	Nem védett / indifferens
Trissemus antennatus	Nem védett / indifferens
Trissemus antennatus serricornis	Nem védett / indifferens
Trissemus impressus	Nem védett / indifferens
Tritoma bipustulata	Nem védett / indifferens
Tritomegas bicolor	Nem védett / indifferens
Tritomegas sexmaculatus	Nem védett / indifferens
Triturus alpestris	Fokozottan védett
Triturus carnifex	Védett
Triturus cristatus	Védett
Triturus dobrogicus	Védett
Triturus vulgaris	Védett
Trixagus duvalii	Nem védett / indifferens
Trixagus carinifrons	Nem védett / indifferens
Trixagus dermestoides	Nem védett / indifferens
Trixagus elateroides	Nem védett / indifferens
Trixagus exul	Nem védett / indifferens
Trocheta bykowskii	Nem védett / indifferens
Trocheta cylindrica	Nem védett / indifferens
Trocheta riparia	Nem védett / indifferens
Trochosa robusta	Nem védett / indifferens
Trochosa ruricola	Nem védett / indifferens
Trochosa spinipalpis	Nem védett / indifferens
Trochosa terricola	Nem védett / indifferens
Troglodytes troglodytes	Védett
Troglops albicans	Nem védett / indifferens
Troglops cephalotes	Nem védett / indifferens
Troglops silo	Nem védett / indifferens
Trogoderma glabrum	Nem védett / indifferens
Trogoderma granarium	Nem védett / indifferens
Trogoderma megatomoides	Nem védett / indifferens
Trogoderma versicolor	Nem védett / indifferens
Trogoxylon impressum	Nem védett / indifferens
Troilus luridus	Nem védett / indifferens
Tropideres albirostris	Nem védett / indifferens
Tropideres dorsalis	Nem védett / indifferens
Tropidia fasciata	Nem védett / indifferens
Tropidia scita	Nem védett / indifferens
Tropidocephala andropogonis	Nem védett / indifferens
Tropidodynerus interruptus	Nem védett / indifferens
Tropidophlebia costalis	Nem védett / indifferens
Tropidothorax leucopterus	Nem védett / indifferens
Tropidotilla littoralis	Nem védett / indifferens
Tropiphorus cucullatus	Nem védett / indifferens
Tropiphorus elevatus	Nem védett / indifferens
Tropiphorus micans	Nem védett / indifferens
Tropistethus holosericus	Nem védett / indifferens
Tropocyclops prasinus	Nem védett / indifferens
Trox cadaverinus	Nem védett / indifferens
Trox eversmanni	Nem védett / indifferens
Trox hispidus	Nem védett / indifferens
Trox niger	Nem védett / indifferens
Trox perrisii	Nem védett / indifferens
Trox sabulosus	Nem védett / indifferens
Trox scaber	Nem védett / indifferens
Troxochrus scabriculus	Nem védett / indifferens
Truncatellina callicratis	Nem védett / indifferens
Truncatellina claustralis	Nem védett / indifferens
Truncatellina cylindrica	Nem védett / indifferens
Tryngites subruficollis	Védett
Trypetimorpha fenestrata	Nem védett / indifferens
Trypocopris vernalis	Nem védett / indifferens
Trypodendron domesticum	Nem védett / indifferens
Trypodendron lineatum	Nem védett / indifferens
Trypodendron signatum	Nem védett / indifferens
Trypophloeus asperatus	Nem védett / indifferens
Trypophloeus granulatus	Nem védett / indifferens
Trypophloeus rybinskii	Nem védett / indifferens
Trypoxylon attenuatum	Nem védett / indifferens
Trypoxylon clavicerum	Nem védett / indifferens
Trypoxylon figulus	Nem védett / indifferens
Trypoxylon fronticorne	Nem védett / indifferens
Trypoxylon kolazyi	Nem védett / indifferens
Trypoxylon scutatum	Nem védett / indifferens
Tuponia elegans	Nem védett / indifferens
Tuponia hippophaes	Nem védett / indifferens
Tuponia mixticolor	Nem védett / indifferens
Tuponia prasina	Nem védett / indifferens
Turdus iliacus	Védett
Turdus merula	Védett
Turdus naumanni	Védett
Turdus philomelos	Védett
Turdus pilaris	Védett
Turdus torquatus	Védett
Turdus torquatus alpestris	Védett
Turdus viscivorus	Védett
Turrutus socialis	Nem védett / indifferens
Tychius aureolus	Nem védett / indifferens
Tychius breviusculus	Nem védett / indifferens
Tychius caldarai	Nem védett / indifferens
Tychius crassirostris	Nem védett / indifferens
Tychius cuprifer	Nem védett / indifferens
Tychius flavus	Nem védett / indifferens
Tychius junceus	Nem védett / indifferens
Tychius kulzeri	Nem védett / indifferens
Tychius lineatulus	Nem védett / indifferens
Tychius medicaginis	Nem védett / indifferens
Tychius meliloti	Nem védett / indifferens
Tychius parallelus	Nem védett / indifferens
Tychius picirostris	Nem védett / indifferens
Tychius polylineatus	Nem védett / indifferens
Tychius pumilus	Nem védett / indifferens
Tychius pusillus	Nem védett / indifferens
Aulacobaris quinquepunctatus	Nem védett / indifferens
Tychius rufipennis	Nem védett / indifferens
Tychius schneideri	Nem védett / indifferens
Tychius sharpi	Nem védett / indifferens
Tychius squamulatus	Nem védett / indifferens
Tychius stephensi	Nem védett / indifferens
Tychius striatulus	Nem védett / indifferens
Tychius subsulcatus	Nem védett / indifferens
Tychius tibialis	Nem védett / indifferens
Tychius tridentinus	Nem védett / indifferens
Tychius trivialis	Nem védett / indifferens
Tychus dalmatinus	Nem védett / indifferens
Tychus niger	Nem védett / indifferens
Tychus rufus	Nem védett / indifferens
Typhaea stercorea	Nem védett / indifferens
Zonocyba bifasciata	Nem védett / indifferens
Typhlocyba quercus	Nem védett / indifferens
Typhloiulus polypodus	Nem védett / indifferens
Typhochrestus digitatus	Nem védett / indifferens
Tyria jacobaeae	Védett
Tyrus mucronatus	Nem védett / indifferens
Tyta luctuosa	Nem védett / indifferens
Tyto alba	Fokozottan védett
Tyto alba guttata	Fokozottan védett
Tytthaspis sedecimpunctata	Nem védett / indifferens
Tytthus pygmaeus	Nem védett / indifferens
Udea accolalis	Nem védett / indifferens
Udea ferrugalis	Nem védett / indifferens
Udea fulvalis	Nem védett / indifferens
Udea inquinatalis	Nem védett / indifferens
Udea institalis	Nem védett / indifferens
Udea lutealis	Nem védett / indifferens
Udea nebulalis	Nem védett / indifferens
Udea olivalis	Nem védett / indifferens
Udea prunalis	Nem védett / indifferens
Uleiota planata	Nem védett / indifferens
Ulmicola spinipes	Nem védett / indifferens
Uloborus plumipes	Nem védett / indifferens
Uloborus walckenaerius	Nem védett / indifferens
Uloma culinaris	Nem védett / indifferens
Uloma rufa	Nem védett / indifferens
Ulopa reticulata	Nem védett / indifferens
Ulorhinus bilineatus	Nem védett / indifferens
Umbra krameri	Fokozottan védett
Unaspis euonymi	Nem védett / indifferens
Unciger foetidus	Nem védett / indifferens
Unciger transsilvanicus	Nem védett / indifferens
Unio crassus	Védett
Unio pictorum	Nem védett / indifferens
Unio tumidus	Nem védett / indifferens
Upupa epops	Védett
Uranotaenia unguiculata	Nem védett / indifferens
Uresiphita gilvata	Nem védett / indifferens
Urocoras longispinus	Nem védett / indifferens
Urophorus rubripennis	Nem védett / indifferens
Ursus arctos	Fokozottan védett
Urticicola umbrosus	Nem védett / indifferens
Utecha lugens	Nem védett / indifferens
Utecha trivia	Nem védett / indifferens
Utetheisa pulchella	Nem védett / indifferens
Vadonia steveni	Védett
Vadonia unipunctata	Nem védett / indifferens
Valeria oleagina	Nem védett / indifferens
Valgus hemipterus	Nem védett / indifferens
Vallonia costata	Nem védett / indifferens
Vallonia enniensis	Nem védett / indifferens
Vallonia pulchella	Nem védett / indifferens
Valvata cristata	Nem védett / indifferens
Valvata macrostoma	Nem védett / indifferens
Valvata piscinalis	Nem védett / indifferens
Vanellus vanellus	Védett
Vanessa atalanta	Védett
Vanessa cardui	Nem védett / indifferens
Variimorda basalis	Nem védett / indifferens
Variimorda briantea	Nem védett / indifferens
Variimorda villosa	Nem védett / indifferens
Variimorda mendax	Nem védett / indifferens
Velia affinis	Nem védett / indifferens
Velia affinis filippii	Nem védett / indifferens
Velia caprai	Nem védett / indifferens
Velia saulii	Nem védett / indifferens
Velleius dilatatus	Nem védett / indifferens
Ventocoris trigonus	Nem védett / indifferens
Venustoraphidia nigricollis	Nem védett / indifferens
Verdanus abdominalis	Nem védett / indifferens
Diplocolenus nigrifrons	Nem védett / indifferens
Diplocolenus parcanicus	Nem védett / indifferens
Vertigo alpestris	Nem védett / indifferens
Vertigo angustior	Védett
Vertigo antivertigo	Nem védett / indifferens
Vertigo moulinsiana	Védett
Vertigo pusilla	Nem védett / indifferens
Vertigo pygmaea	Nem védett / indifferens
Vertigo substriata	Nem védett / indifferens
Vespa crabro	Nem védett / indifferens
Vespertilio murinus	Védett
Vespula austriaca	Nem védett / indifferens
Vespula germanica	Nem védett / indifferens
Vespula rufa	Nem védett / indifferens
Vespula vulgaris	Nem védett / indifferens
Vestia gulo	Védett
Vestia turgida	Védett
Vibidia duodecimguttata	Nem védett / indifferens
Viguierella coeca	Nem védett / indifferens
Viguierella paludosa	Nem védett / indifferens
Vilpianus galii	Nem védett / indifferens
Vimba vimba	Nem védett / indifferens
Vincenzellus ruficollis	Nem védett / indifferens
Vipera berus	Fokozottan védett
Vipera ursinii	Fokozottan védett
Vipera ursinii rakosiensis	Fokozottan védett
Vitrea contracta	Nem védett / indifferens
Vitrea crystallina	Nem védett / indifferens
Vitrea diaphana	Nem védett / indifferens
Vitrea subrimata	Nem védett / indifferens
Vitrea transsylvanica	Nem védett / indifferens
Vitrina pellucida	Nem védett / indifferens
Vittacoccus longicornis	Nem védett / indifferens
Vitula biviella	Nem védett / indifferens
Viviparus acerosus	Nem védett / indifferens
Viviparus contectus	Nem védett / indifferens
Volinus sticticus	Nem védett / indifferens
Volucella bombylans	Nem védett / indifferens
Volucella inanis	Nem védett / indifferens
Volucella inflata	Nem védett / indifferens
Volucella pellucens	Nem védett / indifferens
Volucella zonaria	Nem védett / indifferens
Vulcaniella extremella	Nem védett / indifferens
Vulcaniella pomposella	Nem védett / indifferens
Vulpes vulpes	Nem védett / indifferens
Wagneriala sinuata	Nem védett / indifferens
Walckenaeria acuminata	Nem védett / indifferens
Walckenaeria antica	Nem védett / indifferens
Walckenaeria atrotibialis	Nem védett / indifferens
Walckenaeria capito	Nem védett / indifferens
Walckenaeria corniculans	Nem védett / indifferens
Walckenaeria cucullata	Nem védett / indifferens
Walckenaeria cuspidata obsoleta	Nem védett / indifferens
Walckenaeria dysderoides	Nem védett / indifferens
Walckenaeria furcillata	Nem védett / indifferens
Walckenaeria incisa	Nem védett / indifferens
Walckenaeria kochi	Nem védett / indifferens
Walckenaeria mitrata	Nem védett / indifferens
Walckenaeria nodosa	Nem védett / indifferens
Walckenaeria nudipalpis	Nem védett / indifferens
Walckenaeria obtusa	Nem védett / indifferens
Walckenaeria simplex	Nem védett / indifferens
Walckenaeria unicornis	Nem védett / indifferens
Walckenaeria vigilax	Nem védett / indifferens
Watsonalla binaria	Nem védett / indifferens
Watsonalla cultraria	Nem védett / indifferens
Watsonarctia casta	Nem védett / indifferens
Wesmaelius concinnus	Nem védett / indifferens
Wesmaelius helveticus	Nem védett / indifferens
Wesmaelius malladai	Nem védett / indifferens
Wesmaelius mortoni	Nem védett / indifferens
Wesmaelius nervosus	Nem védett / indifferens
Wesmaelius quadrifasciatus	Nem védett / indifferens
Wesmaelius ravus	Nem védett / indifferens
Wesmaelius subnebulosus	Nem védett / indifferens
Wesmaelius tjederi	Nem védett / indifferens
Wheeleria obsoletus	Nem védett / indifferens
Whittleia paveli	Nem védett / indifferens
Whittleia undulella	Nem védett / indifferens
Witlesia pallida	Nem védett / indifferens
Wlassicsia pannonica	Nem védett / indifferens
Wockia asperipunctella	Nem védett / indifferens
Wormaldia occipitalis	Nem védett / indifferens
Xanthandrus comtus	Nem védett / indifferens
Xanthia gilvago	Nem védett / indifferens
Xanthia icteritia	Nem védett / indifferens
Xanthia ocellaris	Nem védett / indifferens
Xanthia togata	Nem védett / indifferens
Xanthochilus quadratus	Nem védett / indifferens
Xanthocrambus lucellus	Nem védett / indifferens
Xanthocrambus saxonellus	Nem védett / indifferens
Xanthodelphax flaveolus	Nem védett / indifferens
Xanthodelphax stramineus	Nem védett / indifferens
Xanthogramma dives	Nem védett / indifferens
Xanthogramma festivum	Nem védett / indifferens
Xanthogramma laetum	Nem védett / indifferens
Xanthogramma pedissequum	Nem védett / indifferens
Xantholinus azuganus	Nem védett / indifferens
Xantholinus coiffaiti	Nem védett / indifferens
Xantholinus decorus	Nem védett / indifferens
Xantholinus dvoraki	Nem védett / indifferens
Xantholinus elegans	Nem védett / indifferens
Xantholinus kaszabi	Nem védett / indifferens
Xantholinus laevigatus	Nem védett / indifferens
Xantholinus linearis	Nem védett / indifferens
Xantholinus longiventris	Nem védett / indifferens
Xantholinus tricolor	Nem védett / indifferens
Xanthoperla apicalis	Nem védett / indifferens
Xanthorhoe biriviata	Nem védett / indifferens
Xanthorhoe designata	Nem védett / indifferens
Xanthorhoe ferrugata	Nem védett / indifferens
Xanthorhoe fluctuata	Nem védett / indifferens
Xanthorhoe montanata	Nem védett / indifferens
Xanthorhoe quadrifasiata	Nem védett / indifferens
Xanthorhoe spadicearia	Nem védett / indifferens
Xanthosphaera barnevillii	Nem védett / indifferens
Xanthostigma xanthostigma	Nem védett / indifferens
Xenostrongylus arcuatus	Nem védett / indifferens
Xenota myrmecobia	Nem védett / indifferens
Xenus cinereus	Védett
Xerasia meschniggi	Védett
Xerochlorita dumosa	Nem védett / indifferens
Xerochlorita mendax	Nem védett / indifferens
Xerochlorita prasina	Nem védett / indifferens
Xerocnephasia rigana	Nem védett / indifferens
Xerolenta obvia	Nem védett / indifferens
Xerolycosa miniata	Nem védett / indifferens
Xerolycosa nemoralis	Nem védett / indifferens
Xestia ashworthii	Nem védett / indifferens
Xestia ashworthii candelarum	Nem védett / indifferens
Xestia baja	Nem védett / indifferens
Xestia castanea	Nem védett / indifferens
Xestia c-nigrum	Nem védett / indifferens
Xestia ditrapezium	Nem védett / indifferens
Xestia sexstrigata	Védett
Xestia stigmatica	Nem védett / indifferens
Xestia triangulum	Nem védett / indifferens
Xestia xanthographa	Nem védett / indifferens
Xestobium plumbeum	Nem védett / indifferens
Xestobium rufovillosum	Nem védett / indifferens
Xestoiulus imbecillus	Nem védett / indifferens
Xestoiulus laeticollis	Nem védett / indifferens
Xestoiulus laeticollis dudichi	Nem védett / indifferens
Xestoiulus laeticollis evae	Nem védett / indifferens
Xiphophorus helleri	Nem védett / indifferens
Xyleborinus saxesenii	Nem védett / indifferens
Xyleborus alni	Nem védett / indifferens
Xyleborus cryptographus	Nem védett / indifferens
Xyleborus dispar	Nem védett / indifferens
Xyleborus dryographus	Nem védett / indifferens
Xyleborus eurygraphus	Nem védett / indifferens
Xyleborus monographus	Nem védett / indifferens
Xyleborus pfeilii	Nem védett / indifferens
Xylena exsoleta	Nem védett / indifferens
Xylena vetusta	Nem védett / indifferens
Xyletinus ater	Nem védett / indifferens
Xyletinus distinguendus	Nem védett / indifferens
Xyletinus fibyensis	Nem védett / indifferens
Xyletinus laticollis	Nem védett / indifferens
Xyletinus longitarsis	Nem védett / indifferens
Xyletinus moraviensis	Nem védett / indifferens
Xyletinus pectinatus	Nem védett / indifferens
Xyletinus pseudooblongulus	Nem védett / indifferens
Xyletinus subrotundatus	Nem védett / indifferens
Xyletinus vaederoeensis	Nem védett / indifferens
Xylita laevigata	Nem védett / indifferens
Xylita livida	Nem védett / indifferens
Xylocleptes bispinus	Nem védett / indifferens
Xylococcus filiferus	Nem védett / indifferens
Xylocopa iris	Nem védett / indifferens
Xylocopa valga	Nem védett / indifferens
Xylocopa violacea	Nem védett / indifferens
Xylocoridea brevipennis	Nem védett / indifferens
Xylocoris cursitans	Nem védett / indifferens
Xylocoris formicetorum	Nem védett / indifferens
Xylocoris galactinus	Nem védett / indifferens
Xylocoris obliquus	Nem védett / indifferens
Xylodromus affinis	Nem védett / indifferens
Xylodromus concinnus	Nem védett / indifferens
Xylodromus depressus	Nem védett / indifferens
Xylodromus testaceus	Nem védett / indifferens
Xylographus bostrichoides	Nem védett / indifferens
Xylopertha retusa	Nem védett / indifferens
Xylophilus testaceus	Nem védett / indifferens
Xylostiba bosnica	Nem védett / indifferens
Xylostiba monilicornis	Nem védett / indifferens
Xylota abiens	Nem védett / indifferens
Xylota florum	Nem védett / indifferens
Xylota ignava	Nem védett / indifferens
Xylota meigeniana	Nem védett / indifferens
Xylota segnis	Nem védett / indifferens
Xylota sylvarum	Nem védett / indifferens
Xylota tarda	Nem védett / indifferens
Xylota xanthocnema	Nem védett / indifferens
Xylotrechus antilope	Nem védett / indifferens
Xylotrechus arvicola	Nem védett / indifferens
Xylotrechus pantherinus	Védett
Xylotrechus rusticus	Nem védett / indifferens
Xysticus acerbus	Nem védett / indifferens
Xysticus albomaculatus	Nem védett / indifferens
Xysticus audax	Nem védett / indifferens
Xysticus bifasciatus	Nem védett / indifferens
Xysticus cristatus	Nem védett / indifferens
Xysticus embriki	Nem védett / indifferens
Xysticus erraticus	Nem védett / indifferens
Xysticus gallicus	Nem védett / indifferens
Xysticus graecus	Nem védett / indifferens
Xysticus kempeleni	Nem védett / indifferens
Xysticus kochi	Nem védett / indifferens
Xysticus lanio	Nem védett / indifferens
Xysticus lendli	Nem védett / indifferens
Xysticus lineatus	Nem védett / indifferens
Xysticus luctator	Nem védett / indifferens
Xysticus luctuosus	Nem védett / indifferens
Xysticus marmoratus	Nem védett / indifferens
Xysticus ninnii	Nem védett / indifferens
Xysticus robustus	Nem védett / indifferens
Xysticus sabulosus	Nem védett / indifferens
Xysticus striatipes	Nem védett / indifferens
Xysticus ulmi	Nem védett / indifferens
Xystophora carchariella	Nem védett / indifferens
Xystophora pulveratella	Nem védett / indifferens
Dichagyris forcipula	Nem védett / indifferens
Dichagyris nigrescens	Nem védett / indifferens
Dichagyris signifera	Nem védett / indifferens
Sitticus arenarius	Nem védett / indifferens
Sitticus horvathi	Nem védett / indifferens
Sitticus vittatus	Nem védett / indifferens
Zygina tiliae	Nem védett / indifferens
Ylodes kawraiskii	Nem védett / indifferens
Ylodes simulans	Nem védett / indifferens
Yponomeuta cagnagella	Nem védett / indifferens
Yponomeuta evonymella	Nem védett / indifferens
Yponomeuta irrorella	Nem védett / indifferens
Yponomeuta malinellus	Nem védett / indifferens
Yponomeuta padella	Nem védett / indifferens
Yponomeuta plumbella	Nem védett / indifferens
Yponomeuta rorrella	Nem védett / indifferens
Yponomeuta sedella	Nem védett / indifferens
Ypsolopha alpella	Nem védett / indifferens
Ypsolopha asperella	Nem védett / indifferens
Ypsolopha chazariella	Nem védett / indifferens
Ypsolopha dentella	Nem védett / indifferens
Ypsolopha falcella	Nem védett / indifferens
Ypsolopha horridella	Nem védett / indifferens
Ypsolopha lucella	Nem védett / indifferens
Ypsolopha mucronella	Nem védett / indifferens
Ypsolopha parenthesella	Nem védett / indifferens
Ypsolopha persicella	Nem védett / indifferens
Ypsolopha scabrella	Nem védett / indifferens
Ypsolopha sequella	Nem védett / indifferens
Ypsolopha sylvella	Nem védett / indifferens
Ypsolopha ustella	Nem védett / indifferens
Ypsolopha vittella	Nem védett / indifferens
Zabrus spinipes	Nem védett / indifferens
Zabrus tenebrioides	Nem védett / indifferens
Zacladus asperatus	Nem védett / indifferens
Zacladus exiguus	Nem védett / indifferens
Zacladus geranii	Nem védett / indifferens
Zanclognatha lunalis	Nem védett / indifferens
Zanclognatha tarsipennalis	Nem védett / indifferens
Zanclognatha zelleralis	Nem védett / indifferens
Zeadolopus latipes	Nem védett / indifferens
Zebrina detrita	Nem védett / indifferens
Zeiraphera griseana	Nem védett / indifferens
Zeiraphera isertana	Nem védett / indifferens
Zeiraphera rufimitrana	Nem védett / indifferens
Zelotes aeneus	Nem védett / indifferens
Zelotes apricorum	Nem védett / indifferens
Zelotes aurantiacus	Nem védett / indifferens
Zelotes caucasius	Nem védett / indifferens
Zelotes clivicola	Nem védett / indifferens
Zelotes electus	Nem védett / indifferens
Zelotes erebeus	Nem védett / indifferens
Zelotes gracilis	Nem védett / indifferens
Zelotes hermani	Nem védett / indifferens
Zelotes latreillei	Nem védett / indifferens
Zelotes longipes	Nem védett / indifferens
Zelotes mundus	Nem védett / indifferens
Zelotes petrensis	Nem védett / indifferens
Zelotes pygmaeus	Nem védett / indifferens
Zelotes segrex	Nem védett / indifferens
Zelotes subterraneus	Nem védett / indifferens
Zerynthia polyxena	Védett
Zeteotomus brevicornis	Nem védett / indifferens
Zeuzera pyrina	Nem védett / indifferens
Zicrona caerulea	Nem védett / indifferens
Zilla diodia	Nem védett / indifferens
Zilora obscura	Nem védett / indifferens
Zingel streber	Fokozottan védett
Zingel zingel	Fokozottan védett
Zodarion germanicum	Nem védett / indifferens
Zodarion rubidum	Nem védett / indifferens
Zonitis immaculata	Nem védett / indifferens
Zonitis nana	Nem védett / indifferens
Zonitis praeusta	Nem védett / indifferens
Zonitoides arboreus	Nem védett / indifferens
Zonitoides nitidus	Nem védett / indifferens
Zootoca vivipara	Fokozottan védett
Zootoca vivipara pannonica	Védett
Zophodia grossulariella	Nem védett / indifferens
Zora manicata	Nem védett / indifferens
Zora nemoralis	Nem védett / indifferens
Zora pardalis	Nem védett / indifferens
Zora silvestris	Nem védett / indifferens
Zora spinimana	Nem védett / indifferens
Zorochros demustoides	Nem védett / indifferens
Zorochros meridionalis	Nem védett / indifferens
Zorochros quadriguttatus	Nem védett / indifferens
Zuphium olens	Nem védett / indifferens
Zygaena angelicae	Nem védett / indifferens
Zygaena brizae	Nem védett / indifferens
Zygaena carniolica	Nem védett / indifferens
Zygaena cynarae	Nem védett / indifferens
Zygaena ephialtes	Nem védett / indifferens
Zygaena fausta	Védett
Zygaena filipendulae	Nem védett / indifferens
Zygaena laeta	Fokozottan védett
Zygaena lonicerae	Nem védett / indifferens
Zygaena loti	Nem védett / indifferens
Zygaena minos	Nem védett / indifferens
Zygaena osterodensis	Nem védett / indifferens
Zygaena punctum	Nem védett / indifferens
Zygaena purpuralis	Nem védett / indifferens
Zygaena viciae	Nem védett / indifferens
Zygiella atrica	Nem védett / indifferens
Zygiella x-notata	Nem védett / indifferens
Zygina angusta	Nem védett / indifferens
Zygina discolor	Nem védett / indifferens
Zygina dorsalis	Nem védett / indifferens
Zygina flammigera	Nem védett / indifferens
Zygina frauenfeldi	Nem védett / indifferens
Zygina hyperici	Nem védett / indifferens
Zygina lunaris	Nem védett / indifferens
Zygina nivea	Nem védett / indifferens
Zygina ordinaria	Nem védett / indifferens
Zygina rhamni	Nem védett / indifferens
Zygina tithide	Nem védett / indifferens
Zyginidia pullula	Nem védett / indifferens
Zyginidia scutellaris	Nem védett / indifferens
Zyras cognatus	Nem védett / indifferens
Zyras collaris	Nem védett / indifferens
Zyras fulgidus	Nem védett / indifferens
Zyras funestus	Nem védett / indifferens
Zyras hampei	Nem védett / indifferens
Zyras haworthi	Nem védett / indifferens
Zyras humeralis	Nem védett / indifferens
Zyras laticollis	Nem védett / indifferens
Zyras limbatus	Nem védett / indifferens
Zyras lugens	Nem védett / indifferens
Zyras ruficollis	Nem védett / indifferens
Zyras similis	Nem védett / indifferens
Trachemys scripta elegans	Inváziós
Trachemys scripta	Inváziós
Eresus moravicus	Védett
Aix galericulata	Inváziós
Rhysodes sulcatus	Védett
Omoglymmius germari	Védett
Bombina bombina × variegata	Védett
Anguis colchica	Védett
Cydalima perspectalis	Inváziós
Knipowitschia caucasica	Inváziós
Carassius auratus	Inváziós
Buprestis haemorrhoidalis	Nem védett / indifferens
Ctenophora guttata	Nem védett / indifferens
Halyomorpha halys	Inváziós
Leptoglossus occidentalis	Inváziós
Bythinella thermophila	Nem védett / jelentős
Gasterosteus gymnurus	Nem védett / indifferens
Rhyssa persuasoria	Védett
Pisum elatius	Védett
Clematis viticella	Nem védett / indifferens
Myosurus minimus	Nem védett / jelentős
Batrachium rhipiphyllum	Nem védett / jelentős
Batrachium trichophyllum	Nem védett / indifferens
Batrachium rionii	Nem védett / jelentős
Lathyrus pannonicus	Védett
Isopyrum thalictroides	Nem védett / indifferens
Actaea spicata	Nem védett / indifferens
Cimicifuga europaea	Fokozottan védett
Aquilegia vulgaris	Védett
Aquilegia vulgaris subsp. nigricans	Védett
Consolida regalis	Nem védett / indifferens
Consolida regalis subsp. paniculata	Nem védett / indifferens
Paeonia officinalis subsp. banatica	Fokozottan védett
Acer saccharinum	Nem védett / indifferens
Cerastium tomentosum	Nem védett / indifferens
Phytolacca esculenta	Inváziós
Lathyrus pisiformis	Fokozottan védett
Lathyrus pratensis	Nem védett / indifferens
Lathyrus aphaca	Nem védett / jelentős
Lathyrus nissolia	Védett
Lathyrus tuberosus	Nem védett / indifferens
Clematis integrifolia	Védett
Clematis alpina	Védett
Clematis flammula	Nem védett / indifferens
Portulaca grandiflora	Nem védett / indifferens
Thymelaea passerina	Nem védett / jelentős
Daphne laureola	Védett
Daphne mezereum	Védett
Daphne cneorum	Védett
Paeonia officinalis	Nem védett / indifferens
Caltha palustris	Nem védett / indifferens
Caltha palustris subsp. laeta	Nem védett / jelentős
Lathyrus latifolius	Nem védett / jelentős
Lathyrus hirsutus	Nem védett / indifferens
Lathyrus sphaericus	Nem védett / jelentős
Lathyrus sativus	Nem védett / indifferens
Rosa rugosa	Nem védett / indifferens
Padus serotina	Inváziós
Glycine soja	Nem védett / indifferens
Daphne cneorum subsp. arbusculoides	Védett
Hippophaë rhamnoides subsp. carpatica	Védett
Lathyrus sylvestris	Nem védett / jelentős
Pulsatilla pratensis subsp. zimmermannii	Védett
Hepatica nobilis	Védett
Anemone sylvestris	Védett
Anemone nemorosa	Nem védett / jelentős
Anemone ranunculoides	Nem védett / indifferens
Anemone trifolia	Védett
Clematis recta	Nem védett / jelentős
Clematis vitalba	Nem védett / indifferens
Caltha palustris subsp. cornuta	Nem védett / jelentős
Trollius europaeus	Védett
Consolida orientalis	Nem védett / indifferens
Aconitum anthora	Védett
Aconitum variegatum subsp. gracilis	Védett
Aconitum vulparia	Védett
Aconitum moldavicum	Védett
Lythrum salicaria	Nem védett / indifferens
Berberis vulgaris	Nem védett / indifferens
Mahonia aquifolium	Nem védett / indifferens
Pisum sativum	Nem védett / indifferens
Batrachium baudotii	Nem védett / jelentős
Trollius europaeus subsp. demissorum	Védett
Trollius europaeus subsp. tatrae	Védett
Phaseolus coccineus	Nem védett / indifferens
Ammannia verticillata	Nem védett / jelentős
Peplis portula	Nem védett / indifferens
Lythrum thesioides	Nem védett / jelentős
Lythrum linifolium	Védett
Lythrum hyssopifolia	Nem védett / indifferens
Lythrum tribracteatum	Védett
Lythrum virgatum	Nem védett / indifferens
Helleborus dumetorum	Védett
Helleborus odorus	Védett
Helleborus purpurascens	Védett
Helleborus viridis	Nem védett / jelentős
Eranthis hyemalis	Védett
Nigella arvensis	Nem védett / indifferens
Nigella damascena	Nem védett / indifferens
Lathyrus pannonicus subsp. collinus	Védett
Lathyrus pallescens	Fokozottan védett
Lathyrus palustris	Védett
Phaseolus vulgaris	Nem védett / indifferens
Elaeagnus angustifolia	Inváziós
Ceratocephalus testiculatus	Nem védett / jelentős
Batrachium aquatile	Nem védett / jelentős
Cerasus vulgaris	Nem védett / indifferens
Batrachium radians	Nem védett / jelentős
Lathyrus niger	Nem védett / indifferens
Lathyrus venetus	Védett
Lathyrus vernus	Nem védett / indifferens
Lathyrus linifolius var. montanus	Védett
Pulsatilla patens	Fokozottan védett
Pulsatilla grandis	Védett
Pulsatilla × mixta	Védett
Pulsatilla pratensis subsp. nigricans	Védett
Pulsatilla pratensis subsp. nigricans lus. rubra	Védett
Pulsatilla pratensis subsp. hungarica	Fokozottan védett
Batrachium circinatum	Nem védett / jelentős
Batrachium fluitans	Védett
Batrachium peltatum	Nem védett / indifferens
Rapistrum rugosum	Nem védett / indifferens
Ficaria verna	Nem védett / indifferens
Ficaria verna subsp. bulbifera	Nem védett / indifferens
Ficaria verna subsp. calthifolia	Nem védett / indifferens
Ranunculus polyphyllus	Védett
Ranunculus flammula	Nem védett / jelentős
Ranunculus lingua	Védett
Ranunculus sceleratus	Nem védett / indifferens
Ranunculus repens	Nem védett / indifferens
Ranunculus bulbosus	Nem védett / indifferens
Ranunculus sardous	Nem védett / indifferens
Ranunculus polyanthemos	Nem védett / indifferens
Ranunculus lanuginosus	Nem védett / indifferens
Ranunculus nemorosus	Védett
Ranunculus acris	Nem védett / indifferens
Ranunculus strigulosus	Védett
Ranunculus auricomus	Nem védett / indifferens
Ranunculus cassubicus	Nem védett / indifferens
Ranunculus fallax	Nem védett / indifferens
Ranunculus arvensis	Nem védett / jelentős
Ranunculus lateriflorus	Védett
Ranunculus pedatus	Nem védett / jelentős
Ranunculus illyricus	Védett
Ranunculus psilostachys	Védett
Ranunculus parviflorus	Nem védett / indifferens
Ranunculus cymbalaria	Nem védett / indifferens
Thalictrum aquilegiifolium	Védett
Thalictrum foetidum	Védett
Thalictrum minus	Nem védett / jelentős
Thalictrum minus subsp. pseudominus	Védett
Thalictrum simplex	Nem védett / indifferens
Sorbus × thaiszii	Védett
Sorbus × pseudodanubialis	Védett
Sorbus × ulmifolia	Védett
Sorbus × vajdae	Védett
Thalictrum simplex subsp. galioides	Nem védett / indifferens
Thalictrum flavum	Nem védett / jelentős
Thalictrum lucidum	Nem védett / indifferens
Adonis vernalis	Védett
Adonis × hybrida	Fokozottan védett
Adonis flammea	Nem védett / jelentős
Adonis aestivalis	Nem védett / indifferens
Nymphaea alba	Védett
Nuphar lutea	Nem védett / jelentős
Ceratophyllum submersum	Nem védett / jelentős
Ceratophyllum demersum	Nem védett / indifferens
Asarum europaeum	Nem védett / indifferens
Aristolochia clematitis	Nem védett / indifferens
Spiraea salicifolia	Védett
Spiraea crenata	Védett
Spiraea media	Védett
Aruncus sylvestris	Védett
Cotoneaster tomentosus	Védett
Cotoneaster integerrimus	Védett
Cotoneaster matrensis	Nem védett / jelentős
Cotoneaster niger	Védett
Cydonia oblonga	Nem védett / indifferens
Pyrus communis	Nem védett / indifferens
Pyrus pyraster	Nem védett / indifferens
Pyrus nivalis	Védett
Pyrus nivalis subsp. salviifolia	Védett
Pyrus × austriaca	Nem védett / jelentős
Pyrus magyarica	Fokozottan védett
Pyrus × mecsekensis	Nem védett / jelentős
Pyrus × karpatiana	Fokozottan védett
Malus sylvestris	Nem védett / indifferens
Malus sylvestris subsp. dasyphylla	Nem védett / indifferens
Malus domestica	Nem védett / indifferens
Sorbus domestica	Védett
Sorbus aucuparia	Nem védett / indifferens
Sorbus aria	Védett
Sorbus aria agg.	Védett
Sorbus graeca	Védett
Sorbus austriaca subsp. hazslinszkyana	Védett
Sorbus × danubialis	Védett
Sorbus torminalis	Nem védett / indifferens
Sorbus × pseudosemiincisa	Védett
Sorbus × pseudobakonyensis	Védett
Sorbus × pseudolatifolia	Védett
Sorbus × simonkaiana	Védett
Sorbus × vertesensis	Védett
Sorbus × semiincisa	Védett
Sorbus × pseudovertesensis	Védett
Sorbus × redliana	Védett
Sorbus × latissima	Védett
Sorbus × balatonica	Védett
Sorbus × barthae	Védett
Sorbus × borosiana	Védett
Sorbus × adamii	Védett
Sorbus × andreanszkyana	Védett
Sorbus × bakonyensis	Védett
Sorbus × gayeriana	Védett
Rubus pyramidalis	Nem védett / indifferens
Sorbus × gerecseensis	Védett
Sorbus × karpatii	Védett
Sorbus × decipientiformis	Védett
Sorbus × degenii	Védett
Sorbus × eugenii-kelleri	Védett
Amelanchier ovalis	Védett
Mespilus germanica	Nem védett / indifferens
Crataegus oxyacantha	Nem védett / indifferens
Crataegus monogyna	Nem védett / indifferens
Crataegus monogyna subsp. nordica	Nem védett / jelentős
Crataegus curvisepala subsp. calycina	Nem védett / jelentős
Crataegus nigra	Fokozottan védett
Rubus saxatilis	Védett
Rubus idaeus	Nem védett / indifferens
Rubus caesius	Nem védett / indifferens
Rubus canescens	Nem védett / indifferens
Rubus nessensis	Nem védett / indifferens
Rubus fruticosus agg.	Nem védett / indifferens
Rubus sulcatus	Nem védett / indifferens
Rubus plicatus	Nem védett / indifferens
Rubus divaricatus	Nem védett / indifferens
Rubus rhombifolius	Nem védett / indifferens
Rubus silesiacus	Nem védett / indifferens
Rubus macrophyllus	Nem védett / indifferens
Rubus sylvaticus	Nem védett / indifferens
Rubus rhamnifolius	Nem védett / indifferens
Rubus bifrons	Nem védett / indifferens
Rubus discolor	Nem védett / indifferens
Rubus candicans	Nem védett / indifferens
Rubus vestitus	Nem védett / indifferens
Rubus radula	Nem védett / indifferens
Rubus thyrsiflorus	Nem védett / indifferens
Rubus rudis	Nem védett / indifferens
Rubus koehleri	Nem védett / indifferens
Rubus scaber	Nem védett / indifferens
Rubus schleicheri	Nem védett / indifferens
Rubus bellardii	Nem védett / indifferens
Rubus serpens	Nem védett / indifferens
Rubus hirtus	Nem védett / indifferens
Fragaria vesca	Nem védett / indifferens
Fragaria moschata	Nem védett / jelentős
Fragaria viridis	Nem védett / indifferens
Comarum palustre	Fokozottan védett
Potentilla rupestris	Védett
Potentilla alba	Nem védett / jelentős
Potentilla micrantha	Nem védett / indifferens
Potentilla supina	Nem védett / jelentős
Potentilla anserina	Nem védett / indifferens
Potentilla erecta	Nem védett / indifferens
Potentilla reptans	Nem védett / indifferens
Potentilla argentea	Nem védett / indifferens
Potentilla argentea subsp. grandiceps	Nem védett / indifferens
Potentilla argentea subsp. tenuiloba	Nem védett / indifferens
Potentilla argentea subsp. demissa	Nem védett / indifferens
Potentilla argentea subsp. decumbens	Nem védett / indifferens
Potentilla argentea subsp. macrotoma	Nem védett / indifferens
Potentilla impolita	Nem védett / indifferens
Potentilla collina	Nem védett / indifferens
Potentilla thyrsiflora	Nem védett / jelentős
Pisum sativum subsp. arvense	Nem védett / indifferens
Potentilla wiemanniana var. javorkae	Nem védett / jelentős
Potentilla leucopolitana	Nem védett / indifferens
Potentilla inclinata	Nem védett / indifferens
Potentilla recta	Nem védett / indifferens
Potentilla recta subsp. tuberosa	Nem védett / indifferens
Potentilla recta subsp. laciniosa	Nem védett / indifferens
Potentilla recta subsp. pilosa	Nem védett / indifferens
Potentilla recta subsp. crassa	Nem védett / indifferens
Potentilla recta subsp. obscura	Nem védett / indifferens
Potentilla recta subsp. leucotricha	Nem védett / indifferens
Potentilla pedata	Nem védett / jelentős
Potentilla heptaphylla	Nem védett / indifferens
Potentilla patula	Védett
Potentilla arenaria	Nem védett / indifferens
Potentilla neumanniana	Nem védett / jelentős
Potentilla pusilla	Nem védett / jelentős
Waldsteinia geoides	Nem védett / jelentős
Geum urbanum	Nem védett / indifferens
Geum aleppicum	Védett
Geum rivale	Védett
Filipendula ulmaria	Nem védett / indifferens
Filipendula vulgaris	Nem védett / indifferens
Agrimonia eupatoria	Nem védett / indifferens
Agrimonia procera	Nem védett / jelentős
Aremonia agrimonioides	Védett
Sanguisorba officinalis	Nem védett / jelentős
Sanguisorba minor	Nem védett / indifferens
Sanguisorba minor subsp. muricata	Nem védett / indifferens
Aphanes arvensis	Nem védett / jelentős
Aphanes microcarpa	Nem védett / jelentős
Alchemilla glaucescens	Védett
Alchemilla hungarica	Védett
Alchemilla glabra	Védett
Alchemilla xanthochlora	Védett
Alchemilla gracilis	Védett
Alchemilla acutiloba	Védett
Alchemilla crinita	Védett
Alchemilla subcrenata	Védett
Alchemilla monticola	Védett
Alchemilla filicaulis	Védett
Rosa pendulina	Védett
Rosa spinosissima	Nem védett / jelentős
Rosa gallica	Nem védett / jelentős
Rosa arvensis	Nem védett / jelentős
Rosa stylosa	Nem védett / jelentős
Rosa × reversa	Védett
Rosa livescens	Nem védett / jelentős
Rosa sancti-andreae	Védett
Rosa tomentosa	Nem védett / jelentős
Rosa micrantha	Nem védett / jelentős
Rosa hungarica	Nem védett / jelentős
Rosa polyacantha	Nem védett / jelentős
Rosa rubiginosa	Nem védett / indifferens
Rosa agrestis	Nem védett / jelentős
Rosa inodora	Nem védett / indifferens
Rosa albiflora	Nem védett / indifferens
Rosa gizellae	Nem védett / jelentős
Rosa elliptica	Nem védett / jelentős
Rosa zalana	Nem védett / jelentős
Rosa zagrabiensis	Nem védett / jelentős
Rosa szaboi	Nem védett / jelentős
Rosa obtusifolia	Nem védett / jelentős
Rosa canina	Nem védett / indifferens
Rosa corymbifera	Nem védett / indifferens
Rosa deseglisei	Nem védett / indifferens
Rosa dumalis	Nem védett / indifferens
Rosa subcanina	Nem védett / indifferens
Rosa caesia	Nem védett / jelentős
Rosa subcollina	Nem védett / indifferens
Rosa glauca	Nem védett / indifferens
Rosa sherardii	Nem védett / indifferens
Rosa kmetiana	Nem védett / jelentős
Rosa scarbriuscula	Nem védett / jelentős
Padus avium	Nem védett / jelentős
Cerasus mahaleb	Nem védett / indifferens
Cerasus fruticosa	Nem védett / jelentős
Cerasus avium	Nem védett / indifferens
Cerasus × mohacsyana	Nem védett / jelentős
Amygdalus communis	Nem védett / indifferens
Amygdalus nana	Védett
Prunus cerasifera	Nem védett / indifferens
Prunus domestica	Nem védett / indifferens
Prunus domestica subsp. insititia	Nem védett / indifferens
Prunus spinosa	Nem védett / indifferens
Prunus spinosa subsp. fruticans	Nem védett / indifferens
Prunus spinosa subsp. dasyphylla	Nem védett / indifferens
Sedum caespitosum	Védett
Sedum spurium	Nem védett / indifferens
Sedum maximum	Nem védett / indifferens
Sedum hispanicum	Védett
Sedum album	Nem védett / indifferens
Sorbus × zolyomii	Védett
Sorbus × huljakii	Védett
Sedum reflexum	Nem védett / indifferens
Sedum reflexum subsp. glaucum	Nem védett / indifferens
Sedum acre	Nem védett / indifferens
Sedum neglectum subsp. sopianae	Védett
Sedum sexangulare	Nem védett / indifferens
Sedum sartorianum subsp. hillebrandtii	Védett
Sempervivum tectorum	Védett
Sempervivum marmoreum	Védett
Jovibarba hirta	Védett
Jovibarba sobolifera	Védett
Saxifraga paniculata	Védett
Saxifraga bulbifera	Nem védett / jelentős
Saxifraga granulata	Nem védett / jelentős
Saxifraga tridactylites	Nem védett / indifferens
Saxifraga adscendens	Védett
Chrysosplenium alternifolium	Nem védett / indifferens
Parnassia palustris	Védett
Philadelphus coronarius	Nem védett / indifferens
Ribes uva-crispa	Nem védett / indifferens
Ribes uva-crispa subsp. reclinatum	Nem védett / indifferens
Ribes uva-crispa subsp. grossularia	Nem védett / indifferens
Ribes alpinum	Védett
Ribes nigrum	Védett
Ribes petraeum	Védett
Ribes rubrum	Nem védett / indifferens
Ribes rubrum subsp. rubrum	Nem védett / indifferens
Ribes rubrum subsp. sylvestre	Nem védett / jelentős
Ribes aureum	Inváziós
Cercis siliquastrum	Nem védett / indifferens
Gleditsia triacanthos	Nem védett / indifferens
Lupinus polyphyllus	Nem védett / indifferens
Lupinus albus	Nem védett / indifferens
Lupinus angustifolius	Nem védett / indifferens
Lupinus luteus	Nem védett / indifferens
Genista germanica	Nem védett / jelentős
Genista pilosa	Nem védett / indifferens
Genista tinctoria	Nem védett / indifferens
Genista tinctoria subsp. elatior	Nem védett / indifferens
Genista ovata subsp. nervata	Nem védett / indifferens
Chamaespartium sagittale	Nem védett / jelentős
Laburnum anagyroides	Nem védett / jelentős
Sarothamnus scoparius	Nem védett / jelentős
Cytisus procumbens	Nem védett / jelentős
Lembotropis nigricans	Nem védett / indifferens
Chamaecytisus supinus	Nem védett / jelentős
Chamaecytisus supinus subsp. aggregatus	Nem védett / indifferens
Chamaecytisus supinus subsp. pseudorochelii	Védett
Salvia verbenacea	Nem védett / indifferens
Chamaecytisus supinus subsp. pannonicus	Nem védett / indifferens
Chamaecytisus albus	Védett
Chamaecytisus austriacus	Nem védett / jelentős
Chamaecytisus heuffelii	Védett
Chamaecytisus ratisbonensis	Nem védett / indifferens
Chamaecytisus ciliatus	Védett
Chamaecytisus hirsutus subsp. leucotrichus	Nem védett / jelentős
Ononis pusilla	Nem védett / jelentős
Ononis spinosa	Nem védett / indifferens
Ononis spinosa subsp. austriaca	Nem védett / indifferens
Ononis arvensis	Nem védett / indifferens
Ononis spinosiformis	Nem védett / jelentős
Ononis spinosiformis subsp. semihircina	Nem védett / jelentős
Trigonella procumbens	Nem védett / jelentős
Trigonella caerulea	Nem védett / indifferens
Trigonella foenum-graecum	Nem védett / indifferens
Trigonella gladiata	Védett
Trigonella monspeliaca	Nem védett / jelentős
Medicago lupulina	Nem védett / indifferens
Medicago sativa	Nem védett / indifferens
Medicago falcata	Nem védett / indifferens
Medicago × varia	Nem védett / indifferens
Medicago prostrata	Nem védett / jelentős
Medicago minima	Nem védett / indifferens
Medicago arabica	Nem védett / jelentős
Medicago orbicularis	Védett
Medicago rigidula	Védett
Medicago nigra	Nem védett / indifferens
Melilotus dentatus	Nem védett / jelentős
Melilotus altissimus subsp. macrorrhizus	Nem védett / jelentős
Melilotus officinalis	Nem védett / indifferens
Melilotus albus	Nem védett / indifferens
Trifolium campestre	Nem védett / indifferens
Trifolium aureum	Nem védett / indifferens
Trifolium patens	Nem védett / indifferens
Trifolium dubium	Nem védett / indifferens
Trifolium micranthum	Nem védett / jelentős
Trifolium montanum	Nem védett / indifferens
Trifolium hybridum	Nem védett / indifferens
Trifolium hybridum subsp. elegans	Nem védett / indifferens
Trifolium repens	Nem védett / indifferens
Trifolium strictum	Nem védett / jelentős
Trifolium retusum	Nem védett / jelentős
Trifolium angulatum	Nem védett / jelentős
Trifolium fragiferum	Nem védett / indifferens
Trifolium fragiferum subsp. bonannii	Nem védett / indifferens
Trifolium resupinatum	Nem védett / jelentős
Trifolium ornithopodioides	Védett
Trifolium vesiculosum	Védett
Trifolium diffusum	Nem védett / jelentős
Trifolium medium	Nem védett / indifferens
Trifolium medium subsp. sarosiense	Nem védett / indifferens
Trifolium medium subsp. banaticum	Nem védett / indifferens
Trifolium alpestre	Nem védett / indifferens
Trifolium rubens	Nem védett / jelentős
Trifolium ochroleucon	Nem védett / jelentős
Trifolium pannonicum	Nem védett / jelentős
Trifolium incarnatum	Nem védett / indifferens
Trifolium subterraneum var. brachycladum	Védett
Trifolium pratense	Nem védett / indifferens
Trifolium pratense subsp. expansum	Nem védett / indifferens
Trifolium pratense subsp. sativum	Nem védett / indifferens
Trifolium pallidum	Nem védett / jelentős
Trifolium arvense	Nem védett / indifferens
Trifolium striatum	Nem védett / jelentős
Anthyllis vulneraria	Nem védett / indifferens
Anthyllis vulneraria subsp. polyphylla	Nem védett / indifferens
Anthyllis vulneraria subsp. alpestris	Nem védett / jelentős
Anthyllis vulneraria subsp. carpatica	Nem védett / jelentős
Dorycnium germanicum	Nem védett / indifferens
Dorycnium herbaceum	Nem védett / jelentős
Tetragonolobus maritimus subsp. siliquosus	Nem védett / indifferens
Lotus angustissimus	Nem védett / jelentős
Lotus uliginosus	Nem védett / indifferens
Lotus borbasii	Védett
Lotus corniculatus	Nem védett / indifferens
Lotus tenuis	Nem védett / indifferens
Amorpha fruticosa	Inváziós
Galega officinalis	Nem védett / indifferens
Robinia pseudo-acacia	Inváziós
Colutea arborescens	Nem védett / jelentős
Astragalus contortuplicatus	Védett
Astragalus sulcatus	Védett
Astragalus glycyphyllos	Nem védett / indifferens
Astrantia major	Védett
Astragalus dasyanthus	Fokozottan védett
Astragalus exscapus	Védett
Astragalus cicer	Nem védett / indifferens
Astragalus asper	Védett
Astragalus austriacus	Nem védett / jelentős
Astragalus onobrychis	Nem védett / indifferens
Astragalus varius	Védett
Astragalus vesicarius subsp. albidus	Védett
Oxytropis pilosa subsp. hungarica	Védett
Glycyrrhiza echinata	Nem védett / indifferens
Glycyrrhiza glabra	Nem védett / indifferens
Vitis vulpina	Inváziós
Vitis vinifera	Nem védett / indifferens
Parthenocissus tricuspidata	Inváziós
Parthenocissus quinquefolia	Inváziós
Parthenocissus inserta	Inváziós
Hedera helix	Nem védett / indifferens
Cornus mas	Nem védett / indifferens
Cornus sanguinea	Nem védett / indifferens
Cornus sanguinea subsp. hungarica	Nem védett / indifferens
Hydrocotyle vulgaris	Védett
Sanicula europaea	Nem védett / indifferens
Eryngium campestre	Nem védett / indifferens
Eryngium planum	Nem védett / jelentős
Physocaulis nodosus	Nem védett / jelentős
Chaerophyllum aromaticum	Nem védett / indifferens
Chaerophyllum hirsutum	Védett
Chaerophyllum aureum	Védett
Chaerophyllum temulum	Nem védett / indifferens
Chaerophyllum bulbosum	Nem védett / indifferens
Anthriscus caucalis	Nem védett / indifferens
Anthriscus cerefolium subsp. trichosperma	Nem védett / indifferens
Anthriscus sylvestris	Nem védett / indifferens
Anthriscus nitida	Védett
Scandix pecten-veneris	Nem védett / jelentős
Torilis arvensis	Nem védett / indifferens
Torilis japonica	Nem védett / indifferens
Torilis ucranica	Védett
Caucalis latifolia	Nem védett / jelentős
Caucalis platycarpos	Nem védett / indifferens
Vaccinium microcarpum	Nem védett / jelentős
Centaurium erythraea subsp. austriacum	Nem védett / indifferens
Galium rubioides	Nem védett / jelentős
Caucalis platycarpos subsp. muricata	Nem védett / jelentős
Orlaya grandiflora	Nem védett / jelentős
Coriandrum sativum	Nem védett / indifferens
Bifora radians	Nem védett / jelentős
Smyrnium perfoliatum	Nem védett / jelentős
Physospermum cornubiense	Védett
Conium maculatum	Nem védett / indifferens
Pleurospermum austriacum	Védett
Bupleurum rotundifolium	Nem védett / jelentős
Bupleurum longifolium	Védett
Bupleurum falcatum	Nem védett / indifferens
Bupleurum falcatum subsp. dilatatum	Nem védett / indifferens
Bupleurum praealtum	Nem védett / jelentős
Peucedanum cervaria	Nem védett / indifferens
Peucedanum oreoselinum	Nem védett / jelentős
Peucedanum alsaticum	Nem védett / indifferens
Peucedanum carvifolia	Nem védett / jelentős
Peucedanum officinale	Védett
Peucedanum arenarium	Védett
Pastinaca sativa subsp. pratensis	Nem védett / indifferens
Heracleum sphondylium	Nem védett / indifferens
Heracleum sphondylium subsp. trachycarpum	Nem védett / indifferens
Heracleum sphondylium subsp. flavescens	Nem védett / indifferens
Heracleum sphondylium subsp. chlorathum	Nem védett / indifferens
Heracleum mantegazzianum	Inváziós
Tordylium maximum	Nem védett / jelentős
Laser trilobum	Nem védett / jelentős
Laserpitium prutenicum	Nem védett / jelentős
Laserpitium latifolium	Nem védett / jelentős
Daucus carota	Nem védett / indifferens
Sherardia arvensis	Nem védett / jelentős
Asperula taurina subsp. leucanthera	Védett
Asperula arvensis	Nem védett / jelentős
Asperula orientalis	Nem védett / jelentős
Asperula tinctoria	Nem védett / indifferens
Asperula cynanchica	Nem védett / indifferens
Asperula cynanchica subsp. montana	Nem védett / indifferens
Cruciata pedemontana	Nem védett / jelentős
Cruciata glabra	Nem védett / indifferens
Cruciata laevipes	Nem védett / indifferens
Galium boreale	Nem védett / jelentős
Galium boreale subsp. pseudorubioides	Nem védett / indifferens
Galium rotundifolium	Nem védett / indifferens
Galium odoratum	Nem védett / indifferens
Galium rivale	Nem védett / jelentős
Galium aparine	Nem védett / indifferens
Galium spurium	Nem védett / indifferens
Coronilla emerus	Védett
Coronilla varia	Nem védett / indifferens
Coronilla elegans	Védett
Coronilla coronata	Védett
Coronilla vaginalis	Védett
Hippocrepis comosa	Nem védett / jelentős
Onobrychis viciifolia	Nem védett / indifferens
Onobrychis arenaria	Nem védett / indifferens
Vicia hirsuta	Nem védett / indifferens
Vicia tetrasperma	Nem védett / indifferens
Vicia tenuissima	Nem védett / indifferens
Vicia articulata	Nem védett / indifferens
Vicia ervilia	Nem védett / indifferens
Vicia pisiformis	Nem védett / jelentős
Vicia dumetorum	Nem védett / indifferens
Vicia sparsiflora	Védett
Vicia cassubica	Nem védett / indifferens
Vicia sylvatica	Védett
Aethusa cynapium	Nem védett / indifferens
Vicia biennis	Fokozottan védett
Vicia villosa	Nem védett / indifferens
Vicia villosa subsp. pseudovillosa	Nem védett / indifferens
Vicia cracca	Nem védett / indifferens
Vicia tenuifolia	Nem védett / indifferens
Vicia tenuifolia subsp. stenophylla	Nem védett / indifferens
Vicia oroboides	Védett
Vicia sepium	Nem védett / indifferens
Vicia lathyroides	Nem védett / indifferens
Vicia grandiflora	Nem védett / indifferens
Vicia lutea	Nem védett / jelentős
Vicia peregrina	Nem védett / indifferens
Vicia sativa	Nem védett / indifferens
Vicia sativa subsp. cordata	Nem védett / indifferens
Vicia angustifolia	Nem védett / indifferens
Vicia angustifolia subsp. segetalis	Nem védett / indifferens
Vicia pannonica	Nem védett / indifferens
Vicia pannonica subsp. striata	Nem védett / indifferens
Vicia narbonensis subsp. serratifolia	Védett
Vicia faba	Nem védett / indifferens
Lens culinaris	Nem védett / indifferens
Lathyrus transsilvanicus	Védett
Lythrum × scrabrum	Nem védett / indifferens
Ludwigia palustris	Védett
Epilobium hirsutum	Nem védett / indifferens
Epilobium parviflorum	Nem védett / indifferens
Epilobium lanceolatum	Nem védett / jelentős
Epilobium montanum	Nem védett / indifferens
Epilobium collinum	Nem védett / jelentős
Epilobium roseum	Nem védett / indifferens
Epilobium palustre	Védett
Epilobium obscurum	Nem védett / jelentős
Epilobium tetragonum	Nem védett / indifferens
Epilobium tetragonum subsp. lamyi	Nem védett / indifferens
Epilobium adenocaulon	Inváziós
Epilobium angustifolium	Nem védett / indifferens
Epilobium dodonaei	Védett
Oenothera biennis	Inváziós
Oenothera suaveolens	Nem védett / indifferens
Oenothera rubricaulis	Nem védett / indifferens
Oenothera hoelscheri	Nem védett / indifferens
Oenothera erythrosepala	Nem védett / indifferens
Oenothera salicifolia	Nem védett / indifferens
Oenothera syrticola	Nem védett / indifferens
Circaea lutetiana	Nem védett / indifferens
Circaea alpina	Védett
Circaea × intermedia	Védett
Trapa natans	Védett
Myriophyllum verticillatum	Nem védett / jelentős
Myriophyllum spicatum	Nem védett / indifferens
Hippuris vulgaris	Védett
Dictamnus albus	Védett
Ailanthus altissima	Inváziós
Polygala major	Védett
Polygala comosa	Nem védett / indifferens
Polygala vulgaris	Nem védett / indifferens
Polygala vulgaris subsp. oxyptera	Nem védett / indifferens
Polygala nicaeensis subsp. carniolica	Nem védett / jelentős
Polygala amara	Nem védett / jelentős
Polygala amara subsp. brachyptera	Nem védett / jelentős
Polygala amarella	Védett
Polygala amarella subsp. austriaca	Védett
Cotinus coggygria	Nem védett / jelentős
Rhus hirta	Inváziós
Acer tataricum	Nem védett / indifferens
Urtica dioica	Nem védett / indifferens
Acer pseudo-platanus	Nem védett / indifferens
Acer platanoides	Nem védett / indifferens
Acer campestre	Nem védett / indifferens
Acer negundo	Inváziós
Aesculus hippocastanum	Nem védett / indifferens
Impatiens noli-tangere	Nem védett / indifferens
Impatiens parviflora	Inváziós
Impatiens glandulifera	Inváziós
Impatiens balfouri	Inváziós
Ilex aquifolium	Nem védett / indifferens
Euonymus verrucosus	Nem védett / indifferens
Euonymus europaea	Nem védett / indifferens
Staphylea pinnata	Nem védett / indifferens
Rhamnus catharticus	Nem védett / indifferens
Rhamnus saxatilis	Védett
Frangula alnus	Nem védett / indifferens
Vitis sylvestris	Védett
Vitis rupestris	Inváziós
Bupleurum pachnospermum	Védett
Bupleurum affine	Nem védett / indifferens
Bupleurum tenuissimum	Nem védett / indifferens
Trinia glauca	Nem védett / jelentős
Trinia ramosissima	Védett
Apium graveolens	Nem védett / indifferens
Apium repens	Védett
Cicuta virosa	Védett
Falcaria vulgaris	Nem védett / indifferens
Carum carvi	Nem védett / indifferens
Pimpinella major	Nem védett / jelentős
Pimpinella saxifraga	Nem védett / indifferens
Pimpinella saxifraga subsp. nigra	Nem védett / indifferens
Aegopodium podagraria	Nem védett / indifferens
Berula erecta	Nem védett / jelentős
Sium latifolium	Nem védett / indifferens
Sium sisaroideum	Védett
Seseli hippomarathrum	Nem védett / jelentős
Seseli annuum	Nem védett / indifferens
Seseli leucospermum	Fokozottan védett
Seseli varium	Nem védett / jelentős
Seseli osseum	Nem védett / indifferens
Libanotis pyrenaica	Nem védett / indifferens
Libanotis pyrenaica subsp. intermedia	Nem védett / indifferens
Oenanthe aquatica	Nem védett / indifferens
Oenanthe fistulosa	Nem védett / jelentős
Oenanthe silaifolia	Nem védett / jelentős
Oenanthe banatica	Nem védett / jelentős
Aethusa cynapium subsp. agrestis	Nem védett / indifferens
Aethusa cynapium subsp. cynapioides	Nem védett / indifferens
Foeniculum vulgare	Nem védett / indifferens
Anethum graveolens var. hortorum	Nem védett / indifferens
Silaum silaus	Nem védett / jelentős
Silaum peucedanoides	Védett
Cnidium dubium	Védett
Selinum carvifolia	Nem védett / jelentős
Angelica palustris	Fokozottan védett
Angelica sylvestris	Nem védett / indifferens
Angelica archangelica	Nem védett / indifferens
Ferula sadleriana	Fokozottan védett
Peucedanum verticillare	Védett
Peucedanum palustre	Védett
Galium spurium subsp. infestum	Nem védett / indifferens
Galium tricornutum	Nem védett / jelentős
Galium tenuissimum	Védett
Galium parisiense	Nem védett / jelentős
Galium parisiense subsp. anglicum	Nem védett / indifferens
Galium divaricatum	Nem védett / jelentős
Salix triandra	Nem védett / indifferens
Galium uliginosum	Nem védett / indifferens
Galium palustre	Nem védett / indifferens
Galium elongatum	Nem védett / jelentős
Galium sylvaticum	Nem védett / indifferens
Galium schultesii	Nem védett / indifferens
Galium glaucum	Nem védett / indifferens
Galium glaucum subsp. hirsutum	Nem védett / indifferens
Galium verum	Nem védett / indifferens
Galium verum subsp. wirtgenii	Nem védett / indifferens
Galium abaujense	Nem védett / jelentős
Galium mollugo	Nem védett / indifferens
Galium album	Nem védett / indifferens
Galium album subsp. picnotrichum	Nem védett / indifferens
Galium lucidum	Nem védett / indifferens
Galium pumilum	Nem védett / jelentős
Galium austriacum	Védett
Rubia tinctorum	Nem védett / indifferens
Sambucus ebulus	Nem védett / indifferens
Sambucus nigra	Nem védett / indifferens
Campanula patula subsp. neglecta	Nem védett / indifferens
Sambucus racemosa	Nem védett / jelentős
Viburnum opulus	Nem védett / indifferens
Viburnum lantana	Nem védett / indifferens
Symphoricarpos rivularis	Nem védett / indifferens
Lonicera caprifolium	Védett
Lonicera xylosteum	Nem védett / indifferens
Lonicera nigra	Védett
Adoxa moschatellina	Nem védett / jelentős
Valerianella coronata	Nem védett / jelentős
Valerianella dentata	Nem védett / indifferens
Valerianella rimosa	Nem védett / indifferens
Valerianella pumila	Nem védett / jelentős
Valerianella locusta	Nem védett / indifferens
Fraxinus pennsylvanica	Inváziós
Syringa vulgaris	Inváziós
Scabiosa columbaria subsp. pseudobanatica	Nem védett / jelentős
Scabiosa triandra subsp. agrestis	Nem védett / indifferens
Tilia tomentosa	Nem védett / indifferens
Tilia platyphyllos	Nem védett / indifferens
Tilia platyphyllos subsp. cordifolia	Nem védett / indifferens
Tilia platyphyllos subsp. pseudorubra	Nem védett / indifferens
Tilia rubra	Nem védett / indifferens
Tilia cordata	Nem védett / indifferens
Abutilon theophrasti	Nem védett / indifferens
Lavatera thuringiaca	Nem védett / indifferens
Lavathera trimestris	Nem védett / indifferens
Althaea hirsuta	Nem védett / jelentős
Althaea cannabina	Nem védett / jelentős
Althaea officinalis	Nem védett / indifferens
Althaea officinalis subsp. pseudoarmeniaca	Nem védett / indifferens
Ligustrum vulgare	Nem védett / indifferens
Centaurium littorale subsp. uliginosum	Nem védett / jelentős
Centaurium erythraea	Nem védett / indifferens
Centaurium erythraea subsp. turcicum	Nem védett / indifferens
Centaurium pulchellum	Nem védett / indifferens
Blackstonia acuminata	Védett
Gentianella ciliata	Védett
Gentiana cruciata	Védett
Gentiana asclepiadea	Védett
Gentiana pneumonanthe	Védett
Valerianella carinata	Nem védett / indifferens
Valeriana excelsa	Védett
Valeriana stolonifera	Nem védett / indifferens
Valeriana officinalis	Nem védett / indifferens
Valeriana dioica	Nem védett / jelentős
Valeriana simplicifolia	Védett
Valeriana tripteris subsp. austriaca	Védett
Aster amellus	Védett
Aster tripolium subsp. pannonicus	Nem védett / indifferens
Aster novae-angliae	Nem védett / indifferens
Aster novi-belgii	Inváziós
Aster × versicolor	Nem védett / indifferens
Aster × salignus	Inváziós
Aster × lanceolatus	Inváziós
Aster tradescantii	Inváziós
Aster bellidiastrum	Nem védett / indifferens
Stenactis annua	Inváziós
Crepis rhoeadifolia	Nem védett / indifferens
Crepis tectorum	Nem védett / indifferens
Crepis biennis	Nem védett / indifferens
Crepis pannonica	Fokozottan védett
Crepis nicaeensis	Védett
Crepis capillaris	Nem védett / jelentős
Crepis polymorpha	Nem védett / jelentős
Crepis setosa	Nem védett / indifferens
Cyclamen purpurascens	Védett
Armeria elongata	Védett
Gagea szovitsii	Védett
Limonium gmelini subsp. hungaricum	Nem védett / indifferens
Rumex acetosella	Nem védett / indifferens
Rumex acetosella subsp. tenuifolius	Nem védett / indifferens
Rumex acetosa	Nem védett / indifferens
Rumex thyrsiflorus	Nem védett / indifferens
Rumex scutatus	Nem védett / indifferens
Rumex pseudonatronatus	Védett
Rumex aquaticus	Nem védett / jelentős
Rumex confertus	Nem védett / jelentős
Rumex patientia	Nem védett / indifferens
Rumex patientia subsp. recurvatus	Nem védett / indifferens
Rumex patientia subsp. orientalis	Nem védett / indifferens
Rumex kerneri	Nem védett / indifferens
Rumex crispus	Nem védett / indifferens
Rumex stenophyllus	Nem védett / indifferens
Rumex conglomeratus	Nem védett / indifferens
Rumex sanguineus	Nem védett / indifferens
Rumex hydrolapathum	Nem védett / indifferens
Rumex obtusifolius	Nem védett / indifferens
Hibiscus trionum	Nem védett / indifferens
Rumex obtusifolius subsp. obtusifolius	Nem védett / indifferens
Rumex obtusifolius subsp. sylvestris	Nem védett / indifferens
Rumex obtusifolius subsp. transiens	Nem védett / indifferens
Rumex obtusifolius subsp. subalpinus	Nem védett / indifferens
Rumex pulcher	Nem védett / indifferens
Rumex dentatus subsp. halacsyi	Nem védett / indifferens
Rumex palustris	Nem védett / indifferens
Rumex maritimus	Nem védett / jelentős
Polygonum bistorta	Védett
Polygonum amphibium	Nem védett / indifferens
Polygonum lapathifolium	Nem védett / indifferens
Polygonum lapathifolium subsp. danubiale	Nem védett / indifferens
Polygonum lapathifolium subsp. incanum	Nem védett / indifferens
Polygonum persicaria	Nem védett / indifferens
Polygonum hydropiper	Nem védett / indifferens
Dipsacus laciniatus	Nem védett / indifferens
Dipsacus sylvestris	Nem védett / indifferens
Dipsacus × pseudosylvester	Nem védett / indifferens
Cephalaria pilosa	Nem védett / jelentős
Cephalaria transsylvanica	Nem védett / jelentős
Succisa pratensis	Nem védett / jelentős
Succisella inflexa	Nem védett / jelentős
Knautia arvensis	Nem védett / indifferens
Knautia arvensis subsp. pannonica	Nem védett / indifferens
Knautia arvensis subsp. rosea	Nem védett / indifferens
Knautia kitaibelii subsp. tomentella	Fokozottan védett
Knautia dipsacifolia	Védett
Knautia drymeia	Nem védett / indifferens
Knautia × ramosissima	Nem védett / indifferens
Scabiosa canescens	Védett
Scabiosa ochroleuca	Nem védett / indifferens
Scabiosa columbaria	Nem védett / indifferens
Alcea biennis	Nem védett / jelentős
Alcea rosea	Nem védett / indifferens
Malva alcea	Nem védett / indifferens
Malva sylvestris	Nem védett / indifferens
Malva neglecta	Nem védett / indifferens
Malva pusilla	Nem védett / indifferens
Malva moschata	Nem védett / indifferens
Malva crispa	Nem védett / indifferens
Nonea lutea	Nem védett / indifferens
Radiola linoides	Védett
Linum catharticum	Nem védett / indifferens
Linum flavum	Védett
Linum dolomiticum	Fokozottan védett
Linum trigynum	Védett
Linum hirsutum	Védett
Linum hirsutum subsp. glabrescens	Védett
Linum tenuifolium	Védett
Linum austriacum	Nem védett / jelentős
Linum perenne	Nem védett / indifferens
Linum usitatissimum	Nem védett / indifferens
Oxalis acetosella	Nem védett / indifferens
Oxalis fontana	Inváziós
Oxalis corniculata	Inváziós
Oxalis dillenii	Inváziós
Geranium phaeum	Nem védett / indifferens
Geranium robertianum	Nem védett / indifferens
Geranium lucidum	Nem védett / jelentős
Geranium bohemicum	Nem védett / jelentős
Geranium divaricatum	Nem védett / indifferens
Geranium sibiricum	Nem védett / indifferens
Geranium molle	Nem védett / indifferens
Geranium molle subsp. stripulare	Nem védett / indifferens
Geranium columbinum	Nem védett / indifferens
Geranium dissectum	Nem védett / indifferens
Geranium rotundifolium	Nem védett / indifferens
Geranium pusillum	Nem védett / indifferens
Geranium pyrenaicum	Nem védett / indifferens
Geranium palustre	Nem védett / jelentős
Geranium sanguineum	Nem védett / indifferens
Geranium sylvaticum	Védett
Geranium pratense	Nem védett / jelentős
Erodium neilreichii	Nem védett / jelentős
Erodium ciconium	Nem védett / indifferens
Erodium cicutarium	Nem védett / indifferens
Tribulus terrestris	Nem védett / indifferens
Tribulus terrestris subsp. orientalis	Nem védett / indifferens
Mercurialis annua	Nem védett / jelentős
Mercurialis perennis	Nem védett / indifferens
Mercurialis ovata	Nem védett / indifferens
Mercurialis × paxii	Nem védett / indifferens
Ricinus communis	Nem védett / indifferens
Euphorbia nutans	Nem védett / indifferens
Euphorbia humifusa	Nem védett / indifferens
Euphorbia maculata	Nem védett / indifferens
Euphorbia palustris	Nem védett / jelentős
Euphorbia villosa	Nem védett / jelentős
Euphorbia epithymoides	Nem védett / indifferens
Euphorbia dulcis	Nem védett / indifferens
Euphorbia dulcis subsp. incompta	Nem védett / indifferens
Euphorbia angulata	Nem védett / jelentős
Euphorbia verrucosa	Nem védett / jelentős
Euphorbia platyphyllos	Nem védett / indifferens
Euphorbia platyphyllos subsp. literata	Nem védett / indifferens
Euphorbia stricta	Nem védett / jelentős
Euphorbia helioscopia	Nem védett / indifferens
Euphorbia amygdaloides	Nem védett / indifferens
Euphorbia salicifolia	Nem védett / indifferens
Euphorbia cyparissias	Nem védett / indifferens
Euphorbia esula	Nem védett / indifferens
Euphorbia esula subsp. pinifolia	Nem védett / indifferens
Euphorbia lucida	Nem védett / indifferens
Euphorbia virgata	Nem védett / jelentős
Euphorbia seguierana	Nem védett / indifferens
Euphorbia pannonica	Nem védett / jelentős
Euphorbia falcata	Nem védett / indifferens
Euphorbia falcata subsp. acuminata	Nem védett / indifferens
Euphorbia peplus	Nem védett / indifferens
Euphorbia taurinensis	Nem védett / indifferens
Euphorbia exigua	Nem védett / indifferens
Euphorbia segetalis	Nem védett / indifferens
Callitriche palustris	Nem védett / indifferens
Callitriche cophocarpa	Nem védett / indifferens
Fraxinus ornus	Nem védett / indifferens
Fraxinus excelsior	Nem védett / indifferens
Fraxinus angustifolia subsp. pannonica	Nem védett / indifferens
Gentianella austriaca	Védett
Gentianella livonica	Védett
Prunella × spuria	Védett
Menyanthes trifoliata	Védett
Nymphoides peltata	Védett
Asclepias syriaca	Inváziós
Vincetoxicum hirundinaria	Nem védett / indifferens
Vincetoxicum pannonicum	Fokozottan védett
Vinca minor	Nem védett / indifferens
Vinca herbacea	Védett
Vinca major	Nem védett / indifferens
Cuscuta lupuliformis	Nem védett / jelentős
Cuscuta europaea	Nem védett / indifferens
Cuscuta epithymum subsp. kotschyi	Nem védett / indifferens
Cuscuta trifolii	Nem védett / indifferens
Cuscuta epilinum	Nem védett / indifferens
Cuscuta campestris	Inváziós
Cuscuta australis	Nem védett / indifferens
Cuscuta australis subsp. tinei	Nem védett / indifferens
Cuscuta australis subsp. cesatiana	Nem védett / indifferens
Convolvulus arvensis	Nem védett / indifferens
Convolvulus cantabrica	Védett
Calystegia sepium	Nem védett / indifferens
Ipomoea purpurea	Nem védett / indifferens
Phacelia tanacetifolia	Nem védett / indifferens
Phacelia congesta	Nem védett / indifferens
Heliotropium europaeum	Nem védett / indifferens
Heliotropium supinum	Védett
Omphalodes scorpioides	Védett
Omphalodes verna	Nem védett / indifferens
Cynoglossum officinale	Nem védett / indifferens
Cynoglossum hungaricum	Nem védett / indifferens
Lappula patula	Nem védett / jelentős
Lappula squarrosa	Nem védett / indifferens
Ajuga reptans	Nem védett / indifferens
Lappula heteracantha	Nem védett / jelentős
Lappula deflexa	Nem védett / indifferens
Asperugo procumbens	Nem védett / jelentős
Veronica chamaedrys	Nem védett / indifferens
Orobanche cernua	Nem védett / jelentős
Symphytum tuberosum subsp. angustifolium	Nem védett / indifferens
Symphytum officinale	Nem védett / indifferens
Symphytum officinale subsp. uliginosum	Nem védett / indifferens
Symphytum officinale subsp. bohemicum	Nem védett / indifferens
Borago officinalis	Nem védett / indifferens
Anchusa barrelieri	Védett
Anchusa officinalis	Nem védett / indifferens
Anchusa ochroleuca subsp. pustulata	Fokozottan védett
Anchusa azurea	Nem védett / jelentős
Lycopsis arvensis	Nem védett / jelentős
Nonea pulla	Nem védett / indifferens
Alkanna tinctoria	Védett
Pulmonaria angustifolia	Védett
Pulmonaria officinalis	Nem védett / indifferens
Pulmonaria obscura	Nem védett / indifferens
Pulmonaria mollis	Nem védett / indifferens
Myosotis sparsiflora	Nem védett / indifferens
Myosotis caespitosa	Védett
Myosotis palustris	Nem védett / indifferens
Myosotis nemorosa	Nem védett / indifferens
Myosotis sylvatica	Nem védett / indifferens
Myosotis stenophylla	Védett
Myosotis arvensis	Nem védett / indifferens
Myosotis ramosissima	Nem védett / indifferens
Myosotis stricta	Nem védett / indifferens
Myosotis discolor	Nem védett / jelentős
Lithospermum officinale	Nem védett / jelentős
Lithospermum purpureo-coeruleum	Nem védett / indifferens
Lithospermum arvense	Nem védett / indifferens
Veronica beccabunga	Nem védett / indifferens
Veronica scutellata	Nem védett / indifferens
Veronica montana	Nem védett / jelentős
Veronica officinalis	Nem védett / indifferens
Veronica chamaedrys subsp. vindobonensis	Nem védett / indifferens
Veronica prostrata	Nem védett / indifferens
Veronica austriaca	Nem védett / indifferens
Veronica austriaca subsp. dentata	Nem védett / indifferens
Veronica teucrium	Nem védett / indifferens
Veronica asutriaca subsp. bihariense	Nem védett / indifferens
Veronica paniculata	Fokozottan védett
Veronica longifolia	Védett
Veronica pallens	Védett
Veronica spicata	Nem védett / indifferens
Veronica spicata subsp. nitens	Nem védett / indifferens
Veronica orchidea	Nem védett / indifferens
Veronica serpyllifolia	Nem védett / indifferens
Solidago graminifolia	Nem védett / indifferens
Xanthium albinum subsp. riparium	Nem védett / indifferens
Veronica verna	Nem védett / indifferens
Veronica dillenii	Nem védett / indifferens
Veronica triphyllos	Nem védett / indifferens
Veronica praecox	Nem védett / indifferens
Veronica arvensis	Nem védett / indifferens
Veronica acinifolia	Nem védett / jelentős
Veronica agrestis	Nem védett / jelentős
Veronica polita	Nem védett / indifferens
Veronica opaca	Nem védett / indifferens
Euphrasia rostkoviana	Nem védett / indifferens
Euphrasia kerneri	Nem védett / indifferens
Xanthium saccharatum	Inváziós
Euphrasia stricta	Nem védett / indifferens
Euphrasia tatarica	Nem védett / indifferens
Orthantha lutea	Nem védett / indifferens
Odontites vernus	Nem védett / indifferens
Odontites vulgaris	Nem védett / indifferens
Rhinanthus minor	Nem védett / indifferens
Veronica persica	Inváziós
Veronica hederifolia	Nem védett / indifferens
Veronica hederifolia subsp. triloba	Nem védett / indifferens
Veronica hederifolia subsp. lucorum	Nem védett / indifferens
Veronica peregrina	Nem védett / indifferens
Veronica filiformis	Nem védett / indifferens
Digitalis grandiflora	Nem védett / indifferens
Digitalis lanata	Fokozottan védett
Digitalis ferruginea	Fokozottan védett
Digitalis purpurea	Nem védett / indifferens
Melampyrum cristatum	Nem védett / indifferens
Melampyrum arvense	Nem védett / jelentős
Melampyrum barbatum	Nem védett / indifferens
Melampyrum nemorosum	Nem védett / indifferens
Lactuca quercina subsp. sagittata	Nem védett / indifferens
Luzula divulgata	Nem védett / jelentős
Melampyrum bihariense prol. romanicum	Védett
Melampyrum pratense	Nem védett / indifferens
Lithospermum arvense subsp. coerulescens	Nem védett / indifferens
Onosma visianii	Védett
Onosma arenarium	Védett
Festuca javorkae	Nem védett / indifferens
Onosma arenarium subsp. tuberculatum	Védett
Onosma tornense	Fokozottan védett
Cerinthe minor	Nem védett / indifferens
Echium italicum	Nem védett / jelentős
Echium russicum	Védett
Echium vulgare	Nem védett / indifferens
Verbena officinalis	Nem védett / indifferens
Verbena supina	Védett
Ajuga chamaepitys	Nem védett / jelentős
Ajuga chamaepitys subsp. ciliata	Nem védett / jelentős
Ajuga laxmannii	Védett
Ajuga genevensis	Nem védett / indifferens
Teucrium scorodonia	Védett
Teucrium montanum	Nem védett / indifferens
Teucrium montanum subsp. subvillosum	Nem védett / jelentős
Teucrium botrys	Nem védett / jelentős
Teucrium chamaedrys	Nem védett / indifferens
Teucrium scordium	Nem védett / jelentős
Lavandula angustifolia	Nem védett / indifferens
Scutellaria hastifolia	Nem védett / indifferens
Scutellaria galericulata	Nem védett / indifferens
Scutellaria columnae	Védett
Scutellaria altissima	Nem védett / indifferens
Marrubium vulgare	Nem védett / indifferens
Marrubium peregrinum	Nem védett / indifferens
Marrubium × paniculatum	Nem védett / indifferens
Sideritis montana	Nem védett / jelentős
Nepeta pannonica	Nem védett / jelentős
Nepeta cataria	Nem védett / jelentős
Nepeta parviflora	Fokozottan védett
Glechoma hederacea	Nem védett / indifferens
Glechoma hirsuta	Nem védett / indifferens
Dracocephalum austriacum	Fokozottan védett
Dracocephalum ruyschiana	Fokozottan védett
Dracocephalum moldavicum	Nem védett / indifferens
Prunella grandiflora	Védett
Prunella vulgaris	Nem védett / indifferens
Prunella laciniata	Nem védett / indifferens
Festuca vojtkoi	Nem védett / jelentős
Poa stiriaca	Nem védett / indifferens
Prunella × intermedia	Nem védett / jelentős
Prunella × bicolor	Védett
Melittis melissophyllum	Nem védett / jelentős
Melittis carpatica	Nem védett / indifferens
Phlomis tuberosa	Védett
Galeopsis ladanum	Nem védett / jelentős
Bromus carinatus	Nem védett / indifferens
Galeopsis ladanum subsp. angustifolia	Nem védett / jelentős
Galeopsis speciosa	Nem védett / indifferens
Galeopsis pubescens	Nem védett / indifferens
Galeopsis tetrahit	Nem védett / indifferens
Galeopsis bifida	Nem védett / indifferens
Galeopsis segetum	Nem védett / indifferens
Galeobdolon luteum	Nem védett / indifferens
Galeobdolon argentatum	Nem védett / indifferens
Galeobdolon luteum subsp. montanum	Nem védett / indifferens
Lamium orvala	Védett
Lamium amplexicaule	Nem védett / indifferens
Lamium purpureum	Nem védett / indifferens
Lamium album	Nem védett / indifferens
Lamium maculatum	Nem védett / indifferens
Leonurus cardiaca	Nem védett / indifferens
Leonurus cardiaca subsp. villosus	Nem védett / indifferens
Leonurus marrubiastrum	Nem védett / indifferens
Ballota nigra	Nem védett / indifferens
Betonica officinalis	Nem védett / indifferens
Stachys annua	Nem védett / indifferens
Stachys recta	Nem védett / indifferens
Stachys sylvatica	Nem védett / indifferens
Stachys palustris	Nem védett / indifferens
Stachys alpina	Védett
Stachys germanica	Nem védett / jelentős
Stachys byzantina	Nem védett / indifferens
Salvia glutinosa	Nem védett / indifferens
Salvia verticillata	Nem védett / indifferens
Salvia aethiops	Nem védett / jelentős
Salvia austriaca	Nem védett / indifferens
Salvia nutans	Fokozottan védett
Salvia nemorosa	Nem védett / indifferens
Salvia pratensis	Nem védett / indifferens
Salvia sclarea	Nem védett / indifferens
Salvia officinalis	Nem védett / indifferens
Melissa officinalis	Nem védett / indifferens
Satureja hortensis	Nem védett / indifferens
Clinopodium vulgare	Nem védett / indifferens
Acinos arvensis	Nem védett / indifferens
Calamintha menthifolia subsp. sylvatica	Nem védett / jelentős
Calamintha einseleana	Nem védett / indifferens
Calamintha thymifolia	Fokozottan védett
Hyssopus officinalis	Nem védett / indifferens
Origanum vulgare	Nem védett / indifferens
Origanum vulgare subsp. prismaticum	Nem védett / indifferens
Thymus pannonicus	Nem védett / indifferens
Thymus glabrescens	Nem védett / indifferens
Thymus glabrescens subsp. decipiens	Nem védett / indifferens
Thymus praecox	Nem védett / indifferens
Thymus serpyllum	Nem védett / indifferens
Thymus pulegioides	Nem védett / indifferens
Bromus lanceolatus	Nem védett / indifferens
Thymus pulegioides subsp. montanus	Nem védett / indifferens
Thymus vulgaris	Nem védett / indifferens
Lycopus europaeus	Nem védett / indifferens
Lycopus europaeus subsp. mollis	Nem védett / indifferens
Anthoxanthum aristatum	Nem védett / indifferens
Scolochloa festucacea	Védett
Lycopus exaltatus	Nem védett / indifferens
Lycopus × intercendens	Nem védett / indifferens
Mentha pulegium	Nem védett / indifferens
Mentha longifolia	Nem védett / indifferens
Mentha × dumetorum	Nem védett / indifferens
Mentha × dalmatica	Nem védett / indifferens
Mentha × carinthiaca	Nem védett / indifferens
Eragrostis multicaulis	Nem védett / indifferens
Mentha aquatica	Nem védett / indifferens
Mentha arvensis	Nem védett / indifferens
Mentha × gentilis	Nem védett / indifferens
Mentha × verticillata	Nem védett / indifferens
Nicandra physalodes	Nem védett / indifferens
Lycium barbarum	Nem védett / indifferens
Lycium chinense	Inváziós
Atropa bella-donna	Nem védett / indifferens
Scopolia carniolica	Védett
Hyoscyamus niger	Nem védett / indifferens
Physalis alkekengi	Nem védett / indifferens
Solanum dulcamara	Nem védett / indifferens
Solanum nigrum	Nem védett / indifferens
Solanum nigrum subsp. schultesii	Nem védett / indifferens
Solanum alatum	Nem védett / indifferens
Solanum luteum	Nem védett / indifferens
Solanum rostratum	Nem védett / indifferens
Datura stramonium	Nem védett / indifferens
Nicotiana rustica	Nem védett / indifferens
Eragrostis mexicana	Nem védett / indifferens
Nicotiana tabacum	Nem védett / indifferens
Verbascum phoeniceum	Nem védett / indifferens
Verbascum blattaria	Nem védett / indifferens
Verbascum thapsus	Nem védett / indifferens
Verbascum densiflorum	Nem védett / indifferens
Verbascum phlomoides	Nem védett / indifferens
Verbascum lychnitis	Nem védett / indifferens
Verbascum speciosum	Nem védett / indifferens
Verbascum pulverulentum	Nem védett / indifferens
Verbascum austriacum	Nem védett / indifferens
Verbascum nigrum	Nem védett / indifferens
Cymbalaria muralis	Nem védett / indifferens
Kickxia spuria	Nem védett / indifferens
Kickxia elatine	Nem védett / indifferens
Kickxia × confinus	Nem védett / indifferens
Linaria arvensis	Nem védett / jelentős
Linaria genistifolia	Nem védett / indifferens
Linaria vulgaris	Nem védett / indifferens
Linaria angustissima	Nem védett / jelentős
Linaria × kocianovichii	Védett
Misopates orontium	Nem védett / jelentős
Antirrhinum majus	Nem védett / indifferens
Chaenorhinum minus	Nem védett / jelentős
Scrophularia vernalis	Védett
Scrophularia scopolii	Védett
Scrophularia nodosa	Nem védett / indifferens
Scrophularia umbrosa	Nem védett / indifferens
Scrophularia umbrosa subsp. neesii	Nem védett / indifferens
Gratiola officinalis	Nem védett / indifferens
Limosella aquatica	Nem védett / indifferens
Lindernia procumbens	Védett
Veronica anagallis-aquatica	Nem védett / indifferens
Veronica anagalloides	Nem védett / jelentős
Veronica catenata	Nem védett / jelentős
Veronica scardica	Nem védett / jelentős
Rhinanthus serotinus	Nem védett / indifferens
Panicum dichotomiflorum	Inváziós
Lemna minuta	Inváziós
Lemna turionifera	Inváziós
Lemna aequinoctalis	Inváziós
Rhinanthus borbasii	Nem védett / indifferens
Carex demissa	Nem védett / jelentős
Rhinanthus rumelicus	Nem védett / indifferens
Rhinanthus wagneri	Nem védett / indifferens
Rhinanthus alectorolophus	Nem védett / indifferens
Pedicularis palustris	Védett
Lathraea squamaria	Nem védett / indifferens
Globularia cordifolia	Védett
Globularia punctata	Nem védett / jelentős
Orobanche ramosa	Nem védett / indifferens
Orobanche nana	Védett
Orobanche caesia	Védett
Orobanche arenaria	Nem védett / jelentős
Orobanche purpurea	Nem védett / jelentős
Orobanche coerulescens subsp. occidentalis	Védett
Orobanche cernua subsp. cumana	Nem védett / indifferens
Orobanche alba	Nem védett / indifferens
Orobanche reticulata	Nem védett / jelentős
Orobanche loricata	Védett
Orobanche picridis	Nem védett / jelentős
Cardamine amara	Védett
Cardamine pratensis	Nem védett / indifferens
Cardamine pratensis subsp. dentata	Nem védett / indifferens
Cardamine pratensis subsp. paludosus	Nem védett / indifferens
Cardamine pratensis subsp. matthioli	Nem védett / indifferens
Cardamine trifolia	Védett
Dentaria bulbifera	Nem védett / indifferens
Dentaria enneaphyllos	Nem védett / indifferens
Dentaria glandulosa	Védett
Dentaria trifolia	Védett
Barbarea stricta	Nem védett / indifferens
Barbarea vulgaris	Nem védett / indifferens
Orobanche minor	Nem védett / indifferens
Orobanche hederae	Védett
Orobanche caryophyllacea	Nem védett / indifferens
Orobanche teucrii	Nem védett / jelentős
Orobanche lutea	Nem védett / indifferens
Orobanche elatior	Nem védett / indifferens
Orobanche alsatica	Nem védett / jelentős
Orobanche flava	Védett
Orobanche gracilis	Nem védett / indifferens
Pinguicula vulgaris	Fokozottan védett
Pinguicula alpina	Nem védett / jelentős
Utricularia vulgaris	Nem védett / jelentős
Utricularia australis	Nem védett / jelentős
Utricularia minor	Védett
Utricularia bremii	Fokozottan védett
Plantago arenaria	Nem védett / jelentős
Plantago tenuiflora	Nem védett / indifferens
Plantago maritima	Nem védett / indifferens
Plantago schwarzenbergiana	Védett
Plantago argentea	Védett
Plantago lanceolata	Nem védett / indifferens
Plantago lanceolata subsp. dubia	Nem védett / indifferens
Plantago lanceolata subsp. sphaerostachya	Nem védett / indifferens
Plantago altissima	Nem védett / jelentős
Plantago maxima	Fokozottan védett
Plantago media	Nem védett / indifferens
Plantago major	Nem védett / indifferens
Plantago major subsp. intermedia	Nem védett / indifferens
Plantago major subsp. winteri	Nem védett / indifferens
Chelidonium majus	Nem védett / indifferens
Glaucium flavum	Nem védett / jelentős
Glaucium corniculatum	Nem védett / jelentős
Papaver argemone	Nem védett / jelentős
Papaver hybridum	Nem védett / jelentős
Papaver dubium	Nem védett / indifferens
Papaver rhoeas	Nem védett / indifferens
Papaver somniferum	Nem védett / indifferens
Corydalis cava	Nem védett / indifferens
Corydalis solida	Nem védett / indifferens
Corydalis solida subsp. slivenensis	Nem védett / indifferens
Corydalis intermedia	Védett
Corydalis pumila	Nem védett / indifferens
Corydalis × campylochila	Védett
Corydalis lutea	Nem védett / indifferens
Fumaria rostellata	Nem védett / indifferens
Fumaria officinalis	Nem védett / indifferens
Fumaria schleicheri	Nem védett / indifferens
Fumaria vaillantii	Nem védett / indifferens
Fumaria parviflora	Nem védett / indifferens
Brassica elongata	Nem védett / jelentős
Brassica elongata subsp. armoracioides	Nem védett / jelentős
Brassica × juncea	Nem védett / indifferens
Brassica nigra	Nem védett / indifferens
Brassica rapa subsp. sylvestris	Nem védett / indifferens
Brassica × napus	Nem védett / indifferens
Brassica oleracea	Nem védett / indifferens
Erucastrum gallicum	Nem védett / jelentős
Erucastrum nasturtiifolium	Nem védett / indifferens
Sinapis arvensis	Nem védett / indifferens
Sinapis alba	Nem védett / indifferens
Sinapis alba subsp. dissecta	Nem védett / indifferens
Eruca sativa	Nem védett / indifferens
Diplotaxis muralis	Nem védett / indifferens
Diplotaxis tenuifolia	Nem védett / indifferens
Diplotaxis erucoides	Nem védett / indifferens
Raphanus raphanistrum	Nem védett / indifferens
Raphanus sativus	Nem védett / indifferens
Calepina irregularis	Nem védett / jelentős
Crambe tataria	Fokozottan védett
Rapistrum perenne	Nem védett / jelentős
Conringia orientalis	Nem védett / jelentős
Conringia austriaca	Védett
Lepidium campestre	Nem védett / indifferens
Lepidium perfoliatum	Nem védett / indifferens
Lepidium crasssifolium	Nem védett / indifferens
Lepidium ruderale	Nem védett / indifferens
Lepidium graminifolium	Nem védett / indifferens
Lepidium densiflorum	Nem védett / indifferens
Lepidium virginicum	Nem védett / indifferens
Cardaria draba	Nem védett / indifferens
Coronopus squamatus	Nem védett / jelentős
Coronopus didymus	Nem védett / indifferens
Isatis tinctoria	Védett
Isatis tinctoria subsp. praecox	Védett
Biscutella laevigata	Nem védett / jelentős
Biscutella laevigata subsp. kerneri	Nem védett / jelentős
Biscutella laevigata subsp. hungarica	Nem védett / jelentős
Biscutella laevigata subsp. austriaca	Nem védett / jelentős
Biscutella laevigata subsp. illyrica	Nem védett / jelentős
Aethionema saxatile	Védett
Thlaspi arvense	Nem védett / indifferens
Thlaspi alliaceum	Védett
Sorbus × budaiana	Védett
Thlaspi perfoliatum	Nem védett / indifferens
Tulipa sylvestris	Nem védett / indifferens
Thlaspi coerulescens	Védett
Thlaspi montanum	Védett
Thlaspi goesingense	Védett
Thlaspi jankae	Védett
Thlaspi kovatsii subsp. schudichii	Védett
Teesdalia nudicaulis	Védett
Capsella bursa-pastoris	Nem védett / indifferens
Capsella rubella	Nem védett / indifferens
Hornungia petraea	Nem védett / jelentős
Myagrum perfoliatum	Nem védett / indifferens
Neslea paniculata	Nem védett / jelentős
Bunias orientalis	Nem védett / indifferens
Bunias erucago	Nem védett / indifferens
Euclidium syriacum	Nem védett / indifferens
Lunaria rediviva	Védett
Lunaria annua	Védett
Lunaria annua subsp. pachyrrhiza	Védett
Peltaria perennis	Nem védett / jelentős
Alyssum montanum	Nem védett / jelentős
Alyssum montanum subsp. brymii	Nem védett / jelentős
Alyssum montanum subsp. gmelinii	Nem védett / jelentős
Alyssum tortuosum	Nem védett / indifferens
Alyssum tortuosum subsp. tortuosum	Nem védett / indifferens
Alyssum tortuosum subsp. heterophyllum	Nem védett / indifferens
Alyssum alyssoides	Nem védett / indifferens
Alyssum desertorum	Nem védett / indifferens
Alyssum saxatile	Védett
Berteroa incana	Nem védett / indifferens
Draba lasiocarpa	Védett
Draba muralis	Nem védett / indifferens
Sorbus × tobani	Védett
Draba nemorosa	Nem védett / indifferens
Erophila praecox	Nem védett / indifferens
Erophila spathulata	Nem védett / indifferens
Erophila verna	Nem védett / indifferens
Sorbus × majeri	Védett
Sorbus × dracofolius	Védett
Sorbus × bodajkensis	Védett
Sorbus × vallerubusensis	Védett
Sorbus × veszpremensis	Védett
Sorbus × acutiserratus	Védett
Turritis glabra	Nem védett / indifferens
Filipendula ulmaria subsp. denudata	Nem védett / indifferens
Tilia rubra ssp. caucasica	Nem védett / indifferens
Armoracia lapathifolia	Nem védett / indifferens
Armoracia macrocarpa	Fokozottan védett
Cardamine impatiens	Nem védett / indifferens
Cardamine hirsuta	Nem védett / jelentős
Cardamine flexuosa	Nem védett / indifferens
Cardamine parviflora	Nem védett / jelentős
Barbarea verna	Nem védett / indifferens
Cardaminopsis petraea	Védett
Cardaminopsis arenosa	Nem védett / indifferens
Cardaminopsis arenosa subsp. petrogena	Nem védett / indifferens
Cardaminopsis arenosa subsp. borbasii	Nem védett / indifferens
Arabis turrita	Nem védett / indifferens
Arabis alpina	Védett
Arabis auriculata	Nem védett / indifferens
Arabis hirsuta	Nem védett / indifferens
Arabis hirsuta subsp. sagittata	Nem védett / indifferens
Arabis hirsuta subsp. gerardii	Nem védett / indifferens
Nasturtium officinale	Védett
Rorippa palustris	Nem védett / indifferens
Rorippa austriaca	Nem védett / indifferens
Rorippa amphibia	Nem védett / indifferens
Rorippa sylvestris	Nem védett / indifferens
Rorippa sylvestris subsp. sylvestris	Nem védett / indifferens
Rorippa sylvestris subsp. kerneri	Nem védett / indifferens
Rorippa × anceps	Nem védett / indifferens
Rorippa × astylis	Nem védett / indifferens
Rorippa × armoracioides	Nem védett / indifferens
Rorippa × hungarica	Nem védett / indifferens
Matthiola incana	Nem védett / indifferens
Matthiola longipetala subsp. bicornis	Nem védett / indifferens
Malcolmia africana	Nem védett / indifferens
Chorispora tenella	Nem védett / indifferens
Hesperis tristis	Nem védett / jelentős
Hesperis matronalis	Védett
Hesperis matronalis subsp. spontanea	Védett
Hesperis matronalis subsp. candida	Védett
Camelina sativa	Nem védett / indifferens
Hesperis matronalis subsp. vrabelyiana	Fokozottan védett
Hesperis sylvestris	Védett
Erysimum cheiranthoides	Nem védett / indifferens
Erysimum repandum	Nem védett / indifferens
Erysimum crepidifolium	Védett
Erysimum hieracifolium	Nem védett / jelentős
Erysimum odoratum	Védett
Erysimum odoratum subsp. buekkense	Védett
Erysimum pallidiflorum	Fokozottan védett
Erysimum diffusum	Nem védett / indifferens
Syrenia cana	Nem védett / indifferens
Alliaria petiolata	Nem védett / indifferens
Descurainia sophia	Nem védett / indifferens
Sisymbrium officinale	Nem védett / indifferens
Sisymbrium strictissimum	Nem védett / indifferens
Sisymbrium polymorphum	Védett
Sisymbrium loeselii	Nem védett / indifferens
Sisymbrium altissimum	Nem védett / indifferens
Sisymbrium orientale	Nem védett / indifferens
Arabidopsis thaliana	Nem védett / indifferens
Camelina microcarpa	Nem védett / jelentős
Camelina microcarpa subsp. pilosa	Nem védett / indifferens
Camelina alyssum	Nem védett / jelentős
Camelina rumelica	Nem védett / indifferens
Reseda luteola	Nem védett / jelentős
Reseda lutea	Nem védett / indifferens
Reseda inodora	Védett
Reseda phyteuma	Nem védett / jelentős
Drosera rotundifolia	Védett
Drosera anglica	Nem védett / jelentős
Aldrovanda vesiculosa	Védett
Myricaria germanica	Védett
Tamarix tetrandra	Nem védett / indifferens
Tamarix gallica	Nem védett / indifferens
Tamarix ramosissima	Nem védett / indifferens
Helianthemum canum	Nem védett / jelentős
Helianthemum nummularium	Nem védett / indifferens
Helianthemum ovatum	Nem védett / indifferens
Fumana procumbens	Nem védett / jelentős
Viola biflora	Védett
Viola odorata	Nem védett / indifferens
Viola suavis	Nem védett / indifferens
Viola alba	Nem védett / indifferens
Viola collina	Védett
Viola ambigua	Nem védett / jelentős
Viola hirta	Nem védett / indifferens
Viola mirabilis	Nem védett / indifferens
Viola rupestris	Nem védett / jelentős
Viola sylvestris	Nem védett / indifferens
Viola riviniana	Nem védett / indifferens
Viola canina	Nem védett / indifferens
Viola montana	Nem védett / indifferens
Viola stagnina	Védett
Viola pumila	Nem védett / indifferens
Viola elatior	Nem védett / indifferens
Viola elatior subsp. jordanii	Nem védett / jelentős
Viola tricolor	Nem védett / indifferens
Viola tricolor subsp. subalpina	Nem védett / jelentős
Viola tricolor subsp. polychroma	Nem védett / jelentős
Viola arvensis	Nem védett / indifferens
Viola kitaibeliana	Nem védett / indifferens
Elatine alsinastrum	Védett
Elatine triandra	Védett
Elatine hungarica	Védett
Elatine hydropiper	Védett
Elatine hexandra	Védett
Thladiantha dubia	Nem védett / indifferens
Bryonia alba	Nem védett / indifferens
Bryonia dioica	Nem védett / jelentős
Ecballium elaterium	Nem védett / indifferens
Cucurbita pepo	Nem védett / indifferens
Sicyos angulatus	Nem védett / indifferens
Echinocystis lobata	Inváziós
Hypericum humifusum	Nem védett / jelentős
Hypericum tetrapterum	Nem védett / jelentős
Hypericum perforatum	Nem védett / indifferens
Hypericum maculatum	Védett
Hypericum maculatum subsp. obtusiusculum	Nem védett / jelentős
Hypericum hirsutum	Nem védett / indifferens
Campanula latifolia	Fokozottan védett
Campanula rapunculoides	Nem védett / indifferens
Campanula trachelium	Nem védett / indifferens
Campanula bononiensis	Nem védett / indifferens
Campanula rotundifolia	Nem védett / indifferens
Campanula moravica	Nem védett / indifferens
Campanula xylocarpa	Nem védett / indifferens
Campanula persicifolia	Nem védett / indifferens
Campanula rapunculus	Nem védett / jelentős
Campanula patula	Nem védett / indifferens
Adenophora liliifolia	Fokozottan védett
Legousia speculum-veneris	Nem védett / jelentős
Asyneuma canescens	Védett
Asyneuma canescens subsp. salicifolium	Védett
Phyteuma spicatum	Védett
Phyteuma orbiculare	Védett
Jasione montana	Nem védett / jelentős
Eupatorium cannabinum	Nem védett / indifferens
Solidago virgaurea	Nem védett / indifferens
Solidago canadensis	Inváziós
Solidago gigantea subsp. serotina	Inváziós
Bellis perennis	Nem védett / indifferens
Aster linosyris	Nem védett / indifferens
Aster oleifolius	Fokozottan védett
Aster sedifolius	Védett
Aster sedifolius subsp. canus	Védett
Stenactis annua subsp. strigosa	Inváziós
Erigeron canadensis	Inváziós
Erigeron acris	Nem védett / indifferens
Xeranthemum annuum	Nem védett / indifferens
Xeranthemum cylindraceum	Nem védett / jelentős
Carlina acaulis	Védett
Carlina vulgaris	Nem védett / indifferens
Carlina vulgaris subsp. intermedia	Nem védett / indifferens
Carlina vulgaris subsp. longifolia	Nem védett / indifferens
Arctium tomentosum	Nem védett / indifferens
Arctium lappa	Nem védett / indifferens
Arctium minus	Nem védett / indifferens
Arctium nemorosum	Nem védett / indifferens
Arctium nemorosum subsp. pubens	Nem védett / indifferens
Jurinea mollis	Védett
Jurinea mollis subsp. macrocalathia	Védett
Hypericum barbatum	Fokozottan védett
Hypericum elegans	Védett
Hypericum montanum	Nem védett / indifferens
Hypericum × desetangsii	Védett
Chimaphila umbellata	Védett
Moneses uniflora	Védett
Orthilia secunda	Védett
Pyrola rotundifolia	Védett
Pyrola chlorantha	Védett
Pyrola minor	Védett
Pyrola media	Védett
Monotropa hypopitys	Nem védett / jelentős
Monotropa hypopitys subsp. hypophegea	Nem védett / jelentős
Calluna vulgaris	Nem védett / indifferens
Andromeda polifolia	Nem védett / jelentős
Vaccinium oxycoccos	Védett
Vaccinium vitis-idaea	Védett
Vaccinium myrtillus	Nem védett / jelentős
Campanula glomerata	Nem védett / indifferens
Campanula glomerata subsp. farinosa	Nem védett / indifferens
Campanula glomerata subsp. elliptica	Nem védett / indifferens
Campanula macrostachya	Fokozottan védett
Campanula cervicaria	Nem védett / indifferens
Campanula sibirica	Nem védett / indifferens
Campanula sibirica subsp. divergentiformis	Nem védett / jelentős
Erigeron acris subsp. angulosus	Nem védett / indifferens
Erigeron acris subsp. macrophyllus	Nem védett / jelentős
Micropus erectus	Nem védett / jelentős
Filago vulgaris	Nem védett / indifferens
Filago vulgaris subsp. lutescens	Nem védett / jelentős
Filago arvensis	Nem védett / indifferens
Filago minima	Nem védett / jelentős
Antennaria dioica	Nem védett / jelentős
Gnaphalium sylvaticum	Nem védett / jelentős
Gnaphalium uliginosum	Nem védett / indifferens
Gnaphalium luteo-album	Nem védett / jelentős
Helichrysum arenarium	Védett
Inula helenium	Védett
Inula conyza	Nem védett / indifferens
Inula ensifolia	Nem védett / indifferens
Inula salicina	Nem védett / indifferens
Inula salicina subsp. aspera	Nem védett / indifferens
Inula salicina subsp. denticulata	Nem védett / indifferens
Inula spiraeifolia	Védett
Inula hirta	Nem védett / indifferens
Inula germanica	Védett
Inula britannica	Nem védett / indifferens
Inula oculus-christi	Védett
Pulicaria vulgaris	Nem védett / indifferens
Pulicaria dysenterica	Nem védett / indifferens
Carpesium cernuum	Nem védett / indifferens
Carpesium abrotanoides	Védett
Buphthalmum salicifolium	Védett
Telekia speciosa	Védett
Ambrosia artemisifolia	Inváziós
Iva xanthiifolia	Inváziós
Xanthium spinosum	Inváziós
Xanthium strumarium	Nem védett / indifferens
Xanthium italicum	Inváziós
Rudbeckia laciniata	Inváziós
Rudbeckia hirta	Nem védett / indifferens
Helianthus annuus	Nem védett / indifferens
Helianthus tuberosus	Inváziós
Helianthus rigidus	Inváziós
Bidens tripartita	Nem védett / indifferens
Bidens cernua	Nem védett / jelentős
Bidens frondosa	Inváziós
Galinsoga parviflora	Inváziós
Galinsoga ciliata	Nem védett / indifferens
Tagetes patula	Nem védett / indifferens
Anthemis cotula	Nem védett / indifferens
Anthemis tinctoria	Nem védett / indifferens
Anthemis austriaca	Nem védett / indifferens
Anthemis arvensis	Nem védett / indifferens
Anthemis ruthenicus	Nem védett / indifferens
Achillea ptarmica	Védett
Achillea ochroleuca	Védett
Achillea horanszkyi	Fokozottan védett
Achillea nobilis subsp. neilreichii	Nem védett / indifferens
Achillea distans	Nem védett / jelentős
Achillea distans subsp. stricta	Nem védett / indifferens
Achillea crithmifolia	Védett
Achillea tuzsonii	Fokozottan védett
Achillea asplenifolia	Nem védett / indifferens
Achillea millefolium	Nem védett / indifferens
Achillea collina	Nem védett / indifferens
Achillea pannonica	Nem védett / indifferens
Achillea setacea	Nem védett / indifferens
Matricaria discoidea	Nem védett / indifferens
Matricaria chamomilla	Nem védett / indifferens
Matricaria maritima subsp. inodora	Nem védett / indifferens
Matricaria tenuifolia	Nem védett / indifferens
Chrysanthemum leucanthemum	Nem védett / indifferens
Chrysanthemum leucanthemum subsp. sylvestre	Nem védett / indifferens
Artemisia × gayeriana	Nem védett / indifferens
Chrysanthemum lanceolatum	Nem védett / jelentős
Chrysanthemum serotinum	Védett
Chrysanthemum corymbosum	Nem védett / indifferens
Chrysanthemum parthenium	Nem védett / indifferens
Chrysanthemum segetum	Nem védett / indifferens
Tanacetum vulgare	Nem védett / indifferens
Artemisia vulgaris	Nem védett / indifferens
Artemisia pontica	Nem védett / jelentős
Artemisia austriaca	Nem védett / jelentős
Artemisia absinthium	Nem védett / indifferens
Artemisia alba	Nem védett / jelentős
Artemisia alba subsp. saxatilis	Nem védett / jelentős
Artemisia alba subsp. canescens	Nem védett / jelentős
Artemisia campestris	Nem védett / indifferens
Artemisia campestris subsp. lednicensis	Nem védett / indifferens
Artemisia scoparia	Nem védett / jelentős
Artemisia annua	Nem védett / indifferens
Artemisia santonicum	Nem védett / indifferens
Artemisia santonicum subsp. patens	Nem védett / indifferens
Artemisia abrotanum	Nem védett / indifferens
Tussilago farfara	Nem védett / indifferens
Petasites hybridus	Nem védett / indifferens
Petasites albus	Védett
Erechtites hieraciifolia	Inváziós
Arnica montana	Védett
Doronicum hungaricum	Védett
Doronicum austriacum	Védett
Doronicum orientale	Védett
Doronicum × sopianae	Védett
Senecio integrifolius	Nem védett / jelentős
Senecio aurantiacus	Védett
Senecio ovirensis	Védett
Senecio ovirensis subsp. gaudinii	Védett
Senecio rivularis	Védett
Senecio vulgaris	Nem védett / indifferens
Senecio sylvaticus	Nem védett / indifferens
Senecio viscosus	Nem védett / jelentős
Senecio vernalis	Inváziós
Senecio rupestris	Nem védett / jelentős
Senecio erucifolius	Nem védett / indifferens
Senecio erucifolius subsp. tenuifolius	Nem védett / indifferens
Senecio jacobaea	Nem védett / indifferens
Senecio erraticus subsp. barbareifolius	Nem védett / indifferens
Senecio aquaticus	Védett
Senecio paludosus	Védett
Senecio paludosus subsp. lanatus	Védett
Senecio fluviatilis	Nem védett / jelentős
Senecio nemorensis	Nem védett / jelentős
Senecio nemorensis subsp. fuchsii	Nem védett / indifferens
Senecio doria	Nem védett / jelentős
Senecio umbrosus	Fokozottan védett
Senecio inaequidens	Nem védett / indifferens
Ligularia sibirica	Védett
Calendula officinalis	Nem védett / indifferens
Echinops sphaerocephalus	Nem védett / indifferens
Echinops ruthenicus	Védett
Jurinea mollis subsp. dolomitica	Védett
Carduus nutans subsp. macrolepis	Nem védett / indifferens
Carduus acanthoides	Nem védett / indifferens
Carduus hamulosus	Védett
Carduus crispus	Nem védett / indifferens
Serratula tinctoria	Nem védett / indifferens
Carduus glaucus	Védett
Carduus collinus	Védett
Carduus × orthocephalus	Nem védett / indifferens
Carduus × solteszii	Védett
Cirsium vulgare	Nem védett / indifferens
Cirsium eriophorum	Nem védett / indifferens
Cirsium boujarti	Védett
Cirsium furiens	Védett
Cirsium arvense	Nem védett / indifferens
Cirsium brachycephalum	Védett
Cirsium palustre	Nem védett / jelentős
Cirsium canum	Nem védett / indifferens
Cirsium pannonicum	Nem védett / jelentős
Cirsium rivulare	Védett
Cirsium erisithales	Védett
Cirsium × linkianum	Védett
Cirsium oleraceum	Nem védett / indifferens
Cirsium × tataricum	Nem védett / indifferens
Cirsium × hybridum	Nem védett / indifferens
Cirsium × silesiacum	Nem védett / indifferens
Onopordum acanthium	Nem védett / indifferens
Crupina vulgaris	Nem védett / jelentős
Serratula lycopifolia	Fokozottan védett
Serratula radiata	Védett
Centaurea calcitrapa	Nem védett / jelentős
Centaurea solstitialis	Védett
Centaurea cyanus	Nem védett / indifferens
Centaurea triumfetti	Nem védett / jelentős
Centaurea triumfetti subsp. aligera	Védett
Centaurea triumfetti subsp. stricta	Védett
Centaurea mollis	Védett
Centaurea salonitana var. taurica	Nem védett / indifferens
Centaurea scabiosa	Nem védett / indifferens
Centaurea fritschii	Nem védett / indifferens
Centaurea spinulosa	Nem védett / indifferens
Centaurea sadleriana	Védett
Centaurea diffusa	Nem védett / indifferens
Centaurea arenaria	Védett
Centaurea arenaria subsp. borysthenica	Védett
Centaurea arenaria subsp. tauscheri	Védett
Centaurea micranthos	Nem védett / indifferens
Centaurea rhenana	Nem védett / indifferens
Centaurea jacea	Nem védett / indifferens
Centaurea jacea subsp. subjacea	Nem védett / indifferens
Centaurea pannonica	Nem védett / indifferens
Centaurea banatica	Nem védett / indifferens
Centaurea macroptilon	Nem védett / indifferens
Centaurea macroptilon subsp. oxylepis	Nem védett / indifferens
Centaurea nigrescens	Nem védett / jelentős
Centaurea nigrescens subsp. vochinensis	Nem védett / jelentős
Centaurea pseudophrygia	Védett
Centaurea stenolepis	Nem védett / jelentős
Centaurea indurata	Nem védett / jelentős
Carthamus lanatus	Nem védett / jelentős
Cichorium intybus	Nem védett / indifferens
Lapsana communis	Nem védett / indifferens
Hypochoeris maculata	Nem védett / jelentős
Hypochoeris radicata	Nem védett / jelentős
Leontodon saxatilis	Nem védett / indifferens
Melandrium sylvestre	Védett
Silene nemoralis	Védett
Silene nutans	Nem védett / indifferens
Silene viridiflora	Nem védett / jelentős
Silene longiflora	Védett
Silene multiflora	Védett
Silene otites	Nem védett / indifferens
Silene otites subsp. wolgensis	Nem védett / indifferens
Silene otites subsp. pseudo-otites	Nem védett / indifferens
Silene borysthenica	Védett
Silene vulgaris	Nem védett / indifferens
Silene flavescens	Fokozottan védett
Silene armeria	Nem védett / indifferens
Silene dichotoma	Nem védett / jelentős
Silene gallica	Nem védett / indifferens
Silene conica	Nem védett / jelentős
Cucubalus baccifer	Nem védett / indifferens
Gypsophila muralis	Nem védett / indifferens
Gypsophila fastigiata subsp. arenaria	Védett
Gypsophila paniculata	Nem védett / jelentős
Petrorhagia saxifraga	Nem védett / indifferens
Petrorhagia prolifera	Nem védett / indifferens
Petrorhagia glumacea var. obcordata	Nem védett / jelentős
Vaccaria hispanica	Nem védett / indifferens
Vaccaria hispanica subsp. grandiflora	Nem védett / indifferens
Dianthus collinus	Védett
Dianthus collinus subsp. glabriusculus	Védett
Dianthus carthusianorum subsp. carthusianorum	Nem védett / jelentős
Dianthus barbatus	Nem védett / indifferens
Dianthus plumarius subsp. praecox	Fokozottan védett
Dianthus plumarius subsp. lumnitzeri	Fokozottan védett
Dianthus plumarius subsp. regis-stephani	Nem védett / jelentős
Dianthus arenarius subsp. borussicus	Védett
Cerastium fontanum subsp. macrocarpum	Nem védett / indifferens
Cerastium arvense	Nem védett / indifferens
Cerastium arvense subsp. calcicola	Védett
Holosteum umbellatum	Nem védett / indifferens
Holosteum umbellatum subsp. glutinosum	Nem védett / indifferens
Moenchia mantica	Nem védett / jelentős
Sagina procumbens	Nem védett / indifferens
Sagina micropetala	Nem védett / indifferens
Sagina ciliata	Nem védett / indifferens
Sagina nodosa	Nem védett / indifferens
Sagina subulata	Nem védett / jelentős
Sagina sabuletorum	Nem védett / indifferens
Sagina saginoides	Nem védett / indifferens
Minuartia viscosa	Nem védett / jelentős
Minuartia fastigiata	Nem védett / jelentős
Minuartia glomerata	Nem védett / jelentős
Minuartia setacea	Nem védett / jelentős
Minuartia verna	Nem védett / jelentős
Minuartia verna subsp. ramosissima	Nem védett / jelentős
Minuartia frutescens	Védett
Arenaria procera subsp. glabra	Nem védett / jelentős
Arenaria serpyllifolia	Nem védett / indifferens
Leontodon incanus	Védett
Leontodon autumnalis	Nem védett / indifferens
Leontodon hispidus	Nem védett / indifferens
Helminthia echioides	Nem védett / indifferens
Picris hieracioides	Nem védett / indifferens
Lactuca perennis	Nem védett / indifferens
Picris hieracioides subsp. crepoides	Nem védett / indifferens
Picris hieracioides subsp. spinulosa	Nem védett / indifferens
Tragopogon floccosus	Védett
Tragopogon dubius	Nem védett / indifferens
Tragopogon dubius subsp. major	Nem védett / indifferens
Tragopogon orientalis	Nem védett / indifferens
Scorzonera purpurea	Védett
Scorzonera hispanica	Nem védett / jelentős
Scorzonera austriaca	Nem védett / jelentős
Scorzonera humilis	Védett
Scorzonera parviflora	Nem védett / jelentős
Podospermum canum	Nem védett / indifferens
Podospermum laciniatum	Nem védett / jelentős
Chondrilla juncea	Nem védett / indifferens
Taraxacum serotinum	Védett
Taraxacum bessarabicum	Nem védett / jelentős
Taraxacum laevigatum	Nem védett / indifferens
Taraxacum palustre	Nem védett / jelentős
Taraxacum officinale	Nem védett / indifferens
Mycelis muralis	Nem védett / indifferens
Lactuca viminea	Nem védett / indifferens
Lactuca quercina	Nem védett / indifferens
Lactuca saligna	Nem védett / indifferens
Lactuca serriola	Nem védett / indifferens
Sonchus palustris	Védett
Sonchus arvensis	Nem védett / indifferens
Sonchus arvensis subsp. uliginosus	Nem védett / indifferens
Sonchus oleraceus	Nem védett / indifferens
Sonchus asper	Nem védett / indifferens
Prenanthes purpurea	Nem védett / jelentős
Crepis paludosa	Nem védett / jelentős
Crepis praemorsa	Nem védett / jelentős
Crepis pulchra	Nem védett / indifferens
Hieracium × viridifolium	Nem védett / indifferens
Hieracium hoppeanum	Nem védett / indifferens
Hieracium × schultesii	Nem védett / indifferens
Hieracium × bifurcum	Nem védett / jelentős
Hieracium × laschii	Nem védett / jelentős
Hieracium × brachiatum	Nem védett / jelentős
Hieracium rothianum	Nem védett / indifferens
Hieracium pilosella	Nem védett / indifferens
Hieracium lactucella	Nem védett / jelentős
Hieracium aurantiacum	Védett
Hieracium caespitosum	Nem védett / jelentős
Hieracium × flagellare	Nem védett / indifferens
Hieracium × floribundum	Nem védett / jelentős
Hieracium × piloselliflorum	Nem védett / jelentős
Hieracium bauhinii	Nem védett / indifferens
Hieracium × leptophyton	Nem védett / indifferens
Hieracium × koernickeanum	Nem védett / indifferens
Hieracium × polymastix	Nem védett / jelentős
Hieracium × densiflorum	Nem védett / indifferens
Hieracium × fallacinum	Nem védett / jelentős
Hieracium auriculoides	Nem védett / jelentős
Hieracium × euchaetium	Nem védett / indifferens
Hieracium cymosum	Nem védett / indifferens
Hieracium × sciadophorum	Nem védett / jelentős
Hieracium × zizianum	Nem védett / indifferens
Hieracium piloselloides	Nem védett / indifferens
Hieracium × florentinoides	Nem védett / indifferens
Hieracium × sulphureum	Nem védett / jelentős
Hieracium × arvicola	Nem védett / indifferens
Hieracium × calodon	Nem védett / indifferens
Hieracium echioides	Nem védett / indifferens
Hieracium × fallax	Nem védett / indifferens
Hieracium staticifolium	Védett
Hieracium bupleuroides subsp. tatrae	Védett
Hieracium pallidum	Nem védett / indifferens
Hieracium × saxifraga	Nem védett / jelentős
Hieracium kossuthianum	Védett
Hieracium × glaucinum	Nem védett / indifferens
Hieracium × wiesbaurianum	Nem védett / jelentős
Hieracium sylvaticum	Nem védett / indifferens
Hieracium × praecurrens	Nem védett / indifferens
Hieracium × diaphanoides	Nem védett / indifferens
Hieracium lachenalii	Nem védett / indifferens
Hieracium maculatum	Nem védett / indifferens
Hieracium bifidum	Nem védett / indifferens
Hieracium caesium	Nem védett / indifferens
Hieracium × laevicaule	Nem védett / indifferens
Hieracium × ramosum	Nem védett / jelentős
Hieracium laevigatum	Nem védett / indifferens
Hieracium umbellatum	Nem védett / indifferens
Hieracium × latifolium	Nem védett / indifferens
Hieracium × laurinum	Nem védett / jelentős
Hieracium sabaudum	Nem védett / indifferens
Hieracium racemosum	Nem védett / indifferens
Loranthus europaeus	Nem védett / indifferens
Viscum album	Nem védett / indifferens
Viscum album subsp. abietis	Nem védett / indifferens
Viscum album subsp. austriacum	Nem védett / indifferens
Thesium bavarum	Nem védett / indifferens
Thesium linophyllon	Nem védett / indifferens
Thesium arvense	Nem védett / indifferens
Thesium dollineri	Nem védett / jelentős
Thesium dollineri subsp. simplex	Nem védett / jelentős
Phytolacca americana	Inváziós
Oxybaphus nyctagineus	Inváziós
Montia fontana subsp. minor	Védett
Portulaca oleracea	Nem védett / indifferens
Agrostemma githago	Védett
Viscaria vulgaris	Nem védett / indifferens
Lychnis coronaria	Védett
Lychnis flos-cuculi	Nem védett / indifferens
Melandrium noctiflorum	Nem védett / jelentős
Melandrium viscosum	Nem védett / indifferens
Melandrium album	Nem védett / indifferens
Dianthus serotinus	Védett
Dianthus superbus	Védett
Dianthus deltoides	Védett
Dianthus armeria	Nem védett / indifferens
Dianthus armeria subsp. armeriastrum	Nem védett / indifferens
Dianthus diutinus	Fokozottan védett
Dianthus giganteiformis	Védett
Dianthus pontederae	Nem védett / indifferens
Dianthus carthusianorum	Nem védett / jelentős
Dianthus carthusianorum subsp. latifolius	Nem védett / jelentős
Dianthus carthusianorum subsp. saxigenus	Nem védett / jelentős
Dianthus × hellwigii	Védett
Saponaria officinalis	Nem védett / indifferens
Stellaria nemorum	Nem védett / jelentős
Stellaria media	Nem védett / indifferens
Stellaria media subsp. neglecta	Nem védett / indifferens
Stellaria media subsp. pallida	Nem védett / indifferens
Stellaria holostea	Nem védett / indifferens
Stellaria uliginosa	Nem védett / indifferens
Stellaria graminea	Nem védett / indifferens
Stellaria palustris	Védett
Myosoton aquaticum	Nem védett / indifferens
Cerastium dubium	Nem védett / indifferens
Cerastium sylvaticum	Nem védett / indifferens
Cerastium glomeratum	Nem védett / indifferens
Cerastium brachypetalum	Nem védett / indifferens
Cerastium brachypetalum subsp. tauricum	Nem védett / indifferens
Cerastium brachypetalum subsp. tenoreanum	Nem védett / indifferens
Cerastium subtetrandrum	Nem védett / indifferens
Cerastium semidecandrum	Nem védett / indifferens
Cerastium pumilum	Nem védett / indifferens
Cerastium pumilum subsp. pallens	Nem védett / indifferens
Cerastium fontanum	Nem védett / indifferens
Arenaria leptoclados	Nem védett / indifferens
Moehringia trinervia	Nem védett / indifferens
Moehringia muscosa	Védett
Spergula arvensis	Nem védett / indifferens
Spergula pentandra	Nem védett / indifferens
Spergularia maritima	Nem védett / jelentős
Spergularia marina	Nem védett / indifferens
Spergularia rubra	Nem védett / indifferens
Scleranthus perennis	Védett
Scleranthus polycarpos	Nem védett / jelentős
Scleranthus verticillatus	Nem védett / jelentős
Scleranthus annuus	Nem védett / indifferens
Paronychia cephalotes	Védett
Herniaria glabra	Nem védett / indifferens
Herniaria hirsuta	Nem védett / jelentős
Herniaria incana	Védett
Polycnemum heuffelii	Nem védett / indifferens
Polycnemum verrucosum	Nem védett / indifferens
Polycnemum arvense	Nem védett / indifferens
Polycnemum majus	Nem védett / indifferens
Beta trigyna	Nem védett / indifferens
Chenopodium aristatum	Inváziós
Chenopodium ambrosioides	Nem védett / indifferens
Chenopodium botrys	Nem védett / indifferens
Chenopodium schraderanum	Nem védett / indifferens
Chenopodium bonus-henricus	Nem védett / indifferens
Chenopodium polyspermum	Nem védett / indifferens
Chenopodium vulvaria	Nem védett / indifferens
Chenopodium hybridum	Nem védett / indifferens
Chenopodium murale	Nem védett / indifferens
Chenopodium urbicum	Nem védett / indifferens
Chenopodium rubrum	Nem védett / indifferens
Chenopodium botryoides	Nem védett / jelentős
Chenopodium glaucum	Nem védett / indifferens
Chenopodium ficifolium	Nem védett / indifferens
Chenopodium opulifolium	Nem védett / indifferens
Chenopodium strictum	Inváziós
Chenopodium album	Nem védett / indifferens
Chenopodium suecicum	Nem védett / indifferens
Chenopodium giganteum	Nem védett / indifferens
Chenopodium foliosum	Nem védett / indifferens
Chenopodium pumilio	Nem védett / indifferens
Atriplex hortensis	Nem védett / indifferens
Atriplex acuminata	Nem védett / indifferens
Atriplex littoralis	Nem védett / indifferens
Atriplex oblongifolia	Nem védett / indifferens
Atriplex patula	Nem védett / indifferens
Atriplex prostrata	Nem védett / indifferens
Atriplex tatarica	Nem védett / indifferens
Atriplex rosea	Nem védett / indifferens
Ceratoides latens	Védett
Camphorosma annua	Nem védett / indifferens
Bassia sedoides	Védett
Kochia scoparia	Inváziós
Kochia prostrata	Nem védett / indifferens
Kochia laniflora	Nem védett / indifferens
Corispermum nitidum	Védett
Corispermum canescens	Védett
Salicornia prostrata	Nem védett / jelentős
Salicornia prostrata subsp. simonkaiana	Nem védett / jelentős
Suaeda maritima subsp. salinaria	Nem védett / indifferens
Suaeda prostrata subsp. prostrata	Nem védett / indifferens
Suaeda pannonica	Nem védett / indifferens
Salsola kali subsp. ruthenica	Nem védett / indifferens
Salsola soda	Védett
Amaranthus retroflexus	Nem védett / indifferens
Amaranthus chlorostachys	Inváziós
Amaranthus bouchonii	Nem védett / indifferens
Amaranthus patulus	Nem védett / indifferens
Amaranthus paniculatus	Nem védett / indifferens
Amaranthus blitoides	Inváziós
Amaranthus albus	Nem védett / indifferens
Amaranthus crispus	Nem védett / indifferens
Amaranthus graecizans subsp. sylvestris	Nem védett / indifferens
Amaranthus deflexus	Nem védett / indifferens
Amaranthus lividus	Nem védett / indifferens
Amaranthus lividus subsp. ascendens	Nem védett / indifferens
Primula vulgaris	Védett
Primula elatior	Védett
Primula veris	Nem védett / indifferens
Primula veris subsp. inflata	Nem védett / indifferens
Primula auricula subsp. hungarica	Fokozottan védett
Primula farinosa subsp. alpigena	Fokozottan védett
Primula × brevistyla	Védett
Androsace maxima	Védett
Androsace elongata	Nem védett / jelentős
Hottonia palustris	Védett
Samolus valerandi	Védett
Lysimachia nummularia	Nem védett / indifferens
Lysimachia nemorum	Védett
Lysimachia thyrsiflora	Nem védett / jelentős
Lysimachia vulgaris	Nem védett / indifferens
Lysimachia punctata	Nem védett / jelentős
Glaux maritima	Védett
Anagallis arvensis	Nem védett / indifferens
Anagallis foemina	Nem védett / indifferens
Centunculus minimus	Nem védett / jelentős
Polygonum mite	Nem védett / indifferens
Polygonum minus	Nem védett / indifferens
Polygonum arenarium	Nem védett / indifferens
Polygonum graminifolium	Nem védett / jelentős
Polygonum patulum	Nem védett / indifferens
Polygonum aviculare	Nem védett / indifferens
Polygonum neglectum	Nem védett / indifferens
Fallopia convolvulus	Nem védett / indifferens
Fallopia dumetorum	Nem védett / indifferens
Reynoutria japonica	Inváziós
Reynoutria sachalinensis	Inváziós
Fagopyrum esculentum	Nem védett / indifferens
Morus alba	Nem védett / indifferens
Morus nigra	Nem védett / indifferens
Ficus carica	Nem védett / indifferens
Humulus lupulus	Nem védett / indifferens
Humulus scandens	Inváziós
Cannabis sativa	Nem védett / indifferens
Cannabis sativa subsp. spontanea	Nem védett / indifferens
Urtica kioviensis	Védett
Urtica urens	Nem védett / indifferens
Urtica pilulifera	Nem védett / indifferens
Parietaria officinalis	Nem védett / indifferens
Ulmus laevis	Nem védett / indifferens
Ulmus minor	Nem védett / indifferens
Ulmus glabra	Nem védett / indifferens
Celtis australis	Nem védett / indifferens
Celtis occidentalis	Inváziós
Carpinus orientalis	Védett
Carpinus betulus	Nem védett / indifferens
Ostrya carpinifolia	Nem védett / jelentős
Corylus avellana	Nem védett / indifferens
Corylus colurna	Nem védett / indifferens
Betula pendula	Nem védett / indifferens
Betula pubescens	Védett
Alnus viridis	Védett
Alnus glutinosa	Nem védett / indifferens
Alnus incana	Nem védett / jelentős
Fagus sylvatica	Nem védett / indifferens
Fagus sylvatica subsp. moesiaca	Nem védett / indifferens
Castanea sativa	Nem védett / indifferens
Quercus cerris	Nem védett / indifferens
Quercus farnetto	Nem védett / indifferens
Quercus pubescens	Nem védett / indifferens
Quercus virgiliana	Nem védett / indifferens
Quercus petraea	Nem védett / indifferens
Quercus dalechampii	Nem védett / indifferens
Quercus polycarpa	Nem védett / indifferens
Quercus robur	Nem védett / indifferens
Quercus robur subsp. asterotricha	Nem védett / indifferens
Ulmus pumila	Nem védett / indifferens
Maclura pomifera	Nem védett / indifferens
Quercus robur subsp. pilosa	Nem védett / indifferens
Quercus robur subsp. slavonica	Nem védett / indifferens
Quercus rubra	Nem védett / indifferens
Juglans regia	Nem védett / indifferens
Juglans nigra	Nem védett / indifferens
Populus tremula	Nem védett / indifferens
Populus alba	Nem védett / indifferens
Populus nigra	Nem védett / indifferens
Populus × canescens	Nem védett / indifferens
Populus simonii	Nem védett / indifferens
Populus deltoides	Nem védett / indifferens
Populus × canadensis	Nem védett / indifferens
Populus × gileadensis	Nem védett / indifferens
Salix pentandra	Védett
Salix fragilis	Nem védett / indifferens
Salix triandra subsp. discolor	Nem védett / indifferens
Salix alba	Nem védett / indifferens
Salix alba subsp. vitellina	Nem védett / indifferens
Salix nigricans	Védett
Salix caprea	Nem védett / indifferens
Salix aurita	Védett
Salix cinerea	Nem védett / indifferens
Salix eleagnos	Védett
Salix viminalis	Nem védett / indifferens
Salix rosmarinifolia	Nem védett / jelentős
Salix purpurea	Nem védett / indifferens
Salix × multinervis	Védett
Alisma plantago-aquatica	Nem védett / indifferens
Alisma lanceolatum	Nem védett / indifferens
Alisma gramineum	Nem védett / jelentős
Caldesia parnassifolia	Fokozottan védett
Sagittaria sagittifolia	Nem védett / indifferens
Butomus umbellatus	Nem védett / indifferens
Elodea canadensis	Inváziós
Elodea densa	Nem védett / indifferens
Elodea nuttallii	Nem védett / indifferens
Vallisneria spiralis	Nem védett / indifferens
Stratiotes aloides	Nem védett / jelentős
Hydrocharis morsus-ranae	Nem védett / indifferens
Triglochin maritimum	Nem védett / jelentős
Triglochin palustre	Védett
Potamogeton natans	Nem védett / jelentős
Potamogeton nodosus	Nem védett / jelentős
Potamogeton coloratus	Védett
Potamogeton lucens	Nem védett / jelentős
Potamogeton × zizii	Nem védett / jelentős
Potamogeton gramineus	Nem védett / jelentős
Potamogeton perfoliatus	Nem védett / indifferens
Potamogeton crispus	Nem védett / indifferens
Potamogeton acutifolius	Nem védett / jelentős
Potamogeton obtusifolius	Védett
Potamogeton berchtoldii	Nem védett / indifferens
Potamogeton panormitanus	Nem védett / indifferens
Potamogeton trichoides	Nem védett / jelentős
Potamogeton pectinatus	Nem védett / indifferens
Potamogeton pectinatus subsp. balatonicus	Nem védett / indifferens
Potamogeton filiformis	Nem védett / jelentős
Groenlandia densa	Védett
Zannichellia palustris	Nem védett / jelentős
Zannichellia palustris subsp. polycarpa	Nem védett / jelentős
Zannichellia palustris subsp. pedicellata	Nem védett / jelentős
Najas marina	Nem védett / indifferens
Najas minor	Nem védett / jelentős
Veratrum nigrum	Nem védett / indifferens
Veratrum album	Védett
Veratrum album subsp. lobelianum	Védett
Bulbocodium versicolor	Fokozottan védett
Colchicum hungaricum	Fokozottan védett
Colchicum arenarium	Fokozottan védett
Colchicum autumnale	Nem védett / indifferens
Asphodelus albus	Védett
Anthericum liliago	Védett
Anthericum ramosum	Nem védett / indifferens
Hemerocallis lilio-asphodelus	Fokozottan védett
Hemerocallis fulva	Nem védett / indifferens
Gagea villosa	Nem védett / indifferens
Gagea bohemica	Védett
Gagea minima	Nem védett / indifferens
Gagea spathacea	Védett
Gagea pratensis	Nem védett / indifferens
Gagea lutea	Nem védett / indifferens
Gagea pusilla	Nem védett / jelentős
Allium ursinum	Nem védett / jelentős
Allium victorialis	Védett
Allium atropurpureum	Nem védett / jelentős
Allium angulosum	Nem védett / jelentős
Allium montanum	Nem védett / indifferens
Allium suaveolens	Védett
Allium moschatum	Védett
Allium carinatum	Védett
Allium oleraceum	Nem védett / jelentős
Allium paniculatum subsp. marginatum	Védett
Allium flavum	Nem védett / indifferens
Allium vineale	Nem védett / indifferens
Allium sphaerocephalon	Védett
Allium sphaerocephalon subsp. amethystinum	Védett
Allium rotundum subsp. waldsteinii	Nem védett / jelentős
Allium scorodoprasum	Nem védett / indifferens
Allium atroviolaceum	Nem védett / jelentős
Lilium martagon	Védett
Lilium bulbiferum	Fokozottan védett
Fritillaria meleagris	Védett
Erythronium dens-canis	Védett
Scilla spetana	Védett
Scilla drunensis	Védett
Scilla kladnii	Védett
Scilla vindobonensis	Védett
Scilla autumnalis	Védett
Scilla siberica	Nem védett / indifferens
Ornithogalum boucheanum	Nem védett / indifferens
Ornithogalum degenianum	Nem védett / jelentős
Ornithogalum pyramidale	Védett
Ornithogalum sphaerocarpum	Védett
Ornithogalum comosum	Védett
Ornithogalum orthophyllum	Nem védett / indifferens
Ornithogalum umbellatum	Nem védett / indifferens
Ornithogalum refractum	Védett
Muscari comosum	Nem védett / indifferens
Muscari tenuiflorum	Nem védett / jelentős
Muscari racemosum	Nem védett / indifferens
Muscari botryoides	Védett
Muscari botryoides subsp. transsilvanicum	Védett
Crocus albiflorus	Védett
Muscari botryoides subsp. kerneri	Védett
Asparagus officinalis	Nem védett / indifferens
Ruscus hypoglossum	Védett
Ruscus aculeatus	Védett
Majanthemum bifolium	Nem védett / jelentős
Polygonatum verticillatum	Védett
Polygonatum latifolium	Nem védett / indifferens
Polygonatum odoratum	Nem védett / indifferens
Polygonatum multiflorum	Nem védett / indifferens
Convallaria majalis	Nem védett / indifferens
Paris quadrifolia	Nem védett / jelentős
Tofieldia calyculata	Nem védett / jelentős
Galanthus nivalis	Védett
Leucojum vernum	Védett
Leucojum aestivum	Védett
Sternbergia colchiciflora	Védett
Narcissus angustifolius	Védett
Narcissus pseudonarcissus	Nem védett / indifferens
Narcissus poëticus	Nem védett / indifferens
Tamus communis	Védett
Crocus reticulatus	Védett
Crocus heuffelianus	Védett
Crocus tommasinianus	Védett
Gladiolus palustris	Fokozottan védett
Gladiolus imbricatus	Védett
Iris pumila	Védett
Iris arenaria	Védett
Iris aphylla subsp. hungarica	Fokozottan védett
Iris variegata	Védett
Iris germanica	Nem védett / indifferens
Iris pseudacorus	Nem védett / indifferens
Iris spuria	Védett
Iris sibirica	Védett
Iris graminea	Védett
Iris graminea subsp. pseudocyperus	Védett
Juncus bufonius	Nem védett / indifferens
Juncus bufonius subsp. nastanthus	Nem védett / indifferens
Juncus sphaerocarpus	Nem védett / jelentős
Juncus tenageia	Nem védett / jelentős
Juncus compressus	Nem védett / indifferens
Juncus gerardii	Nem védett / indifferens
Juncus tenuis	Inváziós
Juncus inflexus	Nem védett / indifferens
Juncus conglomeratus	Nem védett / indifferens
Juncus effusus	Nem védett / indifferens
Juncus capitatus	Nem védett / jelentős
Juncus maritimus	Védett
Juncus bulbosus	Nem védett / jelentős
Juncus subnodulosus	Nem védett / jelentős
Juncus atratus	Nem védett / jelentős
Juncus alpinus	Védett
Juncus articulatus	Nem védett / indifferens
Luzula forsteri	Nem védett / jelentős
Luzula pilosa	Nem védett / indifferens
Luzula luzuloides	Nem védett / indifferens
Luzula campestris	Nem védett / indifferens
Luzula multiflora	Nem védett / jelentős
Luzula pallescens	Nem védett / jelentős
Cypripedium calceolus	Fokozottan védett
Cephalanthera rubra	Védett
Cephalanthera damasonium	Védett
Cephalanthera × schulzei	Védett
Cephalanthera longifolia	Védett
Epipactis palustris	Védett
Carex caespitosa	Védett
Epipactis atrorubens	Védett
Epipactis atrorubens subsp. borbasii	Védett
Epipactis × graberi	Védett
Epipactis microphylla	Védett
Epipactis purpurata	Védett
Epipactis purpurata var. rosea	Védett
Epipactis helleborine	Védett
Epipactis pontica	Védett
Epipactis gracilis	Fokozottan védett
Epipactis muelleri	Védett
Epipactis leptochila	Védett
Epipactis leptochila subsp. neglecta	Védett
Epipactis bugacensis	Fokozottan védett
Epipactis albensis	Védett
Epipactis nordeniorum	Védett
Epipactis tallosii	Védett
Epipactis helleborine agg.	Védett
Limodorum abortivum	Védett
Carex elata	Nem védett / jelentős
Listera ovata	Védett
Neottia nidus-avis	Védett
Spiranthes spiralis	Védett
Spiranthes aestivalis	Védett
Goodyera repens	Védett
Epipogium aphyllum	Fokozottan védett
Herminium monorchis	Nem védett / jelentős
Coeloglossum viride	Védett
Platanthera bifolia	Védett
Platanthera chlorantha	Védett
Platanthera × hybrida	Védett
Gymnadenia conopsea	Védett
Gymnadenia conopsea subsp. densiflora	Védett
Dactylorhiza incarnata subsp. haematodes	Védett
Gymnadenia odoratissima	Védett
Ophrys insectifera	Fokozottan védett
Ophrys sphecodes	Fokozottan védett
Ophrys scolopax subsp. cornuta	Fokozottan védett
Ophrys × nelsonii	Fokozottan védett
Ophrys fuciflora	Fokozottan védett
Ophrys holubyana	Fokozottan védett
Ophrys apifera	Fokozottan védett
Orchis morio	Védett
Orchis coriophora	Védett
Orchis × timbalii	Védett
X Orchidactyla Drudei	Védett
Orchis ustulata	Védett
Orchis ustulata subsp. aestivalis	Védett
Orchis tridentata	Védett
Orchis simia	Védett
Orchis militaris	Védett
Orchis purpurea	Védett
Orchis × hybrida	Védett
Orchis mascula subsp. signifera	Védett
Orchis pallens	Védett
Orchis laxiflora subsp. palustris	Védett
Orchis laxiflora subsp. elegans	Védett
Traunsteinera globosa	Fokozottan védett
Dactylorhiza sambucina	Védett
Dactylorhiza incarnata	Védett
Dactylorhiza incarnata var. hyphaematodes	Védett
Dactylorhiza incarnata subsp. serotina	Védett
Dactylorhiza majalis	Védett
Dactylorhiza × aschersoniana	Védett
Dactylorhiza maculata	Védett
Dactylorhiza fuchsii	Védett
Dactylorhiza fuchsii subsp. sooiana	Védett
Orchis × angusticruris	Védett
Dactylorhiza maculata subsp. transsylvanica	Védett
Anacamptis pyramidalis	Védett
Himantoglossum caprinum	Fokozottan védett
Himantoglossum adriaticum	Fokozottan védett
Liparis loeselii	Fokozottan védett
Corallorhiza trifida	Védett
Malaxis monophyllos	Nem védett / jelentős
Hammarbya paludosa	Fokozottan védett
Scirpus sylvaticus	Nem védett / indifferens
Scirpus radicans	Nem védett / jelentős
Bolboschoenus maritimus	Nem védett / indifferens
Blysmus compressus	Védett
Holoschoenus romanus subsp. holoschoenus	Nem védett / jelentős
Schoenoplectus lacustris	Nem védett / indifferens
Schoenoplectus tabernaemontani	Nem védett / indifferens
Schoenoplectus litoralis	Nem védett / jelentős
Schoenoplectus triqueter	Nem védett / jelentős
Schoenoplectus americanus subsp. triangularis	Nem védett / jelentős
Schoenoplectus mucronatus	Nem védett / jelentős
Schoenoplectus supinus	Nem védett / indifferens
Schoenoplectus setaceus	Nem védett / jelentős
Eleocharis acicularis	Nem védett / jelentős
Eleocharis quinqueflora	Védett
Eleocharis ovata	Nem védett / jelentős
Eleocharis carniolica	Védett
Eleocharis palustris	Nem védett / indifferens
Eleocharis mamillata	Nem védett / indifferens
Cynosurus echinatus	Nem védett / indifferens
Eleocharis austriaca	Nem védett / indifferens
Eleocharis uniglumis	Védett
Trichophorum alpinum	Nem védett / indifferens
Trichophorum caespitosum	Nem védett / indifferens
Eriophorum vaginatum	Védett
Eriophorum gracile	Védett
Eriophorum latifolium	Védett
Eriophorum angustifolium	Védett
Cyperus fuscus	Nem védett / indifferens
Cyperus difformis	Inváziós
Chlorocyperus glomeratus	Nem védett / indifferens
Chlorocyperus longus	Nem védett / jelentős
Chlorocyperus glaber	Nem védett / jelentős
Dichostylis micheliana	Nem védett / jelentős
Acorellus pannonicus	Nem védett / jelentős
Pycreus flavescens	Nem védett / jelentős
Schoenus nigricans	Védett
Schoenus ferrugineus	Nem védett / indifferens
Cladium mariscus	Nem védett / indifferens
Cladium mariscus subsp. martii	Nem védett / indifferens
Rhynchospora alba	Védett
Carex davalliana	Védett
Carex bohemica	Védett
Carex stenophylla	Nem védett / indifferens
Carex divisa	Nem védett / jelentős
Carex diandra	Védett
Carex appropinquata	Védett
Carex paniculata	Védett
Carex vulpina	Nem védett / indifferens
Carex cuprina	Nem védett / indifferens
Carex spicata	Nem védett / indifferens
Carex pairae	Nem védett / indifferens
Carex pairae subsp. leersiana	Nem védett / indifferens
Carex divulsa	Nem védett / indifferens
Carex praecox	Nem védett / indifferens
Carex brizoides	Nem védett / jelentős
Carex elongata	Nem védett / jelentős
Carex disticha	Nem védett / jelentős
Carex repens	Védett
Carex leporina	Nem védett / jelentős
Carex canescens	Védett
Carex echinata	Védett
Carex remota	Nem védett / indifferens
Carex nigra	Nem védett / jelentős
Carex gracilis	Nem védett / indifferens
Carex gracilis subsp. intermedia	Nem védett / indifferens
Carex buekii	Védett
Carex hartmannii	Védett
Carex buxbaumii	Védett
Carex limosa	Nem védett / jelentős
Carex flacca	Nem védett / indifferens
Carex pendula	Nem védett / jelentős
Carex pallescens	Nem védett / indifferens
Carex tomentosa	Nem védett / indifferens
Carex ericetorum	Nem védett / jelentős
Carex montana	Nem védett / indifferens
Carex pilulifera	Nem védett / jelentős
Carex fritschii	Védett
Carex supina	Nem védett / jelentős
Carex liparicarpos	Nem védett / indifferens
Carex caryophyllea	Nem védett / indifferens
Carex umbrosa	Védett
Carex hallerana	Nem védett / jelentős
Carex humilis	Nem védett / indifferens
Carex digitata	Nem védett / indifferens
Carex alba	Védett
Carex panicea	Nem védett / jelentős
Carex sylvatica	Nem védett / indifferens
Carex strigosa	Védett
Carex brevicollis	Védett
Carex michelii	Nem védett / indifferens
Carex pilosa	Nem védett / indifferens
Carex distans	Nem védett / indifferens
Carex hostiana	Nem védett / jelentős
Carex flava	Nem védett / jelentős
Carex lepidocarpa	Nem védett / jelentős
Carex serotina	Nem védett / jelentős
Carex hordeistichos	Nem védett / jelentős
Carex secalina	Nem védett / jelentős
Carex pseudocyperus	Nem védett / jelentős
Carex rostrata	Védett
Carex vesicaria	Nem védett / indifferens
Carex acutiformis	Nem védett / indifferens
Carex riparia	Nem védett / indifferens
Carex melanostachya	Nem védett / indifferens
Carex lasiocarpa	Védett
Carex hirta	Nem védett / indifferens
Commelina communis	Nem védett / indifferens
Bromus ramosus	Nem védett / indifferens
Bromus benekenii	Nem védett / indifferens
Bromus erectus	Nem védett / indifferens
Bromus pannonicus	Nem védett / indifferens
Bromus inermis	Nem védett / indifferens
Bromus rigidus	Nem védett / indifferens
Bromus sterilis	Nem védett / indifferens
Bromus tectorum	Nem védett / indifferens
Bromus arvensis	Nem védett / indifferens
Bromus racemosus	Nem védett / indifferens
Bromus commutatus	Nem védett / indifferens
Bromus mollis	Nem védett / indifferens
Bromus lepidus	Nem védett / indifferens
Bromus squarrosus	Nem védett / indifferens
Bromus japonicus	Nem védett / indifferens
Bromus secalinus	Nem védett / jelentős
Bromus brachystachys	Nem védett / indifferens
Bromus madritensis	Nem védett / indifferens
Bromus willdenowii	Nem védett / indifferens
Brachypodium pinnatum	Nem védett / indifferens
Brachypodium pinnatum subsp. rupestre	Nem védett / indifferens
Brachypodium sylvaticum	Nem védett / indifferens
Festuca amethystina	Védett
Festuca ovina	Nem védett / indifferens
Festuca tenuifolia	Nem védett / indifferens
Festuca pallens	Nem védett / indifferens
Festuca pallens subsp. pannonica	Nem védett / jelentős
Festuca vaginata	Nem védett / indifferens
Festuca vaginata subsp. dominii	Nem védett / indifferens
Festuca × wagneri	Védett
Festuca dalmatica	Védett
Festuca pseudodalmatica	Nem védett / indifferens
Festuca valesiaca	Nem védett / indifferens
Festuca rupicola	Nem védett / indifferens
Festuca pseudovina	Nem védett / indifferens
Festuca heterophylla	Nem védett / indifferens
Festuca rubra	Nem védett / indifferens
Festuca nigrescens	Nem védett / indifferens
Festuca gigantea	Nem védett / indifferens
Festuca arundinacea	Nem védett / indifferens
Festuca arundinacea subsp. uechtritziana	Nem védett / indifferens
Festuca pratensis	Nem védett / indifferens
Festuca altissima	Nem védett / jelentős
Festuca drymeia	Nem védett / jelentős
Vulpia myuros	Nem védett / indifferens
Vulpia bromoides	Nem védett / indifferens
Glyceria maxima	Nem védett / indifferens
Glyceria fluitans	Nem védett / indifferens
Glyceria plicata	Nem védett / indifferens
Glyceria declinata	Nem védett / jelentős
Glyceria nemoralis	Nem védett / jelentős
Melica ciliata	Nem védett / indifferens
Glyceria × pedicellata	Nem védett / indifferens
Puccinellia distans	Nem védett / indifferens
Puccinellia limosa	Nem védett / indifferens
Puccinellia peisonis	Védett
Puccinellia pannonica	Nem védett / indifferens
Sclerochloa dura	Nem védett / indifferens
Poa remota	Védett
Poa pratensis	Nem védett / indifferens
Poa angustifolia	Nem védett / indifferens
Poa subcoerulea	Nem védett / indifferens
Poa trivialis	Nem védett / indifferens
Poa nemoralis	Nem védett / indifferens
Poa palustris	Nem védett / indifferens
Poa pannonica subsp. scabra	Védett
Poa compressa	Nem védett / indifferens
Poa badensis	Nem védett / jelentős
Poa bulbosa	Nem védett / indifferens
Poa annua	Nem védett / indifferens
Poa supina	Nem védett / indifferens
Briza media	Nem védett / indifferens
Catabrosa aquatica	Nem védett / jelentős
Dactylis glomerata	Nem védett / indifferens
Dactylis polygama	Nem védett / indifferens
Cynosurus cristatus	Nem védett / jelentős
Melica transsilvanica	Nem védett / indifferens
Melica altissima	Nem védett / jelentős
Melica uniflora	Nem védett / indifferens
Melica nutans	Nem védett / indifferens
Melica picta	Nem védett / jelentős
Sesleria heuflerana	Védett
Sesleria hungarica	Védett
Sesleria sadlerana	Védett
Sesleria varia	Védett
Sesleria uliginosa	Védett
Lolium perenne	Nem védett / indifferens
Lolium multiflorum	Nem védett / indifferens
Lolium temulentum	Nem védett / indifferens
Lolium remotum	Nem védett / indifferens
Molinia caerulea	Nem védett / indifferens
Molinia caerulea subsp. hungarica	Nem védett / indifferens
Molinia caerulea subsp. horanszkyi	Nem védett / indifferens
Molinia caerulea subsp. simonii	Nem védett / indifferens
Molinia arundinacea	Nem védett / indifferens
Molinia arundinacea subsp. ujhelyii	Nem védett / indifferens
Molinia arundinacea subsp. pocsii	Nem védett / indifferens
Cephalozia lacinulata	Védett
Agropyron pectinatum	Nem védett / jelentős
Agropyron caninum	Nem védett / indifferens
Agropyron repens	Nem védett / indifferens
Agropyron intermedium	Nem védett / indifferens
Agropyron intermedium subsp. trichophorum	Nem védett / indifferens
Haynaldia villosa	Nem védett / indifferens
Aegilops cylindrica	Nem védett / indifferens
Secale sylvestre	Nem védett / indifferens
Hordeum murinum	Nem védett / indifferens
Hordeum murinum subsp. leporinum	Nem védett / indifferens
Hordeum hystrix	Nem védett / indifferens
Hordeum marinum	Nem védett / indifferens
Hordelymus europaeus	Nem védett / indifferens
Taeniatherum asperum	Nem védett / jelentős
Phragmites australis	Nem védett / indifferens
Beckmannia eruciformis	Nem védett / indifferens
Sphagnum quinquefarium	Védett
Pholiurus pannonicus	Nem védett / indifferens
Nardus stricta	Nem védett / jelentős
Aira caryophyllea	Nem védett / jelentős
Aira elegantissima	Nem védett / jelentős
Deschampsia flexuosa	Nem védett / indifferens
Phleum paniculatum	Nem védett / indifferens
Deschampsia caespitosa	Nem védett / indifferens
Deschampsia caespitosa subsp. parviflora	Nem védett / indifferens
Holcus mollis	Nem védett / indifferens
Holcus lanatus	Nem védett / indifferens
Arrhenatherum elatius	Nem védett / indifferens
Trisetum flavescens	Nem védett / indifferens
Ventenata dubia	Nem védett / jelentős
Avena fatua	Nem védett / indifferens
Avena sativa	Nem védett / indifferens
Avena nuda	Nem védett / indifferens
Avena sterilis subsp. ludoviciana	Nem védett / indifferens
Avena strigosa	Nem védett / indifferens
Avena barbata	Nem védett / indifferens
Helictotrichon pubescens	Nem védett / indifferens
Helictotrichon compressum	Védett
Helictotrichon pratense	Nem védett / indifferens
Helictotrichon praeustum	Nem védett / indifferens
Gaudinia fragilis	Nem védett / indifferens
Danthonia alpina	Nem védett / indifferens
Sieglingia decumbens	Nem védett / jelentős
Corynephorus canescens	Nem védett / jelentős
Koeleria glauca	Nem védett / indifferens
Koeleria glauca subsp. rochelii	Nem védett / indifferens
Koeleria pyramidata	Védett
Koeleria grandis	Nem védett / indifferens
Koeleria majoriflora	Védett
Koeleria javorkae	Védett
Koeleria cristata	Nem védett / indifferens
Apera spica-venti	Nem védett / indifferens
Apera interrupta	Nem védett / jelentős
Agrostis canina	Nem védett / indifferens
Agrostis vinealis	Nem védett / indifferens
Agrostis capillaris	Nem védett / indifferens
Agrostis stolonifera	Nem védett / indifferens
Calamagrostis arundinacea	Nem védett / indifferens
Calamagrostis varia	Védett
Calamagrostis stricta	Védett
Calamagrostis canescens	Nem védett / jelentős
Calamagrostis pseudophragmites	Védett
Calamagrostis epigeios	Nem védett / indifferens
Calamagrostis purpurea	Védett
Phleum pratense	Nem védett / indifferens
Phleum hubbardii	Nem védett / indifferens
Phleum phleoides	Nem védett / indifferens
Alopecurus pratensis	Nem védett / indifferens
Alopecurus myosuroides	Nem védett / indifferens
Alopecurus geniculatus	Nem védett / indifferens
Alopecurus aequalis	Nem védett / indifferens
Stipa bromoides	Védett
Stipa capillata	Nem védett / indifferens
Stipa tirsa	Védett
Stipa dasyphylla	Védett
Stipa borysthenica	Védett
Stipa joannis	Védett
Stipa joannis subsp. puberula	Védett
Stipa eriocaulis	Védett
Stipa pulcherrima	Védett
Piptatherum virescens	Nem védett / jelentős
Piptatherum miliaceum	Nem védett / indifferens
Milium effusum	Nem védett / indifferens
Hierochloe repens	Nem védett / jelentős
Hierochloe australis	Nem védett / jelentős
Anthoxanthum odoratum	Nem védett / indifferens
Phalaris canariensis	Nem védett / indifferens
Phalaroides arundinacea	Nem védett / indifferens
Eragrostis pilosa	Nem védett / indifferens
Eragrostis minor	Nem védett / indifferens
Eragrostis megastachya	Nem védett / jelentős
Eragrostis parviflora	Nem védett / indifferens
Cleistogenes serotina	Nem védett / jelentős
Cynodon dactylon	Nem védett / indifferens
Eleusine indica	Inváziós
Crypsis aculeata	Nem védett / indifferens
Heleochloa alopecuroides	Nem védett / jelentős
Heleochloa schoenoides	Nem védett / jelentős
Tragus racemosus	Inváziós
Leersia oryzoides	Nem védett / indifferens
Cenchrus incertus	Inváziós
Panicum capillare	Inváziós
Panicum miliaceum	Nem védett / indifferens
Panicum miliaceum subsp. ruderale	Inváziós
Panicum philadelphicum	Nem védett / indifferens
Digitaria ischaemum	Nem védett / indifferens
Digitaria sanguinalis	Nem védett / indifferens
Digitaria ciliaris	Nem védett / indifferens
Echinochloa crus-galli	Nem védett / indifferens
Echinochloa occidentalis	Nem védett / indifferens
Echinochloa phyllopogon	Inváziós
Echinochloa oryzoides	Inváziós
Echinochloa eruciformis	Inváziós
Setaria verticillata	Nem védett / indifferens
Setaria × decipiens	Nem védett / indifferens
Setaria pumila	Nem védett / indifferens
Setaria viridis	Nem védett / indifferens
Setaria italica	Nem védett / indifferens
Bothriochloa ischaemum	Nem védett / indifferens
Chrysopogon gryllus	Nem védett / indifferens
Sorghum halepense	Inváziós
Sorghum sudanense	Nem védett / indifferens
Sorghum bicolor	Nem védett / indifferens
Acorus calamus	Védett
Arum maculatum	Nem védett / indifferens
Arum orientale subsp. bessarabicum	Nem védett / jelentős
Calla palustris	Nem védett / indifferens
Lemna trisulca	Nem védett / indifferens
Lemna minor	Nem védett / indifferens
Lemna gibba	Nem védett / jelentős
Spirodela polyrhiza	Nem védett / indifferens
Wolffia arrhiza	Nem védett / jelentős
Sparganium minimum	Fokozottan védett
Sparganium emersum	Nem védett / jelentős
Sparganium erectum	Nem védett / indifferens
Sparganium erectum subsp. microcarpum	Nem védett / jelentős
Sparganium erectum subsp. neglectum	Nem védett / indifferens
Typha minima	Nem védett / jelentős
Typha laxmannii	Inváziós
Typha angustifolia	Nem védett / indifferens
Typha latifolia	Nem védett / indifferens
Typha shuttleworthii	Nem védett / jelentős
Huperzia selago	Védett
Lycopodium annotinum	Védett
Lycopodium clavatum	Védett
Diphasium complanatum	Védett
Diphasium tristachyum	Nem védett / jelentős
Diphasium issleri	Védett
Selaginella helvetica	Védett
Equisetum telmateia	Nem védett / jelentős
Equisetum arvense	Nem védett / indifferens
Equisetum sylvaticum	Védett
Equisetum fluviatile	Nem védett / jelentős
Equisetum palustre	Nem védett / indifferens
Equisetum hyemale	Védett
Equisetum × moorei	Védett
Equisetum ramosissimum	Nem védett / indifferens
Equisetum variegatum	Védett
Botrychium lunaria	Védett
Botrychium matricariifolium	Fokozottan védett
Botrychium multifidum	Fokozottan védett
Botrychium virginianum subsp. europaeum	Fokozottan védett
Ophioglossum vulgatum	Védett
Osmunda regalis	Fokozottan védett
Pteridium aquilinum	Nem védett / indifferens
Cheilanthes marantae	Fokozottan védett
Polypodium vulgare	Nem védett / indifferens
Polypodium interjectum	Nem védett / jelentős
Phyllitis scolopendrium	Védett
Asplenium adiantum-nigrum	Védett
Asplenium ruta-muraria	Nem védett / indifferens
Asplenium lepidum	Védett
Asplenium septentrionale	Nem védett / jelentős
Asplenium fontanum	Védett
Asplenium trichomanes	Nem védett / indifferens
Asplenium trichomanes subsp. quadrivalens	Nem védett / indifferens
Asplenium viride	Védett
Ceterach officinarum	Védett
Ceterach javorkaeanum	Védett
Oreopteris limbosperma	Védett
Thelypteris palustris	Védett
Gymnocarpium dryopteris	Védett
Gymnocarpium robertianum	Védett
Phegopteris connectilis	Védett
Athyrium filix-femina	Nem védett / indifferens
Matteuccia struthiopteris	Védett
Cystopteris fragilis	Nem védett / indifferens
Woodsia ilvensis	Fokozottan védett
Woodsia alpina	Védett
Polystichum lonchitis	Védett
Polystichum aculeatum	Védett
Polystichum braunii	Védett
Polystichum setiferum	Védett
Jungermannia subulata	Védett
Dryopteris cristata	Fokozottan védett
Dryopteris filix-mas	Nem védett / indifferens
Dryopteris pseudo-mas	Védett
Dryopteris carthusiana	Védett
Dryopteris dilatata	Védett
Dryopteris expansa	Védett
Dryopteris × tavelii	Nem védett / jelentős
Blechnum spicant	Védett
Marsilea quadrifolia	Védett
Salvinia natans	Védett
Azolla caroliniana	Inváziós
Taxus baccata	Nem védett / jelentős
Abies alba	Nem védett / jelentős
Picea abies	Nem védett / indifferens
Larix decidua	Nem védett / indifferens
Pinus sylvestris	Nem védett / indifferens
Pinus nigra	Nem védett / indifferens
Juniperus communis	Nem védett / indifferens
Platycladus orientalis	Nem védett / indifferens
Ephedra distachya	Fokozottan védett
Azolla filiculoides	Inváziós
Pyrus pyraster subsp. anchras	Nem védett / indifferens
Crataegus curvisepala	Nem védett / jelentős
Lotus corniculatus var. villosus	Nem védett / indifferens
Vicia grandiflora subsp. sordida	Nem védett / indifferens
Vicia grandiflora subsp. biebersteinii	Nem védett / indifferens
Acer acuminatilobum	Nem védett / jelentős
Lindernia dubia	Nem védett / jelentős
Asarum europaeum subsp. caucasicum	Nem védett / indifferens
Myriophyllum brasiliense	Inváziós
Chamaecytisus virescens	Nem védett / jelentős
Polygonum arenastrum	Nem védett / indifferens
Scilla drunensis subsp. buekkensis	Védett
Scilla paratheticum	Védett
Epipactis placentina	Fokozottan védett
Cyperus esculentus	Nem védett / indifferens
Carex curvata	Nem védett / indifferens
Agropyron elongatum	Védett
Sphagnum palustre	Védett
Mannia triandra	Védett
Riccia frostii	Védett
Riccia huebeneriana	Védett
Lophozia ascendens	Védett
Frullania inflata	Védett
Sphagnum compactum	Védett
Sphagnum subsecundum	Védett
Sphagnum contortum	Védett
Sphagnum platyphyllum	Védett
Sphagnum obtusum	Védett
Sphagnum recurvum	Védett
Sphagnum squarrosum	Védett
Sphagnum teres	Védett
Sphagnum fimbriatum	Védett
Sphagnum girgensohnii	Védett
Sphagnum russowii	Védett
Sphagnum subnitens	Védett
Sphagnum centrale	Védett
Sphagnum magellanicum	Védett
Sphagnum auriculatum	Védett
Sphagnum capillifolium	Védett
Sphagnum cuspidatum	Védett
Sphagnum warnstorfi	Védett
Buxbaumia viridis	Védett
Dicranella humilis	Védett
Dicranum viride	Védett
Fissidens arnoldii	Védett
Fissidens exiguus	Védett
Fissidens algarvicus	Védett
Weissia rostellata	Védett
Phascum floerkeanum	Védett
Aloina bifrons	Védett
Pterygoneurum lamellatum	Védett
Tortula brevissima	Védett
Grimmia plagiopodia	Védett
Grimmia sessitana	Védett
Grimmia teretinervis	Védett
Ephemerum cohaerens	Védett
Ephemerum recurvifolium	Védett
Physcomitrium sphaericum	Védett
Bryum neodamense	Védett
Bryum warneum	Védett
Bryum stirtonii	Védett
Bryum versicolor	Védett
Meesia triquetra	Védett
Campylostelium saxicola	Védett
Orthotrichum stellatum	Védett
Orthotrichum rogeri	Védett
Orthotrichum scanicum	Védett
Neckera pennata	Védett
Anacamptodon splachnoides	Védett
Anomodon rostratus	Védett
Calliergon stramineum	Védett
Calliergon giganteum	Védett
Scorpidium scorpioides	Védett
Campylium elodes	Védett
Drepanocladus sendtneri	Védett
Drepanocladus exannulatus	Védett
Drepanocladus vernicosus	Védett
Drepanocladus revolvens	Védett
Drepanocladus lycopodioides	Védett
Amblystegium saxatile	Védett
Brachythecium geheebii	Védett
Brachythecium oxycladum	Védett
Rynchostegium rotundifolium	Védett
Rynchostegiella jacquinii	Védett
Taxiphyllum densifolium	Védett
Asterella saccata	Védett
Brachydontium trichoides	Védett
Desmatodon cernuus	Védett
Didymodon glaucus	Védett
Enthostodon hungaricus	Védett
Pyramidula tetragona	Védett
Hilpertia velenovskyi	Védett
Reynoutria × bohemica	Inváziós
Myosotis sicula	Nem védett / jelentős
Leucobryum glaucum	Védett
Asplenium × alternifolium	Nem védett / indifferens
Crocus vittatus	Védett
Dryopteris × sarvelii	Védett
Duchesnea indica	Inváziós
Ptelea trifoliata	Nem védett / indifferens
Heracleum sosnowskyi	Inváziós
Reynoutria aubertii	Inváziós
Sorbus × semipinnata	Védett
Sorbus × rotundifolia	Nem védett / jelentős
Sorbus × buekkensis	Védett
Anogramma leptophylla	Védett
Calamagrostis villosa	Nem védett / jelentős
Cardaminopsis halleri	Nem védett / jelentős
Carex transsylvanica	Védett
Dactylorhiza lapponica	Védett
Dactylorhiza ochroleuca	Fokozottan védett
Epipactis voethii	Védett
Lilium martagon subsp. alpinum	Védett
Sorbus × javorkae	Védett
Sorbus × pannonica	Védett
Sorbus incisa	Védett
Sorbus × subdanubialis	Védett
Hypericum mutilum	Nem védett / indifferens
Dactylorhiza incarnata var. ochrantha	Védett
Ageratina altissima	Inváziós
Orobanche pancicii	Védett
Paeonia tenuifolia	Fokozottan védett
Robinia hispida	Inváziós
Aster laevis	Inváziós
Ceratocephalus falcata	Nem védett / jelentős
Bolboschoenus glaucus	Nem védett / jelentős
Bolboschoenus laticarpus	Nem védett / jelentős
Bolboschoenus planiculmis	Nem védett / jelentős
Orobanche bartlingii	Védett
Berberis julianae	Inváziós
Epipactis futakii	Védett
Epipactis helleborine subsp. minor	Védett
Epipactis helleborine subsp. orbicularis	Védett
Epipactis latina	Védett
Parietaria diffusa	Inváziós
Corispermum marschallii	Nem védett / jelentős
Corispermum leptopterum	Nem védett / jelentős
Salsola collina	Nem védett / indifferens
Montia linearis	Nem védett / indifferens
Soldanella hungarica	Nem védett / indifferens
Sorbus × vrabelyiana	Nem védett / jelentős
Agropyron elongatum cv. szarvasi-1	Inváziós
Sorbus × pseudograeca	Védett
Spiraea × van-houttei	Nem védett / indifferens
× Asplenoceterach badense	Védett
Paulownia tomentosa	Inváziós
Euphorbia lathyris	Inváziós
Asplenium × murbeckii	Nem védett / indifferens
Rubus armeniacus	Inváziós
Brunnera macrophylla	Inváziós
Sorbus carpinifolia	Védett
Petunia × atkinsiana	Inváziós
Koelreuteria paniculata	Inváziós
Pyracantha coccinea	Inváziós
Carduus × budaianus	Védett
Carduus × littoralis	Védett
Cirsium × candolleanum	Védett
Silene donetzica	Nem védett / jelentős
Sporobolus cryptandrus	Inváziós
Viola sororia	Inváziós
Silphium perfoliatum	Inváziós
Yucca filamentosa	Nem védett / indifferens
Catalpa bignonioides	Nem védett / indifferens
Campsis radicans	Nem védett / indifferens
Arundo donax	Nem védett / indifferens
Gaillardia pulchella	Inváziós
Lycopersicon esculentum	Nem védett / indifferens
Datura wrightii	Nem védett / indifferens
Persica vulgaris	Nem védett / indifferens
Cornus alba	Nem védett / indifferens
Amaranthus cruentus	Nem védett / indifferens
Gypsophila perfoliata	Nem védett / indifferens
Rubus praecox	Nem védett / indifferens
Broussonetia papyrifera	Nem védett / indifferens
Heliopsis helianthoides	Nem védett / indifferens
Cardamine occulta	Nem védett / indifferens
Euphorbia prostrata	Nem védett / indifferens
Platanus × hybrida	Nem védett / indifferens
Thuja orientalis	Nem védett / indifferens
Gaillardia aristata	Inváziós
Euphorbia myrsinites	Nem védett / indifferens
Chamaecyparis lawsoniana	Nem védett / indifferens
Fallopia x bohemica	Inváziós
Bison bison	Inváziós
Callosciurus erythraeus	Inváziós
Citellus citellus	Fokozottan védett
Crocidura sp.	Védett
Erinaceus roumanicus	Védett
Herpestes javanicus	Inváziós
Muntiacus reevesii	Inváziós
Mustela eversmannii	Védett
Mustela nivalis	Védett
Myocastor coypus	Inváziós
Myoxus glis	Védett
Nasua nasua	Inváziós
Nasua nasua	Inváziós
Neomys sp.	Védett
Ovis musimon	Inváziós
Sciurus carolinensis	Inváziós
Sciurus niger	Inváziós
Sorex araneus	Védett
Sorex sp.	Védett
Spalax leucodon	Fokozottan védett
Tamias sibiricus	Inváziós
Barbastella sp.	Védett
Chiroptera	Védett
Eptesicus nilssonii	Védett
Eptesicus sp.	Védett
Miniopterus sp.	Védett
Myotis blythii	Védett
Myotis oxygnathus	Védett
Myotis sp.	Védett
Nyctalus sp.	Védett
Pipistrellus savii	Védett
Pipistrellus sp.	Védett
Plecotus sp.	Védett
Rhinolophus sp.	Védett
Vespertilio sp.	Védett
Acrida hungarica	Védett
Actebia fugax	Védett
Aglais io	Védett
Amphimelania holandri	Védett
Amphipyra cinnamomea	Védett
Anaciaeschna isosceles	Védett
Anodonta woodiana	Inváziós
Apatura sp.	Védett
Argynnis paphia f. valesiana	Védett
Aricia agestis	Védett
Aricia artaxerxes	Védett
Aricia eumedon	Védett
Arion vulgaris	Inváziós
Asteroscopus syriacus	Fokozottan védett
Astiotes dilecta	Védett
Bittacus hageni	Védett
Bittacus italicus	Védett
Bombus silvarum	Védett
Callimorpha quadripunctaria	Védett
Callimorpha quadripunctata	Védett
Calomera littoralis	Védett
Calomera littoralis nemoralis	Védett
Camptogramma scripturata	Védett
Camptorrhinus simplex	Védett
Camptorrhinus statua	Védett
Carabus clathratus	Védett
Carabus convexus convexus	Védett
Carabus linnei	Védett
Carabus scabriusculus scabriusculus	Védett
Carabus sp.	Védett
Carabus ullrichi	Védett
Carabus zawadszkii	Fokozottan védett
Caradrina gilva	Védett
Chamaesphecia colpiformis	Védett
Chariaspilates formosarius	Védett
Charissa ambiguata	Védett
Charissa intermedia	Védett
Charissa pullata	Védett
Charissa variegata	Védett
Chilostoma banaticum	Védett
Chlaenius decipiens	Védett
Chlaenius sulcicollis	Védett
Chondrosoma fiduciarium	Fokozottan védett
Cicindela transversalis	Védett
Coraebus fasciatus	Védett
Coscinia cribraria	Védett
Cucullia mixta	Fokozottan védett
Dasypoda mixta	Védett
Diabrotica virgifera virgifera	Inváziós
Dichagyris musiva	Védett
Dioszeghyana schmidtii	Fokozottan védett
Discoloxia blomeri	Védett
Dorcadion decipiens	Védett
Dorcadion fulvum	Védett
Dorcadion fulvum cervae	Fokozottan védett
Dorcadion fulvum fulvum	Védett
Ennomos quercarius	Védett
Epatolmis luctifera	Védett
Ephemerella mesoleuca	Védett
Ephesia diversa	Védett
Eresus cinnaberinus	Védett
Eriocheir sinensis	Inváziós
Eublemma pannonica	Fokozottan védett
Eudia pavonia	Védett
Eudia spini	Védett
Euphya scripturata	Védett
Euxoa hastifera	Védett
Everes alcetas	Védett
Everes decolorata	Védett
Fabula zollikoferi	Védett
Fagotia acicularis	Védett
Fagotia daudebartii	Védett
Formica execta	5_melleklet
Formica polyctenarufa	5_melleklet
Formica sp.	5_melleklet
Gerris najas	Védett
Haitia acuta	Inváziós
Homarus americanus	Inváziós
Hygromia kovacsi	Fokozottan védett
Hygromia transsylvanica	Védett
Hyles galii	Védett
Hypantria cunea	Inváziós
Hypenodes pannonica	Védett
Hyponephele lupina	Védett
Ichneumon dispar	Védett
Inocellia braueri	Védett
Isognomostoma isognomostoma	Védett
Isophya camptoxypha	Védett
Isophya pienensis	Védett
Jolana iolas	Fokozottan védett
Lamellocossus terebrus	Védett
Lampra rutilans	Védett
Lamprodila decipiens	Védett
Lamprodila festiva	Védett
Lamprodila mirifica	Védett
Lamprodila rutilans	Védett
Liocola lugubris	Védett
Lycaena dispar hungarica	Védett
Lycosa vultuosa	Védett
Macroplea mutica	Védett
Macrosiagon bimaculata	Védett
Maculinea alcon	Védett
Maculinea alcon xerophila	Védett
Maculinea arion	Védett
Maculinea nausithous	Védett
Maculinea teleius	Védett
Mantispa aphavexelle	Védett
Megopis scabricornis	Védett
Melitaea ornata	Védett
Modicogryllus truncatus	Védett
Morimus funereus	Védett
Myrmecaelurus punctulatus	Védett
Nemesia pannonica	Védett
Netocia hungarica	Védett
Netocia ungarica	Védett
Potosia ungarica	Védett
Nymphalis atalanta	Védett
Nymphalis c-album	Védett
Nymphalis io	Védett
Nymphalis urticae	Védett
Nymphalis vau-album	Védett
Odontognophos dumetatus	Védett
Oodescelis melas	Védett
Oodescelis polita	Védett
Orconectes virilis	Inváziós
Oryctes nasicornis holdhausi	Védett
Oxychilus depressus	Védett
Oxychilus orientalis	Védett
Oxytrypia orbiculosa	Fokozottan védett
Pacifastacus leniusculus	Inváziós
Paladilhia hungarica	Védett
Perforatella vicinus	Védett
Perizoma sagittata	Védett
Pharmacis fusconebulosus	Védett
Phenacolimax annularis	Védett
Phengaris alcon	Védett
Phengaris arion	Védett
Phengaris nausithous	Védett
Phengaris teleius	Védett
Phyllodesma ilicifolia	Védett
Phyllomorpha laciniata	Védett
Plebejus idas	Védett
Plebejus sephirus	Fokozottan védett
Poecilimon brunneri	Fokozottan védett
Pomatias rivulare	Védett
Potosia aeruginosa	Védett
Potosia fieberi	Védett
Procambarus clarkii	Inváziós
Procambarus falax	Inváziós
Procambarus falax virginalis	Inváziós
Procambarus falax forma virginalis	Inváziós
Procambarus fallax	Inváziós
Procambarus sp.	Inváziós
Potosia affinis	Védett
Protichneumon pisarius	Védett
Pseudophilotes schiffermuelleri	Védett
Psilothrix femoralis	Védett
Ptilophorus dufourii	Védett
Reskovitsia alborivularis	Védett
Rheumaptera undulata	Védett
Rhyparioides metelkanus	Fokozottan védett
Sadleriana pannonica	Védett
Saragossa implexa	Védett
Scarites terricola	Védett
Scoliantides orion	Védett
Shargacucullia thapsiphaga	Védett
Sphyracephala europaea	Védett
Stilbium cyanurum	Védett
Stylurus flavipes	Védett
Synansphecia affinis	Védett
Theodoxus danubialis danubialis	Védett
Trebacosa europaea	Védett
Trichia unidentata	Védett
Valvata naticina	Védett
Vespa velutina nigrithorax	Inváziós
Agaricus bohusii	Védett
Amanita caesarea	Védett
Amanita lepiotoides	Védett
Amanita vittadinii	Védett
Battarrea phalloides	Védett
Boletus dupainii	Védett
Cantharellus melanoxeros	Védett
Cetraria aculeata	Védett
Cetraria islandica	Védett
Cladonia arbuscula	Védett
Cladonia magyarica	Védett
Cladonia mitis	Védett
Cladonia rangiferina	Védett
Cortinarius (Phl.) paracephalixus	Védett
Cortinarius (Phl.) praestans	Védett
Disciotis venosa	Védett
Elaphomyces anthracinus	Védett
Elaphomyces leveillei	Védett
Elaphomyces maculatus	Védett
Elaphomyces mutabilis	Védett
Elaphomyces persooni	Védett
Elaphomyces virgatosporus	Védett
Endoptychum agaricoides	Védett
Entoloma porphyrophaeum	Védett
Flammulina ononidis	Védett
Ganoderma cupreolaccatum	Védett
Geastrum hungaricum	Védett
Gomphidius roseus	Védett
Gomphus clavatus	Védett
Grifola frondosa	Védett
Gyrodon lividus	Védett
Hapalopilus croceus	Védett
Hericium cirrhatum	Védett
Hericium erinaceum	Védett
Hygrocybe calyptriformis	Védett
Hygrocybe punicea	Védett
Hygrophorus marzuolus	Védett
Hygrophorus poetarum	Védett
Hypsizygus ulmarius	Védett
Lactarius helvus	Védett
Leccinum variicolor	Védett
Leucopaxillus compactus	Védett
Leucopaxillus lepistoides	Védett
Leucopaxillus macrocephalus	Védett
Lobaria pulmonaria	Védett
Lycoperdon mammiforme	Védett
Peltigera leucophlebia	Védett
Phellodon niger	Védett
Pholiota sqarrosoides	Védett
Phylloporus pelletieri	Védett
Pluteus umbrosus	Védett
Polyporus rhizophilus	Védett
Polyporus tuberaster	Védett
Polyporus umbellatus	Védett
Porphyrellus porphyrosporus	Védett
Pseudoboletus parasiticus	Védett
Rhodotus palmatus	Védett
Russula claroflava	Védett
Sarcodon joeides	Védett
Sarcodon scabrosus	Védett
Scutiger pescaprae	Védett
Solorina saccata	Védett
Squamanita schreieri	Védett
Strobilomyces strobilaceus	Védett
Tulostoma volvulatum	Védett
Umbilicaria deusta	Védett
Umbilicaria hirsuta	Védett
Umbilicaria polyphylla	Védett
Usnea florida	Védett
Volvariella bombycina	Védett
Xanthoparmelia pokornyi	Védett
Xanthoparmelia pseudohungarica	Védett
Xanthoparmelia pulvinaris	Védett
Xanthoparmelia ryssolea	Védett
Xanthoparmelia subdiffluens	Védett
Acipenser gueldenstaedti	Védett
Alosa immaculata	Védett
Carassius auratus gibelio	Inváziós
Carassius gibelio auratus	Inváziós
Cobitis taenia	Védett
Eudontomyzon vladykovi	Fokozottan védett
Gobio albipinnatus	Védett
Gobio kesslerii	Fokozottan védett
Gobio uranoscopus	Fokozottan védett
Gymnocephalus schraetzer	Védett
Leuciscus souffia	Védett
Rhodeus sericeus	Védett
Rhodeus sericeus amarus	Védett
Romanogobio albipinnatus	Védett
Rutilus pigus	Védett
Rutilus pigus virgo	Védett
Dolichophis caspius	Fokozottan védett
Lacerta agilis argus	Védett
Lacerta sp.	Védett
Lacerta vivipara	Fokozottan védett
Lacerta vivipara pannonica	Fokozottan védett
Natrix sp.	Védett
Podarcis sp.	Védett
Reptilia	Védett
Trachemys scripta scripta	Inváziós
Vipera sp.	Védett
Zamenis longissimus	Védett
Amphibia	Védett
Anura	Védett
Bombina sp.	Védett
Bufo sp.	Védett
Lissotriton sp.	Védett
Lissotriton vulgaris	Védett
Lithobates catesbeianus	Inváziós
Mesotriton alpestris	Fokozottan védett
Pelophylax kl. esculentus	Védett
Pelophylax lessonae	Védett
Pelophylax ridibundus	Védett
Pelophylax sp.	Védett
Rana catesbeianus	Inváziós
Rana esculenta	Védett
Rana sp.	Védett
Triturus montandoni	8_melleklet
Triturus sp.	Védett
Actitis hypoleucos	Védett
Alopochen aegyptiaca	Inváziós
Lyrurus tetrix	Védett
Alopochen aegyptiacus 	Inváziós
Apus melba	Védett
Aquila fasciata	Fokozottan védett
Aquila pennata	Fokozottan védett
Ardea alba	Fokozottan védett
Bubo scandiacus	Fokozottan védett
Carduelis chloris	Védett
Cecropis daurica	Védett
Cercotrichas galactotes	8_melleklet
Chlidonias hybrida	Fokozottan védett
Chroicocephalus genei	Védett
Chroicocephalus ridibundus	Védett
Cisticola juncidis	Védett
Clamator glandarius	Védett
Corvus corone corone	Védett
Corvus splendens	Inváziós
Curruca communis	Védett
Curruca curruca	Védett
Curruca nisoria	Védett
Cyanistes caeruleus	Védett
Delichon urbica	Védett
Dendrocopos sp.	Védett
Dryobates minor	Védett
Egretta alba	Fokozottan védett
Elanus caeruleus	Védett
Emberiza rustica	Védett
Ereunetes alpina	Védett
Ereunetes ferrugineus	Védett
Ereunetes temminckii	Védett
Falco biarmicus	Fokozottan védett
Falco rusticolus	Fokozottan védett
Ficedula sp.	Védett
Grus virgo	Védett
Hippolais pallida	Védett
Hydrocoloeus minutus	Védett
Hydroprogne caspia	Védett
Ichthyaetus ichthyaetus	Védett
Ichthyaetus melanocephalus	Fokozottan védett
Larus audouinii	Védett
Larus fuscus graellsii	Védett
Leiopicus medius	Védett
Lophophanes cristatus	Védett
Mergus albellus	Védett
Microcarbo pygmeus	Fokozottan védett
Miliaria calandra	Védett
Monticola solitarius	Védett
Nyctea scandiaca	Fokozottan védett
Pastor roseus	Védett
Periparus ater	Védett
Phalaropus sp.	Védett
Pluvialis dominicus	8_melleklet
Podiceps ruficollis	Védett
Poecile montanus	Védett
Poecile palustris	Védett
Regulus ignicapilla	Védett
Saxicola torquatus	Védett
Stercorarius skua	Védett
Sternula albifrons	Fokozottan védett
Tarsiger cyanurus	Védett
Tetrastes bonasia	Fokozottan védett
Threskiornis aethiopicus	Inváziós
Tringa hypoleucos	Védett
Vanellus gregarius	Fokozottan védett
Vanellus leucurus	Védett
Vanellus spinosus	Védett
Xema sabini	Védett
Aconitum sp.	Védett
Aconitum variegatum	Védett
Adonis volgensis	Fokozottan védett
Adonis x hybrida	Fokozottan védett
Alchemilla micans	Védett
Alchemilla sp.	Védett
Allium marginatum	Védett
Allium paniculatum	Védett
Allium paniculatum ssp. marginatum	Védett
Alternanthera philoxeroides	Inváziós
Amaranthus powellii	Inváziós
Ambrosia artemisiifolia	Inváziós
Anacamptis coriophora	Védett
Anacamptis palustris ssp. elegans	Védett
Anacamptis palustris ssp. palustris	Védett
Anchusa ochroleuca	Fokozottan védett
Anthriscus nitidus	Védett
Apamea syriaca	Védett
Aquilegia nigricans	Védett
Aruncus dioicus	Védett
Asperula taurina	Védett
Asplenium ceterach	Védett
Asplenium javorkeanum	Védett
Asplenium scolopendrium	Védett
Aster ×salignus	Inváziós
Aster lanceolatus	Inváziós
Aster sedifolius ssp. canus	Védett
Aster sedifolius ssp. sedifolius	Védett
Aster x versicolor	Inváziós
Astragalus vesicarius ssp. albidus	Védett
Aurinia saxatilis	Védett
Avena sterilis ssp. ludoviciana	Inváziós
Avenula compressa	Védett
Azolla mexicana	Inváziós
Baccharis halimifolia	Inváziós
Botrychium virginianum	Fokozottan védett
Brachydontium trichodes	Védett
Brachytecium geheebii	Védett
Brachytecium oxycladum	Védett
Buddleja davidii	Inváziós
Bulbocodium vernum	Fokozottan védett
Cabomba caroliniana	Inváziós
Calamagrostis phragmitoides	Védett
Campylium vernicosus	Védett
Cardamine glanduligera	Védett
Cardamine waldsteinii	Védett
Carduus crassifolius ssp. glaucus	Védett
Carex cespitosa	Védett
Carex depressa ssp. transsilvanica	Védett
Carex hartmanii	Védett
Centaurea arenaria ssp. tauscheri	Védett
Centaurea montana ssp. mollis	Védett
Centaurea scabiosa ssp. sadleriana	Védett
Centaurea triumfettii	Védett
Centaurea triumfettii ssp. aligera	Védett
Cephalanthera alba	Védett
Cephalanthera sp.	Védett
Cerastium arvense ssp. matrense	Védett
Cerastium arvense ssp. molle	Védett
Ceratophyllum tanaiticum	Védett
Ceterach javorkeanum	Védett
Cetraria aculeata  	Védett
Chamaecytisus hirsutus ssp. ciliatus	Védett
Chamaecytisus rochelii	Védett
Chamaenerion dodonaei	Védett
Chlorocrepis staticifolia	Védett
Cirsium boujartii	Védett
Cladonia spp. (subgenus Cladinia)	Nem védett
Conyza canadensis	Inváziós
Cotoneaster sp.	Védett
Crataegus x degenii	Fokozottan védett
Cyperus esculentus var. leptostachyus	Inváziós
Dactylorhiza incarnata ssp. haematodes	Védett
Dactylorhiza incarnata ssp. incarnata	Védett
Dactylorhiza incarnata ssp. ochroleuca	Fokozottan védett
Dactylorhiza incarnata ssp. serotina	Védett
Jurinea sp.	Védett
Dactylorhiza maculata ssp. transylvanica	Védett
Dactylorhiza viridis	Védett
Daphne sp.	Védett
Dianthus arenarius	Védett
Dianthus arenarius ssp. borussicus	Védett
Dianthus giganteiformis ssp. pontederae	Védett
Dianthus plumarius ssp. lumnitzeri	Fokozottan védett
Dianthus plumarius ssp. praecox	Fokozottan védett
Diphasiastrum complanatum	Védett
Diphasiastrum issleri	Védett
Drepanocladus cossonii	Védett
Dryopteris affinis	Védett
Echium maculatum	Védett
Eichhornia crassipes	Inváziós
Elymus elongatus	Védett
Epipactis atrorubens ssp. borbasii	Védett
Epipactis exilis	Fokozottan védett
Epipactis mecsekensis	Védett
Epipactis moravica	Védett
Epipactis neglecta	Védett
Epipactis peitzii	Védett
Epipactis pseudopurpurata	Védett
Epipactis sp.	Védett
Erigeron annuus	Inváziós
Erysimum witmannii ssp. pallidiflorum	Fokozottan védett
Erysimum wittmannii	Fokozottan védett
Fallopia japonica	Inváziós
Fallopia sachalinensis	Inváziós
Fallopia sp.	Inváziós
Festuca pallens ssp. pannonica	Védett
Festuca wagneri	Védett
Gentianella amarella	Védett
Gentianopsis ciliata	Védett
Gunnera manicata	Inváziós
Gunnera tinctoria	Inváziós
Gymnadenia densiflora	Védett
Gymnocarpium sp.	Védett
Gypsophila arenaria	Védett
Gypsophila fastigiata	Védett
Gypsophila fastigiata ssp. arenaria	Védett
Hamatocaulis vernicosus	Védett
Helianthus ×laetiflorus	Inváziós
Helianthus decapetalus	Inváziós
Helianthus pauciflorus	Inváziós
Helianthus sp.	Inváziós
Heracleum persicum	Inváziós
Hesperis matronalis ssp. vrabelyiana	Fokozottan védett
Hesperis vrabelyiana	Fokozottan védett
Hieracium bupleuroides	Védett
Himantoglossum jankae	Fokozottan védett
Hippocrepis emerus	Védett
Hippophae rhamnoides	Védett
Hippophaë rhamnoides	Védett
Humulus japonicus	Inváziós
Hydrocotyle ranunculoides	Inváziós
Iris aphylla ssp. hungarica	Fokozottan védett
Iris humilis ssp. arenaria	Védett
Jovibarba globifera	Védett
Jovibarba globifera ssp. globifera	Védett
Jovibarba globifera ssp. hirta	Védett
Jovibarba sp.	Védett
Juncus alpinoarticulatus	Védett
Jurinea glycacantha	Védett
Knautia arvensis ssp. kitaibelii	Fokozottan védett
Knautia kitaibelii ssp. tomentella	Fokozottan védett
Knautia maxima	Védett
Krascheninnikovia ceratoides	Védett
Lagarosiphon major	Inváziós
Lathyrus lacteus	Védett
Lathyrus linifolius	Védett
Lathyrus pannonicus ssp. collinus	Védett
Lathyrus transsylvanicus	Védett
Leucanthemella serotina	Védett
Lilium bulbiferum ssp. bulbiferum	Fokozottan védett
Linaria biebersteinii	Védett
Linum hirsutum ssp. glabrescens	Védett
Linum hirsutum ssp. hirsutum	Védett
Ludwigia grandiflora	Inváziós
Ludwigia peploides	Inváziós
Lycopodium complanatum	Védett
Lycopodium issleri	Védett
Lycopodium spp.	Védett
Lysichiton americanus	Inváziós
Melampyrum bihariense	Védett
Micromeria thymifolia	Fokozottan védett
Microstegium vimineum	Inváziós
Minuartia hirsuta	Védett
Monochoria korsakowii	Inváziós
Montia fontana ssp. chondrosperma	Védett
Myosotis laxa ssp. caespitosa	Védett
Myriophyllum aquaticum	Inváziós
Myriophyllum heterophyllum	Inváziós
Narcissus poeticus ssp. radiiflorus	Védett
Narcissus radiiflorus	Védett
Neotinea tridentata	Védett
Neotinea ustulata	Védett
Neotinea ustulata ssp. aestivalis	Védett
Neotinea ustulata ssp. ustulata	Védett
Neotinea x dietrichiana	Védett
Neottia ovata	Védett
Notholaena marantae	Fokozottan védett
Onosma arenaria	Védett
Onosma arenaria ssp. tuberculata	Védett
Onosma sp.	Védett
Ophrys bertolonii	Fokozottan védett
Ophrys fuciflora ssp. fuciflora	Fokozottan védett
Ophrys fuciflora ssp. holubyana	Fokozottan védett
Ophrys holoserica	Fokozottan védett
Ophrys oestrifera	Fokozottan védett
Ophrys scolopax	Fokozottan védett
Ophrys scolopax ssp. cornuta	Fokozottan védett
Ophrys sp.	Fokozottan védett
Ophrys sphegodes	Fokozottan védett
Opuntia fragilis	Inváziós
Orchidaceae	Védett
Orchis laxiflora	Védett
Orchis laxiflora ssp. elegans	Védett
Orchis laxiflora ssp. palustris	Védett
Orchis mascula	Védett
Orchis mascula ssp. signifera	Védett
Orchis palustris	Védett
Orchis sp.	Védett
Ornithogalum brevistylum	Védett
Ornithogalum pannonicum	Védett
Orobanche artemisiae-campestris	Védett
Orobanche coerulescens	Védett
Oxalis stricta	Inváziós
Oxytropis pilosa	Védett
Oxytropis pilosa ssp. hungarica	Védett
Paeonia officinalis ssp. banatica	Fokozottan védett
Panicum miliaceum ssp. ruderale	Inváziós
Panicum sp.	Inváziós
Parthenium hysterophorus	Inváziós
Paulownia sp.	Inváziós
Pennisetum setaceum	Inváziós
Persicaria bistorta	Védett
Persicaria perfoliata	Inváziós
Petrosimonia triandra	Védett
Phascum floekeanum	Védett
Pistia stratiotes	Inváziós
Platanthera sp.	Védett
Poa pannonica ssp. glabra	Védett
Poa scabra	Védett
Polystichum sp.	Védett
Potentilla indica	Inváziós
Potentilla palustris	Fokozottan védett
Primula auricula	Fokozottan védett
Primula farinosa	Fokozottan védett
Primula nutans	7_melleklet
Primula x brevistyla	Védett
Prospero elisae	Védett
Prospero paratheticum	Védett
Prunella x dissecta	Védett
Prunus serotina	Inváziós
Prunus tenella	Védett
Pseudolysimachion incanum	Védett
Pseudolysimachion longifolium	Védett
Pseudolysimachion spurium	Fokozottan védett
Pseudolysimachion spurium ssp. foliosum	Fokozottan védett
Puccinellia festuciformis ssp. intermedia	Védett
Pueraria lobata	Inváziós
Pueraria montana	Inváziós
Pueraria montana var. lobata	Inváziós
Pulsatilla flavescens	Fokozottan védett
Pulsatilla montana	Védett
Pulsatilla nigricans	Védett
Pulsatilla pratensis ssp. hungarica	Fokozottan védett
Pulsatilla pratensis ssp. nigricans	Védett
Pulsatilla sp.	Védett
Pulsatilla zimmermannii	Védett
Pyrola sp.	Védett
Ranunculus fluitans	Védett
Reynoutria sp.	Inváziós
Reynoutria x bohemica	Inváziós
Rhus typhina	Inváziós
Rhynchostegiella jacquinii	Védett
Rhynchostegium rotundifolium	Védett
Robinia pseudoacacia	Inváziós
Rosa villosa	Védett
Salix elaeagnos	Védett
Salix myrsinifolia	Védett
Scilla autumnalis agg.	Védett
Scilla bifolia agg.	Védett
Securigera elegans	Védett
Sedum acre ssp. neglectum	Védett
Sedum hillebrandtii	Védett
Sedum urvillei ssp. hillebrandtii	Védett
Sempervivum marmoreum auct. hung.	Védett
Sempervivum matricum	Védett
Sempervivum sp.	Védett
Seseli peucedanoides	Védett
Sesleria albicans	Védett
Sesleria caerulea	Védett
Sesleria heufleriana	Védett
Sesleria heufleriana ssp. hungarica	Védett
Sesleria sadleriana	Védett
Sesleria sp.	Védett
Silene bupleuroides	Védett
Silene dioica	Védett
Sium sisarum	Védett
Solidago gigantea	Inváziós
Solidago sp.	Inváziós
Sorbus × kitaibeliana	Védett
Sorbus acutiserratus	Védett
Sorbus adami	Védett
Sorbus andreanszkyana	Védett
Sorbus austriaca	Védett
Sorbus bakonyensis	Védett
Sorbus balatonica	Védett
Sorbus barabitsii	Védett
Sorbus barthae	Védett
Sorbus bodajkensis	Védett
Sorbus borosiana	Védett
Sorbus budaiana	Védett
Sorbus buekkensis	Védett
Sorbus cretica	Védett
Sorbus danubialis	Védett
Sorbus decipientiformis	Védett
Sorbus degenii	Védett
Sorbus dracofolius	Védett
Sorbus eugenii-kelleri	Védett
Sorbus gayerana	Védett
Sorbus gerecseensis	Védett
Sorbus hazslinszkyana	Védett
Sorbus huljakii	Védett
Sorbus javorkae	Védett
Sorbus karpatii	Védett
Sorbus latissima	Védett
Sorbus majeri	Védett
Sorbus pannonica	Védett
Sorbus pseudobakonyensis	Védett
Sorbus pseudodanubialis	Védett
Sorbus pseudolatifolia	Védett
Sorbus pseudosemiincisa	Védett
Sorbus pseudovertesensis	Védett
Sorbus redliana	Védett
Sorbus semiincisa	Védett
Sorbus simonkaiana	Védett
Sorbus sooi	Védett
Sorbus subdanubialis	Védett
Sorbus thaiszii	Védett
Sorbus tobani	Védett
Sorbus ulmifolia	Védett
Sorbus vajdae	Védett
Sorbus vallerubusensis	Védett
Sorbus vertesensis	Védett
Sorbus veszpremensis	Védett
Sorbus x buekkensis	Védett
Sorbus zolyomii	Védett
Sparganium natans	Fokozottan védett
Sphagnum russowi	Védett
Sphagnum sp.	Védett
Sphagnum spp.	Védett
Stipa pennata	Védett
Stipa stenophylla	Védett
Tephroseris aurantiaca	Védett
Tephroseris crispa	Védett
Tephroseris longifolia	Védett
Thalictrum minus ssp. pseudominus	Védett
Thalictrum pseudominus	Védett
Thlaspi caerulescens	Védett
Thlaspi hungaricum	Védett
Thlaspi jankae agg.	Védett
Thlaspi kovatsii	Védett
Thlaspi kovatsii ssp. schudichii	Védett
Torilis ucrainica	Védett
Trifolium subterraneum	Védett
Ulmus procera	Inváziós
Valeriana officinalis ssp. sambucifolia	Védett
Valeriana tripteris	Védett
Vicia narbonensis	Védett
Vitis riparia	Inváziós
Weisia rostellata	Védett
Xanthium albinum ssp. riparium	Inváziós
Xanthium sp.	Inváziós
Polymitarcis virgo	Védett
Fallopia x hybrida	Inváziós
Phytolacca acinosa	Inváziós
Anacamptis morio	Védett
Carabus ulrichii	Védett
Eresus sandaliatus	Védett
Eresus hermani	Védett
Eresus sp.	Védett
Ludwigia repens	Inváziós
Phyllostachys bambusoides	Inváziós
Hygrophila polysperma	Inváziós
Gaillardia sp.	Inváziós
Egeria densa	Inváziós
Shinnersia rivularis	Inváziós
Cherax quadricarinatus	Inváziós
Xiphophorus sp.	Inváziós
Xiphophorus maculatus	Inváziós
Proterorhinus semilunaris	Inváziós
Opuntia phaeacantha	Inváziós
Opuntia sp.	Inváziós
Larus heuglini	Védett
Ablepharus kitaibelii	Fokozottan védett
Phytolacca sp.	Inváziós
Linaria cannabina	Védett
Opuntia humifusa	Inváziós
Opuntia chlorotica	Inváziós
Opuntia compressa	Inváziós
Opuntia macrorhiza	Inváziós
Opuntia polyacantha	Inváziós
Opuntia tortispina	Inváziós
Azolla sp.	Inváziós
Graptemys kohni	Inváziós
Limnobium laevigatum	Inváziós
Myriophyllum sp.	Inváziós
Gymnocoronis spilanthoides	Inváziós
Acacia saligna	Inváziós
Acridotheres tristis	Inváziós
Lespedeza cuneata	Inváziós
Lygodium japonicum	Inváziós
Plotosus lineatus	Inváziós
Polygonum perfoliatum	Inváziós
Prosopis juliflora	Inváziós
Salvinia molesta	Inváziós
Triadica sebifera	Inváziós
Andropogon virginicus	Inváziós
Arthurdendyus triangulatus	Inváziós
Cardiospermum grandiflorum	Inváziós
Cortaderia jubata	Inváziós
Ehrharta calycina	Inváziós
Yucca sp.	Inváziós
Pelophylax esculentus	Védett
Dendrocygna bicolor	8_melleklet
Anas discors	8_melleklet
Testudo hermanni	8_melleklet
Carabus clathratus auraniensis	Védett
Carabus ulrichii fastuosus	Védett
Maculinea arion ligurica	Védett
Leuciscus aspius	Nem védett
Dianthus plumarius ssp. regis-stephani	Fokozottan védett
Carassius auratus auratus	Inváziós
Maculinea alcon alcon	Védett
\.


--
-- Name: ssp_vedettseg ssp_vedettseg_pkey; Type: CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_vedettseg
    ADD CONSTRAINT ssp_vedettseg_pkey PRIMARY KEY (species_name);


--
-- Name: ssp_vedettseg ssp_vedettseg_species_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ssp_admin
--

ALTER TABLE ONLY public.ssp_vedettseg
    ADD CONSTRAINT ssp_vedettseg_species_name_fkey FOREIGN KEY (species_name) REFERENCES public.ssp_speciesnames(species_name);


--
-- PostgreSQL database dump complete
--

