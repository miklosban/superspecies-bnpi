
{{
config(
    materialized='table',
    post_hook=["{{ postgres.index(this, 'subspecies')}}",
               'CREATE INDEX trgm_idx_species_name ON {{ this }} USING gin (species_name gin_trgm_ops)',
               'CREATE INDEX trgm_idx_magyarnev ON {{ this }} USING gin (magyarnev gin_trgm_ops)',
               "{{ postgres.index(this, 'namestatus')}}",
               "{{ postgres.index(this, 'taxon')}}",
               "{{ postgres.index(this, 'vedettseg')}}",],
    )
}}

SELECT s.species_name,subspecies,namestatus,auctor,
	g1.value AS bern,
	g2.value AS bonn,
	g3.value AS corine,
	g4.value AS iucn,
	g5.value AS mvk,
	g6.value AS voros_lista,
	g7.value AS washington, 
        string_agg(mispell,',') as mispells,
        string_agg(magyarnev,',') as magyarnev,
        vedettseg,f.values AS natura2000,eszmei_ertek,shortname,taxon,
     j.species_name AS "odonata_hun", n.species_name AS "fungi_hun", o.species_name AS "orthoptera_hun"
FROM ssp_speciesnames s
    -- General tables
        FULL JOIN ssp_subspecies c ON s.species_name=c.species_name
        FULL JOIN ssp_namestatus d ON s.species_name=d.species_name
        FULL JOIN ssp_auctor l ON s.species_name=l.species_name
        FULL JOIN ssp_iagreements_bern g1 ON s.species_name=g1.species_name
        FULL JOIN ssp_iagreements_bonn g2 ON s.species_name=g2.species_name
        FULL JOIN ssp_iagreements_corine g3 ON s.species_name=g3.species_name
        FULL JOIN ssp_iagreements_iucn g4 ON s.species_name=g4.species_name
        FULL JOIN ssp_iagreements_mvk g5 ON s.species_name=g5.species_name
        FULL JOIN ssp_iagreements_voros_lista g6 ON s.species_name=g6.species_name
        FULL JOIN ssp_iagreements_washington g7 ON s.species_name=g7.species_name
        FULL JOIN ssp_common_mispells m ON s.species_name=m.species_name
    -- Hungary special tables
        FULL JOIN ssp_nationalnames_hun b ON s.species_name=b.species_name
        FULL JOIN ssp_vedettseg e ON s.species_name=e.species_name
        FULL JOIN ssp_natura2000 f ON s.species_name=f.species_name
        FULL JOIN ssp_eszmeiertek h ON s.species_name=h.species_name
        FULL JOIN ssp_shortnames i ON s.species_name=i.species_name
        FULL JOIN ssp_taxon_hun k ON s.species_name=k.species_name
        FULL JOIN ssp_odonata_hun j ON s.species_name=j.species_name
        FULL JOIN ssp_fungi_hun n ON s.species_name=n.species_name
        FULL JOIN ssp_orthoptera_hun o ON s.species_name=o.species_name
    GROUP BY s.species_name,c.subspecies, d.namestatus, e.vedettseg, f.values, g1.value,g2.value, g3.value,g4.value, g5.value, g6.value, g7.value, h.eszmei_ertek, i.shortname, j.species_name,k.taxon, l.auctor, m.mispell, n.species_name, o.species_name

